<?php

$lang['required'] 			= "O campo %s é obrigatório.";
$lang['isset']				= "O campo %s deve ser informado.";
$lang['valid_email']		= "O campo %s deve conter um endereço de e-mail válido.";
$lang['valid_emails'] 		= "O campo %s deve conter endereços de e-mails válidos.";
$lang['valid_url'] 			= "The %s field must contain a valid URL.";
$lang['valid_ip'] 			= "The %s field must contain a valid IP.";
$lang['min_length']			= "O campo %s deve ter pelo menos %s caracteres.";
$lang['max_length']			= "O campo %s deve ter no máximo %s caracteres.";
$lang['exact_length']		= "O campo %s deve conter exatamente %s caracteres.";
$lang['alpha']				= "O campo %s deve conter apenas letras.";
$lang['alpha_numeric']		= "O campo %s deve conter apenas letras e números.";
$lang['alpha_dash']			= "O campo %s deve conter apenas letras, números, sublinhados e traços.";
$lang['numeric']			= "O campo %s deve conter apenas números.";
$lang['is_numeric']			= "O campo %s deve conter um valor válido.";
$lang['integer']			= "O campo %s deve conter um valor inteiro.";
$lang['regex_match']		= "The %s field is not in the correct format.";
$lang['matches']			= "O campo %s deve coincidir com o campo %s.";
$lang['is_natural']			= "O campo %s deve conter um número positivo.";
$lang['is_natural_no_zero']	= "O campo %s deve conter um número maior que zero.";
$lang['decimal']			= "O campo %s deve conter um número decimal.";
$lang['less_than']			= "The %s field must contain a number less than %s.";
$lang['greater_than']		= "The %s field must contain a number greater than %s.";

//MCV
$lang['valida_data']		= "O campo %s deve conter uma data válida.";
$lang['valida_timestamp']	= "O campo %s deve conter uma data válida.";
$lang['valida_moeda']		= "O campo %s deve conter um valor em reais (R$) válido.";
$lang['valida_combo']		= "O campo %s deve ser selecionado.";
$lang['valida_cnpj_cpf']    = "O campo %s deve ter um CNPJ ou CPF válido.";
$lang['valida_socios']      = "Todas as informações sobre os sócios devem ser válidas.";
$lang['valida_regiaoatend'] = "As informações sobre regiões de atendimento devem ser válidas.";
$lang['valida_atuacao']     = "O campo %s deve ser informado corretamente.";
$lang['valida_str_codigos'] = "O campo %s deve ser informado corretamente.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */