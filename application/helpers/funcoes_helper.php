<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Helper de Funções
*
* Contém funções auxiliares para o desenvolvimento.
* 
* @author		Moisés Viana
* @package		application
* @subpackage	application.funcoes_helper
* @since		2012-03-25
*
*/

/* 
 * sendEmail()
 * Envia um email, utilizando a biblioteca de integração do MAILEE.ME.
 * @param string title
 * @param string subject
 * @param string/array emails
 * @param string message
 * @return boolean send
 */
function sendEmail($title = null, $subject = null, $emails = null, $message = null)
{
	// Faz inclusão da biblioteca de envio Mailee
	include_once('application/libraries/Mailee.php');
	
	// Define a chave da API, necessária para envio correto dos emails
	if(!defined('MAILEE_CONFIG_SITE'))
	{
		define('MAILEE_CONFIG_SITE','http://api.32cc914607b0c.bnemail.mailee.me');
	}
	
	// Cria a mensagem, encaminhando os parâmetros, em seguida envia
	$objmessage = new MaileeMessage(array('from_name' => 'Biblioteca Nacional', 'from_email' => 'noreply@bn.br', 'title' => $title, 'subject' => $subject, 'emails' => $emails, 'html' => $message));
	
	// echo('1 - <br />');
	// print_r($objmessage);
	// echo('<br /><br />');
	
	// Salva a mensagem como rascunho
	$m = $objmessage->save();
	
	// echo('2 - <br />');
	// print_r($m);
	// echo('<br /><br />');
	
	// Envia mensagem (somente se $m for um objeto)
	if(is_object($m))
	{
		$m = $m->ready(array('when' => 'now'));
		
		// DEBUG
		// print_r( $m );
	
		// Valida algum possível erro
		return ($m->error == '' && $m->status == '200') ? true : false;
	} else { return false; }
}

/* 
 * get_table_dados
 *
 * @access	public
 * @param	integer	id do tipo de usuario
 * @return	string nome da tabela com dados de acordo com o tipo do usuário
 */
function get_table_dados($tipoUser)
{
	$table = '';
	
	switch($tipoUser)
	{
		case 1:
			$table = 'cadadm';
			break;
		case 2:
			$table = 'cadbiblioteca';
			break;
		case 3:
			$table = 'caddistribuidor';
			break;
		case 4:
			$table = 'cadpdv';
			break;
		case 5:
			$table = 'cadeditora';
			break;
		case 6:
			$table = 'cadbibliotecafinan';
			break;
		case 7:
			$table = 'cadbibliotecacomite';
			break;
	}
	
	return $table;
}

/* 
 * exibe_validacao_erros
 *
 * @access	public
 * @param	array	erros a serem exibidos
 * @return	string
 */
function exibe_validacao_erros($erros = Array())
{
	if (validation_errors() != '' || count($erros) > 0)
	{
		echo '<div id="errosValidacao" class="errosValidacao">';
		echo '	<div class="errorAlert">';
		echo '		<table align="center">';
		echo '			<tr>';
		echo '				<td><img src="' . URL_IMG . 'alert.gif" border="0" /></td>';
		echo '				<td style="padding-left:10px;"><b>ATENÇÃO:</b> Verifique os dados informados.</td>';
		echo '			</tr>';
		echo '		</table>';
		echo '	</div>';
		echo validation_errors();
		
		if (count($erros) > 0)
		{
			for ($i = 0; $i < count($erros); $i++)
			{
				echo '<div class="error" style="line-height:30px;">' . $erros[$i] . '</div>';
			}
		}
		
		echo '</div>';
	}
}

/* 
 * montaPaginacao
 *
 * @access	public
 * @param	nPaginas = numero total de paginas / nPag = numero da pagina atual / limit_de = a partir de qual registro vai mostrar / exibir_pp = qts registros irá exibir por página / urlPesq = url para link
 * @return	string html
 */
function montaPaginacao($nPaginas, $nPag, $limit_de, $exibir_pp, $urlLink)
{
	if ($nPaginas > 15)
	{
		if ($nPag > 1)
		{
			echo '&nbsp; <a href="' . $urlLink . '1" class="semefeito corPadrao" alt="ir para primeira página" title="ir para primeira página"><img src="' . URL_IMG . 'pag_pri.png" border="0" /></a>';
			echo '<a href="' . $urlLink . ($nPag - 1) . '" class="semefeito corPadrao" alt="ir para página anterior" title="ir para página anterior"><img src="' . URL_IMG . 'pag_prev.png" border="0" /></a>';
		}
		
		for ($i = 1; $i <= $nPaginas; $i++)
		{
			$escreve = 0;
			
			if ($nPag < 8)
			{
				if ($i <= 15)
				{
					$escreve = 1;
				}
			}
			elseif ($nPag > ($nPaginas - 7))
			{
				if ($i > ($nPaginas - 15))
				{
					$escreve = 1;
				}
			}
			else
			{
				if ($i >= ($nPag - 7) && $i <= ($nPag + 7))
				{
					$escreve = 1;
				}
			}
			
			if ($escreve == 1)
			{
				if (($i-1) * $exibir_pp == $limit_de)
				{
					echo '&nbsp; <a href="' . $urlLink . $i . '" class="semefeito fontred"><b>' . $i . '</b></a>';
				}
				else
				{
					echo '&nbsp; <a href="' . $urlLink . $i . '" class="semefeito corPadrao">' . $i . '</a>';
				}
			}
		}
		
		if ($nPag < $nPaginas)
		{
			echo '&nbsp; <a href="' . $urlLink . ($nPag + 1) . '" class="semefeito corPadrao" alt="ir para próxima página" title="ir para próxima página"><img src="' . URL_IMG . 'pag_next.png" border="0" /></a>';
			echo '<a href="' . $urlLink . $nPaginas . '" class="semefeito corPadrao" alt="ir para última página" title="ir para última página"><img src="' . URL_IMG . 'pag_ult.png" border="0" /></a>';
		}
	}
	else
	{
		for ($i = 1; $i <= $nPaginas; $i++)
		{
			if (($i-1) * $exibir_pp == $limit_de)
			{
				echo '&nbsp; <a href="' . $urlLink . $i . '" class="semefeito fontred"><b>' . $i . '</b></a>';
			}
			else
			{
				echo '&nbsp; <a href="' . $urlLink . $i . '" class="semefeito corPadrao">' . $i . '</a>';
			}
		}
	}
}

/* 
 * montaPaginacaoPesquisa
 *
 * @access	public
 * 
 * @param	nPaginas    = numero total de paginas
 * @param	nPag        = numero da pagina atual
 * @param	limit_de    = a partir de qual registro vai mostrar
 * @param	exibir_pp   = qts registros irá exibir por página
 * @param	urlPesq     = url para link
 * @param   form        = name do form da pesquisa 
 *  
 * @return	string html
 */
function montaPaginacaoPesquisa($nPaginas, $nPag, $limit_de, $exibir_pp, $urlLink, $form)
{
	if ($nPaginas > 15)
	{
		if ($nPag > 1)
		{
            $jsA = "javascript:$('" . $form . "').setAttribute('action', '" . $urlLink . "1'); $('" . $form . "').submit();";
            $jsB = "javascript:$('" . $form . "').setAttribute('action', '" . $urlLink . ($nPag - 1) . "'); $('" . $form . "').submit();";
			echo '&nbsp; <a href="' . $jsA . '" class="semefeito corPadrao" alt="ir para primeira página" title="ir para primeira página"><img src="' . URL_IMG . 'pag_pri.png" border="0" /></a>';
			echo '<a href="' . $jsB . '" class="semefeito corPadrao" alt="ir para página anterior" title="ir para página anterior"><img src="' . URL_IMG . 'pag_prev.png" border="0" /></a>';
		}
		
		for ($i = 1; $i <= $nPaginas; $i++)
		{
			$escreve = 0;
			
			if ($nPag < 8)
			{
				if ($i <= 15)
				{
					$escreve = 1;
				}
			}
			elseif ($nPag > ($nPaginas - 7))
			{
				if ($i > ($nPaginas - 15))
				{
					$escreve = 1;
				}
			}
			else
			{
				if ($i >= ($nPag - 7) && $i <= ($nPag + 7))
				{
					$escreve = 1;
				}
			}
			
			if ($escreve == 1)
			{
                $js = "javascript:$('" . $form . "').setAttribute('action', '" . $urlLink . $i . "'); $('" . $form . "').submit();";
				if (($i-1) * $exibir_pp == $limit_de)
				{
					echo '&nbsp; <a href="' . $js . '" class="semefeito fontred"><b>' . $i . '</b></a>';
				}
				else
				{
					echo '&nbsp; <a href="' . $js . '" class="semefeito corPadrao">' . $i . '</a>';
				}
			}
		}
		
		if ($nPag < $nPaginas)
		{
            $jsA = "javascript:$('" . $form . "').setAttribute('action', '" . $urlLink . ($nPag + 1) . "'); $('" . $form . "').submit();";
            $jsB = "javascript:$('" . $form . "').setAttribute('action', '" . $urlLink . $nPaginas . "'); $('" . $form . "').submit();";
			echo '&nbsp; <a href="' . $jsA . '" class="semefeito corPadrao" alt="ir para próxima página" title="ir para próxima página"><img src="' . URL_IMG . 'pag_next.png" border="0" /></a>';
			echo '<a href="' . $jsB . '" class="semefeito corPadrao" alt="ir para última página" title="ir para última página"><img src="' . URL_IMG . 'pag_ult.png" border="0" /></a>';
		}
	}
	else
	{
		for ($i = 1; $i <= $nPaginas; $i++)
		{
            $js = "javascript:$('" . $form . "').setAttribute('action', '" . $urlLink . $i . "'); $('" . $form . "').submit();";
			if (($i-1) * $exibir_pp == $limit_de)
			{
				echo '&nbsp; <a href="' . $js . '" class="semefeito fontred"><b>' . $i . '</b></a>';
			}
			else
			{
				echo '&nbsp; <a href="' . $js . '" class="semefeito corPadrao">' . $i . '</a>';
			}
		}
	}
}

/* 
 * masc_real
 *
 * @access	public
 * @param	string	valor em formato de numero válido
 * @return	string  valor em formato de reais
 */
function masc_real($valor)
{
	return number_format($valor, 2, ",", ".");
}

/* 
 * removeAcentos
 *
 * @access	public
 * @param	string e boolean que indica se deve retornar maiusculo
 * @return	string
 */
function removeAcentos($str, $upper = false, $enc = "UTF-8")
{
	$acentos = array(
				'A' => '/&Agrave;|&Aacute;|&Acirc;|&Atilde;|&Auml;|&Aring;/',
				'a' => '/&agrave;|&aacute;|&acirc;|&atilde;|&auml;|&aring;/',
				'C' => '/&Ccedil;/',
				'c' => '/&ccedil;/',
				'E' => '/&Egrave;|&Eacute;|&Ecirc;|&Euml;/',
				'e' => '/&egrave;|&eacute;|&ecirc;|&euml;/',
				'I' => '/&Igrave;|&Iacute;|&Icirc;|&Iuml;/',
				'i' => '/&igrave;|&iacute;|&icirc;|&iuml;/',
				'N' => '/&Ntilde;/',
				'n' => '/&ntilde;/',
				'O' => '/&Ograve;|&Oacute;|&Ocirc;|&Otilde;|&Ouml;/',
				'o' => '/&ograve;|&oacute;|&ocirc;|&otilde;|&ouml;/',
				'U' => '/&Ugrave;|&Uacute;|&Ucirc;|&Uuml;/',
				'u' => '/&ugrave;|&uacute;|&ucirc;|&uuml;/',
				'Y' => '/&Yacute;/',
				'y' => '/&yacute;|&yuml;/',
				'a.' => '/&ordf;/',
				'o.' => '/&ordm;/');

	$str = preg_replace($acentos, array_keys($acentos), htmlentities($str, ENT_NOQUOTES, $enc));
	
	if ($upper == TRUE)
	{
		$str = strtoupper($str);
	}
	
	return $str;
}
/* 
 * removeAcentosArquivo
 *
 * @access	public
 * @param	string e boolean que indica se deve retornar maiusculo
 * @return	string
 */
function removeAcentosArquivo($str, $upper, $enc = "UTF-8")
{
	$str = preg_replace("[^a-zA-Z0-9_]", "", strtr($str, "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ°ºª,;.", "aaaaeeiooouucAAAAEEIOOOUUC      "));

	if ($upper == TRUE)
	{
		$str = strtoupper($str);
	}
	
	return $str;
}

/* 
 * add_corLinha
 *
 * @access	public
 * @param	array de arrays / tipo de cor / nLinha (se add numero da linha)
 * @return	arrays com um elemento a mais de índice CORLINHA
 */
function add_corLinha($arr, $tipoCor = 1, $nLinha = FALSE)
{
	$tamArr = count($arr);
	
	switch($tipoCor)
	{
		case 2:
			$lp = 'l_p2';
			$li = 'l_i2';
			break;
		default:
			$lp = 'l_p';
			$li = 'l_i';
			break;
	}
	
	for ($i = 0; $i < $tamArr; $i++)
	{
		if ($nLinha)
		{
			$arr[$i]['NLINHA'] = $i + 1;
		}
		
		$arr[$i]['CORLINHA'] = ($i % 2 == 0 ? $lp : $li);
	}
	
	return $arr;
}

/* 
 * getValor
 *
 * @access	public
 * @param	string	valor em formato de reais
 * @return	string  valor em formato de numero válido
 */
function getValor($valor)
{
	$valorAux = '';
	
	if ($valor != '')
	{
		if (strpos($valor, ','))
		{
			$valorAux = str_replace('.', '',  $valor);
			$valorAux = str_replace(',', '.', $valorAux);
		}
		else
		{
			$valorAux = $valor;
		}
		
		if (is_numeric($valorAux))
		{
			return $valorAux;
		}
		else
		{
			return 0;
		}
	}
	else
	{
		return 0;
	}
}

 /* 
 * br_to_eua
 *
 * @access	public
 * @param	string data em formato brasileiro
 * @return	string data em formato eua (banco)
 */
function br_to_eua($str)
{
	if ($str != '')
	{
		return substr($str,6,4)."/".substr($str,3,2)."/".substr($str,0,2);
	}
	else
	{
		return NULL;
	}
}

 /* 
 * eua_to_br
 *
 * @access	public
 * @param	string data em formato eua (banco)
 * @return	string data em formato brasileiro
 */
function eua_to_br($str)
{
	if ($str != '')
	{
		return substr($str,8,2)."/".substr($str,5,2)."/".substr($str,0,4);
	}
	else
	{
		return NULL;
	}
}

 /* 
 * datediff
 *
 * @access	public
 * @param	string data em formato eua (banco)
 * @param	string data em formato eua (banco)
 * @return	numero de dias de diferenca
 */
function datediff($date_one = null, $date_two = null)
{
	$diff = null;
	if(!is_null($date_one) && !is_null($date_one))
	{
		$date_one = str_replace('/', '-', $date_one);
		$date_two = str_replace('/', '-', $date_two);
		
		$diff = strtotime($date_one) - strtotime($date_two);
		$diff = round($diff / (3600*24));
	}
	return $diff;
}

/**
* add_date()
* Adiciona um intervalo é data encaminhada. Pode-se adicionar dias, meses, anos, 
* horas e segundos dependendo do caracter que for encaminhado. Formato da data encaminhada
* deve estar no formato 'Y-m-d'.
* Caracteres permitidos:
* -> 'd' (Dia) 
* -> 'm' (Mes)
* -> 'w' (semana)
* -> 'Y' (Ano)
* -> 'h' (hora)
* -> 'i' (minutos)
* -> 's' (segundos)
* @param string date
* @param string charInterval
* @param integer valueAdd
* @return string dateAdded
*/
function add_date($date = null, $charInterval = null, $valueAdd = null)
{
	$newDate = $date;
	if(!is_null($charInterval) && !is_null($valueAdd))
	{
		// Transforma data em array de informações sobre
		// data para ser utilizado cada pedaço em separado
		// echo($date);
		$strDate = format_date($date, true);
		$arrDate = getdate(strtotime($date));
		$intHour = $arrDate['hours'];
		$intMins = $arrDate['minutes'];
		$intSecs = $arrDate['seconds'];
		$intMont = $arrDate['mon'];
		$intMDay = $arrDate['mday'];
		$intYear = $arrDate['year'];
		
		// O bloco abaixo adiciona o intervalo à sua respectiva
		// parte na data encaminhada, logo em seguida, converte
		// a data novamente.
		switch(strtolower($charInterval))
		{
			case 'm': $intMont += $valueAdd; break;
			case 'd': $intMDay += $valueAdd; break;
			case 'w': $intMDay += ($valueAdd * 7); break;
			case 'y': $intYear += $valueAdd; break;
			case 'h': $intHour += $valueAdd; break;
			case 'i': $intMins += $valueAdd; break;
			case 's': $intSecs += $valueAdd; break;
		}
		
		// Coleta o formato da data encaminhada, para ser 
		// criado a data com a adição do intevalo encaminhado. 
		// Formatado com base na localidade atual
		$newDate = date('Y-m-d H:i:s', mktime($intHour, $intMins, $intSecs, $intMont, $intMDay, $intYear));
	}
	return $newDate;
}

/* 
* format_date()
* Formata uma data, com default passando para brasil (separador '/') ou se segundo 
* parametro for true, formata para banco de dados (separador '-').
* @param string data
* @param boolean to_database
* @return string new_date
*/
function format_date($str, $to_database = false)
{
	if($to_database)
	{
		if ($str != '')
		{ 
			return substr($str,6,4)."/".substr($str,3,2)."/".substr($str,0,2); 
		} else {
			return '';
		}
	} else {
		if ($str != '')
		{
			return substr($str,8,2)."/".substr($str,5,2)."/".substr($str,0,4);
		} else {
			return '';
		}

	}
}

/* 
 * horadiff
 *
 * @access	public
 * @param	string hora em formato 24h 00:00:00
 * @param	string hora em formato 24h 00:00:00
 * @return	numero de segundos de diferenca
 */
function horadiff($hora_one = null, $hora_two = null)
{
	$diff = null;
	if(!is_null($hora_one) && !is_null($hora_two))
	{
        $hora_one = explode(':', $hora_one);
        $hora_two = explode(':', $hora_two);
        
        $hora_one = (isset($hora_one[1]) && isset($hora_one[2])) ? $hora_one : 0;
        $hora_two = (isset($hora_two[1]) && isset($hora_two[2])) ? $hora_two : 0;
        
        $segundos_one = round(($hora_one[0] * 3600) + ($hora_one[1] * 60) + $hora_one[2]);
        $segundos_two = round(($hora_two[0] * 3600) + ($hora_two[1] * 60) + $hora_two[2]);
        
		$diff = $segundos_two - $segundos_one;
	}
	return $diff;
}

 /* 
 * montaOptionsArray
 *
 * @access	public
 * @param	array   array de dados que serão transformado em options de um select
 *				    o primeiro chave do array vai para nome, e o segundo para o value do option
 * @param	mixed   valor que deve ser comparado para marcar o 'selected'
 * @param	boolean se é para criar uma opção em branco ou não
 * @return	string html de options
 */
function montaOptionsArray($data = null, $value_to_selected = null, $blank_option = true)
{
	$html = '';
	if(!is_null($data) && is_array($data))
	{
		// $data = array_values($data);
		//print_r($data);
		$val_selected = (!is_null($value_to_selected)) ? $value_to_selected : '';
		if($blank_option)
		{
			$html .= '<option value=""';
			$html .= ($val_selected == '') ? ' selected="selected"></option>' : '></option>';
		}
		foreach($data as $row)
		{
			$row = array_values($row);
			$html .= '<option value="' . removeAcentos($row[0], true) . '"';
			$html .= ($val_selected == removeAcentos($row[0], true)) ? ' selected="selected">' : '>';
			$html .= array_key_exists(1, $row) ? removeAcentos($row[1], true) : removeAcentos($row[0], true);
			$html .= '</option>';
		}
	}
	return $html;
}

 /* 
 * monta_options_array()
 * Monta um conjunto de tags html <option> com base em um array encaminhado
 * como param.
 * @param array data
 * @param valor_to_select
 * @param boolean blank_option
 * @return	string html de options
 */
function monta_options_array($data = null, $value_to_selected = null, $blank_option = true)
{
	$html = '';
	if(!is_null($data) && is_array($data))
	{
		// echo($value_to_selected);
		// $data = array_values($data);
		// print_r($data);
		$val_selected = (!is_null($value_to_selected)) ? $value_to_selected : '';
		if($blank_option)
		{
			$html .= '<option value=""';
			$html .= ($val_selected == '') ? ' selected="selected"></option>' : '></option>';
		}
		foreach($data as $row)
		{
			$row = array_values($row);
			$html .= '<option value="' . removeAcentos($row[0], true) . '"';
			$html .= ($val_selected == removeAcentos($row[0], true)) ? ' selected="selected">' : '>';
			$html .= array_key_exists(1, $row) ? removeAcentos($row[1], true) : removeAcentos($row[0], true);
			$html .= '</option>';
		}
	}
	return $html;
}

 /*
 * calcDistanciaPontos
 *
 * @access	public
 * @param	
 * @return	distancia em metros
 */
function calcDistanciaPontos($p1LA, $p1LO, $p2LA, $p2LO)
{
	$r = 6371.0;
	
	$p1LA = $p1LA * pi() / 180.0;
	$p1LO = $p1LO * pi() / 180.0;
	$p2LA = $p2LA * pi() / 180.0;
	$p2LO = $p2LO * pi() / 180.0;
	
	$dLat  = $p2LA - $p1LA;
	$dLong = $p2LO - $p1LO;
	
	$a = sin($dLat / 2) * sin($dLat / 2) + cos($p1LA) * cos($p2LA) * sin($dLong / 2) * sin($dLong / 2);
	$c = 2 * atan2(sqrt($a), sqrt(1 - $a));
	
	return round($r * $c * 1000);
}

 /* 
 * masc_cpf
 *
 * @access	public
 * @param	
 * @return	
 */
function masc_cpf($str)
{
	if ($str != '')
	{
		$p1 = substr($str,0,3);
		$p2 = substr($str,3,3);
		$p3 = substr($str,6,3);
		$p4 = substr($str,9,2);
		
		return $p1 . ($p2 != '' ? '.' . $p2 : '') . ($p3 != '' ? '.' . $p3 : '') . ($p4 != '' ? '-' . $p4 : '');
	}
	else
	{
		return '';
	}
}

 /* 
 * masc_cnpj
 *
 * @access	public
 * @param	
 * @return	
 */
function masc_cnpj($str)
{
	if ($str != '')
	{
		$p1 = substr($str,0,2);
		$p2 = substr($str,2,3);
		$p3 = substr($str,5,3);
		$p4 = substr($str,8,4);
		$p5 = substr($str,12,2);
		
		return $p1 . ($p2 != '' ? '.' . $p2 : '') . ($p3 != '' ? '.' . $p3 : '') . ($p4 != '' ? '/' . $p4 : '') . ($p5 != '' ? '-' . $p5 : '');
	}
	else
	{
		return '';
	}
}

/*função que receberá o idUf e retornará seu respectivo nome */
function func_rel_uf($idUf)
{
	
    return true;
}

/**
* Autor:
* Pablo Costa <pablo@users.sourceforge.net>
*
* Função:
* Calculo do Modulo 11 para geracao do digito verificador
* de boletos bancarios conforme documentos obtidos
* da Febraban - www.febraban.org.br
*
* Entrada:
* $num: string numérica para a qual se deseja calcularo digito verificador;
* $base: valor maximo de multiplicacao [2-$base]
* $r: quando especificado um devolve somente o resto
*
* Saída:
* Retorna o Digito verificador.
*
* Observações:
* - Script desenvolvido sem nenhum reaproveitamento de código pré existente.
* - Assume-se que a verificação do formato das variáveis de entrada é feita antes da execução deste script.
*/

function modulo11($num, $base=9, $r=0) {

	$soma = 0;
	$fator = 2;
	
	/* Separacao dos numeros */
	for ($i = strlen($num); $i > 0; $i--) {
		// pega cada numero isoladamente
		$numeros[$i] = substr($num,$i-1,1);
		
		// Efetua multiplicacao do numero pelo falor
		$parcial[$i] = $numeros[$i] * $fator;
		
		// Soma dos digitos
		$soma += $parcial[$i];
		
		if ($fator == $base) {
			// restaura fator de multiplicacao para 2
			$fator = 1;
		}
		$fator++;
	}
	
	/* Calculo do modulo 11 */
	if ($r == 0) {
		$soma *= 10;
		$digito = $soma % 11;
		
		if ($digito == 10) {
			$digito = 0;
		}
		return $digito;
	} elseif ($r == 1) {
		$resto = $soma % 11;
		return $resto;
	}
}

/**
* get_value()
* Retorna um valor de um array, idependentemente do INDEX existir ou não.
* @param array data
* @param string index
* @return mixed value
*/
function get_value($data = null, $index = null)
{
	$value = '';
	if(!is_null($data) && !is_null($index) && is_array($data))
	{
		// passa os indices do array encaminhado como param para lower case
		$arr_aux = array_change_key_case($data, CASE_LOWER);
		$value = (isset($arr_aux[strtolower($index)])) ? $arr_aux[strtolower($index)] : '';
	}
	return $value;
}

/**
* replace_tag()
* Faz a substituição do valor <NUMERO_COLUNA> em uma string por um valor de 
* um íncide de array.
* @param string value
* @param array data 
* @return string new_string
*/
function replace_tag($value = null, $arr_data = array())
{
	// Casa todas as ocorrencias de {TAG_A-Z0-9a-z_-}
	preg_match_all('/{[0-9a-zA-Z_-]+}/', $value, $matches);	
	
	// Varre todas as ocorrencias, substituindo valor do array por tag
	foreach($matches as $match)
	{	
		$counter = count($match);
		for($i = 0; $i < $counter; $i++)
		{
			// Coleta o indice do array
			$index = str_replace('}', '', str_replace('{','', $match[$i]));
			
			// coleta o indice do array por numero, caso tag == numero
			$data = (valida_integer($index)) ? array_values($arr_data) : $arr_data;
			
			// Faz replace da tag por valor
			$value = str_replace($match[$i], get_value($data, $index), $value);
		}
	}
	return $value;
}

/**
* get_mensagem()
* Exibe mensagem customizada, conforme o tipo iformado.
* @param string tipo
* @param string titulo
* @param string descricao
* @param boolean close
* @param string style
* @return void
*/
function get_mensagem($type = 'info', $title = '', $description = '', $close = false, $style = '')
{
	$id = uniqid();
	$type = ($type != 'info' && $type != 'warning' && $type != 'error' && $type != 'success' && $type != 'note') ? 'info' : $type;
	$html  = '<div id="message_general_' . $id . '">';
	$html .= '<div class="msg_general ' . $type . '" ' . (($style != '') ? 'style="' . $style . '"' : '') . '>';
	$html .= '<div>';
	$html .= ($close) ? '<img src="' . URL_IMG . '/icon_close.png" class="close" title="Fechar" onclick="javascript:$(\'#message_general_' . $id . '\').hide();" />' : '';
	$html .= '<img src="' . URL_IMG . '/icon_' . $type . '.png" />';
	$html .= ($title != '') ? '<h5>' . $title . '</h5>' : '';
	$html .= '<div>' . $description . '</div>';
	$html .= '</div>';
	$html .= '</div>';
	$html .= '</div>';
	return $html;
} 

/**
* mensagem()
* Exibe mensagem customizada, conforme o tipo iformado.
* @param string tipo
* @param string titulo
* @param string descricao
* @param boolean close
* @param string style
* @return void
*/
function mensagem($type = 'info', $title = '', $description = '', $close = false, $style = '')
{
	echo(get_mensagem($type, $title, $description, $close, $style));
} 

/**
* start_box()
* Inicializa o html de uma caixa genérica.
* @param string style
* @return void
*/
function start_box($title = '', $icon = '', $style = '')
{
	$html  = '<div class="generic_box" ' . (($style != '') ? 'style="' . $style . '"' : '') . '>';
	$html .= '<div id="header">';
	$html .= ($icon) ? '<img src="' . URL_IMG . $icon . '" style="width:16px;" />' : '';
	$html .= '<h6>' . $title . '</h6>';
	$html .= '</div>';
	$html .= '<div id="content" style="padding:0;">';
	echo $html;
}

/**
* end_box()
* Finaliza o html de uma caixa genérica.
* @return void
*/
function end_box()
{
	$html  = '</div>';
	$html .= '<div id="footer"></div>';
	$html .= '</div>';
	echo $html;
}

/* 
 * to_moeda()
 * Formata um float para estilo moeda, R$
 * @param double $valor
 * @return string $new_valor
 */
function to_moeda($valor)
{
	return number_format($valor, 2, ",", ".");
}

/* 
 * to_float()
 * Converte de formato em reais para formato flutuante.
 * @param string valor
 * @return double new_valor
 */
function to_float($valor)
{
	$valorAux = '';
	if($valor != '')
	{
		if(strpos($valor, ','))
		{
			$valorAux = str_replace('.', '',  $valor);
			$valorAux = str_replace(',', '.', $valorAux);
		} else {
			$valorAux = $valor;
		}
		if(is_numeric($valorAux))
		{
			return $valorAux;
		} else {
			return 0;
		}
	} else {
		return 0;
	}
}

/**
 * upload_file()
 * A function for easily uploading files. This function will automatically generate a new 
 * file name so that files are not overwritten.
 * Taken From: http://www.bin-co.com/php/scripts/upload_function/
 * Adapted By: Leandro Mangini Antunes
 * @param string $file_id      - The name of the input field contianing the file.
 * @param string $folder       - The folder to which the file should be uploaded to - it must be writable. OPTIONAL
 * @param string $types        - A list of comma(,) seperated extensions that can be uploaded. If it is empty, anything goes [OPTIONAL]
 * @param integer $max_kb_size - Max KB Size of file
 * @return mixed This is somewhat complicated - this function returns an array with two values...
 *               The first element is randomly generated filename to which the file was uploaded to.
 *               The second element is the status - if the upload failed, it will be 'Error : Cannot upload the file 'name.txt'.' or something like that
 */
function upload_file($file_id, $folder = '', $types = '', $max_kb_size = 25600)
{
    if(!$_FILES[$file_id]['name']) return array('','Arquivo não especificado');
	
	// Testa se tamanho de imagem é maior que 200KB
	// limit the size of the file to 200KB
	$label_max_size = round(($max_kb_size / 1024) * 8);
	
	// Temos de converter para KB pois tamanho de imagem vem em bytes
	$file_size = round($_FILES[$file_id]['size']  / 1024);
	
	if($file_size > $label_max_size) 
	{
		$result = "'" . $_FILES[$file_id]['name'] . "' possui tamanho maior que $label_max_size KB.";
        return array('', $result);
    }
	
	// Trata envio de arquivo, verificando se existe 'fila de espera' no banco de dados (tbl upl_upload)
	$CI =& get_instance();
	$CI->load->model('upload_model');
	$CI->load->model('configuracao_model');
	$count_fila = $CI->upload_model->get_count_fila_upload();
	if($count_fila > $CI->configuracao_model->get_sis_config_value('max_fila_upload'))
	{
		$result = "Atenção! A transmissão deste arquivo pode falhar em função do grande fluxo de dados neste momento. Para sua comodidade, sugerimos tentar novamente em alguns minutos.";
        return array('', $result);
	}
	
	// Remove acentos, espaços e etc no nome do arquivo
    // $file_title = $_FILES[$file_id]['name'];
    $file_title = remove_special_chars($_FILES[$file_id]['name'], '_');
	
	// DEBUG
	// echo($file_title);
	// die();
    
	// Get file extension
    $ext_arr = explode(".", basename($file_title));
    $ext = strtolower($ext_arr[count($ext_arr)-1]); // Get the last extension

    // Not really uniqe - but for all practical reasons, it is
    $uniqer = substr(md5(uniqid(rand(),1)),0,5);
    $file_name = $uniqer . '_' . $file_title; // Get Unique Name

    $all_types = explode(",",strtolower($types));
    if($types) 
	{
        if(in_array($ext,$all_types));
        else {
            $result = "'".$_FILES[$file_id]['name']."' não é um arquivo válido."; // Show error if any.
            return array('',$result);
        }
    }

    // Where the file must be uploaded to
    if($folder) $folder .= '/'; // Add a '/' at the end of the folder
    $uploadfile = $folder . $file_name;

	// Start transaction and saves inside of the list that being uploaded
	// $CI->upload_model->db->trans_start();
	// $CI->upload_model->db->trans_complete();
	$CI->load->library('session');
	$id_file = $CI->upload_model->save_in_fila($file_name, $file_size, $CI->session->userdata('idUsuario'));
	
    $result = '';
    // Move the file from the stored location to the new location
    if (!move_uploaded_file($_FILES[$file_id]['tmp_name'], $uploadfile)) {
        $result = "Não foi possível fazer o upload do arquivo '".$_FILES[$file_id]['name']."'"; // Show error if any.
        if(!file_exists($folder)) {
            $result .= " : Diretório não existe.";
        } elseif(!is_writable($folder)) {
            $result .= " : Diretório sem permissão de escrita.";
        } elseif(!is_writable($uploadfile)) {
            $result .= " : Arquivo sem permissão de escrita.";
        }
        $file_name = '';
    } else {
        if(!$_FILES[$file_id]['size']) { // Check if the file is made
            @unlink($uploadfile); // Delete the Empty file
            $file_name = '';
            $result = "Arquivo vazio, por favor utilize um arquivo válido."; // Show the error message
        } else {
            chmod($uploadfile, 0777);// Make it universally writable.
        }
    }
	
	// Remove line from list of uploading files
	$CI->upload_model->remove_from_fila($id_file);
    return array($file_name,$result);
}

/**
 * remove_special_chars()
 * Remove todos os caracteres especiais de um arquivo. Inclui espaço em branco.
 * O segundo parametro informa com qual caracter fazer o replace.
 * @param string string
 * @param string caracter_replace
 * @return string new_string
 */
function remove_special_chars($string = '', $caracter_replace = '')
{
    return preg_replace("/[^a-zA-Z0-9_\.]/", $caracter_replace, $string);
}

/**
* valida_integer()
* Verifica se um valor encaminhado é unicamente inteiro.
* @param int number
* @return boolean Valid
*/
function valida_integer($number = null)
{
	if(!is_null($number))
	{
		if(preg_match("/^-?[0-9]+$/", $number))
		{
			return true;
		}
	}
	return false;
}

/**
* valida_alfa()
* Valida uma sequencia de caracteres, especificando se estes são 
* alfa-numéricos ou não. Aceita espaço.
* @param string word
* @return boolean valid
*/
function valida_alfa($word = null)
{
	if(!is_null($word))
	{
		if(preg_match("/^[a-zA-Z0-9\s]+$/", $word))
		{
			return true;
		}
	}
	return false;
}

/**
* valida_float()
* Valida se um número encaminhado é float em seu formato, ou não.
* @param double number
* @return boolean valid
*/
function valida_float($number = null)
{
	if(!is_null($number))
	{
		if(preg_match("/^-?[0-9]+(\.[0-9]+)?$/", $number))
		{
			return true;
		}
	}
	return false;
}

/**
* valida_currency()
* Valida se um número encaminhado está em formato moeda, com base na localização atual.
* @param double number
* @return boolean valid
*/
function valida_currency($number = null)
{
	if(!is_null($number))
	{
		if(preg_match("/^-?[0-9\\.]+([\\,0-9]+)?$/", $number))
		{
			return true;
		}
	}
	return false;
}

/**
* valida_array_empty_values()
* Valida se todos os elementos de um array estão com valores nulos ou vazios. Retorna booleano.
* @param array data
* @return boolean
*/
function valida_array_empty_values($data = array())
{
	if(count($data) > 0)
	{
		foreach($data as $k => $v)
		{
			if($v != '' && !is_null($v)) { return false; }
		}
	}
	return true;
}

/* End of file funcoes_helper.php */