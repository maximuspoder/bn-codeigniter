<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Helper de Estruturas (estrutura_helper.php)
*
* Arquivo que contém as estruturas básicas e genéricas do sistema.
* 
* @author		Moisés Viana
* @package		application
* @subpackage	application.estrutura_helper
* @since		2012-04-18
*
*/

/**
* monta_header()
* Monta o header do sistema (sem menu), com base se usuario está logado ou não.
* @param integer logado
* @return void
*/
function monta_header($logado = 0)
{
	$html  = '<div id="header" ';
	$html .= ($logado == 0) ? 'class="header_shadow">' : '>';
	
	// Barra do governo
	$html .= '<div id="govbar">';
		$html .= '<a class="cultura" title="Ministério da Cultura" href="http://www.cultura.gov.br/" target="_blank">Ministério da Cultura</a>';
		$html .= '<a class="brasil" title="Brasil - País rico é País sem pobreza" href="http://www.brasil.gov.br/" target="_blank">Brasil</a>';
	$html .= '</div>';
	$html .= '<div id="logo">';
	
	// Logotipo
	$html .= ($logado == 0) ? '<div style="text-align:left;width:970px;margin:5px auto 0 auto;">' : '<div style="text-align:left;margin:5px 0 0 8px;">';
		$html .= '<a href="' . URL_EXEC . 'home"><img src="' . URL_IMG . 'logoHeaderBN.jpg" style="" border="0" /></a>';
		
	if ($logado != 0)
	{
		$CI =& get_instance();
		$CI->load->library('session');
		if ($CI->session->userdata('acessoSis') != FALSE)
		{
			// Linha do nome do usuário e ícone do user
			$html .= '<div style="color:#FFFFFF; float:right;padding:6px 0 10px 0;padding-right:15px;text-align:right;">';
			$html .= '<span><strong>' . $CI->session->userdata('nomeLogin'). ' (' . $CI->session->userdata('descTipoUsuario') . ')</strong></span>';
			$html .= '<img src="' . URL_IMG . 'icon_user.png" style="margin:5px 0 -4px 5px;" /><br />';
			
			// Coleta o nome do edital e 
			$CI->load->library('programa');
			$html .= $CI->programa->get_nome_programa($CI->session->userdata('programa')) . ' - ';
			$html .= '&nbsp;<a href="' . URL_EXEC . 'programa/alterar_edital/" title="Alterar Edital" style="color:#fff;">Alterar</a>';
			$html .= '<img src="' . URL_IMG . $CI->programa->get_icone_programa($CI->session->userdata('programa')) . '" style="margin:5px 0 -4px 5px;width:16px;" /><br />';
			$html .= '</div>';
		}
	}
	$html .= '</div>';
	$html .= '</div>';
	$html .= '</div>';
	
	// Printa html do header
	echo $html;
}

/**
* monta_menu()
* Monta o menu do sistema com base no tipo de usuário.
* @param integer idTipoUsuario
* @return void
*/
function monta_menu($idTipoUsuario)
{
	$CI =& get_instance();
	$CI->load->library('session');
	
    $membro = $CI->session->userdata('idMembro');
    $programa = $CI->session->userdata('programa');
    
	?>
    <div id="divmenu">
	<?php if ($idTipoUsuario == 1) /* adminsitrador */ { ?>
		<ul class="pureCssMenu pureCssMenum">
			<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>home"><img src="<?php echo URL_IMG; ?>icon_home.png" style="display:block;" />Início</a></li>
			<li class="pureCssMenui"><a class="pureCssMenui" href="#"><span>Módulos</span></a>
				<ul class="pureCssMenum">
					<li class="pureCssMenui"><a class="pureCssMenui" href="#"><span>Bibliotecas</span></a>
						<ul class="pureCssMenum">
							<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>biblioteca/search">Bibliotecas</a></li>
							<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>resp_financeiro/search">Responsáveis Financeiros</a></li>
							<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>comite_acervo/search">Comitê de Acervo</a></li>
						</ul>
					</li>
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>editora/search">Editoras</a></li>
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>pdv/search">Pontos de Venda</a></li>
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>distribuidor/search">Distribuidores</a></li>
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>livro/search">Livros</a></li>
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>pedido/search">Pedidos</a></li>
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>usuario/search">Usuários</a></li>
				</ul>
			</li>
			<li class="pureCssMenui"><a class="pureCssMenui" href="#"><span>Pesquisa</span></a>
				<ul class="pureCssMenum">
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>pesquisa/pesquisar_livros">Livros</a></li>
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>acesso_externo/pesquisar_livros">Livros (Externa)</a></li>
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>pesquisa/pesquisar_estabelecimento">Estabelecimentos</a></li>
				</ul>
			</li>
			<li class="pureCssMenui"><a class="pureCssMenui" href="#"><span>Gerenciadores</span></a>
				<ul class="pureCssMenum">
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>cnab/gerenciadorCpb">Gerenciador CPB</a></li>
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>adm/gerenciadorSniic">Gerenciador SNIIC (Antigo)</a></li>
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>adm/gerenciadorSniicIntegracao">Gerenciador SNIIC Integração</a></li>
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>adm/gerenciadorCadbiblioteca">Gerenciador Bibliotecas</a>
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>adm/unificadorcadastros">Unificador de Cadastros</a></li>
				</ul>
			</li>
			<li class="pureCssMenui"><a class="pureCssMenui" href="#"><span>Meus Dados</span></a>
				<ul class="pureCssMenum">
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>usuario/form_update_senha">Alterar Senha</a></li>
				</ul>
			</li>
			<li class="pureCssMenui"><a class="pureCssMenui" href="#"><span>Administração</span></a>
				<ul class="pureCssMenum">
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>relatorio/search">Relatórios</a></li>
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>adm/listaConfiguracao">Configurações</a></li>
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>adm/logsis">Log de Atividades</a></li>
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>adm/painel_financeiro">Painel Financeiro</a></li>
				</ul>
			</li>
			<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>home/sair">Sair</a></li>
		</ul>
	
	
	<?php } elseif ($idTipoUsuario == 2) /* biblioteca */ { ?>
		<ul class="pureCssMenu pureCssMenum">
			<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>home"><img src="<?php echo URL_IMG; ?>icon_home.png" style="display:block;" />Início</a></li>
            <li class="pureCssMenui"><a class="pureCssMenui" href="#"><span>Pesquisa</span></a>
				<ul class="pureCssMenum">
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>pesquisa/pesquisar_estabelecimento">Estabelecimentos</a></li>
					<?php if ($programa == 1) { ?><li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>pesquisa/pesquisarLivros">Livros</a></li><?php } ?>
					<?php if ($programa == 2) { ?><li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>pesquisa/pesquisarLivrosIndicacao">Indicação de Livros</a></li><?php } ?>
				</ul>
			</li>
			<li class="pureCssMenui"><a class="pureCssMenui" href="#"><span>Pedidos</span></a>
				<ul class="pureCssMenum">
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>pedido/lista_pedidos_biblioteca">Meus Pedidos</a></li>
				</ul>
			</li>
			<li class="pureCssMenui"><a class="pureCssMenui" href="#"><span>Dados Cadastrais</span></a>
				<ul class="pureCssMenum">
                    <?php if($membro == 0) { ?>
                        <!--<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>biblioteca/respFinanceiroFormcad/">Responsável Financeiro</a></li>-->
                        <li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>biblioteca/lista_responsaveis_financeiros/">Responsável Financeiro</a></li>
                        <li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>biblioteca/lista_comite_acervo/">Comitê de Acervo</a></li>
                        <!--<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>biblioteca/comite/">Comitê de Acervo</a></li>-->
                        <li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>biblioteca/form_alt/edit">Biblioteca</a></li>
                    <?php } ?>
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>usuario/form_update_senha">Alterar Senha</a></li>
				</ul>
			</li>
			<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>home/sair">Sair</a></li>
		</ul>
		
		
	<?php } elseif ($idTipoUsuario == 3) /* distribuidor */ { ?>
		<ul class="pureCssMenu pureCssMenum">
			<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>home"><img src="<?php echo URL_IMG; ?>icon_home.png" style="display:block;" />Início</a></li>
            <li class="pureCssMenui"><a class="pureCssMenui" href="#"><span>Pesquisa</span></a>
				<ul class="pureCssMenum">
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>pesquisa/pesquisar_estabelecimento">Estabelecimentos</a></li>
				</ul>
			</li>
			<li class="pureCssMenui"><a class="pureCssMenui" href="#"><span>Dados Cadastrais</span></a>
				<ul class="pureCssMenum">
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>distribuidor/form_alt/edit">Alterar Cadastro</a></li>
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>usuario/form_update_senha">Alterar Senha</a></li>
				</ul>
			</li>
			<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>home/sair">Sair</a></li>
		</ul>
	
	
	<?php } elseif ($idTipoUsuario == 4) /* ponto de venda */ { ?>
		<ul class="pureCssMenu pureCssMenum">
			<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>home"><img src="<?php echo URL_IMG; ?>icon_home.png" style="display:block;" />Início</a></li>
            <li class="pureCssMenui"><a class="pureCssMenui" href="#"><span>Pesquisa</span></a>
				<ul class="pureCssMenum">
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>pesquisa/pesquisar_estabelecimento">Estabelecimentos</a></li>
				</ul>
			</li>
            <li class="pureCssMenui"><a class="pureCssMenui" href="#"><span>Pedidos</span></a>
				<ul class="pureCssMenum">
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>pedido/lista_pedidos_pdv">Meus Pedidos</a></li>
				</ul>
			</li>
            <li class="pureCssMenui"><a class="pureCssMenui" href="#"><span>Dados Cadastrais</span></a>
				<ul class="pureCssMenum">
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>pdv/form_alt/edit">Alterar Cadastro</a></li>
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>usuario/form_update_senha">Alterar Senha</a></li>
				</ul>
			</li>
			<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>home/sair">Sair</a></li>
		</ul>
		
		
	<?php } elseif ($idTipoUsuario == 5) /* editora */ { ?>
		<ul class="pureCssMenu pureCssMenum">
			<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>home"><img src="<?php echo URL_IMG; ?>icon_home.png" style="display:block;" />Início</a></li>
            <li class="pureCssMenui"><a class="pureCssMenui" href="#"><span>Pesquisa</span></a>
				<ul class="pureCssMenum">
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>pesquisa/pesquisar_estabelecimento">Estabelecimentos</a></li>
				</ul>
			</li>
			<li class="pureCssMenui"><a class="pureCssMenui" href="#"><span>Livros</span></a>
				<ul class="pureCssMenum">
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>editora/livro_precad">Cadastrar Livro</a></li>
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>editora/livroImportar">Importar Livros</a></li>
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>editora/livros">Gerenciar Livros</a></li>
                    <li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>editora/livroParticipante">Livros Participantes</a></li>
				</ul>
			</li>
			<li class="pureCssMenui"><a class="pureCssMenui" href="#"><span>Dados Cadastrais</span></a>
				<ul class="pureCssMenum">
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>editora/form_alt/edit">Alterar Cadastro</a></li>
					<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>usuario/form_update_senha">Alterar Senha</a></li>
				</ul>
			</li>
			<li class="pureCssMenui"><a class="pureCssMenui" href="<?php echo URL_EXEC; ?>home/sair">Sair</a></li>
		</ul>
	<?php } ?> 
	</div>
	<?php
}

/* Footer */
function monta_footer()
{
	?>
	<div id="footer">
		<a href="http://www.conectait.com.br" target="_blank"><img src="<?php echo URL_IMG; ?>logo_mini1.png" style="margin-top:7px; margin-right:15px;" border="0" /></a>
	</div><!-- #footer -->
    <div id="divLoadingX">
        <div id="divLoadingXCombo">
            <img src="<?php echo URL_IMG; ?>progressbar_green.gif" /> <br /> Aguarde, carregando...
        </div>
    </div>
	<?php
}

/* div para consulta de CEP */
function add_div_CEP()
{
	?>
	<div id="popCep"><div id="popCepCombo"></div></div>
	<?php
}

/* div para ligthbox */
function add_div_ligthbox($width = NULL , $height = NULL)
{
    if($width != NULL && $height != NULL)
    {
        $style = 'width: ' .$width . 'px; height: ' . $height . 'px;';
    }
    else
    {	
        $style = 'width: 600px; height: 220px;';
    }
	?>
    <div id="ligthbox_bg">
        <div id="ligthbox" style="<?php echo $style; ?>">
        <div style="height: 15px; text-align: right;"><a onclick="$('ligthbox_bg').setStyle({display : 'none'});"><img src="<?php echo URL_IMG; ?>close_22.png" title="FECHAR" border="0" alt="FECHAR" style="cursor:pointer;" /></a></div>
        <div id="ligthboxContext" ></div>
        </div>
    </div>
	<?php
}

/* Elementos para JS */
function add_elementos_CONFIG()
{
	$html  = '<input type="hidden" name="URL_JS" id="URL_JS" value="' . URL_JS . '" />';
	$html .= '<input type="hidden" name="URL_IMG" id="URL_IMG" value="' . URL_IMG . '" />';
	$html .= '<input type="hidden" name="URL_EXEC" id="URL_EXEC" value="' . URL_EXEC . '" />';
	echo $html;
}

/**
* box_edital()
* Monta o box com informações do edital, pendências, etc.
* @return void
*/
function box_edital()
{
	// Carrega componentes necessários
	$CI =& get_instance();
	$CI->load->library('session');
	$CI->load->library('programa');
	$CI->load->model('programa_model');
	
	// Coleta os ids necessários para funçoes posteriores
	$idprograma = $CI->session->userdata('programa');
	$idusuario = $CI->session->userdata('idUsuario');
	$tpusuario = $CI->session->userdata('tipoUsuario');
	
	// Seta o icone e o titulo do box
	$title = ($idprograma == 0) ? 'Editais em andamento' : $CI->programa_model->get_nome_programa($idprograma);
	$icone = ($idprograma == 0) ? 'icon_editais_andamento.png' : $CI->programa_model->get_icone_programa($idprograma);
	
	// Inicializa o box do edital
	start_box($title, $icone, 'margin-bottom:30px;');
		// Caso nenhum edital esteja selecionado, exibir quadro com todos os editais em andamento
		if($idprograma == 0)
		{
			$programas = $CI->programa_model->get_programas_ativos_by_usuario($idusuario);
			$i = 0;
			foreach($programas as $programa)
			{
				echo('<div' . ($i > 0 ? ' style="margin-top:15px;">' : '>'));
				echo('<img src="' . URL_IMG . 'icon_bullet_go.png" style="float:left;margin:1px 0 0 0;" /><strong>' . get_value($programa, 'NOME_EDITAL') . '</strong>' . ((get_value($programa, 'IDUSUARIO') != '') ? '<span class="font_10 comment"> (Estou habilitado neste edital)</span>' : '') . '<br />');
				// echo('<a href="javascript:void(0);" style="margin:0 0 0 17px;" onclick="open_modal(\'Texto do Edital\', \'' . '<strong>' . get_value($programa, 'NOME_EDITAL') .'</strong><br />' . get_value($programa, 'TEXTO_EDITAL') . '\', \'' . URL_IMG . get_value($programa, 'ICONE_EDITAL') . '\', 300, 400);">Veja o texto do edital</a><br />');
				// echo('<a href="javascript:void(0);" style="margin:0 0 0 17px;" onclick="iframe_modal(\'Lista de Contemplados\', \'' . URL_EXEC . 'programa/modal_lista_contemplados/' . get_value($programa, 'IDPROGRAMA') . '\', \'' . URL_IMG . get_value($programa, 'ICONE_EDITAL') . '\', 800, 800);">Veja a lista de contemplados</a><br />');
				echo '<div style="margin-top:5px;"><a href="' . URL_EXEC . 'application/files/' . get_value($programa, 'ARQUIVO_EDITAL') . '" target="_blank">Arquivo PDF do Edital</a></div>';
				echo '<div style="margin-top:5px;"><a href="javascript:void(0);" onclick="open_modal(\'Texto do Edital\', \'<strong>' . get_value($programa, 'NOME_EDITAL') . '</strong><br />' . get_value($programa, 'TEXTO_EDITAL') . '\', \'' . URL_IMG . get_value($programa, 'ICONE_EDITAL') . '\', 300, 400);">Descrição do Edital</a></div>';
				echo '<div style="margin-top:5px;"><a href="javascript:void(0);" onclick="iframe_modal(\'Lista de Contemplados\', \'' . URL_EXEC . 'programa/modal_lista_contemplados/' . get_value($programa, 'IDPROGRAMA') . '\', \'' . URL_IMG . get_value($programa, 'ICONE_EDITAL') . '\', 600, 800);">Veja a lista de contemplados</a></div>';
				echo('</div>');
				$i++;
			}
		} else {
			// Coleta id usuario e faz switch, chama programa model que 
			// verifica pendencias para cada um dos tipos de usuarios
			// Neste nível o usuário está com um edital setado na sessão
			switch($tpusuario)
			{
				// Administrador
				case 1: $pendencias = ''; break;
			
				// Biblioteca
				case 2:	
					$pendencias = $CI->programa->get_pendencias_biblioteca($idprograma);
				break;
				
				// Distribuidor
				case 3: 
					$pendencias = $CI->programa->get_pendencias_distribuidor($idprograma);
				break;
				
				// Pdv
				case 4:	
					$pendencias = $CI->programa->get_pendencias_pdv($idprograma);
				break;
				
				// Editora
				case 5: 
					$pendencias = $CI->programa->get_pendencias_editora($idprograma);
				break;
				
				// Responsável financeiro
				case 6: 
					$pendencias = $CI->programa->get_pendencias_biblioteca($idprograma);
				break;
				
				// Comitê de acervo
				case 7: 
					$pendencias = $CI->programa->get_pendencias_biblioteca($idprograma);
				break;
			}
			
			// Coleta dados do programa
			$programa = $CI->programa->get_dados_programa($idprograma);
			
			// Texto e lista de contemplados do edital
			echo('<strong>Informações gerais</strong><br />');
			echo '<div style="margin-top:5px;"><a href="' . URL_EXEC . 'application/files/' . get_value($programa, 'ARQUIVO_EDITAL') . '" target="_blank">Arquivo PDF do Edital</a></div>';
			echo '<div style="margin-top:5px;"><a href="javascript:void(0);" onclick="open_modal(\'Texto do Edital\', \'<strong>' . get_value($programa, 'NOME_EDITAL') . '</strong><br />' . get_value($programa, 'TEXTO_EDITAL') . '\', \'' . URL_IMG . get_value($programa, 'ICONE_EDITAL') . '\', 300, 400);">Descrição do Edital</a></div>';
			echo '<div style="margin-top:5px;"><a href="javascript:void(0);" onclick="iframe_modal(\'Lista de Contemplados\', \'' . URL_EXEC . 'programa/modal_lista_contemplados/' . get_value($programa, 'IDPROGRAMA') . '\', \'' . URL_IMG . get_value($programa, 'ICONE_EDITAL') . '\', 600, 800);">Veja a lista de contemplados</a></div>';
			
			// Quotas e Pedidos
			echo($CI->programa->get_quotas_e_pedidos($idprograma));
			
			// Responsável financeiro
			echo($CI->programa->get_responsavel_financeiro($idprograma));
			
			// Comitê de Acervo
			echo($CI->programa->get_comite($idprograma));
			
			// PDV Parceiro
			echo($CI->programa->get_pdv_parceiro($idprograma));
			
			echo('<div style="height:15px;border-bottom:2px dotted #aaa;margin-bottom:10px;">&nbsp;</div>');
			echo('<div style="margin-bottom:5px;"><strong>Pendências para este edital</strong></div>');
			echo($pendencias);		
		}
		// switch (tipo user)
		// case biblioteca 
		// echo this->prog_model->pendencias_biblioteca(idusuario, idprograma);
		// show.
	end_box();
}

/**
* box_minhas_informacoes()
* Monta o box com informações do edital, pendências, etc.
* @return void
*/
function box_minhas_informacoes()
{
	// Inicializa html do box de minhas informações
	start_box('Minhas informações', 'icon_home.png', 'margin-bottom:20px;');
		$CI =& get_instance();
		$CI->load->library('session');
		$CI->load->model('usuario_model');
		$tp_usuario = $CI->session->userdata('tipoUsuario');
		
		// Faz chaveamento conforme o tipo de usuario, resp. financ. ou comitê
		if($tp_usuario == 2)
		{
			if($CI->session->userdata('idMembro') != 0)
			{
				$id_usuario = $CI->session->userdata('idMembro');
			} else if ($CI->session->userdata('idFinanceiro')){
				$id_usuario = $CI->session->userdata('idFinanceiro');
			} else {
				$id_usuario = $CI->session->userdata('idUsuario');
			}
		} else {
			$id_usuario = $CI->session->userdata('idUsuario');
		}
		
		// Coleta dados do usuario
		$usuario = $CI->usuario_model->get_dados_usuario_home($id_usuario);
		
		echo('<div style="width:70px;display:inline-block !important"><strong>Meu ID: </strong></div>' . get_value($usuario, 'IDUSUARIO') . '<br />');
		echo('<div style="width:70px;display:inline-block !important"><strong>Sou um(a): </strong></div>' . get_value($usuario, 'TIPO_ENTIDADE') . '<br />');
		
		echo('<div class="bold" style="margin-top:15px;">Nome da Entidade</div>');
		echo(get_value($usuario, 'RAZAOSOCIAL'));
		
		echo('<div class="bold" style="margin-top:15px;">CPF / CNPJ</div>');
		echo(get_value($usuario, 'CPF_CNPJ'));
		
		echo('<div class="bold" style="margin-top:15px;">Email do Responsável / Email Geral</div>');
		echo(get_value($usuario, 'EMAILRESP'));
		echo((get_value($usuario, 'EMAILRESP') != get_value($usuario, 'EMAILGERAL')) ? '<br />' . get_value($usuario, 'EMAILGERAL') : '');
		
		echo('<div class="bold" style="margin-top:15px;">Telefone</div>');
		echo(get_value($usuario, 'TELEFONEGERAL'));
	end_box();
}

/* End of file estrutura_helper.php */