<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Helper de Erros (error_helper.php)
*
* Arquivo que contém as estruturas básicas e genéricas para administração de erros do sistema.
* 
* @author		Leandro M. Antunes
* @package		application
* @subpackage	application.error_helper
* @since		2012-06-01
*
*/

/**
* bn_error_handler()
* Handler de erros genéricos do sistema. Cada erro disparado enviará a esta função os dados de
* erros, que por sua vez será encaminhado ao email configurado em config.inc.php, em includes.
* @param integer errno
* @param string errstr
* @param string errfile
* @param string errline
* @return void
*/
function bn_error_handler($errno, $errstr, $errfile, $errline)
{
    // Tratamento de email
	if(!defined('EMAIL_SUPORTE'))
	{
		define('EMAIL_SUPORTE', 'leandro.antunes@conectait.com.br');
	}
	
	// Tratamento de erro
	switch ($errno) {
		// Erro genérico
		case E_USER_ERROR:   $errtype = 'Erro comum'; break;

		// Warnings
		case E_USER_WARNING: $errtype = 'Warning';    break;

		// Notices
		case E_USER_NOTICE:  $errtype = 'Notice';     break;

		// Erro sem tipo
		default: $errtype = 'Erro genérico (não identificado)'; break;
    }
	
	// Envia email
	// Inicializa controller e cabecalho do email
	$CI = get_instance();
	$html_email = '<div style="font-family:Arial, Tahoma, Helvetica;font-size:12px;">';
	$html_email .= '<div style="font-size:18px;font-weight:bold;padding-bottom:15px;border-bottom:1px solid #bbb;width:100%">';
	$html_email .= '<span>Portal do Livro FBN - Erro</span>';
	$html_email .= '<span style="font-size:10px;font-weight:normal;float:right;color:#ccc;">Enviado as ' . date('d/m/Y H:i:s') . '</span>';
	$html_email .= '</div>';
	$html_email .= '<div style="margin-top:15px;">Um erro foi encontrado ao utilizar o portal. Abaixo, as descrições do problema:</div><br />';
	$html_email .= '<strong>Tipo de Erro:</strong> ' . $errtype . '<br />';
	$html_email .= '<strong>Erro Nº:</strong> ' . $errno . '<br />';
	$html_email .= '<strong>Descrição do erro:</strong> ' . $errstr . '<br />';
	$html_email .= '<strong>No arquivo:</strong> ' . $errfile . '<br />';
	$html_email .= '<strong>Na linha do arquivo:</strong> ' . $errline . '<br /><br />';
	$html_email .= '<div style="border-top:1px solid #bbb;padding-top:5px;width:100%;font-size:10px;color:#ccc">' . PHP_VERSION . ' (' . PHP_OS . ')</div>';
	$html_email .= '</div>';
	
	sendEmail('[ERROR] - Portal do Livro FBN', '[ERROR] - Portal do Livro FBN', EMAIL_SUPORTE, $html_email);
	$CI->load->view('error_handler', array('errno' => $errno, 'errstr' => $errno, 'errfile' => $errno, 'errline' => $errno, 'errtype' => $error_handler));

    /* Don't execute PHP internal error handler */
    return true;
}