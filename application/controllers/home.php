<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('session');
		if ($this->session->userdata('acessoSis') == FALSE)
		{
			redirect('/login/');
			exit;
		}
	}
	
	/*************************************************************************
	 *
	 * CONTROLLERS HOME
	 *
	 * index               : chama home de acordo o tipo de usuário
	 * sair                : logoff do sistema
	 * _homeAdm            : monta home do tipo de usuário admin
	 * _homeBiblioteca     : monta home do tipo de usuário biblioteca
	 * _homeDistribuidor   : monta home do tipo de usuário distribuidor
	 * _homePdv            : monta home do tipo de usuário ponto de venda
	 * _homeEditora        : monta home do tipo de usuário editora
	 * form_alt_senha      : formulario de alteração de senha do usuario
	 * habilita            : faz a habilitação do usuario ao edital
	 *
	 *************************************************************************/
	
	function index()
	{
        // print_r($this->session->all_userdata());
		// verifica se o usuário indicou o programa para a sessão
        if($this->session->userdata('programa') != '-1')
        { 
            switch($this->session->userdata('tipoUsuario'))
            {
                case 1:
                    $this->_homeAdm();
                    break;
                case 2:
                    $this->_homeBiblioteca();
                    break;
                case 3:
                    $this->_homeDistribuidor();
                    break;
                case 4:
                    $this->_homePdv();
                    break;
                case 5:
                    $this->_homeEditora();
                    break;
            }
        }
        else
        {
            $this->load->model('usuario_model');
            $arrSession = Array();
            $programas = $this->usuario_model->getProgramasById($this->session->userdata('idUsuario'));
			
            if(count($programas) == 0)
            {
			    // usuario sem habilitação de edital
			    $arrSession['programa']       = 0;
			    $arrSession['nomePrograma']   = 'Nenhum Edital';
			    $this->session->set_userdata($arrSession);
			    redirect('/home/');
            } else {
                $arrDados = Array();
                $arrDados['editais'] = $programas;
				$arrDados['nome_usuario'] = $this->usuario_model->get_nome_usuario($this->session->userdata('idUsuario'));
                $this->parser->parse('programa/selecionar_edital', $arrDados);
            }
        }
        
	}
    
    function selecionaEdital()
	{
       // seta o usuario escolha habilitação de edital para forçar a troca
       $arrSession['programa']       = '-1';
       $arrSession['nomePrograma']   = '';
       $this->session->set_userdata($arrSession);
       redirect('/home/');
	}
    
    function edital($idPrograma = 0)
	{
        if($idPrograma == 0)
        {
            $arrSession = Array();
            $arrSession['programa']       = 0;
            $arrSession['nomePrograma']   = 'Nenhum Edital';
            $this->session->set_userdata($arrSession);
        }
        else
        {
            $this->load->model('global_model');
            $programas = $this->global_model->get_dado('programa', 'DESCPROGRAMA', 'IDPROGRAMA = ' . trim($idPrograma));
            // seta o edital na sessão e direciona pra home
            $arrSession = Array();
            $arrSession['programa']       = trim($idPrograma);
            $arrSession['nomePrograma']   = $programas;
            $this->session->set_userdata($arrSession);
        }
        redirect('/home/');
        
	}
	
	function sair()
	{
		//salva log
		$this->load->library('logsis', Array('ACCESS', 'OK', '', 0, 'Logoff no sistema', ''));
		
		$this->session->sess_destroy();
		
		redirect('/login/');
	}
	
	private function _homeAdm()
	{
		$this->load->model('usuario_model');
		$arguments['nome_usuario'] = $this->usuario_model->get_nome_usuario($this->session->userdata('idUsuario'));
		$this->load->view('adm/home_view', $arguments);
	}
	
	private function _homeBiblioteca()
	{
		$this->load->model('configuracao_model');
		$this->load->model('global_model');
		$this->load->model('biblioteca_model');
		$this->load->model('usuario_model');
		$this->load->model('livro_model');
		$this->load->model('pedido_model');
		
		// Coleta os dados da configuração
		$config8 = $this->configuracao_model->getConfigData('8');
		
		// habilitado
		$habilitado = $this->global_model->get_dado('usuarioprograma', "'S'", 'IDUSUARIO = ' . $this->session->userdata('idUsuario') . ' AND IDPROGRAMA =' . $this->session->userdata('programa'));
		$arrDados = Array();
		
		// Caso em periodo para conferencia de pedidos
		$arrDados['conferencia'] = ($this->configuracao_model->validaConfig(22, date('Y-m-d'))) ? true : false;
		
		// IDCONFIGURACAO 8: Período para habilitação de bibliotecas
        $arrDados['boolPeriodoAceite'] = $this->configuracao_model->validaEditais('8');
		$arrDados['dateDe']  = (count($config8) > 0) ? $config8[0]['DATA_DE'] : '';
        $arrDados['dateAte'] = (count($config8) > 0) ? $config8[0]['DATA_ATE'] : '';        
		$arrDados['habilitado'] = $habilitado;
        $arrDados['NOME_EDITAL'] = $this->global_model->get_dado('programa', 'NOME_EDITAL', ' IDPROGRAMA = ' . $this->session->userdata('programa'));
		
		// Informação para usuários fazerem indicação de livros
		$pode_indicar = ($this->session->userdata('programa') == 2) ? $this->biblioteca_model->pode_fazer_indicacao($this->session->userdata('idUsuario')) : false;
		$arrDados['pode_indicar'] = $pode_indicar;
		
		// XLS Títulos inexigíveis
		$arrDados['livros_inexigiveis'] = $this->livro_model->get_lista_inexigiveis();
		
		// Verifica se existe algum documento na listagem de pedidos marcado para liberação de edição, caso exista exibir aviso de que o usuário deve alterá-lo.
		$arrDados['pedidos_com_docs_liberar_edicao'] = $this->pedido_model->get_pedidos_com_docs_liberar_edicao($this->session->userdata('idUsuario'), $this->session->userdata('tipoUsuario'));
		$arrDados['pedidos_conferencia'] = '';
		if(count($arrDados['pedidos_com_docs_liberar_edicao']) > 0)
		{
			foreach($arrDados['pedidos_com_docs_liberar_edicao'] as $pedido) { $arrDados['pedidos_conferencia'] .= '<br /><a href="' . URL_EXEC . 'pedido/conferencia/' . get_value($pedido, 'IDPEDIDO') . '">Conferência do pedido de ID ' . get_value($pedido, 'IDPEDIDO') . '</a>'; }
		}
		
		// Nome do usuário
		if($this->session->userdata('idMembro') != 0 || $this->session->userdata('idFinanceiro') != 0)
		{
			$arrDados['nome_usuario'] = ($this->session->userdata('idMembro') != 0) ? $this->usuario_model->get_nome_usuario($this->session->userdata('idMembro')) : $this->usuario_model->get_nome_usuario($this->session->userdata('idFinanceiro'));
		} else {
			$arrDados['nome_usuario'] = $this->usuario_model->get_nome_usuario($this->session->userdata('idUsuario'));
		}
		
		$this->load->view('biblioteca/home_view', $arrDados);
	}
	
	private function _homeDistribuidor()
	{
		$this->load->model('configuracao_model');
		$this->load->model('global_model');
		$this->load->model('usuario_model');
		$this->load->model('livro_model');
		
		// Coleta os dados da configuração
		$config = $this->configuracao_model->getConfigData('10');
		
		// habilitado
		$habilitado = $this->global_model->get_dado('usuarioprograma', "'S'", 'IDUSUARIO = ' . $this->session->userdata('idUsuario') . ' AND IDPROGRAMA =' . $this->session->userdata('programa'));
		
		// IDCONFIGURACAO 10: Período para habilitação de distribuidoras
        $arrDados['boolPeriodoAceite'] = $this->configuracao_model->validaEditais('10');
        $arrDados['dateDe']  = (count($config) > 0) ? $config[0]['DATA_DE'] : '';
        $arrDados['dateAte'] = (count($config) > 0) ? $config[0]['DATA_ATE'] : '';
        $arrDados['habilitado'] = $habilitado;
        $arrDados['NOME_EDITAL'] = $this->global_model->get_dado('programa', 'NOME_EDITAL', ' IDPROGRAMA = ' . $this->session->userdata('programa'));
		
		// XLS Títulos inexigíveis
		$arrDados['livros_inexigiveis'] = $this->livro_model->get_lista_inexigiveis();
		
		// Nome do usuário
		$arrDados['nome_usuario'] = $this->usuario_model->get_nome_usuario($this->session->userdata('idUsuario'));
		
		$this->load->view('distribuidor/home_view', $arrDados);
	}
	
	private function _homePdv()
	{
		$this->load->model('configuracao_model');
		$this->load->model('global_model');
		$this->load->model('usuario_model');
		$this->load->model('livro_model');
		$this->load->model('pedido_model');
		
		// Coleta os dados da configuração
		$config = $this->configuracao_model->getConfigData('11');
		
		// Conferencia
		$arrDados['conferencia'] = ($this->configuracao_model->validaConfig(22, date('Y-m-d'))) ? true : false;
		
		// habilitado
		$habilitado = $this->global_model->get_dado('usuarioprograma', "'S'", 'IDUSUARIO = ' . $this->session->userdata('idUsuario') . ' AND IDPROGRAMA =' . $this->session->userdata('programa'));
		
		// IDCONFIGURACAO 11: Período para habilitação de pdv
		$arrDados['boolPeriodoAceite'] = $this->configuracao_model->validaEditais('11');
		$arrDados['dateDe']  = (count($config) > 0) ? $config[0]['DATA_DE'] : '';
        $arrDados['dateAte'] = (count($config) > 0) ? $config[0]['DATA_ATE'] : '';
        $arrDados['habilitado'] = $habilitado;
		$arrDados['NOME_EDITAL'] = $this->global_model->get_dado('programa', 'NOME_EDITAL', ' IDPROGRAMA = ' . $this->session->userdata('programa'));
		
		// XLS Títulos inexigíveis
		$arrDados['livros_inexigiveis'] = $this->livro_model->get_lista_inexigiveis();
		
		// Verifica se existe algum documento na listagem de pedidos marcado para liberação de edição, caso exista exibir aviso de que o usuário deve alterá-lo.
		$arrDados['pedidos_com_docs_liberar_edicao'] = $this->pedido_model->get_pedidos_com_docs_liberar_edicao($this->session->userdata('idUsuario'), $this->session->userdata('tipoUsuario'));
		$arrDados['pedidos_conferencia'] = '';
		if(count($arrDados['pedidos_com_docs_liberar_edicao']) > 0)
		{
			foreach($arrDados['pedidos_com_docs_liberar_edicao'] as $pedido) { $arrDados['pedidos_conferencia'] .= '<br /><a href="' . URL_EXEC . 'pedido/conferencia/' . get_value($pedido, 'IDPEDIDO') . '">Conferência do pedido de ID ' . get_value($pedido, 'IDPEDIDO') . '</a>'; }
		}
		
		// Nome do usuário
		$arrDados['nome_usuario'] = $this->usuario_model->get_nome_usuario($this->session->userdata('idUsuario'));
        
		$this->load->view('pdv/home_view', $arrDados);
	}
	
	private function _homeEditora()
	{
		$this->load->model('configuracao_model');
		$this->load->model('global_model');
		$this->load->model('usuario_model');
		$this->load->model('pedido_model');
		$this->load->model('livro_model');
		
		// Coleta os dados da configuração
		$config = $this->configuracao_model->getConfigData('9');
		
		// Habilitado
		$habilitado = $this->global_model->get_dado('usuarioprograma', "'S'", 'IDUSUARIO = ' . $this->session->userdata('idUsuario') . ' AND IDPROGRAMA =' . $this->session->userdata('programa'));
		
		// IDCONFIGURACAO 9: Período para habilitação de editoras
		$arrDados['boolPeriodoAceite'] = $this->configuracao_model->validaEditais('9');
		$arrDados['dateDe']  = (count($config) > 0) ? $config[0]['DATA_DE'] : '';
        $arrDados['dateAte'] = (count($config) > 0) ? $config[0]['DATA_ATE'] : '';
        $arrDados['habilitado'] = $habilitado;
        $arrDados['NOME_EDITAL'] = $this->global_model->get_dado('programa', 'NOME_EDITAL', ' IDPROGRAMA = ' . $this->session->userdata('programa'));
		$arrDados['idusuario'] = $this->session->userdata('idUsuario');
		
		// XLS Livros adquiridos em pedidos
		$arrDados['livros_geral'] = $this->pedido_model->get_itens_pedido_editora($arrDados['idusuario'], $this->session->userdata('programa'));
		$arrDados['livros_pdv']   = $this->pedido_model->get_itens_pedido_editora_por_pdv($arrDados['idusuario'], $this->session->userdata('programa'));
		
		// XLS Títulos inexigíveis
		$arrDados['livros_inexigiveis'] = $this->livro_model->get_lista_inexigiveis();
		
		// Nome do usuário
		$arrDados['nome_usuario'] = $this->usuario_model->get_nome_usuario($this->session->userdata('idUsuario'));
        
		$this->load->view('editora/home_view', $arrDados);
	}
	
	function habilita($idprograma)
	{
		$this->load->model('global_model');
		$this->load->model('configuracao_model');
		
		$habilitado = $this->global_model->get_dado('usuarioprograma', "'S'", 'IDUSUARIO = ' . $this->session->userdata('idUsuario') . ' AND IDPROGRAMA =' . $idprograma);
		$periodo    = FALSE;
		
		if ($habilitado != 'S')
		{
			switch($this->session->userdata('tipoUsuario'))
			{
				case 2:
					$periodo = $this->configuracao_model->validaConfig('8', date('Y/m/d'), $idprograma);
					break;
				case 3:
					$periodo = $this->configuracao_model->validaConfig('10', date('Y/m/d'), $idprograma);
					break;
				case 4:
					$periodo = $this->configuracao_model->validaConfig('11', date('Y/m/d'), $idprograma);
					break;
				case 5:
					$periodo = $this->configuracao_model->validaConfig('9', date('Y/m/d'), $idprograma);
					break;
			}
			
			if ($periodo)
			{
				$arrDadosIns = Array();
                $arrDadosIns['IDUSUARIO']       = $this->session->userdata('idUsuario');
                $arrDadosIns['IDPROGRAMA']      = $idprograma;

                $result = $this->global_model->insert('usuarioprograma', $arrDadosIns);

				if ($result == TRUE)
				{
					//salva log
					$this->load->library('logsis', Array('INSERT', 'OK', 'usuarioprograma', 0, 'Habilitação no edital', ''));
					
					$arrSucesso['msg']      = 'Habilitação realizada com sucesso!';
					$arrSucesso['link']     = 'home';
					$arrSucesso['txtlink']  = 'Ir para Home';
					$arrSucesso['tipoUser'] = $this->session->userdata('tipoUsuario');
					$this->load->view('sucesso_view', $arrSucesso);
				}
				else
				{
					//salva log
					$this->load->library('logsis', Array('INSERT', 'ERRO', 'usuarioprograma', 0, 'Habilitação no edital', ''));
					
					$arrErro['msg']      = 'Ocorreu um erro na tentativa de habilitar-se no edital.';
					$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
					$this->load->view('erro_view', $arrErro);
				}
			}
			else
			{
				$arrErro['msg']      = 'Habilitação não permitida.';
				$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
				$this->load->view('erro_view', $arrErro);
			}
		}
		else
		{
			$arrErro['msg']      = 'Usuário já habilitado.';
			$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
			$this->load->view('erro_view', $arrErro);
		}
	}
}

/* End of file home.php */
/* Location: ./system/application/controllers/home.php */