<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		
		$this->load->library('session');
		//$this->output->enable_profiler(TRUE);
	}
	
	function index($msg_login = '')
	{
		// Carrega model de programas, para coleta de programas ativos
		$this->load->model('programa_model');
		
		// Coleta todos os programas ativos em andamento
		$args['programas'] = $this->programa_model->get_programas_ativos();
		$args['msg_login'] = $msg_login;
		
		$this->session->sess_destroy();
		$this->load->view('login_view', $args);
	}
	
	function exec($tipo = NULL)
	{
		// Verifica qual tipo de usuário
		$arrDados = Array();
        $arrDados['cadastrarSniic'] = FALSE;
		
		// OLD: Verifica qual tipo de cadastro de usuário
		/*switch($tipo)
		{
			case '1' : $arrDados['titulo'] = "- Cadastro Nacional de Livros de Baixo Preço -"; break;
			case '2' : $arrDados['titulo'] = "- Cadastro Nacional de Bibliotecas -"; $arrDados['cadastrarSniic'] = TRUE; break;
			case '3' : $arrDados['titulo'] = "- Cadastro Nacional de Pontos de Venda -"; break;
			case '4' : $arrDados['titulo'] = "- Cadastro Nacional de Distribuidoras -"; break;
			default :  $arrDados['titulo'] = "- Cadastro Nacional de Bibliotecas -"; break;
		}
		*/
		
		// Valida os campos encaminhados (usuario / senha)
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('userLogin', 'Login', 'trim|required|xss_clean');
		$this->form_validation->set_rules('userSenha', 'Senha', 'trim|required|xss_clean|md5');
		
		// Roda validação
		if ($this->form_validation->run() == false)
		{
			// Destrói a sessão e joga para a area de login novamente, caso validação false
			$this->session->sess_destroy();
			// $this->load->view('login_view', $arrDados);
			$this->index();
		} else {
			// Caso contrário, verifica os dados do usuário
			$this->load->model('usuario_model');
			$this->load->model('global_model');
			
			// Coleta user e senha
			$login = $this->input->post('userLogin');
			$senha = $this->input->post('userSenha');
			
			// Valida usuário
			$dadosLogin = $this->usuario_model->validaLogin($login, $senha);
			if($dadosLogin == false)
			{
				// Usuário ou senhas incorretos
				$this->session->sess_destroy();
				$arrDados['notLogin'] = 'Usuário / senha incorretos.';
				//$this->load->view('login_view', $arrDados);
				$this->index($arrDados['notLogin']);
			} elseif($dadosLogin->ATIVO == 'N') {
                // Usuário não ativo / validado
				$this->session->sess_destroy();
				$arrDados['notLogin'] = 'Usuário não validado. Para validar seu usuário, acesse a URL que foi encaminhada por e-mail após o seu cadastramento ou clique <a href="'. URL_EXEC . 'autocad/renviaemail">aqui</a> para que um novo email de validação seja enviado.';
				//$this->load->view('login_view', $arrDados);
				$this->index($arrDados['notLogin']);
            } elseif($this->usuario_model->isBloqueado($dadosLogin->IDUSUARIO)) {
				// Usuário bloqueado
				$this->session->sess_destroy();
				$motivo = $this->usuario_model->motivoBloqueio($dadosLogin->IDUSUARIO);
				$arrDados['notLogin'] = "Este usuário encontra-se bloqueado para acesso ao sistema.<br />Motivo: $motivo";
				// $this->load->view('login_view', $arrDados);
				$this->index($arrDados['notLogin']);
			} else {
				// Tudo certo! Inicia sessao
				$arrSession = Array();
				$arrSession['acessoSis']       = 1;
				$arrSession['idUsuario']       = $dadosLogin->IDUSUARIO;
				$arrSession['tipoUsuario']     = $dadosLogin->IDUSUARIOTIPO;
				$arrSession['descTipoUsuario'] = $dadosLogin->NOMEUSUARIOTIPO;
				$arrSession['nomeLogin']       = $login;
				$arrSession['idFinanceiro']    = 0;
				$arrSession['idMembro']        = 0;
				
				// Atualizado em: 02/07/2012
				// Novas variáveis de sessão
				$arrSession['grupo_usuario'] = strtoupper($dadosLogin->NOMEUSUARIOTIPO);
				$arrSession['id_usuario']    = $dadosLogin->IDUSUARIO;
				
				// Seta idusuario e tipo
				$idUsuario = $dadosLogin->IDUSUARIO;
				$tipoUser  = $dadosLogin->IDUSUARIOTIPO;
				
				// Altera os tipos de usuario caso seja comite ou financeiro
				if ($dadosLogin->IDUSUARIOTIPO == 6 || $dadosLogin->IDUSUARIOTIPO == 7)
				{
					// Para usuários financeiros
					if ($dadosLogin->IDUSUARIOTIPO == 6)
					{
						$idBiblioteca = $this->global_model->get_dado('cadbibliotecafinan', 'IDBIBLIOTECA', 'IDUSUARIO = ' . $dadosLogin->IDUSUARIO);
						$arrSession['idUsuario']    = $idBiblioteca;
						$arrSession['idFinanceiro'] = $dadosLogin->IDUSUARIO;
					}
					
					// Para usuarios do comite de acervo
					if ($dadosLogin->IDUSUARIOTIPO == 7)
					{
						$idBiblioteca = $this->global_model->get_dado('cadbibliotecacomite', 'IDBIBLIOTECA', 'IDUSUARIO = ' . $dadosLogin->IDUSUARIO);
						$arrSession['idUsuario'] = $idBiblioteca;
						$arrSession['idMembro']  = $dadosLogin->IDUSUARIO;
					}
					
					// Seta o tipo de usuario geral para biblioteca, quando for financeiro ou comite
					$arrSession['tipoUsuario']  = 2;
					$idUsuario = $idBiblioteca;
					$tipoUser  = 2;
				}
				
				// Coleta os dados do usuario
				$dadosCad = $this->usuario_model->getDadosCadastro($idUsuario, $tipoUser);
				$arrSession['nomeEmpresa'] = $dadosCad[0]['RAZAOSOCIAL'];
				
                // Negativo para o programa forçando a seleção.
                $arrSession['programa']  = '-1';
				
				// Seta o array de dados de sessao
				$this->session->set_userdata($arrSession);
				
				// salva log, redirect para home
				$this->load->library('logsis', Array('ACCESS', 'OK', '', 0, 'Login no sistema', ''));
				redirect('/home/');
			}
		}
	}
}

/* End of file login.php */
/* Location: ./system/application/controllers/login.php */