<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe _Exemplo
*
* <DESCRIÇÃO DA CLASSE AQUI> Exemplo de classe controller.
* 
* @author		<AUTOR>
* @package		application
* @subpackage	controllers.<nome_controller>
* @since		<DATA>
*
*/
class _Exemplo extends CI_Controller {
	
	/**
	* __construct()
	* @return void
	*/
	function __construct()
	{
		parent::__construct();
	}
	
	/**
	* metodo_exemplo()
	* <DESCRIÇÃO MÉTODO AQUI> Exemplo de método.
	* @param type name
	* @return type name
	*/
	function metodo_exemplo()
	{
		// Do something here
	}
}
