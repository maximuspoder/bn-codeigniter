<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cidade extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('session');
		
		if ($this->session->userdata('acessoSis') == FALSE)
		{
			redirect('/login/');
			exit;
		}
	}
	
	/*************************************************************************
	 *
	 * CONTROLLERS DE ADMINISTRACAO
	 *
	 * set_cidade_verificada : seta cidade para verificada (s ou n)
	 *
	 *************************************************************************/
	
	function index()
	{
		$arrErro['msg']      = 'Página não existe.';
		$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
		$this->load->view('erro_view', $arrErro);
	}
	
	function nfunc()
	{
		$arrErro['msg']      = 'Esta funcionalidade não está disponível.';
		$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
		$this->load->view('erro_view', $arrErro);
	}
	
	function set_cidade_verificada($idcidade = '', $situacao = '')
	{
		$this->load->model('cidade_model');
		$this->cidade_model->set_cidade_verificada($idcidade, $situacao);
	}
	
	function set_cidade_verificada_sniic($idcidade = '', $situacao = '')
	{
		$this->load->model('cidade_model');
		$this->cidade_model->set_cidade_verificada_sniic($idcidade, $situacao);
	}
	
	/**
	* ajax_get_options_cidades_by_uf()
	* Retorna html de options de cidades com base numa uf encaminhada.
	* @param string iduf
	* @return string options
	*/
	function ajax_get_options_cidades_by_uf($uf)
	{
		$this->load->model('cidade_model');
		echo monta_options_array($this->cidade_model->get_cidades_by_uf($uf));
	}
}
