<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe _Exemplo
*
* <DESCRI��O DA CLASSE AQUI> Exemplo de classe controller.
* 
* @author		Fernando Alves
* @package		application
* @subpackage	controllers.endere�o
* @since		2012-07-12
*
*/
class Endereco extends CI_Controller {
	
	/**
	* __construct()
	* @return void
	*/
	
	function __construct()
	{
		parent::__construct();
	}
	
	/**
	* search()
	* M�todo inicial para a entidade endereco
	* @param $actual_page
	*/
	function search($actual_page = 0)
	{
		// Carrega a camada model.
		$this->load->model('distribuidor_model');
		$this->load->model('endereco_model');
		
		// Coleta o formul�rio de filtros e array de dados da tabela de logradouros
		$args['filtros'] = $this->input->post();
		
		
		// Inicializa o filtro sem valores.
		if
		(
			get_value($args['filtros'], 'c__iduf') == '' && 
			get_value($args['filtros'], 'b__nomebairro') == '' && 
			get_value($args['filtros'], 'l__nomelogradouro') == '' && 
			get_value($args['filtros'], 'l__cep') == ''
		){
			$data = array();
		} else {
			$data = $this->endereco_model->get_lista_modulo($args['filtros']);
		}
		
		// Carrega a biblioteca para o grid
		$this->load->library('array_table');

		$this->array_table->set_id('module_table');
		$this->array_table->set_data($data);
		$this->array_table->set_actual_page($actual_page);
		$this->array_table->set_items_per_page(8);
		$this->array_table->set_page_link('endereco/search');
		$this->array_table->add_column('<input type="radio" name="valores" value="{0}|{1}|{2}|{3}|{4}|{5}" >');
		$this->array_table->add_column('<a href="' . URL_EXEC . 'endereco/form_new_endereco/{0}/"><img src="' . URL_IMG . 'building_add.png" title="Adicionar Novo Logradouro" /></a>');
		$this->array_table->set_columns(array('#', 'CEP', 'ESTADO', 'CIDADE', 'BAIRRO', 'LOGRADOURO'));

		// Processa tabela do modulo
		$args['module_table'] = $this->array_table->get_html();		
		// Monta options de ufs
		$args['options_uf'] = monta_options_array($this->endereco_model->get_ufs(), get_value($args['filtros'], 'c__iduf'));
		$args['options_cidades'] = monta_options_array($this->endereco_model->get_cidades_by_uf(get_value($args['filtros'], 'c__iduf')), get_value($args['filtros'], 'c__idcidade'));
		// Carrega a camada de visualiza��o.
		$this->load->view('endereco/search', $args);
	}
	
	/**
	* form_new_endereco()
	* M�todo para cadastrar novo logradouro
	* @param $actual_page
	*/
	function form_new_endereco($idlogradouro)
	{
		// Carrega a camada model
		$this->load->model('endereco_model');
		
		$args['dados'] = $this->endereco_model->get_logradouro_nomes($idlogradouro);
		$args['cidade'] = $args['dados'][0]['CIDADE'];
		$args['estado'] = $args['dados'][0]['ESTADO'];
		$args['cep'] = $args['dados'][0]['CEP'];
		$args['bairro'] = $args['dados'][0]['BAIRRO'];
		$args['idlogradouro'] = $idlogradouro;
		
		$args['options_logradourotipo'] = monta_options_array($this->endereco_model->get_logradourotipo());
		
		$this->load->view('endereco/form_new_endereco', $args);

		
		// Carrega a camada de visualiza��o
		
	}
	/**
	* form_new_endereco_process()
	* M�todo para cadastrar novo logradouro
	* @param $actual_page
	*/	
	function form_new_endereco_process()
	{		
		$args['dados_formulario'] = $this->input->post();
		
		// Carrega a camada model
		$this->load->model('endereco_model');
		// Carrega a camada model global_model. 
		$this->load->model('global_model');
		// Pega os ids de cidade, bairro, cep.
		$args['dados'] = $this->endereco_model->get_ids_logradouro($this->input->post('idlogradouro'));

		$arr_dados['idcidade'] = $args['dados'][0]['IDCIDADE'];
		$arr_dados['idbairro'] = $args['dados'][0]['IDBAIRRO'];
		$arr_dados['idlogradourotipo'] = $this->input->post('idlogradourotipo');
		$arr_dados['cep'] = $args['dados'][0]['CEP'];
		$arr_dados['nomelogradouro'] = strtoupper($this->input->post('nomelogradouro'));
		$arr_dados['datahora_add'] = date('Y-m-d H:i:s');

		$num_rows = $this->global_model->insert('logradouro', $arr_dados);
		if($num_rows > 0)
		{
			// Retorna o �ltimo Idlogradouro.
			$args['dados_logradouro_lastid'] = $this->endereco_model->get_last_idlogradouro();
			// carrega a camada de visualiza��o.
			$this->load->view('endereco/form_new_endereco_sucess', $args);
		}		
	}
	
	
	/**
	* ajax_get_options_bairro_by_cidade()
	* M�todo para buscar bairros das cidades
	* @param $actual_page
	*/	
	function ajax_get_options_bairro_by_cidade($cidade)
	{
		$this->load->model('endereco_model');
		echo monta_options_array($this->endereco_model->get_bairro_by_cidade($cidade));	
	}
}
