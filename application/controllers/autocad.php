<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Autocad extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}
	
	function index()
	{
		$arrErro['msg']      = 'Página não existe.';
		$arrErro['tipoUser'] = 0;
		$this->load->view('erro_view', $arrErro);
	}
	
	/*************************************************************************
	 *
	 * CONTROLLERS DE AUTOCAD
	 *
	 * atualiza              : especifico bibliotecas
	 * preautocad            : seleção de tipo de cadastro e validação se já existe o CPF/CNPJ
	 * biblioteca            : bibliotecas fazem seu proprio cadastro
	 * pdv                   : pontos de venda fazem seu proprio cadastro
	 * editora               : editoras fazem seu proprio cadastro
	 * distribuidor          : distribuidores fazem seu proprio cadastro
	 * _geraChave            : gera chave de ativação
	 * valida                : valida chave
	 * solicitaSenha         : solicitacao de nova senha
	 * listaCidades          : exibe cidades de uma UF para selecionar regiao de atendimento
	 * listaEditoras         : exibe editoras para selecionar editoras que trabalha
	 *
	 *************************************************************************/
	
	function atualiza($chave = 0, $acao = '')
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('usuario_model');
		$this->load->model('sniic_model');
		$this->load->model('global_model');
		$this->load->model('configuracao_model');
		
		$arrDados = Array();
		$verifica = 0;
		
		if ($acao == 'verifica')
		{
			$verifica = 1;
			
			$this->form_validation->set_rules('chave', 'Chave', 'trim|required|max_length[13]|xss_clean');
			
			if ($this->form_validation->run() == TRUE)
			{
				$arrSniic = $this->sniic_model->getSniicByChave(strtoupper($this->input->post('chave')));
				
				if (count($arrSniic) == 1)
				{
					//valida se registro baseado no sniic já nao foi cadastrado
					$where       = sprintf("IDSNIIC = %s", $arrSniic[0]['ID_BIBLIOTECA']);
					$sniicExiste = $this->global_model->get_dado('cadbiblioteca', '1', $where);
					
					if ($sniicExiste == 1)
					{
						$arrDados['outrosErros'] = 'Cadastro já atualizado. Acesse o sistema com os dados que lhe foram enviados por e-mail.';
						$acao = '';
					}
					else
					{
						//tratar CPF/CNPJ
						$arrRep  = Array('.','-','/',' ');
						$ccAux   = str_replace($arrRep, '', $arrSniic[0]['CPF_CNPJ']);
						$tipoAux = 'PJ';
						
						if (strlen($ccAux) <= 11)
						{
							$ccAux   = masc_cpf($ccAux);
							$tipoAux = 'PF';
						}
						else
						{
							$ccAux   = masc_cnpj($ccAux);
							$tipoAux = 'PJ';
						}
						
						$urlCadastro = 'autocad/biblioteca/new/' . $tipoAux . '/' . str_replace('/', '_', $ccAux) . '/' . $arrSniic[0]['ID_BIBLIOTECA'];
						redirect($urlCadastro);
					}
				}
				else
				{
					$arrDados['outrosErros'] = 'Chave inválida.';
					$acao = '';
				}
			}
			else
			{
				$acao = '';
			}
		}
		
		//informações zeradas para nao dar erro no parse
		$arrDados['chave'] = ($chave != '0' ? $chave : '');
		
		//periodos de cadastro porm tipo de estabelecimento
		$per_biblioteca = $this->configuracao_model->validaConfig('1', date('Y/m/d'));
		
		if ($acao == '')
		{
            if( ! $per_biblioteca)
            {
                $arrErro['msg']      = 'Cadastro não disponível no momento.';
				$arrErro['tipoUser'] = 0;
				$this->load->view('erro_view', $arrErro);
            }
            else
            {
                $this->parser->parse('atualiza_view', $arrDados);
            }
		}
	}
	
	function preautocad($acao = '', $aux = '')
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('usuario_model');
		$this->load->model('global_model');
		$this->load->model('configuracao_model');
		
		$arrDados   = Array();
		$verifica   = 0;
		$tipopessoa = '';
		
		if ($acao == 'verifica')
		{
			$verifica   = 1;
			$tipopessoa = $this->input->post('tipopessoa');
			
			$this->form_validation->set_rules('tipocadastro', 'Tipo Cadastro', 'required|xss_clean');
			$this->form_validation->set_rules('tipopessoa',   'Tipo Pessoa',   'required|xss_clean');
			$this->form_validation->set_rules('cnpjcpf',      'CNPJ/CPF',      'trim|required|max_length[18]|valida_cnpj_cpf|xss_clean');
			
			if ($this->form_validation->run() == TRUE)
			{
                $urlCadastro = '';

                switch($this->input->post('tipocadastro'))
                {
                    case 2:
                    	$this->load->model('sniic_model');
						$arrRep        = Array('.','-','/','_');
						$where_cnpjcpf = str_replace($arrRep, '',  $this->input->post('cnpjcpf'));
						
                    	$sniicCad = $this->sniic_model->getSniicCadastradoByCpfCnpj($where_cnpjcpf);
                    	
                    	$urlCadastro = count($sniicCad) == 0 ? 'autocad/biblioteca/new/' . $this->input->post('tipopessoa') . '/' . str_replace('/','_',$this->input->post('cnpjcpf'))
                    										 : 'autocad/biblioteca/add/' . $this->input->post('tipopessoa') . '/' . str_replace('/','_',$this->input->post('cnpjcpf'));
                        break;
                    case 3:
                        $urlCadastro = 'autocad/distribuidor/new/' . $this->input->post('tipopessoa') . '/' . str_replace('/','_',$this->input->post('cnpjcpf'));
                        break;
                    case 4:
                        $urlCadastro = 'autocad/pdv/new/' . $this->input->post('tipopessoa') . '/' . str_replace('/','_',$this->input->post('cnpjcpf'));
                        break;
                    case 5:
                        $urlCadastro = 'autocad/editora/new/' . $this->input->post('tipopessoa') . '/' . str_replace('/','_',$this->input->post('cnpjcpf'));
                        break;
                }

                $arrUsers = $this->usuario_model->listaUsuariosPreCad($this->input->post('cnpjcpf'));
                $tamArr   = count($arrUsers);
				
                if ($tamArr > 0)
                {
                    for ($i = 0; $i < $tamArr; $i++)
                    {
                        $dadosCad = $this->usuario_model->getDadosCadastro($arrUsers[$i]['IDUSUARIO'], $arrUsers[$i]['IDUSUARIOTIPO']);
						
                        $arrUsers[$i]['RAZAOSOCIAL'] = $dadosCad[0]['RAZAOSOCIAL'];
                    }
					
                    $arrDados2 = Array();
                    $arrDados2['usuarios']    = $arrUsers;
                    $arrDados2['urlCadastro'] = $urlCadastro;
                    $arrDados2['tipopessoa']  = $this->input->post('tipopessoa');
                    $arrDados2['cpfcnpj']     = $this->input->post('cnpjcpf');
					
                    $this->parser->parse('preautocad_listausers_view', $arrDados2);
                }
                else
                {
                    redirect($urlCadastro);
                }
			}
			else
			{
				$acao = '';
			}
		}
		
		//informações zeradas para nao dar erro no parse
		$arrDados['tipopessoa_aux'] = $tipopessoa;
		$arrDados['cnpjcpf']        = '';
		$arrDados['verifica']       = $verifica;
		
		//periodos de cadastro porm tipo de estabelecimento
		$arrDados['per_biblioteca'] = $this->configuracao_model->validaConfig('1', date('Y/m/d'));
		$arrDados['per_editora']    = $this->configuracao_model->validaConfig('2', date('Y/m/d'));
		$arrDados['per_dist']       = $this->configuracao_model->validaConfig('3', date('Y/m/d'));
		$arrDados['per_pdv']        = $this->configuracao_model->validaConfig('4', date('Y/m/d'));
		
		if ($acao == '' && $aux == '')
		{
            if( ! $arrDados['per_biblioteca'] && ! $arrDados['per_editora'] && ! $arrDados['per_dist'] && ! $arrDados['per_pdv'])
            {
                $arrErro['msg']      = 'Nenhum tipo de cadastro disponível no momento.';
				$arrErro['tipoUser'] = 0;
				$this->load->view('erro_view', $arrErro);
            }
            else
            {
                $this->parser->parse('preautocad_form_view', $arrDados);
            }
		}
        
        // cadastro para biblioteca
        if($acao == 'cnb' || $aux == 'cnb')
        {
            if( ! $arrDados['per_biblioteca'])
            {
                $arrErro['msg']      = 'O cadastro de biblioteca está indisponível no momento.';
				$arrErro['tipoUser'] = 0;
				$this->load->view('erro_view', $arrErro);
            }
            else
            {
                $this->parser->parse('biblioteca/preautocad_form_view',$arrDados);
            }
        }
        
	}
	
	function biblioteca($acao = '', $auxTP = 'PJ', $auxCpjCnpj = '', $idSniic = NULL, $migrar = 0, $nPag = 0)
	{		
		$this->load->library('session');
		$this->load->model('configuracao_model');
		$this->load->model('usuario_model');

		if($acao == 'save')	{
			if ($nPag == 0) $nPag = $this->input->post('nPag');
		}
		
		if ($this->configuracao_model->validaConfig('1', date('Y/m/d')))
		{
			$this->load->helper('form');
			$this->load->library('form_validation');
			$this->load->model('global_model');
			$this->load->model('biblioteca_model');
			$this->load->model('naturezajur_model');
            $this->load->model('sniic_model');
            
            $sniic         = Array();
            $sniicCadastro = Array();
            
            //Validar se CPF/CNPJ existe na SNIIC para biblioteca
            $arrRep        = Array('.','-','/','_');
            $where_cnpjcpf = str_replace($arrRep, '',  $auxCpjCnpj);
            
            if ($acao == 'save')
            {
				$where_idsniic = $this->input->post('idsniic');
				$sniic = $this->sniic_model->getSniicByIdSniic($where_idsniic);
            }
            else if($acao == 'add')
            {
                // cadastrar bibliotecas novas que tem cnpj/cpf inclusos na lista do sniic
                $acao = 'new';
            }
            else
            {
                if($idSniic == NULL)
                {
                    $sniic = $this->sniic_model->getSniicByCpfCnpj($where_cnpjcpf);
                }
                else
                {
                    $sniic = $this->sniic_model->getSniicByIdSniic($idSniic);
                }
            }
            
            //lista de bibliotecas já cadastradas para o CPF_CNPJ
            $sniicCad = $this->sniic_model->getSniicCadastradoByCpfCnpj($where_cnpjcpf);
            
            // monta lista das bibliotecas existentes para esse CPF_CNPJ
            foreach ($sniicCad as $key => $value) 
            {
                array_push($sniicCadastro, $value['NOME_BIBLIOTECA']);
            }            
            if(count($sniic) > 1)
            {
                $arrDados2 = Array();
                $arrDados2['usuarios'] = $sniic;				
                $arrDados2['migrar'] = $migrar;
                
                // monta lista de bibliotecas para esse CPF_CNPJ
                foreach ($sniic as $key => $value)
                {
                    if(count($sniicCadastro) > 0 && in_array($value['NOME'],$sniicCadastro))
                    {
                        $arrDados2['usuarios'][$key]['urlcadastro']   = "alert('A biblioteca " . $value['NOME'] . " já está cadastrada.')";
                        $arrDados2['usuarios'][$key]['cor']           = '#FF0000';
                    }
                    else
                    {
                    	
                        $arrDados2['usuarios'][$key]['urlcadastro']   = "window.location.href='" . URL_EXEC . 'autocad/biblioteca/new/' . $auxTP . '/' . $auxCpjCnpj . '/' . $value['ID_BIBLIOTECA'] . "'";
                        $arrDados2['usuarios'][$key]['cor']           = '#000000'; 
                    }
                }
				
                $arrDados2['cadoutro']   = "window.location.href='" . URL_EXEC . 'autocad/biblioteca/add/' . $auxTP . '/' . $auxCpjCnpj . "'";
                
                $this->parser->parse('biblioteca/preautocad_listausers_view', $arrDados2);
            }
            elseif(count($sniic) == 1 || count($sniic) == 0)
            {
                $nomeAux = (count($sniic) == 1) ? $sniic[0]['NOME'] : '';
				
                if(count($sniicCad) == 0 || ! in_array($nomeAux, $sniicCadastro))
                {
                    $arrDados   = Array();
                    $tipopessoa = $auxTP;
                    $cnpjcpf    = str_replace('_','/',$auxCpjCnpj);
					
                    if ($acao == 'save' && isset($_POST))
                    {
						$arrRespQuest = Array();
						
						foreach($_POST as $key => $value)
						{
							if(stristr($key, 'qx'))
							{
								$value = ( ! isset($value)) ? 0 : $value;
								$this->form_validation->set_rules($key, 'Questionário', 'xss_clean');
								
								array_push($arrRespQuest, $key . '|_|' . $value);
							}
						}
						
						$respQuest = (count($arrRespQuest) > 0 ? implode('#_#', $arrRespQuest) : '');
						
                        $tipopessoa = $this->input->post('tipopessoa');
                        $labelAux   = ($tipopessoa == 'PJ' ? 'Razão Social' : 'Nome');
						
                        $this->form_validation->set_rules('tipopessoa',      'Tipo Pessoa',             'required');
                        $this->form_validation->set_rules('cnpjcpf',         'CNPJ/CPF',                'trim|required|max_length[18]|valida_cnpj_cpf|xss_clean');
                        $this->form_validation->set_rules('iestadual',       'Inscrição Estadual',      'trim|max_length[20]|xss_clean');
                        $this->form_validation->set_rules('razaosocial',     $labelAux,                 'trim|required|max_length[100]|xss_clean');
                        $this->form_validation->set_rules('nomefantasia',    'Nome Fantasia',           'trim|max_length[100]|xss_clean');
                        $this->form_validation->set_rules('naturezajur',     'Natureza Jurídica',       'valida_combo|xss_clean');
                        $this->form_validation->set_rules('telefone1',       'Telefone Geral',          'trim|required|max_length[15]|xss_clean');
                        $this->form_validation->set_rules('email',           'E-mail Geral',            'trim|required|max_length[100]|valid_email');
                        $this->form_validation->set_rules('site',            'Site',                    'trim|max_length[100]|xss_clean');
                        $this->form_validation->set_rules('login',           'Login',                   'trim|required|min_length[6]|max_length[20]|alpha_numeric|xss_clean');
                        $this->form_validation->set_rules('idlogradouro',    'Endereço',                'is_numeric');
                        $this->form_validation->set_rules('cep',             '',                        '');
                        $this->form_validation->set_rules('uf',              '',                        '');
                        $this->form_validation->set_rules('cidade',          '',                        '');
                        $this->form_validation->set_rules('bairro',          '',                        '');
                        $this->form_validation->set_rules('logradouro',      '',                        '');
                        $this->form_validation->set_rules('logcomplemento',  '',                        '');
                        $this->form_validation->set_rules('end_numero',      'Endereço - Número',       'trim|required|max_length[8]|xss_clean');
                        $this->form_validation->set_rules('end_complemento', 'Complemento',             'trim|max_length[30]|xss_clean');
                        $this->form_validation->set_rules('nomeresp',        'Nome Responsável',        'trim|required|max_length[100]|xss_clean');
                        $this->form_validation->set_rules('telefoneresp',    'Telefone do Responsável', 'trim|required|max_length[15]|xss_clean');
                        $this->form_validation->set_rules('skyperesp',       'Skype do Responsável',    'trim|max_length[30]|xss_clean');
                        $this->form_validation->set_rules('emailresp',       'E-mail do Responsável',   'trim|required|max_length[100]|matches[emailresp2]|valid_email');
                        $this->form_validation->set_rules('emailresp2',      'E-mail do Responsável',   '');
                        $this->form_validation->set_rules('aceite',          'Atesto que as informações prestadas...', 'required|xss_clean');

                        if ($this->form_validation->run() == TRUE)
                        {
                        	$bib = $this->input->post('idbiblioteca');
                        	
                        	if ($bib != "") {

                        		$where = sprintf("IDUSUARIO = %s", $bib);

                        		$this->db->trans_start();
                        			                                
	                            $arrDadosUpd = Array();
	                            $arrDadosUpd['IDLOGRADOURO']    = ($this->input->post('idlogradouro') != '' ? $this->input->post('idlogradouro') : NULL);
	                            $arrDadosUpd['IDNATUREZAJUR']   = $this->input->post('naturezajur');
	                            $arrDadosUpd['IESTADUAL']       = $this->input->post('iestadual');
	                            $arrDadosUpd['RAZAOSOCIAL']     = removeAcentos($this->input->post('razaosocial'), TRUE);
	                            $arrDadosUpd['NOMEFANTASIA']    = removeAcentos($this->input->post('nomefantasia'), TRUE);
	                            $arrDadosUpd['TELEFONEGERAL']   = $this->input->post('telefone1');
	                            $arrDadosUpd['EMAILGERAL']      = $this->input->post('email');
	                            $arrDadosUpd['SITE']            = $this->input->post('site');
	                            $arrDadosUpd['NOMERESP']        = removeAcentos($this->input->post('nomeresp'), TRUE);
	                            $arrDadosUpd['TELEFONERESP']    = $this->input->post('telefoneresp');
	                            $arrDadosUpd['EMAILRESP']       = $this->input->post('emailresp');
	                            $arrDadosUpd['SKYPERESP']       = $this->input->post('skyperesp');
	                            $arrDadosUpd['END_NUMERO']      = $this->input->post('end_numero');
	                            $arrDadosUpd['END_COMPLEMENTO'] = removeAcentos($this->input->post('end_complemento'), TRUE);
	                            //$arrDadosUpd['TERMOACEITE']     = 'S';
	                            //$arrDadosUpd['LATITUDE']        = NULL;
	                            //$arrDadosUpd['LONGITUDE']       = NULL;
	                            $arrDadosUpd['DATA_UPD']        = date('Y/m/d H:i:s');
	                            $arrDadosUpd['IDSNIIC']         = (count($sniic) == 1) ? $sniic[0]['ID_BIBLIOTECA'] : 0;

	                            $this->global_model->update('cadbiblioteca', $arrDadosUpd, $where);

	                            if ($this->input->post('habilitado') == 'S')
	                            {
	                            	$arrDadosUsr = Array();
	                            	$arrDadosUsr['HABILITADO']  = 'S';
	                            	$this->global_model->update('usuario', $arrDadosUsr, $where);
	                            }
                        		$arrCredSniic = $this->sniic_model->getCreditoByIdBiblioteca($sniic[0]['ID_BIBLIOTECA']);
								
								if (count($arrCredSniic) > 0)
								{
									$arrCred = array();
									$arrCredInfo = array();
									
									foreach ($arrCredSniic as $cs){

										// verifica se existe crédito para a biblioteca receptora no edital correspondente
										$vlrCredBib = $this->biblioteca_model->getCreditoAtualPrograma($bib, $cs['IDPROGRAMA']);

										if ($vlrCredBib > 0)
										{

											if ($vlrCredBib < $cs['VLRCREDITO'])
											{
												// se o crédito existente na biblioteca for menor do que o do sniic,
												// transfere do sniic, se for maior, não faz nada
												$arrCred['VLRCREDITO']			= $cs['VLRCREDITO'];
												$arrCred['VLRCREDITO_INICIAL']	= $cs['VLRCREDITO_INICIAL'];
												$arrCred['DATA_CREDITO']		= $cs['DATA_CREDITO'];
												
												$this->global_model->update('credito', $arrCred, $where);
												
												$ci = $this->sniic_model->getCreditoInfoByIdBiblioteca($sniic[0]['ID_BIBLIOTECA'], $cs['IDPROGRAMA']);
												
												if (count($ci) > 0) {
													$arrCredInfo['VLRCRITERIOPROPORCIONAL']			= $ci[0]['VLRCRITERIOPROPORCIONAL'];
													$arrCredInfo['VLRCRITERIOPOPULACIONAL']			= $ci[0]['VLRCRITERIOPOPULACIONAL'];
													$arrCredInfo['VLRCRITERIOQUALIFICADOR']			= $ci[0]['VLRCRITERIOQUALIFICADOR'];
													$arrCredInfo['VLRCRITERIOPOPULACIONALSOBRA']	= $ci[0]['VLRCRITERIOPOPULACIONALSOBRA'];
													$arrCredInfo['VLRCRITERIOKIT']					= $ci[0]['VLRCRITERIOKIT'];
													$arrCredInfo['VLRCRITERIOKITSOBRA']				= $ci[0]['VLRCRITERIOKITSOBRA'];
													$arrCredInfo['PONTUACAO']						= $ci[0]['PONTUACAO'];

													$this->global_model->update('creditoinfo', $arrCredInfo, $where);
												}

											}
										}
										else
										{
											$arrCred['IDPROGRAMA']			= $cs['IDPROGRAMA'];
											$arrCred['IDUSUARIO']			= $bib;
											$arrCred['VLRCREDITO']			= $cs['VLRCREDITO'];
											$arrCred['VLRCREDITO_INICIAL']	= $cs['VLRCREDITO_INICIAL'];
											$arrCred['DATA_CREDITO']		= $cs['DATA_CREDITO'];
											
											$this->global_model->insert('credito', $arrCred);
											
											$ci = $this->sniic_model->getCreditoInfoByIdBiblioteca($sniic[0]['ID_BIBLIOTECA'], $cs['IDPROGRAMA']);
											
											if (count($ci) > 0) {
												$arrCredInfo['IDPROGRAMA']						= $ci[0]['IDPROGRAMA'];
												$arrCredInfo['IDUSUARIO']						= $bib;
												$arrCredInfo['VLRCRITERIOPROPORCIONAL']			= $ci[0]['VLRCRITERIOPROPORCIONAL'];
												$arrCredInfo['VLRCRITERIOPOPULACIONAL']			= $ci[0]['VLRCRITERIOPOPULACIONAL'];
												$arrCredInfo['VLRCRITERIOQUALIFICADOR']			= $ci[0]['VLRCRITERIOQUALIFICADOR'];
												$arrCredInfo['VLRCRITERIOPOPULACIONALSOBRA']	= $ci[0]['VLRCRITERIOPOPULACIONALSOBRA'];
												$arrCredInfo['VLRCRITERIOKIT']					= $ci[0]['VLRCRITERIOKIT'];
												$arrCredInfo['VLRCRITERIOKITSOBRA']				= $ci[0]['VLRCRITERIOKITSOBRA'];
												$arrCredInfo['PONTUACAO']						= $ci[0]['PONTUACAO'];

												$this->global_model->insert('creditoinfo', $arrCredInfo);
											}
										}
									}
								}

								$arrLog = Array();
								$arrLog['IDUSUARIO']      = $this->session->userdata('idUsuario');
								$arrLog['IDBIBLIOTECA']   = $sniic[0]['ID_BIBLIOTECA'];
								$arrLog['ORIGEM']         = "SNIIC";
								$arrLog['TIPOLOG']        = "MIGRAÇÃO";
								$arrLog['MOTIVO']   	  = $this->input->post('observacao');
												
								$this->global_model->insert('log_gerenciamento', $arrLog);
								
								$this->db->trans_complete();		
								
								$arrSucesso['msg']			= 'Cadastro realizado com sucesso!';
	                            $arrSucesso['link']			= 'login';
	                            $arrSucesso['txtlink']		= 'Ir para Página Inicial';
	                            $arrSucesso['tipoUser']		= 0;
								$arrSucesso['migrar']		= $migrar;
								$arrSucesso['nPag']			= $nPag;
	                            $arrSucesso['msgMigrar']	= 'Associação realizada com sucesso!';
	                            $this->load->view('sucesso_view', $arrSucesso);
	                            	                            
                        		// teste
                        	} else {
								//valida se login já existe
	                            $where       = sprintf("LOGIN = '%s'", strtolower($this->input->post('login')));
	                            $loginExiste = $this->global_model->get_dado('usuario', '1', $where);
								
	                            if(count($sniic) == 0)
	                            {
	                                //valida se registro baseado na razão social e cnpj/cpf já nao foi cadastrado
	                                $where = sprintf("cb.RAZAOSOCIAL = '%s' 
	                                                  AND REPLACE(REPLACE(REPLACE(u.CPF_CNPJ, '.', ''), '-', ''), '/', '') ='%s'", 
	                                                    removeAcentos($this->input->post('razaosocial'), TRUE),
	                                                    str_replace($arrRep, '',  $this->input->post('cnpjcpf')));
	                                
	                                $sniicExiste = $this->global_model->get_dado_join('cadbiblioteca cb', '1', 'usuario u ON (u.IDUSUARIO = cb.IDUSUARIO)', $where);
	                            }
	                            else
	                            {
	                                //valida se registro baseado no sniic já nao foi cadastrado
	                                $where       = sprintf("IDSNIIC = %s", $sniic[0]['ID_BIBLIOTECA']);
	                                $sniicExiste = $this->global_model->get_dado('cadbiblioteca', '1', $where);
	                            }
	                            
	                            if ($sniicExiste == 1)
	                            {
	                                $arrDados['outrosErros'] = 'Biblioteca já cadastrada.';
	                                $acao = '';
	                            }
								elseif ($loginExiste == 1)
	                            {
	                                $arrDados['outrosErros'] = 'Esse nome de Login já está sendo usado. Informe outro.';
	                                $acao = '';
	                            }
	                            elseif ($tipopessoa != 'PJ' && $tipopessoa != 'PF')
	                            {
	                                $arrDados['outrosErros'] = 'Tipo pessoa inválido.';
	                                $acao = '';
	                            }
	                            elseif ($this->input->post('idlogradouro') == '')
	                            {
	                                $arrDados['outrosErros'] = 'Informe o endereço.';
	                                $acao = '';
	                            }
	                            else
	                            {
	                                $chaveAtiv = $this->_geraChave($this->input->post('cnpjcpf'));
	                                $senha     = substr($chaveAtiv, 0, 6);
	
	                                $this->db->trans_start();
	
	                                $arrDadosIns = Array();
	                                $arrDadosIns['IDUSUARIOTIPO']   = 2; //biblioteca
	                                $arrDadosIns['TIPOPESSOA']      = $this->input->post('tipopessoa');
	                                $arrDadosIns['CPF_CNPJ']        = $this->input->post('cnpjcpf');
	                                $arrDadosIns['LOGIN']           = strtolower($this->input->post('login'));
	                                $arrDadosIns['SENHA']           = md5($senha);
	                                $arrDadosIns['CHAVE_ATIVACAO']  = $chaveAtiv;
	                                $arrDadosIns['ATIVO']           = 'N';
									
	                                $this->global_model->insert('usuario', $arrDadosIns);
	                                $idNewUser = $this->global_model->get_insert_id();
									
	                                $arrDadosIns = Array();
	                                $arrDadosIns['IDUSUARIO']       = $idNewUser;
	                                $arrDadosIns['IDLOGRADOURO']    = ($this->input->post('idlogradouro') != '' ? $this->input->post('idlogradouro') : NULL);
	                                $arrDadosIns['IDNATUREZAJUR']   = $this->input->post('naturezajur');
	                                $arrDadosIns['IESTADUAL']       = $this->input->post('iestadual');
	                                $arrDadosIns['RAZAOSOCIAL']     = removeAcentos($this->input->post('razaosocial'), TRUE);
	                                $arrDadosIns['NOMEFANTASIA']    = removeAcentos($this->input->post('nomefantasia'), TRUE);
	                                $arrDadosIns['TELEFONEGERAL']   = $this->input->post('telefone1');
	                                $arrDadosIns['EMAILGERAL']      = $this->input->post('email');
	                                $arrDadosIns['SITE']            = $this->input->post('site');
	                                $arrDadosIns['NOMERESP']        = removeAcentos($this->input->post('nomeresp'), TRUE);
	                                $arrDadosIns['TELEFONERESP']    = $this->input->post('telefoneresp');
	                                $arrDadosIns['EMAILRESP']       = $this->input->post('emailresp');
	                                $arrDadosIns['SKYPERESP']       = $this->input->post('skyperesp');
	                                $arrDadosIns['END_NUMERO']      = $this->input->post('end_numero');
	                                $arrDadosIns['END_COMPLEMENTO'] = removeAcentos($this->input->post('end_complemento'), TRUE);
	                                $arrDadosIns['TERMOACEITE']     = 'S';
	                                $arrDadosIns['LATITUDE']        = NULL;
	                                $arrDadosIns['LONGITUDE']       = NULL;
	                                $arrDadosIns['DATA_UPD']        = date('Y/m/d H:i:s');
	                                $arrDadosIns['IDSNIIC']         = (count($sniic) == 1) ? $sniic[0]['ID_BIBLIOTECA'] : 0;
	                                
	                                if(count($sniic) == 1)
									{
	                                    $arrDadosIns['QUESTIONARIO'] = '';
	                                }
	                                else 
	                                {
	                                    $arrDadosIns['QUESTIONARIO'] = $respQuest;
	                                }
									
									$this->global_model->insert('cadbiblioteca', $arrDadosIns);
									$idUsuario = $this->usuario_model->getLastId();
									
									if(count($sniic) == 1)
	                                {
	                                	// verifica se tem crédito no sniic. Se tiver, transfere.
	                                	$arrCredSniic = $this->sniic_model->getCreditoByIdBiblioteca($sniic[0]['ID_BIBLIOTECA']);
								
										if (count($arrCredSniic) > 0)
										{
											$where = sprintf("ID_BIBLIOTECA = %d", $sniic[0]['ID_BIBLIOTECA']);
											
											$arrCred		= array();
											$arrUpdCred		= array();
											$arrCredInfo	= array();
											
											foreach ($arrCredSniic as $cs){
		
												$arrCred['IDPROGRAMA']			= $cs['IDPROGRAMA'];
												$arrCred['IDUSUARIO']			= $idUsuario;
												$arrCred['VLRCREDITO']			= $cs['VLRCREDITO'];
												$arrCred['VLRCREDITO_INICIAL']	= $cs['VLRCREDITO_INICIAL'];
												$arrCred['DATA_CREDITO']		= $cs['DATA_CREDITO'];
													
												$this->global_model->insert('credito', $arrCred);
												
												$ci = $this->sniic_model->getCreditoInfoByIdBiblioteca($sniic[0]['ID_BIBLIOTECA'], $cs['IDPROGRAMA']);
												
												if (count($ci) > 0) {
													$arrCredInfo['IDPROGRAMA']						= $ci[0]['IDPROGRAMA'];
													$arrCredInfo['IDUSUARIO']						= $idUsuario;
													$arrCredInfo['VLRCRITERIOPROPORCIONAL']			= $ci[0]['VLRCRITERIOPROPORCIONAL'];
													$arrCredInfo['VLRCRITERIOPOPULACIONAL']			= $ci[0]['VLRCRITERIOPOPULACIONAL'];
													$arrCredInfo['VLRCRITERIOQUALIFICADOR']			= $ci[0]['VLRCRITERIOQUALIFICADOR'];
													$arrCredInfo['VLRCRITERIOPOPULACIONALSOBRA']	= $ci[0]['VLRCRITERIOPOPULACIONALSOBRA'];
													$arrCredInfo['VLRCRITERIOKIT']					= $ci[0]['VLRCRITERIOKIT'];
													$arrCredInfo['VLRCRITERIOKITSOBRA']				= $ci[0]['VLRCRITERIOKITSOBRA'];
													$arrCredInfo['PONTUACAO']						= $ci[0]['PONTUACAO'];
			
													$this->global_model->insert('creditoinfo', $arrCredInfo);
												}
											}
											
											$arrUpdCred['VLRCREDITO']			= 0;
											$arrUpdCred['VLRCREDITO_INICIAL']	= 0;
												
											$this->global_model->update('sniiconline.credito', $arrUpdCred, $where);
										}
	                                	// insere dados da para não ter bibliotecas duplicadas
	                                    $arrDadosAuxBib =  Array();
	                                    $arrDadosAuxBib['CPF_CNPJ']        = $sniic[0]['CPF_CNPJ'];
	                                    $arrDadosAuxBib['NOME_BIBLIOTECA'] = $sniic[0]['NOME'];
	
	                                    $this->global_model->insert('aux_biblioteca', $arrDadosAuxBib);
	                                }
	                                
	                                $this->db->trans_complete();
	                                
	                                if ($this->db->trans_status() === FALSE)
	                                {
	                                    $arrErro['msg']      = 'Ocorreu um erro na tentativa de salvar o cadastro.';
	                                    $arrErro['tipoUser'] = 0;
	                                    $this->load->view('erro_view', $arrErro);
	                                }
	                                else
	                                {
	                                    //geocodificar endereço
	                                    $arrEnd      = $this->biblioteca_model->getDadosCadastro($idNewUser);
	                                    $address     = str_replace(' ','+',$arrEnd[0]['NOMELOGRADOURO2']) . ',+' . str_replace(' ','+',$arrEnd[0]['END_NUMERO']) . ',+' . str_replace(' ','+',$arrEnd[0]['NOMEBAIRRO']) . ',+' . str_replace(' ','+',$arrEnd[0]['NOMECIDADESUB']) . '+' . $arrEnd[0]['IDUF'];
	                                    $request_url = MAPS_HOST . "&address=" . $address;
	
	                                    $arqXml = simplexml_load_file($request_url);
	
	                                    if ($arqXml)
	                                    {
	                                        $status = $arqXml->status;
	
	                                        if ($status == 'OK')
	                                        {
	                                            $dadosLatLong = $arqXml->xpath('/GeocodeResponse/result[1]/geometry/location');
	
	                                            $arrGeoCod = Array();
	                                            $arrGeoCod['LATITUDE']  = $dadosLatLong[0]->lat;
	                                            $arrGeoCod['LONGITUDE'] = $dadosLatLong[0]->lng;
	
	                                            $this->global_model->update('cadbiblioteca', $arrGeoCod, 'IDUSUARIO = ' . $idNewUser);
	                                        }
	                                    }
	                                    
	                                    //enviar e-mail para ativação do cadastro
	                                    $this->load->library('email');
	
	                                    /* INICIO ENVIO LOCALHOST -> deixar comentado em produção */
	                                    $config = Array();
	                                    $config['protocol']     = 'smtp';
	                                    $config['smtp_host']    = '10.10.1.1';
	                                    $config['smtp_port']    = '25';
	                                    $config['smtp_timeout'] = '30';
	                                    $config['smtp_user']    = EMAIL_FROM;
	                                    $config['smtp_pass']    = EMAIL_PASSWORD;
	                                    $config['newline']      = "\r\n";
	
	                                    $config2 = Array();
	                                    $config2['protocol']     = 'smtp';
	                                    $config2['smtp_host']    = 'ssl://smtp.googlemail.com';
	                                    $config2['smtp_port']    = '465';
	                                    $config2['smtp_timeout'] = '30';
	                                    $config2['smtp_user']    = 'moisesviana@gmail.com';
	                                    $config2['smtp_pass']    = '';
	                                    $config2['newline']      = "\r\n";
	
	                                    $this->email->initialize($config);
	                                    /* FIM ENVIO LOCALHOST */
	
	                                    $msgEmail = "A Fundação Biblioteca Nacional agradece o seu cadastramento, seus dados de acesso são: \n\nLogin: " . $this->input->post('login') . " \nSenha: " . $senha . " \n\nATENÇÃO!\nATENÇÃO!\n\n Para conseguir acessar o sistema é necessário que seja feita a validação do seu cadastro. Valide seu cadastro acessando o endereço: " . URL_EXEC . "autocad/valida/" . $chaveAtiv . " \n\nAtenciosamente, \nBiblioteca Nacional";
	
	                                    $this->email->from(EMAIL_FROM, EMAIL_FROM_NAME);
	                                    $this->email->to($this->input->post('emailresp'));
	                                    $this->email->subject('Validação de Cadastro');
	                                    $this->email->message($msgEmail);
	
	                                    if ($this->email->send())
	                                    {
	                                        //Dados para a gravação de Log
	                                        if($migrar != 0)
											{
												$arrLog = Array();
												$arrLog['IDUSUARIO']      = $this->session->userdata('idUsuario');
												$arrLog['IDBIBLIOTECA']   = $idUsuario;
												$arrLog['ORIGEM']         = "SNIIC";
												$arrLog['TIPOLOG']        = "MIGRAÇÃO";
												$arrLog['MOTIVO']   	  = $this->input->post('observacao');
												
												$this->global_model->insert('log_gerenciamento', $arrLog);
											}
											
											$arrSucesso['msg']      = 'Cadastro realizado com sucesso!<br /> Você em breve receberá um e-mail para validação do seu cadastro. Foi enviado para o e-mail do responsável: ' . $this->input->post('emailresp') . '.';
	                                        $arrSucesso['link']     = 'login';
	                                        $arrSucesso['txtlink']  = 'Ir para Página Inicial';
	                                        $arrSucesso['tipoUser'] = 0;
											$arrSucesso['migrar'] = $migrar;
											$arrSucesso['nPag'] = $nPag;
	                                        $arrSucesso['msgMigrar']      = 'Migração realizada com sucesso!<br /> Você em breve receberá um e-mail para validação do seu cadastro. Foi enviado para o e-mail do responsável: ' . $this->input->post('emailresp') . '.';
	                                        $this->load->view('sucesso_view', $arrSucesso);
	                                    }
	                                    else
	                                    {
	                                        $arrErro['msg']      = 'Seu cadastro foi salvo com sucesso, porém não conseguimos enviar o e-mail de validação. <br />Por favor, entre em contato com a Biblioteca Nacional para a ativação do seu cadastro.';
	                                        $arrErro['tipoUser'] = 0;
	                                        $this->load->view('erro_view', $arrErro);
	
	                                        //echo $this->email->print_debugger();
	                                    }
	                                }
	                            }
                        	}
                        }
                        else
                        {
                            $acao = '';
                        }
                    }
					
					$arrDados['fromSniic'] = (count($sniic) == 0 ? 0 : 1);

                    if (count($sniic) == 0 || $acao == 'save')
                    {
                        if(count($sniic) == 0)
                        {
                            $sniic[0]['ID_BIBLIOTECA'] = '';
                        }
                        
                        $sniic[0]['NOME']            = ''; 
                        $sniic[0]['TELEFONE']        = ''; 
                        $sniic[0]['EMAIL']           = ''; 
                        $sniic[0]['SITE']            = '';
                        $sniic[0]['CEP']             = '';
                        $sniic[0]['NOME_DIRIGENTE']  = '';
                        $sniic[0]['EMAIL_DIRIGENTE'] = '';
                        
                    }
                    
                    $arrMunicipiosCadbib = array();

                    if (isset($migrar) && $migrar == 1) {
                    	$arrMunicipiosCadbib = $this->biblioteca_model->getBibliotecasByCidade(removeAcentos($sniic[0]['MUNICIPIO'], true), true);
                    }
                    
                    //informações zeradas para nao dar erro no parse
                    
                    $arrDados['options_cadbib']  = count($arrMunicipiosCadbib)==0?"":montaOptionsArray($arrMunicipiosCadbib);
                    $arrDados['tipopessoa_aux']  = $tipopessoa;
                    $arrDados['cnpjcpf']         = $cnpjcpf;
                    $arrDados['iestadual']       = '';
                    $arrDados['razaosocial']     = $sniic[0]['NOME'];
                    $arrDados['nomefantasia']    = '';
                    $arrDados['telefone1']       = $sniic[0]['TELEFONE'];
                    $arrDados['email']           = $sniic[0]['EMAIL'];
                    $arrDados['site']            = $sniic[0]['SITE'];
                    $arrDados['login']           = '';
                    $arrDados['idlogradouro']    = '';
                    $arrDados['cep']             = str_replace(' ','',$sniic[0]['CEP']);
                    $arrDados['uf']              = '';
                    $arrDados['cidade']          = '';
                    $arrDados['bairro']          = '';
                    $arrDados['logradouro']      = '';
                    $arrDados['logcomplemento']  = '';
                    $arrDados['end_numero']      = '';
                    $arrDados['end_complemento'] = '';
                    $arrDados['nomeresp']        = $sniic[0]['NOME_DIRIGENTE'];
                    $arrDados['telefoneresp']    = '';
                    $arrDados['skyperesp']       = '';
                    $arrDados['emailresp']       = $sniic[0]['EMAIL_DIRIGENTE'];
                    $arrDados['emailresp2']      = '';
                    $arrDados['idsniic']         = $sniic[0]['ID_BIBLIOTECA'];
                    $arrDados['migrar']          = $migrar;
                    
                    if (isset($nPag)) $arrDados['nPag'] = $nPag;
					
                    if ($acao == '' || $acao == 'new')
                    {
                        $arrDados['natjur'] = $this->naturezajur_model->getCombo('');
						
                        $this->parser->parse('biblioteca/autocad_form_view', $arrDados);
                    }
                }
                else
                {
                    $arrErro['msg'] = 'Esta biblioteca já está cadastrada.';
                    $arrErro['tipoUser'] = 0;
                    $this->load->view('erro_view', $arrErro);
                }
            }
            else
            {
                $arrErro['msg']      = 'Cadastro não permitido. Cadastre-se primeiramente no SNIIC.';
                $arrErro['tipoUser'] = 0;
                $this->load->view('erro_view', $arrErro);
            }
		}
		else
		{
			$arrErro['msg']      = 'Cadastro não permitido.';
			$arrErro['tipoUser'] = 0;
			$this->load->view('erro_view', $arrErro);
		}
	}
	
	function pdv($acao = '', $auxTP = 'PJ', $auxCpjCnpj = '')
	{
		$this->load->model('configuracao_model');
		
		if ($this->configuracao_model->validaConfig('4', date('Y/m/d')))
		{
			$this->load->helper('form');
			$this->load->library('form_validation');
			$this->load->library('usuario_class');
			$this->load->model('global_model');
			$this->load->model('pdv_model');
			$this->load->model('naturezajur_model');
			
			$arrDados   = Array();
			$tipopessoa = $auxTP;
			$cnpjcpf    = str_replace('_','/',$auxCpjCnpj);
			
			if ($acao == 'save')
			{
				$tipopessoa = $this->input->post('tipopessoa');
				$labelAux   = ($tipopessoa == 'PJ' ? 'Razão Social' : 'Nome');
				
				$this->form_validation->set_rules('tipopessoa',      'Tipo Pessoa',                          'required');
				$this->form_validation->set_rules('cnpjcpf',         'CNPJ/CPF',                             'trim|required|max_length[18]|valida_cnpj_cpf|xss_clean');
				$this->form_validation->set_rules('iestadual',       'Inscrição Estadual',                   'trim|max_length[20]|xss_clean');
				$this->form_validation->set_rules('razaosocial',     $labelAux,                              'trim|required|max_length[100]|xss_clean');
				$this->form_validation->set_rules('nomefantasia',    'Nome Fantasia',                        'trim|max_length[100]|xss_clean');
				$this->form_validation->set_rules('naturezajur',     'Natureza Jurídica',                    'valida_combo|xss_clean');
				$this->form_validation->set_rules('telefone1',       'Telefone Geral',                       'trim|required|max_length[15]|xss_clean');
				$this->form_validation->set_rules('email',           'E-mail Geral',                         'trim|required|max_length[100]|valid_email');
				$this->form_validation->set_rules('site',            'Site',                                 'trim|max_length[100]|xss_clean');
				$this->form_validation->set_rules('login',           'Login',                                'trim|required|min_length[6]|max_length[20]|alpha_numeric|xss_clean');
				$this->form_validation->set_rules('idlogradouro',    'Endereço',                             'is_numeric');
				$this->form_validation->set_rules('cep',             '',                                     '');
				$this->form_validation->set_rules('uf',              '',                                     '');
				$this->form_validation->set_rules('cidade',          '',                                     '');
				$this->form_validation->set_rules('bairro',          '',                                     '');
				$this->form_validation->set_rules('logradouro',      '',                                     '');
				$this->form_validation->set_rules('logcomplemento',  '',                                     '');
				$this->form_validation->set_rules('end_numero',      'Endereço - Número',                    'trim|required|max_length[8]|xss_clean');
				$this->form_validation->set_rules('end_complemento', 'Complemento',                          'trim|max_length[30]|xss_clean');
				$this->form_validation->set_rules('area_total',      'Área total de vendas',                 'trim|max_length[10]|xss_clean');
				$this->form_validation->set_rules('area_livros',     'Área de vendas para livros',           'trim|max_length[10]|xss_clean');
				$this->form_validation->set_rules('assoc_ent_sind',  'Associado a entidade sindical',        'trim|max_length[100]|xss_clean');
				$this->form_validation->set_rules('fil_ent_assoc',   'Filiada a entidade(s) associativa(s)', 'trim|max_length[100]|xss_clean');
				$this->form_validation->set_rules('banco',           'Banco',                                'trim|max_length[20]|xss_clean');
				$this->form_validation->set_rules('agencia',         'Agência',                              'trim|max_length[20]|xss_clean');
				$this->form_validation->set_rules('conta',           'Conta',                                'trim|max_length[20]|xss_clean');
				$this->form_validation->set_rules('nomeresp',        'Nome Responsável',                     'trim|required|max_length[100]|xss_clean');
				$this->form_validation->set_rules('telefoneresp',    'Telefone do Responsável',              'trim|required|max_length[15]|xss_clean');
				$this->form_validation->set_rules('skyperesp',       'Skype do Responsável',                 'trim|max_length[30]|xss_clean');
				$this->form_validation->set_rules('emailresp',       'E-mail do Responsável',                'trim|required|max_length[100]|matches[emailresp2]|valid_email');
				$this->form_validation->set_rules('emailresp2',      'E-mail do Responsável',                '');
				//$this->form_validation->set_rules('aceite',          'Aceite dos termos do programa',        'required|xss_clean');
				$this->form_validation->set_rules('socios',          'Sócios',                               'trim|valida_socios|xss_clean');
				
				if ($this->form_validation->run() == TRUE)
				{
					$where = sprintf("LOGIN = '%s'", strtolower($this->input->post('login')));
					$loginExiste = $this->global_model->get_dado('usuario', '1', $where);
					
					if ($loginExiste == 1)
					{
						$arrDados['outrosErros'] = 'Esse nome de Login já está sendo usado. Informe outro.';
						$acao = '';
					}
					elseif ($tipopessoa != 'PJ' && $tipopessoa != 'PF')
					{
						$arrDados['outrosErros'] = 'Tipo pessoa inválido.';
						$acao = '';
					}
					elseif ($this->input->post('idlogradouro') == '')
					{
						$arrDados['outrosErros'] = 'Informe o endereço.';
						$acao = '';
					}
					else
					{
						$chaveAtiv = $this->_geraChave($this->input->post('cnpjcpf'));
						$senha     = substr($chaveAtiv, 0, 6);
						
						$this->db->trans_start();
						
						$arrDadosIns = Array();
						$arrDadosIns['IDUSUARIOTIPO']   = 4; //PDV
						$arrDadosIns['TIPOPESSOA']      = $this->input->post('tipopessoa');
						$arrDadosIns['CPF_CNPJ']        = $this->input->post('cnpjcpf');
						$arrDadosIns['LOGIN']           = strtolower($this->input->post('login'));
						$arrDadosIns['SENHA']           = md5($senha);
						$arrDadosIns['CHAVE_ATIVACAO']  = $chaveAtiv;
						$arrDadosIns['ATIVO']           = 'N';
						
						$this->global_model->insert('usuario', $arrDadosIns);
						$idNewUser = $this->global_model->get_insert_id();
						
						$arrDadosIns = Array();
						$arrDadosIns['IDUSUARIO']          = $idNewUser;
						$arrDadosIns['IDLOGRADOURO']       = ($this->input->post('idlogradouro') != '' ? $this->input->post('idlogradouro') : NULL);
						$arrDadosIns['IDNATUREZAJUR']      = $this->input->post('naturezajur');
						$arrDadosIns['IESTADUAL']          = $this->input->post('iestadual');
						$arrDadosIns['RAZAOSOCIAL']        = removeAcentos($this->input->post('razaosocial'), TRUE);
						$arrDadosIns['NOMEFANTASIA']       = removeAcentos($this->input->post('nomefantasia'), TRUE);
						$arrDadosIns['TELEFONEGERAL']      = $this->input->post('telefone1');
						$arrDadosIns['EMAILGERAL']         = $this->input->post('email');
						$arrDadosIns['SITE']               = $this->input->post('site');
						$arrDadosIns['AREATOTAL_VENDAS']   = $this->input->post('area_total');
						$arrDadosIns['AREATOTAL_LIVROS']   = $this->input->post('area_livros');
						$arrDadosIns['NOMERESP']           = removeAcentos($this->input->post('nomeresp'), TRUE);
						$arrDadosIns['TELEFONERESP']       = $this->input->post('telefoneresp');
						$arrDadosIns['EMAILRESP']          = $this->input->post('emailresp');
						$arrDadosIns['SKYPERESP']          = $this->input->post('skyperesp');
						$arrDadosIns['END_NUMERO']         = $this->input->post('end_numero');
						$arrDadosIns['END_COMPLEMENTO']    = removeAcentos($this->input->post('end_complemento'), TRUE);
						$arrDadosIns['BANCO']              = $this->input->post('banco');
						$arrDadosIns['AGENCIA']            = $this->input->post('agencia');
						$arrDadosIns['CONTA']              = $this->input->post('conta');
						$arrDadosIns['ASSOC_ENT_SINDICAL'] = $this->input->post('assoc_ent_sind');
						$arrDadosIns['FILIACAO_ENT_ASSOC'] = $this->input->post('fil_ent_assoc');
						$arrDadosIns['TERMOACEITE']        = 'S';
						$arrDadosIns['LATITUDE']           = NULL;
						$arrDadosIns['LONGITUDE']          = NULL;
						$arrDadosIns['DATA_UPD']           = date('Y/m/d H:i:s');
						
						$this->global_model->insert('cadpdv', $arrDadosIns);
						
						if ($this->input->post('socios') != '')
						{
							$this->usuario_class->save_socio($idNewUser, $this->input->post('socios'));
						}
						
						$this->db->trans_complete();
						
						if ($this->db->trans_status() === FALSE)
						{
							$arrErro['msg']      = 'Ocorreu um erro na tentativa de salvar o cadastro.';
							$arrErro['tipoUser'] = 0;
							$this->load->view('erro_view', $arrErro);
						}
						else
						{
							//geocodificar endereço
							$arrEnd      = $this->pdv_model->getDadosCadastro($idNewUser);
							$address     = str_replace(' ','+',$arrEnd[0]['NOMELOGRADOURO2']) . ',+' . str_replace(' ','+',$arrEnd[0]['END_NUMERO']) . ',+' . str_replace(' ','+',$arrEnd[0]['NOMEBAIRRO']) . ',+' . str_replace(' ','+',$arrEnd[0]['NOMECIDADESUB']) . '+' . $arrEnd[0]['IDUF'];
							$request_url = MAPS_HOST . "&address=" . $address;
							
							$arqXml = simplexml_load_file($request_url);
							
							if ($arqXml)
							{
								$status = $arqXml->status;
								
								if ($status == 'OK')
								{
									$dadosLatLong = $arqXml->xpath('/GeocodeResponse/result[1]/geometry/location');
									
									$arrGeoCod = Array();
									$arrGeoCod['LATITUDE']  = $dadosLatLong[0]->lat;
									$arrGeoCod['LONGITUDE'] = $dadosLatLong[0]->lng;
									
									$this->global_model->update('cadpdv', $arrGeoCod, 'IDUSUARIO = ' . $idNewUser);
								}
							}
							
							//enviar e-mail para ativação do cadastro
							$this->load->library('email');
							
							/* INICIO ENVIO LOCALHOST -> deixar comentado em produção */
							$config = Array();
							$config['protocol']     = 'smtp';
							$config['smtp_host']    = '10.10.1.1';
							$config['smtp_port']    = '25';
							$config['smtp_timeout'] = '30';
							$config['smtp_user']    = EMAIL_FROM;
							$config['smtp_pass']    = EMAIL_PASSWORD;
							$config['newline']      = "\r\n";
							
							$config2 = Array();
							$config2['protocol']     = 'smtp';
							$config2['smtp_host']    = 'ssl://smtp.googlemail.com';
							$config2['smtp_port']    = '465';
							$config2['smtp_timeout'] = '30';
							$config2['smtp_user']    = 'moisesviana@gmail.com';
							$config2['smtp_pass']    = '';
							$config2['newline']      = "\r\n";
							
							$this->email->initialize($config);
							/* FIM ENVIO LOCALHOST */
							
							$msgEmail = "A Fundação Biblioteca Nacional agradece o seu cadastramento, seus dados de acesso são: \n\nLogin: " . $this->input->post('login') . " \nSenha: " . $senha . " \n\nATENÇÃO!\nATENÇÃO!\n\n Para conseguir acessar o sistema é necessário que seja feita a validação do seu cadastro. Valide seu cadastro acessando o endereço: " . URL_EXEC . "autocad/valida/" . $chaveAtiv . " \n\nAtenciosamente, \nBiblioteca Nacional";
							
							$this->email->from(EMAIL_FROM, EMAIL_FROM_NAME);
							$this->email->to($this->input->post('emailresp'));
							$this->email->subject('Validação de Cadastro');
							$this->email->message($msgEmail);
							
							if ($this->email->send())
							{
								$arrSucesso['msg']      = 'Cadastro realizado com sucesso!<br /> Você em breve receberá um e-mail para validação do seu cadastro. Foi enviado para o e-mail do responsável: ' . $this->input->post('emailresp') . '.';
								$arrSucesso['link']     = 'login';
								$arrSucesso['txtlink']  = 'Ir para Página Inicial';
								$arrSucesso['tipoUser'] = 0;
								$this->load->view('sucesso_view', $arrSucesso);
							}
							else
							{
								$arrErro['msg']      = 'Seu cadastro foi salvo com sucesso, porém não conseguimos enviar o e-mail de validação. <br />Por favor, entre em contato com a Biblioteca Nacional para a ativação do seu cadastro.';
								$arrErro['tipoUser'] = 0;
								$this->load->view('erro_view', $arrErro);
								
								//echo $this->email->print_debugger();
							}
						}
					}
				}
				else
				{
					$acao = '';
				}
			}
			
			//informações zeradas para nao dar erro no parse
			$arrDados['tipopessoa_aux']  = $tipopessoa;
			$arrDados['cnpjcpf']         = $cnpjcpf;
			$arrDados['iestadual']       = '';
			$arrDados['razaosocial']     = '';
			$arrDados['nomefantasia']    = '';
			$arrDados['telefone1']       = '';
			$arrDados['email']           = '';
			$arrDados['site']            = '';
			$arrDados['login']           = '';
			$arrDados['idlogradouro']    = '';
			$arrDados['cep']             = '';
			$arrDados['uf']              = '';
			$arrDados['cidade']          = '';
			$arrDados['bairro']          = '';
			$arrDados['logradouro']      = '';
			$arrDados['logcomplemento']  = '';
			$arrDados['end_numero']      = '';
			$arrDados['end_complemento'] = '';
			$arrDados['area_total']      = '';
			$arrDados['area_livros']     = '';
			$arrDados['assoc_ent_sind']  = '';
			$arrDados['fil_ent_assoc']   = '';
			$arrDados['banco']           = '';
			$arrDados['agencia']         = '';
			$arrDados['conta']           = '';
			$arrDados['nomeresp']        = '';
			$arrDados['telefoneresp']    = '';
			$arrDados['skyperesp']       = '';
			$arrDados['emailresp']       = '';
			$arrDados['emailresp2']      = '';
			$arrDados['socios']          = '';
			
			if ($acao == '' || $acao == 'new')
			{
				$arrDados['natjur']    = $this->naturezajur_model->getCombo($tipopessoa);
				
				$this->parser->parse('pdv/autocad_form_view', $arrDados);
			}
		}
		else
		{
			$arrErro['msg']      = 'Cadastro não permitido.';
			$arrErro['tipoUser'] = 0;
			$this->load->view('erro_view', $arrErro);
		}
	}
	
	function editora($acao = '', $auxTP = 'PJ', $auxCpjCnpj = '')
	{
		$this->load->model('configuracao_model');
		
		if ($this->configuracao_model->validaConfig('2', date('Y/m/d')))
		{
			$this->load->helper('form');
			$this->load->library('form_validation');
			$this->load->library('usuario_class');
			$this->load->model('global_model');
			$this->load->model('naturezajur_model');
			$this->load->model('isbn_model');
			
			$arrDados   = Array();
			$tipopessoa = $auxTP;
			$cnpjcpf    = str_replace('_','/',$auxCpjCnpj);
			$buscaIsbn  = 1;
			
			if ($acao == 'save')
			{
				$buscaIsbn  = 0;
				$tipopessoa = $this->input->post('tipopessoa');
				$labelAux   = ($tipopessoa == 'PJ' ? 'Razão Social' : 'Nome');
				
				$this->form_validation->set_rules('tipopessoa',      'Tipo Pessoa',                          'required');
				$this->form_validation->set_rules('cnpjcpf',         'CNPJ/CPF',                             'trim|required|max_length[18]|valida_cnpj_cpf|xss_clean');
				$this->form_validation->set_rules('iestadual',       'Inscrição Estadual',                   'trim|max_length[20]|xss_clean');
				$this->form_validation->set_rules('razaosocial',     $labelAux,                              'trim|required|max_length[100]|xss_clean');
				$this->form_validation->set_rules('nomefantasia',    'Nome Fantasia',                        'trim|max_length[100]|xss_clean');
				$this->form_validation->set_rules('naturezajur',     'Natureza Jurídica',                    'valida_combo|xss_clean');
				$this->form_validation->set_rules('atividadeprinc',  'Atividade Principal',                  'valida_combo|xss_clean');
				$this->form_validation->set_rules('telefone1',       'Telefone Geral',                       'trim|required|max_length[15]|xss_clean');
				$this->form_validation->set_rules('email',           'E-mail Geral',                         'trim|required|max_length[100]|valid_email');
				$this->form_validation->set_rules('site',            'Site',                                 'trim|max_length[100]|xss_clean');
				$this->form_validation->set_rules('login',           'Login',                                'trim|required|min_length[6]|max_length[20]|alpha_numeric|xss_clean');
				$this->form_validation->set_rules('idlogradouro',    'Endereço',                             'is_numeric');
				$this->form_validation->set_rules('cep',             '',                                     '');
				$this->form_validation->set_rules('uf',              '',                                     '');
				$this->form_validation->set_rules('cidade',          '',                                     '');
				$this->form_validation->set_rules('bairro',          '',                                     '');
				$this->form_validation->set_rules('logradouro',      '',                                     '');
				$this->form_validation->set_rules('logcomplemento',  '',                                     '');
				$this->form_validation->set_rules('end_numero',      'Endereço - Número',                    'trim|required|max_length[8]|xss_clean');
				$this->form_validation->set_rules('end_complemento', 'Complemento',                          'trim|max_length[30]|xss_clean');
				$this->form_validation->set_rules('qtd_titulos',     'Quantidade de títulos ativos',         'trim|integer|xss_clean');
				$this->form_validation->set_rules('tem_rede',        'Mantém rede de ...',                   'trim|required|xss_clean');
				$this->form_validation->set_rules('assoc_ent_sind',  'Associado a entidade sindical',        'trim|max_length[100]|xss_clean');
				$this->form_validation->set_rules('fil_ent_assoc',   'Filiada a entidade(s) associativa(s)', 'trim|max_length[100]|xss_clean');
				$this->form_validation->set_rules('nomeresp',        'Nome Responsável',                     'trim|required|max_length[100]|xss_clean');
				$this->form_validation->set_rules('telefoneresp',    'Telefone do Responsável',              'trim|required|max_length[15]|xss_clean');
				$this->form_validation->set_rules('skyperesp',       'Skype do Responsável',                 'trim|max_length[30]|xss_clean');
				$this->form_validation->set_rules('emailresp',       'E-mail do Responsável',                'trim|required|max_length[100]|matches[emailresp2]|valid_email');
				$this->form_validation->set_rules('emailresp2',      'E-mail do Responsável',                '');
				//$this->form_validation->set_rules('aceite',          'Aceite dos termos do programa',        'required|xss_clean');
				$this->form_validation->set_rules('socios',          'Sócios',                               'trim|valida_socios|xss_clean');
				$this->form_validation->set_rules('atuacao',         'Atua com',                             'trim|valida_atuacao|xss_clean');
				
				if ($this->form_validation->run() == TRUE)
				{
					$where = sprintf("LOGIN = '%s'", strtolower($this->input->post('login')));
					$loginExiste = $this->global_model->get_dado('usuario', '1', $where);
					
					//verifica se existe cadastro no ISBN
					$arrEx = Array('.','-','/');
					$dadosIsbnEd = $this->isbn_model->validaEditora(str_replace($arrEx, '', $this->input->post('cnpjcpf')), ($tipopessoa == 'PF' ? 1 : 2));
					
					if ($loginExiste == 1)
					{
						$arrDados['outrosErros'] = 'Esse nome de Login já está sendo usado. Informe outro.';
						$acao = '';
					}
					elseif ($tipopessoa != 'PJ' && $tipopessoa != 'PF')
					{
						$arrDados['outrosErros'] = 'Tipo pessoa inválido.';
						$acao = '';
					}
					elseif (count($dadosIsbnEd) == 0)
					{
						$arrDados['outrosErros'] = 'O CNPJ/CPF da editora não foi encontrado no ISBN.';
						$acao = '';
					}
					elseif ($this->input->post('idlogradouro') == '')
					{
						$arrDados['outrosErros'] = 'Informe o endereço.';
						$acao = '';
					}
					else
					{
						$chaveAtiv = $this->_geraChave($this->input->post('cnpjcpf'));
						$senha     = substr($chaveAtiv, 0, 6);
						
						$this->db->trans_start();
						
						$arrDadosIns = Array();
						$arrDadosIns['IDUSUARIOTIPO']   = 5; //EDITORA
						$arrDadosIns['TIPOPESSOA']      = $this->input->post('tipopessoa');
						$arrDadosIns['CPF_CNPJ']        = $this->input->post('cnpjcpf');
						$arrDadosIns['LOGIN']           = strtolower($this->input->post('login'));
						$arrDadosIns['SENHA']           = md5($senha);
						$arrDadosIns['CHAVE_ATIVACAO']  = $chaveAtiv;
						$arrDadosIns['ATIVO']           = 'N';
						
						$this->global_model->insert('usuario', $arrDadosIns);
						$idNewUser = $this->global_model->get_insert_id();
						
						$arrDadosIns = Array();
						$arrDadosIns['IDUSUARIO']          = $idNewUser;
						$arrDadosIns['IDLOGRADOURO']       = ($this->input->post('idlogradouro') != '' ? $this->input->post('idlogradouro') : NULL);
						$arrDadosIns['IDNATUREZAJUR']      = $this->input->post('naturezajur');
						$arrDadosIns['IESTADUAL']          = $this->input->post('iestadual');
						$arrDadosIns['RAZAOSOCIAL']        = removeAcentos($this->input->post('razaosocial'), TRUE);
						$arrDadosIns['NOMEFANTASIA']       = removeAcentos($this->input->post('nomefantasia'), TRUE);
						$arrDadosIns['TELEFONEGERAL']      = $this->input->post('telefone1');
						$arrDadosIns['EMAILGERAL']         = $this->input->post('email');
						$arrDadosIns['SITE']               = $this->input->post('site');
						$arrDadosIns['NOMERESP']           = removeAcentos($this->input->post('nomeresp'), TRUE);
						$arrDadosIns['TELEFONERESP']       = $this->input->post('telefoneresp');
						$arrDadosIns['EMAILRESP']          = $this->input->post('emailresp');
						$arrDadosIns['SKYPERESP']          = $this->input->post('skyperesp');
						$arrDadosIns['END_NUMERO']         = $this->input->post('end_numero');
						$arrDadosIns['END_COMPLEMENTO']    = removeAcentos($this->input->post('end_complemento'), TRUE);
						$arrDadosIns['QTD_TITULO_ATIVOS']  = ($this->input->post('qtd_titulos') != '' ? $this->input->post('qtd_titulos') : 0);
						$arrDadosIns['ASSOC_ENT_SINDICAL'] = $this->input->post('assoc_ent_sind');
						$arrDadosIns['FILIACAO_ENT_ASSOC'] = $this->input->post('fil_ent_assoc');
						$arrDadosIns['TEM_REDE_DIST']      = ($this->input->post('tem_rede') == 'S' ? 'S' : 'N');
						$arrDadosIns['TERMOACEITE']        = 'S';
						$arrDadosIns['DATA_UPD']           = date('Y/m/d H:i:s');
						
						$this->global_model->insert('cadeditora', $arrDadosIns);
						
						if ($this->input->post('socios') != '')
						{
							$this->usuario_class->save_socio($idNewUser, $this->input->post('socios'));
						}
						
						if ($this->input->post('atuacao') != '' || $this->input->post('atividadeprinc') != 0)
						{
							$this->usuario_class->save_atuacao($idNewUser, $this->input->post('atuacao'), $this->input->post('atividadeprinc'));
						}
						
						$this->db->trans_complete();
						
						if ($this->db->trans_status() === FALSE)
						{
							$arrErro['msg']      = 'Ocorreu um erro na tentativa de salvar o cadastro.';
							$arrErro['tipoUser'] = 0;
							$this->load->view('erro_view', $arrErro);
						}
						else
						{
							//enviar e-mail para ativação do cadastro
							$this->load->library('email');
							
							/* INICIO ENVIO LOCALHOST -> deixar comentado em produção */
							$config = Array();
							$config['protocol']     = 'smtp';
							$config['smtp_host']    = '10.10.1.1';
							$config['smtp_port']    = '25';
							$config['smtp_timeout'] = '30';
							$config['smtp_user']    = EMAIL_FROM;
							$config['smtp_pass']    = EMAIL_PASSWORD;
							$config['newline']      = "\r\n";
							
							$config2 = Array();
							$config2['protocol']     = 'smtp';
							$config2['smtp_host']    = 'ssl://smtp.googlemail.com';
							$config2['smtp_port']    = '465';
							$config2['smtp_timeout'] = '30';
							$config2['smtp_user']    = 'moisesviana@gmail.com';
							$config2['smtp_pass']    = '';
							$config2['newline']      = "\r\n";
							
							$this->email->initialize($config);
							/* FIM ENVIO LOCALHOST */
							
							$msgEmail = "A Fundação Biblioteca Nacional agradece o seu cadastramento, seus dados de acesso são: \n\nLogin: " . $this->input->post('login') . " \nSenha: " . $senha . " \n\nATENÇÃO!\nATENÇÃO!\n\n Para conseguir acessar o sistema é necessário que seja feita a validação do seu cadastro. Valide seu cadastro acessando o endereço: " . URL_EXEC . "autocad/valida/" . $chaveAtiv . " \n\nAtenciosamente, \nBiblioteca Nacional";
							
							$this->email->from(EMAIL_FROM, EMAIL_FROM_NAME);
							$this->email->to($this->input->post('emailresp'));
							$this->email->subject('Validação de Cadastro');
							$this->email->message($msgEmail);
							
							if ($this->email->send())
							{
								$arrSucesso['msg']      = 'Cadastro realizado com sucesso!<br /> Você em breve receberá um e-mail para validação do seu cadastro. Foi enviado para o e-mail do responsável: ' . $this->input->post('emailresp') . '.';
								$arrSucesso['link']     = 'login';
								$arrSucesso['txtlink']  = 'Ir para Página Inicial';
								$arrSucesso['tipoUser'] = 0;
								$this->load->view('sucesso_view', $arrSucesso);
							}
							else
							{
								$arrErro['msg']      = 'Seu cadastro foi salvo com sucesso, porém não conseguimos enviar o e-mail de validação. <br />Por favor, entre em contato com a Biblioteca Nacional para a ativação do seu cadastro.';
								$arrErro['tipoUser'] = 0;
								$this->load->view('erro_view', $arrErro);
								
								//echo $this->email->print_debugger();
							}
						}
					}
				}
				else
				{
					$acao = '';
				}
			}
			
			//informações zeradas para nao dar erro no parse
			$arrDados['tipopessoa_aux']  = $tipopessoa;
			$arrDados['cnpjcpf']         = $cnpjcpf;
			$arrDados['iestadual']       = '';
			$arrDados['razaosocial']     = '';
			$arrDados['nomefantasia']    = '';
			$arrDados['telefone1']       = '';
			$arrDados['email']           = '';
			$arrDados['site']            = '';
			$arrDados['login']           = '';
			$arrDados['idlogradouro']    = '';
			$arrDados['cep']             = '';
			$arrDados['uf']              = '';
			$arrDados['cidade']          = '';
			$arrDados['bairro']          = '';
			$arrDados['logradouro']      = '';
			$arrDados['logcomplemento']  = '';
			$arrDados['end_numero']      = '';
			$arrDados['end_complemento'] = '';
			$arrDados['qtd_titulos']     = '';
			$arrDados['assoc_ent_sind']  = '';
			$arrDados['fil_ent_assoc']   = '';
			$arrDados['nomeresp']        = '';
			$arrDados['telefoneresp']    = '';
			$arrDados['skyperesp']       = '';
			$arrDados['emailresp']       = '';
			$arrDados['emailresp2']      = '';
			$arrDados['socios']          = '';
			$arrDados['atuacao']         = '';
			
			//busca dados do ISBN
			if ($buscaIsbn == 1)
			{
				$arrEx = Array('.','-','/');
				$dadosIsbnEd = $this->isbn_model->validaEditora(str_replace($arrEx, '', $cnpjcpf), ($tipopessoa == 'PF' ? 1 : 2));
				
				if (count($dadosIsbnEd) > 0)
				{
					$arrDados['razaosocial']  = $dadosIsbnEd[0]['RAZAOSOCIAL'];
					$arrDados['nomefantasia'] = $dadosIsbnEd[0]['NOMEFANTASIA'];
					$arrDados['email']        = $dadosIsbnEd[0]['EMAIL'];
					$arrDados['site']         = $dadosIsbnEd[0]['SITE'];
				}
				else
				{
					$acao = 'erro';
					$arrErro['msg']      = 'O CNPJ/CPF da editora não foi encontrado no ISBN.';
					$arrErro['tipoUser'] = 0;
					$this->load->view('erro_view', $arrErro);
				}
			}
			
			if ($acao == '' || $acao == 'new')
			{
				$arrDados['natjur']    = $this->naturezajur_model->getCombo($tipopessoa);
				$arrDados['atividade'] = $this->global_model->selectx('atividade', 'ATIVO = \'S\'', 'NOMEATIVIDADE');
				
				$this->parser->parse('editora/autocad_form_view', $arrDados);
			}
		}
		else
		{
			$arrErro['msg']      = 'Cadastro não permitido.';
			$arrErro['tipoUser'] = 0;
			$this->load->view('erro_view', $arrErro);
		}
	}
	
	function distribuidor($acao = '', $auxTP = 'PJ', $auxCpjCnpj = '')
	{
		$this->load->model('configuracao_model');
		
		if ($this->configuracao_model->validaConfig('3', date('Y/m/d')))
		{
			$this->load->helper('form');
			$this->load->library('form_validation');
			$this->load->library('usuario_class');
			$this->load->model('global_model');
			$this->load->model('naturezajur_model');
			
			$arrDados   = Array();
			$tipopessoa = $auxTP;
			$cnpjcpf    = str_replace('_','/',$auxCpjCnpj);
			
			if ($acao == 'save')
			{
				$tipopessoa = $this->input->post('tipopessoa');
				$labelAux   = ($tipopessoa == 'PJ' ? 'Razão Social' : 'Nome');
				
				$this->form_validation->set_rules('tipopessoa',      'Tipo Pessoa',                          'required');
				$this->form_validation->set_rules('cnpjcpf',         'CNPJ/CPF',                             'trim|required|max_length[18]|valida_cnpj_cpf|xss_clean');
				$this->form_validation->set_rules('iestadual',       'Inscrição Estadual',                   'trim|max_length[20]|xss_clean');
				$this->form_validation->set_rules('razaosocial',     $labelAux,                              'trim|required|max_length[100]|xss_clean');
				$this->form_validation->set_rules('nomefantasia',    'Nome Fantasia',                        'trim|max_length[100]|xss_clean');
				$this->form_validation->set_rules('naturezajur',     'Natureza Jurídica',                    'valida_combo|xss_clean');
				$this->form_validation->set_rules('telefone1',       'Telefone Geral',                       'trim|required|max_length[15]|xss_clean');
				$this->form_validation->set_rules('email',           'E-mail Geral',                         'trim|required|max_length[100]|valid_email');
				$this->form_validation->set_rules('site',            'Site',                                 'trim|max_length[100]|xss_clean');
				$this->form_validation->set_rules('login',           'Login',                                'trim|required|min_length[6]|max_length[20]|alpha_numeric|xss_clean');
				$this->form_validation->set_rules('idlogradouro',    'Endereço',                             'is_numeric');
				$this->form_validation->set_rules('cep',             '',                                     '');
				$this->form_validation->set_rules('uf',              '',                                     '');
				$this->form_validation->set_rules('cidade',          '',                                     '');
				$this->form_validation->set_rules('bairro',          '',                                     '');
				$this->form_validation->set_rules('logradouro',      '',                                     '');
				$this->form_validation->set_rules('logcomplemento',  '',                                     '');
				$this->form_validation->set_rules('end_numero',      'Endereço - Número',                    'trim|required|max_length[8]|xss_clean');
				$this->form_validation->set_rules('end_complemento', 'Complemento',                          'trim|max_length[30]|xss_clean');
				$this->form_validation->set_rules('assoc_ent_sind',  'Associado a entidade sindical',        'trim|max_length[100]|xss_clean');
				$this->form_validation->set_rules('fil_ent_assoc',   'Filiada a entidade(s) associativa(s)', 'trim|max_length[100]|xss_clean');
				$this->form_validation->set_rules('nomeresp',        'Nome Responsável',                     'trim|required|max_length[100]|xss_clean');
				$this->form_validation->set_rules('telefoneresp',    'Telefone do Responsável',              'trim|required|max_length[15]|xss_clean');
				$this->form_validation->set_rules('skyperesp',       'Skype do Responsável',                 'trim|max_length[30]|xss_clean');
				$this->form_validation->set_rules('emailresp',       'E-mail do Responsável',                'trim|required|max_length[100]|matches[emailresp2]|valid_email');
				$this->form_validation->set_rules('emailresp2',      'E-mail do Responsável',                '');
				//$this->form_validation->set_rules('aceite',          'Aceite dos termos do programa',        'required|xss_clean');
				$this->form_validation->set_rules('socios',          'Sócios',                               'trim|valida_socios|xss_clean');
				$this->form_validation->set_rules('atuacao',         'Atua com',                             'trim|valida_atuacao|xss_clean');
				$this->form_validation->set_rules('regiaoatend',     'Região de atendimento',                'trim|valida_regiaoatend|xss_clean');
				$this->form_validation->set_rules('cidadeatend',     'Região de atendimento',                'trim|valida_str_codigos|xss_clean');
				$this->form_validation->set_rules('editorastrab',    'Editoras com que trabalha',            'trim|valida_str_codigos|xss_clean');
				
				if ($this->form_validation->run() == TRUE)
				{
					$where = sprintf("LOGIN = '%s'", strtolower($this->input->post('login')));
					$loginExiste = $this->global_model->get_dado('usuario', '1', $where);
					
					if ($loginExiste == 1)
					{
						$arrDados['outrosErros'] = 'Esse nome de Login já está sendo usado. Informe outro.';
						$acao = '';
					}
					elseif ($tipopessoa != 'PJ' && $tipopessoa != 'PF')
					{
						$arrDados['outrosErros'] = 'Tipo pessoa inválido.';
						$acao = '';
					}
					elseif ($this->input->post('idlogradouro') == '')
					{
						$arrDados['outrosErros'] = 'Informe o endereço.';
						$acao = '';
					}
					else
					{
						$chaveAtiv = $this->_geraChave($this->input->post('cnpjcpf'));
						$senha     = substr($chaveAtiv, 0, 6);
						
						$this->db->trans_start();
						
						$arrDadosIns = Array();
						$arrDadosIns['IDUSUARIOTIPO']   = 3; //DISTRIBUIDOR
						$arrDadosIns['TIPOPESSOA']      = $this->input->post('tipopessoa');
						$arrDadosIns['CPF_CNPJ']        = $this->input->post('cnpjcpf');
						$arrDadosIns['LOGIN']           = strtolower($this->input->post('login'));
						$arrDadosIns['SENHA']           = md5($senha);
						$arrDadosIns['CHAVE_ATIVACAO']  = $chaveAtiv;
						$arrDadosIns['ATIVO']           = 'N';
						
						$this->global_model->insert('usuario', $arrDadosIns);
						$idNewUser = $this->global_model->get_insert_id();
						
						$arrDadosIns = Array();
						$arrDadosIns['IDUSUARIO']          = $idNewUser;
						$arrDadosIns['IDLOGRADOURO']       = ($this->input->post('idlogradouro') != '' ? $this->input->post('idlogradouro') : NULL);
						$arrDadosIns['IDNATUREZAJUR']      = $this->input->post('naturezajur');
						$arrDadosIns['IESTADUAL']          = $this->input->post('iestadual');
						$arrDadosIns['RAZAOSOCIAL']        = removeAcentos($this->input->post('razaosocial'), TRUE);
						$arrDadosIns['NOMEFANTASIA']       = removeAcentos($this->input->post('nomefantasia'), TRUE);
						$arrDadosIns['TELEFONEGERAL']      = $this->input->post('telefone1');
						$arrDadosIns['EMAILGERAL']         = $this->input->post('email');
						$arrDadosIns['SITE']               = $this->input->post('site');
						$arrDadosIns['NOMERESP']           = removeAcentos($this->input->post('nomeresp'), TRUE);
						$arrDadosIns['TELEFONERESP']       = $this->input->post('telefoneresp');
						$arrDadosIns['EMAILRESP']          = $this->input->post('emailresp');
						$arrDadosIns['SKYPERESP']          = $this->input->post('skyperesp');
						$arrDadosIns['END_NUMERO']         = $this->input->post('end_numero');
						$arrDadosIns['END_COMPLEMENTO']    = removeAcentos($this->input->post('end_complemento'), TRUE);
						$arrDadosIns['ASSOC_ENT_SINDICAL'] = $this->input->post('assoc_ent_sind');
						$arrDadosIns['FILIACAO_ENT_ASSOC'] = $this->input->post('fil_ent_assoc');
						$arrDadosIns['TERMOACEITE']        = 'S';
						$arrDadosIns['DATA_UPD']           = date('Y/m/d H:i:s');
						
						$this->global_model->insert('caddistribuidor', $arrDadosIns);
						
						if ($this->input->post('socios') != '')
						{
							$this->usuario_class->save_socio($idNewUser, $this->input->post('socios'));
						}
						
						if ($this->input->post('atuacao') != '')
						{
							$this->usuario_class->save_atuacao($idNewUser, $this->input->post('atuacao'));
						}
						
						if ($this->input->post('editorastrab') != '')
						{
							$this->usuario_class->save_editoratrab($idNewUser, $this->input->post('editorastrab'));
						}
						
						if ($this->input->post('regiaoatend') != '')
						{
							$this->usuario_class->save_regiao_atende($idNewUser, $this->input->post('regiaoatend'));
						}
						
						if ($this->input->post('cidadeatend') != '')
						{
							$this->usuario_class->save_cidade_atende($idNewUser, $this->input->post('cidadeatend'));
						}
						
						$this->db->trans_complete();
						
						if ($this->db->trans_status() === FALSE)
						{
							$arrErro['msg']      = 'Ocorreu um erro na tentativa de salvar o cadastro.';
							$arrErro['tipoUser'] = 0;
							$this->load->view('erro_view', $arrErro);
						}
						else
						{
							//enviar e-mail para ativação do cadastro
							$this->load->library('email');
							
							/* INICIO ENVIO LOCALHOST -> deixar comentado em produção */
							$config = Array();
							$config['protocol']     = 'smtp';
							$config['smtp_host']    = '10.10.1.1';
							$config['smtp_port']    = '25';
							$config['smtp_timeout'] = '30';
							$config['smtp_user']    = EMAIL_FROM;
							$config['smtp_pass']    = EMAIL_PASSWORD;
							$config['newline']      = "\r\n";
							
							$config2 = Array();
							$config2['protocol']     = 'smtp';
							$config2['smtp_host']    = 'ssl://smtp.googlemail.com';
							$config2['smtp_port']    = '465';
							$config2['smtp_timeout'] = '30';
							$config2['smtp_user']    = 'moisesviana@gmail.com';
							$config2['smtp_pass']    = '';
							$config2['newline']      = "\r\n";
							
							$this->email->initialize($config);
							/* FIM ENVIO LOCALHOST */
							
							$msgEmail = "A Fundação Biblioteca Nacional agradece o seu cadastramento, seus dados de acesso são: \n\nLogin: " . $this->input->post('login') . " \nSenha: " . $senha . " \n\nATENÇÃO!\nATENÇÃO!\n\n Para conseguir acessar o sistema é necessário que seja feita a validação do seu cadastro. Valide seu cadastro acessando o endereço: " . URL_EXEC . "autocad/valida/" . $chaveAtiv . " \n\nAtenciosamente, \nBiblioteca Nacional";
							
							$this->email->from(EMAIL_FROM, EMAIL_FROM_NAME);
							$this->email->to($this->input->post('emailresp'));
							$this->email->subject('Validação de Cadastro');
							$this->email->message($msgEmail);
							
							if ($this->email->send())
							{
								$arrSucesso['msg']      = 'Cadastro realizado com sucesso!<br /> Você em breve receberá um e-mail para validação do seu cadastro. Foi enviado para o e-mail do responsável: ' . $this->input->post('emailresp') . '.';
								$arrSucesso['link']     = 'login';
								$arrSucesso['txtlink']  = 'Ir para Página Inicial';
								$arrSucesso['tipoUser'] = 0;
								$this->load->view('sucesso_view', $arrSucesso);
							}
							else
							{
								$arrErro['msg']      = 'Seu cadastro foi salvo com sucesso, porém não conseguimos enviar o e-mail de validação. <br />Por favor, entre em contato com a Biblioteca Nacional para a ativação do seu cadastro.';
								$arrErro['tipoUser'] = 0;
								$this->load->view('erro_view', $arrErro);
								
								//echo $this->email->print_debugger();
							}
						}
					}
				}
				else
				{
					$acao = '';
				}
			}
			
			//informações zeradas para nao dar erro no parse
			$arrDados['tipopessoa_aux']  = $tipopessoa;
			$arrDados['cnpjcpf']         = $cnpjcpf;
			$arrDados['iestadual']       = '';
			$arrDados['razaosocial']     = '';
			$arrDados['nomefantasia']    = '';
			$arrDados['telefone1']       = '';
			$arrDados['email']           = '';
			$arrDados['site']            = '';
			$arrDados['login']           = '';
			$arrDados['idlogradouro']    = '';
			$arrDados['cep']             = '';
			$arrDados['uf']              = '';
			$arrDados['cidade']          = '';
			$arrDados['bairro']          = '';
			$arrDados['logradouro']      = '';
			$arrDados['logcomplemento']  = '';
			$arrDados['end_numero']      = '';
			$arrDados['end_complemento'] = '';
			$arrDados['assoc_ent_sind']  = '';
			$arrDados['fil_ent_assoc']   = '';
			$arrDados['nomeresp']        = '';
			$arrDados['telefoneresp']    = '';
			$arrDados['skyperesp']       = '';
			$arrDados['emailresp']       = '';
			$arrDados['emailresp2']      = '';
			$arrDados['socios']          = '';
			$arrDados['atuacao']         = '';
			$arrDados['regiaoatend']     = '';
			$arrDados['cidadeatend']     = '';
			$arrDados['editorastrab']    = '';
			
			if ($acao == '' || $acao == 'new')
			{
				$arrDados['natjur']    = $this->naturezajur_model->getCombo($tipopessoa);
				$arrDados['atividade'] = $this->global_model->selectx('atividade', 'ATIVO = \'S\'', 'NOMEATIVIDADE');
				$arrDados['ufs']       = $this->global_model->selectx('uf', '1 = 1', 'NOMEUF');
				
				$this->parser->parse('distribuidor/autocad_form_view', $arrDados);
			}
		}
		else
		{
			$arrErro['msg']      = 'Cadastro não permitido.';
			$arrErro['tipoUser'] = 0;
			$this->load->view('erro_view', $arrErro);
		}
	}
	
	private function _geraChave($strAux)
	{
		return md5($strAux . mt_rand(1, 999) . time());
	}
	
	function valida($chave = '')
	{
		if ($chave != '')
		{
			$this->load->model('global_model');
			$this->load->helper('security');
			
			$chave = xss_clean($chave);
			
			$where      = sprintf("CHAVE_ATIVACAO = '%s' AND ATIVO = 'N' AND DATA_ATIVACAO IS NULL", $chave);
			$validChave = $this->global_model->get_dado('usuario', '1', $where);
			
			if ($validChave == 1)
			{
				$arrUpd = Array();
				$arrUpd['DATA_ATIVACAO'] = date('Y/m/d H:i:s');
				$arrUpd['ATIVO']         = 'S';
				
				$this->db->trans_start();
				
				$where = sprintf("CHAVE_ATIVACAO = '%s'", $chave);
				$this->global_model->update('usuario', $arrUpd, $where);
				
				$this->db->trans_complete();
						
				if ($this->db->trans_status() === FALSE)
				{
					$arrErro['msg']      = 'Erro ao validar o cadastro. Tente novamente.';
					$arrErro['tipoUser'] = 0;
					$this->load->view('erro_view', $arrErro);
				}
				else
				{
					$arrSucesso['msg']      = 'Cadastro validado com sucesso!<br />Guarde a senha que foi enviada para o seu e-mail, com ela você poderá acessar o sistema.';
					$arrSucesso['link']     = 'login';
					$arrSucesso['txtlink']  = 'Ir para Página Inicial';
					$arrSucesso['tipoUser'] = 0;
					$this->load->view('sucesso_view', $arrSucesso);
				}
			}
			else
			{
				$where       = sprintf("CHAVE_ATIVACAO = '%s' AND ATIVO = 'S'", $chave);
				$validChave2 = $this->global_model->get_dado('usuario', '1', $where);
				
				if ($validChave2 == 1)
				{
					$arrErro['msg']      = 'Chave já validada.';
					$arrErro['tipoUser'] = 0;
					$this->load->view('erro_view', $arrErro);
				}
				else
				{
					$arrErro['msg']      = 'Chave inválida.';
					$arrErro['tipoUser'] = 0;
					$this->load->view('erro_view', $arrErro);
				}
			}
		}
		else
		{
			$arrErro['msg']      = 'Chave inválida.';
			$arrErro['tipoUser'] = 0;
			$this->load->view('erro_view', $arrErro);
		}
	}
	        
	function solicitaSenha($acao = '', $tipo = '', $ajax = false)
	{
		$this->load->helper('string');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('global_model');
		$this->load->model('usuario_model');
		
		$arrDados = Array();
		$arrDados['displayFromCPFCNPJ'] = 'none';
		$arrDados['checkFromCPFCNPJ'] = $arrDados['displayFromLogin'] = '';
		$arrDados['checkFromLogin'] ='checked';
		
		if ($acao == 'save' && $tipo != '')
		{
			$arrDados['displayFromLogin'] = $arrDados['checkFromLogin'] = $arrDados['displayFromCPFCNPJ'] = $arrDados['checkFromCPFCNPJ'] = '';
			
			if ($ajax)
			{
				$valida = true;
			}
			else
			{
				if($tipo == 'login')
				{
					$arrDados['checkFromLogin'] = 'checked';
					$arrDados['displayFromCPFCNPJ'] = 'none'; 
					$this->form_validation->set_rules('login', 'Login', 'trim|required|xss_clean');
					$this->form_validation->set_rules('email_login', 'E-mail', 'trim|required|xss_clean');
				}
				else
				{
					$arrDados['checkFromCPFCNPJ'] = 'checked';
					$arrDados['displayFromLogin'] = 'none';
					$this->form_validation->set_rules('cpfcnpj', 'CPF/CNPJ', 'trim|required|xss_clean');
					$this->form_validation->set_rules('email_cpfcnpj', 'E-mail', 'trim|required|xss_clean');
				}

				$valida = $this->form_validation->run();
			}
			
			if ($valida == TRUE)
			{

				if($tipo == 'login')
				{
					$dadosUser = $this->usuario_model->getDadosByLogin($this->input->post('login'));

					if (count($dadosUser) > 0)
					{
						$email = $this->usuario_model->validaEmailUsuario(get_table_dados($dadosUser[0]['IDUSUARIOTIPO']), $this->input->post('email_login'), $dadosUser[0]['IDUSUARIO']);
						if(!$email)
							$dadosUser = Array();
					}
				}
				else
				{
					$dadosUser = $this->usuario_model->getDadosByCPFCNPJ($this->input->post('cpfcnpj'));
					if (count($dadosUser) > 0)
					{
						$users = Array();
						foreach ($dadosUser as $key => $dado) {
							$email = $this->usuario_model->validaEmailUsuario(get_table_dados($dado['IDUSUARIOTIPO']), $this->input->post('email_cpfcnpj'), $dado['IDUSUARIO']);
							if($email)
							{
								$dadoCad= $this->usuario_model->getDadosCadastro($dado['IDUSUARIO'], $dado['IDUSUARIOTIPO']);
								
								$users[$key]['tipoCadastro'] = $this->global_model->get_dado('usuariotipo','NOMEUSUARIOTIPO','IDUSUARIOTIPO = ' . $dado['IDUSUARIOTIPO']);
								$users[$key]['razaoSocial']  = $dadoCad[0]['RAZAOSOCIAL'];
								$users[$key]['login']        = $dado['LOGIN'];
								$users[$key]['email']        = $dadoCad[0]['EMAILRESP'];
							}

						}
						
						if (count($users) == 0)
						{
							$dadosUser = Array();
						}
						elseif(count($users) > 1)
						{
						   $arrDados['listaLogin'] = $users;
						   $this->parser->parse('solicitaSenha_form_view', $arrDados);
						   return;
						}
					}
				}

				if (count($dadosUser) > 0)
				{
                    if($dadosUser[0]['ATIVO'] == 'S')
                    {
                        //gerar nova senha e salva
                        $novasenha = random_string('alnum', 6);

                        $arrUpd = Array();
                        $arrUpd['SENHA'] = md5($novasenha);

                        $this->db->trans_start();

                        $where = sprintf("IDUSUARIO = %s", $dadosUser[0]['IDUSUARIO']);
                        $this->global_model->update('usuario', $arrUpd, $where);

                        $this->db->trans_complete();

                        if ($this->db->trans_status() === FALSE)
                        {
                            $arrErro['msg']      = 'Ocorreu um erro na tentativa de redefinir a senha. Tente novamente.';
                            $arrErro['tipoUser'] = 0;
                            $this->load->view('erro_view', $arrErro);
                        }
                        else
                        {
                            //pegar email e enviar e-mail com nova senha
                            $dadosCad = $this->usuario_model->getDadosCadastro($dadosUser[0]['IDUSUARIO'], $dadosUser[0]['IDUSUARIOTIPO']);

                            $this->load->library('email');

                            /* INICIO ENVIO LOCALHOST -> deixar comentado em produção */
                            $config = Array();
                            $config['protocol']     = 'smtp';
                            $config['smtp_host']    = '10.10.1.1';
                            $config['smtp_port']    = '25';
                            $config['smtp_timeout'] = '30';
                            $config['smtp_user']    = EMAIL_FROM;
                            $config['smtp_pass']    = EMAIL_PASSWORD;
                            $config['newline']      = "\r\n";

                            $config2 = Array();
                            $config2['protocol']     = 'smtp';
                            $config2['smtp_host']    = 'ssl://smtp.googlemail.com';
                            $config2['smtp_port']    = '465';
                            $config2['smtp_timeout'] = '30';
                            $config2['smtp_user']    = 'moisesviana@gmail.com';
                            $config2['smtp_pass']    = '';
                            $config2['newline']      = "\r\n";

                            $this->email->initialize($config);
                            /* FIM ENVIO LOCALHOST */

                            $msgEmail = "Sua senha no sistema foi redefinida:\n\nLogin: " . $dadosUser[0]['LOGIN'] . "\nSenha: " . $novasenha . " \n\nAtenciosamente, \nBiblioteca Nacional";

                            $this->email->from(EMAIL_FROM, EMAIL_FROM_NAME);
                            $this->email->to($dadosCad[0]['EMAILRESP']);
                            $this->email->subject('Redefinição de senha');
                            $this->email->message($msgEmail);

                            if ($this->email->send())
                            {
                            	if ($ajax)
                            	{
                            		echo 'Sua nova senha foi enviada para: ' . $dadosCad[0]['EMAILRESP'] . '.';
                            		
                            	}
                            	else 
                            	{
                                    $arrSucesso['msg']      = 'Sua nova senha foi enviada para: ' . $dadosCad[0]['EMAILRESP'] . '.';
                                    $arrSucesso['link']     = 'login';
                                    $arrSucesso['txtlink']  = 'Ir para Página Inicial';
                                    $arrSucesso['tipoUser'] = 0;
                                    $this->load->view('sucesso_view', $arrSucesso);
                            	}
                            }
                            else
                            {
                            	if ($ajax)
                            	{
                            		echo 'Desculpe, não foi possível enviar o e-mail com a nova senha.';
                            	}
                            	else 
                            	{
                            		$arrErro['msg']      = 'Desculpe, não foi possível enviar o e-mail com sua nova senha. <br />Por favor, entre em contato com a Biblioteca Nacional para obtê-la ou tente novamente.';
                                    $arrErro['tipoUser'] = 0;
                                    $this->load->view('erro_view', $arrErro);

                                    //echo $this->email->print_debugger();
                            	}
                            }
                        }
                    }
                    else
                    {
                        $arrDados['outrosErros'] = 'Cadastro não validado. Não é possível redefinir sua senha. Para validar acesse o endereço que foi enviado por e-mail após o seu cadastramento.';
                        $acao = '';
                    }
				}
				else
				{
					$arrDados['outrosErros'] = 'Dados não encontrados.';
					$acao = '';
				}
			}
			else
			{
				$acao = '';
			}
		}
		
		if ($acao == '' || $tipo == '')
		{
			$this->parser->parse('solicitaSenha_form_view', $arrDados);
		}
	}
	
	function listaCidades()
	{
		$uf      = $this->input->post('uf', TRUE);
		$cidades = $this->input->post('cidades', TRUE);
		
		$arrAuxCid = ($cidades != '' ? explode('#', $cidades) : Array());
		
		$this->load->model('logradouro_model');
		$this->load->model('global_model');
		
		$arrCidades = $this->logradouro_model->getCidades($uf);
		$qtdCidades = count($arrCidades);
		
		for ($i = 0; $i < $qtdCidades; $i++)
		{
			$arrCidades[$i]['CORLINHA'] = ($i % 2 == 0 ? 'l_p' : 'l_i');
			$arrCidades[$i]['CHECKED']  = '';
			
			if (in_array($arrCidades[$i]['IDCIDADE'], $arrAuxCid))
			{
				$arrCidades[$i]['CHECKED'] = 'checked';
			}
		}
		
		$arrDados = Array();
		$arrDados['uf']      = $uf;
		$arrDados['nomeuf']  = $this->global_model->get_dado('uf','NOMEUF','IDUF = \'' . $uf . '\'');
		$arrDados['cidades'] = $arrCidades;
		
		$this->parser->parse('distribuidor/listacidades_view', $arrDados);
	}
	
	function listaEditoras()
	{
		$editoras = $this->input->post('editoras', TRUE);
		
		$arrAuxEdit = ($editoras != '' ? explode('#', $editoras) : Array());
		
		$this->load->model('usuario_model');
		
		$arrEditoras = $this->usuario_model->getCadListaEditoras();
		$qtdEdit     = count($arrEditoras);
		
		for ($i = 0; $i < $qtdEdit; $i++)
		{
			$arrEditoras[$i]['CORLINHA'] = ($i % 2 == 0 ? 'l_p' : 'l_i');
			$arrEditoras[$i]['CHECKED']  = '';
			
			if (in_array($arrEditoras[$i]['IDUSUARIO'], $arrAuxEdit))
			{
				$arrEditoras[$i]['CHECKED'] = 'checked';
			}
		}
		
		$arrDados = Array();
		$arrDados['editoras'] = $arrEditoras;
		
		$this->parser->parse('distribuidor/listaeditoras_view', $arrDados);
	}
    
    function renviaEmail($acao = '', $tipo = '', $ajax = false)
	{
		$this->load->helper('string');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('global_model');
		$this->load->model('usuario_model');
		
		$arrDados = Array();
		
		if ($acao == 'save')
		{
            if($tipo == '')
            {
                $this->form_validation->set_rules('cpfcnpj', 'CPF/CNPJ', 'trim|required|xss_clean');
            }
            else
            {
                $this->form_validation->set_rules('login', 'CPF/CNPJ', 'trim|required|xss_clean');
            }
            
			if ($this->form_validation->run() == TRUE)
			{	
                if($tipo == '')
                {
                    $dadosUser = $this->usuario_model->getDadosByCPFCNPJ($this->input->post('cpfcnpj'));
                    if (count($dadosUser) > 0)
                    {
                        $users = Array();
                        foreach ($dadosUser as $key => $dado) 
                        {
                            if($dado['ATIVO'] == 'N')
                            {
                                $dadoCad = $this->usuario_model->getDadosCadastro($dado['IDUSUARIO'], $dado['IDUSUARIOTIPO']);

                                $users[$key]['tipoCadastro'] = $this->global_model->get_dado('usuariotipo','NOMEUSUARIOTIPO','IDUSUARIOTIPO = ' . $dado['IDUSUARIOTIPO']);
                                $users[$key]['razaoSocial']  = $dadoCad[0]['RAZAOSOCIAL'];
                                $users[$key]['login']        = $dado['LOGIN'];
                                $users[$key]['email']        = $dadoCad[0]['EMAILRESP'];
                            }

                        }

                        if(count($users) > 0)
                        {
                            $arrDados['listaCadastro'] = $users;
                            $this->parser->parse('reenviaEmail_form_view', $arrDados);
                        }
                        else
                        {
                            $arrDados['outrosErros'] = 'Cadastro validado. Não é possível reenviar o e-mail de validação. Caso não lembre sua senha entre em redefinir senha.';
                            $acao = '';
                        }
                    }
                    else
                    {
                        $arrDados['outrosErros'] = 'Dados não encontrados.';
                        $acao = '';
                    }
                }
                else
                {
                    $dadosUser = $this->usuario_model->getDadosByLogin($this->input->post('login')); 

                    if (count($dadosUser) > 0)
                    {
                        if($dadosUser[0]['ATIVO'] == 'N')
                        {
                            $dadoCad = $this->usuario_model->getDadosCadastro($dadosUser[0]['IDUSUARIO'], $dadosUser[0]['IDUSUARIOTIPO']);
                            //gerar nova senha e salva
                            $novasenha = random_string('alnum', 6);

                            $arrUpd = Array();
                            $arrUpd['SENHA'] = md5($novasenha);

                            $this->db->trans_start();

                            $where = sprintf("IDUSUARIO = %s", $dadosUser[0]['IDUSUARIO']);
                            $this->global_model->update('usuario', $arrUpd, $where);

                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $arrErro['msg']      = 'Ocorreu um erro na tentativa de redefinir a senha. Tente novamente.';
                                $arrErro['tipoUser'] = 0;
                                $this->load->view('erro_view', $arrErro);
                            }
                            else
                            {
                                //pegar email e enviar e-mail com nova senha
                                $dadosCad = $this->usuario_model->getDadosCadastro($dadosUser[0]['IDUSUARIO'], $dadosUser[0]['IDUSUARIOTIPO']);

                                $this->load->library('email');

                                /* INICIO ENVIO LOCALHOST -> deixar comentado em produção */
                                $config = Array();
                                $config['protocol']     = 'smtp';
                                $config['smtp_host']    = '10.10.1.1';
                                $config['smtp_port']    = '25';
                                $config['smtp_timeout'] = '30';
                                $config['smtp_user']    = EMAIL_FROM;
                                $config['smtp_pass']    = EMAIL_PASSWORD;
                                $config['newline']      = "\r\n";

                                $this->email->initialize($config);
                                
                                /* FIM ENVIO LOCALHOST */
                                $msgEmail = "A Fundação Biblioteca Nacional agradece o seu cadastramento, seus dados de acesso são: \n\nLogin: " . $dadosUser[0]['LOGIN'] . " \nSenha: " . $novasenha . " \n\nATENÇÃO!\nATENÇÃO!\n\n Para conseguir acessar o sistema é necessário que seja feita a validação do seu cadastro. Valide seu cadastro acessando o endereço: " . URL_EXEC . "autocad/valida/" . $dadosUser[0]['CHAVE_ATIVACAO'] . " \n\nAtenciosamente, \nBiblioteca Nacional";

                                $this->email->from(EMAIL_FROM, EMAIL_FROM_NAME);
                                $this->email->to($dadosCad[0]['EMAILRESP']);
                                $this->email->subject('Validação de Cadastro');
                                $this->email->message($msgEmail);

                                if ($this->email->send())
                                {
	                                if ($ajax)
	                            	{
	                            		echo 'Foi reenviado o email de validação para: ' . $dadosCad[0]['EMAILRESP'] . '.';
	                            	}
	                            	else 
	                            	{
	                                	$arrSucesso['msg']      = 'Foi reenviado o email de validação para: ' . $dadosCad[0]['EMAILRESP'] . '.';
	                                    $arrSucesso['link']     = 'login';
	                                    $arrSucesso['txtlink']  = 'Ir para Página Inicial';
	                                    $arrSucesso['tipoUser'] = 0;
	                                    $this->load->view('sucesso_view', $arrSucesso);
	                                }
                                }
                                else
                                {
	                                if ($ajax)
	                            	{
	                            		echo 'Desculpe, não foi possível enviar o e-mail de validação.';	                            		
	                            	}
	                            	else 
	                            	{
	                                	$arrErro['msg']      = 'Desculpe, não foi possível reenviar o e-mail de validação. <br />Por favor, entre em contato com a Biblioteca Nacional ou tente novamente.';
	                                    $arrErro['tipoUser'] = 0;
	                                    $this->load->view('erro_view', $arrErro);
	                                }
                                }
                            }
                        }
                        else
                        {
                            $arrDados['outrosErros'] = 'Cadastro validado. Não é possível reenviar o e-mail de validação. Caso não lembre sua senha entre em redefinir senha.';
                            $acao = '';
                            
                            if ($ajax) echo $arrDados['outrosErros'];
                        }
                    }
                    else
                    {
                        $arrDados['outrosErros'] = 'Dados não encontrados.';
                        $acao = '';
                        
                        if ($ajax) echo $arrDados['outrosErros'];
                    }
                }
            }
        }
        else
        {
            $acao = '';
        }
        
        if ($acao == '' && $tipo == '')
        {
            $this->parser->parse('reenviaEmail_form_view', $arrDados);
        }
	}
}

/* End of file autocad.php */
/* Location: ./system/application/controllers/autocad.php */