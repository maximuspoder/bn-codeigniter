<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Publico extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}
	
	/*************************************************************************
	 *
	 * CONTROLLERS PUBLICOS
	 *
	 * listaLivros     : lista livros participantes do edital
	 *
	 *************************************************************************/
	
	function index()
	{
		$arrErro['msg']      = 'Página não existe.';
		$arrErro['tipoUser'] = 0;
		$this->load->view('erro_view', $arrErro);
	}
	
	function listaLivros($nPag = 1)
	{
		$this->load->library('form_validation');
		$this->load->model('livro_model');
                $this->load->model('global_model');
		
		$arrDados = Array();
		$arrDados['nPag']      = (is_numeric($nPag) && $nPag > 0 ? $nPag : 1);
		$arrDados['exibir_pp'] = 30;
		$arrDados['limit_de']  = ($arrDados['nPag'] - 1) * $arrDados['exibir_pp'];
		
		//cria array para filtros
        $where = Array();
		
        $this->form_validation->set_rules('filtro_isbn','ISBN','trim|xss_clean');
        $this->form_validation->set_rules('filtro_titulo','Título','trim|xss_clean');
        $this->form_validation->set_rules('filtro_autor','Autor','trim|xss_clean');
        $this->form_validation->set_rules('filtro_editora','Editora','trim|xss_clean');
        
        if($this->form_validation->run() == TRUE || $this->input->post() == FALSE)
        {
            $isbn    = $this->input->post('filtro_isbn');
            $titulo  = $this->input->post('filtro_titulo');
            $autor   = $this->input->post('filtro_autor');
            $editora = $this->input->post('filtro_editora');

            // Filtro para editora
            if($editora != '')
            {
                array_push($where, " (e.RAZAOSOCIAL LIKE '%" . htmlspecialchars($editora) . "%' OR e.NOMEFANTASIA LIKE '%" . htmlspecialchars($editora) . "%') ");
            }
            
            // Filtro para confirmação
            if($isbn != '')
            {
                if(strlen($isbn) == 10)
                {
                    $isbn = '978' . $isbn;
                }
                
                array_push($where, " l.ISBN = '" . $isbn . "' ");
            }

            // Filtro para Título
            if($titulo != '')
            {
                array_push($where, " l.TITULO LIKE '%" . $titulo . "%' ");
            }

            // Filtro para Autor
            if($autor != '')
            {
                array_push($where, " l.AUTOR LIKE '%" . $autor . "%' ");
            }
            
        }
		
        // set filtros para pesquisa.
        $arrDados['where'] = (count($where) > 0) ? implode(" AND ", $where) : '1 = 1';
        
		//pesquisa
		$arrDados['livros'] = $this->livro_model->livrosParticipantes($arrDados, FALSE);
		$tamArr             = count($arrDados['livros']);
		$arrDados['vazio']  = ($tamArr == 0 ? TRUE : FALSE);

		for ($i = 0; $i < $tamArr; $i++)
		{
			$arrDados['livros'][$i]['NLINHA'] = $i + 1 + $arrDados['limit_de'];
                        
                        $where = sprintf("IDLIVRO = %s ", $arrDados['livros'][$i]['IDLIVRO'] );
			$links = $this->global_model->get_dado('livro', 'LINKDADOS', $where);
                        $arrDados['livros'][$i]['COR'] = ($links == "" ? "#000000" : "#0000FF");
		}
		
		$arrDados['livros'] = add_corLinha($arrDados['livros'], 1);
		
		//dados da paginação
		$qtdReg = $this->livro_model->livrosParticipantes($arrDados, TRUE);
		$nPaginas = $qtdReg[0]['CONT'] / $arrDados['exibir_pp'];
		
		if ($qtdReg[0]['CONT'] % $arrDados['exibir_pp'] != 0)
		{
			$nPaginas = (int)$nPaginas + 1;
		}
		
		$arrDados['nPaginas'] = $nPaginas;
		$arrDados['qtdReg']   = $qtdReg[0]['CONT'];
		
		$this->parser->parse('publico/lista_titulos_programa', $arrDados);
	}
	
}

/* End of file Publico.php */
/* Location: ./system/application/controllers/Publico.php */