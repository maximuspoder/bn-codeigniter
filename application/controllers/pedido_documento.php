<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe Pedido_Documento
*
* Abstração de classe para entidade pedido_documento.
* 
* @author		Leandro Mangini Antunes
* @package		application
* @subpackage	application.pedido_documento
* @since		2012-05-09
*
*/
class Pedido_Documento extends CI_Controller 
{

	/**
	* __construct()
	* Construtor de classe.
	* @return object
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if ($this->session->userdata('acessoSis') == FALSE)
		{
			redirect('/login/');
			exit;
		}
	}
	
	/**
	* modal_anexos_documento()
	* Exibe html de modal de anexos de um documentoformulario padrao de dados da biblioteca.
	* @param integer idusuario
	* @return void
	*/
	function modal_anexos_documento($id_pedido_documento = 0)
	{
		// Coleta anexos
		$this->load->model('pedido_model');
		$this->load->model('pedido_documento_model');
		$anexos = $this->pedido_documento_model->get_anexos($id_pedido_documento);
		$documento = $this->pedido_documento_model->get_dados_documento($id_pedido_documento);
		$pedido = $this->pedido_model->get_dados_pedido(get_value($documento, 'IDPEDIDO'));
		
		// Load view
		$this->parser->parse('pedido_documento/modal_anexos_documento', array('anexos' => $anexos, 'id_pedido_documento' => $id_pedido_documento, 'tipo_usuario' => $this->session->userdata('tipoUsuario'), 'documento' => $documento, 'pedido' => $pedido));
	}
	
	/**
	* modal_inserir_anexo()
	* Exibe html da tela de insercao de anexo para um documento.
	* @param integer idpedido
	* @return void
	*/
	function modal_inserir_anexo($id_pedido_documento = 0)
	{
		// Passa id_pedido_doc para ser usado em view
		$args['id_pedido_documento'] = $id_pedido_documento;
		
		// Parse do html da modal
		$this->load->view('pedido_documento/modal_inserir_anexo', $args);
	}
	
	/**
	* modal_inserir_anexo_proccess()
	* Processa o envio de formulário feito pela dialog de inserção de anexos.
	* @return void
	**/
	function modal_inserir_anexo_proccess()
	{
		// Somente ADM, biblioteca ou pdv podem entrar nesta tela
        if($_FILES['arquivo']['name']) {
			// Tenta fazer o upload do arquivo (5MB)
			list($file, $error) = upload_file('arquivo', 'application/uploads/', 'jpg,jpeg,gif,png,bmp,pdf', 656000);
			
			// Argumentos p/ view
			$arr_args['error'] = '';
			$arr_args['option'] = '';
			
			// Se nao tiver erro, entao faz insert no banco de dados	
			if($error)
			{
				$arr_args['error'] = $error;
			} else {
				// Carrega model global, para insert
				$this->load->library('logsis');
				$this->load->model('global_model');
				$this->load->model('pedido_documento_model');
				
				// Monta array de inserção
				$args['id_pedido_documento'] = $this->input->post('id_pedido_documento');
				$args['arquivo'] = $file;
				$args['arquivo_original'] = $_FILES['arquivo']['name'];
				$args['numero_doc'] = $this->input->post('numero_doc');
				$args['valor_doc'] = $this->input->post('valor_doc');
				
				// Insert
				$this->global_model->insert('pedido_documento_anexo', $args);
				$this->logsis->insereLog(array('INSERT', 'OK', 'pedido_documento_anexo', $this->input->post('id_pedido_documento'), 'Inserção de anexo para documento do pedido', var_export($args, true)));
				
				// Insere observação do pedido (não lida caso liberar edição = 'S')
				// Coleta dados do documento
				$documento = $this->pedido_documento_model->get_dados_documento($this->input->post('id_pedido_documento'));
				$entidade = ($this->session->userdata('tipoUsuario') == 2) ? 'BIBLIOTECA' : 'PDV';
				$args_obs['idpedido'] = get_value($documento, 'IDPEDIDO');
				$args_obs['idusuario'] = $this->session->userdata('idUsuario');
				$args_obs['categoria'] = 'ACOES_' . $entidade;
				$args_obs['observacao'] = 'Inserção de anexo de documento (ID Documento ' . $this->input->post('id_pedido_documento') . ', Número do doc. anexo ' . $this->input->post('numero_doc') . ') feita por ' . $entidade . '.';
				$args_obs['lido'] = (get_value($documento, 'LIBERAR_EDICAO') == 'S') ? 'N' : 'S';
				$this->global_model->insert('pedido_observacao', $args_obs);
				$this->logsis->insereLog(array('INSERT', 'OK', 'pedido_observacao', $this->input->post('idpedido'), 'Observação de pedido inserida', var_export($args_obs, true)));
				
				// Coleta o ultimo ID inserido para coletar os dados
				// e envia para model process que popula os combos dos pedidos
				$arr_args['option'] = $this->pedido_documento_model->get_dados_documento($this->global_model->get_insert_id());
			}
			
			// Parse na tela que fecha a dialog, e chama o load da tabela de documentos
			redirect('/pedido_documento/modal_anexos_documento/' . $this->input->post('id_pedido_documento'));
		}
    }
	
	/**
	* modal_deletar_anexo()
	* Exibe html da tela de confirmação de deleção de anexo encaminhado como parâmetro.
	* @param integer id_documento_anexo
	* @return void
	*/
	function modal_deletar_anexo($id_documento_anexo = 0, $id_pedido_documento = 0)
	{
		$this->parser->parse('pedido_documento/modal_deletar_anexo', array('id_documento_anexo' => $id_documento_anexo, 'id_pedido_documento' => $id_pedido_documento));
	}
	
	/**
	* modal_deletar_anexo_proccess()
	* Processa o envio de formulário feito pela dialog ajax de deleção de anexos.
	* @return void
	**/
	function modal_deletar_anexo_proccess()
	{
		$this->load->library('logsis');
		$this->load->model('global_model');
		$this->load->model('pedido_model');
		$this->load->model('pedido_documento_model');
		
		// Coleta dados do documento para remove o anexo fisicamente do banco
		$anexo = $this->pedido_documento_model->get_dados_anexo($this->input->post('id_pedido_documento_anexo'));
		
		// Deleção
		$this->global_model->delete('pedido_documento_anexo', array('id_pedido_documento_anexo' => $this->input->post('id_pedido_documento_anexo')));
		$this->logsis->insereLog(array('DELETE', 'OK', 'pedido_documento_anexo', $this->input->post('id_pedido_documento'), 'Delete de anexo para documento do pedido', 'id_pedido_documento_anexo => ' . $this->input->post('id_pedido_documento_anexo')));
		
		// Remove anexo fisicamente da app
		unlink('application/uploads/' . get_value($anexo, 'ARQUIVO'));
		
		// Insere observação do pedido (não lida caso liberar edição = 'S')
		// Coleta dados do documento
		$documento = $this->pedido_documento_model->get_dados_documento($this->input->post('id_pedido_documento'));
		$entidade = ($this->session->userdata('tipoUsuario') == 2) ? 'BIBLIOTECA' : 'PDV';
		$args_obs['idpedido'] = get_value($documento, 'IDPEDIDO');
		$args_obs['idusuario'] = $this->session->userdata('idUsuario');
		$args_obs['categoria'] = 'ACOES_' . $entidade;
		$args_obs['observacao'] = 'Deleção de anexo de documento (ID Documento ' . $this->input->post('id_pedido_documento') . ', Número do doc. anexo ' . get_value($anexo, 'NUMERO_DOC') . ') feita por ' . $entidade . '.';
		$args_obs['lido'] = (get_value($documento, 'LIBERAR_EDICAO') == 'S') ? 'N' : 'S';
		$this->global_model->insert('pedido_observacao', $args_obs);
		$this->logsis->insereLog(array('INSERT', 'OK', 'pedido_observacao', $this->input->post('idpedido'), 'Observação de pedido inserida', var_export($args_obs, true)));
		
		// Parse na tela que fecha a dialog, e chama o load da tabela de documentos
		redirect('pedido_documento/modal_anexos_documento/' . $this->input->post('id_pedido_documento'));
    }
	
	/**
	* modal_liberar_edicao_documento()
	* Exibe html da tela de liberação de edição de documento.
	* @param integer id_pedido_documento
	* @return void
	*/
	function modal_liberar_edicao_documento($id_pedido_documento = 0)
	{
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Coleta dados do documento
			$this->load->model('pedido_documento_model');
			$args['documento'] = $this->pedido_documento_model->get_dados_documento($id_pedido_documento);
			
			$this->load->view('pedido_documento/modal_liberar_edicao_documento', $args);
		}
	}
	
	/**
	* modal_liberar_edicao_documento_proccess()
	* Processa formulário de liberação para edição de documento.
	* @return void
	*/
	function modal_liberar_edicao_documento_proccess()
	{
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Carrega models necessarios
			$this->load->model('global_model');
			
			// Calcula se é para liberar ou nao, conforme o que foi ou não marcado
			$liberar = ($this->input->post('liberar_edicao') != '') ? 'S' : 'N';
			$this->global_model->update('pedido_documento', array('LIBERAR_EDICAO' => $liberar), "ID_PEDIDO_DOCUMENTO = " . $this->input->post('id_pedido_documento'));
			
			// Fecha modal
			$this->load->view('close_modal');
		}
	}
}
