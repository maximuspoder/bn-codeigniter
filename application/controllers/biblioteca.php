<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe Biblioteca
*
* Esta classe contém métodos para a abstração da entidade biblioteca.
* 
* @author		Leandro Mangini Antunes
* @package		application
* @subpackage	controllers.biblioteca
* @since		2012-07-02
*
*/
class Biblioteca extends CI_Controller {

	/**
	* __construct()
	* @return object
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if ($this->session->userdata('acessoSis') == FALSE)
		{
			redirect('/login/');
			exit;
		}
	}
	
	/*************************************************************************
	 *
	 * CONTROLLERS DE BIBLIOTECA
	 *
	 * form_alt                 : formulario de alteração de cadastro 
	 * escolhapdv               : lista pdvs para escolha do parceiro
	 * savePdvParc              : salva pdv parceiro
     * respFinanceiroFormcad    : formulario de cadastro/alteração de responsavel financeiro da biblioteca
     * comite                   : listagem e link para cadastro de comite
     * comiteform               : form de cadastro e edição do comite
     * comitedel                : exclusao de membro do comite
	 * modallistaagenciasbb     : modal listagem e selecao de agencia bb
	 *
	 *************************************************************************/
	
	function index()
	{
		$arrErro['msg']      = 'Página não existe.';
		$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
		$this->load->view('erro_view', $arrErro);
	}
	
	function form_alt($acao = 'edit')
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('global_model');
		$this->load->model('biblioteca_model');
		$this->load->model('naturezajur_model');
		
		$arrDados   = Array();
		$idUsuario  = $this->session->userdata('idUsuario');
		$tipopessoa = '';
		
		if ($acao == 'save')
		{
			$tipopessoa = $this->input->post('tipopessoa');
			$labelAux   = ($tipopessoa == 'PJ' ? 'Razão Social' : 'Nome');
			
			$this->form_validation->set_rules('tipopessoa',      'Tipo Pessoa',             'required');
			$this->form_validation->set_rules('cnpjcpf',         'CNPJ/CPF',                'trim|required|max_length[18]|valida_cnpj_cpf|xss_clean');
			$this->form_validation->set_rules('iestadual',       'Inscrição Estadual',      'trim|max_length[20]|xss_clean');
			$this->form_validation->set_rules('razaosocial',     $labelAux,                 'trim|required|max_length[100]|xss_clean');
			$this->form_validation->set_rules('nomefantasia',    'Nome Fantasia',           'trim|max_length[100]|xss_clean');
			$this->form_validation->set_rules('naturezajur',     'Natureza Jurídica',       'valida_combo|xss_clean');
			$this->form_validation->set_rules('telefone1',       'Telefone Geral',          'trim|required|max_length[15]|xss_clean');
			$this->form_validation->set_rules('email',           'E-mail Geral',            'trim|required|max_length[100]|valid_email');
			$this->form_validation->set_rules('site',            'Site',                    'trim|max_length[100]|xss_clean');
			$this->form_validation->set_rules('login',           'Login',                   '');
			$this->form_validation->set_rules('idlogradouro',    'Endereço',                'is_numeric');
			$this->form_validation->set_rules('cep',             '',                        '');
			$this->form_validation->set_rules('uf',              '',                        '');
			$this->form_validation->set_rules('cidade',          '',                        '');
			$this->form_validation->set_rules('bairro',          '',                        '');
			$this->form_validation->set_rules('logradouro',      '',                        '');
			$this->form_validation->set_rules('logcomplemento',  '',                        '');
			$this->form_validation->set_rules('end_numero',      'Endereço - Número',       'trim|required|max_length[8]');
			$this->form_validation->set_rules('end_complemento', 'Complemento',             'trim|max_length[30]|xss_clean');
			$this->form_validation->set_rules('nomeresp',        'Nome Responsável',        'trim|required|max_length[100]|xss_clean');
			$this->form_validation->set_rules('telefoneresp',    'Telefone do Responsável', 'trim|required|max_length[15]|xss_clean');
			$this->form_validation->set_rules('skyperesp',       'Skype do Responsável',    'trim|max_length[30]|xss_clean');
			$this->form_validation->set_rules('emailresp',       'E-mail do Responsável',   'trim|required|max_length[100]|valid_email');
			
			if ($this->form_validation->run() == TRUE)
			{
				if ($tipopessoa != 'PJ' && $tipopessoa != 'PF')
				{
					$arrDados['outrosErros'] = 'Tipo pessoa inválido.';
					$acao = '';
				}
				elseif ($this->input->post('idlogradouro') == '')
				{
					$arrDados['outrosErros'] = 'Informe o endereço.';
					$acao = '';
				}
				else
				{
					$where = sprintf("IDUSUARIO = %s", $idUsuario);
					
					$this->db->trans_start();
					
					$arrUpd = Array();
					$arrUpd['IDLOGRADOURO']    = ($this->input->post('idlogradouro') != '' ? $this->input->post('idlogradouro') : NULL);
					$arrUpd['IDNATUREZAJUR']   = $this->input->post('naturezajur');
					$arrUpd['IESTADUAL']       = $this->input->post('iestadual');
					$arrUpd['RAZAOSOCIAL']     = removeAcentos($this->input->post('razaosocial'), TRUE);
					$arrUpd['NOMEFANTASIA']    = removeAcentos($this->input->post('nomefantasia'), TRUE);
					$arrUpd['TELEFONEGERAL']   = $this->input->post('telefone1');
					$arrUpd['EMAILGERAL']      = $this->input->post('email');
					$arrUpd['SITE']            = $this->input->post('site');
					$arrUpd['NOMERESP']        = removeAcentos($this->input->post('nomeresp'), TRUE);
					$arrUpd['TELEFONERESP']    = $this->input->post('telefoneresp');
					$arrUpd['EMAILRESP']       = $this->input->post('emailresp');
					$arrUpd['SKYPERESP']       = $this->input->post('skyperesp');
					$arrUpd['END_NUMERO']      = $this->input->post('end_numero');
					$arrUpd['END_COMPLEMENTO'] = removeAcentos($this->input->post('end_complemento'), TRUE);
					$arrUpd['LATITUDE']        = NULL;
					$arrUpd['LONGITUDE']       = NULL;
					$arrUpd['DATA_UPD']        = date('Y/m/d H:i:s');
					
					$this->global_model->update('cadbiblioteca', $arrUpd, $where);
					
					$this->db->trans_complete();
					
					if ($this->db->trans_status() === FALSE)
					{
						//salva log
						$this->load->library('logsis', Array('UPDATE', 'ERRO', 'cadbiblioteca', $idUsuario, 'Atualização de cadastro de biblioteca', var_export($arrUpd, TRUE)));
						
						$arrErro['msg']      = 'Ocorreu um erro na tentativa de atualizar o cadastro.';
						$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
						$this->load->view('erro_view', $arrErro);
					}
					else
					{
						//geocodificar endereço
						$arrEnd      = $this->biblioteca_model->getDadosCadastro($idUsuario);
						$address     = str_replace(' ','+',$arrEnd[0]['NOMELOGRADOURO2']) . ',+' . str_replace(' ','+',$arrEnd[0]['END_NUMERO']) . ',+' . str_replace(' ','+',$arrEnd[0]['NOMEBAIRRO']) . ',+' . str_replace(' ','+',$arrEnd[0]['NOMECIDADESUB']) . '+' . $arrEnd[0]['IDUF'];
						$request_url = MAPS_HOST . "&address=" . $address;
						$arqXml = simplexml_load_file($request_url);

						if ($arqXml)
						{
							$status = $arqXml->status;
							
							if ($status == 'OK')
							{
								$dadosLatLong = $arqXml->xpath('/GeocodeResponse/result[1]/geometry/location');
								
								$arrGeoCod = Array();
								$arrGeoCod['LATITUDE']  = $dadosLatLong[0]->lat;
								$arrGeoCod['LONGITUDE'] = $dadosLatLong[0]->lng;
								
								$this->global_model->update('cadbiblioteca', $arrGeoCod, $where);
								
								//aprnas para log
								$arrUpd['LATITUDE']  = $dadosLatLong[0]->lat;
								$arrUpd['LONGITUDE'] = $dadosLatLong[0]->lng;
							}
						}
						
						//salva log
						$this->load->library('logsis', Array('UPDATE', 'OK', 'cadbiblioteca', $idUsuario, 'Atualização de cadastro de biblioteca', var_export($arrUpd, TRUE)));
						
						$arrSucesso['msg']      = 'Cadastro atualizado com sucesso!';
						$arrSucesso['link']     = 'home';
						$arrSucesso['txtlink']  = 'Ir para Home';
						$arrSucesso['tipoUser'] = $this->session->userdata('tipoUsuario');
						$this->load->view('sucesso_view', $arrSucesso);
					}
				}
			}
			else
			{
				$acao = '';
			}
		}
		
		//informações zeradas para nao dar erro no parse
		$arrDados['tipopessoa2']     = $tipopessoa;
		$arrDados['tipopessoa_aux']  = '';
		$arrDados['cnpjcpf']         = '';
		$arrDados['naturezajur_aux'] = 0;
		$arrDados['iestadual']       = '';
		$arrDados['razaosocial']     = '';
		$arrDados['nomefantasia']    = '';
		$arrDados['telefone1']       = '';
		$arrDados['email']           = '';
		$arrDados['site']            = '';
		$arrDados['login']           = '';
		$arrDados['idlogradouro']    = '';
		$arrDados['cep']             = '';
		$arrDados['uf']              = '';
		$arrDados['cidade']          = '';
		$arrDados['bairro']          = '';
		$arrDados['logradouro']      = '';
		$arrDados['logcomplemento']  = '';
		$arrDados['end_numero']      = '';
		$arrDados['end_complemento'] = '';
		$arrDados['nomeresp']        = '';
		$arrDados['telefoneresp']    = '';
		$arrDados['skyperesp']       = '';
		$arrDados['emailresp']       = '';
		$arrDados['loadDadosEdit']   = 0;
		
		if ($acao == 'edit')
		{
			$dadosB = $this->biblioteca_model->getDadosCadastro($idUsuario);
			
			$arrDados['tipopessoa2']     = $dadosB[0]['TIPOPESSOA'];
			$arrDados['tipopessoa_aux']  = $dadosB[0]['TIPOPESSOA'];
			$arrDados['cnpjcpf']         = $dadosB[0]['CPF_CNPJ'];
			$arrDados['naturezajur_aux'] = $dadosB[0]['IDNATUREZAJUR'];
			$arrDados['iestadual']       = $dadosB[0]['IESTADUAL'];
			$arrDados['razaosocial']     = $dadosB[0]['RAZAOSOCIAL'];
			$arrDados['nomefantasia']    = $dadosB[0]['NOMEFANTASIA'];
			$arrDados['telefone1']       = $dadosB[0]['TELEFONEGERAL'];
			$arrDados['email']           = $dadosB[0]['EMAILGERAL'];
			$arrDados['site']            = $dadosB[0]['SITE'];
			$arrDados['login']           = $dadosB[0]['LOGIN'];
			$arrDados['idlogradouro']    = $dadosB[0]['IDLOGRADOURO'];
			$arrDados['cep']             = $dadosB[0]['CEP'];
			$arrDados['uf']              = $dadosB[0]['IDUF'];
			$arrDados['cidade']          = $dadosB[0]['NOMECIDADESUB'];
			$arrDados['bairro']          = $dadosB[0]['NOMEBAIRRO'];
			$arrDados['logradouro']      = $dadosB[0]['NOMELOGRADOURO2'];
			$arrDados['logcomplemento']  = $dadosB[0]['COMPLEMENTO'];
			$arrDados['end_numero']      = $dadosB[0]['END_NUMERO'];
			$arrDados['end_complemento'] = $dadosB[0]['END_COMPLEMENTO'];
			$arrDados['nomeresp']        = $dadosB[0]['NOMERESP'];
			$arrDados['telefoneresp']    = $dadosB[0]['TELEFONERESP'];
			$arrDados['skyperesp']       = $dadosB[0]['SKYPERESP'];
			$arrDados['emailresp']       = $dadosB[0]['EMAILRESP'];
			$arrDados['loadDadosEdit']   = 1;
			
			$acao = '';
		}
		
		if ($acao == '')
		{
			$arrDados['natjur'] = $this->naturezajur_model->getCombo($arrDados['tipopessoa2']);
			
			$this->parser->parse('biblioteca/cadastro_formalt_view', $arrDados);
		}
	}
	
	function escolhapdv($acao = '')
	{
		$this->load->model('biblioteca_model');
		$this->load->model('configuracao_model');
		$this->load->model('pdv_model');
		$this->load->model('global_model');
		
		// IDCONFIGURACAO 7: Período para escolha do PDV parceiro
		$pdvParc           = $this->biblioteca_model->getPdvParceiro($this->session->userdata('programa'), $this->session->userdata('idUsuario'));
		$jaEscolheuPdv     = (count($pdvParc) > 0 ? TRUE : FALSE);
		$boolPeriodoSetPdv = ($this->configuracao_model->validaConfig('7', date('Y/m/d'))) ? TRUE : FALSE;
		
		//HABILITACAO
		$habilitado        = $this->global_model->get_dado('usuarioprograma', "'S'", 'IDUSUARIO = ' . $this->session->userdata('idUsuario') . ' AND IDPROGRAMA =' . $this->session->userdata('programa'));
		
		if($habilitado == 'S')
		{
			if ($boolPeriodoSetPdv && !$jaEscolheuPdv)
			{
				$enderecoUser = $this->biblioteca_model->getDadosCadastro($this->session->userdata('idUsuario'));
				
				if (count($enderecoUser) == 1)
				{
					$arrDados     = Array();
					$arrPdv       = Array();
					$arrEnderecos = Array();
					$arrIdLogra   = Array();
					$arrIdPdvs    = Array();
					$raio         = '';
					
					if ($acao == 'setRaio')
					{
						$raio = ($this->input->post('raio') && is_numeric($this->input->post('raio')) && $this->input->post('raio') > 0 ? $this->input->post('raio') : '');
					}
					
					$arrDados['txtpdv']   = '';
					$arrDados['mensagem'] = '';
					
					//pegando PDVs da propria cidade da biblioteca
					$pdvs   = $this->pdv_model->listaGeo($enderecoUser[0]['IDCIDADE'],0);
					$qtdPdv = count($pdvs);
					
					if ($qtdPdv > 0 && $raio == '')
					{
						$arrDados['mensagem'] = 'O sistema está exibindo estabelecimentos da sua cidade.';
						
						for ($i = 0; $i < $qtdPdv; $i++)
						{
							array_push($arrEnderecos, $pdvs[$i]);
							array_push($arrIdPdvs, $pdvs[$i]['IDUSUARIO']);
							array_push($arrIdLogra, $pdvs[$i]['IDLOGRADOURO']);
							array_push($arrPdv, $pdvs[$i]['LATITUDE'] . '||' . $pdvs[$i]['LONGITUDE'] . '||' . $pdvs[$i]['RAZAOSOCIAL'] . '||' . $pdvs[$i]['TELEFONEGERAL'] . '||' . $pdvs[$i]['NOMELOGRADOURO2'] . '||' . $pdvs[$i]['END_NUMERO'] . '||' . $pdvs[$i]['END_COMPLEMENTO'] . '||' . $pdvs[$i]['IDUSUARIO']);
						}
					}
					else
					{
						$pdvs   = $this->pdv_model->listaGeo(0,1);
						$qtdPdv = count($pdvs);
						
						
						
						if ($qtdPdv > 0)
						{
							if ($raio == '')
							{
								if ($enderecoUser[0]['LATITUDE'] != '' && $enderecoUser[0]['LONGITUDE'] != '')
								{
									$arrDados['mensagem'] = 'O sistema está exibindo estabelecimentos em um raio de 50km.';
									
									//pegando PDVs em um raio de até 50 km
									for ($i = 0; $i < $qtdPdv; $i++)
									{
										if ($pdvs[$i]['LATITUDE'] != '' && $pdvs[$i]['LONGITUDE'] != '')
										{
											$distancia = calcDistanciaPontos($enderecoUser[0]['LATITUDE'], $enderecoUser[0]['LONGITUDE'], $pdvs[$i]['LATITUDE'], $pdvs[$i]['LONGITUDE']);
										}
										else
										{
											$distancia = 9999999;
										}
										
										$pdvs[$i]['DISTANCIA'] = $distancia;
										
										if ($distancia <= 50000)
										{
											array_push($arrEnderecos, $pdvs[$i]);
											array_push($arrIdPdvs, $pdvs[$i]['IDUSUARIO']);
											array_push($arrIdLogra, $pdvs[$i]['IDLOGRADOURO']);
											array_push($arrPdv, $pdvs[$i]['LATITUDE'] . '||' . $pdvs[$i]['LONGITUDE'] . '||' . $pdvs[$i]['RAZAOSOCIAL'] . '||' . $pdvs[$i]['TELEFONEGERAL'] . '||' . $pdvs[$i]['NOMELOGRADOURO2'] . '||' . $pdvs[$i]['END_NUMERO'] . '||' . $pdvs[$i]['END_COMPLEMENTO'] . '||' . $pdvs[$i]['IDUSUARIO']);
										}
									}
									
									if (count($arrPdv) == 0)
									{
										$arrDados['mensagem'] = 'O sistema está exibindo estabelecimentos em um raio de 100km.';
										
										//pegando PDVs em um raio de até 100 km
										for ($i = 0; $i < $qtdPdv; $i++)
										{
											if ($pdvs[$i]['DISTANCIA'] <= 100000)
											{
												array_push($arrEnderecos, $pdvs[$i]);
												array_push($arrIdPdvs, $pdvs[$i]['IDUSUARIO']);
												array_push($arrIdLogra, $pdvs[$i]['IDLOGRADOURO']);
												array_push($arrPdv, $pdvs[$i]['LATITUDE'] . '||' . $pdvs[$i]['LONGITUDE'] . '||' . $pdvs[$i]['RAZAOSOCIAL'] . '||' . $pdvs[$i]['TELEFONEGERAL'] . '||' . $pdvs[$i]['NOMELOGRADOURO2'] . '||' . $pdvs[$i]['END_NUMERO'] . '||' . $pdvs[$i]['END_COMPLEMENTO'] . '||' . $pdvs[$i]['IDUSUARIO']);
											}
										}
									}
									
									if (count($arrPdv) == 0)
									{
										$arrDados['mensagem'] = 'O sistema está exibindo estabelecimentos em um raio de 150km.';
										
										//pegando PDVs em um raio de até 150 km
										for ($i = 0; $i < $qtdPdv; $i++)
										{
											if ($pdvs[$i]['DISTANCIA'] <= 150000)
											{
												array_push($arrEnderecos, $pdvs[$i]);
												array_push($arrIdPdvs, $pdvs[$i]['IDUSUARIO']);
												array_push($arrIdLogra, $pdvs[$i]['IDLOGRADOURO']);
												array_push($arrPdv, $pdvs[$i]['LATITUDE'] . '||' . $pdvs[$i]['LONGITUDE'] . '||' . $pdvs[$i]['RAZAOSOCIAL'] . '||' . $pdvs[$i]['TELEFONEGERAL'] . '||' . $pdvs[$i]['NOMELOGRADOURO2'] . '||' . $pdvs[$i]['END_NUMERO'] . '||' . $pdvs[$i]['END_COMPLEMENTO'] . '||' . $pdvs[$i]['IDUSUARIO']);
											}
										}
									}
									
									if (count($arrPdv) == 0)
									{
										$arrDados['mensagem'] = 'O sistema está exibindo estabelecimentos em um raio de 200km.';
										
										//pegando PDVs em um raio de até 200 km
										for ($i = 0; $i < $qtdPdv; $i++)
										{
											if ($pdvs[$i]['DISTANCIA'] <= 200000)
											{
												array_push($arrEnderecos, $pdvs[$i]);
												array_push($arrIdPdvs, $pdvs[$i]['IDUSUARIO']);
												array_push($arrIdLogra, $pdvs[$i]['IDLOGRADOURO']);
												array_push($arrPdv, $pdvs[$i]['LATITUDE'] . '||' . $pdvs[$i]['LONGITUDE'] . '||' . $pdvs[$i]['RAZAOSOCIAL'] . '||' . $pdvs[$i]['TELEFONEGERAL'] . '||' . $pdvs[$i]['NOMELOGRADOURO2'] . '||' . $pdvs[$i]['END_NUMERO'] . '||' . $pdvs[$i]['END_COMPLEMENTO'] . '||' . $pdvs[$i]['IDUSUARIO']);
											}
										}
									}
								}
								
								if (count($arrPdv) == 0)
								{
									$arrDados['mensagem'] = 'O sistema está exibindo estabelecimentos do seu estado.';
									
									//pegando todos os PDVs da mesma UF
									for ($i = 0; $i < $qtdPdv; $i++)
									{
										if ($pdvs[$i]['IDUF'] == $enderecoUser[0]['IDUF'])
										{
											array_push($arrEnderecos, $pdvs[$i]);
											array_push($arrIdPdvs, $pdvs[$i]['IDUSUARIO']);
											array_push($arrIdLogra, $pdvs[$i]['IDLOGRADOURO']);
											array_push($arrPdv, $pdvs[$i]['LATITUDE'] . '||' . $pdvs[$i]['LONGITUDE'] . '||' . $pdvs[$i]['RAZAOSOCIAL'] . '||' . $pdvs[$i]['TELEFONEGERAL'] . '||' . $pdvs[$i]['NOMELOGRADOURO2'] . '||' . $pdvs[$i]['END_NUMERO'] . '||' . $pdvs[$i]['END_COMPLEMENTO'] . '||' . $pdvs[$i]['IDUSUARIO']);
										}
									}
								}
								
								if (count($arrPdv) == 0)
								{
									$arrDados['mensagem'] = 'O sistema está exibindo todos os estabelecimentos cadastrados.';
									
									for ($i = 0; $i < $qtdPdv; $i++)
									{
										array_push($arrEnderecos, $pdvs[$i]);
										array_push($arrIdPdvs, $pdvs[$i]['IDUSUARIO']);
										array_push($arrIdLogra, $pdvs[$i]['IDLOGRADOURO']);
										array_push($arrPdv, $pdvs[$i]['LATITUDE'] . '||' . $pdvs[$i]['LONGITUDE'] . '||' . $pdvs[$i]['RAZAOSOCIAL'] . '||' . $pdvs[$i]['TELEFONEGERAL'] . '||' . $pdvs[$i]['NOMELOGRADOURO2'] . '||' . $pdvs[$i]['END_NUMERO'] . '||' . $pdvs[$i]['END_COMPLEMENTO'] . '||' . $pdvs[$i]['IDUSUARIO']);
									}
								}
							}
							else
							{
								if ($enderecoUser[0]['LATITUDE'] != '' && $enderecoUser[0]['LONGITUDE'] != '')
								{
									$arrDados['mensagem'] = 'O sistema está exibindo estabelecimentos em um raio de ' . $raio . 'km.';
									
									//pegando PDVs até o raio informado
									for ($i = 0; $i < $qtdPdv; $i++)
									{
										if ($pdvs[$i]['LATITUDE'] != '' && $pdvs[$i]['LONGITUDE'] != '')
										{
											$distancia = calcDistanciaPontos($enderecoUser[0]['LATITUDE'], $enderecoUser[0]['LONGITUDE'], $pdvs[$i]['LATITUDE'], $pdvs[$i]['LONGITUDE']);
										}
										else
										{
											$distancia = 9999999;
										}
										
										$pdvs[$i]['DISTANCIA'] = $distancia;
										
										if ($distancia <= ($raio * 1000))
										{
											array_push($arrEnderecos, $pdvs[$i]);
											array_push($arrIdPdvs, $pdvs[$i]['IDUSUARIO']);
											array_push($arrIdLogra, $pdvs[$i]['IDLOGRADOURO']);
											array_push($arrPdv, $pdvs[$i]['LATITUDE'] . '||' . $pdvs[$i]['LONGITUDE'] . '||' . $pdvs[$i]['RAZAOSOCIAL'] . '||' . $pdvs[$i]['TELEFONEGERAL'] . '||' . $pdvs[$i]['NOMELOGRADOURO2'] . '||' . $pdvs[$i]['END_NUMERO'] . '||' . $pdvs[$i]['END_COMPLEMENTO'] . '||' . $pdvs[$i]['IDUSUARIO']);
										}
									}
									
									if (count($arrPdv) == 0)
									{
										$arrDados['mensagem'] = '<b>NENHUM estabelecimento habilitado foi encontrado no raio de ' . $raio . 'km. Tente novamente aumentando o raio de abrangência.</b>';
									}
								}
								else
								{
									$arrDados['mensagem'] = 'O sistema está exibindo todos os estabelecimentos cadastrados. Não foi possível calcular a distância a partir da sua biblioteca.';
									
									for ($i = 0; $i < $qtdPdv; $i++)
									{
										array_push($arrEnderecos, $pdvs[$i]);
										array_push($arrIdPdvs, $pdvs[$i]['IDUSUARIO']);
										array_push($arrIdLogra, $pdvs[$i]['IDLOGRADOURO']);
										array_push($arrPdv, $pdvs[$i]['LATITUDE'] . '||' . $pdvs[$i]['LONGITUDE'] . '||' . $pdvs[$i]['RAZAOSOCIAL'] . '||' . $pdvs[$i]['TELEFONEGERAL'] . '||' . $pdvs[$i]['NOMELOGRADOURO2'] . '||' . $pdvs[$i]['END_NUMERO'] . '||' . $pdvs[$i]['END_COMPLEMENTO'] . '||' . $pdvs[$i]['IDUSUARIO']);
									}
								}
							}
						}
						else
						{
							$arrErro['msg']      = 'Nenhum ponto de venda disponível.';
							$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
							$this->load->view('erro_view', $arrErro);
						}
					}
					
					$arrDados['txtpdv']    = implode('|#', $arrPdv);
					$arrDados['raio']      = $raio;
					$arrDados['enderecos'] = $arrEnderecos;
					$arrDados['enderecos'] = add_corLinha($arrDados['enderecos'], 1);
					
					$arrDados['enderecos2'] = Array();
					
					if (count($arrIdPdvs) > 0 && count($arrIdLogra) > 0)
					{
						$arrDados['enderecos2'] = $this->pdv_model->listaGeo2($arrIdPdvs, $arrIdLogra);
						$arrDados['enderecos2'] = add_corLinha($arrDados['enderecos2'], 1);
					}
					
					$this->parser->parse('biblioteca/escolhapdv_view', $arrDados);
				}
				else
				{
					$arrErro['msg']      = 'Página não encontrada.';
					$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
					$this->load->view('erro_view', $arrErro);
				}
			}
			elseif ($boolPeriodoSetPdv && $jaEscolheuPdv)
			{
				$arrErro['msg']      = 'Você já escolheu seu Ponto de Venda Parceiro.';
				$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
				$this->load->view('erro_view', $arrErro);
			}
			else
			{
				$arrErro['msg']      = 'Funcionalidade indisponível.';
				$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
				$this->load->view('erro_view', $arrErro);
			}
		}
		else
		{
			$arrErro['msg']      = 'Para esta operação é necessário que sua biblioteca esteja habilitada no edital.';
			$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
			$this->load->view('erro_view', $arrErro);
		}
	}
	
	function savePdvParc($idPdv)
	{
		$this->load->model('global_model');
		$this->load->model('biblioteca_model');
		$this->load->model('configuracao_model');
		
		// IDCONFIGURACAO 7: Período para escolha do PDV parceiro
		$pdvParc           = $this->biblioteca_model->getPdvParceiro($this->session->userdata('programa'), $this->session->userdata('idUsuario'));
		$jaEscolheuPdv     = (count($pdvParc) > 0 ? TRUE : FALSE);
		$boolPeriodoSetPdv = ($this->configuracao_model->validaConfig('7', date('Y/m/d'))) ? TRUE : FALSE;
		
		//HABILITACAO
		$habilitado        = $this->global_model->get_dado('usuarioprograma', "'S'", 'IDUSUARIO = ' . $this->session->userdata('idUsuario') . ' AND IDPROGRAMA =' . $this->session->userdata('programa'));
		
		if ($habilitado == 'S')
		{
			if ($boolPeriodoSetPdv && ! $jaEscolheuPdv)
			{
				if (is_numeric($idPdv) && $idPdv > 0)
				{
					$this->load->model('global_model');
					$this->load->model('biblioteca_model');
					
					$where    = sprintf("IDUSUARIO = %s", $idPdv);
					$pdvExist = $this->global_model->get_dado('cadpdv', '1', $where);
					
					if ($pdvExist == 1)
					{
						$this->db->trans_start();
						
						$arrIns = Array();
						$arrIns['IDPROGRAMA']   = $this->session->userdata('programa');
						$arrIns['IDBIBLIOTECA'] = $this->session->userdata('idUsuario');
						$arrIns['IDPDV']        = $idPdv;
						
						$this->global_model->insert('programapdvparc', $arrIns);
						
						//verificar se a biblioteca ja tem pedido... se tiver deve atualizar com o PDV escolhido
						$where    = sprintf("IDPROGRAMA = %s AND IDBIBLIOTECA = %s", $this->session->userdata('programa'), $this->session->userdata('idUsuario'));
						$idPedido = $this->global_model->get_dado('pedido', 'IDPEDIDO', $where);
						
						if ($idPedido != '')
						{
							$arrUpd = Array();
							$arrUpd['IDPDV'] = $idPdv;
							
							$this->global_model->update('pedido', $arrUpd, 'IDPEDIDO = ' . $idPedido);
						}
						
						$this->db->trans_complete();
						
						if ($this->db->trans_status() === FALSE)
						{
							//salva log
							$this->load->library('logsis', Array('INSERT', 'ERRO', 'programapdvparc', $this->session->userdata('idUsuario'), 'Escolha do Ponto de Venda Parceiro', var_export($arrIns, TRUE)));
							
							$arrErro['msg']      = 'Ocorreu um erro na tentativa de salvar o ponto de venda parceiro.';
							$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
							$this->load->view('erro_view', $arrErro);
						}
						else
						{
							//salva log
							$this->load->library('logsis', Array('INSERT', 'OK', 'programapdvparc', $this->session->userdata('idUsuario'), 'Escolha do Ponto de Venda Parceiro', var_export($arrIns, TRUE)));
							
							$arrSucesso['msg']      = 'Ponto de Venda Parceiro salvo com sucesso!';
							$arrSucesso['link']     = 'home';
							$arrSucesso['txtlink']  = 'Ir para Home';
							$arrSucesso['tipoUser'] = $this->session->userdata('tipoUsuario');
							$this->load->view('sucesso_view', $arrSucesso);
						}
					}
					else
					{
						$arrErro['msg']      = 'Ponto de Venda inválido.';
						$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
						$this->load->view('erro_view', $arrErro);
					}
				}
				else
				{
					$arrErro['msg']      = 'Ponto de Venda inválido.';
					$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
					$this->load->view('erro_view', $arrErro);
				}
			}
			elseif ($boolPeriodoSetPdv && $jaEscolheuPdv)
			{
				$arrErro['msg']      = 'Você já escolheu seu Ponto de Venda Parceiro.';
				$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
				$this->load->view('erro_view', $arrErro);
			}
			else
			{
				$arrErro['msg']      = 'Funcionalidade indisponível.';
				$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
				$this->load->view('erro_view', $arrErro);
			}
		}
		else
		{
			$arrErro['msg']      = 'Para esta operação é necessário que sua biblioteca esteja habilitada no edital.';
			$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
			$this->load->view('erro_view', $arrErro);
		}
	}
    
    function respFinanceiroFormcad($acao = '')
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('global_model');
		$this->load->model('biblioteca_model');
        $this->load->model('configuracao_model');
		
		// Só pode verificar os dados do cadastro caso 
		// o id do programa seja != 0
		if($this->session->userdata('programa') == 0)
		{
			$this->load->view('selecionar_edital');
		} else {
			
			
			$arrDados = Array();
			// OLDER: LIBERAVA EDICAO DO CADASTRO DE RESPONSAVEL FINANCEIRO
			// NA DATA DE HABILITACAO DE BIBLIOTECAS
			// $arrDados['boolPeriodoAceite'] = ($this->configuracao_model->validaConfig('8', date('Y/m/d'))) ? TRUE : FALSE;
			$arrDados['isFinanceiro'] = true;
			$arrDados['boolPeriodoEdicao'] = ($this->configuracao_model->validaConfig('20', date('Y/m/d'))) ? TRUE : FALSE;
			
			// Coleta dados do responsável financeiro, com base no programa atual
			$dados = $this->biblioteca_model->getDadosCadastroFinan($this->session->userdata('idUsuario'), $this->session->userdata('programa'));
			
			if ($acao == 'save')
			{
				$this->form_validation->set_rules('cpf',             'CPF',                     'trim|required|max_length[14]|valida_cnpj_cpf|xss_clean');
				$this->form_validation->set_rules('nome',            'Nome',                    'trim|required|max_length[100]|xss_clean');
				$this->form_validation->set_rules('telefone',        'Telefone',                'trim|required|max_length[15]|xss_clean');
				$this->form_validation->set_rules('email',           'E-mail',                  'trim|required|max_length[100]|valid_email');
				$this->form_validation->set_rules('login',           'Login',                   'trim|required|min_length[6]|max_length[20]|alpha_numeric|xss_clean');
				$this->form_validation->set_rules('nomemae',         'Nome da Mãe',             'trim|required|max_length[100]|xss_clean');
				$this->form_validation->set_rules('dataNasc',        'Data de Nascimento',      'trim|required|max_length[10]|valida_data|xss_clean');
				$this->form_validation->set_rules('agenciabb',       'Agência BB',      		'trim|required|is_numeric|xss_clean');
				
				$this->form_validation->set_rules('idlogradouro',    'Endereço',                'is_numeric');
				$this->form_validation->set_rules('cep',             '',                        '');
				$this->form_validation->set_rules('uf',              '',                        '');
				$this->form_validation->set_rules('cidade',          '',                        '');
				$this->form_validation->set_rules('bairro',          '',                        '');
				$this->form_validation->set_rules('logradouro',      '',                        '');
				$this->form_validation->set_rules('logcomplemento',  '',                        '');
				$this->form_validation->set_rules('end_numero',      'Endereço - Número',       'trim|required|max_length[8]|xss_clean');
				$this->form_validation->set_rules('end_complemento', 'Complemento',             'trim|max_length[30]|xss_clean');
				
				if ($this->form_validation->run() == TRUE)
				{
					$arrDadosUser = Array();
					
					$where       = sprintf("LOGIN = '%s'", strtolower($this->input->post('login')));
					$loginExiste = $this->global_model->get_dado('usuario', '1', $where);
					
					if ($loginExiste == 1 && count($dados) == 0)
					{
						$arrDados['outrosErros'] = 'Esse nome de Login já está sendo usado. Informe outro.';
						$acao = '';
					}
					else
					{
						if (count($dados) == 0)
						{
							$chaveAtiv = md5($this->input->post('cpf') . mt_rand(1, 999) . time());
							$senha     = substr($chaveAtiv, 0, 6);

							$arrDadosUser['SENHA']          = md5($senha);
							$arrDadosUser['CHAVE_ATIVACAO'] = $chaveAtiv;
							$arrDadosUser['IDUSUARIOTIPO']  = 6;
							$arrDadosUser['TIPOPESSOA']     = 'PF';
							$arrDadosUser['LOGIN']          = strtolower($this->input->post('login'));
							$arrDadosUser['ATIVO']          = 'N';
						}

						$arrDadosUser['CPF_CNPJ'] = $this->input->post('cpf');

						$arrDadosBiblioteca = Array();
						$arrDadosBiblioteca['IDBIBLIOTECA']    = $this->session->userdata('idUsuario');
						$arrDadosBiblioteca['IDPROGRAMA']      = $this->session->userdata('programa');
						$arrDadosBiblioteca['IDAGENCIA']       = $this->input->post('agenciabb');
						$arrDadosBiblioteca['IDLOGRADOURO']    = $this->input->post('idlogradouro');
						$arrDadosBiblioteca['NOMERESP']        = removeAcentos($this->input->post('nome'), TRUE);
						$arrDadosBiblioteca['RAZAOSOCIAL']     = removeAcentos($this->input->post('nome'), TRUE);
						$arrDadosBiblioteca['TELEFONERESP']    = $this->input->post('telefone');
						$arrDadosBiblioteca['EMAILRESP']       = $this->input->post('email');
						$arrDadosBiblioteca['DATANASC']        = br_to_eua($this->input->post('dataNasc'));
						$arrDadosBiblioteca['NOMEMAE']         = removeAcentos($this->input->post('nomemae'), TRUE);
						$arrDadosBiblioteca['END_NUMERO']      = $this->input->post('end_numero');
						$arrDadosBiblioteca['END_COMPLEMENTO'] = $this->input->post('end_complemento');
						$arrDadosBiblioteca['DATA_UPD']        = date('Y/m/d H:i:s');

						$this->db->trans_start();

						if (count($dados) == 0)
						{
							// insere na tabela usuario
							$this->global_model->insert('usuario', $arrDadosUser);
							$idRespFinan = $this->global_model->get_insert_id();

							// insere na tabela cadbibliotecafinan
							$arrDadosBiblioteca['IDUSUARIO'] = $idRespFinan;
							$this->global_model->insert('cadbibliotecafinan', $arrDadosBiblioteca);

							$opLog   = 'INSERT';
							$descLog = 'Cadastro de responsável financeiro';
						}
						else
						{
							if($arrDados['boolPeriodoEdicao'])
							{
								$where = sprintf("IDUSUARIO = %s", $dados[0]['IDUSUARIO']);

								// update na tabela usuario
								$this->global_model->update('usuario', $arrDadosUser, $where);

								// update na tabela cadbibliotecafinan
								$this->global_model->update('cadbibliotecafinan', $arrDadosBiblioteca, $where);

								$idRespFinan = $dados[0]['IDUSUARIO'];
								$opLog   = 'UPDATE';
								$descLog = 'Atualização de cadastro do responsável financeiro';
							}
							else
							{
								$arrErro['msg']      = 'O cadastro não pode ser alterado.';
								$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
								$this->load->view('erro_view', $arrErro);
								exit;
							}
						}

						$this->db->trans_complete();

						$arr = Array();

						array_push($arr, $arrDadosUser);
						array_push($arr, $arrDadosBiblioteca);

						if ($this->db->trans_status() === FALSE)
						{
							//salva log
							$this->load->library('logsis', Array($opLog, 'ERRO', 'cadbibliotecafinan', $idRespFinan, $descLog, var_export($arr, TRUE)));

							$arrErro = Array();
							$arrErro['msg']      = 'Ocorreu um erro ao salvar o cadastro.';
							$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
							$this->load->view('erro_view', $arrErro);
						}
						else
						{
							//salva log
							$this->load->library('logsis', Array($opLog, 'OK', 'cadbibliotecafinan', $idRespFinan, $descLog, var_export($arr, TRUE)));

							if (count($dados) == 0)
							{
								//enviar e-mail para ativação do cadastro
								$this->load->library('email');

								/* INICIO ENVIO LOCALHOST -> deixar comentado em produção */
								$config = Array();
								$config['protocol']     = 'smtp';
								$config['smtp_host']    = '10.10.1.1';
								$config['smtp_port']    = '25';
								$config['smtp_timeout'] = '30';
								$config['smtp_user']    = @EMAIL_FROM;
								$config['smtp_pass']    = @EMAIL_PASSWORD;
								$config['newline']      = "\r\n";

								$this->email->initialize($config);
								/* FIM ENVIO LOCALHOST */

								$msgEmail = "A Fundação Biblioteca Nacional agradece o seu cadastramento, seus dados de acesso são: \n\nLogin: " . $this->input->post('login') . " \nSenha: " . $senha . " \n\nATENÇÃO!\nATENÇÃO!\n\n Para conseguir acessar o sistema é necessário que seja feita a validação do seu cadastro. Valide seu cadastro acessando o endereço: " . URL_EXEC . "autocad/valida/" . $chaveAtiv . " \n\nAtenciosamente, \nBiblioteca Nacional";

								$this->email->from(EMAIL_FROM, EMAIL_FROM_NAME);
								$this->email->to($this->input->post('email'));
								$this->email->subject('Validação de Cadastro');
								$this->email->message($msgEmail);

								if ($this->email->send())
								{
									$arrSucesso['msg']      = 'Cadastro realizado com sucesso!<br />Em breve o responsável financeiro receberá um e-mail para validação do seu cadastro. Foi enviado para o e-mail: ' . $this->input->post('email') . '.';
									$arrSucesso['link']     = 'home';
									$arrSucesso['txtlink']  = 'Ir para Home';
									$arrSucesso['tipoUser'] = $this->session->userdata('tipoUsuario');
									$this->load->view('sucesso_view', $arrSucesso);
								}
								else
								{
									$arrErro['msg']      = 'O cadastro foi salvo com sucesso, porém não conseguimos enviar o e-mail de validação. <br />Por favor, entre em contato com a Biblioteca Nacional para a ativação do cadastro.';
									$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
									$this->load->view('erro_view', $arrErro);
								}
							}
							else
							{
								$arrSucesso['msg']      = 'Cadastro atualizado com sucesso!<br /> ';
								$arrSucesso['link']     = 'home';
								$arrSucesso['txtlink']  = 'Ir para Home';
								$arrSucesso['tipoUser'] = $this->session->userdata('tipoUsuario');
								$this->load->view('sucesso_view', $arrSucesso);
							}
						}
					}
				}
				else
				{
					$acao = '';
				}
			}
			
			// informações zeradas para nao dar erro no parse
			$arrDados['programa']         = '';
			$arrDados['cpf']              = '';
			$arrDados['nome']             = '';
			$arrDados['telefone']         = '';
			$arrDados['email']            = '';
			$arrDados['email2']           = '';
			$arrDados['login']            = '';
			$arrDados['nomemae']          = '';
			$arrDados['dataNasc']         = '';
			$arrDados['agenciabb']        = '';
			$arrDados['agenciabb_nome']   = '';
			$arrDados['idlogradouro']     = '';
			$arrDados['cep']              = '';
			$arrDados['uf']               = '';
			$arrDados['cidade']           = '';
			$arrDados['bairro']           = '';
			$arrDados['logradouro']       = '';
			$arrDados['logcomplemento']   = '';
			$arrDados['end_numero']       = '';
			$arrDados['end_complemento']  = '';
			
			if (count($dados) == 1)
			{
				$arrDados['isFinanceiro'] = ($this->session->userdata('idFinanceiro') == 0) ? false : true;
				
				$arrDados['cpf']                    = $dados[0]['CPF_CNPJ'];
				$arrDados['nome']                   = $dados[0]['NOMERESP'];
				$arrDados['telefone']               = $dados[0]['TELEFONERESP'];
				$arrDados['email']                  = $dados[0]['EMAILRESP'];
				$arrDados['email2']                 = $dados[0]['EMAILRESP'];
				$arrDados['login']                  = $dados[0]['LOGIN'];
				$arrDados['nomemae']                = $dados[0]['NOMEMAE'];
				$arrDados['dataNasc']               = eua_to_br($dados[0]['DATANASC']);
				$arrDados['agenciabb']              = $dados[0]['IDAGENCIA'];
				$arrDados['agenciabb_nome']         = $this->biblioteca_model->getNomeAgencia($dados[0]['IDAGENCIA']);
				
				$arrDados['idlogradouro']           = $dados[0]['IDLOGRADOURO'];
				$arrDados['cep']                    = $dados[0]['CEP'];
				$arrDados['uf']                     = $dados[0]['IDUF'];
				$arrDados['cidade']                 = $dados[0]['NOMECIDADESUB'];
				$arrDados['bairro']                 = $dados[0]['NOMEBAIRRO'];
				$arrDados['logradouro']             = $dados[0]['NOMELOGRADOURO'];
				$arrDados['logcomplemento']         = $dados[0]['NOMELOGRADOURO2'];
				$arrDados['end_numero']             = $dados[0]['END_NUMERO'];
				$arrDados['end_complemento']        = $dados[0]['END_COMPLEMENTO'];

				$acao = '';
			}
			elseif (count($dados) > 1)
			{
				$acao = 'edit';
				$arrErro = Array();
				$arrErro['msg']      = 'Responsável financeiro inválido.';
				$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
				$this->load->view('erro_view', $arrErro);
			}
			
			if ($acao == '')
			{			
				$this->parser->parse('biblioteca/formcad_view', $arrDados);
			}
		}
	}
	
	function comite()
	{
		$this->load->library('session');
		$this->load->model('biblioteca_model');
		
		// Só libera tela de comitê caso o id programa esteja setado
		if($this->session->userdata('programa') == 0)
		{
			$this->load->view('selecionar_edital');
		} 
		else{
			$arrDados = Array();
			$arrDados['comite'] = $this->biblioteca_model->listaComite($this->session->userdata('idUsuario'), $this->session->userdata('programa'));
			$tamArr = count($arrDados['comite']);
			$arrDados['vazio'] = ($tamArr == 0 ? TRUE : FALSE);
			$arrDados['comite'] = add_corLinha($arrDados['comite'], 1);
			$this->parser->parse('biblioteca/comite_lista_view', $arrDados);
		}
	}
	
	function comiteform($acao = '', $idMembro = 0)
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('global_model');
		$this->load->model('biblioteca_model');
        $this->load->model('configuracao_model');
		
		if ( ! is_numeric($idMembro) || $idMembro < 0)
		{
			$arrErro = Array();
            $arrErro['msg']      = 'Membro inválido.';
            $arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
            $this->load->view('erro_view', $arrErro);
		}
		else
		{
			$dados    = Array();
			$dadosOk  = TRUE;
			$msgNaoOk = '';
			
			if ($idMembro != 0)
			{
				$dados = $this->biblioteca_model->getDadosMembroComite($this->session->userdata('idUsuario'), $idMembro);
				
				if (count($dados) == 0)
				{
					$dadosOk  = FALSE;
					$msgNaoOk = 'Membro inválido.';
				}
			}
			else
			{
				//valida quantidade máxima
				$qtdMembros = $this->biblioteca_model->validaNewMembro($this->session->userdata('idUsuario'));
				
				if ($qtdMembros >= 5)
				{
					$dadosOk  = FALSE;
					$msgNaoOk = 'Limite de membros do Comitê de Acervo atingido.';
				}
			}
			
			if ($dadosOk)
			{
				$arrDados = Array();
				$arrDados['idMembro'] = $idMembro;
				
				if ($acao == 'save')
				{
					$this->form_validation->set_rules('cpf',             'CPF',                     'trim|required|max_length[14]|valida_cnpj_cpf|xss_clean');
					$this->form_validation->set_rules('nome',            'Nome',                    'trim|required|max_length[100]|xss_clean');
					$this->form_validation->set_rules('telefone',        'Telefone',                'trim|required|max_length[15]|xss_clean');
					$this->form_validation->set_rules('email',           'E-mail',                  'trim|required|max_length[100]|valid_email');
					$this->form_validation->set_rules('email2',          'E-mail',                  'trim|required|max_length[100]|valid_email');
					$this->form_validation->set_rules('login',           'Login',                   'trim|required|min_length[6]|max_length[20]|alpha_numeric|xss_clean');
					$this->form_validation->set_rules('nomemae',         'Nome da Mãe',             'trim|required|max_length[100]|xss_clean');
					$this->form_validation->set_rules('dataNasc',        'Data de Nascimento',      'trim|required|max_length[10]|valida_data|xss_clean');
					
					$this->form_validation->set_rules('idlogradouro',    'Endereço',                'is_numeric');
					$this->form_validation->set_rules('cep',             '',                        '');
					$this->form_validation->set_rules('uf',              '',                        '');
					$this->form_validation->set_rules('cidade',          '',                        '');
					$this->form_validation->set_rules('bairro',          '',                        '');
					$this->form_validation->set_rules('logradouro',      '',                        '');
					$this->form_validation->set_rules('logcomplemento',  '',                        '');
					$this->form_validation->set_rules('end_numero',      'Endereço - Número',       'trim|required|max_length[8]|xss_clean');
					$this->form_validation->set_rules('end_complemento', 'Complemento',             'trim|max_length[30]|xss_clean');
					
					if ($this->form_validation->run() == TRUE)
					{
						$arrDadosUser = Array();
						
						$where       = sprintf("LOGIN = '%s'", strtolower($this->input->post('login')));
						$loginExiste = $this->global_model->get_dado('usuario', '1', $where);
						
						$cpfLiberado = $this->biblioteca_model->validaCpfComite($this->session->userdata('idUsuario'), $idMembro, $this->input->post('cpf'));
						
						if ($loginExiste == 1 && count($dados) == 0)
						{
							$arrDados['outrosErros'] = 'Esse nome de Login já está sendo usado. Informe outro.';
							$acao = '';
						}
						elseif ( ! $cpfLiberado)
						{
							$arrDados['outrosErros'] = 'CPF já cadastrado como membro do Comitê de Acervo.';
							$acao = '';
						}
						else
						{
							if (count($dados) == 0)
							{
								$chaveAtiv = md5($this->input->post('cpf') . mt_rand(1, 999) . time());
								$senha     = substr($chaveAtiv, 0, 6);

								$arrDadosUser['SENHA']          = md5($senha);
								$arrDadosUser['CHAVE_ATIVACAO'] = $chaveAtiv;
								$arrDadosUser['IDUSUARIOTIPO']  = 7;
								$arrDadosUser['TIPOPESSOA']     = 'PF';
								$arrDadosUser['LOGIN']          = strtolower($this->input->post('login'));
								$arrDadosUser['ATIVO']          = 'N';
							}

							$arrDadosUser['CPF_CNPJ'] = $this->input->post('cpf');

							$arrDadosBiblioteca = Array();
							$arrDadosBiblioteca['IDBIBLIOTECA']    = $this->session->userdata('idUsuario');
							$arrDadosBiblioteca['IDPROGRAMA']      = $this->session->userdata('programa');
							$arrDadosBiblioteca['IDLOGRADOURO']    = $this->input->post('idlogradouro');
							$arrDadosBiblioteca['NOMERESP']        = removeAcentos($this->input->post('nome'), TRUE);
							$arrDadosBiblioteca['RAZAOSOCIAL']     = removeAcentos($this->input->post('nome'), TRUE);
							$arrDadosBiblioteca['TELEFONERESP']    = $this->input->post('telefone');
							$arrDadosBiblioteca['EMAILRESP']       = $this->input->post('email');
							$arrDadosBiblioteca['DATANASC']        = br_to_eua($this->input->post('dataNasc'));
							$arrDadosBiblioteca['NOMEMAE']         = removeAcentos($this->input->post('nomemae'), TRUE);
							$arrDadosBiblioteca['END_NUMERO']      = $this->input->post('end_numero');
							$arrDadosBiblioteca['END_COMPLEMENTO'] = $this->input->post('end_complemento');
							$arrDadosBiblioteca['DATA_UPD']        = date('Y/m/d H:i:s');

							$this->db->trans_start();

							if (count($dados) == 0)
							{
								// insere na tabela usuario
								$this->global_model->insert('usuario', $arrDadosUser);
								$idNewMembro = $this->global_model->get_insert_id();
								
								// insere na tabela cadbibliotecacomite
								$arrDadosBiblioteca['IDUSUARIO'] = $idNewMembro;
								$this->global_model->insert('cadbibliotecacomite', $arrDadosBiblioteca);

								$opLog   = 'INSERT';
								$descLog = 'Cadastro de membro do comitê de acervo';
							}
							else
							{
								$where = sprintf("IDUSUARIO = %s", $dados[0]['IDUSUARIO']);
								
								// update na tabela usuario
								$this->global_model->update('usuario', $arrDadosUser, $where);
								
								// update na tabela cadbibliotecacomite
								$this->global_model->update('cadbibliotecacomite', $arrDadosBiblioteca, $where);
								
								$idNewMembro = $dados[0]['IDUSUARIO'];
								$opLog   = 'UPDATE';
								$descLog = 'Atualização de cadastro de membro do comitê de acervo';
							}
							
							$this->db->trans_complete();

							$arr = Array();

							array_push($arr, $arrDadosUser);
							array_push($arr, $arrDadosBiblioteca);

							if ($this->db->trans_status() === FALSE)
							{
								//salva log
								$this->load->library('logsis', Array($opLog, 'ERRO', 'cadbibliotecacomite', $idNewMembro, $descLog, var_export($arr, TRUE)));

								$arrErro = Array();
								$arrErro['msg']      = 'Ocorreu um erro ao salvar o cadastro.';
								$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
								$this->load->view('erro_view', $arrErro);
							}
							else
							{
								//salva log
								$this->load->library('logsis', Array($opLog, 'OK', 'cadbibliotecacomite', $idNewMembro, $descLog, var_export($arr, TRUE)));

								if (count($dados) == 0)
								{
									//enviar e-mail para ativação do cadastro
									$this->load->library('email');

									/* INICIO ENVIO LOCALHOST -> deixar comentado em produção */
									$config = Array();
									$config['protocol']     = 'smtp';
									$config['smtp_host']    = '10.10.1.1';
									$config['smtp_port']    = '25';
									$config['smtp_timeout'] = '30';
									$config['smtp_user']    = @EMAIL_FROM;
									$config['smtp_pass']    = @EMAIL_PASSWORD;
									$config['newline']      = "\r\n";

									$this->email->initialize($config);
									/* FIM ENVIO LOCALHOST */

									$msgEmail = "A Fundação Biblioteca Nacional agradece o seu cadastramento, seus dados de acesso são: \n\nLogin: " . $this->input->post('login') . " \nSenha: " . $senha . " \n\nATENÇÃO!\nATENÇÃO!\n\n Para conseguir acessar o sistema é necessário que seja feita a validação do seu cadastro. Valide seu cadastro acessando o endereço: " . URL_EXEC . "autocad/valida/" . $chaveAtiv . " \n\nAtenciosamente, \nBiblioteca Nacional";

									$this->email->from(EMAIL_FROM, EMAIL_FROM_NAME);
									$this->email->to($this->input->post('email'));
									$this->email->subject('Validação de Cadastro');
									$this->email->message($msgEmail);

									if ($this->email->send())
									{
										$arrSucesso['msg']      = 'Cadastro realizado com sucesso!<br />Em breve o novo membro do comitê receberá um e-mail para validação do seu cadastro. Foi enviado para o e-mail: ' . $this->input->post('email') . '.';
										$arrSucesso['link']     = 'biblioteca/comite';
										$arrSucesso['txtlink']  = 'Listar Comitê';
										$arrSucesso['tipoUser'] = $this->session->userdata('tipoUsuario');
										$this->load->view('sucesso_view', $arrSucesso);
									}
									else
									{
										$arrErro['msg']      = 'O cadastro foi salvo com sucesso, porém não conseguimos enviar o e-mail de validação. <br />Por favor, entre em contato com a Biblioteca Nacional para a ativação do cadastro.';
										$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
										$this->load->view('erro_view', $arrErro);
									}
								}
								else
								{
									$arrSucesso['msg']      = 'Cadastro atualizado com sucesso!<br /> ';
									$arrSucesso['link']     = 'biblioteca/comite';
									$arrSucesso['txtlink']  = 'Listar Comitê';
									$arrSucesso['tipoUser'] = $this->session->userdata('tipoUsuario');
									$this->load->view('sucesso_view', $arrSucesso);
								}
							}
						}
					}
					else
					{
						$acao = '';
					}
				}
				else
				{
					$acao = '';
					
					if (count($dados) == 1)
					{
						$arrDados['cpf']                    = $dados[0]['CPF_CNPJ'];
						$arrDados['nome']                   = $dados[0]['NOMERESP'];
						$arrDados['telefone']               = $dados[0]['TELEFONERESP'];
						$arrDados['email']                  = $dados[0]['EMAILRESP'];
						$arrDados['email2']                 = $dados[0]['EMAILRESP'];
						$arrDados['login']                  = $dados[0]['LOGIN'];
						$arrDados['nomemae']                = $dados[0]['NOMEMAE'];
						$arrDados['dataNasc']               = eua_to_br($dados[0]['DATANASC']);
						
						$arrDados['idlogradouro']           = $dados[0]['IDLOGRADOURO'];
						$arrDados['cep']                    = $dados[0]['CEP'];
						$arrDados['uf']                     = $dados[0]['IDUF'];
						$arrDados['cidade']                 = $dados[0]['NOMECIDADESUB'];
						$arrDados['bairro']                 = $dados[0]['NOMEBAIRRO'];
						$arrDados['logradouro']             = $dados[0]['NOMELOGRADOURO'];
						$arrDados['logcomplemento']         = $dados[0]['NOMELOGRADOURO2'];
						$arrDados['end_numero']             = $dados[0]['END_NUMERO'];
						$arrDados['end_complemento']        = $dados[0]['END_COMPLEMENTO'];
					}
				}
				
				if (count($dados) == 0)
				{
					//informações zeradas para nao dar erro no parse
					$arrDados['cpf']              = '';
					$arrDados['nome']             = '';
					$arrDados['telefone']         = '';
					$arrDados['email']            = '';
					$arrDados['email2']           = '';
					$arrDados['login']            = '';
					$arrDados['nomemae']          = '';
					$arrDados['dataNasc']         = '';
					
					$arrDados['idlogradouro']     = '';
					$arrDados['cep']              = '';
					$arrDados['uf']               = '';
					$arrDados['cidade']           = '';
					$arrDados['bairro']           = '';
					$arrDados['logradouro']       = '';
					$arrDados['logcomplemento']   = '';
					$arrDados['end_numero']       = '';
					$arrDados['end_complemento']  = '';
				}
				
				if ($acao == '')
				{			
					$this->parser->parse('biblioteca/formcad_comite_view', $arrDados);
				}
			}
			else
			{
				$arrErro = Array();
				$arrErro['msg']      = $msgNaoOk;
				$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
				$this->load->view('erro_view', $arrErro);
			}
		}
	}
	
	function comitedel($idMembro = 0)
	{
		$this->load->model('global_model');
		$this->load->model('biblioteca_model');
		
		if ( ! is_numeric($idMembro) || $idMembro <= 0)
		{
			$arrErro = Array();
            $arrErro['msg']      = 'Membro inválido.';
            $arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
            $this->load->view('erro_view', $arrErro);
		}
		else
		{
			$dados = Array();
			
			$dados = $this->biblioteca_model->getDadosMembroComite($this->session->userdata('idUsuario'), $idMembro);
			
			if (count($dados) != 1)
			{
				$arrErro = Array();
				$arrErro['msg']      = 'Membro inválido.';
				$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
				$this->load->view('erro_view', $arrErro);
			}
			else
			{
				$this->db->trans_start();
				
				$arrUpd = Array();
				$arrUpd['DATA_ATIVACAO'] = date('Y/m/d H:i:s');
				$arrUpd['ATIVO'] = 'N';
				
				$where = sprintf("IDUSUARIO = %s", $idMembro);
				$this->global_model->update('usuario', $arrUpd, $where);
				
				$opLog   = 'DELETE';
				$descLog = 'Exclusão de membro do Comitê de Acervo';
				
				$this->db->trans_complete();
				
				if ($this->db->trans_status() === FALSE)
				{
					//salva log
					$this->load->library('logsis', Array($opLog, 'ERRO', 'usuario', $idMembro, $descLog, var_export($arrUpd, TRUE)));

					$arrErro = Array();
					$arrErro['msg']      = 'Ocorreu um erro ao excluir o cadastro.';
					$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
					$this->load->view('erro_view', $arrErro);
				}
				else
				{
					//salva log
					$this->load->library('logsis', Array($opLog, 'OK', 'usuario', $idMembro, $descLog, var_export($arrUpd, TRUE)));

					$arrSucesso['msg']      = 'Cadastro excluído com sucesso!<br /> ';
					$arrSucesso['link']     = 'biblioteca/comite';
					$arrSucesso['txtlink']  = 'Listar Comitê';
					$arrSucesso['tipoUser'] = $this->session->userdata('tipoUsuario');
					$this->load->view('sucesso_view', $arrSucesso);
				}
			}
		}
	}
	
	function modallistaagenciasbb($idinputreturn = '')
	{
		$optionsUf = '';
		$this->load->model('global_model');
		$this->load->model('biblioteca_model');
		
		// Coleta todos os UFS da tabela agenciasbb
		$ufs = $this->biblioteca_model->getAllUfsAgenciasBB();
		
		// Monta opções de ufs
		foreach($ufs as $uf)
		{
			$optionsUf .= '<option value="' . $uf['UF'] . '">' . $uf['UF'] . '</option>';
		}
		
		// Render do view
		$this->parser->parse('biblioteca/modallistaagenciasbb', array('optionsUf' => $optionsUf, 'idInputReturn' => $idinputreturn));
	}
	
	function getOptionsCidadesAgenciasBB($uf = '')
	{
		$optionsCidade = '';
		$this->load->model('global_model');
		$this->load->model('biblioteca_model');
		
		// Coleta todos os UFS da tabela agenciasbb
		$cidades = $this->biblioteca_model->getAllCidadesUfAgenciasBB($uf);
		
		// Adiciona a primeira linha do option
		$optionsCidade = '<option value="">Selecione...</option>';
		
		// Monta opções de ufs
		foreach($cidades as $cidade)
		{
			$optionsCidade .= '<option value="' . $cidade['MUNICIPIO'] . '">' . $cidade['MUNICIPIO'] . '</option>';
		}
		
		// Utilizado em ajax, joga o conteudo na tela
		echo($optionsCidade);
	}
	
	function getOptionsBairrosAgenciasBB($cidade = '')
	{
		$optionsBairro = '';
		$this->load->model('global_model');
		$this->load->model('biblioteca_model');
		
		// Coleta todos os UFS da tabela agenciasbb
		$bairros = $this->biblioteca_model->getAllBairrosCidadeAgenciasBB(urldecode($cidade));
		
		// Adiciona a primeira linha do option
		$optionsBairro = '<option value="">Selecione...</option>';
		
		// Monta opções de ufs
		foreach($bairros as $bairro)
		{
			$optionsBairro .= '<option value="' . $bairro['BAIRRO'] . '">' . $bairro['BAIRRO'] . '</option>';
		}
		
		// Utilizado em ajax, joga o conteudo na tela
		echo($optionsBairro);
	}
	
	function ajaxGetAgencias($uf = '', $cidade = '', $bairro = '')
	{
		$this->load->model('global_model');
		$this->load->model('biblioteca_model');
		
		// Coleta todos os agencias da tabela agenciasbb
		$agencias = $this->biblioteca_model->getAgencias(urldecode($uf), urldecode($cidade), urldecode($bairro));
		
		$this->parser->parse('biblioteca/modallistagencias', array('agencias' => $agencias));
	}
	
	function associarAgenciaBB()
	{
		$this->load->helper('form');
		$this->load->model('global_model');
		$this->load->model('biblioteca_model');
		
		// seta a agencia para usuario e biblioteca encaminhados
		$this->biblioteca_model->associarAgenciaBB($this->input->post('idagenciabb'), $this->input->post('idbiblioteca'), $this->input->post('idrespfinanceiro'));
		
		// Redireciona para home
		redirect('/home/');
	}
	
	function ajaxGetNomeAgencia($idagencia = '')
	{
		$this->load->model('global_model');
		$this->load->model('biblioteca_model');
		
		// Coleta todos os agencias da tabela agenciasbb
		$agencia = $this->biblioteca_model->getNomeAgencia($idagencia);
		
		// Retorno ajax
		echo($agencia);
	}
	
	function getcidades($uf = '')
	{
		$optionsCidade = '';

		$this->load->model('logradouro_model');
		
		// Coleta todos os UFS da tabela agenciasbb
		$cidades = $this->logradouro_model->getCidades($uf);
		
		// Adiciona a primeira linha do option
		$optionsCidade = '<option value="">-- Selecione uma Cidade --</option>';
		
		// Monta opções de ufs
		foreach($cidades as $cidade)
		{
			$optionsCidade .= '<option value="' . $cidade['NOMECIDADESUB'] . '">' . $cidade['NOMECIDADESUB'] . '</option>';
		}
		
		// Utilizado em ajax, joga o conteudo na tela
		echo($optionsCidade);
	}
	
	function getcidadesbib($uf = '')
	{
		// echo $uf;
		$optionsCidade = '';

		$this->load->model('biblioteca_model');
		
		// Coleta cidades da tabela logradouro que obedecem os critérios de no mínimo 2 bibliotecas
		// $cidades = $this->cidade_model->getCidadesComBibliotecasPai($uf);
		$cidades = $this->biblioteca_model->getCidadesCadbiblioteca($uf);
		
		// Adiciona a primeira linha do option
		$optionsCidade = '<option value="">-- Selecione uma Cidade --</option>';
		
		// Monta opções de ufs
		foreach($cidades as $cidade)
		{
			$optionsCidade .= '<option value="' . $cidade['IDCIDADE'] . '">' . $cidade['NOMECIDADE'] . '</option>';
		}
		
		// Utilizado em ajax, joga o conteudo na tela
		echo($optionsCidade);
	}
	
	function getcidadessniic($uf = '')
	{
		// echo $uf;
		$optionsCidade = '';

		$this->load->model('cidade_model');
		
		// Coleta cidades da tabela logradouro que obedecem os critérios de no mínimo 2 bibliotecas
		// $cidades = $this->cidade_model->getCidadesComBibliotecasPai($uf);
		$cidades = $this->cidade_model->getCidadesSniicCredito($uf);
		
		// Adiciona a primeira linha do option
		$optionsCidade = '<option value="">-- Selecione uma Cidade --</option>';
		
		// Monta opções de ufs
		foreach($cidades as $cidade)
		{
			$optionsCidade .= '<option value="' . $cidade['IDCIDADE'] . '">' . removeAcentos($cidade['ENDER_MUNICIPIO'], true) . '</option>';
		}
		
		// Utilizado em ajax, joga o conteudo na tela
		echo($optionsCidade);
	}
	
	function getcidadesbibunificacao($uf = '')
	{
		// echo $uf;
		$optionsCidade = '';

		$this->load->model('cidade_model');
		
		// Coleta cidades da tabela logradouro que obedecem os critérios de no mínimo 2 bibliotecas
		// $cidades = $this->cidade_model->getCidadesComBibliotecasPai($uf);
		$cidades = $this->cidade_model->getCidadesComBibliotecasPai($uf);
		
		// Adiciona a primeira linha do option
		$optionsCidade = '<option value="">-- Selecione uma Cidade --</option>';
		
		// Monta opções de ufs
		foreach($cidades as $cidade)
		{
			$optionsCidade .= '<option value="' . $cidade['IDCIDADE'] . '">' . $cidade['NOMECIDADE'] . '</option>';
		}
		
		// Utilizado em ajax, joga o conteudo na tela
		echo($optionsCidade);
	}
    
    function getidspaiunificacao($idcidade = '')
	{
		echo $idcidade;
		$optionsBibPai = '';

		$this->load->model('biblioteca_model');
		
		// Coleta cidades da tabela logradouro que obedecem os critérios de no mínimo 2 bibliotecas
		$bibpai = $this->biblioteca_model->getIdsBibliotecasPai(array('CIDADE'=> $idcidade));
		
		// Adiciona a primeira linha do option
		$optionsBibPai = '<option value="">-- Selecione uma Biblioteca Pai --</option>';
		
		// Monta opções de Bib Pai
		foreach($bibpai as $bibpai)
		{
			$optionsBibPai .= '<option value="' . $bibpai['IDUSUARIO'] . '">' . $bibpai['RAZAOSOCIAL'] . '</option>';
		}   
		
		// Utilizado em ajax, joga o conteudo na tela
		echo($optionsBibPai);
	}
	
	//Pagina para edição de dados de biblioteca - Gerenciador Sniic (uso dentro de lightbox)
	function modaleditbiblioteca($nPag = 1, $idBiblioteca)
	{
		$this->load->model('global_model');
		$this->load->model('biblioteca_model');
		$this->load->model('naturezajur_model');
	
		$dadosB = $this->biblioteca_model->getDadosCadastro($idBiblioteca);

		$arrDados['nPag']			 = $nPag;
		$arrDados['idbiblioteca']    = $idBiblioteca;
		$arrDados['tipopessoa2']     = $dadosB[0]['TIPOPESSOA'];
		$arrDados['tipopessoa_aux']  = $dadosB[0]['TIPOPESSOA'];
		$arrDados['cnpjcpf']         = $dadosB[0]['CPF_CNPJ'];
		$arrDados['naturezajur_aux'] = $dadosB[0]['IDNATUREZAJUR'];
		$arrDados['iestadual']       = $dadosB[0]['IESTADUAL'];
		$arrDados['razaosocial']     = $dadosB[0]['RAZAOSOCIAL'];
		$arrDados['nomefantasia']    = $dadosB[0]['NOMEFANTASIA'];
		$arrDados['telefone1']       = $dadosB[0]['TELEFONEGERAL'];
		$arrDados['email']           = $dadosB[0]['EMAILGERAL'];
		$arrDados['site']            = $dadosB[0]['SITE'];
		$arrDados['login']           = $dadosB[0]['LOGIN'];
		$arrDados['idlogradouro']    = $dadosB[0]['IDLOGRADOURO'];
		$arrDados['cep']             = $dadosB[0]['CEP'];
		$arrDados['uf']              = $dadosB[0]['IDUF'];
		$arrDados['cidade']          = $dadosB[0]['NOMECIDADESUB'];
		$arrDados['bairro']          = $dadosB[0]['NOMEBAIRRO'];
		$arrDados['logradouro']      = $dadosB[0]['NOMELOGRADOURO2'];
		$arrDados['logcomplemento']  = $dadosB[0]['COMPLEMENTO'];
		$arrDados['end_numero']      = $dadosB[0]['END_NUMERO'];
		$arrDados['end_complemento'] = $dadosB[0]['END_COMPLEMENTO'];
		$arrDados['nomeresp']        = $dadosB[0]['NOMERESP'];
		$arrDados['telefoneresp']    = $dadosB[0]['TELEFONERESP'];
		$arrDados['skyperesp']       = $dadosB[0]['SKYPERESP'];
		$arrDados['emailresp']       = $dadosB[0]['EMAILRESP'];
		$arrDados['loadDadosEdit']   = 1;		
		
		$arrDados['naturezajur'] = $this->naturezajur_model->getCombo($arrDados['tipopessoa2']);		
		$this->parser->parse('biblioteca/modaleditbiblioteca_view',$arrDados);	
	}
	
	//Pagina para edição de dados de biblioteca - Gerenciador Sniic (uso dentro de lightbox)
	function modalsavebiblioteca($idUsuario = '')
	{
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('global_model');
		$this->load->model('biblioteca_model');
        $this->load->model('configuracao_model');
		$this->load->model('naturezajur_model');
		
		$arrDados = array(null);
		
		if($idUsuario != '')
		{
			$tipopessoa = $this->input->post('tipopessoa');
			$labelAux   = ($tipopessoa == 'PJ' ? 'Razão Social' : 'Nome');
			
			$this->form_validation->set_rules('tipopessoa',      'Tipo Pessoa',             'required');
			$this->form_validation->set_rules('cnpjcpf',         'CNPJ/CPF',                'trim|required|max_length[18]|valida_cnpj_cpf|xss_clean');
			$this->form_validation->set_rules('iestadual',       'Inscrição Estadual',      'trim|max_length[20]|xss_clean');
			$this->form_validation->set_rules('razaosocial',     $labelAux,                 'trim|required|max_length[100]|xss_clean');
			$this->form_validation->set_rules('nomefantasia',    'Nome Fantasia',           'trim|max_length[100]|xss_clean');
			$this->form_validation->set_rules('naturezajur',     'Natureza Jurídica',       'valida_combo|xss_clean');
			$this->form_validation->set_rules('telefone1',       'Telefone Geral',          'trim|required|max_length[15]|xss_clean');
			$this->form_validation->set_rules('email',           'E-mail Geral',            'trim|required|max_length[100]|valid_email');
			$this->form_validation->set_rules('site',            'Site',                    'trim|max_length[100]|xss_clean');
			$this->form_validation->set_rules('login',           'Login',                   '');
			$this->form_validation->set_rules('idlogradouro',    'Endereço',                'is_numeric');
			$this->form_validation->set_rules('cep',             '',                        '');
			$this->form_validation->set_rules('uf',              '',                        '');
			$this->form_validation->set_rules('cidade',          '',                        '');
			$this->form_validation->set_rules('bairro',          '',                        '');
			$this->form_validation->set_rules('logradouro',      '',                        '');
			$this->form_validation->set_rules('logcomplemento',  '',                        '');
			$this->form_validation->set_rules('end_numero',      'Endereço - Número',       'trim|required|max_length[8]');
			$this->form_validation->set_rules('end_complemento', 'Complemento',             'trim|max_length[30]|xss_clean');
			$this->form_validation->set_rules('nomeresp',        'Nome Responsável',        'trim|required|max_length[100]|xss_clean');
			$this->form_validation->set_rules('telefoneresp',    'Telefone do Responsável', 'trim|required|max_length[15]|xss_clean');
			$this->form_validation->set_rules('skyperesp',       'Skype do Responsável',    'trim|max_length[30]|xss_clean');
			$this->form_validation->set_rules('emailresp',       'E-mail do Responsável',   'trim|required|max_length[100]|valid_email');
			$this->form_validation->set_rules('observacao',      'Motivo da Edição', 	    'trim|required|xss_clean');
			
			if ($this->form_validation->run() == TRUE)
			{
				if ($tipopessoa != 'PJ' && $tipopessoa != 'PF')
				{
					$arrDados['outrosErros'] = 'Tipo pessoa inválido.';
					$acao = '';
				}
				elseif ($this->input->post('idlogradouro') == '')
				{
					$arrDados['outrosErros'] = 'Informe o endereço.';
					$acao = '';
				}
				else
				{
					$where = sprintf("IDUSUARIO = %s", $idUsuario);
					
					$this->db->trans_start();
					
					$arrUpd = Array();
					$arrUpd['IDLOGRADOURO']    = ($this->input->post('idlogradouro') != '' ? $this->input->post('idlogradouro') : NULL);
					$arrUpd['IDNATUREZAJUR']   = $this->input->post('naturezajur');
					$arrUpd['IESTADUAL']       = $this->input->post('iestadual');
					$arrUpd['RAZAOSOCIAL']     = removeAcentos($this->input->post('razaosocial'), TRUE);
					$arrUpd['NOMEFANTASIA']    = removeAcentos($this->input->post('nomefantasia'), TRUE);
					$arrUpd['TELEFONEGERAL']   = $this->input->post('telefone1');
					$arrUpd['EMAILGERAL']      = $this->input->post('email');
					$arrUpd['SITE']            = $this->input->post('site');
					$arrUpd['NOMERESP']        = removeAcentos($this->input->post('nomeresp'), TRUE);
					$arrUpd['TELEFONERESP']    = $this->input->post('telefoneresp');
					$arrUpd['EMAILRESP']       = $this->input->post('emailresp');
					$arrUpd['SKYPERESP']       = $this->input->post('skyperesp');
					$arrUpd['END_NUMERO']      = $this->input->post('end_numero');
					$arrUpd['END_COMPLEMENTO'] = removeAcentos($this->input->post('end_complemento'), TRUE);
					$arrUpd['LATITUDE']        = NULL;
					$arrUpd['LONGITUDE']       = NULL;
					$arrUpd['DATA_UPD']        = date('Y/m/d H:i:s');
					
					$this->global_model->update('cadbiblioteca', $arrUpd, $where);
					
					//Dados para a gravação de Log
					$arrLog = Array();
					$arrLog['IDUSUARIO']      = $this->session->userdata('idUsuario');
					$arrLog['IDBIBLIOTECA']   = $idUsuario;
					$arrLog['ORIGEM']         = "CADBIBLIOTECA";
					$arrLog['TIPOLOG']        = "EDIÇÃO";
					$arrLog['MOTIVO']   	  = $this->input->post('observacao');
					
					$this->global_model->insert('log_gerenciamento', $arrLog);
					
					// TESTES
					if($VALOR_SNIIC > $VALOR_BINAC)
					$sql = "UPDATE `binac`.`credito` SET VLRCREDITO = '.$VALOR_SNIIC.' WHERE IDUSUARIO = '.$idUsuario.'";
					
					$this->db->trans_complete();
					
					if ($this->db->trans_status() === FALSE)
					{
						//salva log
						$this->load->library('logsis', Array('UPDATE', 'ERRO', 'cadbiblioteca', $idUsuario, 'Atualização de cadastro de biblioteca', var_export($arrUpd, TRUE)));
						
						$arrErro['msg']      = 'Ocorreu um erro na tentativa de atualizar o cadastro.';
						$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
						$this->load->view('erro_view', $arrErro);
					}
					else
					{
						//geocodificar endereço
						$arrEnd      = $this->biblioteca_model->getDadosCadastro($idUsuario);
						$address     = str_replace(' ','+',$arrEnd[0]['NOMELOGRADOURO2']) . ',+' . str_replace(' ','+',$arrEnd[0]['END_NUMERO']) . ',+' . str_replace(' ','+',$arrEnd[0]['NOMEBAIRRO']) . ',+' . str_replace(' ','+',$arrEnd[0]['NOMECIDADESUB']) . '+' . $arrEnd[0]['IDUF'];
						$request_url = MAPS_HOST . "&address=" . $address;
						
						$arqXml = simplexml_load_file($request_url);
						
						if ($arqXml)
						{
							$status = $arqXml->status;
							
							if ($status == 'OK')
							{
								$dadosLatLong = $arqXml->xpath('/GeocodeResponse/result[1]/geometry/location');
								
								$arrGeoCod = Array();
								$arrGeoCod['LATITUDE']  = $dadosLatLong[0]->lat;
								$arrGeoCod['LONGITUDE'] = $dadosLatLong[0]->lng;
								
								$this->global_model->update('cadbiblioteca', $arrGeoCod, $where);
								
								//aprnas para log
								$arrUpd['LATITUDE']  = $dadosLatLong[0]->lat;
								$arrUpd['LONGITUDE'] = $dadosLatLong[0]->lng;
							}
						}
						
						//salva log
						$this->load->library('logsis', Array('UPDATE', 'OK', 'cadbiblioteca', $idUsuario, 'Atualização de cadastro de biblioteca', var_export($arrUpd, TRUE)));
						
						$arrSucesso['msg']      = 'Cadastro atualizado com sucesso!';
						$arrSucesso['link']     = 'adm/gerenciadorSniic/' . $this->input->post('nPag');
						$arrSucesso['txtlink']  = 'Ir para Gerenciador Sniic';
						$arrSucesso['tipoUser'] = $this->session->userdata('tipoUsuario');
						$this->load->view('sucesso_view', $arrSucesso);
					}
				}
			}else
			{		
				$dadosB = $this->biblioteca_model->getDadosCadastro($idUsuario);

				$arrDados['nPag']    		 = $this->input->post('nPag');
				$arrDados['idbiblioteca']    = $idUsuario;
				$arrDados['tipopessoa2']     = $dadosB[0]['TIPOPESSOA'];
				$arrDados['tipopessoa_aux']  = $dadosB[0]['TIPOPESSOA'];
				$arrDados['cnpjcpf']         = $dadosB[0]['CPF_CNPJ'];
				$arrDados['naturezajur_aux'] = $dadosB[0]['IDNATUREZAJUR'];
				$arrDados['iestadual']       = $dadosB[0]['IESTADUAL'];
				$arrDados['razaosocial']     = $dadosB[0]['RAZAOSOCIAL'];
				$arrDados['nomefantasia']    = $dadosB[0]['NOMEFANTASIA'];
				$arrDados['telefone1']       = $dadosB[0]['TELEFONEGERAL'];
				$arrDados['email']           = $dadosB[0]['EMAILGERAL'];
				$arrDados['site']            = $dadosB[0]['SITE'];
				$arrDados['login']           = $dadosB[0]['LOGIN'];
				$arrDados['idlogradouro']    = $dadosB[0]['IDLOGRADOURO'];
				$arrDados['cep']             = $dadosB[0]['CEP'];
				$arrDados['uf']              = $dadosB[0]['IDUF'];
				$arrDados['cidade']          = $dadosB[0]['NOMECIDADESUB'];
				$arrDados['bairro']          = $dadosB[0]['NOMEBAIRRO'];
				$arrDados['logradouro']      = $dadosB[0]['NOMELOGRADOURO2'];
				$arrDados['logcomplemento']  = $dadosB[0]['COMPLEMENTO'];
				$arrDados['end_numero']      = $dadosB[0]['END_NUMERO'];
				$arrDados['end_complemento'] = $dadosB[0]['END_COMPLEMENTO'];
				$arrDados['nomeresp']        = $dadosB[0]['NOMERESP'];
				$arrDados['telefoneresp']    = $dadosB[0]['TELEFONERESP'];
				$arrDados['skyperesp']       = $dadosB[0]['SKYPERESP'];
				$arrDados['emailresp']       = $dadosB[0]['EMAILRESP'];
				$arrDados['loadDadosEdit']   = 1;		
				
				$arrDados['naturezajur'] = $this->naturezajur_model->getCombo($arrDados['tipopessoa2']);
		
				$this->parser->parse('biblioteca/modaleditbiblioteca_view',$arrDados);	
			}
		}else
		{
			$this->parser->parse('adm/gerenciadorsniic', $arrDados);
		}
	}
	
	function bloquear_cadastro($idbiblioteca = 0)
	{
		$origem = $this->input->post('origem');
		$nPag = $this->input->post('nPag');
		
		$this->load->model('global_model');
		$arrDados = array();
		$arrDados['idbiblioteca'] = $idbiblioteca;
		$arrDados['nPag'] = $nPag;
		
		
		if ($origem == 'ANTIGO') {
			$arrBib = $this->global_model->selectx('sniiconline.sniic_biblioteca', 'ID_BIBLIOTECA = ' . $idbiblioteca, 'ID_BIBLIOTECA');
			
			$arrDados['origem'] = 'SNIIC';
			$arrDados['nome'] = $arrBib[0]['NOME_BIBLIOTECA'];
			$arrDados['cidade'] = $arrBib[0]['ENDER_MUNICIPIO'];
		} else {
			$arrBib = $this->global_model->selectx('cadbiblioteca B INNER JOIN logradouro L ON B.IDLOGRADOURO = L.IDLOGRADOURO INNER JOIN cidade C ON L.IDCIDADE = C.IDCIDADE', 'B.IDUSUARIO = ' . $idbiblioteca, 'B.IDUSUARIO');
			
			$arrDados['origem'] = 'CADBIBLIOTECA';
			$arrDados['nome'] = $arrBib[0]['RAZAOSOCIAL'];
			$arrDados['cidade'] = $arrBib[0]['NOMECIDADE'];
		}
			
		//var_dump($arrBib);
		
		$this->parser->parse('biblioteca/bloqueia_cadastro', $arrDados);
	}
	
	function bloqueia($idbiblioteca = 0)
	{
		$this->load->model('global_model');
		
		$arrLog = Array();
		
		$arrLog['IDUSUARIO']      = $this->session->userdata('idUsuario');
		$arrLog['IDBIBLIOTECA']   = $idbiblioteca;
		$arrLog['ORIGEM']         = $this->input->post('origem');
		$arrLog['TIPOLOG']        = "BLOQUEIO";
		$arrLog['MOTIVO']   	  = $this->input->post('motivo');
					
		$this->global_model->insert('log_gerenciamento', $arrLog);
		
		echo "OK";
	}
	
	function desbloqueia()
	{
		$idbiblioteca	= $this->input->post('idbiblioteca');
		
		$this->load->model('biblioteca_model');
		
		if ($this->biblioteca_model->temBloqueioLogGerenciamento($idbiblioteca, 'CADBIBLIOTECA')) {
			$this->biblioteca_model->removeBloqueioLogGerenciamento($idbiblioteca, 'CADBIBLIOTECA');
			$this->load->library('logsis', Array('DELETE', 'OK', 'log_gerenciamento', $idbiblioteca, 'Exclusão de Bloqueio', var_export($_POST, TRUE)));
		}
		
		if ( $this->biblioteca_model->temBloqueioCreditoRestricao($idbiblioteca)) {
			$this->biblioteca_model->removeCreditoRestricao($idbiblioteca);
			$this->load->library('logsis', Array('DELETE', 'OK', 'creditorestricao', $idbiblioteca, 'Exclusão de Registro', var_export($_POST, TRUE)));
		}
		return true;
	}
	
	function migrar($nPag = 1, $chave = 0, $acao = '')
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('usuario_model');
		$this->load->model('sniic_model');
		$this->load->model('global_model');
		$this->load->model('configuracao_model');
		
		$arrDados = Array();
		$verifica = 0;
		
		if ($acao == 'verifica')
		{
			$verifica = 1;
			
			$this->form_validation->set_rules('chave', 'Chave', 'trim|required|max_length[13]|xss_clean');
			
			if ($this->form_validation->run() == TRUE)
			{
				$arrSniic = $this->sniic_model->getSniicByChave(strtoupper($this->input->post('chave')));
				
				if (count($arrSniic) == 1)
				{
					//valida se registro baseado no sniic já nao foi cadastrado
					$where       = sprintf("IDSNIIC = %s", $arrSniic[0]['ID_BIBLIOTECA']);
					$sniicExiste = $this->global_model->get_dado('cadbiblioteca', '1', $where);
					
					if ($sniicExiste == 1)
					{
						$arrDados['outrosErros'] = 'Cadastro já atualizado. Acesse o sistema com os dados que lhe foram enviados por e-mail.';
						$acao = '';
					}
					else
					{
						//tratar CPF/CNPJ
						$arrRep  = Array('.','-','/',' ');
						$ccAux   = str_replace($arrRep, '', $arrSniic[0]['CPF_CNPJ']);
						$tipoAux = 'PJ';
						
						if (strlen($ccAux) <= 11)
						{
							$ccAux   = masc_cpf($ccAux);
							$tipoAux = 'PF';
						}
						else
						{
							$ccAux   = masc_cnpj($ccAux);
							$tipoAux = 'PJ';
						}
						
						$urlCadastro = 'autocad/biblioteca/new/' . $tipoAux . '/' . str_replace('/', '_', $ccAux) . '/' . $arrSniic[0]['ID_BIBLIOTECA']. '/1/' . $nPag;
						redirect($urlCadastro);
					}
				}
				else
				{
					$arrDados['outrosErros'] = 'Chave inválida.';
					$acao = '';
				}
			}
			else
			{
				$acao = '';
			}
		}
		
		//informações zeradas para nao dar erro no parse
		$arrDados['nPag'] = $nPag;
		$arrDados['chave'] = ($chave != '0' ? $chave : '');
		
		//periodos de cadastro porm tipo de estabelecimento
		$per_biblioteca = $this->configuracao_model->validaConfig('1', date('Y/m/d'));
		
		if ($acao == '')
		{
            if( ! $per_biblioteca)
            {
                $arrErro['msg']      = 'Cadastro não disponível no momento.';
				$arrErro['tipoUser'] = 0;
				$this->load->view('erro_view', $arrErro);
            }
            else
            {
               $this->parser->parse('migrar_view', $arrDados);			   
            }
		}
	}
	
	/*
	 * função que pega os dados da biblioteca para atualizar via ajax no autocad
	 * ao mudar a biblioteca correspondente no cadbiblioteca
	 */
	function getdadosbib() {
		$idusuario = $this->input->post('idbiblioteca');
		
		$this->load->model('biblioteca_model');
		
		$arrDados = $this->biblioteca_model->getDadosCadastro($idusuario);
		
		echo $arrDados[0]['TIPOPESSOA']			. '|' .
			 $arrDados[0]['CPF_CNPJ']			. '|' .
			 $arrDados[0]['IESTADUAL']			. '|' .
			 $arrDados[0]['RAZAOSOCIAL']		. '|' .
			 $arrDados[0]['NOMEFANTASIA']		. '|' .
			 $arrDados[0]['IDNATUREZAJUR']		. '|' .
			 $arrDados[0]['TELEFONEGERAL']		. '|' .
			 $arrDados[0]['EMAILGERAL']			. '|' .
			 $arrDados[0]['SITE']				. '|' .
			 $arrDados[0]['LOGIN']				. '|' .
			 $arrDados[0]['IDLOGRADOURO']		. '|' .
			 $arrDados[0]['CEP']				. '|' .
			 $arrDados[0]['IDUF']				. '|' .
			 $arrDados[0]['NOMECIDADESUB']		. '|' .
			 $arrDados[0]['NOMEBAIRRO']			. '|' .
			 $arrDados[0]['NOMELOGRADOURO2']	. '|' .
			 $arrDados[0]['COMPLEMENTO']		. '|' .
			 $arrDados[0]['END_NUMERO']			. '|' .
			 $arrDados[0]['END_COMPLEMENTO']	. '|' .
			 $arrDados[0]['NOMERESP']			. '|' .
			 $arrDados[0]['TELEFONERESP']		. '|' .
			 $arrDados[0]['SKYPERESP']			. '|' .
			 $arrDados[0]['EMAILRESP'];
			 
		
		//var_dump($arrDados);
		
		//echo "Ok";
	}
	
	
	function associaCidadeSniic() {
		$this->load->model('global_model');
		$this->load->model('biblioteca_model');
		$this->load->model('cidade_model');
		$this->load->model('logradouro_model');
		
		$arrEstados = array(
			'Acre'					=> 'AC',
			'Alagoas'				=> 'AL',
			'Amapá'					=> 'AP',
			'Amazonas'				=> 'AM',
			'Bahia'					=> 'BA',
			'Ceará'					=> 'CE',
			'Distrito Federal'		=> 'DF',
			'Espírito Santo'		=> 'ES',
			'Goiás'					=> 'GO',
			'Maranhão'				=> 'MA',
			'Mato Grosso'			=> 'MT',
			'Mato Grosso do Sul'	=> 'MS',
			'Minas Gerais'			=> 'MG',
			'Pará'					=> 'PA',
			'Paraíba'				=> 'PB',
			'Paraná'				=> 'PR',
			'Pernambuco'			=> 'PE',
			'Piauí'					=> 'PI',
			'Rio de Janeiro'		=> 'RJ',
			'Rio Grande do Norte'	=> 'RN',
			'Rio Grande do Sul'		=> 'RS',
			'Rondônia'				=> 'RO',
			'Roraima'				=> 'RR',
			'Santa Catarina'		=> 'SC',
			'São Paulo'				=> 'SP',
			'Sergipe'				=> 'SE',
			'Tocantins'				=> 'TO'
		);
		
		$arrDados = $this->global_model->selectx('sniiconline.sniic_biblioteca','idcidade IS NOT NULL', 'id_biblioteca');
		
		foreach ($arrDados as $row) {
			$arrUpdate = array();
			if ($idcidade = $this->cidade_model->getIdCidadeByNomeExtensoUF(addslashes(removeAcentos($row['ENDER_MUNICIPIO'], true)), $arrEstados[$row['ENDER_ESTADO']])) {
				$arrUpdate['IDCIDADE'] = $idcidade;
				
				$where = sprintf("ID_BIBLIOTECA = %s", $row['ID_BIBLIOTECA']);
				
				$this->global_model->update('sniiconline.sniic_biblioteca', $arrUpdate, $where);
			}
		}
	}
	
	function alteraemail()
	{
		$tabela		= $this->input->post('tabela');
		$idusuario	= $this->input->post('idusuario');
		$email		= $this->input->post('email');
		
		$this->load->model('biblioteca_model');
		
		$this->biblioteca_model->setEmail($tabela, $idusuario, $email);
		
		$this->load->library('logsis', Array('UPDATE', 'OK', $tabela, $idusuario, 'Alteração de email do responsável', var_export($_POST, TRUE)));
		
		return true;
	}

	function sethabilitado()
	{
		$idusuario	= $this->input->post('idusuario');
		$status		= $this->input->post('status');
		
		$this->load->model('biblioteca_model');
		
		$this->biblioteca_model->setHabilitado($idusuario, $status);
		
		$this->load->library('logsis', Array('UPDATE', 'OK', 'usuario', $idusuario, 'Biblioteca habilitada', var_export($_POST, TRUE)));
		
		return true;
	}

	function removesniic()
	{
		$idusuario	= $this->input->post('idusuario');
		
		$this->load->model('biblioteca_model');
		
		$this->biblioteca_model->resetSniic($idusuario);
		
		$this->load->library('logsis', Array('UPDATE', 'OK', 'cadbiblioteca', $idusuario, 'Removido SNIIC da biblioteca', var_export($_POST, TRUE)));
		
		return true;
	}
	
	function removepdvbiblioteca()
	{
		$idbiblioteca	= $this->input->post('idbiblioteca');
		
		$this->load->model('pdv_model');
		
		$this->pdv_model->removePDVBiblioteca($idbiblioteca, 1);
		
		$this->load->library('logsis', Array('DELETE', 'OK', 'programapdvparc', $idbiblioteca, 'Exclusão de PDV', var_export($_POST, TRUE)));
		
		return true;
	}
	
	/**
	* modal_form_view_biblioteca()
	* Exibe html de formulario padrao de dados da biblioteca.
	* @param integer idusuario
	* @return void
	*/
	function modal_form_view_biblioteca($idusuario = 0)
	{
		// Carrega classes necessárias
		$this->load->model('biblioteca_model');
		$args['dados'] = $this->biblioteca_model->get_dados_biblioteca($idusuario);
		
		// Load view
		$this->parser->parse('biblioteca/modal_form_view_biblioteca', $args);
	}
	
	/**
	* search()
	* Função de entrada/pesquisa do modulo.
	* @return void
	*/
	function search($actual_page = 0)
	{
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Coleta o formulário de filtros 
			$args['filtros'] = $this->input->post();
			
			// Carrega tabela de dados, seta o array de dados e limita a paginação
			$this->load->model('biblioteca_model');
			$this->load->model('cidade_model');
			$this->load->library('array_table');
			$this->array_table->set_id('module_table');
			$this->array_table->set_data($this->biblioteca_model->get_lista_modulo($args['filtros']));
			$this->array_table->set_actual_page($actual_page);
			$this->array_table->set_page_link('biblioteca/search');
			
			// Adiciona colunas à tabela e seta as colunas de exibição
			$this->array_table->add_column('<a href="' . URL_EXEC . 'biblioteca/form_update_biblioteca/{0}/?url=biblioteca/search"><img src="' . URL_IMG . 'icon_edit.png" title="Editar" /></a>');
			$this->array_table->add_column('<a href="javascript:void(0);" onclick="ajax_modal(\'Visualizar Dados\', \'' . URL_EXEC . 'biblioteca/modal_form_view_biblioteca/{0}\', \'' . URL_IMG . 'icon_view.png\', 600, 800);"><img src="' . URL_IMG . 'icon_view.png" title="Visualizar" /></a>');
			$this->array_table->add_column('<a href="' . URL_EXEC . 'biblioteca/painel_controle_adm_biblioteca/{0}"><img src="' . URL_IMG . 'icon_gerenciador.png" title="Painel de Controle Biblioteca" /></a>');
			$this->array_table->add_column('{ATIVO_IMG}', false);
			$this->array_table->set_columns(array('#', 'RAZÃO SOCIAL', 'CPF/CNPJ', 'TELEFONE', 'EMAIL', 'RESPONSÁVEL', 'TEL. RESP.', 'EMAIL RESP.', 'LOGIN'));
			
			// Processa tabela do modulo
			$args['module_table'] = $this->array_table->get_html();
			
			// Monta options de ufs
			$args['options_uf'] = monta_options_array($this->biblioteca_model->get_ufs_bibliotecas(), get_value($args['filtros'], 'c__iduf'));
			$args['options_cidades'] = monta_options_array($this->cidade_model->get_cidades_by_uf(get_value($args['filtros'], 'c__iduf')), get_value($args['filtros'], 'c__idcidade'));
			
			$this->load->view('biblioteca/search', $args);
		} else {
			redirect('/home/');
		}	
	}
	
	/**
	* painel_controle_adm_biblioteca()
	* Tela de controle TOTAL do cadastro da biblioteca, por parte do administrador.
	* @param integer idbiblioteca
	* @return void
	*/
	function painel_controle_adm_biblioteca($idbiblioteca = 0, $idprograma = 0)
	{
		if($this->session->userdata('tipoUsuario') == 1 && $idbiblioteca != 0 && $idbiblioteca != '')
		{
			// Load do models e libraries necessários
			$this->load->library('programa');
			$this->load->model('biblioteca_model');
			
			// Coleta idprograma
			if($idprograma == 0 || $idprograma == '')
			{
				$idprograma = ($this->input->post('idprograma') != '') ? $this->input->post('idprograma') : $this->programa->get_first_programa();
			}
			
			// Coleta dados da biblioteca
			$args['dados'] = $this->biblioteca_model->get_dados_biblioteca($idbiblioteca, $idprograma);
			
			// Coleta pendências desta biblitoeca
			$args['pendencias'] = $this->programa->get_pendencias_biblioteca($idprograma, $idbiblioteca);
			
			// Coleta dados do responsável financeiro e starta tabelas
			$this->load->library('array_table');
			$table_respsf = $this->array_table->get_instance();
			$table_comite = $this->array_table->get_instance();
			$table_pdvpar = $this->array_table->get_instance();
			$table_pedido = $this->array_table->get_instance();
			
			$table_respsf->set_id('responsaveis_financeiros');
			$table_respsf->set_data($this->biblioteca_model->get_responsaveis_financeiros($idbiblioteca, $idprograma));
			$table_respsf->add_column('<a href="' . URL_EXEC . 'resp_financeiro/form_update_resp_financeiro/{0}/?url=biblioteca/painel_controle_adm_biblioteca/' . $idbiblioteca . '/' . $idprograma . '"><img src="' . URL_IMG . 'icon_edit.png" title="Editar" /></a>');
			$table_respsf->add_column('<a href="javascript:void(0);" onclick="ajax_modal(\'Visualizar Dados\', \'' . URL_EXEC . 'resp_financeiro/modal_form_view_resp_financeiro/{0}\', \'' . URL_IMG . 'icon_view.png\', 600, 800);"><img src="' . URL_IMG . 'icon_view.png" title="Visualizar" /></a>');
			$table_respsf->add_column('<a href="javascript:void(0);" onclick="iframe_modal(\'Reenvio de Email de Validação\', \'' . URL_EXEC . 'usuario/reenviar_email_validacao/{0}\', \'' . URL_IMG . 'icon_reenviar_email.png\', 600, 750);"><img src="' . URL_IMG . 'icon_reenviar_email.png" title="Reenviar Email de Validação" /></a>');
			$table_respsf->add_column('<a href="javascript:void(0);" onclick="iframe_modal(\'Reenvio de Email de Atualização de Senha\', \'' . URL_EXEC . 'usuario/reenviar_email_atualizacao_senha/{0}\', \'' . URL_IMG . 'icon_reenviar_email_senha.png\', 600, 750);"><img src="' . URL_IMG . 'icon_reenviar_email_senha.png" title="Reenviar Email de Atualização de Senha" /></a>');
			$table_respsf->add_column('{CARTAO_IMG}', false);
			$table_respsf->add_column('{ATIVO_IMG}', false);
			$table_respsf->add_column('{HABILITADO_IMG}', false);
			$table_respsf->set_columns(array('#', 'RAZAOSOCIAL', 'TELEFONERESP', 'EMAILRESP', 'AGENCIA', 'LOGIN'));
			$args['table_resp_financ'] = $table_respsf->get_html();
			
			// Coleta dados do comitê de acervo
			$table_comite->set_id('comite_acervo');
			$table_comite->set_data($this->biblioteca_model->get_comite_acervo($idbiblioteca, $idprograma));
			$table_comite->add_column('<a href="' . URL_EXEC . 'comite_acervo/form_update_comite_acervo/{0}/?url=biblioteca/painel_controle_adm_biblioteca/' . $idbiblioteca . '/' . $idprograma . '"><img src="' . URL_IMG . 'icon_edit.png" title="Editar" /></a>');
			$table_comite->add_column('<a href="javascript:void(0);" onclick="ajax_modal(\'Visualizar Dados\', \'' . URL_EXEC . 'comite_acervo/modal_form_view_comite_acervo/{0}\', \'' . URL_IMG . 'icon_view.png\', 600, 800);"><img src="' . URL_IMG . 'icon_view.png" title="Visualizar" /></a>');
			$table_comite->add_column('<a href="javascript:void(0);" onclick="ajax_modal(\'Reenvio de Email de Validação\', \'' . URL_EXEC . 'usuario/reenviar_email_validacao/{0}\', \'' . URL_IMG . 'icon_reenviar_email.png\', 600, 750);"><img src="' . URL_IMG . 'icon_reenviar_email.png" title="Reenviar Email de Validação" /></a>');
			$table_comite->add_column('<a href="javascript:void(0);" onclick="ajax_modal(\'Reenvio de Email de Atualização de Senha\', \'' . URL_EXEC . 'usuario/reenviar_email_atualizacao_senha/{0}\', \'' . URL_IMG . 'icon_reenviar_email_senha.png\', 600, 750);"><img src="' . URL_IMG . 'icon_reenviar_email_senha.png" title="Reenviar Email de Atualização de Senha" /></a>');
			$table_comite->add_column('{ATIVO_IMG}', false);
			$table_comite->add_column('{HABILITADO_IMG}', false);
			$table_comite->set_columns(array('#', 'RAZAOSOCIAL', 'TELEFONERESP', 'EMAILRESP', 'CPF_CNPJ', 'LOGIN'));
			$args['table_comite_acervo'] = $table_comite->get_html();
			
			// Coleta dados do pdv parceiro
			$table_pdvpar->set_id('pdv_parceiro');
			$table_pdvpar->set_data($this->biblioteca_model->get_lista_pdv_parceiro($idbiblioteca, $idprograma));
			$table_pdvpar->add_column('<a href="' . URL_EXEC . 'pdv/form_update_pdv/{0}/?url=biblioteca/painel_controle_adm_biblioteca/' . $idbiblioteca . '/' . $idprograma . '"><img src="' . URL_IMG . 'icon_edit.png" title="Editar" /></a>');
			$table_pdvpar->add_column('<a href="javascript:void(0);" onclick="ajax_modal(\'Visualizar Detalhes\', \'' . URL_EXEC . 'pdv/modal_form_view_pdv/{0}\', \'' . URL_IMG . 'icon_view.png\', 600, 750);"><img src="' . URL_IMG . 'icon_view.png" title="Visualizar" /></a>');
			$table_pdvpar->add_column('<a href="javascript:void(0);" onclick="ajax_modal(\'Reenvio de Email de Validação\', \'' . URL_EXEC . 'usuario/reenviar_email_validacao/{0}\', \'' . URL_IMG . 'icon_reenviar_email.png\', 600, 750);"><img src="' . URL_IMG . 'icon_reenviar_email.png" title="Reenviar Email de Validação" /></a>');
			$table_pdvpar->add_column('<a href="javascript:void(0);" onclick="ajax_modal(\'Reenvio de Email de Atualização de Senha\', \'' . URL_EXEC . 'usuario/reenviar_email_atualizacao_senha/{0}\', \'' . URL_IMG . 'icon_reenviar_email_senha.png\', 600, 750);"><img src="' . URL_IMG . 'icon_reenviar_email_senha.png" title="Reenviar Email de Atualização de Senha" /></a>');
			$table_pdvpar->add_column('{ATIVO_IMG}', false);
			$table_pdvpar->add_column('{HABILITADO_IMG}', false);
			$table_pdvpar->set_columns(array('#', 'RAZAOSOCIAL', 'TELEFONEGERAL', 'EMAILRESP', 'CPF_CNPJ', 'LOGIN'));
			$args['table_pdv_parceiro'] = $table_pdvpar->get_html();
			
			// Coleta dados dos pedidos da biblioteca
			$table_pedido->set_id('pedidos');
			$table_pedido->set_data($this->biblioteca_model->get_lista_pedidos($idbiblioteca, $idprograma));
			$table_pedido->add_column('<a href="' . URL_EXEC . 'pedido/gerencia_administrativa/{0}"><img src="' . URL_IMG . 'icon_controle_pedido.png" title="Gerência" /></a>');
			$table_pedido->add_column('<a href="' . URL_EXEC . 'pedido/conferencia_administrativa_pedido/{0}"><img src="' . URL_IMG . 'icon_conferencia_adm_pedido.png" title="Conferência Administrativa" /></a>');
			$table_pedido->add_column('<a href="' . URL_EXEC . 'pedido/conferencia/{0}"><img src="' . URL_IMG . 'icon_pedido_conferencia.png" title="Conferência" /></a>');
			$table_pedido->add_column('<a href="javascript:void(0)" onclick="iframe_modal(\'Observações do Pedido\', \'' . URL_EXEC . 'pedido/modal_observacoes/{0}\', \'' . URL_IMG . 'icon_observacoes.png\', 600, 750);"><img src="' . URL_IMG . 'icon_observacoes.png" title="Observações do Pedido" /></a>');
			$table_pedido->set_columns(array('#', 'VALORTOTAL', 'QTD_ITENS', 'QTD_ENTREGUE', 'PERC_ATEND', 'CONF_PDV', 'CONF_BIBLIO', 'SITUACAO'));
			$args['table_pedidos'] = $table_pedido->get_html();
			
			// Load do view
			$this->load->view('biblioteca/painel_controle_adm_biblioteca', $args);
		} else 
		{
			redirect('/home');
		}
	}
	
	/**
	* download_excel()
	* Download de excel de Bibliotecas.
	* @param
	* @return void
	*/
	function download_excel() 
	{
		// Carrega classe excel e model pedido
		$this->load->library('excel');
		$this->load->model('biblioteca_model');
		
		// Executa download do excel
		$this->excel->set_file_name('BIBLIOTECA_' . date('Y-m-d_His'));
		$this->excel->set_data($this->biblioteca_model->get_lista_modulo());
		$this->excel->download_file();
	}
	
	/**
	* form_update_biblioteca()
	* Formulário html de edição de bibliotecas.
	* @return void
	*/
	function form_update_biblioteca($idusuario = 0)
	{
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Carrega classes necessárias para utilização
			$this->load->model('biblioteca_model');
			
			// Dados da entidade
			$args['dados'] = $this->biblioteca_model->get_dados_biblioteca($idusuario);
			
			// Processa url_redirect (para redirecionamento do botão aplicar, ok e voltar do form)
			$args['url_redirect'] = ($this->input->get('url') == '') ? 'biblioteca/search' : $this->input->get('url');
			
			// Load da camada view
			$this->load->view('biblioteca/form_update_biblioteca', $args);
		}
	}
	
	/**
	* form_update_biblioteca_proccess()
	* Processa formulário de atualização de cadastro de entidade.
	* @return void
	*/
	function form_update_biblioteca_proccess()
	{
		// Por enquanto, somente administrador poderá acessar esta tela
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Carrega classes necessárias
			$this->load->library('logsis');
			$this->load->model('global_model');
			
			// Array de dados do cadastro de entidade
			$idusuario = $this->input->post('idusuario');
			$arr_dados['iestadual'] 	= $this->input->post('iestadual');
			$arr_dados['razaosocial'] 	= $this->input->post('razaosocial');
			$arr_dados['nomefantasia'] 	= $this->input->post('nomefantasia');
			$arr_dados['emailgeral'] 	= $this->input->post('emailgeral');
			$arr_dados['site'] 			= $this->input->post('site');
			$arr_dados['telefonegeral'] = $this->input->post('telefonegeral');
			$arr_dados['nomeresp'] 		= $this->input->post('nomeresp');
			$arr_dados['emailresp'] 	= $this->input->post('emailresp');
			$arr_dados['telefoneresp'] 	= $this->input->post('telefoneresp');
			$arr_dados['skyperesp'] 	= $this->input->post('skyperesp');
	
			$arr_dados['idlogradouro'] 	= $this->input->post('idlogradouro');
			$arr_dados['end_numero'] 	= $this->input->post('end_numero');
			$arr_dados['end_complemento'] = $this->input->post('end_complemento');			
			$this->global_model->update('cadbiblioteca', $arr_dados, "IDUSUARIO = $idusuario");
			$this->logsis->insereLog(array('UPDATE', 'OK', 'cadbiblioteca', $this->session->userdata('idUsuario'), 'Atualização de cadastro de Biblioteca', var_export($this->input->post(), true)));
			
			// Array de dados do cadastro de usuario
			$arr_usuario['cpf_cnpj']   = $this->input->post('cpf_cnpj');
			$arr_usuario['ativo'] 	   = $this->input->post('ativo');
			$arr_usuario['tipopessoa'] = $this->input->post('tipopessoa');
			
			$this->global_model->update('usuario', $arr_usuario, "IDUSUARIO = $idusuario");
			$this->logsis->insereLog(array('UPDATE', 'OK', 'usuario', $this->session->userdata('idUsuario'), 'Atualização de cadastro de Usuário', var_export($this->input->post(), true)));
			
			// Redirect para formulário
			$action = $this->input->post('redirect_action');
			if($action == 'ok'){ redirect($this->input->post('redirect_ok')); } else {redirect( $this->input->post('redirect_aplicar') . '/?url=' . $this->input->post('redirect_ok')); }
		} else {
			redirect('/home/');
		}
	}
	
	/**
	* lista_comite_acervo()
	* Exibe a lista com os membros do comite de acervo
	* @param integer idusuario
	* @return void
	*/
	function lista_comite_acervo($idusuario = 0, $idprograma = 0)
	{
		// Coleta o formulário de filtros 
		$args['filtros'] = $this->input->post();	
	
		$idusuario = $this->session->userdata('idUsuario');
		$idprograma = $this->session->userdata('programa');
		
		// Carrega tabela de dados, seta o array de dados e limita a paginação
		$this->load->model("biblioteca_model");
		$this->load->library('array_table');
		$this->array_table->set_id('module_table');
		$this->array_table->set_data($this->biblioteca_model->get_comite($idusuario, $idprograma));
		//$this->array_table->set_actual_page($actual_page);
		$this->array_table->set_page_link('biblioteca/lista_comite_acervo');
		$ativo = $this->biblioteca_model->get_comite($idusuario, $idprograma);
	
		// Adiciona colunas à tabela e seta as colunas de exibição
		$this->array_table->add_column('<a href="' . URL_EXEC . 'biblioteca/form_update_biblioteca_comite_acervo/{0}/?url=biblioteca/lista_comite_acervo"><img src="' . URL_IMG . 'icon_edit.png" title="Editar" /></a>');
		$this->array_table->add_column('<a href="javascript:void(0);" onclick="ajax_modal(\'Visualizar Dados\', \'' . URL_EXEC . 'comite_acervo/modal_form_view_comite_acervo/{0}\', \'' . URL_IMG . 'icon_view.png\', 600, 800);"><img src="' . URL_IMG . 'icon_view.png" title="Visualizar" /></a>');
		$this->array_table->add_column('<a href="' . URL_EXEC . 'biblioteca/form_cancela_membro_comite/{0}"><img src="' . URL_IMG . 'icon_delete.png" title="Cancelar" /></a>');
		$this->array_table->add_column('{ATIVO_IMG}', false);
		$this->array_table->set_columns(array('#', 'NOME', 'CPF CNPJ','TELEFONE', 'EMAIL' , 'LOGIN'));
		
		// Processa tabela do modulo
		$args['module_table'] = $this->array_table->get_html();
		
	    // Chama a view search
		$this->load->view("biblioteca/lista_comite_acervo", $args);
	}
	
	/**
	* form_cancela_membro_comite()
	* Exibe html de formulario padrao de edição de dados do comitê de acervo
	* @param integer idusuario
	* @return void
	*/
	function form_cancela_membro_comite($idusuario = 0)
	{
	   // Carrega o model responsável.
	   $this->load->model("biblioteca_model");
			
	   // Dados da entidade
	   $args['dados'] = $this->biblioteca_model->get_dados_comite_acervo($idusuario);
		
	   // Processa url_redirect (para redirecionamento do botão aplicar, ok e voltar do form)
	   $args['url_redirect'] = ($this->input->get('url') == '') ? 'biblioteca/lista_comite_acervo' : $this->input->get('url');		

	   // Chama a camada de visualização.
	   $this->load->view("biblioteca/form_cancela_membro_comite", $args);

	}
	
	/**
     * cancela_membro_comite_proccess()
     * Desativa o usuário do comite de acervo
     * @param 
     * @return void
     */
    function cancela_membro_comite_proccess()
	{
		// somente usuario tipo biblioteca pode realizar o cancelamento 	
		if($this->session->userdata('tipoUsuario') == 2)
		{
			// carrego a model
			$this->load->model('biblioteca_model');
			$this->load->model('global_model');
			
			// Array de dados do cadastro de usuario
			$idusuario = $this->input->post('idusuario');
			$arr_usuario['ativo']      = "N";
			$this->global_model->update('usuario', $arr_usuario, "IDUSUARIO = $idusuario");
			$this->load->library('logsis', Array('UPDATE', 'OK', 'usuario', $this->session->userdata('idUsuario'), 'Cancelamento do Membro do Comitê de Acervo', var_export($this->input->post(), true)));
				
			// Redirect para formulário
			$action = $this->input->post('redirect_action');
			if($action == 'ok'){ redirect($this->input->post('redirect_ok')); } else {redirect( $this->input->post('redirect_aplicar') . '/?url=' . $this->input->post('redirect_ok')); }
		}else
		{		
		  redirect('/home/');		  
		} 
    }
	
	/**
	* form_update_biblioteca_comite_acervo()
	* Exibe html de formulario padrao de edição de dados do comitê de acervo
	* @param integer idusuario
	* @return void
	*/
	function form_update_biblioteca_comite_acervo($idusuario = 0)
	{
		// Somente usuario do tipo biblioteca tem acesso a esta tela 
		if($this->session->userdata('tipoUsuario') == 2)
		{	
		    $this->load->model("biblioteca_model");
				
		    $args['dados'] = $this->biblioteca_model->get_dados_comite_acervo($idusuario);
			
		    // Processa url_redirect (para redirecionamento do botão aplicar, ok e voltar do form)
		    $args['url_redirect'] = ($this->input->get('url') == '') ? 'biblioteca/lista_comite_acervo' : $this->input->get('url');		
		
	        $this->load->view("biblioteca/form_update_biblioteca_comite_acervo", $args);
		  
		} else 
		{
			redirect('/home/');
		}
	}
	
	/**
	* form_update_biblioteca_comite_acervo_proccess()
	* Processa formulário de atualização de cadastro de membro do comite de acervo.
	* @return void
	*/	
	function form_update_biblioteca_comite_acervo_proccess()
	{
			// Carrega global model
			$this->load->model('global_model');

			// Array de dados do cadastro de cadbibliotecacomite
			$idusuario = $this->input->post('idusuario');
			//$arr_dados['razaosocial']  = $this->input->post('razaosocial');
			$arr_dados['nomeresp']        = $this->input->post('nomeresp');
			$arr_dados['nomemae']         = $this->input->post('nomemae');
			$arr_dados['datanasc']        = format_date($this->input->post('datanasc'), true);
			$arr_dados['telefoneresp']    = $this->input->post('telefoneresp');
			$arr_dados['emailresp']       = $this->input->post('emailresp');
			$arr_dados['idlogradouro']    = $this->input->post('idlogradouro');
			$arr_dados['end_numero']      = $this->input->post('end_numero');
			$arr_dados['end_complemento'] = $this->input->post('end_complemento');
			$this->global_model->update('cadbibliotecacomite', $arr_dados, "IDUSUARIO = $idusuario");
			$this->load->library('logsis', Array('UPDATE', 'OK', 'cadbibliotecacomite', $this->session->userdata('idUsuario'), 'Atualização de cadastro do Comite de Acervo', var_export($this->input->post(), true)));
			
			// Array de dados do cadastro de usuario
			$idusuario = $this->input->post('idusuario');
			$arr_usuario['cpf_cnpj']   = $this->input->post('cpf_cnpj');
			//$arr_usuario['ativo']      = $this->input->post('ativo');
			//$arr_usuario['tipopessoa'] = $this->input->post('tipopessoa');
			$sql = $this->global_model->update('usuario', $arr_usuario, "IDUSUARIO = $idusuario");
			$this->load->library('logsis', Array('UPDATE', 'OK', 'usuario', $this->session->userdata('idUsuario'), 'Atualização de cadastro de Usuário', var_export($this->input->post(), true)));
			
			// Redirect para formulário
			$action = $this->input->post('redirect_action');
			if($action == 'ok'){ redirect($this->input->post('redirect_ok')); } else {redirect( $this->input->post('redirect_aplicar') . '/?url=' . $this->input->post('redirect_ok')); }
		
	}
	
	/**
	* lista_responsaveis_financeiros()
	* Exibe a lista com os responsaveis financeiros
	* @param integer idusuario
	* @param integer idprograma
	* @return void
	*/
	function lista_responsaveis_financeiros($idusuario = 0, $idprograma = 0)
	{
		// Coleta o formulário de filtros 
		$args['filtros'] = $this->input->post();	
	
		$idusuario = $this->session->userdata('idUsuario');
		$idprograma = $this->session->userdata('programa');
		
		// Carrega tabela de dados, seta o array de dados e limita a paginação
		$this->load->model("biblioteca_model");
		
		// coleta a quantidade de responsaveis financeiros
		$args['qdt_rf'] = $this->biblioteca_model->get_qtd_rf_by_biblioteca($idusuario);
				
		$this->load->library('array_table');
		$this->array_table->set_id('module_table');
		$this->array_table->set_data($this->biblioteca_model->get_responsaveis_financeiros($idusuario, $idprograma));
		//$this->array_table->set_actual_page($actual_page);
		$this->array_table->set_page_link('biblioteca/lista_responsaveis_financeiros');

		// Adiciona colunas à tabela e seta as colunas de exibição
		$this->array_table->add_column('<a href="' . URL_EXEC . 'biblioteca/form_update_biblioteca_responsavel_financeiro/{0}/?url=biblioteca/lista_responsaveis_financeiros"><img src="' . URL_IMG . 'icon_edit.png" title="Editar" /></a>');
		$this->array_table->add_column('<a href="javascript:void(0);" onclick="ajax_modal(\'Visualizar Dados\', \'' . URL_EXEC . 'resp_financeiro/modal_form_view_resp_financeiro/{0}\/{0}\', \'' . URL_IMG . 'icon_view.png\', 600, 800);"><img src="' . URL_IMG . 'icon_view.png" title="Visualizar" /></a>');
		$this->array_table->add_column('{ATIVO_IMG}', false);
		$this->array_table->add_column('{CARTAO_IMG}', false);
		$this->array_table->set_columns(array('#', 'NOME', 'TELEFONE', 'EMAIL' , 'LOGIN'));
		
		// Processa tabela do modulo
		$args['module_table'] = $this->array_table->get_html();
		
	    // Chama a view search
		$this->load->view("biblioteca/lista_responsaveis_financeiros", $args);
	}
	
	/**
	* form_update_biblioteca_comite_acervo()
	* Exibe html de formulario padrao de edição de dados do comitê de acervo
	* @param integer idusuario
	* @return void
	*/
	function form_update_biblioteca_responsavel_financeiro($idusuario = 0)
	{
		// Por enquanto, somente administrador poderá acessar esta tela
		if($this->session->userdata('tipoUsuario') == 2)
		{	
            // Carrega o model responsável.
			$this->load->model("biblioteca_model");
				
		   // Dados da entidade
		   $args['dados'] = $this->biblioteca_model->get_dados_responsavel_financeiro($idusuario);
			
		   // Processa url_redirect (para redirecionamento do botão aplicar, ok e voltar do form)
		   $args['url_redirect'] = ($this->input->get('url') == '') ? 'biblioteca/lista_responsaveis_financeiros' : $this->input->get('url');		
		
	       // Chama a camada de visualização.
	       $this->load->view("biblioteca/form_update_biblioteca_responsavel_financeiro", $args);
		  
		} else {
		
		  redirect('/home/');
		  
		}
	}
	
	/**
	* form_update_biblioteca_responsavel_financeiro_proccess()
	* Processa formulário de atualização de cadastro do responsavel financeiro.
	* @return void
	*/	
	function form_update_biblioteca_responsavel_financeiro_proccess()
	{
			// Carrega global model
			$this->load->model('global_model');

			// Array de dados do cadastro de cadbibliotecafinan
			$idusuario = $this->input->post('idusuario');
			//$arr_dados['razaosocial']  = $this->input->post('razaosocial');
			$arr_dados['idagencia'] 	  = $this->input->post('idagencia');
			$arr_dados['nomeresp']        = $this->input->post('nomeresp');
			$arr_dados['nomemae']         = $this->input->post('nomemae');
			$arr_dados['datanasc']        = format_date($this->input->post('datanasc'), true);
			$arr_dados['telefoneresp']    = $this->input->post('telefoneresp');
			$arr_dados['emailresp']       = $this->input->post('emailresp');
			$arr_dados['idlogradouro']    = $this->input->post('idlogradouro');
			$arr_dados['end_numero']      = $this->input->post('end_numero');
			$arr_dados['end_complemento'] = $this->input->post('end_complemento');

			$sql = $this->global_model->update('cadbibliotecafinan', $arr_dados, "IDUSUARIO = $idusuario");
			$this->load->library('logsis', Array('UPDATE', 'OK', 'cadbibliotecacomite', $this->session->userdata('idUsuario'), 'Atualização de cadastro do Responsável financeiro', var_export($this->input->post(), true)));
			
			
			// Array de dados do cadastro de usuario
			$idusuario = $this->input->post('idusuario');
			$arr_usuario['cpf_cnpj']   = $this->input->post('cpf_cnpj');
			//$arr_usuario['ativo']      = $this->input->post('ativo');
			//$arr_usuario['tipopessoa'] = $this->input->post('tipopessoa');
			$this->global_model->update('usuario', $arr_usuario, "IDUSUARIO = $idusuario");
			$this->load->library('logsis', Array('UPDATE', 'OK', 'usuario', $this->session->userdata('idUsuario'), 'Atualização de cadastro de Usuário', var_export($this->input->post(), true)));
			
			// Redirect para formulário
			$action = $this->input->post('redirect_action');
			if($action == 'ok'){ redirect($this->input->post('redirect_ok')); } else {redirect( $this->input->post('redirect_aplicar') . '/?url=' . $this->input->post('redirect_ok')); }
	}
	
}
