<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
*
* Classe Adm
*
* Esta classe contém métodos para a abstração da entidade administrador.
* 
* @author		Fernando Alves
* @package		application
* @subpackage	controllers.adm
* @since		2012-07-11
*
*/
class Adm extends CI_Controller {

	/**
	* __construct()
	* @return object
	*/
	function __construct()
	{
		parent::__construct();
		
		$this->load->library('session');
		
		if ($this->session->userdata('acessoSis') == FALSE)
		{
			redirect('/login/');
			exit;
		}
		
		if ($this->session->userdata('tipoUsuario') != 1)
		{
			redirect('/login/');
			exit;
		}
	}
	
	/*************************************************************************
	 *
	 * CONTROLLERS DE ADMINISTRACAO
	 *
	 * listaBiblioteca        : lista bibliotecas
	 * listaDistribuidor      : lista distribuidores
	 * listaEditora           : lista editoras
	 * listaPdv               : lista pontos de venda
	 * listaLivro             : lista livros
	 * listaConfiguracao      : lista configurações para edição
	 * form_configuracao      : salva configurações
	 * logsis                 : pesquisa logs do sistema
	 * setLatLngPdv           : seta latitude e longitude para PDVs
	 * setLatLngBiblioteca    : seta latitude e longitude para Bibliotecas
     * pedidosLivro           : listar pedidos de livros
	 * pedido                 : detalhar pedido
     * bloqueioLivroIndicacao : bloquear os livros de indicados no 4 Edital
	 *
	 *************************************************************************/
	
	function index()
	{
		$arrErro['msg']      = 'Página não existe.';
		$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
		$this->load->view('erro_view', $arrErro);
	}
	
	function nfunc()
	{
		$arrErro['msg']      = 'Esta funcionalidade não está disponível.';
		$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
		$this->load->view('erro_view', $arrErro);
	}

	function painelControleBiblioteca($nPag = 1, $cidade = 0)
	{
		
	}
	
	function painelControleBibliotecaDetalhe($idbiblioteca = "")
	{
		$return = false;
		if ($idbiblioteca != "")
		{
			$this->load->model('biblioteca_model');
			$this->load->model('pedido_model');
			$this->load->model('pdv_model');
			$this->load->model('sniic_model');
			$this->load->model('global_model');
			$this->load->library('session');
			
			$arrDados = Array();
			
			$arrBib = $this->biblioteca_model->getDadosCadastro($idbiblioteca);
			
			$pedido = $this->pedido_model->getIdPedido($arrBib[0]['IDUSUARIO']);
			
			if ($pedido != "") {
				$pedstatus = $this->pedido_model->getStatusPedido($pedido);
			} else {
				$pedstatus = "";
			}
			//echo $arrBib[0]['IDSNIIC'];
			$arrDados['IDBIBLIOTECA']	= $idbiblioteca;
			$arrDados['IDRESP']			= $arrBib[0]['IDUSUARIO'];
			$arrDados['NOME']			= $arrBib[0]['RAZAOSOCIAL'];
			$arrDados['NOMERESP']		= $arrBib[0]['NOMERESP'];
			$arrDados['CPF_CNPJ']		= $arrBib[0]['CPF_CNPJ'];
			$arrDados['LOGIN']			= $arrBib[0]['LOGIN'];
			$arrDados['EMAILRESP']		= $arrBib[0]['EMAILRESP'];
			$arrDados['ATIVO']			= $arrBib[0]['ATIVO'];
			$arrDados['IMGSTATUS']		= $arrBib[0]['ATIVO'] == 'S' ? 'v_peq.png' : 'x.png';
			$arrDados['DESC_STATUS']	= $arrBib[0]['ATIVO'] == 'S' ? 'VALIDADO' : 'NAO VALIDADO';
			$arrDados['HABILITADO']		= $arrBib[0]['HABILITADO'];
			$arrDados['IDPEDIDO']		= $pedido;
			$arrDados['IDPEDIDOSTATUS'] = $pedstatus;
			$arrDados['TEMBLOQUEIO']	= ($this->biblioteca_model->temBloqueioLogGerenciamento($idbiblioteca, 'CADBIBLIOTECA') || 
										   $this->biblioteca_model->temBloqueioCreditoRestricao($idbiblioteca) )  ? '1' : '0';
			$arrDados['TEMPDV']			= $this->pdv_model->hasPdv($idbiblioteca) ? '1' : '0';
			
			$arrDados['SNIIC']				= $this->sniic_model->getSniicByIdSniic($arrBib[0]['IDSNIIC']);
			
			if ($arrBib[0]['IDSNIIC'] > 0) {
				$credSniic3Edital				= $this->sniic_model->getCreditoByIdBiblioteca($arrBib[0]['IDSNIIC'], 1);
				$arrDados['SNIIC'][0]['VLR3ED']	= empty($credSniic3Edital)?'Sem valor':number_format($credSniic3Edital[0]['VLRCREDITO_INICIAL'], 2, ',', '.');
				
				$credSniic4Edital				= $this->sniic_model->getCreditoByIdBiblioteca($arrBib[0]['IDSNIIC'], 2);
				$arrDados['SNIIC'][0]['VLR4ED']	= empty($credSniic4Edital)?'Sem valor':number_format($credSniic4Edital[0]['VLRCREDITO_INICIAL'], 2, ',', '.');
			}
			
			$cred3edital = $this->global_model->selectx('credito', 'idusuario = ' . $idbiblioteca . ' AND idprograma = 1', 'idusuario');

			if (count($cred3edital) > 0) {
				$arrDados['VLR3ED']			= number_format($cred3edital[0]['VLRCREDITO'], 2, ',', '.');
				$arrDados['VLRINI3ED']		= number_format($cred3edital[0]['VLRCREDITO_INICIAL'], 2, ',', '.');
				$arrDados['VLRGASTO3ED']	= number_format($cred3edital[0]['VLRCREDITO_INICIAL'] - $cred3edital[0]['VLRCREDITO'], 2, ',', '.');
			}
			
			$cred4edital = $this->global_model->selectx('credito', 'idusuario = ' . $idbiblioteca . ' AND idprograma = 2', 'idusuario');
			
			if (count($cred4edital) > 0) {
				$arrDados['VLR4ED']			= number_format($cred4edital[0]['VLRCREDITO'], 2, ',', '.');
				$arrDados['VLRINI4ED']		= number_format($cred4edital[0]['VLRCREDITO_INICIAL'], 2, ',', '.');
				$arrDados['VLRGASTO4ED']	= number_format($cred4edital[0]['VLRCREDITO_INICIAL'] - $cred4edital[0]['VLRCREDITO'], 2, ',', '.');
			}
			
			$arrRespFin = $this->biblioteca_model->getDadosCadastroFinan($idbiblioteca); 
			
			$resp = array();
			$i = 0;
			foreach ($arrRespFin as $row) {
				$resp[$i] = array(
					'RF_IDRESP'		=> $row['IDUSUARIO'],
					'RF_NOMERESP'	=> $row['NOMERESP'],
					'RF_CPF_CNPJ'	=> $row['CPF_CNPJ'],
					'RF_LOGIN'		=> $row['LOGIN'],
					'RF_EMAILRESP'	=> $row['EMAILRESP'],
					'RF_ATIVO'		=> $row['ATIVO']
				);
				$resp[$i]['RF_IMGSTATUS']	= $row['ATIVO'] == 'S' ? 'v_peq.png' : 'x.png';
				$resp[$i]['RF_DESC_STATUS'] = $row['ATIVO'] == 'S' ? 'VALIDADO' : 'NÃO VALIDADO';
				$i++;
			}

			$arrDados['RESPFIN'] = $resp;
			
			$arrComite = $this->biblioteca_model->listaComite($idbiblioteca, $this->session->userdata('programa')); 
			$comite = array();
			$i = 0;
			foreach ($arrComite as $row) {
				$comite[$i] = array(
					'CA_IDRESP'			=> $row['IDUSUARIO'],
					'CA_NOMERESP'		=> $row['NOMERESP'],
					'CA_CPF_CNPJ'		=> $row['CPF_CNPJ'],
					'CA_LOGIN'			=> $row['LOGIN'],
					'CA_EMAILRESP'		=> $row['EMAILRESP'],
					'CA_IMGSTATUS'		=> $row['IMGSTATUS'],
					'CA_DESC_STATUS'	=> $row['DESC_STATUS'],
					'CA_ATIVO'			=> $row['ATIVO']
				);
				
				$i++;
			}
			$arrDados['COMITE'] = $comite;
			
			$this->parser->parse('adm/painel_controle_biblioteca_detalhe_view.php', $arrDados);
			$return = true;
		}
		return $return;
	}
	
	
	function gerenciadorSniic($nPag = 1)
	{
		$this->load->model('biblioteca_model');
		$this->load->model('sniic_model');
		$this->load->model('uf_model');
		$this->load->model('logradouro_model');
		$this->load->model('global_model');
		
		$arrDados = Array();
		$arrDados['nPag']      = (is_numeric($nPag) && $nPag > 0 ? $nPag : 1);
		$arrDados['exibir_pp'] = 100;
		$arrDados['limit_de']  = ($arrDados['nPag'] - 1) * $arrDados['exibir_pp'];
		
		// Filtros
		$filtros = (isset($_POST['filtro_razao_nome_fantasia'])) ? $_POST : null;
		$arrDados['filtros'] = $this->biblioteca_model->getArrayFiltrosSniic($filtros);
		$this->biblioteca_model->setFiltro('RAZAOSOCIAL', $arrDados['filtros']['filtro_razao_nome_fantasia']);
		$this->biblioteca_model->setFiltro('NOMEFANTASIA', $arrDados['filtros']['filtro_razao_nome_fantasia']);
		$this->biblioteca_model->setFiltro('CPFCNPJ', $arrDados['filtros']['filtro_cpf_cnpj']);
		$this->biblioteca_model->setFiltro('TIPOCAD', $arrDados['filtros']['filtro_tipo_cad']);
		$this->biblioteca_model->setFiltro('CIDADE', $arrDados['filtros']['filtro_cidade']);
		$this->biblioteca_model->setFiltro('UF', $arrDados['filtros']['filtro_estado']);
		
		// Coleta options de UF
		$arrDados['options_estados'] = montaOptionsArray($this->uf_model->getEstados(), $arrDados['filtros']['filtro_estado']);

		// Coleta options de cidades
		$arrDados['options_cidades'] = montaOptionsArray($this->logradouro_model->getCidadesPesquisa($arrDados['filtros']['filtro_estado']), $arrDados['filtros']['filtro_cidade']);

		//pesquisa
		$arrDados['bibliotecas'] = $this->biblioteca_model->listaSniic($arrDados, FALSE);
		$tamArr                  = count($arrDados['bibliotecas']);
		$arrDados['vazio']       = ($tamArr == 0 ? TRUE : FALSE);
		
		for ($i = 0; $i < $tamArr; $i++)
		{
			$arrDados['bibliotecas'][$i]['NLINHA'] = $i + 1 + $arrDados['limit_de'];
			
			$origem = $arrDados['bibliotecas'][$i]['TIPO_CAD'] == "ANTIGO" ? "SNIIC" : "CADBIBLIOTECA";
			
			$arrDados['bibliotecas'][$i]['CONTBLOQUEIO'] = $this->global_model->get_dado('log_gerenciamento', 'COUNT(*)', 'ORIGEM = \'' . $origem . '\' AND IDBIBLIOTECA = ' . $arrDados['bibliotecas'][$i]['COD']);
		}
		
		$arrDados['bibliotecas'] = add_corLinha($arrDados['bibliotecas'], 1);
		
		//dados da paginação
		$qtdReg = $this->biblioteca_model->listaSniic($arrDados, TRUE);
		$nPaginas = $qtdReg[0]['CONT'] / $arrDados['exibir_pp'];
		
		if ($qtdReg[0]['CONT'] % $arrDados['exibir_pp'] != 0)
		{
			$nPaginas = (int)$nPaginas + 1;
		}
		
		$arrDados['nPaginas'] = $nPaginas;
		$arrDados['qtdReg']   = $qtdReg[0]['CONT'];
		
		$this->parser->parse('adm/gerenciador_sniic_view.php', $arrDados);
	}
	
	function gerenciadorSniicIntegracao($nPag = 1, $cidade = 0)
	{
		$this->load->model('biblioteca_model');
		$this->load->model('sniic_model');
		$this->load->model('uf_model');
		$this->load->model('logradouro_model');
		$this->load->model('cidade_model');
		$this->load->model('global_model');
		
		$arrDados = Array();
		$arrDados['nPag']      = (is_numeric($nPag) && $nPag > 0 ? $nPag : 1);
		$arrDados['exibir_pp'] = 25;
		$arrDados['limit_de']  = ($arrDados['nPag'] - 1) * $arrDados['exibir_pp'];
		
		// Filtros
		$filtros = (isset($_POST['filtro_razao_nome_fantasia'])) ? $_POST : null;
		$arrDados['filtros'] = $this->biblioteca_model->getArrayFiltrosSniic($filtros);
		
		if ($cidade > 0) {
			$arrDados['filtros']['filtro_estado'] = $this->logradouro_model->getUFByCidade($cidade);
			$arrDados['filtros']['filtro_cidade'] = $cidade;
		}

		$this->biblioteca_model->setFiltro('RAZAOSOCIAL', $arrDados['filtros']['filtro_razao_nome_fantasia']);
		$this->biblioteca_model->setFiltro('NOMEFANTASIA', $arrDados['filtros']['filtro_razao_nome_fantasia']);
		$this->biblioteca_model->setFiltro('CPFCNPJ', $arrDados['filtros']['filtro_cpf_cnpj']);
		$this->biblioteca_model->setFiltro('TIPOCAD', $arrDados['filtros']['filtro_tipo_cad']);
		$this->biblioteca_model->setFiltro('CIDADE', $arrDados['filtros']['filtro_cidade']);
		$this->biblioteca_model->setFiltro('UF', $arrDados['filtros']['filtro_estado']);
		
		$arrDados['options_estados'] = montaOptionsArray($this->uf_model->getEstados(), $arrDados['filtros']['filtro_estado']);
			
		// Coleta options de cidades
		$arrDados['options_cidades'] = montaOptionsArray($this->cidade_model->getCidadesSniicCredito($arrDados['filtros']['filtro_estado']), $arrDados['filtros']['filtro_cidade']);
		
		$arrDados['bibliotecas'] = $this->biblioteca_model->listaSniic2($arrDados, FALSE);
		$tamArr                  = count($arrDados['bibliotecas']);
		$arrDados['vazio']       = ($tamArr == 0 ? TRUE : FALSE);

		// busca cidades para montar o botão de próxima
		$dbcidades = $this->cidade_model->getCidadesSniicCredito();

	    $arrCidades = array();
        
        foreach ($dbcidades as $c) {
            array_push($arrCidades, $c['IDCIDADE']);
        }
        
		if (isset($arrDados['filtros']['filtro_cidade'])) {
			while (current($arrCidades) != $arrDados['filtros']['filtro_cidade']) next($arrCidades);	
			
			$arrDados['proxima'] = next($arrCidades);//$arrProx;
			
			// coleta cidade prev
			$arrDados['anterior'] = prev($arrCidades);//$arrProx;
			$arrDados['anterior'] = prev($arrCidades);//$arrProx;
		} else {
			$arrDados['proxima'] = $arrCidades[1];
			$arrDados['anterior'] = $arrCidades[1];
		}
		
		$arrDados['cidade_verificada'] = $this->cidade_model->isVerificadaSniic($arrDados['filtros']['filtro_cidade']);
		
		for ($i = 0; $i < $tamArr; $i++)
		{
			$arrDados['bibliotecas'][$i]['NLINHA'] = $i + 1 + $arrDados['limit_de'];
			
			$origem = $arrDados['bibliotecas'][$i]['TIPO_CAD'] == "ANTIGO" ? "SNIIC" : "CADBIBLIOTECA";
			
			$arrDados['bibliotecas'][$i]['CONTBLOQUEIO'] = $this->global_model->get_dado('log_gerenciamento', 'COUNT(*)', 'ORIGEM = \'' . $origem . '\' AND IDBIBLIOTECA = ' . $arrDados['bibliotecas'][$i]['COD']);
		}
		
		$arrDados['bibliotecas'] = add_corLinha($arrDados['bibliotecas'], 1);
		
		//dados da paginação
		$qtdReg = $this->biblioteca_model->listaSniic2($arrDados, TRUE);
		$nPaginas = $qtdReg[0]['CONT'] / $arrDados['exibir_pp'];
		
		if ($qtdReg[0]['CONT'] % $arrDados['exibir_pp'] != 0)
		{
			$nPaginas = (int)$nPaginas + 1;
		}
		
		$arrDados['nPaginas'] = $nPaginas;
		$arrDados['qtdReg']   = $qtdReg[0]['CONT'];
		
		$this->parser->parse('adm/gerenciador_sniic_integracao_view.php', $arrDados);
	}
    
    
    function gerenciadorCadbiblioteca($nPag = 1, $cidade = 0){
        $this->load->model('biblioteca_model');
		$this->load->model('uf_model');
		$this->load->model('cidade_model');
        $this->load->model('logradouro_model');
		
		$arrDados = Array();
		$arrDados['nPag']      = (is_numeric($nPag) && $nPag > 0 ? $nPag : 1);
		$arrDados['exibir_pp'] = 20;
		$arrDados['limit_de']  = ($arrDados['nPag'] - 1) * $arrDados['exibir_pp'];
		
		// Filtros
		$filtros = (isset($_POST['filtro_estado'])) ? $_POST : null;
		$arrDados['filtros'] = $this->biblioteca_model->getArrayFiltros($filtros);
		
		if ($cidade > 0) {
			$arrDados['filtros']['filtro_estado'] = $this->logradouro_model->getUFByCidade($cidade);
			$arrDados['filtros']['filtro_cidade'] = $cidade;
		}
		
		/*$this->biblioteca_model->setFiltro('RAZAOSOCIAL', $arrDados['filtros']['filtro_razao_nome_fantasia']);
		$this->biblioteca_model->setFiltro('NOMEFANTASIA', $arrDados['filtros']['filtro_razao_nome_fantasia']);
		$this->biblioteca_model->setFiltro('HABILITADO', $arrDados['filtros']['filtro_habilitado']);
		$this->biblioteca_model->setFiltro('ATIVO', $arrDados['filtros']['filtro_status']);*/
        $this->biblioteca_model->setFiltro('CIDADE', $arrDados['filtros']['filtro_cidade']);
		$this->biblioteca_model->setFiltro('UF', $arrDados['filtros']['filtro_estado']);
		
		//echo  $arrDados['filtros']['filtro_estado'];
		// Coleta options de UF
		$arrDados['options_estados'] = montaOptionsArray($this->uf_model->getEstados(), $arrDados['filtros']['filtro_estado']);
        
        // Coleta options de cidades
		// $arrDados['options_cidades'] = ($arrDados['filtros']['filtro_estado'] != '') ? montaOptionsArray($this->cidade_model->getCidadesCadbibliotecaUnificacaoCadastro($arrDados['filtros']['filtro_estado']), $arrDados['filtros']['filtro_cidade']) : '';
		$arrDados['options_cidades'] = ($arrDados['filtros']['filtro_estado'] != '') ? montaOptionsArray($this->cidade_model->getCidadesCadbibliotecaUnificacaoCadastro($arrDados['filtros']['filtro_estado']), $arrDados['filtros']['filtro_cidade']) : '';
		
		// Coleta opção do checkbox de já verificada
		$arrDados['cidade_verificada'] = $this->cidade_model->isVerificada($arrDados['filtros']['filtro_cidade']);
		
		//pesquisa
		$arrDados['bibliotecas'] = $this->biblioteca_model->listaCadbiblioteca($arrDados, FALSE);
		
		// busca cidades para montar o botão de próxima
		$dbcidades = $this->biblioteca_model->getCidadesCadbiblioteca();
		
        $arrCidades = array();
        
        foreach ($dbcidades as $c) {
            array_push($arrCidades, $c['IDCIDADE']);
        }
        
		if (isset($arrDados['filtros']['filtro_cidade'])) {
			while (current($arrCidades) != $arrDados['filtros']['filtro_cidade']) next($arrCidades);	
			
			$arrDados['proxima'] = next($arrCidades);//$arrProx;
			
			// coleta cidade prev
			$arrDados['anterior'] = prev($arrCidades);//$arrProx;
			$arrDados['anterior'] = prev($arrCidades);//$arrProx;
		} else {
			$arrDados['proxima'] = $arrCidades[1];
			$arrDados['anterior'] = $arrCidades[1];
		}
		
		
		$tamArr                  = count($arrDados['bibliotecas']);
		$arrDados['vazio']       = ($tamArr == 0 ? TRUE : FALSE);
		
		for ($i = 0; $i < $tamArr; $i++)
		{
			$arrDados['bibliotecas'][$i]['NLINHA'] = $i + 1 + $arrDados['limit_de'];
		}
		
		$arrDados['bibliotecas'] = add_corLinha($arrDados['bibliotecas'], 1);
		
		//dados da paginação
		$qtdReg = $this->biblioteca_model->listaCadbiblioteca($arrDados, TRUE);

		$nPaginas = $qtdReg[0]['CONT'] / $arrDados['exibir_pp'];
		
		if ($qtdReg[0]['CONT'] % $arrDados['exibir_pp'] != 0)
		{
			$nPaginas = (int)$nPaginas + 1;
		}
		
		$arrDados['nPaginas'] = $nPaginas;
		$arrDados['qtdReg']   = $qtdReg[0]['CONT'];
		$i = 0;
		foreach ($arrDados['bibliotecas'] as $b) {
			// Coleta options de bibliotecas pai
			
			if ($this->biblioteca_model->temFilhos ($b['IDUSUARIO'])) {
				$arrDados['bibliotecas'][$i]['RAZAOSOCIAL'] = '<a href="'. URL_EXEC . 'adm/unificadorcadastros/'.$b['IDUSUARIO'].'"><b>' . $b['RAZAOSOCIAL'] . '</b></a>';
			}
			$arrDados['bibliotecas'][$i]['IDUSUARIOPAI']  = "<select name='idbibliotecapai' id=idbiblotecapai' class='select' style='width: 65px;' onchange='ajaxSetBibliotecaPai(this.value, ".$b['IDUSUARIO'].");'>\n";
			$arrDados['bibliotecas'][$i]['IDUSUARIOPAI'] .= montaOptionsArray($this->biblioteca_model->getBibiotecasSemPaiByCidade($b['IDCIDADEMASTER'], $b['IDUSUARIO']), $b['IDUSUARIOPAI']);
			$arrDados['bibliotecas'][$i]['IDUSUARIOPAI'] .= "</select>";

			$i++;
		}
		
		$this->parser->parse('adm/gerenciador_cadbiblioteca_view', $arrDados);
        
    }
    
    
    function unificadorCadastros($idpai = ''){
        $this->load->model('biblioteca_model');
        $this->load->model('pedido_model');
        $this->load->model('pdv_model');
		$this->load->model('uf_model');
		$this->load->model('cidade_model');
        $this->load->model('logradouro_model');
		
		$arrDados = Array();
		
		// Filtros
		$filtros = (isset($_POST['filtro_bibpai'])) ? $_POST : null;
		$arrDados['filtros'] = $this->biblioteca_model->getArrayFiltros($filtros);

		// Processa o filtro e consulta com base no idpai, aqui faz 
		// os ajustes necessários em relação ao idpai encaminhado
		$idpai = ($idpai != '') ? $idpai : $arrDados['filtros']['filtro_bibpai'];

		$this->biblioteca_model->setFiltro('IDUSUARIO', $idpai);
    	if ($idpai > 0) {
    		$biblioteca = $this->biblioteca_model->getDadosCadastro($idpai);
    		$arrDados['filtros']['filtro_cidade'] = $biblioteca[0]['IDCIDADE'];
			$arrDados['filtros']['filtro_estado'] = $biblioteca[0]['IDUF'];
		}		
		$arrDados['filtros']['filtro_bibpai'] = $idpai;
		// Coleta options de UF
		$arrDados['options_estados'] = montaOptionsArray($this->uf_model->getEstadosComBibliotecasPai(), $arrDados['filtros']['filtro_estado']);
        
        // Coleta options de cidades
		$arrDados['options_cidades'] = ($arrDados['filtros']['filtro_estado'] != '') ? montaOptionsArray($this->cidade_model->getCidadesComBibliotecasPai($arrDados['filtros']['filtro_estado']), $arrDados['filtros']['filtro_cidade']) : '';
		
		// Coleta options de cidades
		$arrDados['options_idspais'] = montaOptionsArray($this->biblioteca_model->getIdsBibliotecasPai(array('CIDADE' => $arrDados['filtros']['filtro_cidade'])), $idpai);
		
        // pesquisa
		$arrDados['bibliotecas'] = $this->biblioteca_model->listaBibliotecasUnificacao();
		$tamArr                  = count($arrDados['bibliotecas']);
		$arrDados['vazio']       = ($tamArr == 0 ? TRUE : FALSE);
		$arrDados['idpai']       = $idpai;
		
		$arrDados['bibliotecas'] = add_corLinha($arrDados['bibliotecas'], 1);
    	
		// busca bibliotecas para montar o botão de próxima
		$dbbib = $this->biblioteca_model->getBibliotecasComFilhas();

	    $arrBib = array();
        
        foreach ($dbbib as $b) {
            array_push($arrBib, $b['IDUSUARIO']);
        }

		if (isset($arrDados['filtros']['filtro_bibpai'])) {

			for($i = 0; $i < count($arrBib); $i++) {

				$anterior = $i==0?$arrBib[$i]:$arrBib[$i-1];
				$atual = $arrBib[$i];
				$proxima = $i==count($arrBib)-1?$arrBib[$i]:$arrBib[$i+1];
				
				if ($arrBib[$i] == $idpai) break;

			}
			
			$arrDados['proxima'] = $proxima;//$arrProx;
			
			// coleta cidade prev
			$arrDados['anterior'] = $anterior;//$arrProx;
		} else {
			$arrDados['proxima'] = $arrBib[1];
			$arrDados['anterior'] = $arrBib[1];
		}
		// Processa o comite de acervo, pedidos e pontos de venda
	
		$arrDados['comite_biblioteca'] = array();
		$ped = 0;
		$i = 0;
		$env = 0;
		$pdv = 0;
		foreach($arrDados['bibliotecas'] as $biblioteca)
		{
			$arrDados['comite_biblioteca'][$biblioteca['IDUSUARIO']] = $this->biblioteca_model->getIdsComiteAcervo($biblioteca['IDUSUARIO']);
			
			$pedido = $this->pedido_model->getIdPedido($biblioteca['IDUSUARIO']);
			
			if ($pedido != "") {
				
				$statusped = $this->pedido_model->getStatusPedido($pedido);
				
				if ($statusped >= 2) {
					$env++;
				}
				$ped++;
				$arrDados['bibliotecas'][$i]['IDPEDIDO']  = $pedido;
				$arrDados['bibliotecas'][$i]['STATUSPED'] = $statusped;
			} else {
				$arrDados['bibliotecas'][$i]['IDPEDIDO']  = '';
				$arrDados['bibliotecas'][$i]['STATUSPED'] = '';
			}
			if ($this->pdv_model->hasPDV($biblioteca['IDUSUARIO'])) $pdv++;
			$i++;
		}
		$arrDados['contpedidos']  = $ped;
		$arrDados['contenviados'] = $env;
		$arrDados['contpdv']  	  = $pdv;
		
		$this->parser->parse('adm/lista_cadBib_view', $arrDados);
    }

	function unificadorcadastros_processa()
	{
		$this->load->model('biblioteca_model');
		$this->load->model('pedido_model');
		$this->load->model('pdv_model');
		$this->load->model('global_model');
		$this->load->library('logsis');
		$this->load->library('session');
		
		$idpai = $this->input->post('IDPAI');
		
		if (!($this->biblioteca_model->temFilhos($this->input->post('IDPAI')))) {
			echo "<script>
					alert('Biblioteca pai já processada ou não tem filhos.');
					document.location.href='".URL_EXEC."adm/unificadorcadastros/$idpai';
			</script>";
			//echo "Biblioteca pai já processada ou não tem filhos";
			die;
		}
		
		$contpedidos  = $this->input->post('CONTPEDIDOS');
		$contenviados = $this->input->post('CONTENVIADOS');

		if ($contenviados >= 2) {
			echo "<script>
					alert('Bibliotecas possuem juntas mais de 1 pedido enviado. Unificação não permitida.');
					document.location.href='".URL_EXEC."adm/unificadorcadastros/$idpai';
			</script>";
			die;
		}
		if ($contpedidos >= 3) {
			echo "<script>
					alert('Bibliotecas possuem juntas mais de 2 pedidos. Unificação não permitida.');
					document.location.href='".URL_EXEC."adm/unificadorcadastros/$idpai';
			</script>";
			die;
		}		

		// processar o credito e o creditoinfo do registro pai, para não dar erro de duplicidade de chave primária mais abaixo
		// primeiro: 3º edital
		if ($this->input->post('CREDITO_3ED') != $idpai) {
			$arrIDCred3 = $this->global_model->selectx("credito", "idprograma = 1 and idusuario = ".$idpai, 'idusuario');
			
			if (count($arrIDCred3) > 0) {
				foreach ($arrIDCred3 as $row0) {
					$where = sprintf('IDPROGRAMA = 1 AND IDUSUARIO = %d', $idpai);

					$this->logsis->insereLog(Array('DELETE', 'OK', 'credito',  $row0['IDUSUARIO'], 'Unificação de Cadastro', var_export($row0, TRUE)));
					$res = $this->global_model->delete('credito', $where);
				}
			}

			$arrIDCredInfo3 = $this->global_model->selectx("creditoinfo", "idprograma = 1 and idusuario = ".$idpai, 'idusuario');
			
			if (count($arrIDCredInfo3) > 0) {
				foreach ($arrIDCredInfo3 as $row0) {
					$where = sprintf('IDPROGRAMA = 1 AND IDUSUARIO = %d', $idpai);
					
					$this->logsis->insereLog(Array('DELETE', 'OK', 'creditoinfo',  $row0['IDUSUARIO'], 'Unificação de Cadastro', var_export($row0, TRUE)));
					$res = $this->global_model->delete('creditoinfo', $where);
				}
			}
		}

		// segundo: 4º edital
		if ($this->input->post('CREDITO_4ED') != $idpai) {
			$arrIDCred4 = $this->global_model->selectx("credito", "idprograma = 2 and idusuario = ".$idpai, 'idusuario');

			if (count($arrIDCred4) > 0) {

				foreach ($arrIDCred4 as $row0) {
					$where = sprintf('IDPROGRAMA = 2 AND IDUSUARIO = %d', $idpai);
					
					$this->logsis->insereLog(Array('DELETE', 'OK', 'credito',  $row0['IDUSUARIO'], 'Unificação de Cadastro', var_export($row0, TRUE)));
					$res = $this->global_model->delete('credito', $where);
				}
			}
			
			$arrIDCredInfo4 = $this->global_model->selectx("creditoinfo", "idprograma = 2 and idusuario = ".$idpai, 'idusuario');
			
			if (count($arrIDCredInfo4) > 0) {

				foreach ($arrIDCredInfo4 as $row0) {
					$where = sprintf('IDPROGRAMA = 2 AND IDUSUARIO = %d', $idpai);
					
					$this->logsis->insereLog(Array('DELETE', 'OK', 'creditoinfo',  $row0['IDUSUARIO'], 'Unificação de Cadastro', var_export($row0, TRUE)));
					$res = $this->global_model->delete('creditoinfo', $where);
				}
			}
		}

		$arrFilhos = $this->biblioteca_model->listaFilhos($idpai);
		$filhos = array();

		foreach ($arrFilhos as $row) {
			
			// grava todas as alterações nas tabelas com relacionamentos em logsis, a começar pela própria
			// primeira tabela: logsis
			$arrIDLogSis = $this->global_model->select_id('logsis', 'idlogsis', 'idusuario = '.$row['IDUSUARIO']);
			
			if (count($arrIDLogSis) > 0) {
				
				$idlogsis = array();
				
				foreach ($arrIDLogSis as $row1) {
					array_push($idlogsis, $row1['id']);
					$this->logsis->insereLog(Array('UPDATE', 'OK', 'logsis',  $row1['id'], 'Unificação de Cadastro', 'Alterado idusuario de '.$row['IDUSUARIO'].' para ' . $idpai));
				}
				
				$stridlogsis = implode(',', $idlogsis);
				$arrUpd = array();
				$where = sprintf('IDLOGSIS IN (%s)', $stridlogsis);
				
				$arrUpd['IDUSUARIO'] = $idpai;

				$this->global_model->update('logsis', $arrUpd, $where);
			}

			// segunda tabela: log_gerenciamento (registros das filhas serão excluídos)
			$arrIDLogGer = $this->global_model->selectx("log_gerenciamento", "idbiblioteca = ".$row['IDUSUARIO']." AND ORIGEM = 'CADBIBLIOTECA'", 'idbiblioteca');
			
			if (count($arrIDLogGer) > 0) {
				
				foreach ($arrIDLogGer as $row2) {
					$this->logsis->insereLog(Array('DELETE', 'OK', 'log_gerenciamento',  $row2['IDBIBLIOTECA'], 'Unificação de Cadastro', var_export($row2, TRUE)));
				}
				
				$where = sprintf("IDBIBLIOTECA = %d AND ORIGEM = 'CADBIBLIOTECA'", $row['IDUSUARIO']);
				$res = $this->global_model->delete('log_gerenciamento', $where);
			}
			
			// terceira tabela: livrocomentario
			$arrIDLivCom = $this->global_model->select_id('livrocomentario', 'idlivrocomentario', 'idusuario = '.$row['IDUSUARIO']);

			if (count($arrIDLivCom) > 0) {
				
				$idlc = array();
				
				foreach ($arrIDLivCom as $row3) {
					array_push($idlc, $row3['id']);
					$this->logsis->insereLog(Array('UPDATE', 'OK', 'livrocomentario',  $row3['id'], 'Unificação de Cadastro', 'Alterado idusuario de '.$row['IDUSUARIO'].' para ' . $idpai));
				}
				
				$stridlc = implode(',', $idlc);
				$arrUpd = array();
				$where = sprintf('IDLIVROCOMENTARIO IN (%s)', $stridlc);
				
				$arrUpd['IDUSUARIO'] = $idpai;

				$this->global_model->update('livrocomentario', $arrUpd, $where);
			}
	
			// quarta tabela: creditorestricao (registros das filhas serão excluídos)
			$arrIDCredRes = $this->global_model->selectx("creditorestricao", "idusuario = ".$row['IDUSUARIO'], 'idusuario');
			
			if (count($arrIDCredRes) > 0) {

				foreach ($arrIDCredRes as $row4) {
					$this->logsis->insereLog(Array('DELETE', 'OK', 'creditorestricao',  $row4['IDUSUARIO'], 'Unificação de Cadastro', var_export($row4, TRUE)));
				}
				
				$where = sprintf('IDUSUARIO = %d', $row['IDUSUARIO']);
				$res = $this->global_model->delete('creditorestricao', $where);
			}

			// quinta tabela: cadbibliotecafinan
			$arrIDRespFin = $this->biblioteca_model->getDadosCadastroFinan($row['IDUSUARIO']);

			if (count($arrIDRespFin) > 0) {
				
				$idrf = array();
				
				foreach ($arrIDRespFin as $row5) {
					array_push($idrf, $row5['IDUSUARIO']);
					
					if ($row5['ATIVO'] == 'S' && $this->input->post('RESPFIN') != $row['IDUSUARIO'])
						$this->logsis->insereLog(Array('UPDATE', 'OK', 'cadbibliotecafinan',  $row5['IDUSUARIO'], 'Unificação de Cadastro', 'Alterado idbiblioteca de '.$row['IDUSUARIO'].' para ' . $idpai . ' e inativado.'));
					else
						$this->logsis->insereLog(Array('UPDATE', 'OK', 'cadbibliotecafinan',  $row5['IDUSUARIO'], 'Unificação de Cadastro', 'Alterado idbiblioteca de '.$row['IDUSUARIO'].' para ' . $idpai ));
				}

				$stridrf = implode(',', $idrf);
				$arrUpd = array();
				$where = sprintf('IDUSUARIO IN (%s)', $stridrf);
				
				$arrUpd['IDBIBLIOTECA'] = $idpai;

				$this->global_model->update('cadbibliotecafinan', $arrUpd, $where);
				
				$arrUpd = array();
				if ($this->input->post('RESPFIN') != $row['IDUSUARIO']) {
					$arrUpd['ATIVO'] = 'N';
					$arrUpd['DATA_ATIVACAO'] = NULL;
					
					$this->global_model->update('usuario', $arrUpd, $where);
				}
			}
				
			// sexta tabela: cadbibliotecacomite
			$arrIDComite = $this->biblioteca_model->listaComiteTodos($row['IDUSUARIO']);

			if (count($arrIDComite) > 0) {
				
				$idcomite = array();
				
				foreach ($arrIDComite as $row6) {
					array_push($idcomite, $row6['IDUSUARIO']);
					
					if ($row6['ATIVO'] == 'S' && $this->input->post('COMITE') != $row['IDUSUARIO'])
						$this->logsis->insereLog(Array('UPDATE', 'OK', 'cadbibliotecacomite',  $row6['IDUSUARIO'], 'Unificação de Cadastro', 'Alterado idbiblioteca de '.$row['IDUSUARIO'].' para ' . $idpai . ' e inativado.'));
					else
						$this->logsis->insereLog(Array('UPDATE', 'OK', 'cadbibliotecacomite',  $row6['IDUSUARIO'], 'Unificação de Cadastro', 'Alterado idbiblioteca de '.$row['IDUSUARIO'].' para ' . $idpai ));
				}

				$stridcomite = implode(',', $idcomite);
				$arrUpd = array();
				$where = sprintf('IDUSUARIO IN (%s)', $stridcomite);
				
				$arrUpd['IDBIBLIOTECA'] = $idpai;

				$this->global_model->update('cadbibliotecacomite', $arrUpd, $where);

				$arrUpd = array();
				if ($this->input->post('COMITE') != $row['IDUSUARIO']) {
					$arrUpd['ATIVO'] = 'N';
					$arrUpd['DATA_ATIVACAO'] = NULL;
					
					$this->global_model->update('usuario', $arrUpd, $where);
				}
				
			}
	
			// sétima tabela: credito (registros das filhas serão excluídos)
			// primeiro: 3º edital
			$arrIDCred3 = $this->global_model->selectx("credito", "idprograma = 1 and idusuario = ".$row['IDUSUARIO'], 'idusuario');
			
			if (count($arrIDCred3) > 0) {

				foreach ($arrIDCred3 as $row7) {
					$where = sprintf('IDPROGRAMA = 1 AND IDUSUARIO = %d', $row['IDUSUARIO']);
					
					if ($this->input->post('CREDITO_3ED') == $row['IDUSUARIO']) {
						$this->logsis->insereLog(Array('UPDATE', 'OK', 'credito',  $row7['IDUSUARIO'], 'Unificação de Cadastro', '3º Edital - Alterado idusuario de '.$row['IDUSUARIO'].' para ' . $idpai ));
						
						$arrUpd = array();
						$arrUpd['IDUSUARIO'] = $idpai;
						$this->global_model->update('credito', $arrUpd, $where);
					} else {
						$this->logsis->insereLog(Array('DELETE', 'OK', 'credito',  $row7['IDUSUARIO'], 'Unificação de Cadastro', var_export($row7, TRUE)));
						
						$res = $this->global_model->delete('credito', $where);
					}
				}
			}
				
			// segundo: 4º edital
			$arrIDCred4 = $this->global_model->selectx("credito", "idprograma = 2 and idusuario = ".$row['IDUSUARIO'], 'idusuario');
			
			if (count($arrIDCred4) > 0) {
				
				foreach ($arrIDCred4 as $row7) {
					$where = sprintf('IDPROGRAMA = 2 AND IDUSUARIO = %d', $row['IDUSUARIO']);

					if ($this->input->post('CREDITO_4ED') == $row['IDUSUARIO']) {
						$this->logsis->insereLog(Array('UPDATE', 'OK', 'credito',  $row7['IDUSUARIO'], 'Unificação de Cadastro', '4º Edital - Alterado idusuario de '.$row['IDUSUARIO'].' para ' . $idpai ));
						
						$arrUpd = array();
						$arrUpd['IDUSUARIO'] = $idpai;
						$this->global_model->update('credito', $arrUpd, $where);
					} else {
						$this->logsis->insereLog(Array('DELETE', 'OK', 'credito',  $row7['IDUSUARIO'], 'Unificação de Cadastro', var_export($row7, TRUE)));
						
						$res = $this->global_model->delete('credito', $where);
					}
				}
			}			

			// oitava tabela: creditoinfo (registros das filhas serão excluídos)
			// primeiro: 3º edital
			$arrIDCredInfo3 = $this->global_model->selectx("creditoinfo", "idprograma = 1 and idusuario = ".$row['IDUSUARIO'], 'idusuario');
			
			if (count($arrIDCredInfo3) > 0) {
				
				foreach ($arrIDCredInfo3 as $row8) {
					$where = sprintf('IDPROGRAMA = 1 AND IDUSUARIO = %d', $row['IDUSUARIO']);
					
					if ($this->input->post('CREDITO_3ED') == $row['IDUSUARIO']) {
						$this->logsis->insereLog(Array('UPDATE', 'OK', 'creditoinfo',  $row8['IDUSUARIO'], 'Unificação de Cadastro', '3º Edital - Alterado idusuario de '.$row['IDUSUARIO'].' para ' . $idpai ));
						
						$arrUpd = array();
						$arrUpd['IDUSUARIO'] = $idpai;
						$this->global_model->update('creditoinfo', $arrUpd, $where);
					} else {
						$this->logsis->insereLog(Array('DELETE', 'OK', 'creditoinfo',  $row8['IDUSUARIO'], 'Unificação de Cadastro', var_export($row8, TRUE)));
						
						$res = $this->global_model->delete('creditoinfo', $where);
					}
				}
			}
			
			// segundo: 4º edital
			$arrIDCredInfo4 = $this->global_model->selectx("creditoinfo", "idprograma = 2 and idusuario = ".$row['IDUSUARIO'], 'idusuario');

			if (count($arrIDCredInfo4) > 0) {
				
				foreach ($arrIDCredInfo4 as $row8) {
					$where = sprintf('IDPROGRAMA = 2 AND IDUSUARIO = %d', $row['IDUSUARIO']);
					
					if ($this->input->post('CREDITO_4ED') == $row['IDUSUARIO']) {
						$this->logsis->insereLog(Array('UPDATE', 'OK', 'creditoinfo',  $row8['IDUSUARIO'], 'Unificação de Cadastro', '4º Edital - Alterado idusuario de '.$row['IDUSUARIO'].' para ' . $idpai ));
						
						$arrUpd = array();
						$arrUpd['IDUSUARIO'] = $idpai;
						$this->global_model->update('credito', $arrUpd, $where);
					} else {
						$this->logsis->insereLog(Array('DELETE', 'OK', 'creditoinfo',  $row8['IDUSUARIO'], 'Unificação de Cadastro', var_export($row8, TRUE)));
						
						$res = $this->global_model->delete('creditoinfo', $where);
					}
				}
			}
			
			// nona tabela: programapdvparc

			if ($this->input->post('CONTPDV') > 1) {
				
				$this->logsis->insereLog(Array('DELETE', 'OK', 'programapdvparc',  $row['IDUSUARIO'], 'Unificação de Cadastro', 'Excluído PDV da biblioteca'));
				$this->pdv_model->removePDVBiblioteca($row['IDUSUARIO']);
			} else {
				if ($this->pdv_model->hasPDV($row['IDUSUARIO'])) {
					$where = sprintf('IDBIBLIOTECA = %d', $row['IDUSUARIO']);
					
					$this->logsis->insereLog(Array('UPDATE', 'OK', 'programapdvparc',  $row['IDUSUARIO'], 'Unificação de Cadastro', 'Alterado idbiblioteca de '.$row['IDUSUARIO'].' para ' . $idpai ));
						
					$arrUpd = array();
					$arrUpd['IDBIBLIOTECA'] = $idpai;
					$this->global_model->update('programapdvparc', $arrUpd, $where);
				}
			}
					
			// guardar filhos para posterior processamento
			array_push($filhos, $row['IDUSUARIO']);
		}
		
		$strfilhos = implode(',', $filhos);

		// processar pedidos, caso existam
		if ($contpedidos > 0) {

			
			// primeiro: verifica se existe pedido para o pai
			$pedidopai = $this->pedido_model->getIdPedido($idpai);
			
			if ($pedidopai == "") { // não tem pedido? então cria.
				$arrPed = Array();
				$arrPed['IDPROGRAMA']     = IDPROGRAMA;
				$arrPed['IDBIBLIOTECA']   = $idpai;
				$arrPed['IDPDV']          = NULL;
				$arrPed['DATA_ULT_ATU']   = date('Y/m/d H:i:s');
				$arrPed['VALORTOTAL']     = 0;
				$arrPed['IDPEDIDOSTATUS'] = 1;

				$this->global_model->insert('pedido', $arrPed);
	
				$pedidopai = $this->global_model->get_insert_id();
			}
	
			$where = sprintf('IDPEDIDO = %d', $pedidopai);
			
			$pedidocred = $this->pedido_model->getIdPedido($this->input->post('CREDITO_3ED'));

			$arrPedidos = $this->global_model->selectx("pedido", "idbiblioteca IN ($strfilhos)", "idpedido");

			foreach ($arrPedidos as $ped) {
				// antes de qualquer coisa, copia pedido para tabela temporária
				$this->pedido_model->copiaPedidosTabelaTemp($ped['IDPEDIDO']);

				if ($ped['IDPEDIDO'] == $pedidocred) { // vai processar o pedido associado ao crédito para o novo

					$bib = $this->pedido_model->getBibliotecaPedido($ped['IDPEDIDO']);

					$wherepai = sprintf('IDPEDIDO = %d', $pedidopai);
					$wherebib = sprintf('IDUSUARIO = %d', $bib);
					$where = sprintf('IDPEDIDO = %d', $ped['IDPEDIDO']);					
					
					// primeiro processa as tabelas relacionadas
					// exclui os valores existentes do pedido receptor
					$arrPedidoLivro = $this->global_model->selectx('pedidolivro', 'idpedido = '.$pedidopai, 'idpedido');

					if (count($arrPedidoLivro) > 0) {
						foreach ($arrPedidoLivro as $row10) {
							$this->logsis->insereLog(Array('DELETE', 'OK', 'pedidolivro',  $ped['IDBIBLIOTECA'], 'Unificação de Cadastro', var_export($row10, TRUE)));
						}
						
						$this->global_model->delete('pedidolivro', $wherepai);
					}
					
					$arrPedidoLog = $this->global_model->selectx('pedidolog', 'idpedido = '.$pedidopai, 'idpedidolog');

					if (count($arrPedidoLog) > 0) {
						foreach ($arrPedidoLog as $row10) {
							$this->logsis->insereLog(Array('DELETE', 'OK', 'pedidolog',  $ped['IDBIBLIOTECA'], 'Unificação de Cadastro', var_export($row10, TRUE)));
						}
						
						$this->global_model->delete('pedidolog', $wherepai);
					}
					
					$arrPedidoRep = $this->global_model->selectx('pedidoreprimido', 'idpedido = '.$pedidopai, 'idpedido');
					
					if (count($arrPedidoRep) > 0) {		
						foreach ($arrPedidoRep as $row10) {
							$this->logsis->insereLog(Array('DELETE', 'OK', 'pedidoreprimido',  $ped['IDBIBLIOTECA'], 'Unificação de Cadastro', var_export($row10, TRUE)));
						}
								
						$this->global_model->delete('pedidoreprimido', $wherepai);
					}
					
					// copia os valores do pedido atual para o pai
					$arrPedidoLivro = $this->global_model->selectx('pedidolivro', 'idpedido = '.$ped['IDPEDIDO'], 'idpedido');
					
					if (count($arrPedidoLivro) > 0) {
						foreach ($arrPedidoLivro as $row10) {
							$this->logsis->insereLog(Array('UPDATE', 'OK', 'pedidolivro',  $idpai, 'Unificação de Cadastro', 'Alterado idpedido de '.$row10['IDPEDIDO'].' para ' . $pedidopai . ' para ' . var_export($row10, TRUE)));
						}
						
						$arrUpd = array();
						$arrUpd['IDPEDIDO'] = $pedidopai;
						
						$this->global_model->update('pedidolivro', $arrUpd, $where);
					}
					
					$arrPedidoLog = $this->global_model->selectx('pedidolog', 'idpedido = '.$ped['IDPEDIDO'], 'idpedidolog');

					if (count($arrPedidoLog) > 0) {
						foreach ($arrPedidoLog as $row10) {
							$this->logsis->insereLog(Array('UPDATE', 'OK', 'pedidolog',  $idpai, 'Unificação de Cadastro', 'Alterado idpedido de '.$row10['IDPEDIDO'].' para ' . $pedidocred . ' para ' . var_export($row10, TRUE)));
						}
						
						$arrUpd = array();
						$arrUpd['IDPEDIDO']  = $pedidopai;
						
						$this->global_model->update('pedidolog', $arrUpd, $where);
					}
					
					$arrPedidoLog = $this->global_model->selectx('pedidolog', 'idusuario = '.$bib, 'idpedidolog');

					if (count($arrPedidoLog) > 0) {
						foreach ($arrPedidoLog as $row10) {
							$this->logsis->insereLog(Array('UPDATE', 'OK', 'pedidolog',  $idpai, 'Unificação de Cadastro', 'Alterado idusuario de '.$bib.' para ' . $idpai . ' para ' . var_export($row10, TRUE)));
						}
						
						$arrUpd = array();
						$arrUpd['IDUSUARIO']  = $idpai;
						
						$this->global_model->update('pedidolog', $arrUpd, $wherebib);
					}
										
					$arrPedidoRep = $this->global_model->selectx('pedidoreprimido', 'idpedido = '.$ped['IDPEDIDO'], 'idpedido');
					
					if (count($arrPedidoRep) > 0) {		
						foreach ($arrPedidoRep as $row10) {
							$this->logsis->insereLog(Array('UPDATE', 'OK', 'pedidoreprimido',  $idpai, 'Unificação de Cadastro', 'Alterado idpedido de '.$row10['IDPEDIDO'].' para ' . $pedidocred . ' para ' . var_export($row10, TRUE)));
						}
								
						$arrUpd = array();
						$arrUpd['IDPEDIDO'] = $pedidopai;
								
						$this->global_model->update('pedidoreprimido', $arrUpd, $where);
					}
					
					// captura o ponto de venda, pois pode ter sido excluído
					$wherepdv = sprintf("IDPROGRAMA = %s AND IDBIBLIOTECA = %s", IDPROGRAMA, $idpai);
					$idPdv = $this->global_model->get_dado('programapdvparc', 'IDPDV', $wherepdv);

					$where = sprintf('IDPEDIDO = %d', $pedidopai);
					
					$arrUpd = array();	
					$arrUpd['IDBIBLIOTECA']   = $idpai;
					$arrUpd['IDPDV']          = $idPdv != '' ? $idPdv : NULL;
					$arrUpd['DATA_ULT_ATU']   = date('Y/m/d H:i:s');
					$arrUpd['VALORTOTAL']     = $ped['VALORTOTAL'];
					$arrUpd['IDPEDIDOSTATUS'] = $ped['IDPEDIDOSTATUS'];

					$this->global_model->update('pedido', $arrUpd, $where);
					
					$where = sprintf('IDPEDIDO = %d', $ped['IDPEDIDO']);
					$this->global_model->delete('pedido', $where);

				} else {
					$bib = $this->pedido_model->getBibliotecaPedido($ped['IDPEDIDO']);

					$wherepai = sprintf('IDPEDIDO = %d', $pedidopai);
					$wherebib = sprintf('IDUSUARIO = %d', $bib);
					$where = sprintf('IDPEDIDO = %d', $ped['IDPEDIDO']);					
					
					// primeiro processa as tabelas relacionadas
					// exclui os valores existentes do pedido receptor
					$arrPedidoLivro = $this->global_model->selectx('pedidolivro', 'idpedido = '.$ped['IDPEDIDO'], 'idpedido');

					if (count($arrPedidoLivro) > 0) {
						foreach ($arrPedidoLivro as $row10) {
							$this->logsis->insereLog(Array('DELETE', 'OK', 'pedidolivro',  $ped['IDBIBLIOTECA'], 'Unificação de Cadastro', var_export($row10, TRUE)));
						}
						
						$this->global_model->delete('pedidolivro', $where);
					}
					
					$arrPedidoLog = $this->global_model->selectx('pedidolog', 'idpedido = '.$ped['IDPEDIDO'], 'idpedidolog');
					
					if (count($arrPedidoLog) > 0) {
						foreach ($arrPedidoLog as $row10) {
							$this->logsis->insereLog(Array('DELETE', 'OK', 'pedidolog',  $ped['IDBIBLIOTECA'], 'Unificação de Cadastro', var_export($row10, TRUE)));
						}
						
						$this->global_model->delete('pedidolog', $where);
					}
					
					$arrPedidoRep = $this->global_model->selectx('pedidoreprimido', 'idpedido = '.$ped['IDPEDIDO'], 'idpedido');
					
					if (count($arrPedidoRep) > 0) {		
						foreach ($arrPedidoRep as $row10) {
							$this->logsis->insereLog(Array('DELETE', 'OK', 'pedidoreprimido',  $ped['IDBIBLIOTECA'], 'Unificação de Cadastro', var_export($row10, TRUE)));
						}
								
						$this->global_model->delete('pedidoreprimido', $where);
					}
					
					$where = sprintf('IDPEDIDO = %d', $ped['IDPEDIDO']);
					$this->global_model->delete('pedido', $where);					
				} 
			}
		}
		
		
		// processar o restante do comitê e dos responsáveis
		
		if ($this->input->post('RESPFIN') != $idpai) {
			$arrIDRespFin = $this->biblioteca_model->getDadosCadastroFinan($row['IDUSUARIO']);
			
			if (count($arrIDRespFin) > 0) {
				
				$idrf = array();
				
				foreach ($arrIDRespFin as $row5) {
					if ($row5['ATIVO'] == 'S') {
						array_push($idrf, $row5['IDUSUARIO']);
						$this->logsis->insereLog(Array('UPDATE', 'OK', 'cadbibliotecafinan',  $row5['id'], 'Unificação de Cadastro', 'Responsável financeiro inativado.'));
					}
				}
				
				if (count($idrf) > 0) {

					$stridrf = implode(',', $idrf);
				
					$arrUpd = array();
					
					$where = sprintf('IDUSUARIO IN (%s)', $stridrf);
					
					$arrUpd['ATIVO'] = 'N';
					$arrUpd['DATA_ATIVACAO'] = NULL;

					$this->global_model->update('usuario', $arrUpd, $where);
				}
			}
		}
		
		if ($this->input->post('COMITE') != $idpai) {
			$this->load->library('session');
			$arrIDComite = $this->biblioteca_model->listaComite($idpai, $this->session->userdata('programa'));
			
			if (count($arrIDComite) > 0) {
				
				$idcomite = array();
				
				foreach ($arrIDComite as $row6) {
					if ($row6['ATIVO'] == 'S') {
						array_push($idcomite, $row6['IDUSUARIO']);
						$this->logsis->insereLog(Array('UPDATE', 'OK', 'cadbibliotecacomite',  $row6['IDUSUARIO'], 'Unificação de Cadastro', 'Membro do comitê de acervo inativado.'));
					}
				}
				
				if (count($idcomite) > 0) {

					$stridcomite = implode(',', $idcomite);
					
					$arrUpd = array();
					
					$where = sprintf('IDUSUARIO IN (%s)', $stridcomite);
					
					$arrUpd['ATIVO'] = 'N';
					$arrUpd['DATA_ATIVACAO'] = NULL;
					
					$this->global_model->update('usuario', $arrUpd, $where);
				}
			}
			
		}
		
		// copia registros das bibliotecas envolvidas para tabela temporária
		$this->biblioteca_model->copiaBibliotecasTabelaTemp($idpai);
		
		// e finalmente atualiza a biblioteca pai com as informações das filhas
		$arrBib = array();
					
		$where = sprintf('IDUSUARIO = %d', $idpai);

		$arrDadosBibLogradouro = $this->biblioteca_model->getDadosCadastro($this->input->post('LOGRADOURO'));
		
		$arrBib['IDUSUARIOPAI']		= null;
		$arrBib['RAZAOSOCIAL']		= $this->input->post('RAZAOSOCIAL');
		$arrBib['NOMERESP']			= $this->input->post('NOMERESP');
		$arrBib['IDLOGRADOURO']		= $arrDadosBibLogradouro[0]['IDLOGRADOURO'];
		$arrBib['END_NUMERO']		= $arrDadosBibLogradouro[0]['END_NUMERO'];
		$arrBib['END_COMPLEMENTO']	= $arrDadosBibLogradouro[0]['END_COMPLEMENTO'];
		$arrBib['LATITUDE']			= $arrDadosBibLogradouro[0]['LATITUDE'];
		$arrBib['LONGITUDE']		= $arrDadosBibLogradouro[0]['LONGITUDE'];	

		$this->global_model->update('cadbiblioteca', $arrBib, $where);
		
		$arrUsu = array();
		
		$arrUsu['CPF_CNPJ']			= $this->input->post('CPF_CNPJ');
		$arrUsu['ATIVO']			= $this->biblioteca_model->isAtivo($this->input->post('DESC_STATUS')) ? 'S' : 'N';
		$arrUsu['HABILITADO']		= $this->biblioteca_model->isHabilitado($this->input->post('USER_HABILITADO')) ? 'S' : 'N';
		
		$this->global_model->update('usuario', $arrUsu, $where);

		// para depois excluir as bibliotecas filhas do cadastro
		
		$where = sprintf('IDUSUARIO IN (%s)', $strfilhos);
		$res = $this->global_model->delete('cadbiblioteca', $where);
		$res = $this->global_model->delete('usuario', $where);
			
		echo "<script>
					alert('Unificação efetuada com sucesso!');
					document.location.href='".URL_EXEC."adm/unificadorcadastros/".$this->input->post('PROXIMA')."';
		</script>";
	}
	
    function listaBiblioteca($nPag = 1)
	{
		$this->load->model('biblioteca_model');
		$this->load->model('uf_model');
        $this->load->model('logradouro_model');
		
		$arrDados = Array();
		$arrDados['nPag']      = (is_numeric($nPag) && $nPag > 0 ? $nPag : 1);
		$arrDados['exibir_pp'] = 15;
		$arrDados['limit_de']  = ($arrDados['nPag'] - 1) * $arrDados['exibir_pp'];
		
		// Filtros
		$filtros = (isset($_POST['filtro_razao_nome_fantasia'])) ? $_POST : null;
		$arrDados['filtros'] = $this->biblioteca_model->getArrayFiltros($filtros);
		$this->biblioteca_model->setFiltro('RAZAOSOCIAL', $arrDados['filtros']['filtro_razao_nome_fantasia']);
		$this->biblioteca_model->setFiltro('NOMEFANTASIA', $arrDados['filtros']['filtro_razao_nome_fantasia']);
		$this->biblioteca_model->setFiltro('HABILITADO', $arrDados['filtros']['filtro_habilitado']);
		$this->biblioteca_model->setFiltro('ATIVO', $arrDados['filtros']['filtro_status']);
        $this->biblioteca_model->setFiltro('CIDADE', $arrDados['filtros']['filtro_cidade']);
		$this->biblioteca_model->setFiltro('UF', $arrDados['filtros']['filtro_estado']);
		
		// Coleta options de UF
		$arrDados['options_estados'] = montaOptionsArray($this->uf_model->getEstados(), $arrDados['filtros']['filtro_estado']);
        
        // Coleta options de cidades
		$arrDados['options_cidades'] = montaOptionsArray($this->logradouro_model->getCidadesPesquisa($arrDados['filtros']['filtro_estado']), $arrDados['filtros']['filtro_cidade']);
		
		//pesquisa
		$arrDados['bibliotecas'] = $this->biblioteca_model->lista($arrDados, FALSE);
		$tamArr                  = count($arrDados['bibliotecas']);
		$arrDados['vazio']       = ($tamArr == 0 ? TRUE : FALSE);
		
		for ($i = 0; $i < $tamArr; $i++)
		{
			$arrDados['bibliotecas'][$i]['NLINHA'] = $i + 1 + $arrDados['limit_de'];
		}
		
		$arrDados['bibliotecas'] = add_corLinha($arrDados['bibliotecas'], 1);
		
		//dados da paginação
		$qtdReg = $this->biblioteca_model->lista($arrDados, TRUE);
		$nPaginas = $qtdReg[0]['CONT'] / $arrDados['exibir_pp'];
		
		if ($qtdReg[0]['CONT'] % $arrDados['exibir_pp'] != 0)
		{
			$nPaginas = (int)$nPaginas + 1;
		}
		
		$arrDados['nPaginas'] = $nPaginas;
		$arrDados['qtdReg']   = $qtdReg[0]['CONT'];
		
		$this->parser->parse('adm/biblioteca_lista_view', $arrDados);
	}
	
	/**
	* lista_biblioteca_by_id()
	* Mostra na tela os dados por ID.
	* @param integer id 
	* @return void
	**/
	function lista_biblioteca_by_id($idarquivo)
	{
		$this->load->model('global_model');
        $this->load->model('biblioteca_model');
		
		$arrDados = Array();
        $arrDados['info'] = $this->cnab_model->lista_by_id($idarquivo);
		
		$this->parser->parse('adm/bib_by_id_view', $arrDados);
	}
	
	
	function listaDistribuidor($nPag = 1)
	{
		$this->load->model('distribuidor_model');
		$this->load->model('uf_model');
		
		$arrDados = Array();
		$arrDados['nPag']      = (is_numeric($nPag) && $nPag > 0 ? $nPag : 1);
		$arrDados['exibir_pp'] = 15;
		$arrDados['limit_de']  = ($arrDados['nPag'] - 1) * $arrDados['exibir_pp'];
		
		// Filtros
		$filtros = (isset($_POST['filtro_razao_nome_fantasia'])) ? $_POST : null;
		$arrDados['filtros'] = $this->distribuidor_model->getArrayFiltros($filtros);
		$this->distribuidor_model->setFiltro('RAZAOSOCIAL', $arrDados['filtros']['filtro_razao_nome_fantasia']);
		$this->distribuidor_model->setFiltro('NOMEFANTASIA', $arrDados['filtros']['filtro_razao_nome_fantasia']);
		$this->distribuidor_model->setFiltro('HABILITADO', $arrDados['filtros']['filtro_habilitado']);
		$this->distribuidor_model->setFiltro('ATIVO', $arrDados['filtros']['filtro_status']);
		$this->distribuidor_model->setFiltro('UF', $arrDados['filtros']['filtro_estado']);
		
		// Coleta options de UF
		$arrDados['options'] = montaOptionsArray($this->uf_model->getEstados(), $arrDados['filtros']['filtro_estado']);
		
		/*
		// Pesquisa
		$this->load->library('array_table');
		$this->array_table->set_id('tabela_do_ricardo');
		$this->array_table->set_data($this->distribuidor_model->lista($arrDados, FALSE));
		$this->array_table->add_column('<a href="' . URL_EXEC . 'distribuidor/delete/{0}"><img src="' . URL_IMG . 'icon_delete.png" /></a>');
		$this->array_table->add_column('<a href="' . URL_EXEC . 'distribuidor/edit/{0}"><img src="' . URL_IMG . 'icon_edit.png" /></a>');
		$this->array_table->set_columns(array(0, 1, 2, 3, 4, 7));
		
		$arrDados['table_generic'] = $this->array_table->get_html();
		*/
		
		$arrDados['distribuidores'] = $this->distribuidor_model->lista($arrDados, FALSE);
		$tamArr            = count($arrDados['distribuidores']);
		$arrDados['vazio'] = ($tamArr == 0 ? TRUE : FALSE);
		
		for ($i = 0; $i < $tamArr; $i++)
		{
			$arrDados['distribuidores'][$i]['NLINHA'] = $i + 1 + $arrDados['limit_de'];
		}
		
		$arrDados['distribuidores'] = add_corLinha($arrDados['distribuidores'], 1);
		
		//dados da paginação
		$qtdReg = $this->distribuidor_model->lista($arrDados, TRUE);
		$nPaginas = $qtdReg[0]['CONT'] / $arrDados['exibir_pp'];
		
		if ($qtdReg[0]['CONT'] % $arrDados['exibir_pp'] != 0)
		{
			$nPaginas = (int)$nPaginas + 1;
		}
		
		$arrDados['nPaginas'] = $nPaginas;
		$arrDados['qtdReg']   = $qtdReg[0]['CONT'];
		
		$this->parser->parse('adm/distribuidor_lista_view', $arrDados);
	}
	
	function listaEditora($nPag = 1)
	{
		$this->load->model('editora_model');
		$this->load->model('uf_model');
		
		$arrDados = Array();
		$arrDados['nPag']      = (is_numeric($nPag) && $nPag > 0 ? $nPag : 1);
		$arrDados['exibir_pp'] = 15;
		$arrDados['limit_de']  = ($arrDados['nPag'] - 1) * $arrDados['exibir_pp'];
		
		// Filtros
		$filtros = (isset($_POST['filtro_razao_nome_fantasia'])) ? $_POST : null;
		$arrDados['filtros'] = $this->editora_model->getArrayFiltros($filtros);
		$this->editora_model->setFiltro('RAZAOSOCIAL', $arrDados['filtros']['filtro_razao_nome_fantasia']);
		$this->editora_model->setFiltro('NOMEFANTASIA', $arrDados['filtros']['filtro_razao_nome_fantasia']);
		$this->editora_model->setFiltro('HABILITADO', $arrDados['filtros']['filtro_habilitado']);
		$this->editora_model->setFiltro('ATIVO', $arrDados['filtros']['filtro_status']);
		$this->editora_model->setFiltro('UF', $arrDados['filtros']['filtro_estado']);
		
		// Coleta options de UF
		$arrDados['options'] = montaOptionsArray($this->uf_model->getEstados(), $arrDados['filtros']['filtro_estado']);
		
		//pesquisa
		$arrDados['editoras'] = $this->editora_model->lista($arrDados, FALSE);
		$tamArr            = count($arrDados['editoras']);
		$arrDados['vazio'] = ($tamArr == 0 ? TRUE : FALSE);
		
		for ($i = 0; $i < $tamArr; $i++)
		{
			$arrDados['editoras'][$i]['NLINHA'] = $i + 1 + $arrDados['limit_de'];
		}
		
		$arrDados['editoras'] = add_corLinha($arrDados['editoras'], 1);
		
		//dados da paginação
		$qtdReg = $this->editora_model->lista($arrDados, TRUE);
		$nPaginas = $qtdReg[0]['CONT'] / $arrDados['exibir_pp'];
		
		if ($qtdReg[0]['CONT'] % $arrDados['exibir_pp'] != 0)
		{
			$nPaginas = (int)$nPaginas + 1;
		}
		
		$arrDados['nPaginas'] = $nPaginas;
		$arrDados['qtdReg']   = $qtdReg[0]['CONT'];
		
		$this->parser->parse('adm/editora_lista_view', $arrDados);
	}
	
	function listaPdv($nPag = 1)
	{
		$this->load->model('pdv_model');
		$this->load->model('uf_model');
		
		$arrDados = Array();
		$arrDados['nPag']      = (is_numeric($nPag) && $nPag > 0 ? $nPag : 1);
		$arrDados['exibir_pp'] = 15;
		$arrDados['limit_de']  = ($arrDados['nPag'] - 1) * $arrDados['exibir_pp'];
		
		// Filtros
		$filtros = (isset($_POST['filtro_razao_nome_fantasia'])) ? $_POST : null;
		$arrDados['filtros'] = $this->pdv_model->getArrayFiltros($filtros);
		$this->pdv_model->setFiltro('RAZAOSOCIAL', $arrDados['filtros']['filtro_razao_nome_fantasia']);
		$this->pdv_model->setFiltro('NOMEFANTASIA', $arrDados['filtros']['filtro_razao_nome_fantasia']);
		$this->pdv_model->setFiltro('HABILITADO', $arrDados['filtros']['filtro_habilitado']);
		$this->pdv_model->setFiltro('ATIVO', $arrDados['filtros']['filtro_status']);
		$this->pdv_model->setFiltro('UF', $arrDados['filtros']['filtro_estado']);
		
		// Coleta options de UF
		$arrDados['options'] = montaOptionsArray($this->uf_model->getEstados(), $arrDados['filtros']['filtro_estado']);
		
		//pesquisa
		$arrDados['pdvs']  = $this->pdv_model->lista($arrDados, FALSE);
		$tamArr            = count($arrDados['pdvs']);
		$arrDados['vazio'] = ($tamArr == 0 ? TRUE : FALSE);
		
		for ($i = 0; $i < $tamArr; $i++)
		{
			$arrDados['pdvs'][$i]['NLINHA'] = $i + 1 + $arrDados['limit_de'];
		}
		
		$arrDados['pdvs'] = add_corLinha($arrDados['pdvs'], 1);
		
		//dados da paginação
		$qtdReg = $this->pdv_model->lista($arrDados, TRUE);
		$nPaginas = $qtdReg[0]['CONT'] / $arrDados['exibir_pp'];
		
		if ($qtdReg[0]['CONT'] % $arrDados['exibir_pp'] != 0)
		{
			$nPaginas = (int)$nPaginas + 1;
		}
		
		$arrDados['nPaginas'] = $nPaginas;
		$arrDados['qtdReg']   = $qtdReg[0]['CONT'];
		
		$this->parser->parse('adm/pdv_lista_view', $arrDados);
	}
	
    function listaLivro($nPag = 1)
	{
        $this->load->library('form_validation');
		$this->load->model('livro_model');
        $this->load->model('global_model');
		
		$arrDados = Array();
		$arrDados['nPag']      = (is_numeric($nPag) && $nPag > 0 ? $nPag : 1);
		$arrDados['exibir_pp'] = 15;
		$arrDados['limit_de']  = ($arrDados['nPag'] - 1) * $arrDados['exibir_pp'];
        
        //select do programa
		$arrDados['programas'] = $this->global_model->selectx('programa', 'ATIVO = \'S\'', 'DESCPROGRAMA');
        
		//cria array para filtros
        $where = Array();

        $this->form_validation->set_rules('filtro_status','Status','trim|xss_clean');
        $this->form_validation->set_rules('filtro_livro_popular','Situação no Edital','trim|xss_clean');
        $this->form_validation->set_rules('filtro_programa','Edital','trim|xss_clean');
        $this->form_validation->set_rules('filtro_isbn','ISBN','trim|xss_clean');
        $this->form_validation->set_rules('filtro_titulo','Título','trim|xss_clean');
        $this->form_validation->set_rules('filtro_autor','Autor','trim|xss_clean');
        $this->form_validation->set_rules('filtro_editora','Editora','trim|xss_clean');
            
        if($this->form_validation->run() == TRUE || $this->input->post() == FALSE)
        {
            $status         = $this->input->post('filtro_status');
            $livro_popular  = $this->input->post('filtro_livro_popular');
            $isbn           = $this->input->post('filtro_isbn');
            $titulo         = $this->input->post('filtro_titulo');
            $autor          = $this->input->post('filtro_autor');
            $editora        = $this->input->post('filtro_editora');
            $programa       = $this->input->post('filtro_programa');

            
            // Filtro para programa
            if($programa != '')
            {                
               $arrDados['programa'] = " AND pl.IDPROGRAMA = " . trim($programa);
            }
            else
            {
               $arrDados['programa'] = ""; 
            }
            
            // Filtro para editora
            if($editora != '')
            {                
                array_push($where, " (e.RAZAOSOCIAL LIKE '%" . htmlspecialchars($editora) . "%' OR e.NOMEFANTASIA LIKE '%" . htmlspecialchars($editora) . "%') ");
            }
            
            // Filtro para confirmação
            if($status != '')
            {
                array_push($where, " l.CONFIRMACAO = '" . $status . "' ");
            }
            
            // Filtro para livor popular
            if($livro_popular != '')
            {
                if($livro_popular == 'S')
                {
                    array_push($where, " pl.idlivro is not null ");
                }
                else
                {
                    array_push($where, " pl.idlivro is null ");
                }
            }

            // Filtro para confirmação
            if($isbn != '')
            {
                if(strlen($isbn) == 10)
                {
                    $isbn = '978' . $isbn;
                }
                
                array_push($where, " l.ISBN = '" . $isbn . "' ");
            }

            // Filtro para Título
            if($titulo != '')
            {
                array_push($where, " l.TITULO LIKE '%" . $titulo . "%' ");
            }

            // Filtro para Autor
            if($autor != '')
            {
                array_push($where, " l.AUTOR LIKE '%" . $autor . "%' ");
            }
            
        }

        // set filtros para pesquisa.
        $arrDados['where'] = (count($where)>0) ? implode(" AND ", $where) : '1=1';
        
		//pesquisa
		$arrDados['livros'] = $this->livro_model->lista($arrDados, FALSE);
		$tamArr             = count($arrDados['livros']);
		$arrDados['vazio']  = ($tamArr == 0 ? TRUE : FALSE);

		for ($i = 0; $i < $tamArr; $i++)
		{
			$arrDados['livros'][$i]['NLINHA'] = $i + 1 + $arrDados['limit_de'];
            // verifica se o livro foi adiconado ao programa
		}
		
		$arrDados['livros'] = add_corLinha($arrDados['livros'], 1);
		
		//dados da paginação
		$qtdReg = $this->livro_model->lista($arrDados, TRUE);
		$nPaginas = $qtdReg[0]['CONT'] / $arrDados['exibir_pp'];
		
		if ($qtdReg[0]['CONT'] % $arrDados['exibir_pp'] != 0)
		{
			$nPaginas = (int)$nPaginas + 1;
		}
		
		$arrDados['nPaginas'] = $nPaginas;
		$arrDados['qtdReg']   = $qtdReg[0]['CONT'];
		
		$this->parser->parse('adm/livro_lista_view', $arrDados);
	}
	
	function listaConfiguracao($nPag = 1, $erros = array(), $formDados = array(), $acao = '')
	{
		// Instancia classe de validação do formulário e model
		$this->load->model('configuracao_model');
		$this->load->model('global_model');
		$this->load->library('form_validation');

        $this->form_validation->set_rules('programa','Programa','trim|xss_clean');
        $prog = 0;
        if($this->form_validation->run() == TRUE)
        {
            $prog         = $this->input->post('programa');
        }
        
		$arrDados = Array();
		$arrDados['nPag']      = (is_numeric($nPag) && $nPag > 0 ? $nPag : 1);
		$arrDados['exibir_pp'] = 100;
		$arrDados['limit_de']  = ($arrDados['nPag'] - 1) * $arrDados['exibir_pp'];
		
		// Errors
		$arrDados['outrosErros'] = (isset($erros['outrosErros'])) ? $erros['outrosErros'] : null;
		$arrDados['erros'] = $erros;
		
        
        $programas = $this->global_model->selectx('programa', 'ATIVO = \'S\'', 'DESCPROGRAMA');
        
		if(!$prog)
        {
            $arrDados['programa'] = (count($programas) > 0) ? $programas[0]['IDPROGRAMA'] : 0;
        }
        else
        {
            $arrDados['programa'] = $prog;
        }
        
		//pesquisa
		$arrDados['configuracoes'] = $this->configuracao_model->lista($arrDados, FALSE);
		$tamArr                  = count($arrDados['configuracoes']);
		$arrDados['vazio']       = ($tamArr == 0 ? TRUE : FALSE);
		
		for ($i = 0; $i < $tamArr; $i++)
		{
			$arrDados['configuracoes'][$i]['NLINHA'] = $i + 1 + $arrDados['limit_de'];
		}
		
		$arrDados['configuracoes'] = add_corLinha($arrDados['configuracoes'], 1);
		
		//dados da paginação
		$qtdReg = $this->configuracao_model->lista($arrDados, TRUE);
		$nPaginas = $qtdReg[0]['CONT'] / $arrDados['exibir_pp'];
		
		if ($qtdReg[0]['CONT'] % $arrDados['exibir_pp'] != 0)
		{
			$nPaginas = (int)$nPaginas + 1;
		}
		
		$arrDados['nPaginas']   = $nPaginas;
		$arrDados['qtdReg']     = $qtdReg[0]['CONT'];
		$arrDados['acao']       = $acao;
		
		// Valida se existe movimento para desabilitar as configurações 12 | 13 | 14.
		$arrDados['movimentoPrograma'] = $this->global_model->get_dado('programalivro', 'COUNT(*)','IDPROGRAMA = ' . $this->session->userdata('programa'));
		$arrDados['movimentoPedido']   = $this->global_model->get_dado_join('pedidolivro pl', 'COUNT(*)', 'pedido p ON (pl.IDPEDIDO = p.IDPEDIDO)', 'p.IDPROGRAMA = ' . $this->session->userdata('programa'));
        
		// Dados do form
		$arrDados['formData'] = (count($formDados) > 0) ? $formDados : $this->configuracao_model->getDadosToForm($arrDados);
        $arrDados['programas'] = $programas;
        //var_dump($arrDados);
		$this->parser->parse('adm/configuracao_lista_view', $arrDados);
	}
	
	function form_configuracao($acao = '', $programa = 0)
	{
		if(isset($_POST) && $programa != 0)
		{
			$i = 0;
			$arrDados = Array();
			$outrosErros = Array();
			$boolValidation = true;
			
			// Carrega model para ser utilizado
			$this->load->model('configuracao_model');
			
			// Instancia classe de validação do formulário
			$this->load->library('form_validation');
            
            // Global model
            $this->load->model('global_model');
			
            // Valida se existe movimento para desabilitar as configurações 12 | 13 | 14.
            $movimentoPrograma = $this->global_model->get_dado('programalivro', 'COUNT(*)','IDPROGRAMA = ' . $programa);
            $movimentoPedido = $this->global_model->get_dado_join('pedidolivro pl', 'COUNT(*)', 'pedido p ON (pl.IDPEDIDO = p.IDPEDIDO)', 'p.IDPROGRAMA = ' . $programa);
			            
			// Varre o array de POST, verificando se algum campo contém um possível erro
			foreach($_POST as $key => $value)
			{
				// Tratamento para value vazio
				$value = (!isset($value)) ? '' : $value;
				
				// Retira o ID da configuração do nome
				preg_match("/_[0-9]+/", $key, $match);
				$idConfig = trim($match[0], '_');
				
				if($value != '')
				{
					// Seta a variável abaixo para simular a validação true do formulário
					// uma vez que se o formulário não tiver uma validação pelo menos, retornará false na validação
					$boolValidation = false;
					
					// Coleta o nome do campo
					$name = (strlen($this->configuracao_model->getConfiguracaoName($programa, $idConfig)) > 50) ? substr($this->configuracao_model->getConfiguracaoName($programa, $idConfig), 0, 50) . '...' : $this->configuracao_model->getConfiguracaoName($programa, $idConfig);
					
					// Valida conforme o tipo
					if(stristr($key, 'DECIMAL'))
					{
                        // Valida se existe movimento para desabilitar as configurações 13 | 14.
                        if($idConfig == 13 && $movimentoPrograma > 0)                         
                        {
                            $valor = $this->global_model->get_dado('programaconfig', 'VALORDEC','IDPROGRAMA = ' . $programa .  ' AND IDCONFIGURACAO = 13');
                            
                            if($valor != getValor($value))
                            {
                                $outrosErros[$i] = 'O campo ' . $name . ' não pode ser alterado neste momento. Pois já existe movimento para o programa atual.';
                            }
                        }
                        
                        if($idConfig == 14 && $movimentoPrograma > 0)                         
                        {
                            $valor = $this->global_model->get_dado('programaconfig', 'VALORDEC','IDPROGRAMA = ' . $programa .  ' AND IDCONFIGURACAO = 14');

                            if($valor != getValor($value))
                            {
                                $outrosErros[$i] = 'O campo ' . $name . ' não pode ser alterado neste momento. Pois já existe movimento para o programa atual.';
                            }
                        }
                        
						// Transformação do valor moeda para decimal
						$this->form_validation->set_rules($key, $name, 'valida_moeda');
					}
					
					if(stristr($key, 'INTEIRO'))
					{
                        // Valida se existe movimento para desabilitar as configurações 12.
                        if($idConfig == 12 && $movimentoPedido > 0)
                        {
                            $valor = $this->global_model->get_dado('programaconfig', 'VALORINT','IDPROGRAMA = ' . $programa .  ' AND IDCONFIGURACAO = 12');
							
                            if($valor != $value)
                            {
                                $outrosErros[$i] = 'O campo ' . $name . ' não pode ser alterado neste momento. Pois já existe movimento para o programa atual.';
                            }
                        }
                        
						$this->form_validation->set_rules($key, $name, 'integer');
					}
					if(stristr($key, 'PERIODO'))
					{						
                        $this->form_validation->set_rules('periodo_de_' . $idConfig, $name . ' (De)', 'valida_timestamp|isset|required');
                        $this->form_validation->set_rules('periodo_ate_' . $idConfig, $name . ' (Até)', 'valida_timestamp|isset|required');

                        // Testa se a segunda data é menor que a primeira
                        $diff = datediff(br_to_eua($_POST['periodo_ate_' . $idConfig]), br_to_eua($value));
                        if($diff < 0)
                        {
                            $outrosErros[$i] = 'Data do campo ' . $name . ' (Até) é menor que a data de início.';
                            $i++;
                        }
					}
				}
			}
			
			// Coleta outros erros, como data final menor que a inicial
			$arrDados['outrosErros'] = $outrosErros;
			
			// Caso a validação esteja ok
			if(($this->form_validation->run() == true && count($arrDados['outrosErros']) <= 0) || $boolValidation)
			{
                $this->db->trans_start();
				
				// Coleta o IDUSUARIO atual da sessão
				$idUsuario = $this->session->userdata('idUsuario');
				
				foreach($_POST as $key => $value)
				{
					// Tratamento para value vazio
					$value = (!isset($value)) ? '' : $value;
					
					// Retira o ID da configuração do nome
					preg_match("/_[0-9]+/", $key, $match);
					$idConfig = trim($match[0], '_');
					
					// Caso o value esteja vazio, então remove valor do banco
					if($value == '')
					{
						// Tratamento para PERIODO_DE e PERIODO_ATE
						if(stristr($key, 'PERIODO_DE_') || stristr($key, 'INTEIRO') || stristr($key, 'DECIMAL'))
						{
							// Remove o valor da configuração no banco
							$this->configuracao_model->deleteConfiguracaoValue($programa, $idConfig);
						}
					} else {
						// Caso contrário, faz o update conforme o tipo
						// Chama model, encaminhando o tipo a ser alterado, e seu valor
						if(stristr($key, 'DECIMAL'))
						{
							$this->configuracao_model->saveConfiguracaoValue('DECIMAL',$programa, getValor($value), $idConfig, $idUsuario);
						}
						if(stristr($key, 'INTEIRO'))
						{
							$this->configuracao_model->saveConfiguracaoValue('INTEIRO', $programa, $value, $idConfig, $idUsuario);
						}
						if(stristr($key, 'PERIODO'))
						{
							if(stristr($key, 'PERIODO_DE_'))
							{
								$arrDate[0] = br_to_eua($value);
								$arrDate[1] = br_to_eua($_POST['periodo_ate_' . $idConfig]);
								$this->configuracao_model->saveConfiguracaoValue('PERIODO', $programa, $arrDate, $idConfig, $idUsuario);
							}
						}
					}
				}
				
				$this->db->trans_complete();
				
				if ($this->db->trans_status() === FALSE)
				{
					//salva log
					$this->load->library('logsis', Array('UPDATE', 'ERRO', 'programaconfig', 0, 'Atualização de parâmetros do sistema', var_export($_POST, TRUE)));
					
					$arrErro['msg']      = 'Erro ao salvar configurações.';
					$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
					$this->load->view('erro_view', $arrErro);
					exit;
				}
				else
				{
					//salva log
					$this->load->library('logsis', Array('UPDATE', 'OK', 'programaconfig', 0, 'Atualização de parâmetros do sistema', var_export($_POST, TRUE)));
				}
			}
			
            // ADICONA O PROGRAMA NO POST
            $_POST['programa'] = $programa;
            
			$this->listaConfiguracao(1, $arrDados, $_POST, $acao);
		}
		else
		{
			$this->listaConfiguracao();
			
		}
	}
	
	function logsis($nPag = 1)
	{
        $this->load->library('form_validation');
		$this->load->model('logsis_model');
        $this->load->model('global_model');
		
        $arrDados = Array();
        $arrDados['logs']  = Array();
		$arrDados['vazio'] = TRUE;
        
        $this->form_validation->set_rules('filtro_resultado','Situação','trim|xss_clean');
        $this->form_validation->set_rules('filtro_data_de','Data De','trim|valida_data|xss_clean');
        $this->form_validation->set_rules('filtro_data_ate','Data Até','trim|valida_data|xss_clean');
        $this->form_validation->set_rules('filtro_hora_de','Hora De','trim|xss_clean');
        $this->form_validation->set_rules('filtro_hora_ate','Hora Até','trim|xss_clean');
            
        if($this->form_validation->run() == TRUE || $this->input->post() == FALSE)
        {
            $resultado = $this->input->post('filtro_resultado');
            $data_de   = $this->input->post('filtro_data_de');
            $data_ate  = $this->input->post('filtro_data_ate');
            $hora_de   = $this->input->post('filtro_hora_de');
            $hora_ate  = $this->input->post('filtro_hora_ate');

            if($data_de == '' && $data_ate == '' && $hora_de == '' && $hora_ate == '' && $resultado == '')
            {
                $data_de  = date("d/m/Y");
                $data_ate = date("d/m/Y");
            }
			
            if($resultado != '' || ($data_de != '' && $data_ate != '' && $hora_de == '' && $hora_ate == '') || ($data_de != '' && $data_ate != '' && $hora_de != '' && $hora_ate != ''))
            {
                // Testa se a segunda data é menor que a primeira
                $diffDate = datediff(br_to_eua($data_ate), br_to_eua($data_de));

                if($diffDate > 0 || ($diffDate == 0 && (($hora_de == '' && $hora_ate == '') || horadiff($hora_de.":00", $hora_ate.":00") >= 0)))
                {
                    // popula where do array de dados
                    $where = Array();
					
                    if($resultado != '')
					{
                        array_push($where, " l.RESULTADO = '" . $resultado . "' ");
					}
					
                    if($data_de != '' && $data_ate != '')
					{
                        array_push($where, " l.DATALOG BETWEEN STR_TO_DATE('" . $data_de . "', '%d/%m/%Y') AND STR_TO_DATE('" . $data_ate . "', '%d/%m/%Y')" );
					}
					
                    if($hora_de != '' && $hora_ate != '')
					{
                        array_push($where, " l.HORALOG BETWEEN STR_TO_DATE('" . $hora_de . "','%H:%i') AND STR_TO_DATE('" . $hora_ate . "','%H:%i') " );
					}

                    $arrDados['where']     = (count($where) > 0) ? " WHERE " . implode(" AND ", $where) : '';
                    $arrDados['nPag']      = (is_numeric($nPag) && $nPag > 0 ? $nPag : 1);
                    $arrDados['exibir_pp'] = 15;
                    $arrDados['limit_de']  = ($arrDados['nPag'] - 1) * $arrDados['exibir_pp'];
					
                    //pesquisa
                    $arrDados['logs']  = $this->logsis_model->lista($arrDados, FALSE);
                    $tamArr            = count($arrDados['logs']);
                    $arrDados['vazio'] = ($tamArr == 0 ? TRUE : FALSE);

                    for ($i = 0; $i < $tamArr; $i++)
                    {
                        $arrDados['logs'][$i]['NOMEUSUARIO'] = $this->global_model->get_dado(get_table_dados($arrDados['logs'][$i]['IDUSUARIOTIPO']), 'NOMERESP','IDUSUARIO=' . $arrDados['logs'][$i]['IDUSUARIO']);
                        $arrDados['logs'][$i]['NLINHA'] = $i + 1 + $arrDados['limit_de'];
                    }
					
                    $arrDados['logs'] = add_corLinha($arrDados['logs'], 1);
					
                    //dados da paginação
                    $qtdReg = $this->logsis_model->lista($arrDados, TRUE);
                    $nPaginas = $qtdReg[0]['CONT'] / $arrDados['exibir_pp'];
					
                    if ($qtdReg[0]['CONT'] % $arrDados['exibir_pp'] != 0)
                    {
                        $nPaginas = (int)$nPaginas + 1;
                    }
					
                    $arrDados['nPaginas'] = $nPaginas;
                    $arrDados['qtdReg']   = $qtdReg[0]['CONT'];
                }
                else
                {
                    $arrDados['outrosErros'] = "A data / hora De não podem ser maior que a data / hora Até.";
                }
            }
            else
            {
                $arrDados['outrosErros'] = "Deve se preencher as datas De e Até informando sendo o campo hora opcional.";
            }
        }
        
		$this->parser->parse('adm/logs_lista_view', $arrDados);
	}
	
	function setLatLngPdv($pass = '')
	{
		$this->load->model('pdv_model');
		$this->load->model('global_model');
		
		if ($pass == 'bn2011')
		{
			$pdvs   = $this->pdv_model->listaGeo(0,0);
			$qtdPdv = count($pdvs);
			
			for ($i = 0; $i < $qtdPdv; $i++)
			{
				//geocodificar endereço
				$address     = str_replace(' ','+',$pdvs[$i]['NOMELOGRADOURO2']) . ',+' . str_replace(' ','+',$pdvs[$i]['END_NUMERO']) . ',+' . str_replace(' ','+',$pdvs[$i]['NOMEBAIRRO']) . ',+' . str_replace(' ','+',$pdvs[$i]['NOMECIDADESUB']) . '+' . $pdvs[$i]['IDUF'];
				$request_url = MAPS_HOST . "&address=" . $address;
				
				$arqXml = simplexml_load_file($request_url);
				
				if ($arqXml)
				{
					$status = $arqXml->status;
					
					if ($status == 'OK')
					{
						$dadosLatLong = $arqXml->xpath('/GeocodeResponse/result[1]/geometry/location');
						
						$arrGeoCod = Array();
						$arrGeoCod['LATITUDE']  = $dadosLatLong[0]->lat;
						$arrGeoCod['LONGITUDE'] = $dadosLatLong[0]->lng;
						
						$this->global_model->update('cadpdv', $arrGeoCod, 'IDUSUARIO = ' . $pdvs[$i]['IDUSUARIO']);
					}
				}
			}
		}
		else
		{
			$arrErro['msg']      = 'Página não encontrada.';
			$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
			$this->load->view('erro_view', $arrErro);
		}
	}
	
	function setLatLngBiblioteca($pass = '')
	{
		$this->load->model('biblioteca_model');
		$this->load->model('global_model');
		
		if ($pass == 'bn2011')
		{
			$arrb = $this->biblioteca_model->listaGeo();
			$qtdb = count($arrb);
			
			for ($i = 0; $i < $qtdb; $i++)
			{
				//geocodificar endereço
				$address     = str_replace(' ','+',$arrb[$i]['NOMELOGRADOURO2']) . ',+' . str_replace(' ','+',$arrb[$i]['END_NUMERO']) . ',+' . str_replace(' ','+',$arrb[$i]['NOMEBAIRRO']) . ',+' . str_replace(' ','+',$arrb[$i]['NOMECIDADESUB']) . '+' . $arrb[$i]['IDUF'];
				$request_url = MAPS_HOST . "&address=" . $address;
				
				$arqXml = simplexml_load_file($request_url);
				
				if ($arqXml)
				{
					$status = $arqXml->status;
					
					if ($status == 'OK')
					{
						$dadosLatLong = $arqXml->xpath('/GeocodeResponse/result[1]/geometry/location');
						
						$arrGeoCod = Array();
						$arrGeoCod['LATITUDE']  = $dadosLatLong[0]->lat;
						$arrGeoCod['LONGITUDE'] = $dadosLatLong[0]->lng;
						
						$this->global_model->update('cadbiblioteca', $arrGeoCod, 'IDUSUARIO = ' . $arrb[$i]['IDUSUARIO']);
					}
				}
			}
		}
		else
		{
			$arrErro['msg']      = 'Página não encontrada.';
			$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
			$this->load->view('erro_view', $arrErro);
		}
	}

    function pedidosLivro($nPag = 1)
	{
        $this->load->library('form_validation');
		$this->load->model('pedido_model');
        $this->load->model('global_model');

		$arrDados = Array();
		$arrDados['nPag']      = (is_numeric($nPag) && $nPag > 0 ? $nPag : 1);
		$arrDados['exibir_pp'] = 15;
		$arrDados['limit_de']  = ($arrDados['nPag'] - 1) * $arrDados['exibir_pp'];

        //cria array para filtros
        $where = Array();
        
        //pedido diferente de EM CONFECIÇÃO
        array_push($where, "1 = 1");

        $this->form_validation->set_rules('filtro_status','Status','trim|xss_clean');
        $this->form_validation->set_rules('filtro_codpedido','Cod. Pedido','trim|integer|xss_clean');
        $this->form_validation->set_rules('filtro_biblioteca','Biblioteca','trim|xss_clean');
        $this->form_validation->set_rules('filtro_programa','Programa','trim|xss_clean');
        $this->form_validation->set_rules('filtro_pdv','Ponto de Venda','trim|xss_clean');

        if($this->form_validation->run() == TRUE || $this->input->post() == FALSE)
        {
            $status      = $this->input->post('filtro_status');
            $biblioteca  = $this->input->post('filtro_biblioteca');
            $codpedido   = $this->input->post('filtro_codpedido');
            $programa    = $this->input->post('filtro_programa');
            $pdv         = $this->input->post('filtro_pdv');

            // Filtro para programa pedido
            if($programa != '')
            {
                array_push($where, "p.IDPROGRAMA = " . $programa);
            }

            // Filtro para ponto de venda pedido
            if($pdv != '')
            {
                array_push($where, "(cp.NOMEFANTASIA LIKE '%" . $pdv . "%' OR cp.RAZAOSOCIAL LIKE '%" . $pdv . "%')");
            }

            // Filtro para status pedido
            if($status != '')
            {
                array_push($where, "p.IDPEDIDOSTATUS = '" . $status . "'");
            }

            // Filtro para Biblioteca
            if($codpedido != '')
            {
                array_push($where, "p.IDPEDIDO = '" . $codpedido . "'");
            }

            // Filtro para Biblioteca
            if($biblioteca != '')
            {
                array_push($where, "(cb.RAZAOSOCIAL LIKE '%" . $biblioteca . "%' OR cb.NOMEFANTASIA LIKE '%" . $biblioteca . "%')");
            }

        }

        // set filtros para pesquisa.
        $arrDados['where'] = implode(" AND ", $where);

		//pesquisa
		$arrDados['pedidos'] = $this->pedido_model->lista($arrDados, FALSE);
		$tamArr             = count($arrDados['pedidos']);
		$arrDados['vazio']  = ($tamArr == 0 ? TRUE : FALSE);

		for ($i = 0; $i < $tamArr; $i++)
		{
			$arrDados['pedidos'][$i]['NLINHA'] = $i + 1 + $arrDados['limit_de'];
            $ped = $this->pedido_model->listaLivrosPedidoById($arrDados['pedidos'][$i]['IDPEDIDO'], TRUE);
            $arrDados['pedidos'][$i]['QTDTOTAL'] = $ped[0]['QTDTOTAL'];
            $arrDados['pedidos'][$i]['VALORTOTAL'] = "R$ " . masc_real($arrDados['pedidos'][$i]['VALORTOTAL']);
            $arrDados['pedidos'][$i]['NOMEFANTASIA'] = ($arrDados['pedidos'][$i]['NOMEFANTASIA'] != '' ? '/ ' . $arrDados['pedidos'][$i]['NOMEFANTASIA'] : '');
            $arrDados['pedidos'][$i]['NOMEPDV'] = ($arrDados['pedidos'][$i]['NOMEPDV'] != '' ? '/ ' . $arrDados['pedidos'][$i]['NOMEPDV'] : '');
            
		}

		$arrDados['pedidos'] = add_corLinha($arrDados['pedidos'], 1);

		//dados da paginação
		$qtdReg = $this->pedido_model->lista($arrDados, TRUE);
		$nPaginas = $qtdReg[0]['CONT'] / $arrDados['exibir_pp'];

		if ($qtdReg[0]['CONT'] % $arrDados['exibir_pp'] != 0)
		{
			$nPaginas = (int)$nPaginas + 1;
		}

        //monta filtro do status
        $arrDados['statusFilt'] = $this->global_model->selectx('pedidostatus', 'IDPEDIDOSTATUS != 1', 'IDPEDIDOSTATUS');
        
        //monta filtro do programa
        $arrDados['programaFilt'] = $this->global_model->selectx('programa', '1=1', 'DESCPROGRAMA');

		$arrDados['nPaginas'] = $nPaginas;
		$arrDados['qtdReg']   = $qtdReg[0]['CONT'];
		
		$this->parser->parse('adm/pedido_lista_view', $arrDados);
    }

    function pedido($idPedido = null)
    {
        $this->load->library('form_validation');
        $this->load->model('pedido_model');

		if ($idPedido && is_numeric($idPedido))
		{
			$arrDados = Array();
			
			$arrParam = Array();
			$arrParam['limit_de'] = 0;
			$arrParam['exibir_pp'] = 1;

			$arrParam['where'] = " p.IDPEDIDO = '" . $idPedido . "' ";
			
			$arrDados['pedido'] = $this->pedido_model->lista($arrParam, FALSE);

			if (count($arrDados['pedido']) > 0)
			{
				$ped = $this->pedido_model->listaLivrosPedidoById($idPedido, TRUE);
				$arrDados['pedido'][0]['QTDTOTAL'] = $ped[0]['QTDTOTAL'];
				$arrDados['pedido'][0]['VALORTOTAL'] = "R$ " . masc_real($arrDados['pedido'][0]['VALORTOTAL']);

				$arrDados['pedido'][0]['livros'] = $this->pedido_model->listaLivrosPedidoById($idPedido);

				$tamArr = count($arrDados['pedido'][0]['livros']);

				for ($i = 0; $i < $tamArr; $i++)
				{
					// subtotal
					$arrDados['pedido'][0]['livros'][$i]['SUBTOTAL']         = "R$ " . masc_real($arrDados['pedido'][0]['livros'][$i]['QTD'] * $arrDados['pedido'][0]['livros'][$i]['PRECO_UNIT']);
					$arrDados['pedido'][0]['livros'][$i]['PRECO_UNIT']       = "R$ " . masc_real($arrDados['pedido'][0]['livros'][$i]['PRECO_UNIT']);
					// dados do livro
					$arrDados['pedido'][0]['livros'][$i]['DADOSLIVRO']       = '<b>' . $arrDados['pedido'][0]['livros'][$i]['TITULO'] . '</b>';
					$arrDados['pedido'][0]['livros'][$i]['DADOSLIVRO']      .= '<br />Edição: ' . ucwords(strtolower($arrDados['pedido'][0]['livros'][$i]['EDICAO']));
					$arrDados['pedido'][0]['livros'][$i]['DADOSLIVRO']      .= ' - ' . ucwords(strtolower($arrDados['pedido'][0]['livros'][$i]['ANO']));
					$arrDados['pedido'][0]['livros'][$i]['DADOSLIVRO']      .= '<br />Autor: ' . ucwords(strtolower($arrDados['pedido'][0]['livros'][$i]['AUTOR']));
					$arrDados['pedido'][0]['livros'][$i]['DADOSLIVRO']      .= '<br />Editora: ' . ucwords(strtolower($arrDados['pedido'][0]['livros'][$i]['RAZAOSOCIAL']));
				}

				$arrDados['pedido'][0]['livros'] = add_corLinha($arrDados['pedido'][0]['livros'], 1);

                $arrDados['pedido'][0]['historico'] = $this->pedido_model->getHistoricoPedido($idPedido);
				
				$this->parser->parse('adm/pedido_formcad_view', $arrDados);
			}
			else
			{
				$arrErro['msg']      = 'Pedido inválido.';
				$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
				$this->load->view('erro_view', $arrErro);
			}
		}
		else
		{
			$arrErro['msg']      = 'Pedido inválido.';
			$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
			$this->load->view('erro_view', $arrErro);
		}
    }
    
    function lista_log_gerenciamento($idbiblioteca = 0)
    {
    	$origem = $this->input->post('origem') == "ANTIGO" ? "SNIIC" : "CADBIBLIOTECA";
    	
    	$this->load->model('global_model');
		$arrDados = array();
		
		$log = $this->global_model->selectx('log_gerenciamento', 'ORIGEM = \'' . $origem . '\' AND IDBIBLIOTECA = ' . $idbiblioteca, 'DATA_LOG DESC');
		
		for ($i = 0; $i < count($log); $i++) {
			list($databanco, $horabanco) = explode(' ', $log[$i]['DATA_LOG']);
			$log[$i]['DATA_LOG_FORMAT'] = eua_to_br($databanco) . ' ' . $horabanco;
		}
		
		$arrDados['log'] = $log;
		
		$this->parser->parse('adm/lista_log_gerenciamento_modal', $arrDados);
		
    }
    
    function setpai($filho = '', $pai = '') 
	{
      	$this->load->model('biblioteca_model');
    	$this->biblioteca_model->set_pai($filho, $pai);
    }
    
    function bloqueioLivroIndicacao($nPag = 1, $idlivro = null, $tipo = 0)
    {	
		// Valida para caso o usuário seja diferente de biblioteca
		if ($this->session->userdata('tipoUsuario') != 1)
		{
			redirect('/login/');
			exit;
		}
		
        $this->load->library('form_validation');
        
		// Carrega Models
        $this->load->model('global_model');
		$this->load->model('isbn_model');
		$this->load->model('pedido_model');
		$this->load->model('biblioteca_model');
		$this->load->model('configuracao_model');
		
        // bloqueio ou desbloqueio do livro
        if($idlivro != null && $tipo != 0)
        {
            $arrUpdate = Array();
            $arrUpdate['BLOQUEADO']  = ($tipo == 1) ? 'S' : 'N';

            $this->global_model->update('isbnonline.isbn_titulo_unico', $arrUpdate, 'COD_AUX_TITULO = ' . $idlivro);           
        }
        
		$arrDados = Array();
		$arrDados['nPag']      = (is_numeric($nPag) && $nPag > 0 ? $nPag : 1);
		$arrDados['exibir_pp'] = 32;
		$arrDados['limit_de']  = ($arrDados['nPag'] - 1) * $arrDados['exibir_pp'];

        //cria array para filtros
        $where = Array();

        $this->form_validation->set_rules('filtro_titulo','Título','trim|xss_clean');
        $this->form_validation->set_rules('filtro_autor','Autor','trim|xss_clean');

        if($this->form_validation->run() == TRUE || $this->input->post() == FALSE)
        {
            $titulo         = $this->input->post('filtro_titulo');
            $autor          = $this->input->post('filtro_autor');

            array_push($where, " WHERE 1=1 ");

            // Filtro para Título
            if($titulo != '')
            {
                //array_push($where, " AND T.TITULO LIKE '%" . $titulo . "%' ");
                $arrTit = explode(' ', $titulo);

                $wheretit = " ";

                foreach ($arrTit as $tit) {
                    if (strlen($tit) > 2) {
                        $wheretit .= "AND T.TITULO LIKE '%".$tit."%' ";
                    }
                }
                array_push($where, $wheretit);
            }

            // Filtro para Autor
            if($autor != '')
            {
                //array_push($where, " AND T.AUTORES LIKE '%" . $autor . "%' ");
                $arrAut = explode(' ', $autor);

                $whereaut = " ";

                foreach ($arrAut as $aut) {
                    if (strlen($aut) > 2) {
                        $whereaut .= "AND T.AUTORES LIKE '%".$aut."%' ";
                    }
                }
                array_push($where, $whereaut);
            }

        }

        // set filtros para pesquisa.
        $arrDados['where'] = implode(" ", $where);

        // campo para visualização
        $arrDados['visualizacao']  = $this->input->post('visualizacao');

        // Pesquisa de Livros
        if (count($where) > 1 || ($where[0] != " WHERE 1=1 " && $where[0] != " WHERE BI.IDUSUARIO IS NULL ")) {
            $arrDados['livros'] = $this->isbn_model->getLivrosIsbnIndicacaoAdm($arrDados, FALSE);
            $tamArr = count($arrDados['livros']);
            $arrDados['vazio'] = ($tamArr == 0 ? TRUE : FALSE);
            $arrDados['pesquisa'] = TRUE;

            // Calcula cor das linhas
            for ($i = 0; $i < $tamArr; $i++) { $arrDados['livros'][$i]['NLINHA'] = $i + 1 + $arrDados['limit_de']; }
            $arrDados['livros'] = add_corLinha($arrDados['livros'], 1);

            // Dados da paginação
            $qtdReg = $this->isbn_model->getLivrosIsbnIndicacaoAdm($arrDados, TRUE);
            $nPaginas = $qtdReg[0]['CONT'] / $arrDados['exibir_pp'];
            if ($qtdReg[0]['CONT'] % $arrDados['exibir_pp'] != 0) {	$nPaginas = (int)$nPaginas + 1;	}
            $arrDados['nPaginas'] = $nPaginas;
            $arrDados['qtdReg'] = $qtdReg[0]['CONT'];
        } else {
            $arrDados['livros'] = array();
            $tamArr = 0;
            $arrDados['vazio'] = FALSE;
            $arrDados['pesquisa'] = FALSE;
            $arrDados['nPaginas'] = 0;
            $arrDados['qtdReg'] = 0;
        }

        $this->parser->parse('adm/pesquisa_livro_indicacao_view', $arrDados);
    }
	
	/**
	* painel_financeiro()
	* Painel de administração financeira. Exibe estatísticas de dados de valores 
	* creditados, pendencias e afins.
	* @return void
	*/
	function painel_financeiro()
	{
		$this->load->view('adm/painel_financeiro');
	}
}

/* End of file Adm.php */
/* Location: ./system/application/controllers/Adm.php */