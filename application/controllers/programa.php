<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe Programa
*
* Esta classe contém métodos para a abstração da entidade programa.
* 
* @author		Leandro Mangini Antunes
* @package		application
* @subpackage	application.programa
* @since		2012-03-24
*
*/
class Programa extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	
	/**
	* alterar_edital()
	* Tela de alteração de edital, seleção em qual está habilitado.
	* @return void
	*/
	function alterar_edital()
	{
		$this->load->library('session');
        // seta o usuario escolha habilitação de edital para forçar a troca
        $arrSession['programa']       = '-1';
        $arrSession['nomePrograma']   = '';
        $this->session->set_userdata($arrSession);
        redirect('/home/');
	}
	
	/**
	* modal_lista_contemplados()
	* Exibe uma lista de contemplados para um edital específico. Modelo 
	* de grid genérica.
	* @param integer idprograma
	* @return array contemplados
	*/
	function modal_lista_contemplados($idprograma = 0)
	{
		$this->load->model('programa_model');
		$this->load->library('array_table');
        
		// Seta o id e Coleta contemplados
		$this->array_table->set_id('lista_contemplados');
		$this->array_table->set_data($this->programa_model->lista_contemplados($idprograma));
		$this->array_table->set_items_per_page(10000);
		
		// Seta as colunas razao social e vlrcredito
		// $this->array_table->set_columns(array(2,3,4));
		$this->array_table->set_columns(array('UF', 'CIDADE', 'BIBLIOTECA', 'VALOR (R$)'));
		
		// Cospe ajax
		$args['table'] = $this->array_table->get_html();
		
		$this->parser->parse('programa/modal_lista_contemplados', $args);
	}
}
