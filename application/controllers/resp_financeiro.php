<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe Resp_Financeiro
*
* Abstração da entidade responsável financeiro no sistema.
* 
* @author		Leandro Mangini Antunes
* @package		application
* @subpackage	controllers.resp_financeiro
* @since		2012-07-05
*
*/
class Resp_Financeiro extends CI_Controller {
	
	/**
	* __construct()
	* @return void
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if ($this->session->userdata('acessoSis') == FALSE)
		{
			redirect('/login/');
			exit;
		}
	}
	
	/**
	* form_update_resp_financeiro()
	* Exibe html de formulario padrao de edição de dados de responsável financeiro.
	* @param integer idusuario
	* @return void
	*/
	function form_update_resp_financeiro($idusuario = 0)
	{
		// Por enquanto, somente administrador poderá acessar esta tela
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Carrega classes necessárias para utilização
			$this->load->model('biblioteca_model');
			
			// Dados da entidade
			$args['dados'] = $this->biblioteca_model->get_dados_responsavel_financeiro($idusuario);
			
			// Processa url_redirect (para redirecionamento do botão aplicar, ok e voltar do form)
			$args['url_redirect'] = ($this->input->get('url') == '') ? 'resp_financeiro/search' : $this->input->get('url');
			
			// Load da camada view
			$this->load->view('resp_financeiro/form_update_resp_financeiro', $args);
		} else {
			redirect('/home/');
		}
	}
	
	/**
	* form_update_resp_financeiro_proccess()
	* Processa formulário de atualização de cadastro de resp. financeiro.
	* @return void
	*/
	function form_update_resp_financeiro_proccess()
	{
		// Por enquanto, somente administrador poderá acessar esta tela
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Carrega global model
			$this->load->model('global_model');
			
			// Array de dados do cadastro de cadbibliotecafinan
			$idusuario = $this->input->post('idusuario');
			$arr_dados['idlogradouro']    = $this->input->post('idlogradouro');
			$arr_dados['idagencia'] 	  = $this->input->post('idagencia');
			$arr_dados['razaosocial'] 	  = $this->input->post('razaosocial');
			$arr_dados['nomeresp'] 		  = $this->input->post('nomeresp');
			$arr_dados['nomemae'] 		  = $this->input->post('nomemae');
			$arr_dados['datanasc'] 		  = format_date($this->input->post('datanasc'), true);
			$arr_dados['telefoneresp'] 	  = $this->input->post('telefoneresp');
			$arr_dados['emailresp'] 	  = $this->input->post('emailresp');
			$arr_dados['end_numero'] 	  = $this->input->post('end_numero');
			$arr_dados['end_complemento'] = $this->input->post('end_complemento');
			$this->global_model->update('cadbibliotecafinan', $arr_dados, "IDUSUARIO = $idusuario");
			$this->load->library('logsis', Array('UPDATE', 'OK', 'cadbiblitoecafinan', $this->session->userdata('idUsuario'), 'Atualização de cadastro de Responsável Financeiro', var_export($this->input->post(), true)));
			
			// Array de dados do cadastro de usuario
			$idusuario = $this->input->post('idusuario');
			$arr_usuario['cpf_cnpj'] = $this->input->post('cpf_cnpj');
			$arr_usuario['ativo'] = $this->input->post('ativo');
			$arr_usuario['tipopessoa'] = $this->input->post('tipopessoa');
			$this->global_model->update('usuario', $arr_usuario, "IDUSUARIO = $idusuario");
			$this->load->library('logsis', Array('UPDATE', 'OK', 'usuario', $this->session->userdata('idUsuario'), 'Atualização de cadastro de Usuário', var_export($this->input->post(), true)));
			
			// Redirect para formulário
			$action = $this->input->post('redirect_action');
			if($action == 'ok'){ redirect($this->input->post('redirect_ok')); } else {redirect( $this->input->post('redirect_aplicar') . '/?url=' . $this->input->post('redirect_ok')); }
		} else {
			redirect('/home/');
		}
	}
	
	/**
	* form_delete_resp_financeiro()
	* Exibe html de formulario padrao de deleção de responsável financeiro.
	* @param integer idusuario
	* @return void
	*/
	function form_delete_resp_financeiro($idusuario = 0)
	{
		// Por enquanto, somente administrador poderá acessar esta tela
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Carrega model biblioteca
			$this->load->model('biblioteca_model');
			$args['dados'] = $this->biblioteca_model->get_dados_responsavel_financeiro($idusuario);
			
			// Verifica se Responsavel financeiro possui cartao gerado
			$tem_cartao = $this->biblioteca_model->resp_financeiro_tem_cartao_gerado($idusuario);
			$em_process = $this->biblioteca_model->resp_financeiro_em_processo_geracao_cartao($idusuario);
			
			// Processa erros
			$args['msg_erro']  = '';
			$args['msg_erro'] .= ($tem_cartao) ? '&bull;&nbsp;Identificamos que existe um cartão vinculado já emitido. Para prosseguir com a deleção você deve solicitar uma remessa de bloqueio de cadastro, aguardar o retorno e tentar novamente.<br />' : '';
			$args['msg_erro'] .= ($em_process) ? '&bull;&nbsp;Identificamos que existe um cartão vinculado em processo de emissão. Para prosseguir com a deleção você deve solicitar uma remessa de bloqueio de cadastro após o processamento e emissão deste cartão, aguardar o retorno e tentar novamente.<br />' : '';
			
			// Processa basico de redirect
			$args['url_redirect'] = ($this->input->get('url_redirect') == '') ? 'responsavel_financeiro/search' : $this->input->get('url_redirect');
			
			// Load view
			$this->load->view('biblioteca/delete_responsavel_financeiro', $args);
		} else {
			redirect('/home/');
		}
	}
	
	/**
	* form_delete_resp_financeiro_proccess()
	* Processa formulário de deleção de cadastro de resp. financeiro.
	* @return void
	*/
	function form_delete_resp_financeiro_proccess()
	{
		// Por enquanto, somente administrador poderá acessar esta tela
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Carrega global model
			$this->load->model('global_model');
			$this->load->model('biblioteca_model');
			
			// Deleta responsavel financeiro
			$idusuario = ($this->input->post('idusuario') == '') ? 0 : $this->input->post('idusuario');
			$dados = $this->biblioteca_model->get_dados_responsavel_financeiro($idusuario);
			$this->global_model->delete('cadbibliotecafinan', "idusuario = $idusuario");
			$this->load->library('logsis', Array('DELETE', 'OK', 'cadbiblitoecafinan', $this->session->userdata('idUsuario'), 'Deleção de cadastro de Responsável Financeiro', var_export($dados, true)));
			
			// Deleta usuarioprograma
			$dados = $this->global_model->selectx('usuarioprograma', "idusuario = $idusuario", 'idusuario');
			$this->global_model->delete('usuarioprograma', "idusuario = $idusuario");
			$this->logsis->insereLog(Array('DELETE', 'OK', 'usuarioprograma', $this->session->userdata('idUsuario'), 'Deleção de cadastro de Usuário Programa', var_export($dados, true)));
			
			// Deleta usuario
			$dados = $this->global_model->selectx('usuario', "idusuario = $idusuario", 'idusuario');
			$this->global_model->delete('usuario', "idusuario = $idusuario");
			$this->logsis->insereLog(Array('DELETE', 'OK', 'usuario', $this->session->userdata('idUsuario'), 'Deleção de cadastro de Usuário', var_export($dados, true)));
			
			// Redirect para formulário
			$link = $this->input->post('link_redirect');
			if($link != ''){ redirect($link); } else { redirect('biblioteca/delete_responsavel_financeiro/' . $idusuario); }
		} else {
			redirect('/home/');
		}
	}
	
	/**
	* modal_form_view_resp_financeiro()
	* Exibe html de formulario padrao de dados da biblioteca.
	* @param integer idusuario
	* @return void
	*/
	function modal_form_view_resp_financeiro($idusuario = 0, $idprograma = 0)
	{
		// Carrega classes necessárias
		$this->load->model('resp_financeiro_model');
		$args['dados'] = $this->resp_financeiro_model->get_dados_resp_financeiro($idusuario, $idprograma);
		
		// Load view
		$this->parser->parse('resp_financeiro/modal_form_view_resp_financeiro', $args);
	}
	
	
	/**
	* search()
	* Funcao de entrada/pesquisa do modulo.
	* @return void
	*/
	function search($actual_page = 0)
	{
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Coleta o formulario de filtros 
			$args['filtros'] = $this->input->post();
			
			// Carrega tabela de dados, seta o array de dados e limita a paginacao
			$this->load->model('resp_financeiro_model');
			$this->load->library('array_table');
			$this->array_table->set_id('module_table');
			$this->array_table->set_data($this->resp_financeiro_model->get_lista_modulo($args['filtros']));
			$this->array_table->set_actual_page($actual_page);
			$this->array_table->set_page_link('resp_financeiro/search');
			
			// Adiciona colunas da abela e seta as colunas de exibicao
			$this->array_table->set_columns(array('#', 'NOME', 'BIBLIOTECA', 'TELEFONE', 'EMAIL', 'LOGIN'));
			$this->array_table->add_column('<a href="'. URL_EXEC . 'resp_financeiro/form_update_resp_financeiro/{0}/?url=resp_financeiro/search/"><img src="' . URL_IMG . 'icon_edit.png" title="Editar" /></a>');
			$this->array_table->add_column('<a href="javascript:void(0);" onclick="ajax_modal(\'Visualizar Dados\', \'' . URL_EXEC . 'resp_financeiro/modal_form_view_resp_financeiro/{0}\', \'' . URL_IMG . 'icon_view.png\', 600, 800);"><img src="' . URL_IMG . 'icon_view.png" title="Visualizar" /></a>');
			$this->array_table->add_column('{ATIVO_IMG}', false);
			$this->array_table->add_column('{CARTAO_IMG}', false);
			
			// Processa tabela do modulo
			$args['module_table'] = $this->array_table->get_html();
					
			$this->load->view('resp_financeiro/search', $args);
		} else {
			redirect('/home/');
		}	
	}
	
	/**
	* download_excel_livros()
	* Download de excel de livros.
	* @param
	* @return void
	**/
	function download_excel() 
	{
		// Carrega classe excel e model pedido
		$this->load->library('excel');
		$this->load->model('resp_financeiro_model');
		
		// Executa download do excel
		$this->excel->set_file_name('RESP_FINANCEIROS_' . date('Y-m-d_His'));
		$this->excel->set_data($this->resp_financeiro_model->get_lista_modulo());
		$this->excel->download_file();
	}

}
