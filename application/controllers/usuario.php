<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe Usuario
*
* Esta classe contém métodos para a abstração da entidade usuario.
* 
* @author		Fernando Alves
* @package		application
* @subpackage	application.usuario
* @since		2012-07-06
*
*/
class Usuario extends CI_Controller {
	
	
	/**
	* __construct()
	* Testa se usuario logado é administrador.
	* @return void
	*/	
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if ($this->session->userdata('acessoSis') == FALSE)
		{
			redirect('/login/');
			exit;
		}
	}
	
	/**
	* search()
	* Metodo index do modulo de usuarios.
	* @return void
	*/
	function search($actual_page = 0)
	{
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Coleta o formulário de filtros 
			$args['filtros'] = $this->input->post();		 

			// Carrega tabela de dados, seta o array de dados e limita a paginação	    
			$this->load->model('usuario_model');	 
			$this->load->library('array_table');
			$this->array_table->set_id('module_table');
			$this->array_table->set_data($this->usuario_model->get_lista_modulo($args['filtros']));
			$this->array_table->set_actual_page($actual_page);
			$this->array_table->set_page_link('usuario/search');

			// Adiciona colunas à tabela e seta as colunas de exibição
			$this->array_table->add_column('<a href="' . URL_EXEC . 'usuario/form_update_usuario/{0}/?url=usuario/search"><img src="' . URL_IMG . 'icon_edit.png" title="Editar" /></a>');
			$this->array_table->add_column('<a href="javascript:void(0);" onclick="ajax_modal(\'Visualizar Dados\', \'' . URL_EXEC . 'usuario/modal_form_view_usuario/{0}\', \'' . URL_IMG . 'icon_view.png\', 600, 800);"><img src="' . URL_IMG . 'icon_view.png" title="Visualizar" /></a>');	
			$this->array_table->add_column('<a href="javascript:void(0);" onclick="iframe_modal(\'Reenvio de Email de Validação\',\'' . URL_EXEC . 'usuario/reenviar_email_validacao/{0}\', \'' . URL_IMG . 'icon_reenviar_email.png\', 600, 750);"><img src="' . URL_IMG . 'icon_reenviar_email.png" title="Reenviar Email de Validação" /></a>');			
			$this->array_table->add_column('<a href="javascript:void(0);" onclick="iframe_modal(\'Reenvio de Email de Atualização de Senha\',\'' . URL_EXEC . 'usuario/reenviar_email_atualizacao_senha/{0}\', \'' . URL_IMG . 'icon_reenviar_email_senha.png\', 600, 750);"><img src="' . URL_IMG . 'icon_reenviar_email_senha.png" title="Reenviar Email de Atualização de Senha" /></a>');					
			$this->array_table->add_column('{ATIVO_IMG}', false);
			$this->array_table->set_columns(array('#', 'LOGIN', 'IDTIPOUSUARIO', 'TIPO USUÁRIO', 'TIPO PESSOA', 'CPF / CNPJ', 'DT. CADASTRO', 'DT. ATIVAÇÃO', 'ATIVO'));

			// Processa tabela do modulo
			$args['module_table'] = $this->array_table->get_html();

			// Carrega a camada de visualização		
			$this->load->view('usuario/search', $args);
		} else {
			redirect('/home/');
		}
	}
	
	/**
	* form_update_usuario()
	* Exibe html de formulario padrao de edição de dados do usuário.
	* @param integer idusuario
	* @return void
	*/	
	function form_update_usuario($idusuario = 0)
	{
		// Por enquanto, somente administrador poderá acessar esta tela
		if($this->session->userdata('tipoUsuario') == 1)
		{	
			// Coleta o formulário de filtros 
			$args['filtros'] = $this->input->post();
			
			// Carrega o model responsável.
			$this->load->model("usuario_model");

			// Dados da entidade
			$args['dados'] = $this->usuario_model->get_dados_usuario($idusuario);
		
			// Processa url_redirect (para redirecionamento do botão aplicar, ok e voltar do form)
			$args['url_redirect'] = ($this->input->get('url') == '') ? 'usuario/search' : $this->input->get('url');	

			// options dos tipos de usuarios
			$args['options_status'] = monta_options_array($this->usuario_model->get_tipos_usuario(), get_value($args['dados'], 'IDUSUARIOTIPO'), false);
		 
			// Carrega a camada de visualização.
			$this->load->view("usuario/form_update_usuario", $args);
		} else {
			redirect('/home/');
		}
	}
	
	/**
	* modal_form_view_usuario()
	* Exibe html de formulario padrao de dados do usuário.
	* @param integer idusuario
	* @return void
	*/	
   function modal_form_view_usuario($idusuario = 0)
   {
		// Por enquanto, somente administrador poderá acessar esta tela
		if($this->session->userdata('tipoUsuario') == 1)
		{	
			// Coleta o formulário de filtros 
			$args['filtros'] = $this->input->post();		 

			// Carrega o model usuario
			$this->load->model('usuario_model');	 
			
			$args['dados'] = $this->usuario_model->get_dados_usuario($idusuario);
			
			// Carrega a camada de visualização.
			$this->load->view("usuario/modal_form_view_usuario", $args);
			
		} else {
		  rediret('/home/');
		}
   }
	function form_update_usuario_proccess()
	{
		// Por enquanto, somente administrador poderá acessar esta tela
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Carrega global model
			$this->load->model('global_model');

			// Array de dados do cadastro de usuario
			$idusuario = $this->input->post('idusuario');
			$arr_dados['idusuariotipo'] = $this->input->post('idtipousuario');
			$arr_dados['cpf_cnpj'] = $this->input->post('cpf_cnpj');
			$arr_dados['tipopessoa'] = $this->input->post('tipopessoa');
			$arr_dados['ativo'] = $this->input->post('ativo');
			$this->global_model->update('usuario', $arr_dados, "IDUSUARIO = $idusuario");
			$this->load->library('logsis', Array('UPDATE', 'OK', 'usuario', $this->session->userdata('idUsuario'), 'Atualização de cadastro de Responsável Financeiro', var_export($this->input->post(), true)));
			
			// Redirect para formulário
			$action = $this->input->post('redirect_action');
			if($action == 'ok'){ redirect($this->input->post('redirect_ok')); } else {redirect( $this->input->post('redirect_aplicar') . '/?url=' . $this->input->post('redirect_ok')); }
		
		} else {
			redirect('/home/');			
		}
	}
	
	/**
	* download_excel()
	* Download de excel de Usuário.
	* @param
	* @return void
	*/
	function download_excel() 
	{
		// Carrega classe excel e model pedido
		$this->load->library('excel');
		$this->load->model('usuario_model');
		
		// Executa download do excel
		$this->excel->set_file_name('Usuario_' . date('Y-m-d_His'));
		$this->excel->set_data($this->usuario_model->get_lista_modulo());
		$this->excel->download_file();
	}
	
	/**
	* form_update_senha()
	* Exibe html de formulario padrao de edição de senha do usuário
	* @param integer idusuario
	* @return void
	*/		
	function form_update_senha($idusuario = 0)
	{
		// Carrega classes necessárias.
		$this->load->model('usuario_model');		

		// Processa url_redirect (para redirecionamento do botão aplicar, ok e voltar do form)
		$args['url_redirect'] = ($this->input->get('url') == '') ? 'usuario/search' : $this->input->get('url');

		// Id do usuário.
		if($this->session->userdata('tipoUsuario') == 1)
		{
			$args['idusuario'] = ($idusuario == 0) ? $this->session->userdata('idUsuario') : $idusuario;
		} else {
			$args['idusuario'] = $this->session->userdata('idUsuario');
		}
		
		// Coleta os dados do usuario
		$args['dados'] = $this->usuario_model->get_dados_usuario($args['idusuario']);
		
		// Carrega visualizacao
		$this->load->view('usuario/form_update_senha_usuario', $args);

		/*
		Forma antiga de alterar a senha.	
		if ($acao == '' || $acao == 'save')
		{
			$this->load->helper('form');
			$this->load->library('form_validation');
			
			$arrDados = Array();
			
			if ($acao == 'save')
			{
				$this->form_validation->set_rules('senha_atual', 'Senha Atual',           'trim|required|min_length[6]|max_length[20]|xss_clean|md5');
				$this->form_validation->set_rules('nova_senha',  'Nova Senha',            'trim|required|min_length[6]|max_length[20]|matches[senhanova2]|xss_clean');
				$this->form_validation->set_rules('nova_senha_confirma', 'Confirme a Nova Senha', 'trim|required|xss_clean');
				
				if ($this->form_validation->run() == TRUE)
				{
					$this->load->model('global_model');
					
                    if($this->session->userdata('idFinanceiro') != 0)
                    {
                        $idUsuario = $this->session->userdata('idFinanceiro');
                    }
                    else if($this->session->userdata('idMembro') != 0)
                    {
                        $idUsuario = $this->session->userdata('idMembro');
                    }
                    else
                    {
                        $idUsuario = $this->session->userdata('idUsuario');
                    }
					
					$senhaAtualBd = $this->global_model->get_dado('usuario', 'SENHA', 'IDUSUARIO = ' . $idUsuario);
					
					if ($senhaAtualBd == $this->input->post('senhaatual'))
					{
						if (md5($this->input->post('senhanova')) == md5($this->input->post('senhanova2')))
						{
							$arrUpd = Array();
							$arrUpd['SENHA'] = md5($this->input->post('senhanova'));
							
							$where  = sprintf("IDUSUARIO = %s", $idUsuario);
							$result = $this->global_model->update('usuario', $arrUpd, $where);
							
							if ($result == TRUE)
							{
								//salva log
								$this->load->library('logsis', Array('UPDATE', 'OK', 'usuario', 0, 'Alteração de senha de acesso', ''));
						
								$arrSucesso['msg']      = 'Senha alterada com sucesso!';
								$arrSucesso['link']     = 'home';
								$arrSucesso['txtlink']  = 'Ir para Home';
								$arrSucesso['tipoUser'] = $this->session->userdata('tipoUsuario');
								$this->load->view('sucesso_view', $arrSucesso);
							}
							else
							{
								//salva log
								$this->load->library('logsis', Array('UPDATE', 'ERRO', 'usuario', 0, 'Alteração de senha de acesso', ''));
								
								$arrErro['msg']      = 'Ocorreu um erro na tentativa de alterar a senha.';
								$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
								$this->load->view('erro_view', $arrErro);
							}
						}
						else
						{
							$arrDados['outrosErros'] = 'Nova senha diferente da sua confirmação.';
							$acao = '';
						}
					}
					else
					{
						$arrDados['outrosErros'] = 'Senha atual incorreta.';
						$acao = '';
					}
				}
				else
				{
					$acao = '';
				}
			}
			
			if ($acao == '')
			{
				$this->parser->parse('alteraSenha_form_view', $arrDados);
			}
		}
		else
		{
			$arrErro['msg']      = 'Requisição inválida.';
			$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
			$this->load->view('erro_view', $arrErro);
		}
*/		
	}
	
	
	/**
	* form_update_senha_proccess()
	* Processa formulário de atualização de senha do usuario
	* @return void
	*/	
	function form_update_senha_proccess()
	{
		// Carrega global model
		$this->load->model('global_model');

		// Carrega o usuario_model
		$this->load->model('usuario_model');

		// Senha atual.
		$senha_atual = $this->input->post('senha_atual');

		// Dados para update de senha.
		$idusuario = $this->input->post('idusuario');

		// Nova senha a ser inserida no banco.
		$arr_dados['senha'] = md5($this->input->post('nova_senha'));

		// Verifica se a senha e id conferem com a senha e id passados.
		$num_rows = $this->usuario_model->get_dados_usuario_by_id($idusuario, $senha_atual);

		// Passa a id caso haja erro de senha
		$args['idusuario'] = $idusuario;
		// Passa os dados caso haja erro de senha
		$args['dados'] = $this->usuario_model->get_dados_usuario($args['idusuario']);		
		
		// Verifica se a senha e id bateram.
		if($num_rows > 0 )
		{
		// Realiza o update.
		$this->global_model->update('usuario', $arr_dados, "IDUSUARIO = $idusuario");

		// Monta o redirect
		$action = $this->input->post('redirect_action');
		$args['url_redirect'] = ($action == 'ok') ? $this->input->post('redirect_ok') : $this->input->post('redirect_aplicar') . '/?url=' . $this->input->post('redirect_ok');
				
		// Carrega a camada de visualização senha correta.
		$this->load->view('usuario/form_update_senha_proccess', $args);			

		} else {

		$args['senha_errada'] = 1;		

		// Monta o redirect
		$action = $this->input->post('redirect_action');
		$args['url_redirect'] = ($action == 'aplicar') ? $this->input->post('redirect_aplicar') : $this->input->post('redirect_ok') . '/?url=' . $this->input->post('redirect_ok');

		// Carrega a camada de visualização alterar senha.
		$this->load->view('usuario/form_update_senha_usuario', $args);
		}
	}

	/**
	* reenviar_email_validacao()
	* Processa reenvio de email.
	* @return void
	*/		
	function reenviar_email_validacao($idusuario = 0)
	{
		// Por enquanto, somente administrador poderá acessar esta tela
		if($this->session->userdata('tipoUsuario') == 1)
		{	
			// Coleta o formulário de filtros 
			$args['filtros'] = $this->input->post();		 

			// Monta o redirect
			$args['url_redirect'] = ($this->input->get('url') == '') ? 'usuario/usuario_send_mail' : $this->input->get('url');	
			
			// Carrega o model usuario
			$this->load->model('usuario_model');	 
		
			// Pega os dados do usuário.
			$args['dados'] = $this->usuario_model->get_dados_usuario($idusuario);
			
			// Pega o tipo e email geral.
			$args['dados_reativa'] = $this->usuario_model->get_usuario_by_id_reactive_account($idusuario);
			
			// Carrega a camada de visualização.
			$this->load->view('usuario/reenviar_email_validacao', $args);
			
			
		} else {
			// Carrega a camada de visualização.
			$this->load->view('close_modal');
		}
	}

	/**
	* modal_form_view_envia_email()
	* Envia o emai para reativar cadastro.
	* @return void
	*/			
	function modal_form_view_envia_email()
	{
		// Por enquanto, somente administrador poderá acessar esta tela
		if($this->session->userdata('tipoUsuario') == 1)
		{		
			$args['filtros'] = $this->input->post();
			
			// Corpo email.
			$message = "&nbsp;&nbsp;Bom dia. <br />
				&nbsp;&nbsp;Você está recebendo um email de reaticação de cadastro no site <br />
				<b>Fundação Biblioteca Nacional</b>, para reativar seu cadastro e poder<br /> 
				usufruir das funcionalidades do sistema basta clicar no link abaixo e você será <br />
				redirecionado para uma página de confirmação de cadastro reativado com sucesso <br />
				<br />
				<a href=" . URL_EXEC . "/acesso_externo/reativacao_cadastro/" . $args['filtros']['idusuario'] . "/" . $args['filtros']['linkreativacao'] . ">Clique aqui para reativar</a>
				<br /><br />
				&nbsp;&nbsp;Após a confirmação de cadastro ativo você poderá ir a página incial e fazer<br />
				login no site.";
			
			// Email a ser encaminhado.	
			$to = $args['filtros']['emailgeral'];

			//variavel para finalizar o loop.
			$retorno = sendEmail('Reativação de cadastro Fundação Biblioteca Nacional','Reative seu Cadastro na Fundação Biblioteca Nacional', $to, $message );	
			
			while($retorno != true)
			{
				$retorno = sendEmail('Reativação de cadastro Fundação Biblioteca Nacional','Reative seu Cadastro na Fundação Biblioteca Nacional', $to, $message );	
			}
			if($retorno == true)
			{
				// Carrega a camada de visualização.
				$this->load->view('usuario/modal_form_view_usuario_reactive_sucess');
			}
		}
	}
	
	/**
	* reenviar_email_atualizacao_senha()
	* Envia email de alteração de senha.
	* @return void
	*/	
	function reenviar_email_atualizacao_senha($idusuario = 0)
	{
		// Por enquanto, somente administrador poderá acessar esta tela
		if($this->session->userdata('tipoUsuario') == 1)
		{	
			// Coleta o formulário de filtros 
			$args['filtros'] = $this->input->post();		 

			// Monta o redirect
			$args['url_redirect'] = ($this->input->get('url') == '') ? 'usuario/usuario_send_mail' : $this->input->get('url');	
			
			// Carrega o model usuario
			$this->load->model('usuario_model');	 
		
			// Pega os dados do usuário.
			$args['dados'] = $this->usuario_model->get_dados_usuario($idusuario);
			
			// Pega o tipo e email geral.
			$args['dados_reativa'] = $this->usuario_model->get_usuario_by_id_reactive_account($idusuario);
			
			// Carrega a camada de visualização.
			$this->load->view('usuario/reenviar_email_atualizacao_senha', $args);
			
			
		} else {
			// Carrega a camada de visualização.
			$this->load->view('close_modal');
		}
	}

	/**
	* modal_form_view_envia_email_nova_senha()
	* Gera nova senha para o usuário e envia um email.
	* @return void
	*/		
	function modal_form_view_envia_email_nova_senha()
	{
		// Coleta o formulário de filtros 
		$args['filtros'] = $this->input->post();
		
		// Carrega a camada model da entidade.
		$this->load->model('usuario_model');

		// Carrega o helper para o random de string.
		$this->load->helper('string');
		
		// Gera a nova senha.
		$senha_alnum = random_string('alnum', 6);
		$senha = md5($senha_alnum);
		
		// Altera no banco para a nova senha.
		$args['dados'] = $this->usuario_model->update_usuario_senha(get_value($args['filtros'], 'idusuario'), $senha);
		
		// Se a senha foi alterada, envia o email.
		if($args['dados'] > 0)
		{
			$message = "&nbsp;&nbsp;Bom dia. <br />
			&nbsp;&nbsp;Você está recebendo um email com sua nova senha de acesso <br />
			ao site <b>Fundação Biblioteca Nacional</b>. <br />	<br />	
			&nbsp;&nbsp;Nova senha: $senha_alnum <br /><br />
			&nbsp;&nbsp;Para realizar o seu login <a href=" . URL_EXEC . ">clique aqui</a> e digite seu login seguido de <br />
			sua nova senha.";
			
			// Email a ser encaminhado.	
			$to = get_value($args['filtros'], 'emailgeral');
			
			//variavel para finalizar o loop.
			$retorno = sendEmail('Sua nova senha - Fundação Biblioteca Nacional','Sua nova senha - Fundação Biblioteca Nacional', $to, $message );	
			
			while($retorno != true)
			{
				$retorno = sendEmail('Sua nova senha - Fundação Biblioteca Nacional','Sua nova senha - Fundação Biblioteca Nacional', $to, $message );	
			}
			if($retorno == true)
			{
				// Carrega a camada de visualização.
				$this->load->view('usuario/modal_form_view_usuario_updated_sucess');
			}
		} else	{	
			$args['sucesso'] = false;
			// Carrega a camada de visualização.
			$this->load->view('usuario/modal_form_view_usuario_updated_sucess', $args);
		}	
	}
}
