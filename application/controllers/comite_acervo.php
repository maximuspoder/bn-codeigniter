<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe Comite_acervo
*
* Esta classe contém métodos para a abstração da entidade comite de acervo.
* 
* @author		Fernando Alves
* @package		application
* @subpackage	controllers.comite_acervo
* @since		2012-07-05
*
*/
class Comite_acervo extends CI_Controller {
	
	/**
	* __construct()
	* @return void
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if ($this->session->userdata('acessoSis') == FALSE)
		{
			redirect('/login/');
			exit;
		}		
	}

	/**
	* search()
	* Função de entrada/pesquisa do modulo.
	*/
	function search($actual_page = 0)
	{
		if ($this->session->userdata('tipoUsuario') == 1)
		{
			// Coleta o formulário de filtros 
			$args['filtros'] = $this->input->post();	
		
			// Carrega tabela de dados, seta o array de dados e limita a paginação	    
			$this->load->model("comite_acervo_model");
			$this->load->library('array_table');
			$this->array_table->set_id('module_table');
			$this->array_table->set_data($this->comite_acervo_model->get_lista_modulo($args['filtros']));
			$this->array_table->set_actual_page($actual_page);
			$this->array_table->set_page_link('comite_acervo/search');	

			// Adiciona colunas à tabela e seta as colunas de exibição
			$this->array_table->add_column('<a href="' . URL_EXEC . 'comite_acervo/form_update_comite_acervo/{0}/?url=comite_acervo/search"><img src="' . URL_IMG . 'icon_edit.png" title="Editar" /></a>');
			$this->array_table->add_column('<a href="javascript:void(0);" onclick="ajax_modal(\'Visualizar Dados\', \'' . URL_EXEC . 'comite_acervo/modal_form_view_comite_acervo/{0}\', \'' . URL_IMG . 'icon_view.png\', 600, 800);"><img src="' . URL_IMG . 'icon_view.png" title="Visualizar" /></a>');
			$this->array_table->add_column('{ATIVO_IMG}', false);
			$this->array_table->set_columns(array('#', 'NOME', 'BIBLIOTECA', 'TELEFONE', 'EMAIL', 'LOGIN'));
			
			// Processa tabela do modulo
			$args['module_table'] = $this->array_table->get_html();
			
		   // Chama a view search
			$this->load->view("comite_acervo/search", $args);
		} else {
			redirect('/home/');
		}	
	}
	
	
	/**
	* download_excel()
	* Download de excel de Comite de acervo.
	* @param
	* @return void
	*/
	function download_excel() 
	{
		// Carrega classe excel e model pedido
		$this->load->library('excel');
		$this->load->model('comite_acervo_model');
		
		// Executa download do excel
		$this->excel->set_file_name('ComiteAcervo_' . date('Y-m-d_His'));
		$this->excel->set_data($this->comite_acervo_model->get_lista_modulo());
		$this->excel->download_file();
	}	

	/**
	* modal_form_view_comite_acervo()
	* Exibe html de formulario padrao de dados do comite de acervo.
	* @param integer idusuario
	* @return void
	*/	
   function modal_form_view_comite_acervo($idusuario = 0)
   {
		// Carrega o model necessário.
		$this->load->model("comite_acervo_model");
		$args['dados'] = $this->comite_acervo_model->get_dados_comite_acervo($idusuario);

		// Carrega a camada de visualização.
		$this->load->view("comite_acervo/modal_form_view_comite_acervo", $args);
   }
	
	/**
	* form_update_comite_acervo()
	* Exibe html de formulario padrao de edição de dados do comitê de acervo
	* @param integer idusuario
	* @return void
	*/
	function form_update_comite_acervo($idusuario = 0)
	{
		// Por enquanto, somente administrador poderá acessar esta tela
		if($this->session->userdata('tipoUsuario') == 1)
		{	
            // Carrega o model responsável.
			$this->load->model("comite_acervo_model");
				
		   // Dados da entidade
		   $args['dados'] = $this->comite_acervo_model->get_dados_comite_acervo($idusuario);
			
		   // Processa url_redirect (para redirecionamento do botão aplicar, ok e voltar do form)
		   $args['url_redirect'] = ($this->input->get('url') == '') ? 'comite_acervo/search' : $this->input->get('url');		
		
	       // Chama a camada de visualização.
	       $this->load->view("comite_acervo/form_update_comite_acervo", $args);
		  
		} else {
		
		  redirect('/home/');
		  
		}
	}
	
	/**
	* form_update_comite_acervo_proccess()
	* Processa formulário de atualização de cadastro de resp. financeiro.
	* @return void
	*/	
	function form_update_comite_acervo_proccess()
	{
		// Por enquanto, somente administrador poderá acessar esta tela
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Carrega global model
			$this->load->model('global_model');

			// Array de dados do cadastro de cadbibliotecacomite
			$idusuario = $this->input->post('idusuario');
			$arr_dados['idlogradouro']    = $this->input->post('idlogradouro');
			$arr_dados['razaosocial'] = $this->input->post('razaosocial');
			$arr_dados['nomeresp'] = $this->input->post('nomeresp');
			$arr_dados['nomemae'] = $this->input->post('nomemae');
			$arr_dados['datanasc'] = format_date($this->input->post('datanasc'), true);
			$arr_dados['telefoneresp'] = $this->input->post('telefoneresp');
			$arr_dados['emailresp'] = $this->input->post('emailresp');
			$arr_dados['end_numero'] 	  = $this->input->post('end_numero');
			$arr_dados['end_complemento'] = $this->input->post('end_complemento');
			$this->global_model->update('cadbibliotecacomite', $arr_dados, "IDUSUARIO = $idusuario");
			$this->load->library('logsis', Array('UPDATE', 'OK', 'cadbibliotecacomite', $this->session->userdata('idUsuario'), 'Atualização de cadastro de Responsável Financeiro', var_export($this->input->post(), true)));
			
			// Array de dados do cadastro de usuario
			$idusuario = $this->input->post('idusuario');
			$arr_usuario['cpf_cnpj'] = $this->input->post('cpf_cnpj');
			$arr_usuario['ativo'] = $this->input->post('ativo');
			$arr_usuario['tipopessoa'] = $this->input->post('tipopessoa');
			$this->global_model->update('usuario', $arr_usuario, "IDUSUARIO = $idusuario");
			$this->load->library('logsis', Array('UPDATE', 'OK', 'usuario', $this->session->userdata('idUsuario'), 'Atualização de cadastro de Usuário', var_export($this->input->post(), true)));
			
			// Redirect para formulário
			$action = $this->input->post('redirect_action');
			if($action == 'ok'){ redirect($this->input->post('redirect_ok')); } else {redirect( $this->input->post('redirect_aplicar') . '/?url=' . $this->input->post('redirect_ok')); }
		} else {
			redirect('/home/');
		}
	}
	
	
}
