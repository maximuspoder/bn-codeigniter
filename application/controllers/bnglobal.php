<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BNGlobal extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('session');
		
		if ($this->session->userdata('acessoSis') == FALSE)
		{
			redirect('/login/');
			exit;
		}

	}
	
	/*************************************************************************
	 *
	 * CONTROLLERS DE PESQUISA
	 *
	 * detalharLivro         : detalhar os dados do livro
	 *
	 *************************************************************************/
	
	function index()
	{
		$arrErro['msg']      = 'Página não existe.';
		$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
		$this->load->view('erro_view', $arrErro);
	}
	
	function nfunc()
	{
		$arrErro['msg']      = 'Esta funcionalidade não está disponível.';
		$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
		$this->load->view('erro_view', $arrErro);
	}

    function detalharLivro($idLivro = NULL, $tipo = NULL)
    {
		$this->load->model('global_model');
		$this->load->model('livro_model');
		
		$arrDados  = Array();
		
		if (is_numeric($idLivro))
		{
			$dados = $this->livro_model->getDadosCadastro($idLivro);
			
			if (count($dados) > 0)
			{
				$arrDados['idlivro']        = $idLivro;
				$arrDados['isbn']           = $dados[0]['ISBN'];
				$arrDados['titulo']         = $dados[0]['TITULO'];
				$arrDados['autor']          = $dados[0]['AUTOR'];
				$arrDados['suporte_aux']    = ($dados[0]['IDISBNSUPORTE'] != '') ? $this->global_model->get_dado('isbnsuporte', 'DESCSUPORTE', 'IDISBNSUPORTE=' . $dados[0]['IDISBNSUPORTE']) : '';
				$arrDados['fichacat_aux']   = ($dados[0]['FICHACAT'] == 'S') ? 'Sim' : 'Não';
				$arrDados['formato_a']      = ($dados[0]['FORMATO_A'] == 0 ? '' : $dados[0]['FORMATO_A']);
				$arrDados['formato_b']      = ($dados[0]['FORMATO_B'] == 0 ? '' : $dados[0]['FORMATO_B']);
				$arrDados['npaginas']       = ($dados[0]['NPAGINAS'] == 0 ? '' : $dados[0]['NPAGINAS']);
				$arrDados['papel_miolo']    = ($dados[0]['PAPEL_MIOLO'] == 0 ? '' : $dados[0]['PAPEL_MIOLO']);
				$arrDados['papel_capa']     = ($dados[0]['PAPEL_CAPA'] == 0 ? '' : $dados[0]['PAPEL_CAPA']);
				$arrDados['peso']           = ($dados[0]['PESO'] == 0 ? '' : $dados[0]['PESO']);
				$arrDados['orelha_aux']     = ($dados[0]['ORELHA'] == 'S') ? 'Sim' : 'Não';
				$arrDados['tipocapa_aux']   = ($dados[0]['IDISBNTIPOCAPA'] != '') ? $this->global_model->get_dado('isbntipocapa', 'DESCTIPOCAPA', 'IDISBNTIPOCAPA=' . $dados[0]['IDISBNTIPOCAPA']) : '';
				$arrDados['tipocapa_outra'] = $dados[0]['TIPOCAPA_OUTRA'];
				$arrDados['acabamento_aux'] = ($dados[0]['IDISBNACABAMENTO'] != '') ? $this->global_model->get_dado('isbnacabamento', 'DESCACABAMENTO', 'IDISBNACABAMENTO=' . $dados[0]['IDISBNACABAMENTO']) : '';
				$arrDados['idioma_aux']     = ($dados[0]['IDISBNIDIOMA'] != '') ? $this->global_model->get_dado('isbnidioma', 'DESCIDIOMA', 'IDISBNIDIOMA=' . $dados[0]['IDISBNIDIOMA']) : '';
				$arrDados['idiomatrad_aux'] = ($dados[0]['IDISBNIDIOMA_TRAD'] != '') ? $this->global_model->get_dado('isbnidioma', 'DESCIDIOMA', 'IDISBNIDIOMA=' . $dados[0]['IDISBNIDIOMA_TRAD']) : '';
				$arrDados['idassunto']      = ($dados[0]['IDISBNASSUNTO'] != '') ? $this->global_model->get_dado('isbnassunto', 'DESCASSUNTO', 'IDISBNASSUNTO=' . $dados[0]['IDISBNASSUNTO']) : '';
				$arrDados['assunto']        = $dados[0]['DESCASSUNTO'];
				$arrDados['edicao']         = $dados[0]['EDICAO'];
				$arrDados['ano']            = $dados[0]['ANO'];
				$arrDados['palavraschave']  = $dados[0]['PALAVRASCHAVE'];
				$arrDados['sinopse']        = $dados[0]['SINOPSE'];
				$arrDados['linkdados']      = $dados[0]['LINKDADOS'];
				$arrDados['loadDadosEdit']  = 1;
				
				if($tipo == 1)
				{
					$this->parser->parse('livro_forminfo_view', $arrDados);
				}
				else
				{
					$this->parser->parse('adm/livro_forminfo_view', $arrDados);
				}
			}
			else
			{
				$arrErro['msg']      = 'Livro não encontrado.';
				$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
				$this->load->view('erro_view', $arrErro);
			}
		}
		else
		{
			$arrErro['msg']      = 'Livro não encontrado.';
			$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
			$this->load->view('erro_view', $arrErro);
		}
    }

}

/* End of file pesquisa.php */
/* Location: ./system/application/controllers/pesquisa.php */