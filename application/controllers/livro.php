<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe Livro
*
* Abstracao do modulo de livros.
* 
* @author		Ricardo Neves Becker
* @package		application
* @subpackage	application.livro
* @since		2012-06-14
*
*/
class Livro extends CI_Controller {
	
	/**
	* __construct()
	* @return void
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if ($this->session->userdata('acessoSis') == FALSE)
		{
			redirect('/login/');
			exit;
		}
	}
	
	/**
	* index()
	* Executado caso no exista chamada explicita de metodo na url.
	* @return void
	*/
	function index()
	{
		redirect('/relatorio/search');
	}

	/**
	* search()
	* Funcao de entrada/pesquisa do modulo.
	* @return void
	*/
	function search($actual_page = 0)
	{
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Coleta o formulario de filtros 
			$args['filtros'] = $this->input->post();
			
			// Carrega tabela de dados, seta o array de dados e limita a paginacao
			$this->load->model('livro_model');
			$this->load->library('array_table');
			$this->array_table->set_id('module_table');
			$this->array_table->set_data($this->livro_model->get_lista_modulo($args['filtros']));
			$this->array_table->set_actual_page($actual_page);
			$this->array_table->set_page_link('livro/search');
			
			// Adiciona colunas da abela e seta as colunas de exibicao
			$this->array_table->set_columns(array('#', 'ISBN', 'EDITORA', 'AUTOR', 'TITULO', 'EDICAO', 'ANO', 'EDITAL', 'ATUALIZADO EM'));
			$this->array_table->add_column('<a href="' . URL_EXEC . 'bnglobal/detalharLivro/{0}"><img src="' . URL_IMG . 'icon_detalhes.png" title="Visualizar" /></a>');
			$this->array_table->add_column('<a href="javascript:void(0);" onclick="iframe_modal(\'Marcar Inexigível\', \'' . URL_EXEC . 'livro/marca_inexigibilidade_livro/{0}\' , \'' . URL_IMG . 'icon_marcar_inexigivel.png\' , 500,650);"><img src="' . URL_IMG . 'icon_marcar_inexigivel.png" /></a>');
			$this->array_table->add_column('<img src="' . URL_IMG . '{IMGSTATUS}" title="{DESC_STATUS}" />', false );
			$this->array_table->add_column('{IMG_INEXIGIVEL}', false);
			
			// Processa tabela do modulo
			$args['module_table'] = $this->array_table->get_html();
			
			// Monta options de status de pedido
			$args['option_status'] = monta_options_array($this->livro_model->get_status_programa(), get_value($args['filtros'], 'p__idprograma'));
			
			$this->load->view('livro/search', $args);
		} else {
			redirect('/home/');
		}
	}
	
	/**
	* download_excel_livros()
	* Download de excel de livros.
	* @param
	* @return void
	**/
	function download_excel() 
	{
		// Carrega classe excel e model pedido
		$this->load->library('excel');
		$this->load->model('livro_model');
		
		// Executa download do excel
		$this->excel->set_file_name('LIVROS_' . date('Y-m-d_His'));
		$this->excel->set_data($this->livro_model->get_lista_modulo());
		$this->excel->download_file();
	}
	
	/**
    * marca_inexigibilidade_livro()
    * Exibe html da tela de inexigibilidade de livros.
    * @param integer idlivro
    * @return void
    */
    function marca_inexigibilidade_livro($idlivro = 0) 
	{
        $this->load->model('livro_model');

        $args['arq'] = $this->livro_model->getDadosLivro($idlivro);

        $this->parser->parse('livro/inexigibilidade_livro', $args);
    }
	
	/**
    * marca_inexigibilidade_livro_proccess()
    * Envia para o banco a inexigibilidade de livros.
    * @param date data 
    * @return voiD 
	*/
    function marca_inexigibilidade_livro_proccess() 
	{
        // carrego a model
        $this->load->model('livro_model');

        // pego via POST os campos de data e idarquivo
        $idlivro = $this->input->post('idlivro');
		$inex = $this->input->post('inexigivel');
		
        // faço o update
        $this->livro_model->inexigibilidade_livro($idlivro, $inex);
		
        $this->parser->parse('close_modal', array());
		
    }
	
	/**
	* download_excel_livros_inexigiveis()
	* Download de excel de livros inexigiveis.
	* @param
	* @return void
	**/
	function download_excel_livros_inexigiveis()
	{
		// Carrega classe excel e model pedido
		$this->load->library('excel');
		$this->load->model('livro_model');
		
		// Executa download do excel
		$this->excel->set_file_name('LIVROS_INEXIGIVEIS_' . date('Y-m-d_His'));
		$this->excel->set_data($this->livro_model->get_lista_inexigiveis());
		$this->excel->download_file();
	}
}