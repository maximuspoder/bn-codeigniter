<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe Comunicacao
*
* Abstração de classe para entidade comunicacao e modulo de gestão de comunicacao.
* 
* @author		Leandro Mangini Antunes
* @package		application
* @subpackage	application.comunicacao
* @since		2012-05-25
*
*/
class Comunicacao extends CI_Controller 
{

	/**
	* __construct()
	* Construtor de classe.
	* @return object
	*/
	function __construct()
	{
		parent::__construct();
	}
	
	/**
	* comunicado_portal()
	* Exibe html de comunicado para tela de abertura do portal, quando houver.
	* @return void
	*/
	function comunicado_portal()
	{
		$this->load->view('comunicacao/comunicado_portal');
	}
	
	/**
	* manutencao_sistema()
	* Exibe html de comunicado de manutencao do sistema.
	* @return void
	*/
	function manutencao_sistema()
	{
		$this->load->view('comunicacao/manutencao_sistema');
	}
	
	/**
	* instrucoes_pesquisa_livros()
	* Exibe html de instruções de Pesquisa de livros.
	* @return void
	*/
	function instrucoes_pesquisa_livros()
	{
		$this->load->view('comunicacao/instrucoes_pesquisa_livros');
	}
	
	/**
	* instrucoes_pesquisa_estabelecimentos()
	* Exibe html de instruções de Pesquisa de estabelecimentos.
	* @return void
	*/
	function instrucoes_pesquisa_estabelecimento()
	{
		$this->load->view('comunicacao/instrucoes_pesquisa_estabelecimento');
	}
	
	/**
	* informacoes_conferencia_pdv()
	* Exibe html de informacoes da conferencia do pdv.
	* @return void
	*/
	function informacoes_conferencia_pdv()
	{
		$this->load->view('comunicacao/informacoes_conferencia_pdv');
	}
	
	/**
	* informacoes_conferencia_biblioteca()
	* Exibe html de informacoes da conferencia da biblioteca.
	* @return void
	*/
	function informacoes_conferencia_biblioteca()
	{
		$this->load->view('comunicacao/informacoes_conferencia_biblioteca');
	}
	
	/**
	* instrucoes_pesquisa_bibliotecas()
	* Exibe html de instrucoes de pesquisa debibliotecas.
	* @return void
	*/
	function instrucoes_pesquisa_bibliotecas()
	{
		$this->load->view('comunicacao/instrucoes_pesquisa_bibliotecas');
	}
	
	/**
	* comunicado_cnpc()
	* Exibe html do comunicado cnpc, mostrado em modal na area de trabalho
	* @return void
	*/
	function comunicado_cnpc()
	{
		$this->load->view('comunicacao/comunicado_cnpc');
	}
}
