<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Editora extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('session');
		
		if ($this->session->userdata('acessoSis') == FALSE)
		{
			redirect('/login/');
			exit;
		}
	}
	
	/*************************************************************************
	 *
	 * CONTROLLERS DE EDITORA
	 *
	 * form_alt               : formulario de alteração de cadastro
	 * livro_precad           : formulario de pre cadastro de livro
	 * livro_formcad          : formulario de cadastro/alteração de livro
	 * livros                 : gerencia / lista livros
	 * assunto_autocomplete   : lista assunto no autocomplete
     * livroImportar          : importar lista de livros em .txt
     * uploadImportar         : realiza importação dos isbn
     * adicionaProgramaLivro  : adiciona ao programa livro
     * livroParticipante      : listagem de livros participantes do programa
	 *
	 *************************************************************************/
	
	function index()
	{
		$arrErro['msg']      = 'Página não existe.';
		$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
		$this->load->view('erro_view', $arrErro);
	}
	
	function form_alt($acao = 'edit')
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('usuario_class');
		$this->load->model('global_model');
		$this->load->model('editora_model');
		$this->load->model('usuario_model');
		$this->load->model('naturezajur_model');
		
		$arrDados   = Array();
		$idUsuario  = $this->session->userdata('idUsuario');
		$tipopessoa = '';
		
		if ($acao == 'save')
		{
			$tipopessoa = $this->input->post('tipopessoa');
			$labelAux   = ($tipopessoa == 'PJ' ? 'Razão Social' : 'Nome');
			
			$this->form_validation->set_rules('tipopessoa',      'Tipo Pessoa',                          'required');
			$this->form_validation->set_rules('cnpjcpf',         'CNPJ/CPF',                             'trim|required|max_length[18]|valida_cnpj_cpf|xss_clean');
			$this->form_validation->set_rules('iestadual',       'Inscrição Estadual',                   'trim|max_length[20]|xss_clean');
			$this->form_validation->set_rules('razaosocial',     $labelAux,                              'trim|required|max_length[100]|xss_clean');
			$this->form_validation->set_rules('nomefantasia',    'Nome Fantasia',                        'trim|max_length[100]|xss_clean');
			$this->form_validation->set_rules('naturezajur',     'Natureza Jurídica',                    'valida_combo|xss_clean');
			$this->form_validation->set_rules('atividadeprinc',  'Atividade Principal',                  'valida_combo|xss_clean');
			$this->form_validation->set_rules('telefone1',       'Telefone Geral',                       'trim|required|max_length[15]|xss_clean');
			$this->form_validation->set_rules('email',           'E-mail Geral',                         'trim|required|max_length[100]|valid_email');
			$this->form_validation->set_rules('site',            'Site',                                 'trim|max_length[100]|xss_clean');
			$this->form_validation->set_rules('login',           'Login',                                '');
			$this->form_validation->set_rules('idlogradouro',    'Endereço',                             'is_numeric');
			$this->form_validation->set_rules('cep',             '',                                     '');
			$this->form_validation->set_rules('uf',              '',                                     '');
			$this->form_validation->set_rules('cidade',          '',                                     '');
			$this->form_validation->set_rules('bairro',          '',                                     '');
			$this->form_validation->set_rules('logradouro',      '',                                     '');
			$this->form_validation->set_rules('logcomplemento',  '',                                     '');
			$this->form_validation->set_rules('end_numero',      'Endereço - Número',                    'trim|required|max_length[8]|xss_clean');
			$this->form_validation->set_rules('end_complemento', 'Complemento',                          'trim|max_length[30]|xss_clean');
			$this->form_validation->set_rules('qtd_titulos',     'Quantidade de títulos ativos',         'trim|integer|xss_clean');
			$this->form_validation->set_rules('tem_rede',        'Mantém rede de ...',                   'trim|required|xss_clean');
			$this->form_validation->set_rules('assoc_ent_sind',  'Associado a entidade sindical',        'trim|max_length[100]|xss_clean');
			$this->form_validation->set_rules('fil_ent_assoc',   'Filiada a entidade(s) associativa(s)', 'trim|max_length[100]|xss_clean');
			$this->form_validation->set_rules('nomeresp',        'Nome Responsável',                     'trim|required|max_length[100]|xss_clean');
			$this->form_validation->set_rules('telefoneresp',    'Telefone do Responsável',              'trim|required|max_length[15]|xss_clean');
			$this->form_validation->set_rules('skyperesp',       'Skype do Responsável',                 'trim|max_length[30]|xss_clean');
			$this->form_validation->set_rules('emailresp',       'E-mail do Responsável',                'trim|required|max_length[100]|valid_email');
			$this->form_validation->set_rules('socios',          'Sócios',                               'trim|valida_socios|xss_clean');
			$this->form_validation->set_rules('atuacao',         'Atua com',                             'trim|valida_atuacao|xss_clean');
			
			if ($this->form_validation->run() == TRUE)
			{
				if ($tipopessoa != 'PJ' && $tipopessoa != 'PF')
				{
					$arrDados['outrosErros'] = 'Tipo pessoa inválido.';
					$acao = '';
				}
				elseif ($this->input->post('idlogradouro') == '')
				{
					$arrDados['outrosErros'] = 'Informe o endereço.';
					$acao = '';
				}
				else
				{
					$where = sprintf("IDUSUARIO = %s", $idUsuario);
					
					$this->db->trans_start();
					
					$arrUpd = Array();
					$arrUpd['IDLOGRADOURO']       = ($this->input->post('idlogradouro') != '' ? $this->input->post('idlogradouro') : NULL);
					$arrUpd['IDNATUREZAJUR']      = $this->input->post('naturezajur');
					$arrUpd['IESTADUAL']          = $this->input->post('iestadual');
					$arrUpd['RAZAOSOCIAL']        = removeAcentos($this->input->post('razaosocial'), TRUE);
					$arrUpd['NOMEFANTASIA']       = removeAcentos($this->input->post('nomefantasia'), TRUE);
					$arrUpd['TELEFONEGERAL']      = $this->input->post('telefone1');
					$arrUpd['EMAILGERAL']         = $this->input->post('email');
					$arrUpd['SITE']               = $this->input->post('site');
					$arrUpd['NOMERESP']           = removeAcentos($this->input->post('nomeresp'), TRUE);
					$arrUpd['TELEFONERESP']       = $this->input->post('telefoneresp');
					$arrUpd['EMAILRESP']          = $this->input->post('emailresp');
					$arrUpd['SKYPERESP']          = $this->input->post('skyperesp');
					$arrUpd['END_NUMERO']         = $this->input->post('end_numero');
					$arrUpd['END_COMPLEMENTO']    = removeAcentos($this->input->post('end_complemento'), TRUE);
					$arrUpd['QTD_TITULO_ATIVOS']  = ($this->input->post('qtd_titulos') != '' ? $this->input->post('qtd_titulos') : 0);
					$arrUpd['ASSOC_ENT_SINDICAL'] = $this->input->post('assoc_ent_sind');
					$arrUpd['FILIACAO_ENT_ASSOC'] = $this->input->post('fil_ent_assoc');
					$arrUpd['TEM_REDE_DIST']      = ($this->input->post('tem_rede') == 'S' ? 'S' : 'N');
					$arrUpd['DATA_UPD']           = date('Y/m/d H:i:s');
					
					$this->global_model->update('cadeditora', $arrUpd, $where);
					
					$this->usuario_class->delete_socio($idUsuario);
					
					if ($this->input->post('socios') != '')
					{
						$this->usuario_class->save_socio($idUsuario, $this->input->post('socios'));
					}
					
					$this->usuario_class->delete_atuacao($idUsuario);
					
					if ($this->input->post('atuacao') != '' || $this->input->post('atividadeprinc') != 0)
					{
						$this->usuario_class->save_atuacao($idUsuario, $this->input->post('atuacao'), $this->input->post('atividadeprinc'));
					}
					
					$this->db->trans_complete();
					
					if ($this->db->trans_status() === FALSE)
					{
						//salva log
						$this->load->library('logsis', Array('UPDATE', 'ERRO', 'cadeditora', $idUsuario, 'Atualização de cadastro de editora', var_export($arrUpd, TRUE)));
						
						$arrErro['msg']      = 'Ocorreu um erro na tentativa de atualizar o cadastro.';
						$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
						$this->load->view('erro_view', $arrErro);
					}
					else
					{
						//salva log
						$this->load->library('logsis', Array('UPDATE', 'OK', 'cadeditora', $idUsuario, 'Atualização de cadastro de editora', var_export($arrUpd, TRUE)));
						
						$arrSucesso['msg']      = 'Cadastro atualizado com sucesso!';
						$arrSucesso['link']     = 'home';
						$arrSucesso['txtlink']  = 'Ir para Home';
						$arrSucesso['tipoUser'] = $this->session->userdata('tipoUsuario');
						$this->load->view('sucesso_view', $arrSucesso);
					}
				}
			}
			else
			{
				$acao = '';
			}
		}
		
		//informações zeradas para nao dar erro no parse
		$arrDados['tipopessoa2']     = $tipopessoa;
		$arrDados['tipopessoa_aux']  = '';
		$arrDados['cnpjcpf']         = '';
		$arrDados['naturezajur_aux'] = 0;
		$arrDados['atividadep_aux']  = 0;
		$arrDados['tem_rede_aux']    = '';
		$arrDados['iestadual']       = '';
		$arrDados['razaosocial']     = '';
		$arrDados['nomefantasia']    = '';
		$arrDados['telefone1']       = '';
		$arrDados['email']           = '';
		$arrDados['site']            = '';
		$arrDados['login']           = '';
		$arrDados['idlogradouro']    = '';
		$arrDados['cep']             = '';
		$arrDados['uf']              = '';
		$arrDados['cidade']          = '';
		$arrDados['bairro']          = '';
		$arrDados['logradouro']      = '';
		$arrDados['logcomplemento']  = '';
		$arrDados['end_numero']      = '';
		$arrDados['end_complemento'] = '';
		$arrDados['qtd_titulos']     = '';
		$arrDados['assoc_ent_sind']  = '';
		$arrDados['fil_ent_assoc']   = '';
		$arrDados['nomeresp']        = '';
		$arrDados['telefoneresp']    = '';
		$arrDados['skyperesp']       = '';
		$arrDados['emailresp']       = '';
		$arrDados['socios']          = '';
		$arrDados['atuacao']         = '';
		$arrDados['loadDadosEdit']   = 0;
		
		if ($acao == 'edit')
		{
			$dados = $this->editora_model->getDadosCadastro($idUsuario);
			
			$where = sprintf("IDUSUARIO = %s AND PRINCIPAL = 'S'", $idUsuario);
			$atvdPrincipal = $this->global_model->get_dado('cadatividade', 'IDATIVIDADE', $where);
			
			$arrDados['tipopessoa2']     = $dados[0]['TIPOPESSOA'];
			$arrDados['tipopessoa_aux']  = $dados[0]['TIPOPESSOA'];
			$arrDados['cnpjcpf']         = $dados[0]['CPF_CNPJ'];
			$arrDados['naturezajur_aux'] = $dados[0]['IDNATUREZAJUR'];
			$arrDados['atividadep_aux']  = ($atvdPrincipal != '' ? $atvdPrincipal : 0);
			$arrDados['tem_rede_aux']    = $dados[0]['TEM_REDE_DIST'];
			$arrDados['iestadual']       = $dados[0]['IESTADUAL'];
			$arrDados['razaosocial']     = $dados[0]['RAZAOSOCIAL'];
			$arrDados['nomefantasia']    = $dados[0]['NOMEFANTASIA'];
			$arrDados['telefone1']       = $dados[0]['TELEFONEGERAL'];
			$arrDados['email']           = $dados[0]['EMAILGERAL'];
			$arrDados['site']            = $dados[0]['SITE'];
			$arrDados['login']           = $dados[0]['LOGIN'];
			$arrDados['idlogradouro']    = $dados[0]['IDLOGRADOURO'];
			$arrDados['cep']             = $dados[0]['CEP'];
			$arrDados['uf']              = $dados[0]['IDUF'];
			$arrDados['cidade']          = $dados[0]['NOMECIDADESUB'];
			$arrDados['bairro']          = $dados[0]['NOMEBAIRRO'];
			$arrDados['logradouro']      = $dados[0]['NOMELOGRADOURO2'];
			$arrDados['logcomplemento']  = $dados[0]['COMPLEMENTO'];
			$arrDados['end_numero']      = $dados[0]['END_NUMERO'];
			$arrDados['end_complemento'] = $dados[0]['END_COMPLEMENTO'];
			$arrDados['qtd_titulos']     = $dados[0]['QTD_TITULO_ATIVOS'];
			$arrDados['assoc_ent_sind']  = $dados[0]['ASSOC_ENT_SINDICAL'];
			$arrDados['fil_ent_assoc']   = $dados[0]['FILIACAO_ENT_ASSOC'];
			$arrDados['nomeresp']        = $dados[0]['NOMERESP'];
			$arrDados['telefoneresp']    = $dados[0]['TELEFONERESP'];
			$arrDados['skyperesp']       = $dados[0]['SKYPERESP'];
			$arrDados['emailresp']       = $dados[0]['EMAILRESP'];
			$arrDados['socios']          = $this->usuario_class->get_socio($idUsuario, 'string');
			$arrDados['atuacao']         = $this->usuario_class->get_atuacao($idUsuario, 'string');
			$arrDados['loadDadosEdit']   = 1;
			
			$acao = '';
		}
		
		if ($acao == '')
		{
			$arrDados['natjur']    = $this->naturezajur_model->getCombo($arrDados['tipopessoa2']);
			$arrDados['atividade'] = $this->global_model->selectx('atividade', 'ATIVO = \'S\'', 'NOMEATIVIDADE');
			
			$this->parser->parse('editora/cadastro_formalt_view', $arrDados);
		}
	}
	
	function livro_precad($acao = 'valida')
	{
		$this->load->model('configuracao_model');
		
		if ($this->configuracao_model->validaConfig('5', date('Y/m/d')))
		{
			$this->load->helper('form');
			$this->load->library('form_validation');
			$this->load->model('global_model');
			$this->load->model('editora_model');
			$this->load->model('isbn_model');
			$this->load->model('livro_model');
			
			$arrDados  = Array();
			$idUsuario = $this->session->userdata('idUsuario');
			
			if ($acao == 'valida')
			{
				$this->form_validation->set_rules('isbn_livro', 'Código ISBN', 'trim|required|max_length[13]|xss_clean');
				
				if ($this->form_validation->run() == TRUE)
				{
                    $isbn_livro = $this->input->post('isbn_livro');
                    
                    // verifica se o ISBN contem 10 digitos e adiciona o prefixo 978
                    if(strlen($isbn_livro) == 10)
                    {
                        $isbn_livro = '978' . $isbn_livro;
                    }
                    
                    // verifica se o ISBN contem 13 digitos
                    if(strlen($isbn_livro) == 13)
                    {
                        $arrISBN = Array();
                        $arrISBN['idlivro']        = 0;
                        $arrISBN['idisbn']         = $isbn_livro;
                        $arrISBN['isbn']           = $isbn_livro;
                        $arrISBN['fichacat_aux']   = 'S';
                        $arrISBN['preco']          = '';
                        $arrISBN['formato_a']      = '';
                        $arrISBN['formato_b']      = '';    
                        $arrISBN['papel_miolo']    = '';
                        $arrISBN['papel_capa']     = '';
                        $arrISBN['peso']           = '';
                        $arrISBN['orelha_aux']     = 'S';
                        $arrISBN['tipocapa_outra'] = '';
                        $arrISBN['sinopse']        = '';
                        $arrISBN['linkdados']      = '';
                        $arrISBN['loadDadosEdit']  = 1;
                        $arrISBN['palavraschave']  = '';
						
                        $arrISBN['suporte']    = $this->livro_model->listaSuporte();
                        $arrISBN['tipocapa']   = $this->livro_model->listaTipoCapa();
                        $arrISBN['acabamento'] = $this->livro_model->listaAcabamento();
                        $arrISBN['idioma']     = $this->livro_model->listaIdioma();
						
                        $where = sprintf("ISBN = '%s'", $this->input->post('isbn_livro'));
                        $livroJaExiste = $this->global_model->get_dado('livro', '1', $where);
						
                        if ($livroJaExiste == 1)
                        {
                            $arrDados['outrosErros'] = 'Livro já cadastrado.';
                            $acao = '';
                        }
                        else
                        {
                            // verifica se o ISBN é do Brasil = 85
                            $digito = substr($isbn_livro, 3, 2);
							
                            if($digito == '85')
                            {
                                //buscar os dados baseado no ISBN e popular campos do formulário, se não encontrar nao deve permitir a sequencia do cadastro
                                $isbnOnline = $this->isbn_model->getIsbn(substr($isbn_livro, 0, 12));
								
                                if (count($isbnOnline) == 1)
                                {
                                    //deve validar se o CPF/CNPJ da editora logada é o mesmo dos dados do ISBN
                                    $arrEditora = $this->isbn_model->getDadosEditora($isbnOnline[0]['IDEDITORA'], $isbnOnline[0]['TIPOEDITORA']);
									
                                    if (count($arrEditora) == 1)
                                    {
                                        $userCpfCnpj = $this->global_model->get_dado('usuario', 'CPF_CNPJ', 'IDUSUARIO = ' . $this->session->userdata('idUsuario'));
										
                                        $caracS      = Array('.','-','/');
                                        $isbnCpfCnpj = str_replace($caracS, '', $arrEditora[0]['CPFCNPJ']);
                                        $userCpfCnpj = str_replace($caracS, '', $userCpfCnpj);
										
                                        if ($isbnCpfCnpj == $userCpfCnpj)
                                        {
                                            //pegando nome dos autores
                                            $idObra = $isbnOnline[0]['IDOBRA'];
											
                                            $arrAutor  = $this->isbn_model->getAutoresObra($idObra);
                                            $arrAutor2 = Array();
											
                                            for ($i = 0; $i < count($arrAutor); $i++)
                                            {
                                                array_push($arrAutor2, $arrAutor[$i]['PAR_TX_NOME']);
                                            }
											
                                            $arrISBN['titulo']         = $isbnOnline[0]['TITULO'];
                                            $arrISBN['autor']          = implode(' / ', $arrAutor2);
                                            $arrISBN['suporte_aux']    = ($isbnOnline[0]['IDSUPORTE'] == '' ? 0 : $isbnOnline[0]['IDSUPORTE']);
                                            $arrISBN['npaginas']       = $isbnOnline[0]['NPAGINAS'];
                                            $arrISBN['tipocapa_aux']   = ($isbnOnline[0]['SIGLACAPA'] == '' ? 0 : $this->global_model->get_dado("isbntipocapa", "IDISBNTIPOCAPA", "SIGLACAPA = '" . $isbnOnline[0]['SIGLACAPA'] . "'"));
                                            $arrISBN['acabamento_aux'] = ($isbnOnline[0]['SIGLAACAB'] == '' ? 0 : $this->global_model->get_dado("isbnacabamento", "IDISBNACABAMENTO", "SIGLAACABAMENTO = '" . $isbnOnline[0]['SIGLAACAB'] . "'"));
                                            $arrISBN['idioma_aux']     = ($isbnOnline[0]['IDIDIOMAORIG'] == '' ? 0 : $isbnOnline[0]['IDIDIOMAORIG']);
                                            $arrISBN['idiomatrad_aux'] = ($isbnOnline[0]['IDIDIOMATRAD'] == '' ? 0 : $isbnOnline[0]['IDIDIOMATRAD']);
                                            $arrISBN['idassunto']      = ($isbnOnline[0]['IDASSUNTO'] != '' ? $isbnOnline[0]['IDASSUNTO'] : '');
                                            $arrISBN['assunto']        = ($isbnOnline[0]['IDASSUNTO'] != '' ? $this->global_model->get_dado("isbnassunto", "DESCASSUNTO", "IDISBNASSUNTO = " . $isbnOnline[0]['IDASSUNTO']) : '');
                                            $arrISBN['edicao']         = $isbnOnline[0]['EDICAO'];
                                            $arrISBN['ano']            = $isbnOnline[0]['EDICAOANO'];
											
                                            $this->parser->parse('editora/livro_formcad_view', $arrISBN);
                                        }
                                        else
                                        {
                                            $arrDados['outrosErros'] = 'O CNPJ/CPF da editora responsável pelo código ISBN ' . $this->input->post('isbn_livro') . ' não confere com o da sua editora.';
                                            $acao = '';
                                        }
                                    }
                                    else
                                    {
                                        $arrDados['outrosErros'] = 'Editora responsável pelo código ISBN ' . $this->input->post('isbn_livro') . ' não encontrada.';
                                        $acao = '';
                                    }
                                }
                                else
                                {
                                    $arrDados['outrosErros'] = 'Nenhum livro encontrado com o código ISBN ' . $this->input->post('isbn_livro') . '.';
                                    $acao = '';
                                }
                            }
                            else
                            {
								$arrISBN['titulo']         = '';
								$arrISBN['autor']          = '';
								$arrISBN['suporte_aux']    = 0;
								$arrISBN['npaginas']       = '';
								$arrISBN['tipocapa_aux']   = 0;
								$arrISBN['acabamento_aux'] = 0;
								$arrISBN['idioma_aux']     = 0;
								$arrISBN['idiomatrad_aux'] = 0;
								$arrISBN['idassunto']      = '';
								$arrISBN['assunto']        = '';
								$arrISBN['edicao']         = '';
								$arrISBN['ano']            = '';

								$this->parser->parse('editora/livro_formcad_view', $arrISBN);
                            }
                        }
                    }
                    else
                    {
                        $arrDados['outrosErros'] = 'O código ISBN deve conter 10 ou 13 dígitos.';
                        $acao = ''; 
                    }
				}
				else
				{
					$acao = '';
				}
			}
			
			//informações zeradas para nao dar erro no parse
			$arrDados['isbn_livro']  = '';
			
			if ($acao == '')
			{
				$this->parser->parse('editora/livro_precad_view', $arrDados);
			}
		}
		else
		{
			$config = $this->configuracao_model->getConfigData('5');
			$msgPer = ($config[0]['DATA_DE'] != '' ? '<b>Período para cadastro/importação de livros:</b> ' . eua_to_br($config[0]['DATA_DE']) . ' até ' . eua_to_br($config[0]['DATA_ATE']) . '.' : '');
			
			$arrErro['msg']      = '<b>Cadastro não permitido.</b><br /><br /><b>Motivo:</b> Fora do período de cadastros.<br /><br />' . $msgPer;
			$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
			$this->load->view('erro_view', $arrErro);
		}
	}
	
	function livro_formcad($idLivroEdit = 0, $acao = 'edit')
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('usuario_class');
		$this->load->model('global_model');
		$this->load->model('livro_model');
		$this->load->model('isbn_model');
		$this->load->model('configuracao_model');
		
		$arrDados  = Array();
		
		if ($acao == 'save')
		{
			$this->form_validation->set_rules('idlivro',       'Livro',                'required|xss_clean');
			$this->form_validation->set_rules('idisbn',        'Código ISBN',          'trim|required|max_length[13]|xss_clean');
			$this->form_validation->set_rules('isbn',          'Código ISBN',          'trim|required|max_length[13]|xss_clean');
			$this->form_validation->set_rules('titulo',        'Título',               'trim|required|max_length[250]|xss_clean');
			$this->form_validation->set_rules('autor',         'Autor',                'trim|required|max_length[200]|xss_clean');
			$this->form_validation->set_rules('suporte',       'Suporte',              'valida_combo|xss_clean');
			$this->form_validation->set_rules('fichacat',      'Ficha Catalográfica',  'trim|required|xss_clean');
			$this->form_validation->set_rules('preco',         'Preço',                'required|valida_moeda');
			$this->form_validation->set_rules('npaginas',      'Número de Páginas',    'required|integer|is_natural_no_zero');
			$this->form_validation->set_rules('formato_a',     'Dimensão Menor',       'integer|is_natural_no_zero');
			$this->form_validation->set_rules('formato_b',     'Dimensão Maior',       'integer|is_natural_no_zero');
			$this->form_validation->set_rules('papel_miolo',   'Papel Miolo',          'integer|is_natural_no_zero');
			$this->form_validation->set_rules('papel_capa',    'Papel Capa',           'integer|is_natural_no_zero');
			$this->form_validation->set_rules('peso',          'Peso',                 'integer|is_natural_no_zero');
			$this->form_validation->set_rules('orelha',        'Orelha',               'trim|required|xss_clean');
			$this->form_validation->set_rules('tipocapa',      'Tipo de Capa',         'valida_combo|xss_clean');
			$this->form_validation->set_rules('tipocapa_outra','Outro Tipo de Capa',   'trim|max_length[50]|xss_clean');
			$this->form_validation->set_rules('acabamento',    'Acabamento',           'valida_combo|xss_clean');
			$this->form_validation->set_rules('idioma',        'Idioma Original',      'valida_combo|xss_clean');
			$this->form_validation->set_rules('idiomatrad',    'Idioma Tradução',      'xss_clean');
			$this->form_validation->set_rules('idassunto',     'Assunto',              'required');
			$this->form_validation->set_rules('assunto',       'Assunto',              '');
			$this->form_validation->set_rules('edicao',        'Edição',               'trim|required|max_length[20]|xss_clean');
			$this->form_validation->set_rules('ano',           'Ano',                  'required|integer|is_natural_no_zero|exact_length[4]');
			$this->form_validation->set_rules('palavraschave', 'Palavras-Chave',       'trim|max_length[50]|xss_clean');
			$this->form_validation->set_rules('sinopse',       'Sinopse',              'trim|max_length[500]|xss_clean');
			$this->form_validation->set_rules('linkdados',     'Link para Dados',      'trim|max_length[200]|xss_clean');
			
			$idLivroEdit = $this->input->post('idlivro');
			
			if ($this->form_validation->run() == TRUE)
			{
				$where = sprintf("ISBN = '%s' AND IDLIVRO != %s", $this->input->post('idisbn'), $idLivroEdit);
				$livroJaExiste = $this->global_model->get_dado('livro', '1', $where);
				
				//validações para quando está inserindo
				$validaIsbn = 0;
				$periodoOk  = TRUE;
				
				if ($idLivroEdit == 0)
				{
					//valida período de cadastro
					$periodoOk = $this->configuracao_model->validaConfig('5', date('Y/m/d'));
					
                    // verifica se o ISBN é do Brasil = 85
                    $digito = substr($this->input->post('idisbn'), 3, 2);
                    if($digito == '85')
                    {
                        //verifica se existe ISBN, se não encontrar nao deve permitir a sequencia do cadastro
                        $isbnOnline = $this->isbn_model->getIsbn(substr($this->input->post('idisbn'), 0, 12));

                        if (count($isbnOnline) == 1)
                        {
                            //deve validar se o CPF/CNPJ da editora logada é o mesmo dos dados do ISBN
                            $arrEditora = $this->isbn_model->getDadosEditora($isbnOnline[0]['IDEDITORA'], $isbnOnline[0]['TIPOEDITORA']);

                            if (count($arrEditora) == 1)
                            {
                                $userCpfCnpj = $this->global_model->get_dado('usuario', 'CPF_CNPJ', 'IDUSUARIO = ' . $this->session->userdata('idUsuario'));

                                $caracS      = Array('.','-','/');
                                $isbnCpfCnpj = str_replace($caracS, '', $arrEditora[0]['CPFCNPJ']);
                                $userCpfCnpj = str_replace($caracS, '', $userCpfCnpj);

                                if ($isbnCpfCnpj == $userCpfCnpj)
                                {
                                    $validaIsbn = 1;
                                }
                                else
                                {
                                    $validaIsbn    = 4;
                                    $msgValidaIsbn = 'O CNPJ/CPF da editora responsável pelo código ISBN ' . $this->input->post('idisbn') . ' não confere com o da sua editora.';
                                }
                            }
                            else
                            {
                                $validaIsbn    = 3;
                                $msgValidaIsbn = 'Editora responsável pelo código ISBN ' . $this->input->post('idisbn') . ' não encontrada.';
                            }
                        }
                        else
                        {
                            $validaIsbn    = 2;
                            $msgValidaIsbn = 'Nenhum livro encontrado com o código ISBN ' . $this->input->post('idisbn') . '.';
                        }
                    }
                    else
                    {
                        $validaIsbn = 1;
                    }
				}
				else
				{
					$validaIsbn = 1;
				}
				
				//valida dimensoes de acordo com as configurações
				$dimensaoOK = 1;
				
				if ($this->input->post('formato_a') != '' || $this->input->post('formato_b') != '')
				{
					if ( ! is_numeric($this->input->post('formato_a')) || ! $this->configuracao_model->validaConfig(Array(15,16), $this->input->post('formato_a')) ||  ! is_numeric($this->input->post('formato_b')) || ! $this->configuracao_model->validaConfig(Array(17,18), $this->input->post('formato_b')))
					{
						$dimensaoOK = 0;
					}
				}
				
				if ( ! $periodoOk)
				{
					$arrDados['outrosErros'] = 'Cadastro não permitido. Motivo: Fora do período de cadastros.';
					$acao = '';
				}
				elseif ($this->input->post('idisbn') != $this->input->post('isbn'))
				{
					$arrDados['outrosErros'] = 'Código ISBN alterado.';
					$acao = '';
				}
				elseif ($livroJaExiste == 1)
				{
					$arrDados['outrosErros'] = 'Livro já cadastrado.';
					$acao = '';
				}
				elseif ($validaIsbn != 1)
				{
					$arrDados['outrosErros'] = $msgValidaIsbn;
					$acao = '';
				}
				elseif ($dimensaoOK != 1)
				{
					$config15 = $this->configuracao_model->getConfigData('15');
					$config16 = $this->configuracao_model->getConfigData('16');
					$config17 = $this->configuracao_model->getConfigData('17');
					$config18 = $this->configuracao_model->getConfigData('18');
					
					$arrDados['outrosErros'] = 'A Dimensão Menor deve ter entre ' . $config15[0]['VALORINT'] . ' e ' . $config16[0]['VALORINT'] . 'cm e a Dimensão Maior deve ter entre ' . $config17[0]['VALORINT'] . ' e ' . $config18[0]['VALORINT'] . 'cm.';
					$acao = '';
				}
				elseif ($this->input->post('tipocapa') == 3 && $this->input->post('tipocapa_outra') == '')
				{
					$arrDados['outrosErros'] = 'Informe o tipo de capa.';
					$acao = '';
				}
				else
				{
					$editoraOk = 1;
					
					if ($idLivroEdit != 0)
					{
						//validar se editora do livro é a mesma que está logada
						$editoraLivro = $this->global_model->get_dado('livro', 'IDUSUARIO', 'IDLIVRO = ' . $idLivroEdit);
						if ($editoraLivro != $this->session->userdata('idUsuario'))
						{
							$editoraOk = 0;
						}
					}
					
					if ($editoraOk == 1)
					{
						$arrDadosLivro = Array();
						$arrDadosLivro['IDISBNTIPOCAPA']      = ($this->input->post('tipocapa') != 0 ? $this->input->post('tipocapa') : NULL);
						$arrDadosLivro['IDISBNACABAMENTO']    = ($this->input->post('acabamento') != 0 ? $this->input->post('acabamento') : NULL);
						$arrDadosLivro['IDISBNSUPORTE']       = ($this->input->post('suporte') != 0 ? $this->input->post('suporte') : NULL);
						$arrDadosLivro['IDISBNASSUNTO']       = ($this->input->post('idassunto') != '' ? $this->input->post('idassunto') : NULL);
						$arrDadosLivro['IDISBNIDIOMA']        = ($this->input->post('idioma') != 0 ? $this->input->post('idioma') : NULL);
						$arrDadosLivro['IDISBNIDIOMA_TRAD']   = ($this->input->post('idiomatrad') != 0 ? $this->input->post('idiomatrad') : NULL);
						$arrDadosLivro['TITULO']              = removeAcentos($this->input->post('titulo'), TRUE);
						$arrDadosLivro['FICHACAT']            = ($this->input->post('fichacat') == 'S' ? 'S' : 'N');
						$arrDadosLivro['PRECONORMAL']         = getValor($this->input->post('preco'));
						$arrDadosLivro['FORMATO_A']           = ($this->input->post('formato_a') == '' ? 0 : $this->input->post('formato_a'));
						$arrDadosLivro['FORMATO_B']           = ($this->input->post('formato_b') == '' ? 0 : $this->input->post('formato_b'));
						$arrDadosLivro['NPAGINAS']            = $this->input->post('npaginas');
						$arrDadosLivro['PAPEL_MIOLO']         = ($this->input->post('papel_miolo') == '' ? 0 : $this->input->post('papel_miolo'));
						$arrDadosLivro['PAPEL_CAPA']          = ($this->input->post('papel_capa') == '' ? 0 : $this->input->post('papel_capa'));
						$arrDadosLivro['PESO']                = ($this->input->post('peso') == '' ? 0 : $this->input->post('peso'));
						$arrDadosLivro['ORELHA']              = ($this->input->post('orelha') == 'S' ? 'S' : 'N');
						$arrDadosLivro['AUTOR']               = removeAcentos($this->input->post('autor'), TRUE);
						$arrDadosLivro['EDICAO']              = $this->input->post('edicao');
						$arrDadosLivro['ANO']                 = $this->input->post('ano');
						$arrDadosLivro['PALAVRASCHAVE']       = removeAcentos($this->input->post('palavraschave'), TRUE);
						$arrDadosLivro['SINOPSE']             = $this->input->post('sinopse');
						$arrDadosLivro['LINKDADOS']           = $this->input->post('linkdados');
						$arrDadosLivro['TIPOCAPA_OUTRA']      = ($this->input->post('tipocapa') == 3 ? removeAcentos($this->input->post('tipocapa_outra'), TRUE) : NULL);
						$arrDadosLivro['DATA_ULT_ATU']        = date('Y/m/d H:i:s');
						$arrDadosLivro['CONFIRMACAO']         = 'S';
						
						$this->db->trans_start();
						
						if ($idLivroEdit == 0)
						{
							$arrDadosLivro['ISBN']         = $this->input->post('idisbn');
							$arrDadosLivro['IDUSUARIO']    = $this->session->userdata('idUsuario');
							$arrDadosLivro['PRECOPOPULAR'] = 0;
							$arrDadosLivro['ATIVO']        = 'S';
							
							$this->global_model->insert('livro', $arrDadosLivro);
							$idLivroAux = $this->global_model->get_insert_id();
							
							$opLog   = 'INSERT';
							$descLog = 'Cadastro de novo livro';
						}
						else
						{
							$where  = sprintf("IDLIVRO = %s", $idLivroEdit);
							$this->global_model->update('livro', $arrDadosLivro, $where);
							$idLivroAux = $idLivroEdit;
							
							$opLog   = 'UPDATE';
							$descLog = 'Atualização de cadastro de livro';
						}
						
						$this->db->trans_complete();
						
						if ($this->db->trans_status() === FALSE)
						{
							//salva log
							$this->load->library('logsis', Array($opLog, 'ERRO', 'livro', $idLivroAux, $descLog, var_export($arrDadosLivro, TRUE)));
							
							$arrErro = Array();
							$arrErro['msg']      = 'Ocorreu um erro ao salvar o cadastro.';
							$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
							$this->load->view('erro_view', $arrErro);
						}
						else
						{
							//salva log
							$this->load->library('logsis', Array($opLog, 'OK', 'livro', $idLivroAux, $descLog, var_export($arrDadosLivro, TRUE)));
							
							$arrSucesso = Array();
							$arrSucesso['msg']      = ($idLivroEdit == 0 ? 'Cadastro realizado com sucesso!' : 'Cadastro atualizado com sucesso!');
							$arrSucesso['link']     = 'editora/livros';
							$arrSucesso['txtlink']  = 'Ir para Livros';
							$arrSucesso['tipoUser'] = $this->session->userdata('tipoUsuario');
							$this->load->view('sucesso_view', $arrSucesso);
						}
					}
					else
					{
						$arrErro = Array();
						$arrErro['msg']      = 'Livro inválido para sua editora.';
						$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
						$this->load->view('erro_view', $arrErro);
					}
				}
			}
			else
			{
				$acao = '';
			}
		}
		
		//informações zeradas para nao dar erro no parse
		$arrDados['idlivro']        = $idLivroEdit;
		$arrDados['idisbn']         = '';
		$arrDados['isbn']           = '';
		$arrDados['titulo']         = '';
		$arrDados['autor']          = '';
		$arrDados['suporte_aux']    = 0;
		$arrDados['fichacat_aux']   = 'S';
		$arrDados['preco']          = '';
		$arrDados['formato_a']      = '';
		$arrDados['formato_b']      = '';
		$arrDados['npaginas']       = '';
		$arrDados['papel_miolo']    = '';
		$arrDados['papel_capa']     = '';
		$arrDados['peso']           = '';
		$arrDados['orelha_aux']     = 'S';
		$arrDados['tipocapa_aux']   = 0;
		$arrDados['tipocapa_outra'] = '';
		$arrDados['acabamento_aux'] = 0;
		$arrDados['idioma_aux']     = 0;
		$arrDados['idiomatrad_aux'] = 0;
		$arrDados['idassunto']      = '';
		$arrDados['assunto']        = '';
		$arrDados['edicao']         = '';
		$arrDados['ano']            = '';
		$arrDados['palavraschave']  = '';
		$arrDados['sinopse']        = '';
		$arrDados['linkdados']      = '';
		$arrDados['loadDadosEdit']  = 0;
		
		if ($acao == 'edit')
		{
			$editoraLivro = $this->global_model->get_dado('livro', 'IDUSUARIO', 'IDLIVRO = ' . $idLivroEdit);
			
			if ($editoraLivro == $this->session->userdata('idUsuario'))
			{
				$dados = $this->livro_model->getDadosCadastro($idLivroEdit);
				
				if (count($dados) > 0)
				{
					$arrDados['idlivro']        = $idLivroEdit;
					$arrDados['idisbn']         = $dados[0]['ISBN'];
					$arrDados['isbn']           = $dados[0]['ISBN'];
					$arrDados['titulo']         = $dados[0]['TITULO'];
					$arrDados['autor']          = $dados[0]['AUTOR'];
					$arrDados['suporte_aux']    = $dados[0]['IDISBNSUPORTE'];
					$arrDados['fichacat_aux']   = $dados[0]['FICHACAT'];
					$arrDados['preco']          = masc_real($dados[0]['PRECONORMAL']);
					$arrDados['formato_a']      = ($dados[0]['FORMATO_A'] == 0 ? '' : $dados[0]['FORMATO_A']);
					$arrDados['formato_b']      = ($dados[0]['FORMATO_B'] == 0 ? '' : $dados[0]['FORMATO_B']);
					$arrDados['npaginas']       = ($dados[0]['NPAGINAS'] == 0 ? '' : $dados[0]['NPAGINAS']);
					$arrDados['papel_miolo']    = ($dados[0]['PAPEL_MIOLO'] == 0 ? '' : $dados[0]['PAPEL_MIOLO']);
					$arrDados['papel_capa']     = ($dados[0]['PAPEL_CAPA'] == 0 ? '' : $dados[0]['PAPEL_CAPA']);
					$arrDados['peso']           = ($dados[0]['PESO'] == 0 ? '' : $dados[0]['PESO']);
					$arrDados['orelha_aux']     = $dados[0]['ORELHA'];
					$arrDados['tipocapa_aux']   = $dados[0]['IDISBNTIPOCAPA'];
					$arrDados['tipocapa_outra'] = $dados[0]['TIPOCAPA_OUTRA'];
					$arrDados['acabamento_aux'] = $dados[0]['IDISBNACABAMENTO'];
					$arrDados['idioma_aux']     = $dados[0]['IDISBNIDIOMA'];
					$arrDados['idiomatrad_aux'] = $dados[0]['IDISBNIDIOMA_TRAD'];
					$arrDados['idassunto']      = $dados[0]['IDISBNASSUNTO'];
					$arrDados['assunto']        = $dados[0]['DESCASSUNTO'];
					$arrDados['edicao']         = $dados[0]['EDICAO'];
					$arrDados['ano']            = $dados[0]['ANO'];
					$arrDados['palavraschave']  = $dados[0]['PALAVRASCHAVE'];
					$arrDados['sinopse']        = $dados[0]['SINOPSE'];
					$arrDados['linkdados']      = $dados[0]['LINKDADOS'];
					$arrDados['loadDadosEdit']  = 1;
					
					$acao = '';
				}
				else
				{
					$arrErro = Array();
					$arrErro['msg']      = 'Livro não encontrado.';
					$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
					$this->load->view('erro_view', $arrErro);
				}
			}
			else
			{
				$arrErro = Array();
				$arrErro['msg']      = 'Livro não encontrado.';
				$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
				$this->load->view('erro_view', $arrErro);
			}
		}
		
		if ($acao == '')
		{
			$arrDados['suporte']    = $this->livro_model->listaSuporte();
			$arrDados['tipocapa']   = $this->livro_model->listaTipoCapa();
			$arrDados['acabamento'] = $this->livro_model->listaAcabamento();
			$arrDados['idioma']     = $this->livro_model->listaIdioma();
			
			$this->parser->parse('editora/livro_formcad_view', $arrDados);
		}
	}
	
	function livros($nPag = 1)
	{
        $this->load->library('form_validation');
		$this->load->model('livro_model');
        $this->load->model('global_model');
        $this->load->model('configuracao_model');
		
		$arrDados = Array();
		$arrDados['nPag']      = (is_numeric($nPag) && $nPag > 0 ? $nPag : 1);
		$arrDados['exibir_pp'] = 15;
		$arrDados['limit_de']  = ($arrDados['nPag'] - 1) * $arrDados['exibir_pp'];
        
        $arrDados['habilitado'] = $this->global_model->get_dado('usuarioprograma', "'S'", 'IDUSUARIO = ' . $this->session->userdata('idUsuario') . ' AND IDPROGRAMA =' . $this->session->userdata('programa'));
        $arrDados['boolPeriodoInclusaoLivro'] = ($this->configuracao_model->validaConfig('6', date('Y/m/d'))) ? TRUE : FALSE;
        
        //cria array para filtros
        $where = Array();
        array_push($where, " l.IDUSUARIO = '" . $this->session->userdata('idUsuario') . "' ");
		
        $this->form_validation->set_rules('filtro_status','Status','trim|xss_clean');
        $this->form_validation->set_rules('filtro_livro_popular','Situação no Edital','trim|xss_clean');
        $this->form_validation->set_rules('filtro_programa','Edital','trim|xss_clean');
        $this->form_validation->set_rules('filtro_isbn','ISBN','trim|xss_clean');
        $this->form_validation->set_rules('filtro_titulo','Título','trim|xss_clean');
        $this->form_validation->set_rules('filtro_autor','Autor','trim|xss_clean');
            
        if($this->form_validation->run() == TRUE || $this->input->post() == FALSE)
        {
            $status         = $this->input->post('filtro_status');
            $livro_popular  = $this->input->post('filtro_livro_popular');
            $isbn           = $this->input->post('filtro_isbn');
            $titulo         = $this->input->post('filtro_titulo');
            $autor          = $this->input->post('filtro_autor');
            $programa       = $this->input->post('filtro_programa');
 
            // Filtro para programa
           $arrDados['programa'] = " AND pl.IDPROGRAMA = " . $this->session->userdata('programa');

            // Filtro para confirmação
            if($status != '')
            {
                array_push($where, " l.CONFIRMACAO = '" . $status . "' ");
            }
            
            // Filtro para livor popular
            if($livro_popular != '')
            {
                if($livro_popular == 'S')
                {
                    array_push($where, " pl.idlivro is not null ");
                }
                else
                {
                    array_push($where, " pl.idlivro is null ");
                }
            }

            // Filtro para confirmação
            if($isbn != '')
            {
                if(strlen($isbn) == 10)
                {
                    $isbn = '978' . $isbn;
                }
                
                array_push($where, " l.ISBN = '" . $isbn . "' ");
            }

            // Filtro para Título
            if($titulo != '')
            {
                array_push($where, " l.TITULO LIKE '%" . $titulo . "%' ");
            }

            // Filtro para Autor
            if($autor != '')
            {
                array_push($where, " l.AUTOR LIKE '%" . $autor . "%' ");
            }
            
        }

        // set filtros para pesquisa.
        $arrDados['where'] = implode(" AND ", $where);
        
		//pesquisa
		$arrDados['livros'] = $this->livro_model->lista($arrDados, FALSE);
		$tamArr             = count($arrDados['livros']);
		$arrDados['vazio']  = ($tamArr == 0 ? TRUE : FALSE);
		
		for ($i = 0; $i < $tamArr; $i++)
		{
			$arrDados['livros'][$i]['NLINHA'] = $i + 1 + $arrDados['limit_de'];
            // verifica se o livro foi adiconado ao programa
            $arrDados['livros'][$i]['LIVROPOPULAR'] = ($arrDados['livros'][$i]['LIVROPOPULAR'] == '')? 'Não Incluído' : 'Incluído';
		}
		
		$arrDados['livros'] = add_corLinha($arrDados['livros'], 1);
		
		//dados da paginação
		$qtdReg = $this->livro_model->lista($arrDados, TRUE);
		$nPaginas = $qtdReg[0]['CONT'] / $arrDados['exibir_pp'];
		
		if ($qtdReg[0]['CONT'] % $arrDados['exibir_pp'] != 0)
		{
			$nPaginas = (int)$nPaginas + 1;
		}
		
		$arrDados['nPaginas'] = $nPaginas;
		$arrDados['qtdReg']   = $qtdReg[0]['CONT'];
		
		$this->parser->parse('editora/livro_lista_view', $arrDados);
	}
	
	function assunto_autocomplete()
	{
		$dadopesq = $this->input->get('query');
		
		$arrDesc  = Array();
		$arrValue = Array();
		
		$this->load->model('livro_model');
		
		$arrDados  = $this->livro_model->listaAssunto(strtoupper($dadopesq));
		$qtdResult = count($arrDados);
		
		if ($qtdResult > 0)
		{
			for ($i = 0; $i < $qtdResult; $i++)
			{
				array_push($arrDesc,  $arrDados[$i]['DESCASSUNTO']);
				array_push($arrValue, $arrDados[$i]['IDISBNASSUNTO']);
			}
			
			$retorno = "{query:'" . $dadopesq . "', suggestions:['" . implode("','", $arrDesc) . "'], data:['" . implode("','", $arrValue) . "']}";
		}
		else
		{
			$retorno = "{query:'" . $dadopesq . "', suggestions:['nenhum item encontrado'], data:['0']}";
		}
		
		echo $retorno;
	}
    
    function livroImportar()
    {
		$this->load->model('configuracao_model');
		
		if ($this->configuracao_model->validaConfig('5', date('Y/m/d')))
		{
			$this->load->library('form_validation');
			$this->load->view('editora/livro_importar_view');
		}
		else
		{
			$config = $this->configuracao_model->getConfigData('5');
			$msgPer = ($config[0]['DATA_DE'] != '' ? '<b>Período para cadastro/importação de livros:</b> ' . eua_to_br($config[0]['DATA_DE']) . ' até ' . eua_to_br($config[0]['DATA_ATE']) . '.' : '');
			
			$arrErro['msg']      = '<b>Importação não permitida.</b><br /><br /><b>Motivo:</b> Fora do período de cadastros.<br /><br />' . $msgPer;
			$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
			$this->load->view('erro_view', $arrErro);
		}
    }
    
    function uploadImportar()
    {
		$this->load->model('configuracao_model');
		
		if ($this->configuracao_model->validaConfig('5', date('Y/m/d')))
		{
			$arrLog   = Array();
			$arrDados = Array();
			$arrDados['sucessImport'] = 'S';
			
			$this->load->library('form_validation');
			$this->load->model('isbn_model');
			$this->load->model('usuario_model');
			$this->load->model('global_model');
			
			$config = Array();
			$config['upload_path']   = './application/temp/';
			$config['allowed_types'] = 'txt';
			$config['max_size']      = '500';
			$config['max_filename']  = '50';

			$this->load->library('upload', $config);
			
			$contTotal  = 0;
			$contSucess = 0;

			if ( ! $this->upload->do_upload('arquivo_livro'))
			{
				$arrDados['sucessImport'] = 'N';
				$arrDados['outrosErros']= $this->upload->display_errors();
			}
			else
			{
				$arquivo = $this->upload->data();
				$data    = file($arquivo['full_path']);
				
				// Apaga arquivo do servidor
				@unlink($arquivo['full_path']);
				
				// valida se tem mais de 100 registros 
				if(count($data) > 0 && count($data) <= 100)
				{
					$userCpfCnpj = $this->global_model->get_dado("usuario","CPF_CNPJ","IDUSUARIO = ".$this->session->userdata('idUsuario'));
					
					$arrRep = Array('.','-','/');
					$userCpfCnpj = str_replace($arrRep,'', $userCpfCnpj);
					
					foreach ($data as $key => $isbn) 
					{
						$isbn = trim($isbn);
                        if(strlen($isbn) == 10)
                        {
                            $isbn = '978' . $isbn;
                        }
                        
						$contTotal++;
						
						// Buscar os dados ISBN para inserir na tabela livros
						$isbnOnline = $this->isbn_model->getIsbn(substr($isbn, 0, 12));
						
						if (count($isbnOnline) > 0)
						{
							// Deve validar se o CPF/CNPJ da editora logada é o mesmo dos dados do ISBN
							$arrEditora = $this->isbn_model->getDadosEditora($isbnOnline[0]['IDEDITORA'], $isbnOnline[0]['TIPOEDITORA']);
							
							if (count($arrEditora) == 1)
							{
								$isbnCpfCnpj = str_replace($arrRep, '', $arrEditora[0]['CPFCNPJ']);

								if ($isbnCpfCnpj == $userCpfCnpj)
								{
									$livroJaExiste = $this->global_model->get_dado('livro', '1', sprintf("ISBN = '%s'", $isbn));
									
									if($livroJaExiste != 1)
									{
										// Pegando nome dos autores
										$idObra = $isbnOnline[0]['IDOBRA'];

										$arrAutor  = $this->isbn_model->getAutoresObra($idObra);
										$arrAutor2 = Array();

										for ($i = 0; $i < count($arrAutor); $i++)
										{
											array_push($arrAutor2, $arrAutor[$i]['PAR_TX_NOME']);
										}
										
										// Array que de dados para inserir na tabela livros
										$arrLivro = Array();
										$arrLivro['ISBN']                = $isbn;
										$arrLivro['IDISBNTIPOCAPA']      = ($isbnOnline[0]['SIGLACAPA'] == '' ? NULL : $this->global_model->get_dado("isbntipocapa", "IDISBNTIPOCAPA", "SIGLACAPA = '" . $isbnOnline[0]['SIGLACAPA'] . "'"));
										$arrLivro['IDISBNACABAMENTO']    = ($isbnOnline[0]['SIGLAACAB'] == '' ? NULL : $this->global_model->get_dado("isbnacabamento", "IDISBNACABAMENTO", "SIGLAACABAMENTO = '" . $isbnOnline[0]['SIGLAACAB'] . "'"));
										$arrLivro['IDISBNSUPORTE']       = ($isbnOnline[0]['IDSUPORTE'] == '' ? NULL : $isbnOnline[0]['IDSUPORTE']);
										$arrLivro['IDISBNASSUNTO']       = ($isbnOnline[0]['IDASSUNTO'] == '' ? NULL : $isbnOnline[0]['IDASSUNTO']);
										$arrLivro['IDISBNIDIOMA']        = ($isbnOnline[0]['IDIDIOMAORIG'] == '' ? NULL : $isbnOnline[0]['IDIDIOMAORIG']);
										$arrLivro['IDISBNIDIOMA_TRAD']   = ($isbnOnline[0]['IDIDIOMATRAD'] == '' ? NULL : $isbnOnline[0]['IDIDIOMATRAD']);
										$arrLivro['TITULO']              = removeAcentos($isbnOnline[0]['TITULO'], TRUE);
										$arrLivro['FICHACAT']            = 'N';
										$arrLivro['PRECONORMAL']         = 0;
										$arrLivro['FORMATO_A']           = 0;
										$arrLivro['FORMATO_B']           = 0;
										$arrLivro['NPAGINAS']            = $isbnOnline[0]['NPAGINAS'];
										$arrLivro['PAPEL_MIOLO']         = 0;
										$arrLivro['PAPEL_CAPA']          = 0;
										$arrLivro['PESO']                = 0;
										$arrLivro['ORELHA']              = 'N';
										$arrLivro['AUTOR']               = removeAcentos(implode(' / ', $arrAutor2), TRUE);
										$arrLivro['EDICAO']              = $isbnOnline[0]['EDICAO'];
										$arrLivro['ANO']                 = $isbnOnline[0]['EDICAOANO'];
										$arrLivro['PALAVRASCHAVE']       = '';
										$arrLivro['SINOPSE']             = '';
										$arrLivro['LINKDADOS']           = '';
										$arrLivro['TIPOCAPA_OUTRA']      = '';
										$arrLivro['DATA_ULT_ATU']        = NULL; // DATA NÃO EXISTENTE
										$arrLivro['CONFIRMACAO']         = 'N';
										$arrLivro['IDUSUARIO']          = $this->session->userdata('idUsuario');
										$arrLivro['PRECOPOPULAR']       = 0;
										$arrLivro['ATIVO']              = 'S';
										
										// Insert na tabela livros
										$this->global_model->insert("livro", $arrLivro);
										$contSucess++;
									}
									else
									{
										array_push($arrLog, $isbn."|O livro de código ISBN " . $isbn . " já está cadastrado e não pode ser importado.");
									}

								}
								else
								{
									array_push($arrLog, $isbn."|O CNPJ/CPF da editora responsável pelo código ISBN não confere com o da sua editora.");
								}
							}
							else
							{
								array_push($arrLog, $isbn."|Editora responsável pelo código ISBN não encontrada.");
							}
						}
						else
						{
							array_push($arrLog, $isbn."|Código ISBN não encontrado.");
						}
					}
				}
				else
				{
					$arrDados['sucessImport'] = 'N';
					$arrDados['outrosErros']  = (count($data) > 0 ? 'Ultrapassou o limite máximo de registros. Permitido 100 códigos por arquivo.' : 'O arquivo deve conter no mínimo 1 código ISBN.');
				}
			}
			
			$arrDados['arrLog']     = $arrLog;
			$arrDados['contTotal']  = $contTotal;
			$arrDados['contSucess'] = $contSucess;
			
            $descLog = 'Importação de livros.';

            //salva log
            $this->load->library('logsis', Array('Importacao', 'OK', 'livro', 0, $descLog, var_export($arrDados, TRUE)));

			$this->parser->parse('editora/livro_importar_view', $arrDados);
		}
		else
		{
			$config = $this->configuracao_model->getConfigData('5');
			$msgPer = ($config[0]['DATA_DE'] != '' ? '<b>Período para cadastro/importação de livros:</b> ' . eua_to_br($config[0]['DATA_DE']) . ' até ' . eua_to_br($config[0]['DATA_ATE']) . '.' : '');
			
			$arrErro['msg']      = '<b>Importação não permitida.</b><br /><br /><b>Motivo:</b> Fora do período de cadastros.<br /><br />' . $msgPer;
			$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
			$this->load->view('erro_view', $arrErro);
		}
    }

    function adicionaProgramaLivro($idLivro = NULL, $acao = '')
    {
        $this->load->library('form_validation');
		$this->load->model('livro_model');
        $this->load->model('configuracao_model');
		$this->load->model('global_model');

		$arrDados = Array();
        $arrDados['livro'] = Array();
        
         $arrDados['acao'] = $acao;
        //valide configurações do usuário
		$habilitado = $this->global_model->get_dado('usuarioprograma', "'S'", 'IDUSUARIO = ' . $this->session->userdata('idUsuario') . ' AND IDPROGRAMA =' . $this->session->userdata('programa'));
        $boolPeriodoInclusaoLivro = ($this->configuracao_model->validaConfig('6', date('Y/m/d'))) ? TRUE : FALSE;

        if($idLivro != NULL && is_numeric($idLivro) && $boolPeriodoInclusaoLivro && $habilitado == 'S')
		{
            $arrParam['exibir_pp'] = 1;
            $arrParam['limit_de']  = 0;
			
            // set filtros para pesquisa.
            $arrParam['where'] = " l.IDUSUARIO = '" . $this->session->userdata('idUsuario') . "' ";
            $arrParam['where'] .= " AND l.IDLIVRO = '" . $idLivro . "' ";

            // Filtro para programa
            $arrParam['programa'] =  " AND pl.IDPROGRAMA = " . $this->session->userdata('programa');
            
            //pesquisa
            $livro = $this->livro_model->lista($arrParam, FALSE);
            $arrDados['livro'] = $livro;

            if(count($arrDados['livro']) > 0)
            {   
                if($livro[0]['CONFIRMACAO'] == 'S')
                {
                    $arrDados['removeBtn'] = ($livro[0]['LIVROPOPULAR'] != '') ? TRUE : FALSE;
                    $this->form_validation->set_rules('preco','Preço','required|valida_moeda|trim|xss_clean');
                    $this->form_validation->set_rules('quantidade','Quantidade','required|integer|is_natural_no_zero|trim|xss_clean');
                    
                    $arrSave = Array();
                    $opLog = $descLog = '';
                    
                    $precoMin = $this->configuracao_model->getConfigData('13');
                    $precoMax = $this->configuracao_model->getConfigData('14');
                    
                    $arrDados['precoMin'] = masc_real($precoMin[0]['VALORDEC']);
                    $arrDados['precoMax'] = masc_real($precoMax[0]['VALORDEC']);
                    
                    $this->db->trans_start();
					
                    switch($acao)
                    {
                        case 'save':
                            if($this->form_validation->run() == TRUE)
                            {
                                $preco          = getValor($this->input->post('preco'));
                                $quantidade     = $this->input->post('quantidade');
                                $arrSave['idlivro'] = $livro[0]['IDLIVRO'];
                                
								$existeMov = $this->livro_model->verificaMovLivro($this->session->userdata('programa'), $livro[0]['IDLIVRO']);
								
								if ($existeMov == 0)
								{
									$arrConf = Array('13','14');
									$boolValorLivro = ($this->configuracao_model->validaConfig($arrConf, $preco)) ? TRUE : FALSE;
									
									if($boolValorLivro)
									{
										$arrSave['preco'] = $preco;
										$arrSave['quantidade'] = $quantidade;

										$opLog = 'INSERT';
										$descLog = "Inserindo/atualizando dados do livro na tabela programalivro.";
										
										// insert e update
										$this->livro_model->saveProgramaLivro($arrSave);
									}
									else
									{
										$arrDados['outrosErros'] = 'O preço deve estar entre R$ ' . $arrDados['precoMin'] . ' e R$ ' . $arrDados['precoMax'] . '.';
										$acao = 'list';
									}
								}
								else
								{
									$arrDados['outrosErros'] = 'Já existem aquisições para o livro. Não é possível alterar QTD/PREÇO nesse momento.';
									$acao = 'list';
								}
                            }
                            break;
                        case 'remove':
                            $arrSave['idlivro'] = $livro[0]['IDLIVRO'];
                            $arrSave['idprograma'] = $this->session->userdata('programa');
							
							$existeMov = $this->livro_model->verificaMovLivro($this->session->userdata('programa'), $livro[0]['IDLIVRO']);
							
							if ($existeMov == 0)
							{
								$opLog = 'DELETE';
								$descLog = "Delete livro da tabela programalivro.";
								//delete
								$this->global_model->delete('programalivro', 'idlivro =' . $livro[0]['IDLIVRO'] . ' AND idprograma=' . $this->session->userdata('programa'));
							}
							else
							{
								$arrDados['outrosErros'] = 'Já existem aquisições para o livro. Não é possível removê-lo nesse momento.';
								$acao = 'list';
							}
                            break;
                        case 'list':
                            break;
                        default:
                            $arrDados['livro'] = Array();
                            $arrDados['outrosErros'] = 'Está operação não pode ser realizada.';
                    }
					
                    $this->db->trans_complete();
                    
					if($acao != 'list')
                    {
                        if ($this->db->trans_status() === FALSE)
                        {
                            //salva log
                            $this->load->library('logsis', Array($opLog, 'ERRO', 'programalivro', $arrSave['idlivro'], $descLog, var_export($arrSave, TRUE)));
                        }
                        else
                        {
                            //salva log
                            $this->load->library('logsis', Array($opLog, 'OK', 'programalivro', $arrSave['idlivro'], $descLog, var_export($arrSave, TRUE)));
                        }
                    }
					
                    $arrDados['livro'][0]['PRECOP'] = ($arrDados['livro'][0]['PRECOP'] != '' ? masc_real($arrDados['livro'][0]['PRECOP']) : '');
                }
                else
                {
                    $arrDados['livro'] = Array();
                    $arrDados['outrosErros'] = 'Deve-se confirmar os dados do livro para adicioná-lo ao Programa.';
                }

            }
            else
            {
                $arrDados['outrosErros'] = 'Livro não encontrado';
            }
        }
        else
        {
            $arrDados['outrosErros'] = 'Não é possível adicionar esse livro.';
        }

		$this->parser->parse('editora/livro_adiciona_programa_view', $arrDados);
    }
	
    function livroParticipante()
    {
		$this->load->model('livro_model');
        $this->load->model('configuracao_model');
        $this->load->model('global_model');
        
        $arrDados = Array();
        $arrDados['exibir_pp'] = 0;
        $arrDados['limit_de']  = 0;

        // pegar dados para o programa atual
        $arrDados['programa']       = " AND pl.IDPROGRAMA = " . trim($this->session->userdata('programa'));
        $arrDados['nomePrograma']   = $this->session->userdata('nomePrograma');

        //cria array para filtros
        $where  = " l.IDUSUARIO = '" . $this->session->userdata('idUsuario') . "' ";
        $where .= "  AND pl.IDLIVRO IS NOT NULL ";

        // set filtros para pesquisa.
        $arrDados['where'] = $where;

        //pesquisa
        $arrDados['livros'] = $this->livro_model->lista($arrDados, FALSE);
        $tamArr             = count($arrDados['livros']);
        $arrDados['vazio']  = ($tamArr == 0 ? TRUE : FALSE);

        for ($i = 0; $i < $tamArr; $i++)
        {
            $arrDados['livros'][$i]['NLINHA'] = $i + 1 + $arrDados['limit_de'];
            $arrDados['livros'][$i]['PRECOP'] =  masc_real($arrDados['livros'][$i]['PRECOP']);
        }

        $arrDados['livros'] = add_corLinha($arrDados['livros'], 1);

        $this->parser->parse('editora/livro_lista_programa_view', $arrDados);
        
    }
    
	/**
	* search()
	* Função de entrada/pesquisa do modulo.
	* @return void
	*/
	function search($actual_page = 0)
	{
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Coleta o formulário de filtros 
			$args['filtros'] = $this->input->post();
			
			// Carrega tabela de dados, seta o array de dados e limita a paginação
			$this->load->model('editora_model');
			$this->load->model('cidade_model');
			$this->load->library('array_table');
			$this->array_table->set_id('module_table');
			$this->array_table->set_data($this->editora_model->get_lista_modulo($args['filtros']));
			$this->array_table->set_actual_page($actual_page);
			$this->array_table->set_page_link('editora/search');
			
			// Adiciona colunas à tabela e seta as colunas de exibição
			// $this->array_table->add_column('<a href="' . URL_EXEC . 'pedido/gerencia/' . $this->session->userdata('programa') . '/{0}"><img src="' . URL_IMG . 'icon_controle_pedido.png" title="Gerência do Pedido" /></a>');
			// $this->array_table->add_column('<a href="' . URL_EXEC . 'pedido/conferencia_administrativa_pedido/{0}"><img src="' . URL_IMG . 'icon_conferencia_adm_pedido.png" title="Conferência Administrativa" /></a>');
			// $this->array_table->add_column('<a href="' . URL_EXEC . 'pedido/conferencia/{0}"><img src="' . URL_IMG . 'icon_pedido_conferencia.png" title="Conferência" /></a>');
			$this->array_table->add_column('<a href="' . URL_EXEC . 'editora/form_update_editora/{0}/?url=editora/search"><img src="' . URL_IMG . 'icon_edit.png" title="Editar" /></a>');
			$this->array_table->add_column('<a href="javascript:void(0);" onclick="ajax_modal(\'Visualizar Dados\', \'' . URL_EXEC . 'editora/modal_form_view_editora/{0}\', \'' . URL_IMG . 'icon_view.png\', 600, 800);"><img src="' . URL_IMG . 'icon_view.png" title="Visualizar" /></a>');
			$this->array_table->add_column('{ATIVO_IMG}', false);
			$this->array_table->set_columns(array('#', 'RAZÃO SOCIAL', 'CPF/CNPJ', 'TELEFONE', 'EMAIL', 'RESPONSÁVEL', 'TEL. RESP.', 'EMAIL RESP.', 'LOGIN'));
			
			// Processa tabela do modulo
			$args['module_table'] = $this->array_table->get_html();
			
			// Monta options de ufs
			$args['options_uf'] = monta_options_array($this->editora_model->get_ufs_editoras(), get_value($args['filtros'], 'c__iduf'));
			$args['options_cidades'] = monta_options_array($this->cidade_model->get_cidades_by_uf(get_value($args['filtros'], 'c__iduf')), get_value($args['filtros'], 'c__idcidade'));
			
			$this->load->view('editora/search', $args);
		}
	}
	
	/**
	* download_excel()
	* Download de excel de Editoras.
	* @param
	* @return void
	**/
	function download_excel() 
	{
		// Carrega classe excel e model pedido
		$this->load->library('excel');
		$this->load->model('editora_model');
		
		// Executa download do excel
		$this->excel->set_file_name('EDITORAS_' . date('Y-m-d_His'));
		$this->excel->set_data($this->editora_model->get_lista_modulo());
		$this->excel->download_file();
	}
	
	/**
	* download_excel_itens_pedido_editora()
	* Download de excel para editora.
	* @param integer idusuario
	* @return void
	**/
	function download_excel_itens_pedido_editora($idusuario = null) 
	{
		// Carrega classe excel e model pedido
		$this->load->library('excel');
		$this->load->model('pedido_model');
		
		// Executa download do excel
		$this->excel->set_file_name('RELACAO_TITULOS_EDITORA_' . date('Y-m-d_His'));
		$this->excel->set_data($this->pedido_model->get_itens_pedido_editora($idusuario, $this->session->userdata('programa')));
		$this->excel->download_file();
	}
	
	/**
	* download_excel_itens_pedido_editora_por_pdv()
	* Download de excel para editora.
	* @param integer idusuario
	* @return void
	**/
	function download_excel_itens_pedido_editora_por_pdv($idusuario = null) 
	{
		// Carrega classe excel e model pedido
		$this->load->library('excel');
		$this->load->model('pedido_model');
		
		// Executa download do excel
		$this->excel->set_file_name('RELACAO_TITULOS_PDV_' . date('Y-m-d_His'));
		$this->excel->set_data($this->pedido_model->get_itens_pedido_editora_por_pdv($idusuario, $this->session->userdata('programa')));
		$this->excel->download_file();
	}
	
	/**
	* form_update_editora()
	* Formulário html de edição de editora.
	* @return void
	*/
	function form_update_editora($idusuario = 0)
	{
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Carrega classes necessárias para utilização
			$this->load->model('editora_model');
			
			// Dados da entidade
			$args['dados'] = $this->editora_model->get_dados_editora($idusuario);
			
			// Processa url_redirect (para redirecionamento do botão aplicar, ok e voltar do form)
			$args['url_redirect'] = ($this->input->get('url') == '') ? 'editora/search' : $this->input->get('url');
			
			// Load da camada view
			$this->load->view('editora/form_update_editora', $args);
		}
	}
	
	/**
	* form_update_editora_proccess()
	* Processa formulário de atualização de cadastro de entidade.
	* @return void
	*/
	function form_update_editora_proccess()
	{
		// Por enquanto, somente administrador poderá acessar esta tela
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Carrega classes necessárias
			$this->load->library('logsis');
			$this->load->model('global_model');
			
			// Array de dados do cadastro de entidade
			$idusuario = $this->input->post('idusuario');
			$arr_dados['iestadual'] = $this->input->post('iestadual');
			$arr_dados['razaosocial'] = $this->input->post('razaosocial');
			$arr_dados['nomefantasia'] = $this->input->post('nomefantasia');
			$arr_dados['emailgeral'] = $this->input->post('emailgeral');
			$arr_dados['site'] = $this->input->post('site');
			$arr_dados['telefonegeral'] = $this->input->post('telefonegeral');
			$arr_dados['nomeresp'] = $this->input->post('nomeresp');
			$arr_dados['emailresp'] = $this->input->post('emailresp');
			$arr_dados['telefoneresp'] = $this->input->post('telefoneresp');
			$arr_dados['skyperesp'] = $this->input->post('skyperesp');
			$arr_dados['idlogradouro'] = $this->input->post('idlogradouro');
			$arr_dados['end_numero'] = $this->input->post('end_numero');
			$arr_dados['end_complemento'] = $this->input->post('end_complemento');				
			$this->global_model->update('cadeditora', $arr_dados, "IDUSUARIO = $idusuario");
			$this->logsis->insereLog(array('UPDATE', 'OK', 'cadeditora', $this->session->userdata('idUsuario'), 'Atualização de cadastro de Editora', var_export($this->input->post(), true)));
			
			// Array de dados do cadastro de usuario
			$arr_usuario['cpf_cnpj'] = $this->input->post('cpf_cnpj');
			$arr_usuario['ativo'] = $this->input->post('ativo');
			$arr_usuario['tipopessoa'] = $this->input->post('tipopessoa');
			$this->global_model->update('usuario', $arr_usuario, "IDUSUARIO = $idusuario");
			$this->logsis->insereLog(array('UPDATE', 'OK', 'usuario', $this->session->userdata('idUsuario'), 'Atualização de cadastro de Usuário', var_export($this->input->post(), true)));
			
			// Redirect para formulário
			$action = $this->input->post('redirect_action');
			if($action == 'ok'){ redirect($this->input->post('redirect_ok')); } else {redirect( $this->input->post('redirect_aplicar') . '/?url=' . $this->input->post('redirect_ok')); }
		} else {
			redirect('/home/');
		}
	}
	
	/**
	* modal_form_view_editora()
	* Exibe html de formulario padrao de dados da Editora.
	* @param integer idusuario
	* @return void
	*/
	function modal_form_view_editora($idusuario = 0)
	{
		// Carrega classes necessárias
		$this->load->model('editora_model');
		$args['dados'] = $this->editora_model->get_dados_editora($idusuario);
		
		// Load view
		$this->parser->parse('editora/modal_form_view_editora', $args);
	}
}
