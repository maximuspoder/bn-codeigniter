<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Distribuidor extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('session');
		
		if ($this->session->userdata('acessoSis') == FALSE)
		{
			redirect('/login/');
			exit;
		}
	}
	
	/*************************************************************************
	 *
	 * CONTROLLERS DE DISTRIBUIDOR
	 *
	 * form_alt            : formulario de alteração de cadastro
	 *
	 *************************************************************************/
	
	function index()
	{
		$arrErro['msg']      = 'Página não existe.';
		$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
		$this->load->view('erro_view', $arrErro);
	}
	
	/**
	* search()
	* Função de entrada/pesquisa do modulo.
	* @return void
	*/
	function search($actual_page = 0)
	{
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Coleta o formulário de filtros 
			$args['filtros'] = $this->input->post();
			
			// Carrega tabela de dados, seta o array de dados e limita a paginação
			$this->load->model('distribuidor_model');
			$this->load->model('cidade_model');
			$this->load->library('array_table');
			$this->array_table->set_id('module_table');
			$this->array_table->set_data($this->distribuidor_model->get_lista_modulo($args['filtros']));
			$this->array_table->set_actual_page($actual_page);
			$this->array_table->set_page_link('distribuidor/search');
			
			// Adiciona colunas à tabela e seta as colunas de exibição
			// $this->array_table->add_column('<a href="' . URL_EXEC . 'biblioteca/delete_resp_financeiro/{0}"><img src="' . URL_IMG . 'icon_delete.png" title="Deletar" /></a>');
			// $this->array_table->add_column('<a href="' . URL_EXEC . 'biblioteca/edit_resp_financeiro/{0}"><img src="' . URL_IMG . 'icon_edit.png" title="Editar" /></a>');
			// $this->array_table->add_column('<a href="' . URL_EXEC . 'biblioteca/view_resp_financeiro/{0}"><img src="' . URL_IMG . 'icon_view.png" title="Visualizar" /></a>');
			$this->array_table->add_column('{ATIVO_IMG}', false);		
			$this->array_table->add_column('<a href="' . URL_EXEC . 'distribuidor/form_update_distribuidor/{0}/?url=distribuidor/search"><img src="' . URL_IMG . 'icon_edit.png" title="Editar" /></a>');
			$this->array_table->add_column('<a href="javascript:void(0);" onclick="ajax_modal(\'Visualizar Dados\', \'' . URL_EXEC . 'distribuidor/modal_form_view_distribuidor/{0}\', \'' . URL_IMG . 'icon_view.png\', 600, 800);"><img src="' . URL_IMG . 'icon_view.png" title="Visualizar" /></a>');
			$this->array_table->set_columns(array('#', 'RAZÃO SOCIAL', 'CPF/CNPJ', 'TELEFONE', 'EMAIL', 'RESPONSÁVEL', 'TEL. RESP.', 'EMAIL RESP.', 'LOGIN'));
			
			// Processa tabela do modulo
			$args['module_table'] = $this->array_table->get_html();
			
			// Monta options de ufs
			$args['options_uf'] = monta_options_array($this->distribuidor_model->get_ufs_distribuidores(), get_value($args['filtros'], 'c__iduf'));
			$args['options_cidades'] = monta_options_array($this->cidade_model->get_cidades_by_uf(get_value($args['filtros'], 'c__iduf')), get_value($args['filtros'], 'c__idcidade'));
			
			$this->load->view('distribuidor/search', $args);
		} else {
			redirect('/home/');
		}	
	}
	
	function form_alt($acao = 'edit')
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('usuario_class');
		$this->load->model('global_model');
		$this->load->model('distribuidor_model');
		$this->load->model('usuario_model');
		$this->load->model('naturezajur_model');
		
		$arrDados   = Array();
		$idUsuario  = $this->session->userdata('idUsuario');
		$tipopessoa = '';
		
		if ($acao == 'save')
		{
			$tipopessoa = $this->input->post('tipopessoa');
			$labelAux   = ($tipopessoa == 'PJ' ? 'Razão Social' : 'Nome');
			
			$this->form_validation->set_rules('tipopessoa',      'Tipo Pessoa',                          'required');
			$this->form_validation->set_rules('cnpjcpf',         'CNPJ/CPF',                             'trim|required|max_length[18]|valida_cnpj_cpf|xss_clean');
			$this->form_validation->set_rules('iestadual',       'Inscrição Estadual',                   'trim|max_length[20]|xss_clean');
			$this->form_validation->set_rules('razaosocial',     $labelAux,                              'trim|required|max_length[100]|xss_clean');
			$this->form_validation->set_rules('nomefantasia',    'Nome Fantasia',                        'trim|max_length[100]|xss_clean');
			$this->form_validation->set_rules('naturezajur',     'Natureza Jurídica',                    'valida_combo|xss_clean');
			$this->form_validation->set_rules('telefone1',       'Telefone Geral',                       'trim|required|max_length[15]|xss_clean');
			$this->form_validation->set_rules('email',           'E-mail Geral',                         'trim|required|max_length[100]|valid_email');
			$this->form_validation->set_rules('site',            'Site',                                 'trim|max_length[100]|xss_clean');
			$this->form_validation->set_rules('login',           'Login',                                '');
			$this->form_validation->set_rules('idlogradouro',    'Endereço',                             'is_numeric');
			$this->form_validation->set_rules('cep',             '',                                     '');
			$this->form_validation->set_rules('uf',              '',                                     '');
			$this->form_validation->set_rules('cidade',          '',                                     '');
			$this->form_validation->set_rules('bairro',          '',                                     '');
			$this->form_validation->set_rules('logradouro',      '',                                     '');
			$this->form_validation->set_rules('logcomplemento',  '',                                     '');
			$this->form_validation->set_rules('end_numero',      'Endereço - Número',                    'trim|required|max_length[8]|xss_clean');
			$this->form_validation->set_rules('end_complemento', 'Complemento',                          'trim|max_length[30]|xss_clean');
			$this->form_validation->set_rules('assoc_ent_sind',  'Associado a entidade sindical',        'trim|max_length[100]|xss_clean');
			$this->form_validation->set_rules('fil_ent_assoc',   'Filiada a entidade(s) associativa(s)', 'trim|max_length[100]|xss_clean');
			$this->form_validation->set_rules('nomeresp',        'Nome Responsável',                     'trim|required|max_length[100]|xss_clean');
			$this->form_validation->set_rules('telefoneresp',    'Telefone do Responsável',              'trim|required|max_length[15]|xss_clean');
			$this->form_validation->set_rules('skyperesp',       'Skype do Responsável',                 'trim|max_length[30]|xss_clean');
			$this->form_validation->set_rules('emailresp',       'E-mail do Responsável',                'trim|required|max_length[100]|valid_email');
			$this->form_validation->set_rules('socios',          'Sócios',                               'trim|valida_socios|xss_clean');
			$this->form_validation->set_rules('atuacao',         'Atua com',                             'trim|valida_atuacao|xss_clean');
			$this->form_validation->set_rules('regiaoatend',     'Região de atendimento',                'trim|valida_regiaoatend|xss_clean');
			$this->form_validation->set_rules('cidadeatend',     'Região de atendimento',                'trim|valida_str_codigos|xss_clean');
			$this->form_validation->set_rules('editorastrab',    'Editoras com que trabalha',            'trim|valida_str_codigos|xss_clean');
			
			if ($this->form_validation->run() == TRUE)
			{
				if ($tipopessoa != 'PJ' && $tipopessoa != 'PF')
				{
					$arrDados['outrosErros'] = 'Tipo pessoa inválido.';
					$acao = '';
				}
				elseif ($this->input->post('idlogradouro') == '')
				{
					$arrDados['outrosErros'] = 'Informe o endereço.';
					$acao = '';
				}
				else
				{
					$where = sprintf("IDUSUARIO = %s", $idUsuario);
					
					$this->db->trans_start();
					
					$arrUpd = Array();
					$arrUpd['IDLOGRADOURO']       = ($this->input->post('idlogradouro') != '' ? $this->input->post('idlogradouro') : NULL);
					$arrUpd['IDNATUREZAJUR']      = $this->input->post('naturezajur');
					$arrUpd['IESTADUAL']          = $this->input->post('iestadual');
					$arrUpd['RAZAOSOCIAL']        = removeAcentos($this->input->post('razaosocial'), TRUE);
					$arrUpd['NOMEFANTASIA']       = removeAcentos($this->input->post('nomefantasia'), TRUE);
					$arrUpd['TELEFONEGERAL']      = $this->input->post('telefone1');
					$arrUpd['EMAILGERAL']         = $this->input->post('email');
					$arrUpd['SITE']               = $this->input->post('site');
					$arrUpd['NOMERESP']           = removeAcentos($this->input->post('nomeresp'), TRUE);
					$arrUpd['TELEFONERESP']       = $this->input->post('telefoneresp');
					$arrUpd['EMAILRESP']          = $this->input->post('emailresp');
					$arrUpd['SKYPERESP']          = $this->input->post('skyperesp');
					$arrUpd['END_NUMERO']         = $this->input->post('end_numero');
					$arrUpd['END_COMPLEMENTO']    = removeAcentos($this->input->post('end_complemento'), TRUE);
					$arrUpd['ASSOC_ENT_SINDICAL'] = $this->input->post('assoc_ent_sind');
					$arrUpd['FILIACAO_ENT_ASSOC'] = $this->input->post('fil_ent_assoc');
					$arrUpd['DATA_UPD']           = date('Y/m/d H:i:s');
					
					$this->global_model->update('caddistribuidor', $arrUpd, $where);
					
					$this->usuario_class->delete_socio($idUsuario);
					
					if ($this->input->post('socios') != '')
					{
						$this->usuario_class->save_socio($idUsuario, $this->input->post('socios'));
					}
					
					$this->usuario_class->delete_atuacao($idUsuario);
					
					if ($this->input->post('atuacao') != '')
					{
						$this->usuario_class->save_atuacao($idUsuario, $this->input->post('atuacao'));
					}
					
					$this->usuario_class->delete_editoratrab($idUsuario);
					
					if ($this->input->post('editorastrab') != '')
					{
						$this->usuario_class->save_editoratrab($idUsuario, $this->input->post('editorastrab'));
					}
					
					$this->usuario_class->delete_regiao_atende($idUsuario);
					
					if ($this->input->post('regiaoatend') != '')
					{
						$this->usuario_class->save_regiao_atende($idUsuario, $this->input->post('regiaoatend'));
					}
					
					if ($this->input->post('cidadeatend') != '')
					{
						$this->usuario_class->save_cidade_atende($idUsuario, $this->input->post('cidadeatend'));
					}
					
					$this->db->trans_complete();
					
					if ($this->db->trans_status() === FALSE)
					{
						//salva log
						$this->load->library('logsis', Array('UPDATE', 'ERRO', 'caddistribuidor', $idUsuario, 'Atualização de cadastro de distribuidor', var_export($arrUpd, TRUE)));
						
						$arrErro['msg']      = 'Ocorreu um erro na tentativa de atualizar o cadastro.';
						$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
						$this->load->view('erro_view', $arrErro);
					}
					else
					{
						//salva log
						$this->load->library('logsis', Array('UPDATE', 'OK', 'caddistribuidor', $idUsuario, 'Atualização de cadastro de distribuidor', var_export($arrUpd, TRUE)));
						
						$arrSucesso['msg']      = 'Cadastro atualizado com sucesso!';
						$arrSucesso['link']     = 'home';
						$arrSucesso['txtlink']  = 'Ir para Home';
						$arrSucesso['tipoUser'] = $this->session->userdata('tipoUsuario');
						$this->load->view('sucesso_view', $arrSucesso);
					}
				}
			}
			else
			{
				$acao = '';
			}
		}
		
		//informações zeradas para nao dar erro no parse
		$arrDados['tipopessoa2']     = $tipopessoa;
		$arrDados['tipopessoa_aux']  = '';
		$arrDados['cnpjcpf']         = '';
		$arrDados['naturezajur_aux'] = 0;
		$arrDados['iestadual']       = '';
		$arrDados['razaosocial']     = '';
		$arrDados['nomefantasia']    = '';
		$arrDados['telefone1']       = '';
		$arrDados['email']           = '';
		$arrDados['site']            = '';
		$arrDados['login']           = '';
		$arrDados['idlogradouro']    = '';
		$arrDados['cep']             = '';
		$arrDados['uf']              = '';
		$arrDados['cidade']          = '';
		$arrDados['bairro']          = '';
		$arrDados['logradouro']      = '';
		$arrDados['logcomplemento']  = '';
		$arrDados['end_numero']      = '';
		$arrDados['end_complemento'] = '';
		$arrDados['assoc_ent_sind']  = '';
		$arrDados['fil_ent_assoc']   = '';
		$arrDados['nomeresp']        = '';
		$arrDados['telefoneresp']    = '';
		$arrDados['skyperesp']       = '';
		$arrDados['emailresp']       = '';
		$arrDados['socios']          = '';
		$arrDados['atuacao']         = '';
		$arrDados['regiaoatend']     = '';
		$arrDados['cidadeatend']     = '';
		$arrDados['editorastrab']    = '';
		$arrDados['loadDadosEdit']   = 0;
		
		if ($acao == 'edit')
		{
			$dados = $this->distribuidor_model->getDadosCadastro($idUsuario);
			
			$arrDados['tipopessoa2']     = $dados[0]['TIPOPESSOA'];
			$arrDados['tipopessoa_aux']  = $dados[0]['TIPOPESSOA'];
			$arrDados['cnpjcpf']         = $dados[0]['CPF_CNPJ'];
			$arrDados['naturezajur_aux'] = $dados[0]['IDNATUREZAJUR'];
			$arrDados['iestadual']       = $dados[0]['IESTADUAL'];
			$arrDados['razaosocial']     = $dados[0]['RAZAOSOCIAL'];
			$arrDados['nomefantasia']    = $dados[0]['NOMEFANTASIA'];
			$arrDados['telefone1']       = $dados[0]['TELEFONEGERAL'];
			$arrDados['email']           = $dados[0]['EMAILGERAL'];
			$arrDados['site']            = $dados[0]['SITE'];
			$arrDados['login']           = $dados[0]['LOGIN'];
			$arrDados['idlogradouro']    = $dados[0]['IDLOGRADOURO'];
			$arrDados['cep']             = $dados[0]['CEP'];
			$arrDados['uf']              = $dados[0]['IDUF'];
			$arrDados['cidade']          = $dados[0]['NOMECIDADESUB'];
			$arrDados['bairro']          = $dados[0]['NOMEBAIRRO'];
			$arrDados['logradouro']      = $dados[0]['NOMELOGRADOURO2'];
			$arrDados['logcomplemento']  = $dados[0]['COMPLEMENTO'];
			$arrDados['end_numero']      = $dados[0]['END_NUMERO'];
			$arrDados['end_complemento'] = $dados[0]['END_COMPLEMENTO'];
			$arrDados['assoc_ent_sind']  = $dados[0]['ASSOC_ENT_SINDICAL'];
			$arrDados['fil_ent_assoc']   = $dados[0]['FILIACAO_ENT_ASSOC'];
			$arrDados['nomeresp']        = $dados[0]['NOMERESP'];
			$arrDados['telefoneresp']    = $dados[0]['TELEFONERESP'];
			$arrDados['skyperesp']       = $dados[0]['SKYPERESP'];
			$arrDados['emailresp']       = $dados[0]['EMAILRESP'];
			$arrDados['socios']          = $this->usuario_class->get_socio($idUsuario, 'string');
			$arrDados['atuacao']         = $this->usuario_class->get_atuacao($idUsuario, 'string');
			$arrDados['regiaoatend']     = $this->usuario_class->get_regiao_atende($idUsuario, 'uf');
			$arrDados['cidadeatend']     = $this->usuario_class->get_regiao_atende($idUsuario, 'cidade');
			$arrDados['editorastrab']    = $this->usuario_class->get_editoratrab($idUsuario, 'string');
			$arrDados['loadDadosEdit']   = 1;
			
			$acao = '';
		}
		
		if ($acao == '')
		{
			$arrDados['natjur']    = $this->naturezajur_model->getCombo($arrDados['tipopessoa2']);
			$arrDados['atividade'] = $this->global_model->selectx('atividade', 'ATIVO = \'S\'', 'NOMEATIVIDADE');
			$arrDados['ufs']       = $this->global_model->selectx('uf', '1 = 1', 'NOMEUF');
			
			$this->parser->parse('distribuidor/cadastro_formalt_view', $arrDados);
		}
	}
	
	/**
	* download_excel()
	* Download de excel de livros.
	* @param
	* @return void
	**/
	function download_excel() 
	{
		// Carrega classe excel e model pedido
		$this->load->library('excel');
		$this->load->model('distribuidor_model');
		
		// Executa download do excel
		$this->excel->set_file_name('DISTRIBUIDORES_' . date('Y-m-d_His'));
		$this->excel->set_data($this->distribuidor_model->get_lista_modulo());
		$this->excel->download_file();
	}
	
	/**
	* form_update_distribuidor()
	* Formulário html de edição de distribuidor.
	* @return void
	*/
	function form_update_distribuidor($idusuario = 0)
	{	
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Carrega classes necessárias para utilização
			$this->load->model('distribuidor_model');
			
			// Dados da entidade
			$args['dados'] = $this->distribuidor_model->get_dados_distribuidor($idusuario);
			
			// Processa url_redirect (para redirecionamento do botão aplicar, ok e voltar do form)
			$args['url_redirect'] = ($this->input->get('url') == '') ? 'distribuidor/search' : $this->input->get('url');
			
			// Load da camada view
			$this->load->view('distribuidor/form_update_distribuidor', $args);
		}
	}
	
	
	/**
	* modal_form_view_distribuidor()
	* Exibe html de formulario padrao de dados do Distribuidor.
	* @param integer idusuario
	* @return void
	*/
	function modal_form_view_distribuidor($idusuario = 0)
	{
		// Carrega classes necessárias
		$this->load->model('distribuidor_model');
		$args['dados'] = $this->distribuidor_model->get_dados_distribuidor($idusuario);
		
		// Load view
		$this->parser->parse('distribuidor/modal_form_view_distribuidor', $args);
	}	
	
	
	/**
	* form_update_distribuidor_proccess()
	* Processa formulário de atualização de cadastro de entidade.
	* @return void
	*/
	function form_update_distribuidor_proccess()
	{
 		// Por enquanto, somente administrador poderá acessar esta tela
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Carrega classes necessárias
			$this->load->library('logsis');
			$this->load->model('global_model');
			
			// Array de dados do cadastro de entidade
			$idusuario = $this->input->post('idusuario');
			$arr_dados['iestadual'] = $this->input->post('iestadual');
			$arr_dados['razaosocial'] = $this->input->post('razaosocial');
			$arr_dados['nomefantasia'] = $this->input->post('nomefantasia');
			$arr_dados['emailgeral'] = $this->input->post('emailgeral');
			$arr_dados['site'] = $this->input->post('site');
			$arr_dados['telefonegeral'] = $this->input->post('telefonegeral');
			$arr_dados['nomeresp'] = $this->input->post('nomeresp');
			$arr_dados['emailresp'] = $this->input->post('emailresp');
			$arr_dados['telefoneresp'] = $this->input->post('telefoneresp');
			$arr_dados['idlogradouro'] = $this->input->post('idlogradouro');
			$arr_dados['end_numero'] = $this->input->post('end_numero');
			$arr_dados['end_complemento'] = $this->input->post('end_complemento');
			$arr_dados['skyperesp'] = $this->input->post('skyperesp');
			$this->global_model->update('caddistribuidor', $arr_dados, "IDUSUARIO = $idusuario");
			$this->logsis->insereLog(array('UPDATE', 'OK', 'caddistribuidor', $this->session->userdata('idUsuario'), 'Atualização de cadastro de Distribuidor', var_export($this->input->post(), true)));
			
			// Array de dados do cadastro de usuario
			$arr_usuario['cpf_cnpj'] = $this->input->post('cpf_cnpj');
			$arr_usuario['ativo'] = $this->input->post('ativo');
			$arr_usuario['tipopessoa'] = $this->input->post('tipopessoa');
			$this->global_model->update('usuario', $arr_usuario, "IDUSUARIO = $idusuario");
			$this->logsis->insereLog(array('UPDATE', 'OK', 'usuario', $this->session->userdata('idUsuario'), 'Atualização de cadastro de Distribuidor', var_export($this->input->post(), true)));
			
			// Redirect para formulário
			$action = $this->input->post('redirect_action');
			if($action == 'ok'){ redirect($this->input->post('redirect_ok')); } else {redirect( $this->input->post('redirect_aplicar') . '/?url=' . $this->input->post('redirect_ok')); }
		} else {
			redirect('/home/');
		}   
	}


}

/* End of file Distribuidor.php */
/* Location: ./system/application/controllers/Distribuidor.php */