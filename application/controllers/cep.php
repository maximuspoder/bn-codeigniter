<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cep extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}
	
	function index()
	{
		$arrErro['msg']      = 'Página não existe.';
		$arrErro['tipoUser'] = 0;
		$this->load->view('erro_view', $arrErro);
	}
	
	/*************************************************************************
	 *
	 * CONTROLLERS DE CEP
	 *
	 * pesquisa                     : pesquisa por logradouros a partir de um cep
	 * pesquisa2                    : pesquisa por cep a partir de um formulário de estado, cidade e logradouro
	 * formcad                      : formulário de cadastro
	 * save                         : salva novo logradouro
	 * comboCidades                 : carrega combo de cidade de uma UF
	 * comboBairros                 : carrega combo de bairros de uma cidade
	 *
	 *************************************************************************/
	
	function pesquisa()
	{
		$cep           = $this->input->post('cep', TRUE);
		$elemCompletar = $this->input->post('elemCompletar', TRUE);
		$arrDados      = Array();
		
		$this->load->model('logradouro_model');
		
		if ($cep == '') //formulário para pesquisa
		{
			$arrDados['combouf'] = $this->logradouro_model->getUF();
			$this->parser->parse('cep/cep_formpesq_view', $arrDados);
		}
		else //pesquisa o CEP informado
		{
			$arrEnd = $this->logradouro_model->pesquisaLogradouro($cep);
			$qtdEnd = count($arrEnd);
			
			if ($qtdEnd > 0)
			{
				$arrEnd = add_corLinha($arrEnd, 2);
				
				$arrDados['logradouros']   = $arrEnd;
				$arrDados['cep']           = $cep;
				$arrDados['cont']          = ($qtdEnd == 1 ? 'Foi encontrado 1 logradouro para esse CEP.' : 'Foram encontrados ' . $qtdEnd . ' logradouros para esse CEP.');
				$arrDados['elemCompletar'] = $elemCompletar;
				$arrDados['class_tbody']   = ($qtdEnd > 6 ? 'scrollContent_h135' : '');
				
				$arrDados['cont'] = $arrDados['cont'] . ' Se o logradouro desejado não encontra-se na lista abaixo, clique <a class="semefeito corBranca bold" href="javascript:void(0);" onclick="formCadCep(\'' . $cep . '\')">AQUI</a> para cadastrá-lo.';
				
				$this->parser->parse('cep/cep_pesqlogradouro_view', $arrDados);
			}
			else
			{
				//mostra formulário para cadastro do CEP - logradouro
				$this->formcad($cep);
			}
		}
	}
	
	function pesquisa2()
	{
		$uf        = $this->input->post('uf', TRUE);
		$cidade    = $this->input->post('cidade', TRUE);
		$nomelogra = $this->input->post('nomelogra', TRUE);
		$arrDados  = Array();
		
		$this->load->model('logradouro_model');
		
		$arrEnd = $this->logradouro_model->pesquisaCep($uf, $cidade, $nomelogra);
		$qtdEnd = count($arrEnd);
		
		if ($qtdEnd > 0)
		{
			$arrEnd = add_corLinha($arrEnd, 2);
			
			$arrDados['logradouros'] = $arrEnd;
			$arrDados['cont']        = ($qtdEnd == 1 ? 'Foi encontrado 1 logradouro para esse CEP.' : 'Foram encontrados ' . $qtdEnd . ' logradouros para esse CEP.');
			$arrDados['class_tbody'] = ($qtdEnd > 3 ? 'scrollContent_h77' : '');
			$this->parser->parse('cep/cep_pesqcep_view', $arrDados);
		}
		else
		{
			echo '.ERRO.||Nenhum CEP foi encontrado.';
		}
	}
	
	function formcad($cep = 0)
	{
		$temCidade = 0;
		
		if ($cep == 0)
		{
			$cep = $this->input->post('cep', TRUE);
			$temCidade = 1; //sinal que veio de uma pesquisa com resultado mas que nao tinha o logradouro desejado
		}
		
		$arrDados = Array();
		
		$this->load->model('logradouro_model');
		
		if (strlen($cep) == 8)
		{
			$arrCadastrar = Array();
			$arrCadastrar['CEP']           = $cep;
			$arrCadastrar['IDUF']          = '';
			$arrCadastrar['IDCIDADE']      = '';
			$arrCadastrar['NOMECIDADESUB'] = '';
			$arrCadastrar['IDBAIRRO']      = '';
			$arrCadastrar['NOMEBAIRRO']    = '';
			$arrCadastrar['OBS']           = "Não foi encontrado nenhum logradouro, bairro ou cidade para o CEP pesquisado. 
											  Por favor, complete o cadastro do CEP.";
			
			//tenta identificar se o cep é de algum bairro para ajudar no cadastro
			$arrEnd = $this->logradouro_model->pesquisaBairro($cep);
			$qtdEnd = count($arrEnd);
			
			if ($qtdEnd > 0)
			{
				$arrCadastrar['IDUF']          = $arrEnd[0]['IDUF'];
				$arrCadastrar['IDCIDADE']      = $arrEnd[0]['IDCIDADE'];
				$arrCadastrar['NOMECIDADESUB'] = $arrEnd[0]['NOMECIDADESUB'];
				$arrCadastrar['IDBAIRRO']      = $arrEnd[0]['IDBAIRRO'];
				$arrCadastrar['NOMEBAIRRO']    = $arrEnd[0]['NOMEBAIRRO'];
				$arrCadastrar['OBS']           = "Não foi encontrado nenhum logradouro para o CEP pesquisado. 
												  Porém encontramos o bairro que o CEP pertence. 
												  Por favor, complete o cadastro informando o logradouro desejado.";
			}
			else
			{
				//tenta identificar se o cep é de alguma cidade para ajudar no cadastro
				$arrEnd = $this->logradouro_model->pesquisaCidade($cep);
				$qtdEnd = count($arrEnd);
				
				if ($qtdEnd > 0)
				{
					$arrCadastrar['IDUF']          = $arrEnd[0]['IDUF'];
					$arrCadastrar['IDCIDADE']      = $arrEnd[0]['IDCIDADE'];
					$arrCadastrar['NOMECIDADESUB'] = $arrEnd[0]['NOMECIDADESUB'];
					$arrCadastrar['OBS']           = "Não foi encontrado nenhum logradouro para o CEP pesquisado. 
													  Porém encontramos a cidade que o CEP pertence. 
													  Por favor, complete o cadastro informando o bairro e o logradouro desejado.";
				}
			}
			
			//tratar observação caso veio de uma pesquisa com resultado mas que nao tinha o logradouro desejado
			if ($temCidade == 1)
			{
				$arrCadastrar['OBS'] = "Por favor, complete o cadastro.";
			}
			
			$arrDados['dadosCad']       = $arrCadastrar;
			$arrDados['combouf']        = $this->logradouro_model->getUF();
			$arrDados['combotipologra'] = $this->logradouro_model->getTipoLogra();
			
			//solicita cadastro
			$this->parser->parse('cep/cep_cadastro_view', $arrDados);
		}
	}
	
	function save()
	{
		$cep        = $this->input->post('cep', TRUE);
		$uf         = $this->input->post('uf', TRUE);
		$cidade     = $this->input->post('cidade', TRUE);
		$bairro     = $this->input->post('bairro', TRUE);
		$novoBairro = trim(removeAcentos($this->input->post('novoBairro'), TRUE));
		$tipologra  = $this->input->post('tipologra', TRUE);
		$logradouro = trim(removeAcentos($this->input->post('logradouro'), TRUE));
		
		$this->load->model('global_model');
		$this->load->model('logradouro_model');
		
		if (strlen(trim($cep)) == 8 && is_numeric($cep) && $logradouro != '')
		{
			$this->db->trans_start();
			
			$bairroIns = $bairro;
			
			if ($bairro == '-1') //salva novo bairro
			{
				$arrInsBairro = Array();
				$arrInsBairro['IDCIDADE']   = $cidade;
				$arrInsBairro['NOMEBAIRRO'] = $novoBairro;
				
				$this->global_model->insert('bairro', $arrInsBairro);
				$bairroIns = $this->global_model->get_insert_id();
			}
			
			$arrIns = Array();
			$arrIns['IDCIDADE']         = $cidade;
			$arrIns['IDBAIRRO']         = $bairroIns;
			$arrIns['IDLOGRADOUROTIPO'] = $tipologra;
			$arrIns['CEP']              = $cep;
			$arrIns['NOMELOGRADOURO']   = $logradouro;
			$arrIns['LOGRA_ADD']        = 1;
			$arrIns['IDUSUARIO']        = NULL; //$this->session->userdata('idUsuario'); MCV
			
			$this->global_model->insert('logradouro', $arrIns);
			$idNewLogra = $this->global_model->get_insert_id();
			
			$this->db->trans_complete();
			
			if ($this->db->trans_status() === FALSE)
			{
				//$this->load->library('logsis', Array(5, 'LOG_0046', var_export($arrIns, TRUE), 'logradouro', 0));
				
				if ($bairro == '-1')
				{
					//$this->logsis->insereLog(Array(5, 'LOG_0048', var_export($arrInsBairro, TRUE), 'bairro', 0));
				}
				
				echo '.ERRO.||Não foi possível realizar esta operação.';
			}
			else
			{
				//$this->load->library('logsis', Array(5, 'LOG_0045', var_export($arrIns, TRUE), 'logradouro', $idNewLogra));
				
				if ($bairro == '-1')
				{
					//$this->logsis->insereLog(Array(5, 'LOG_0047', var_export($arrInsBairro, TRUE), 'bairro', $bairroIns));
				}
				
				echo 'OK';
			}
		}
		else
		{
			echo '.ERRO.||Não foi possível realizar esta operação. Verifique os dados submetidos.';
		}
	}
	
	function comboCidades()
	{
		$uf       = $this->input->post('uf', TRUE);
		$arrDados = Array();
		
		$this->load->model('logradouro_model');
		
		$arrCidades = $this->logradouro_model->getCidades($uf);
		$qtdCidades = count($arrCidades);
		
		array_push($arrDados, "0|#---");
		
		for ($i = 0; $i < $qtdCidades; $i++)
		{
			array_push($arrDados, $arrCidades[$i]['IDCIDADE'] . "|#" . $arrCidades[$i]['NOMECIDADESUB']);
		}
		
		echo implode('~#', $arrDados);
	}
	
	function comboBairros($opcaoCad = 0)
	{
		$cidade   = $this->input->post('cidade', TRUE);
		$arrDados = Array();
		
		$this->load->model('logradouro_model');
		
		$arrBairros = $this->logradouro_model->getBairros($cidade);
		$qtdBairros = count($arrBairros);
		
		array_push($arrDados, "0|#---");
		
		if ($opcaoCad == 1)
		{
			array_push($arrDados, "-1|#cadastrar novo bairro...");
		}
		
		for ($i = 0; $i < $qtdBairros; $i++)
		{
			array_push($arrDados, $arrBairros[$i]['IDBAIRRO'] . "|#" . $arrBairros[$i]['NOMEBAIRRO']);
		}
		
		echo implode('~#', $arrDados);
	}
	
}

/* End of file cep.php */
/* Location: ./system/application/controllers/cep.php */