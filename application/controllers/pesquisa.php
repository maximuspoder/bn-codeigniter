<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe Pesquisa
*
* Abstração de classe para entidade pesquisa (Estabelecimentos).
* 
* @author		Leandro Mangini Antunes
* @package		application
* @subpackage	controllers.pesquisa
* @since		2012-07-03
*
*/
class Pesquisa extends CI_Controller {

	/**
	* __construct()
	* @return object
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');	
		if ($this->session->userdata('acessoSis') == FALSE)
		{
			redirect('/login/');
			exit;
		}
	}
	
	/**
	* pesquisar_estabelecimento()
	* Monta o formulário de pesquisa de estabelecimento.
	* @return void
	*/
	function pesquisar_estabelecimento($actual_page = 0)
	{
		// Joga o post para variável, para ser utilizado filtros
		$args['post'] = $this->input->post();
		
		// Somente faz pesquisa caso campo razão social esteja preenchido.
		if(get_value($args['post'], 'campo_pesquisa') != '')
		{
			// Carrega classes necessárias
			$this->load->library('array_table');
			$this->load->model('pesquisa_model');
			
			// Conforme o tipo de entidade, monta o sql
			switch(strtolower(get_value($args['post'], 'entidade')))
			{
				case 'pdvs': $this->pesquisa_model->add_sql_pesquisa_pdv($args['post']); break;
				case 'editoras': $this->pesquisa_model->add_sql_pesquisa_editora($args['post']); break;
				case 'bibliotecas': $this->pesquisa_model->add_sql_pesquisa_biblioteca($args['post']); break;
				case 'distribuidores': $this->pesquisa_model->add_sql_pesquisa_distribuidor($args['post']); break;
				default:
					$this->pesquisa_model->add_sql_pesquisa_pdv($args['post']);
					$this->pesquisa_model->add_sql_pesquisa_editora($args['post']);
					$this->pesquisa_model->add_sql_pesquisa_biblioteca($args['post']);
					$this->pesquisa_model->add_sql_pesquisa_distribuidor($args['post']);
				break;
			}
			
			// Executa a pesquisa, monta tabela
			$this->array_table->set_id('module_table');
			$this->array_table->set_data($this->pesquisa_model->execute_pesquisa());
			$this->array_table->set_actual_page($actual_page);
			$this->array_table->set_items_per_page(100);
			$this->array_table->set_page_link('pesquisa/pesquisar_estabelecimento');
			$this->array_table->add_column('{ACAO_IMG}');
			$this->array_table->add_column('{TIPO_ENTIDADE}', false);
			$this->array_table->set_columns(array('#', 'RAZÃO SOCIAL', 'NOME FANTASIA', 'CPF / CNPJ', 'TELEFONE', 'CIDADE', 'UF'));
			
			// processa retorno da tabela
			$args['module_table'] = $this->array_table->get_html();
			$args['header_pesquisa'] = '<div><h4 style="color:#333;margin:30px 0 10px 0;">Resultados para "<span class="italic">' . get_value($args['post'], 'campo_pesquisa') . '</span>":</h4></div>';
		} else {
			$args['module_table'] = '';
			$args['header_pesquisa'] = '';
		}
		
		// Monta options de ufs
		$this->load->model('uf_model');
		$this->load->model('cidade_model');
		$args['options_uf'] = monta_options_array($this->uf_model->get_ufs(), get_value($args['post'], 'iduf'));
		$args['options_cidades'] = monta_options_array($this->cidade_model->get_cidades_by_uf(get_value($args['post'], 'iduf')), get_value($args['post'], 'idcidade'));
		
		// Carrega view
		$this->load->view('pesquisa/pesquisar_estabelecimento', $args);
	}
	
	/**
	* modal_form_view_biblioteca()
	* Exibe html de formulario padrao de dados da biblioteca.
	* @param integer idusuario
	* @return void
	*/
	function modal_form_view_biblioteca($idusuario = 0)
	{
		// Carrega classes necessárias
		$this->load->model('biblioteca_model');
		$args['dados'] = $this->biblioteca_model->get_dados_biblioteca($idusuario);
		
		// Load view
		$this->parser->parse('pesquisa/modal_form_view_biblioteca', $args);
	}
	
	/**
	* pesquisar_livros()
	* Monta o formulário de pesquisa de livros.
	* @return void
	*/
	function pesquisar_livros($actual_page = 0)
	{
		// Carrega view da pesquisa
		// $this->load->view('pesquisa/pesquisa_livros', array());
		
		// Coleta o termo de pesquisa
		$termo_pesquisa = trim(strtolower(removeAcentos($this->input->post('campo_pesquisa'))), ' ');
		
		// Joga o post para variável, para ser utilizado filtros
		$args['post'] = $this->input->post();
		
		// Somente faz pesquisa caso campo razão social esteja preenchido.
		if(get_value($args['post'], 'campo_pesquisa') != '')
		{
			// Carrega classes necessárias
			$this->load->library('array_table');
			$this->load->model('pesquisa_model');
			
			// Adiciona sql dos livros
			$this->pesquisa_model->add_sql_pesquisa_livros(get_value($args['post'], 'campo_pesquisa'));
			
			// Executa a pesquisa, monta tabela
			$this->array_table->set_id('module_table');
			$this->array_table->set_data($this->pesquisa_model->execute_pesquisa());
			$this->array_table->set_actual_page($actual_page);
			$this->array_table->set_items_per_page(100);
			$this->array_table->set_page_link('pesquisa/pesquisar_livros');
			$this->array_table->add_column('{ACAO_IMG}');
			$this->array_table->set_columns(array('#','TITULO','AUTOR','EDIÇÂO','ANO','EDITORA','NOME FANTASIA'));
			
			// processa retorno da tabela
			$args['module_table'] = $this->array_table->get_html();
			$args['header_pesquisa'] = '<div><h4 style="color:#333;margin:30px 0 10px 0;">Resultados para "<span class="italic">' . get_value($args['post'], 'campo_pesquisa') . '</span>":</h4></div>';
		} else {
			$args['module_table'] = '';
			$args['header_pesquisa'] = '';
		}
		
		// Carrega view
		$this->load->view('pesquisa/pesquisar_livros', $args);
	}
	
	/**
	* ajax_pesquisar_livros()
	* Esta função é chamada pelo ajax do formulário de pesquisa de livros.
	* @return void
	*/
	function ajax_pesquisar_livros()
	{
		// Carrega classes necessárias
		$this->load->model('pesquisa_model');
		
		// Passa todas as letras para minusculo e remove os acentos
		$termo_pesquisa = trim(strtolower(removeAcentos($this->input->post('campo_pesquisa'))), ' ');
		
		// Monta o sql com o termo de pesquisa
		$this->pesquisa_model->add_sql_pesquisa_livros($termo_pesquisa);
		
		// Executa sql
		$data = $this->pesquisa_model->execute_pesquisa_livros();
		
		//print_r($data);
		$this->load->library('array_table');
		$this->array_table->set_id('module_table');
		$this->array_table->set_data($data);
		$this->array_table->set_actual_page(1);
		$this->array_table->set_items_per_page(50000);
		$this->array_table->set_page_link('pesquisa/pequisa_livros');
		$this->array_table->add_column('{ACAO_IMG}');
		$this->array_table->set_columns(array('#','TITULO','AUTOR','EDIÇÂO','ANO','EDITORA','NOME FANTASIA'));
		
		// Processa tabela do modulo
		echo $this->array_table->get_html();
	}
	
	/**
	* modal_form_view_pesquisa_livros()
	* Exibe html de formulario padrao de dados dos livros na pesquisa externa.
	* @param integer idusuario
	* @return void
	*/
	function modal_form_view_pesquisa_livros($idlivro = 0)
	{
		// Carrega classes necessárias
		$this->load->model('pesquisa_model');

		$args['dados'] = $this->pesquisa_model->get_dados_pesquisa_livros($idlivro);
		
		// Load view
		$this->parser->parse('pesquisa/modal_form_view_pesquisa_livros', $args);
	}
	
	
	/*************************************************************************
	 *
	 * CONTROLLERS DE PESQUISA
	 *
	 * pesquisarEstab             : pesquisar os estabelecimentos
     * exibeDadosPesquisa         : exibir dados da pesquisa
     * pesquisarLivros            : exibir livros disponíveis estilo ecommerce
     * pesquisarLivrosIndicacao	  : exibir livros disponíveis para indicação stilo rede social
     * comentarios	      		  : form e lista de comentarios
     * comentario_save    		  : salva comentarios
	 *
	 *************************************************************************/
	
	function index()
	{
		$arrErro['msg']      = 'Página não existe.';
		$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
		$this->load->view('erro_view', $arrErro);
	}
	
	function nfunc()
	{
		$arrErro['msg']      = 'Esta funcionalidade não está disponível.';
		$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
		$this->load->view('erro_view', $arrErro);
	}

    function pesquisarEstab($nPag = 1)
    {
		$this->load->library('form_validation');
		$this->load->model('biblioteca_model');
		$this->load->model('uf_model');
        $this->load->model('logradouro_model');        
        $this->load->model('global_model');
		
        
        $arrDados = Array();
        $arrDados['search'] = false;
        $arrDados['ufs'] = $this->global_model->selectx('uf', '1=1', 'IDUF');
		$arrDados['ufs'] = $this->global_model->selectx('cidade', '1=1', 'IDCIDADE');
        
        $this->form_validation->set_rules('uf','UF','trim|xss_clean');
        $this->form_validation->set_rules('cidade','CIDADE','trim|xss_clean');
        $this->form_validation->set_rules('pesquisa','Razão Social / Nome Fantasia','trim|xss_clean');
        $this->form_validation->set_rules('tipo_pesquisa', 'Tipo Pesquisa', 'required');

        $arrDados['form_data'] = $this->input->post();

        if($this->form_validation->run() == TRUE)
        {
            $uf = $this->input->post('uf');
            $cidade = $this->input->post('cidade');
            $pesq = $this->input->post('pesquisa');
            $habilitado = $this->input->post('habilitado');
            
			if($pesq != '' || $uf != '')
            {
                $arrDados['nPag']      = (is_numeric($nPag) && $nPag > 0 ? $nPag : 1);
                $arrDados['exibir_pp'] = 15;
                $arrDados['limit_de']  = ($arrDados['nPag'] - 1) * $arrDados['exibir_pp'];
                $arrDados['table']     = $this->input->post('tipo_pesquisa');

                $arrDados['where']      = '1 = 1';
                $arrDados['where']     .= ($uf != '') ? " AND c.IDUF='" . trim($uf) . "'" : '';
                $arrDados['where']     .= ($cidade != '') ? " AND c.IDCIDADE='" . trim($cidade) . "'" : '';
                $arrDados['where']     .= ($habilitado != '') ? " AND u.IDUSUARIO " . (($habilitado == 'N') ? 'NOT' : '') . " IN (SELECT IDUSUARIO FROM USUARIOPROGRAMA WHERE IDPROGRAMA = " . $this->session->userdata('programa') . ")" : '';
                $arrDados['where']     .= ($pesq != '') ? " AND (t.RAZAOSOCIAL LIKE '%" . removeAcentos($pesq, TRUE) . "%' OR t.NOMEFANTASIA LIKE '%" . removeAcentos($pesq, TRUE) . "%')" : '';
                
                $arrDados['idUsuario'] = $this->session->userdata('idUsuario');
				
				$this->biblioteca_model->setFiltro('CIDADE'	, $arrDados['filtros']['filtro_cidade']);
				$this->biblioteca_model->setFiltro('UF'		, $arrDados['filtros']['filtro_estado']);
						
				// Coleta options de UF
				$arrDados['options_estados'] = montaOptionsArray($this->uf_model->getEstados(), $arrDados['filtros']['filtro_estado']);
				
				// Coleta options de cidades
				$arrDados['options_cidades'] = montaOptionsArray($this->logradouro_model->getCidadesPesquisa($arrDados['filtros']['filtro_estado']), $arrDados['filtros']['filtro_cidade']);

                //pesquisa
                $arrDados['dados']  = $this->global_model->get_dado_pesquisa_estab($arrDados, FALSE);
                $tamArr             = count($arrDados['dados']);
                $arrDados['vazio']  = ($tamArr == 0 ? TRUE : FALSE);

                for ($i = 0; $i < $tamArr; $i++)
                {
                    $arrDados['dados'][$i]['NLINHA'] = $i + 1 + $arrDados['limit_de'];
                }

                $arrDados['dados'] = add_corLinha($arrDados['dados'], 1);

                //dados da paginação
                $qtdReg = $this->global_model->get_dado_pesquisa_estab($arrDados, TRUE);
                $nPaginas = $qtdReg[0]['CONT'] / $arrDados['exibir_pp'];

                if ($qtdReg[0]['CONT'] % $arrDados['exibir_pp'] != 0)
                {
                    $nPaginas = (int)$nPaginas + 1;
                }

                $arrDados['nPaginas'] = $nPaginas;
                $arrDados['qtdReg']   = $qtdReg[0]['CONT'];
                $arrDados['search'] = true;
            }
            else
            {
                $arrDados['outrosErros'] = 'Pelo menos um dos campos Razão Social / Nome Fantasia ou UF deve ser informado.';
            }
        }

		$this->parser->parse('pesquisa/estabelecimento_view', $arrDados);
        
    }
	
    function exibeDadosPesquisa($tipo = NULL, $idUsuario = NULL)
    {
        if ($tipo == NULL || $idUsuario == NULL || ! is_numeric($tipo) || ! is_numeric($idUsuario))
        {
            redirect('/pesquisa/pesquisarEstab/');
			exit;
        }
        else
        {
            $this->load->model('global_model');
            $this->load->model('logradouro_model');
            
			$arrParam = Array();
            $arrParam['table'] = get_table_dados($tipo);
            $arrParam['where'] = 'u.IDUSUARIO = ' . $idUsuario;
            $arrParam['limit_de'] = '0';
            $arrParam['exibir_pp'] = '1';
            
            $dados = $this->global_model->get_dado_pesquisa_estab($arrParam);
            
			if (count($dados) > 0)
			{
				$arrDados['tipopessoa']      = $dados[0]['TIPOPESSOA'];
				$arrDados['tipousuario']     = $dados[0]['NOMEUSUARIOTIPO'];
				$arrDados['razaosocial']     = $dados[0]['RAZAOSOCIAL'];
				$arrDados['nomefantasia']    = $dados[0]['NOMEFANTASIA'];
				$arrDados['telefone1']       = $dados[0]['TELEFONEGERAL'];
				$arrDados['email']           = $dados[0]['EMAILGERAL'];
				$arrDados['nomeresp']        = $dados[0]['NOMERESP'];
				$arrDados['foneresp']        = $dados[0]['TELEFONERESP'];
				$arrDados['emailresp']       = $dados[0]['EMAILRESP'];
				$arrDados['respfinan']   	 = $dados[0]['RESPFINAN'];
				$arrDados['emailfinan']   	 = $dados[0]['EMAILFINAN'];
				$arrDados['fonefinan']   	 = $dados[0]['FONEFINAN'];
				$arrDados['site']            = $dados[0]['SITE'];
				$arrDados['cep']             = $dados[0]['CEP'];
				$arrDados['uf']              = $dados[0]['IDUF'];
				$arrDados['cidade']          = $dados[0]['NOMECIDADESUB'];
				$arrDados['bairro']          = $dados[0]['NOMEBAIRRO'];
				$arrDados['logradouro']      = $dados[0]['NOMELOGRADOURO'];
				$arrDados['logcomplemento']  = $dados[0]['COMPLEMENTO'];
				$arrDados['end_numero']      = $dados[0]['END_NUMERO'];
				$arrDados['end_complemento'] = $dados[0]['END_COMPLEMENTO'];
				$arrDados['tipo'] 			 = $tipo;
				
				
				$this->parser->parse('pesquisa/estabelecimento_info_view', $arrDados);
			}
			else
			{
				$arrErro['msg']      = 'Página não encontrada.';
				$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
				$this->load->view('erro_view', $arrErro);
            }
        }
    }
	
	function pesquisarLivros($nPag = 1)
    {	
		// Valida para caso o usuário seja diferente de biblioteca
		if ($this->session->userdata('tipoUsuario') != 2)
		{
			redirect('/login/');
			exit;
		}
		
		// Carrega Models
		$this->load->model('livro_model');
		$this->load->model('pedido_model');
		$this->load->model('biblioteca_model');
		$this->load->model('configuracao_model');
		
		$arrDados = Array();
		$arrDados['nPag']      = (is_numeric($nPag) && $nPag > 0 ? $nPag : 1);
		$arrDados['exibir_pp'] = 32;
		$arrDados['limit_de']  = ($arrDados['nPag'] - 1) * $arrDados['exibir_pp'];
		
		// Carrega informação se biblioteca está habilitada ou se está no período para aquisição
		if( ! $this->biblioteca_model->isHabilitado($this->session->userdata('idUsuario')) || ! $this->configuracao_model->validaConfig(19, date('Y-m-d')))
		{
			$arrErro['msg']  = '';
			$arrErro['msg'] .= (!$this->biblioteca_model->isHabilitado($this->session->userdata('idUsuario'))) ? 'Sua biblioteca não está habilitada. ' : '';
			$arrErro['msg'] .= (!$this->configuracao_model->validaConfig(19, date('Y-m-d'))) ? 'O período para aquisição de livros não está vigente.'   : '';
			$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
			$this->load->view('erro_view', $arrErro);
		}
		else
		{
			// Carrega crédito atual
			$arrDados['credito'] = masc_real($this->biblioteca_model->getCreditoAtual($this->session->userdata('idUsuario')));
			
			// Monta os itens da lista lateral
			// Primeiramente, descobre o ID do pedido da biblioteca atualmente logada
			if($this->pedido_model->pedidoExists($this->session->userdata('idUsuario')))
			{
				$idPedido = $this->pedido_model->getIdPedido($this->session->userdata('idUsuario'));
				$arrDados['qtdeItens']   = $this->pedido_model->getCountItens($idPedido);
				$arrDados['pedidoItens'] = $this->pedido_model->getItens($idPedido);
			} else {
				$idPedido = '';
				$arrDados['qtdeItens']   = 0;
				$arrDados['pedidoItens'] = array();
			}
			
			// Filtros
			$filtros = (isset($_POST['filtro_titulo'])) ? $_POST : null;
			$arrDados['filtros'] = $this->livro_model->getArrayFiltros($filtros);
			$this->livro_model->setFiltro('AUTOR', $arrDados['filtros']['filtro_autor']);
			$this->livro_model->setFiltro('EDITORA', $arrDados['filtros']['filtro_editora']);
			$this->livro_model->setFiltro('TITULO', $arrDados['filtros']['filtro_titulo']);
			$this->livro_model->setFiltro('SINOPSE', $arrDados['filtros']['filtro_sinopse']);
			$this->livro_model->setFiltro('PALAVRASCHAVE', $arrDados['filtros']['filtro_palavraschave']);
			$this->livro_model->setFiltro('CONFIRMACAO', $arrDados['filtros']['filtro_status']);
			$this->livro_model->setFiltro('ISBN', $arrDados['filtros']['filtro_isbn']);
			$this->livro_model->setFiltro('VISUALIZACAO', $arrDados['filtros']['visualizacao']);
			
			// Pesquisa de Livros
			$arrDados['idPedido'] = $idPedido;
			$arrDados['livros'] = $this->livro_model->listaPesquisa($arrDados, FALSE);
			$tamArr = count($arrDados['livros']);
			$arrDados['vazio'] = ($tamArr == 0 ? TRUE : FALSE);
			
			// Calcula cor das linhas
			for ($i = 0; $i < $tamArr; $i++) { $arrDados['livros'][$i]['NLINHA'] = $i + 1 + $arrDados['limit_de']; }
			$arrDados['livros'] = add_corLinha($arrDados['livros'], 1);
			
			// Dados da paginação
			$qtdReg = $this->livro_model->listaPesquisa($arrDados, TRUE);
			$nPaginas = $qtdReg[0]['CONT'] / $arrDados['exibir_pp'];
			if ($qtdReg[0]['CONT'] % $arrDados['exibir_pp'] != 0) {	$nPaginas = (int)$nPaginas + 1;	}
			$arrDados['nPaginas'] = $nPaginas;
			$arrDados['qtdReg'] = $qtdReg[0]['CONT'];
			
			$this->parser->parse('pesquisa/pesquisa_livro_view', $arrDados);
		}
    }
 
    function pesquisarLivrosIndicacao($nPag = 1)
    {	
		// Valida para caso o usuário seja diferente de biblioteca
		if ($this->session->userdata('tipoUsuario') != 2)
		{
			redirect('/login/');
			exit;
		}
		
        $this->load->library('form_validation');
        
		// Carrega Models
        $this->load->model('global_model');
		$this->load->model('isbn_model');
		$this->load->model('pedido_model');
		$this->load->model('biblioteca_model');
		$this->load->model('configuracao_model');
		
		$arrDados = Array();
		$arrDados['nPag']      = (is_numeric($nPag) && $nPag > 0 ? $nPag : 1);
		$arrDados['exibir_pp'] = 32;
		$arrDados['limit_de']  = ($arrDados['nPag'] - 1) * $arrDados['exibir_pp'];
		
		// Carrega informação se biblioteca está habilitada ou se está no período para aquisição
		if( ! $this->biblioteca_model->isHabilitadoPrograma($this->session->userdata('idUsuario'),$this->session->userdata('programa')) || ! $this->configuracao_model->validaConfig(21, date('Y-m-d')))
		{
			$arrErro['msg']  = '';
			$arrErro['msg'] .= (!$this->biblioteca_model->isHabilitadoPrograma($this->session->userdata('idUsuario'),$this->session->userdata('programa'))) ? 'Sua biblioteca não está habilitada para o edital. ' : '';
			$arrErro['msg'] .= (!$this->configuracao_model->validaConfig(21, date('Y-m-d'))) ? 'O período para indicação de livros não está vigente.'   : '';
			$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
			$this->load->view('erro_view', $arrErro);
		}
		else
		{
			// Carrega crédito atual
			$arrDados['credito'] = $this->global_model->get_dado('programaindicacao', "INDICACOES", 'IDUSUARIO = ' . $this->session->userdata('idUsuario') . ' AND IDPROGRAMA =' . $this->session->userdata('programa'));
			$arrDados['creditado'] = $this->global_model->get_dado('bibliotecaindicacao', "count(*)", 'IDUSUARIO = ' . $this->session->userdata('idUsuario') . ' AND IDPROGRAMA =' . $this->session->userdata('programa'));

             // Lista de indicados
            $arrDados['Itens']      = $this->biblioteca_model->getItensIndicados($this->session->userdata('idUsuario'));
			$arrDados['qtdeItens']  = count($arrDados['Itens']);
			
            //cria array para filtros
            $where = Array();

            $this->form_validation->set_rules('filtro_titulo','Título','trim|xss_clean');
            $this->form_validation->set_rules('filtro_autor','Autor','trim|xss_clean');
            $this->form_validation->set_rules('filtro_indicacao','Indicação','trim|xss_clean');
            //$this->livro_model->setFiltro('VISUALIZACAO', $arrDados['filtros']['visualizacao']);
            
            //$tit = "COMER, REZAR   E  AMAR";
            //$arrTit = explode(' ', removeAcentosArquivo($tit, true));
            
            //var_dump($arrTit);
            
            if($this->form_validation->run() == TRUE || $this->input->post() == FALSE)
            {
                $titulo         = $this->input->post('filtro_titulo');
                $autor          = $this->input->post('filtro_autor');
                $indicacao		= $this->input->post('filtro_indicacao');

                // Filtro para Visualização
                if($indicacao != '')
                {
                    if($indicacao == 'S')
                    {
                    	array_push($where, " WHERE BI.IDUSUARIO = " . $this->session->userdata('idUsuario'));
                    }
                    elseif($indicacao == 'N')
                    {
                    	array_push($where, " WHERE BI.IDUSUARIO IS NULL ");
                    }
                }
                else
                {
                	array_push($where, " WHERE 1=1 ");
                }
                // Filtro para Título
                if($titulo != '')
                {
                    //array_push($where, " AND T.TITULO LIKE '%" . $titulo . "%' ");
                    $arrTit = explode(' ', $titulo);

                    $wheretit = " ";
                    
                    foreach ($arrTit as $tit) {
                    	if (strlen($tit) > 2) {
                    		$wheretit .= "AND T.TITULO LIKE '%".$tit."%' ";
                    	}
                    }
                    array_push($where, $wheretit);
                }

                // Filtro para Autor
                if($autor != '')
                {
                    //array_push($where, " AND T.AUTORES LIKE '%" . $autor . "%' ");
                    $arrAut = explode(' ', $autor);

                    $whereaut = " ";
                    
                    foreach ($arrAut as $aut) {
                    	if (strlen($aut) > 2) {
                    		$whereaut .= "AND T.AUTORES LIKE '%".$aut."%' ";
                    	}
                    }
                    array_push($where, $whereaut);
                }
                
            }
            
            // set filtros para pesquisa.
            $arrDados['where'] = implode(" ", $where);

            // campo para indicação
            $arrDados['indicacao']  = $this->input->post('filtro_indicacao');
                        
            // campo para visualização
            $arrDados['visualizacao']  = $this->input->post('visualizacao');
            
			// Pesquisa de Livros
            if (count($where) > 1 || ($where[0] != " WHERE 1=1 " && $where[0] != " WHERE BI.IDUSUARIO IS NULL ")) {
				$arrDados['livros'] = $this->isbn_model->getLivrosIsbnIndicacao($arrDados, FALSE);
				$tamArr = count($arrDados['livros']);
				$arrDados['vazio'] = ($tamArr == 0 ? TRUE : FALSE);
				$arrDados['pesquisa'] = TRUE;
				
				// Calcula cor das linhas
				for ($i = 0; $i < $tamArr; $i++) { $arrDados['livros'][$i]['NLINHA'] = $i + 1 + $arrDados['limit_de']; }
				$arrDados['livros'] = add_corLinha($arrDados['livros'], 1);
				
				// Dados da paginação
				$qtdReg = $this->isbn_model->getLivrosIsbnIndicacao($arrDados, TRUE);
				$nPaginas = $qtdReg[0]['CONT'] / $arrDados['exibir_pp'];
				if ($qtdReg[0]['CONT'] % $arrDados['exibir_pp'] != 0) {	$nPaginas = (int)$nPaginas + 1;	}
				$arrDados['nPaginas'] = $nPaginas;
				$arrDados['qtdReg'] = $qtdReg[0]['CONT'];
            } else {
				$arrDados['livros'] = array();
				$tamArr = 0;
				$arrDados['vazio'] = FALSE;
				$arrDados['pesquisa'] = FALSE;
				$arrDados['nPaginas'] = 0;
				$arrDados['qtdReg'] = 0;
            }
			
			$this->parser->parse('pesquisa/pesquisa_livro_indicacao_view', $arrDados);
		}
    }
    
    function indicaLivro($idLivro = 0, $idEditora = 0, $pg = 0)
    {
    	$post = false;
    	if ($idLivro == 0 && $idEditora == 0 && $pg == 0) {
	    	$idLivro	= $this->input->post('idlivro');
	    	$idEditora	= $this->input->post('ideditora');
	    	$pg			= $this->input->post('nPag');
	    	$post		= true;
    	}
    	
        $this->load->model('global_model');
        $credito = $this->global_model->get_dado('programaindicacao', "INDICACOES", 'IDUSUARIO = ' . $this->session->userdata('idUsuario') . ' AND IDPROGRAMA =' . $this->session->userdata('programa'));

        if($credito > 0)
        {
            //insere na lista de indicações
            $arrIns = Array();
            $arrIns['IDPROGRAMA']	= $this->session->userdata('programa');
            $arrIns['IDUSUARIO']	= $this->session->userdata('idUsuario');
            $arrIns['IDLIVRO']		= $idLivro;
            $arrIns['IDEDITORA']	= $idEditora;

            $this->global_model->insert('bibliotecaindicacao', $arrIns);

            // atualiza subtraindo possibilidades de indicar
            $arrUpd = Array();
            $arrUpd['INDICACOES'] = ($this->global_model->get_dado('programaindicacao', "INDICACOES", 'IDUSUARIO = ' . $this->session->userdata('idUsuario') . ' AND IDPROGRAMA =' . $this->session->userdata('programa'))-1);

            $where = sprintf("IDUSUARIO = %s", $this->session->userdata('idUsuario'));
            //$this->global_model->update('programaindicacao', $arrUpd, $where);

            if ($post) {
            	echo $pg;	
            } else {
            	echo '<script>LoadedX();</script>';
            	die;
            }
            
        }
        else
        {
            echo "ERRO";
        }
    }
    
    function removeLivro($idLivro, $pg)
    {
        $this->load->model('global_model');

        $where = sprintf('IDPROGRAMA = %d AND IDUSUARIO = %d AND IDLIVRO = %d', $this->session->userdata('programa'), $this->session->userdata('idUsuario'), $idLivro);
        $res = $this->global_model->delete('bibliotecaindicacao', $where);

        // atualiza adicionando possibilidades de indicar
        $arrUpd = Array();
        $arrUpd['INDICACOES'] = ($this->global_model->get_dado('programaindicacao', "INDICACOES", 'IDUSUARIO = ' . $this->session->userdata('idUsuario') . ' AND IDPROGRAMA =' . $this->session->userdata('programa'))+1);

        $where = sprintf("IDUSUARIO = %s", $this->session->userdata('idUsuario'));
        //$this->global_model->update('programaindicacao', $arrUpd, $where);

        echo $pg;
    }
    
    function modalpesquisalivroindicacaohelp()
    {   
    	$this->load->view('pesquisa/modal_pesquisa_livro_indicacao_help.php');
    }
    
    function lista_editoras_titulo_unico($idTituloUnico = 0, $nPag = 0)
    {
    	$this->load->model('isbn_model');
		$arrDados = array();
		
		$nPag = $this->input->post('nPag');
		
		$editoras = $this->isbn_model->getEditorasTituloUnico($idTituloUnico);
		
		/*for ($i = 0; $i < count($log); $i++) {
			list($databanco, $horabanco) = explode(' ', $log[$i]['DATA_LOG']);
			$log[$i]['DATA_LOG_FORMAT'] = eua_to_br($databanco) . ' ' . $horabanco;
		}*/
		
		$arrDados['editoras']	= $editoras;
		$arrDados['idlivro']	= $idTituloUnico;
		$arrDados['nPag']		= $nPag;
		//echo count($arrDados['editoras']);
		//echo BASE_URL_SISTEMA . 'pesquisa/indicaLivro/' . $idTituloUnico . '/' . $editoras[0]['IDEDITORA'] . '/' . $nPag;
		//die;
		//if (count($arrDados['editoras']) == 1) {
		//	$url = BASE_URL_SISTEMA . 'pesquisa/indicaLivro/' . $idTituloUnico . '/' . $editoras[0]['IDEDITORA'] . '/' . $nPag;
			//echo $url;
			//die;
		//	header("Location: $url");
		//} else {
			$this->parser->parse('pesquisa/lista_editoras_titulo_unico_modal', $arrDados);
		//}
		
    } 
    
	function comentarios($idLivro)
	{
		// Valida para caso o usuário seja diferente de biblioteca
		if ($this->session->userdata('tipoUsuario') != 2)
		{
			echo '.ERRO.||Permissão negada.';
			exit;
		}
		
		$this->load->model('livro_model');
		$this->load->model('comentario_model');
		
		$arrDados = Array();
		
		// Coleta dados do livro
		$livro = $this->livro_model->getDadosLivro($idLivro);
		$arrDados['idLivro'] = $livro['IDLIVRO'];
		$arrDados['titulo']  = $livro['TITULO'];
		$arrDados['autor']   = $livro['AUTOR'];
		$arrDados['comentarios'] = $this->comentario_model->listaComentarios($idLivro);
		$arrDados['comentarios'] = add_corLinha($arrDados['comentarios'], 1);
		
		$this->parser->parse('pesquisa/comentarios_view', $arrDados);
	}
	
	function comentario_save()
	{
		if ($this->session->userdata('tipoUsuario') == 2) //se for biblioteca
		{
			$this->load->model('global_model');
			
			$idLivro    = $this->input->post('idLivro', TRUE);
			$comentario = $this->input->post('comentario', TRUE);
			$idUser     = $this->session->userdata('idUsuario');
			
			if (trim($comentario) != '' && is_numeric($idLivro))
			{
				$this->db->trans_start();
				
				$arrIns = Array();
				$arrIns['IDLIVRO']    = $idLivro;
				$arrIns['IDUSUARIO']  = $idUser;
				$arrIns['COMENTARIO'] = $comentario;
				
				$this->global_model->insert('livrocomentario', $arrIns);
				
				$this->db->trans_complete();
				
				if ($this->db->trans_status() === FALSE)
				{
					$this->load->library('logsis', Array('INSERT', 'ERRO', 'livrocomentario', $idUser, 'Erro ao escrever comentário para livro', var_export($arrIns, TRUE)));
					
					echo '.ERRO.||Ocorreu um erro ao enviar o pedido. Tente novamente.';
				}
				else
				{
					$this->load->library('logsis', Array('INSERT', 'OK', 'livrocomentario', $idUser, 'Escreveu comentário para livro', var_export($arrIns, TRUE)));
					
					echo 'OK||' . $idLivro;
				}
			}
			else
			{
				echo '.ERRO.||Dados inválidos.';
			}
		}
		else
		{
			echo '.ERRO.||Acesso não permitido.';
		}
	}

}