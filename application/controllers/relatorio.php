﻿<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe Relatorio
*
* Abstração do módulo de relatórios.
* 
* @author		Leandro Mangini Antunes
* @package		application
* @subpackage	application.relatorio
* @since		2012-03-27
*
*/
class Relatorio extends CI_Controller {
	
	/**
	* __construct()
	* Testa se usuario logado é administrador.
	* @return void
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if ($this->session->userdata('acessoSis') == FALSE)
		{
			redirect('/login/');
			exit;
		}
		if ($this->session->userdata('tipoUsuario') != 1)
		{
			redirect('/login/');
			exit;
		}
	}
	
	/**
	* index()
	* Executado caso não exista chamada explícita de método na url.
	* @return void
	*/
	function index()
	{
		redirect('/relatorio/search');
	}
	
	/**
	* search()
	* Exibe listagem de entrada do módulo de relatórios.
	* @return void
	*/
	function search($actual_page = 0)
	{
		// Coleta o formulário de filtros 
		$args['filtros'] = $this->input->post();
		
		// Carrega tabela de dados, seta o array de dados e limita a paginação
		$this->load->model('relatorio_model');
		$this->load->library('array_table');
		$this->array_table->set_id('module_table');		
		$this->array_table->set_data($this->relatorio_model->get_lista_modulo($args['filtros']));
		$this->array_table->set_actual_page($actual_page);
		$this->array_table->set_page_link('relatorio/search');
		
		// adiciona campos a tabela
		$this->array_table->add_column('<a href="' . URL_EXEC . 'relatorio/play/{0}" target="_blank" title="Executar Relatório"><img src="' . URL_IMG . 'icon_play.png" /></a>');
		$this->array_table->set_columns(array('#', 'CATEGORIA', 'TITULO', 'DESCRICAO', 'DATA DE CRIAÇÂO', 'DATA DA ULTIMA EXECUÇÂO', 'ULTIMO USUÁRIO A EXECUTAR'));
		
		// Processa a tabela html
		$args['module_table'] = $this->array_table->get_html();

		// Monta options de status de pedido
		$args['option_categoria'] = monta_options_array($this->relatorio_model->get_categorias_relatorio(), get_value($args['filtros'], 'RC__ID_RELATORIO_CATEGORIA'));
		
		$this->parser->parse('relatorio/search', $args);
	}
	
	/**
	* play()
	* Roda o sql do relatorio selecionado.
	* @param integer idrelatorio
	* @return void
	*/
	function play($idrelatorio = 0)
	{
		// Carrega tabela de dados, seta o array de dados e limita a paginação
		$this->load->model('relatorio_model');
		$this->load->model('global_model');
		$this->load->library('array_table');
		$this->array_table->set_id('module_table');
		$this->array_table->set_data($this->relatorio_model->execute_relatorio_sql($idrelatorio));
		$this->array_table->set_page_link('relatorio/play');
		
		$args['dados'] = $this->relatorio_model->get_dados_relatorio($idrelatorio);
		
		// Processa a tabela html
		$args['module_table'] = $this->array_table->get_html();
		
		// Insere registro de execução de relatório
		$files = array();
		$files['id_relatorio'] 	= $idrelatorio;
		$files['idusuario'] 	= $this->session->userdata('idUsuario');
		$this->global_model->insert('rpt_relatorio_execucao', $files);

		$this->parser->parse('relatorio/show_result_play', $args);
	}
	
	/**
	* download_excel()
	* Download de excel com os resualtdados da pequisa.
	* @param
	* @return void
	*/
	function download_excel()
	{
		// Carrega classe excel e model pedido
		$this->load->library('excel');
		$this->load->model('relatorio_model');
		
		// Executa download do excel
		$this->excel->set_file_name('RELATORIO' . date('Y-m-d_His'));
		$this->excel->set_data($this->relatorio_model->get_lista_modulo());
		$this->excel->download_file();
	}
	
	
	/**
	* lista_excel_result()
	* Download do excel com o resulatado da pesquisa que foi gerada.
	* @param integer idrelatorio
	* @return void
	*/
	function lista_excel_result($idrelatorio = 0)
	{
		// Carrega classe excel e model pedido
		$this->load->library('excel');
		$this->load->model('relatorio_model');
		
		// Executa download do excel
		$this->excel->set_file_name('RELATORIO' . date('Y-m-d_His'));
		$this->excel->set_data($this->relatorio_model->execute_relatorio_sql($idrelatorio));
		$this->excel->download_file();
	}
	
	/**
	* form_insert_relatorio()
	* Mostra a pagina de inserção
	* @return void
	*/
	function form_insert_relatorio()
	{		
		$this->load->model('relatorio_model');
		
		//$args['dados'] = $this->relatorio_model->get_categoria_sql();
		$args['options_categorias'] = monta_options_array($this->relatorio_model->get_categorias_relatorio());
		
		// Processa url_redirect (para redirecionamento do botão aplicar, ok e voltar do form)
		$args['url_redirect'] = ($this->input->get('url') == '') ? 'relatorio/search' : $this->input->get('url');
		
		// Load view
		$this->load->view('relatorio/form_insert_relatorio', $args);
	}
	
	/**
	* form_insert_relatorio_proccess()
	* Insere um sql qualquer para buscas.
	* @param integer 
	* @return void
	*/
	function form_insert_relatorio_proccess()
	{		
		$this->load->model('global_model');
		
		$args = array();
		// carrega os campos a serem inseridos na tabela
		$categoria  = $this->input->post('id_relatorio_categoria');
		$titulo 	= $this->input->post('titulo');
		$descricao  = $this->input->post('descricao');
		$sql 		= $this->input->post('sql');
		
		$args['id_relatorio_categoria'] = $categoria;
		$args['titulo']    				= $titulo;
		$args['descricao'] 				= $descricao;
		$args['sql'] 	   				= $sql;
		
		// insere na tabela
		$this->global_model->insert('rpt_relatorio', $args);
		
		// Redirect para formulário
		$action = $this->input->post('redirect_action');
		if($action == 'ok'){ redirect($this->input->post('redirect_ok')); } else {redirect( $this->input->post('redirect_aplicar') . '/?url=' . $this->input->post('redirect_ok')); }
	}
}