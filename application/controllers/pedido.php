<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pedido extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if ($this->session->userdata('acessoSis') == FALSE)
		{
			redirect('/login/');
			exit;
		}
		
		// Valida para caso o usuário seja diferente de biblioteca ou administrador
		if ($this->session->userdata('tipoUsuario') != 1 && $this->session->userdata('tipoUsuario') != 2 && $this->session->userdata('tipoUsuario') != 4 && $this->session->userdata('tipoUsuario') != 5)
		{
			redirect('/login/');
			exit;
		}
	}
	
	/*************************************************************************
	 *
	 * CONTROLLERS DE PEDIDO
	 *
	 * lista			      : Lista pedidos para a biblioteca corrente
	 * addPedidoItem	      : Adiciona itens ao pedido da biblioteca logada
	 * removePedidoItem       : 
	 * modalFormNewItem       :	 
	 * modalFormUpdateItem    : 
	 * addNewItem 	          : 
	 * updateItem 	          : 
	 * removeItem 	          : 
	 * gerencia               : tela para gerenciar com link para finalizar pedido
	 * gerencia_itens         : exibe itens do pedido
	 * gerencia_totais        : exibe parte de totais do pedido
	 * gerencia_envioped      : envia pedido ao PDV
	 * gerencia_confirmaped   : confirma recebimento do pedido
	 * gerencia_formconf      : formulario para confirmação de qtd recebida
	 * gerencia_saveajusteped : salva ajuste de quantidade recebida do pedido
	 *
	 *************************************************************************/
	
	function index()
	{
		$arrErro['msg']      = 'Página não existe.';
		$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
		$this->load->view('erro_view', $arrErro);
	}
	
	function nfunc()
	{
		$arrErro['msg']      = 'Esta funcionalidade não está disponível.';
		$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
		$this->load->view('erro_view', $arrErro);
	}
	
	function lista($nPag = 0)
	{
		$this->load->model('pedido_model');
		$this->load->model('programa_model');
		
		$arrDados = Array();
		$arrDados['nPag']      = (is_numeric($nPag) && $nPag > 0 ? $nPag : 1);
		$arrDados['exibir_pp'] = 15;
		$arrDados['limit_de']  = ($arrDados['nPag'] - 1) * $arrDados['exibir_pp'];
		
		// Filtros
		$filtros = (isset($_POST['filtro_programa'])) ? $_POST : null;
		$arrDados['filtros'] = $this->pedido_model->getArrayFiltros($filtros);
		$this->pedido_model->setFiltro('PROGRAMA', $arrDados['filtros']['filtro_programa']);
		$this->pedido_model->setFiltro('STATUS', $arrDados['filtros']['filtro_status']);
		$this->pedido_model->setFiltro('PDV', $arrDados['filtros']['filtro_pdv']);
		$this->pedido_model->setFiltro('IDUSUARIO', $this->session->userdata('idUsuario'));
		
		// Coleta options de PROGRAMA
		$arrDados['optionsPrograma'] = monta_options_array($this->programa_model->getAllProgramas(), $arrDados['filtros']['filtro_programa']);
		
		// Coleta options de STATUS DE PEDIDO
		$arrDados['optionsStatus'] = monta_options_array($this->pedido_model->getAllStatus(), $arrDados['filtros']['filtro_status']);
		
		// pesquisa
		$arrDados['pedidos'] = $this->pedido_model->listaPedidosBiblioteca($arrDados, FALSE);
		$tamArr = count($arrDados['pedidos']);
		$arrDados['vazio'] = ($tamArr == 0 ? TRUE : FALSE);
		
		for ($i = 0; $i < $tamArr; $i++)
		{
			$arrDados['pedidos'][$i]['NLINHA'] = $i + 1 + $arrDados['limit_de'];
			$arrDados['pedidos'][$i]['VALORTOTAL'] = 'R$ ' . masc_real($arrDados['pedidos'][$i]['VALORTOTAL']);
		}
		
		$arrDados['pedidos'] = add_corLinha($arrDados['pedidos'], 1);
		
		// dados da paginação
		$qtdReg = $this->pedido_model->listaPedidosBiblioteca($arrDados, TRUE);
		$nPaginas = $qtdReg[0]['CONT'] / $arrDados['exibir_pp'];
		
		if ($qtdReg[0]['CONT'] % $arrDados['exibir_pp'] != 0)
		{
			$nPaginas = (int)$nPaginas + 1;
		}
		
		$arrDados['nPaginas'] = $nPaginas;
		$arrDados['qtdReg']   = $qtdReg[0]['CONT'];
		
		$this->parser->parse('pedido/pedido_lista_view', $arrDados);
	}
	
	function addPedidoItem($idItem = 0)
	{
		// Valida para caso o usuário seja diferente de biblioteca
		if ($this->session->userdata('tipoUsuario') != 2)
		{
			echo '.ERRO.||Permissão negada.';
			exit;
		}
		
		// Carrega model para verificação de pedido
		$this->load->model('pedido_model');
		
		// Coleta o idpedido do usuario atual
		$idPedido = $this->pedido_model->getIdPedido($this->session->userdata('idUsuario'));
		
		// Verifica se o item já existe, para chamar modal de atualização, caso contrário, inserção
		if($this->pedido_model->itemExists($idItem, $idPedido) && $this->pedido_model->pedidoExists($this->session->userdata('idUsuario')))
		{
			$this->_modalFormUpdateItem($idItem);
		}
		else
		{
			$this->_modalFormNewItem($idItem);
		}
	}
	
	function removePedidoItem($idItem = 0)
	{
		// Valida para caso o usuário seja diferente de biblioteca
		if ($this->session->userdata('tipoUsuario') != 2)
		{
			echo '.ERRO.||Permissão negada.';
			exit;
		}
		
		// Carrega model para verificação de pedido
		$this->load->model('livro_model');
		$this->load->model('pedido_model');
		
		//valida se item existe no pedido do usuário
		if ($this->pedido_model->validaItemPed($this->session->userdata('programa'), $this->session->userdata('idUsuario'), $idItem))
		{
			$arrDados = Array();
			
			// Coleta o idpedido do usuario atual (presume-se que até este ponto já exista um pedido cadastrado para o user biblioteca logado)
			$idPedido = $this->pedido_model->getIdPedido($this->session->userdata('idUsuario'));
			
			// Coleta dados do livro
			$livro = $this->livro_model->getDadosLivro($idItem);
			$arrDados['idLivro'] = $livro['IDLIVRO'];
			$arrDados['titulo']  = $livro['TITULO'];
			$arrDados['autor']   = $livro['AUTOR'];
			$arrDados['editora'] = $livro['RAZAOSOCIAL'];
			$arrDados['edicao']  = $livro['EDICAO'];
			$arrDados['ano']     = $livro['ANO'];
			$arrDados['preco']   = $livro['PRECO'];
			$arrDados['visualizacao']	= $this->input->post('visualizacao');
			
			// Coleta os dados do item com base no id do item encaminhado
			$idPedido = $this->pedido_model->getIdPedido($this->session->userdata('idUsuario'));
			$itemPedido = $this->pedido_model->getDadosItem($idItem, $idPedido);
			$arrDados['qtd'] = $itemPedido['QTD'];
			
			$this->parser->parse('pedido/form_delete_item', $arrDados);
		}
		else
		{
			echo 'Operação não permitida.';
		}
	}
	
	function _modalFormNewItem($idItem = 0)
	{
		// Valida para caso o usuário seja diferente de biblioteca
		if ($this->session->userdata('tipoUsuario') != 2)
		{
			echo '.ERRO.||Permissão negada.';
			exit;
		}
		
		$this->load->model('livro_model');
		
		//valida se livro faz parte do programa
		if ($this->livro_model->validaLivroPrograma($this->session->userdata('programa'), $idItem))
		{
			$arrDados = Array();
			
			// Coleta dados do livro
			$livro = $this->livro_model->getDadosLivro($idItem);
			$arrDados['idLivro']		= $livro['IDLIVRO'];
			$arrDados['titulo']			= $livro['TITULO'];
			$arrDados['autor']			= $livro['AUTOR'];
			$arrDados['editora']		= $livro['RAZAOSOCIAL'];
			$arrDados['edicao']			= $livro['EDICAO'];
			$arrDados['ano']			= $livro['ANO'];
			$arrDados['preco']			= $livro['PRECO'];
			$arrDados['visualizacao']	= $this->input->post('visualizacao');
			
			$this->parser->parse('pedido/form_new_item', $arrDados);
		}
		else
		{
			echo 'O livro informado não faz parte do programa.';
		}
	}
	
	function _modalFormUpdateItem($idItem = 0)
	{
		// Valida para caso o usuário seja diferente de biblioteca
		if ($this->session->userdata('tipoUsuario') != 2)
		{
			echo '.ERRO.||Permissão negada.';
			exit;
		}
		
		$this->load->model('livro_model');
		$this->load->model('pedido_model');
		
		//valida se item existe no pedido do usuário
		if ($this->pedido_model->validaItemPed($this->session->userdata('programa'), $this->session->userdata('idUsuario'), $idItem))
		{
			$arrDados = Array();
			
			// Coleta dados do livro
			$livro = $this->livro_model->getDadosLivro($idItem);
			$arrDados['idLivro'] = $livro['IDLIVRO'];
			$arrDados['titulo']  = $livro['TITULO'];
			$arrDados['autor']   = $livro['AUTOR'];
			$arrDados['editora'] = $livro['RAZAOSOCIAL'];
			$arrDados['edicao']  = $livro['EDICAO'];
			$arrDados['ano']     = $livro['ANO'];
			$arrDados['preco']   = $livro['PRECO'];
			$arrDados['visualizacao']	= $this->input->post('visualizacao');
			
			// Coleta os dados do item com base no id do item encaminhado
			$idPedido = $this->pedido_model->getIdPedido($this->session->userdata('idUsuario'));
			$itemPedido = $this->pedido_model->getDadosItem($idItem, $idPedido);
			$arrDados['qtd'] = $itemPedido['QTD'];
			
			$this->parser->parse('pedido/form_update_item', $arrDados);
		}
		else
		{
			echo 'Operação não permitida.';
		}
	}
	
	function _modalVerificaComite($idBiblioteca = 0)
	{
		// Valida para caso o usuário seja diferente de biblioteca
		if ($this->session->userdata('tipoUsuario') != 2)
		{
			echo '.ERRO.||Permissão negada.';
			exit;
		}
		
		$this->load->model('biblioteca_model');
		$this->load->library('session');
		
		//valida se biblioteca tem comitê
		
		if ($comite = $this->biblioteca_model->listaComite($idBiblioteca, $this->session->userdata('programa')))
		{
			$dados = Array();
			$arrDados = Array();
			
			foreach ($comite as $c) {
				$dc = array(
					'id'	=> $c['IDUSUARIO'],
					'nome'	=> $c['NOMERESP'],
					'login'	=> $c['LOGIN']
				);
				
				array_push($dados, $dc);
			}
			
			$arrDados['idBiblioteca'] = $comite[0]['IDBIBLIOTECA'];
			$arrDados['comite'] = $dados;
		
			$this->parser->parse('pedido/valida_comite', $arrDados);
		}
		else
		{
			echo 'A biblioteca informada não possui comitê de acervo.';
		}
	}
		
	function addNewItem($idItem = 0, $qtd = 0)
	{
		// Valida para caso o usuário seja diferente de biblioteca
		if ($this->session->userdata('tipoUsuario') != 2)
		{
			echo '.ERRO.||Permissão negada.';
			exit;
		}
		
		// Carrega model para verificação de pedido
		$this->load->model('livro_model');
		$this->load->model('pedido_model');
		$this->load->model('biblioteca_model');
		$this->load->model('configuracao_model');
		$this->load->model('global_model');
		
		//valida se livro faz parte do programa
		if ($this->livro_model->validaLivroPrograma($this->session->userdata('programa'), $idItem))
		{
			// Faz validação parcial de erros
			$msgError  = '';
			$msgError .= (!$this->biblioteca_model->isHabilitado($this->session->userdata('idUsuario'))) ? "Sua biblioteca não está habilitada.\n" : '';
			$msgError .= (!$this->configuracao_model->validaConfig(19, date('Y-m-d'))) ? "O período para aquisição de livros não está vigente.\n" : '';
			if($msgError != '')
			{
				echo('.ERRO.||' . $msgError);
				return null;
			}
			
			//valida quantidade
			if ( ! is_numeric($qtd) || $qtd <= 0)
			{
				echo '.ERRO.||Quantidade inválida.';
				exit;
			}
			
			$this->db->trans_start();
			
			// Verifica se usuário tem pedido criado, caso contrário, cria um novo
			if( ! $this->pedido_model->pedidoExists($this->session->userdata('idUsuario')))
			{
				$where = sprintf("IDPROGRAMA = %s AND IDBIBLIOTECA = %s", $this->session->userdata('programa'), $this->session->userdata('idUsuario'));
				$idPdv = $this->global_model->get_dado('programapdvparc', 'IDPDV', $where);
				
				$arrPed = Array();
				$arrPed['IDPROGRAMA']     = $this->session->userdata('programa');
				$arrPed['IDBIBLIOTECA']   = $this->session->userdata('idUsuario');
				$arrPed['IDPDV']          = ($idPdv != '' ? $idPdv : NULL);
				$arrPed['DATA_ULT_ATU']   = date('Y/m/d H:i:s');
				$arrPed['VALORTOTAL']     = 0;
				$arrPed['IDPEDIDOSTATUS'] = 1;
				
				$this->global_model->insert('pedido', $arrPed);
				
				$arrLog = Array();
				$arrLog['IDPEDIDO']       = $this->global_model->get_insert_id();
				$arrLog['IDPEDIDOSTATUS'] = 1;
				$arrLog['IDUSUARIO']      = $this->session->userdata('idUsuario');
				$arrLog['DATAHORA']       = date('Y/m/d H:i:s');
				
				$this->global_model->insert('pedidolog', $arrLog);
			}
			
			// Coleta o idpedido do usuario atual
			$idPedido = $this->pedido_model->getIdPedido($this->session->userdata('idUsuario'));
			
			// Verifica se pedido está em confecção
			$msgError .= (!$this->pedido_model->isStatusConfeccao($idPedido)) ? "O pedido atual não está mais em processo de confecção.\n" : '';
			
			if ($msgError == '')
			{
				// Coleta dados do livro
				$livro = $this->livro_model->getDadosLivro($idItem);
				$preco = $livro['PRECO'];
				
				// Validação para crédito atual em função do quanto se estará gastando
				$creditoAtual = $this->biblioteca_model->getCreditoAtual($this->session->userdata('idUsuario'));
				$valorCompra  = $qtd * $preco;
				
				// Valida o resultado, se valorCompra >= creditoAtual então não permite adicionar à lista
				$msgError .= ($valorCompra > $creditoAtual) ? "Você não possui créditos suficientes para adicionar a quantidade selecionada na sua lista de aquisições.\n" : '';
			}
			
			if ($msgError == '')
			{
				// Próxima Validação, valida se a quantidade que está prestes a ser adicionada está disponível
				$qtdeDisponivel   = $this->livro_model->getQtdeDisponivel($idItem);
				$qtdeMaxPermitida = $this->configuracao_model->getConfigValue(12);
				
				// Valida o resultado, se qtd >= qtdeDisponivel então não permite adicionar à lista ou se 
				// qdte disponivel for menor que zero, exibe informacao que livro esta esgotado
				if($qtdeDisponivel <= 0)
				{
					$msgError .= "A quantidade disponível para este livro está esgotada.\n";
				}
				elseif ($qtd > $qtdeDisponivel)
				{
					$msgError .= "A quantidade selecionada para este livro não está disponível.\nTente novamente informando uma quantidade menor.\n";
				}
				
				if($msgError != '')
				{
					// Salva em demanda reprimida
					$this->pedido_model->saveDemandaReprimida($idPedido, $idItem, $qtdeDisponivel, $qtdeMaxPermitida, $qtd);
				}
			}
			
			if ($msgError == '')
			{
				// valida quantidade máxima permitida
				$msgError .= ($qtd > $qtdeMaxPermitida) ? "A quantidade selecionada ultrapassou a quantidade máxima permitida para aquisição deste livro.\nTente novamente informando uma quantidade menor.\n" : '';
				
				if($msgError != '')
				{
					// Salva em demanda reprimida
					$this->pedido_model->saveDemandaReprimida($idPedido, $idItem, $qtdeDisponivel, $qtdeMaxPermitida, $qtd);
				}
			}
			
			if ($msgError == '')
			{
				// Adiciona um item ao pedido atual
				$this->pedido_model->addItem($idItem, $qtd, $idPedido);
			}
			
			$this->db->trans_complete();
			
			if($msgError != '')
			{
				echo '.ERRO.||' . $msgError;
				return null;
			}
			
			if ($this->db->trans_status() === FALSE)
			{
				echo '.ERRO.||Ocorreu um erro ao realizar a operação. Tente novamente.';
			}
			else
			{
				echo 'OK';
			}
		}
		else
		{
			echo '.ERRO.||O livro informado não faz parte do programa.';
		}
	}
	
	function updateItem($idItem = 0, $qtd = 0)
	{
		// Valida para caso o usuário seja diferente de biblioteca
		if ($this->session->userdata('tipoUsuario') != 2)
		{
			echo '.ERRO.||Permissão negada.';
			exit;
		}
		
		// Carrega model para verificação de pedido
		$this->load->model('livro_model');
		$this->load->model('pedido_model');
		$this->load->model('biblioteca_model');
		$this->load->model('configuracao_model');
		
		//valida se item existe no pedido do usuário
		if ($this->pedido_model->validaItemPed($this->session->userdata('programa'), $this->session->userdata('idUsuario'), $idItem))
		{
			// Coleta o idpedido do usuario atual (presume-se que se chegou até aqui, idPedido existe)
			$idPedido = $this->pedido_model->getIdPedido($this->session->userdata('idUsuario'));
			
			// Faz validação parcial de erros
			$msgError  = '';
			$msgError .= (!$this->biblioteca_model->isHabilitado($this->session->userdata('idUsuario'))) ? "Sua biblioteca não está habilitada.\n" : '';
			$msgError .= (!$this->configuracao_model->validaConfig(19, date('Y-m-d'))) ? "O período para aquisição de livros não está vigente.\n" : '';
			$msgError .= (!$this->pedido_model->isStatusConfeccao($idPedido)) ? "O pedido atual não está mais em processo de confecção.\n" : '';
			if($msgError != '')
			{
				echo('.ERRO.||' . $msgError);
				return null;
			}
			
			//valida quantidade
			if ( ! is_numeric($qtd) || $qtd <= 0)
			{
				echo '.ERRO.||Quantidade inválida.';
				exit;
			}
			
			// Coleta dados do livro
			$livro = $this->livro_model->getDadosLivro($idItem);
			$preco = $livro['PRECO'];
			
			// Validação para crédito atual em função do quanto se estará gastando
			$creditoAtual = $this->biblioteca_model->getCreditoAtual($this->session->userdata('idUsuario'));
			$valorCompra  = $qtd * $preco;
			
			// Valida o resultado, se valorCompra >= creditoAtual então não permite adicionar à lista
			$msgError .= ($valorCompra > $creditoAtual) ? "Você não possui créditos suficientes para adicionar a quantidade selecionada na sua lista de aquisições.\n" : '';
			if($msgError != '')
			{
				echo('.ERRO.||' . $msgError);
				return null;
			}
			
			$this->db->trans_start();
			
			// Próxima Validação, valida se a quantidade que está prestes a ser adicionada está disponível
			$qtdeDisponivel   = $this->livro_model->getQtdeDisponivel($idItem);
			$itemDados        = $this->pedido_model->getDadosItem($idItem, $idPedido);
			$qtdeMaxPermitida = $this->configuracao_model->getConfigValue(12);
			
			// Valida o resultado, se qtd >= qtdeDisponivel então não permite adicionar à lista ou se 
			// qdte disponivel for menor que zero, exibe informacao que livro esta esgotado - para o update
			// tem que ser levado em consideração a quantidade já existente do item atual, isto é, essa também
			// é uma quantidade disponível
			if($qtdeDisponivel <= 0 && $qtd > $itemDados['QTD'])
			{
				$msgError .= "A quantidade disponível para este livro está esgotada.\n";
			}
			else if ($qtd > round($qtdeDisponivel + $itemDados['QTD']))
			{
				$msgError .= "A quantidade selecionada para este livro não está disponível.\nTente novamente informando uma quantidade menor.\n";
			}
			
			if($msgError != '')
			{
				// Salva em demanda reprimida
				$this->pedido_model->saveDemandaReprimida($idPedido, $idItem, $qtdeDisponivel, $qtdeMaxPermitida, $qtd);
			}
			
			if($msgError == '')
			{
				// valor quantidade máxima permitida
				$msgError .= ($qtd > $qtdeMaxPermitida) ? "A quantidade selecionada ultrapassou a quantidade máxima permitida para aquisição deste livro.\nTente novamente informando uma quantidade menor.\n" : '';
				
				if($msgError != '')
				{
					// Salva em demanda reprimida
					$this->pedido_model->saveDemandaReprimida($idPedido, $idItem, $qtdeDisponivel, $qtdeMaxPermitida, $qtd);
				}
			}
			
			if($msgError == '')
			{
				// Atualiza o item do pedido atual
				$this->pedido_model->updateItem($idItem, $qtd, $idPedido);
			}
			
			$this->db->trans_complete();
			
			if($msgError != '')
			{
				echo '.ERRO.||' . $msgError;
				return null;
			}
			
			if ($this->db->trans_status() === FALSE)
			{
				echo '.ERRO.||Ocorreu um erro ao realizar a operação. Tente novamente.';
			}
			else
			{
				echo 'OK';
			}
		}
		else
		{
			echo '.ERRO.||Operação não permitida.';
		}
	}
	
	function removeItem($idItem = 0)
	{
		// Valida para caso o usuário seja diferente de biblioteca
		if ($this->session->userdata('tipoUsuario') != 2)
		{
			echo '.ERRO.||Permissão negada.';
			exit;
		}
		
		// Carrega model para verificação de pedido
		$this->load->model('pedido_model');
		$this->load->model('biblioteca_model');
		$this->load->model('configuracao_model');
		
		//valida se item existe no pedido do usuário
		if ($this->pedido_model->validaItemPed($this->session->userdata('programa'), $this->session->userdata('idUsuario'), $idItem))
		{
			// Coleta o idpedido do usuario atual (presume-se que se chegou até aqui, idPedido existe)
			$idPedido = $this->pedido_model->getIdPedido($this->session->userdata('idUsuario'));
			
			// Faz validação parcial de erros
			$msgError  = '';
			$msgError .= (!$this->biblioteca_model->isHabilitado($this->session->userdata('idUsuario'))) ? "Sua biblioteca não está habilitada.\n" : '';
			$msgError .= (!$this->configuracao_model->validaConfig(19, date('Y-m-d'))) ? "O período para aquisição de livros não está vigente.\n" : '';
			$msgError .= (!$this->pedido_model->isStatusConfeccao($idPedido)) ? "O pedido atual não está mais em processo de confecção.\n" : '';
			if($msgError != '')
			{
				echo('.ERRO.||' . $msgError);
				return null;
			}
			
			// Atualiza o item do pedido atual
			$this->pedido_model->removeItem($idItem, $idPedido);
		}
		else
		{
			echo '.ERRO.||Operação não permitida.';
		}
	}
	
	function gerencia($idPrograma = null, $idPedido = 0)
	{
        if($idPrograma == null)
        {
            $idPrograma = $this->session->userdata('programa');
        }
        
		// Testa se está no período para a aquisição / realização de pedidos
		$this->load->model('configuracao_model');
		$arrErro['msg'] = (!$this->configuracao_model->validaConfig(23, date('Y-m-d'))) ? 'O período para gerenciamento de pedidos não está vigente.'   : '';
		$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
		if($arrErro['msg'] != '' && $arrErro['tipoUser'] != 1)
		{
			$this->load->view('erro_view', $arrErro);
		} else {
			if ($this->session->userdata('tipoUsuario') == 1 || $this->session->userdata('tipoUsuario') == 2) //se for administrador ou biblioteca
			{
				$this->load->model('biblioteca_model');
				$this->load->model('pedido_model');
				$this->load->model('pdv_model');
				$this->load->model('global_model');
				
				$idUser = $this->session->userdata('idUsuario');
				
				if ($idPedido == 0)
				{
					$where = sprintf("IDPROGRAMA = %s AND IDBIBLIOTECA = %s", $this->session->userdata('programa'), $idUser);
					$idPedido = $this->global_model->get_dado('pedido', 'IDPEDIDO',$where);
				}
				
				if ($this->session->userdata('tipoUsuario') == 1)
				{
					$idUser = 0;
				}
				
				$dadosPed = $this->pedido_model->getDadosPedido($idPrograma, $idUser, $idPedido);
				
				if (count($dadosPed) > 0)
				{
					$arrLivrosPed = $this->pedido_model->getItensPedido($idPrograma, $idUser, $idPedido);
					
					$arrDados = Array();
					$arrDados['dadosped']  = $dadosPed;
					$arrDados['tipoUser']  = $this->session->userdata('tipoUsuario');
					$arrDados['statusPed'] = $dadosPed[0]['IDPEDIDOSTATUS'];
					$arrDados['historico'] = $this->pedido_model->getHistoricoPedido($idPedido);
					$arrDados['nf']        = '';
					$arrDados['ce']        = '';
					$arrDados['temLivros'] = (count($arrLivrosPed) > 0 ? TRUE : FALSE);
					// QUANTIDADE DE MEMBROS NO COMITE DE ACERVO
					$arrDados['qtdMembrosComite'] = $this->biblioteca_model->validaNewMembro($this->session->userdata('idUsuario'));
					
					if ($arrDados['statusPed'] >= 4)
					{
						if (file_exists('./application/doc_pdv/' . md5('nf' . $idPedido) . '.jpg'))
						{
							$arrDados['nf'] = md5('nf' . $idPedido) . '.jpg';
						}
						
						if (file_exists('./application/doc_pdv/' . md5('ce' . $idPedido) . '.jpg'))
						{
							$arrDados['ce'] = md5('ce' . $idPedido) . '.jpg';
						}
					}
					
					$this->parser->parse('pedido/gerencia_view', $arrDados);
				}
				else
				{
					$arrErro['msg']      = 'Pedido não encontrado.';
					$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
					$this->load->view('erro_view', $arrErro);
				}
			}
			else
			{
				$arrErro['msg']      = 'Acesso não permitido.';
				$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
				$this->load->view('erro_view', $arrErro);
			}
		}
	}

    function alteraStatus($idPedido = 0){
       
        if ($this->session->userdata('tipoUsuario') == 1) //se for administrador
		{
			$this->load->model('global_model');
			
			$this->db->trans_start();
			$arrUpd = Array();
			$arrUpd['IDPEDIDOSTATUS'] = 1;
			$arrUpd['DATA_ULT_ATU']   = date('Y/m/d H:i:s');
			
			$where = sprintf("IDPEDIDO = %d",  $idPedido);
			
			$res = $this->global_model->update('pedido', $arrUpd, $where);

			$arrIns = Array();
			$arrIns['IDPEDIDO']       = $idPedido;
			$arrIns['IDPEDIDOSTATUS'] = 1;
			$arrIns['IDUSUARIO']      = $this->session->userdata('idUsuario');
			$arrIns['DATAHORA']       = date('Y/m/d H:i:s');
						
			$this->global_model->insert('pedidolog', $arrIns);
						
			$this->db->trans_complete();
            
            // LOG
            $this->load->library('LOGSIS', Array('UPDATE', 'OK', '', $this->session->userdata('idUsuario'), 'Pedido alterado para Em Confecção',''));
        }
		else
		{
			$arrErro['msg']      = 'Acesso não permitido.';
			$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
			$this->load->view('erro_view', $arrErro);
		}
        
    }
    
	function gerencia_itens()
	{
		if ($this->session->userdata('tipoUsuario') == 1 || $this->session->userdata('tipoUsuario') == 2) //se for administrador ou biblioteca
		{
			$this->load->model('pedido_model');
			$this->load->model('global_model');
			
			$idPrograma = $this->input->post('idprograma');
			$idPedido   = $this->input->post('idpedido');
			$idUser     = $this->session->userdata('idUsuario');
			
			if ($this->session->userdata('tipoUsuario') == 1)
			{
				$idUser = 0;
			}
			
			$arrDados = Array();
			
			$arrDados['livros']    = $this->pedido_model->getItensPedido($idPrograma, $idUser, $idPedido);
			$tamArr                = count($arrDados['livros']);
			$arrDados['vazio']     = ($tamArr == 0 ? TRUE : FALSE);
			$arrDados['livros']    = add_corLinha($arrDados['livros'], 1, TRUE);
			$arrDados['tipoUser']  = $this->session->userdata('tipoUsuario');
			$arrDados['statusPed'] = $this->global_model->get_dado('pedido', 'IDPEDIDOSTATUS','IDPEDIDO = ' . $idPedido);
			
			for ($i = 0; $i < $tamArr; $i++)
			{
				$arrDados['livros'][$i]['DADOSLIVRO'] = '<b>' . $arrDados['livros'][$i]['TITULO'] . '</b><br /><i>Edição: ' . $arrDados['livros'][$i]['EDICAO'] . ' - ' . $arrDados['livros'][$i]['ANO'] . '</i> <br />Autor: ' . ucwords(strtolower($arrDados['livros'][$i]['AUTOR'])) . '<br />Editora: ' . ucwords(strtolower($arrDados['livros'][$i]['EDITORA']));
				$arrDados['livros'][$i]['PRECO_UNIT'] = masc_real($arrDados['livros'][$i]['PRECO_UNIT']);
				$arrDados['livros'][$i]['SUBTOTAL']   = masc_real($arrDados['livros'][$i]['SUBTOTAL']);
			}
			
			$this->parser->parse('pedido/gerencia_itens_view', $arrDados);
		}
		else
		{
			echo '.ERRO.||Acesso não permitido.';
		}
	}
	
	function gerencia_totais()
	{
		if ($this->session->userdata('tipoUsuario') == 1 || $this->session->userdata('tipoUsuario') == 2) //se for administrador ou biblioteca
		{
			$this->load->model('pedido_model');
			
			$idPrograma = $this->input->post('idprograma');
			$idPedido   = $this->input->post('idpedido');
			$idUser     = $this->session->userdata('idUsuario');
			
			if ($this->session->userdata('tipoUsuario') == 1)
			{
				$idUser = 0;
			}
			
			$dadosPed = $this->pedido_model->getDadosPedido($idPrograma, $idUser, $idPedido);
			
			if (count($dadosPed) > 0)
			{
				$arrDados = Array();
				$arrDados['qtdItens']    = $this->pedido_model->getCountItens($idPedido);
				$arrDados['qtdUnidades'] = $this->pedido_model->getCountExemplares($idPedido);
				$arrDados['valorTotal']  = masc_real($dadosPed[0]['VALORTOTAL']);
				
				$this->parser->parse('pedido/gerencia_totais_view', $arrDados);
			}
			else
			{
				echo '.ERRO.||Pedido não encontrado.';
			}
		}
		else
		{
			echo '.ERRO.||Acesso não permitido.';
		}
	}
	
	function gerencia_envioped()
	{
		if ($this->session->userdata('tipoUsuario') == 2) //se for biblioteca
		{
			$this->load->model('pedido_model');
			$this->load->model('global_model');
			
			$idPrograma = $this->input->post('idprograma');
			$idPedido   = $this->input->post('idpedido');
			$idUser     = $this->session->userdata('idUsuario');
			
			$dadosPed = $this->pedido_model->getDadosPedido($idPrograma, $idUser, $idPedido);
			
			if (count($dadosPed) > 0)
			{
				if ($dadosPed[0]['RAZAOSOCIAL_P'] != '')
				{
					if ($dadosPed[0]['IDPEDIDOSTATUS'] == 1)
					{
						$this->db->trans_start();
						
						$arrUpd = Array();
						$arrUpd['IDPEDIDOSTATUS'] = 2;
						$arrUpd['DATA_ULT_ATU']   = date('Y/m/d H:i:s');
						
						$this->global_model->update('pedido', $arrUpd, 'IDPEDIDO = ' . $idPedido);
						
						$arrIns = Array();
						$arrIns['IDPEDIDO']       = $idPedido;
						$arrIns['IDPEDIDOSTATUS'] = 2;
						$arrIns['IDUSUARIO']      = $idUser;
						$arrIns['DATAHORA']       = date('Y/m/d H:i:s');
						
						$this->global_model->insert('pedidolog', $arrIns);
						
						$this->db->trans_complete();
						
						if ($this->db->trans_status() === FALSE)
						{
							$this->load->library('logsis', Array('UPDATE', 'ERRO', 'pedido', $idUser, 'Envio de pedido para PDV', var_export($arrUpd, TRUE)));
							
							echo '.ERRO.||Ocorreu um erro ao enviar o pedido. Tente novamente.';
						}
						else
						{
							$this->load->library('logsis', Array('UPDATE', 'OK', 'pedido', $idUser, 'Envio de pedido para PDV', var_export($arrUpd, TRUE)));
							
							echo 'OK';
						}
					}
					else
					{
						echo '.ERRO.||Pedido já enviado.';
					}
				}
				else
				{
					echo '.ERRO.||Para enviar o pedido você deve selecionar seu Ponto de Venda Parceiro.';
				}
			}
			else
			{
				echo '.ERRO.||Pedido não encontrado.';
			}
		}
		else
		{
			echo '.ERRO.||Acesso não permitido.';
		}
	}
	
	function gerencia_confirmaped()
	{
		if ($this->session->userdata('tipoUsuario') == 2) //se for biblioteca
		{
			if ($this->session->userdata('idFinanceiro') != 0)
			{
				$this->load->model('pedido_model');
				$this->load->model('global_model');
				
				$idPrograma = $this->input->post('idprograma');
				$idPedido   = $this->input->post('idpedido');
				$idUser     = $this->session->userdata('idUsuario');
				
				$dadosPed = $this->pedido_model->getDadosPedido($idPrograma, $idUser, $idPedido);
				
				if (count($dadosPed) > 0)
				{
					if ($dadosPed[0]['IDPEDIDOSTATUS'] == 4)
					{
						$this->db->trans_start();
						
						$arrUpd = Array();
						$arrUpd['IDPEDIDOSTATUS'] = 5;
						$arrUpd['DATA_ULT_ATU']   = date('Y/m/d H:i:s');
						
						$this->global_model->update('pedido', $arrUpd, 'IDPEDIDO = ' . $idPedido);
						
						$arrIns = Array();
						$arrIns['IDPEDIDO']       = $idPedido;
						$arrIns['IDPEDIDOSTATUS'] = 5;
						$arrIns['IDUSUARIO']      = $idUser;
						$arrIns['DATAHORA']       = date('Y/m/d H:i:s');
						
						$this->global_model->insert('pedidolog', $arrIns);
						
						$this->db->trans_complete();
						
						if ($this->db->trans_status() === FALSE)
						{
							$this->load->library('logsis', Array('UPDATE', 'ERRO', 'pedido', $idUser, 'Confirmação de recebimento de pedido pela Biblioteca', var_export($arrUpd, TRUE)));
							
							echo '.ERRO.||Ocorreu um erro ao enviar o pedido. Tente novamente.';
						}
						else
						{
							$this->load->library('logsis', Array('UPDATE', 'OK', 'pedido', $idUser, 'Confirmação de recebimento de pedido pela Biblioteca', var_export($arrUpd, TRUE)));
							
							echo 'OK';
						}
					}
					else
					{
						echo '.ERRO.||Não é possível confirmar o pedido nesse momento.';
					}
				}
				else
				{
					echo '.ERRO.||Pedido não encontrado.';
				}
			}
			else
			{
				echo '.ERRO.||Essa confirmação só pode ser feita pelo responsável financeiro.';
			}
		}
		else
		{
			echo '.ERRO.||Acesso não permitido.';
		}
	}
	
	function gerencia_formconf($idItem = 0)
	{
		// Valida para caso o usuário seja diferente de biblioteca
		if ($this->session->userdata('tipoUsuario') != 2)
		{
			echo 'Permissão negada.';
			exit;
		}
		
		$this->load->model('pedido_model');
		$this->load->model('livro_model');
		
		$idPedido = $this->pedido_model->getIdPedido($this->session->userdata('idUsuario'));
		
		if($this->pedido_model->itemExists($idItem, $idPedido) && $this->pedido_model->pedidoExists($this->session->userdata('idUsuario')))
		{
			$arrDados = Array();
			
			$livro = $this->livro_model->getDadosLivro($idItem);
			
			$arrDados['idLivro'] = $livro['IDLIVRO'];
			$arrDados['titulo']  = $livro['TITULO'];
			$arrDados['autor']   = $livro['AUTOR'];
			$arrDados['editora'] = $livro['RAZAOSOCIAL'];
			$arrDados['edicao']  = $livro['EDICAO'];
			$arrDados['ano']     = $livro['ANO'];
			
			$itemPedido = $this->pedido_model->getDadosItem($idItem, $idPedido);
			
			$arrDados['qtd_orig'] = $itemPedido['QTD'];
			$arrDados['qtd']      = $itemPedido['QTD_ENTREGUE'];
			$arrDados['preco']    = $itemPedido['PRECO_UNIT'];
			
			$this->parser->parse('pedido/gerencia_ajusteitem_view', $arrDados);
		}
		else
		{
			echo 'Operação não permitida.';
		}
	}
	
	function gerencia_saveajusteped($idItem = 0, $qtd = 0)
	{
		if ($this->session->userdata('tipoUsuario') == 2) //se for biblioteca
		{
			$this->load->model('pedido_model');
			$this->load->model('global_model');
			
			$idPedido = $this->pedido_model->getIdPedido($this->session->userdata('idUsuario'));
			$dadosPed = $this->pedido_model->getDadosPedido($this->session->userdata('programa'), $this->session->userdata('idUsuario'), $idPedido);
			
			if (count($dadosPed) > 0)
			{
				if ($dadosPed[0]['IDPEDIDOSTATUS'] == 4)
				{
					if($this->pedido_model->itemExists($idItem, $idPedido))
					{
						if (is_numeric($qtd) && $qtd >= 0)
						{
							$where    = sprintf("IDPEDIDO = %s AND IDLIVRO = %s", $idPedido, $idItem);
							$qtdAtual = $this->global_model->get_dado('pedidolivro', 'QTD', $where);
							
							if ($qtd <= $qtdAtual)
							{
								$this->db->trans_start();
								
								$arrUpd = Array();
								$arrUpd['QTD_ENTREGUE'] = $qtd;
								
								$where = sprintf("IDPEDIDO = %s AND IDLIVRO = %s", $idPedido, $idItem);
								$this->global_model->update('pedidolivro', $arrUpd, $where);
								
								$this->db->trans_complete();
								
								if ($this->db->trans_status() === FALSE)
								{
									$this->load->library('logsis', Array('UPDATE', 'ERRO', 'pedidolivro', $this->session->userdata('idUsuario'), 'Ajuste de quantidade recebida pela biblioteca', var_export($arrUpd, TRUE)));
									
									echo '.ERRO.||Ocorreu um erro ao salvar a alteração. Tente novamente.';
								}
								else
								{
									$this->load->library('logsis', Array('UPDATE', 'OK', 'pedidolivro', $this->session->userdata('idUsuario'), 'Ajuste de quantidade recebida pela biblioteca', var_export($arrUpd, TRUE)));
									
									echo 'OK';
								}
							}
							else
							{
								echo '.ERRO.||Não é possível aumentar a quantidade recebida além do pedido original.';
							}
						}
						else
						{
							echo '.ERRO.||Quantidade inválida.';
						}
					}
					else
					{
						echo '.ERRO.||Livro não consta no pedido.';
					}
				}
				else
				{
					echo '.ERRO.||Não é possível ajustar quantidades recebidas nesse momento.';
				}
			}
			else
			{
				echo '.ERRO.||Pedido não encontrado.';
			}
		}
		else
		{
			echo '.ERRO.||Acesso não permitido.';
		}
	}
	
	function gerencia_validacomite($idBiblioteca = 0)
	{
		// Valida para caso o usuário seja diferente de responsável financeiro
		if ($this->session->userdata('descTipoUsuario') != 'FINANCEIRO - BIBLIOTECA')
		{
			echo '.ERRO.||Apenas o responsável financeiro tem permissão para finalizar o pedido.';
			exit;
		} else {
			$this->_modalVerificaComite($idBiblioteca);
		}
	}

	function gerencia_verificasenhascomite()
	{
		$idbiblioteca	= $this->input->post('idbiblioteca');
		$idusuario		= $this->input->post('idusuario');
		$senha			= $this->input->post('senha');

		if (!is_array($idusuario))	$idusuario = explode(',', $idusuario);
		if (!is_array($senha)) 		$senha = explode(',', $senha);
		
		$this->load->model('biblioteca_model');
		
		$ok = true;
		
		for($i = 0; $i < count($idusuario); $i++) {
			$c = $this->biblioteca_model->getDadosMembroComite($idbiblioteca, $idusuario[$i]);
			
			if ($c[0]['SENHA'] != md5($senha[$i])) {
				echo "Senha para o usuário " . $c[0]['LOGIN'] . " incorreta.\n";
				$ok = false;
			}
		}

	    if ($ok) {
        	$this->load->library('logsis', Array('INSERT', 'OK', '', $this->session->userdata('idUsuario'), 'Validado pelo comitê de acervo', var_export($_POST, TRUE)));
        	echo "OK";
		}
		
	}
	
	/**
	* set_pedido_recebido_pdv()
	* Seta o status do pedido para o recebido pelo pdv, utilizado na conferencia de pedidos.
	* @return void
	*/
	function set_pedido_recebido_pdv()
	{
		$this->load->model('global_model');
		$this->global_model->update('pedido', array('IDPEDIDOSTATUS' => $this->input->post('idpedidostatus')), "IDPEDIDO = " . $this->input->post('idpedido'));
		redirect('pedido/conferencia/' . $this->input->post('idpedido'));
	}
	
	/**
	* conferencia()
	* Tela de conferência do pedido. Verifica se o tipo de usuário é biblioteca ou
	* PDV, e redireciona para a view de cada um.
	* @param integer idpedido
	* @return void
	**/
	function conferencia($idpedido = 0)
	{
		$this->load->model('configuracao_model');
		if($this->configuracao_model->validaConfig(22, date('Y-m-d')))
		{
			// Somente ADM, biblioteca ou pdv podem entrar nesta tela
			if(($this->session->userdata('tipoUsuario') == 1 || $this->session->userdata('tipoUsuario') == 2 || $this->session->userdata('tipoUsuario') ==  4) && ($idpedido != 0 && $idpedido != ''))
			{
				// Carrega classes necessárias
				$this->load->model('pedido_model');
				
				// Seta o tipo usuario (admin assume como biblioteca)
				$tipo_usuario = ($this->session->userdata('tipoUsuario') == 2 || $this->session->userdata('tipoUsuario') == 1) ? 2 : 4;
				
				// Seta o tipo de view a ser carregada
				$view_page = ($tipo_usuario == 2) ? 'conferencia_biblioteca' : 'conferencia_pdv';
				
				// Coleta dados do pedido
				$pedido = $this->pedido_model->get_dados_pedido($idpedido);
				$arr_args['pedido'] = $pedido;
				$arr_args['pedido_itens_count'] = $this->pedido_model->get_count_itens($idpedido);
				$arr_args['pedido_count_titulos'] = $this->pedido_model->get_count_titulos($idpedido);
				$arr_args['tipo_usuario'] = $tipo_usuario;
				$arr_args['pedido_model'] = $this->pedido_model;
				
				$vlr_pedido = (get_value($pedido, 'TIPO_ATENDIMENTO') == 1) ? $this->pedido_model->get_sum_itens_obrigatorios($idpedido) : get_value($pedido, 'VALORTOTAL');
				$arr_args['vlr_total'] = $vlr_pedido * get_value($pedido, 'PERCENTUAL_ATENDIMENTO');
				
				// Verifica a quantidade de entregas
				// $is_primeira_entrega = $this->pedido_model->is_primeira_entrega_biblioteca($idpedido);
				// $arr_args['numero_entrega'] = ($is_primeira_entrega) ? 1 : 2;
				$arr_args['entrega_atual'] = ($this->pedido_model->get_quantidade_entregas($idpedido, $tipo_usuario) + 1);
				
				// Itens não conferidos (count)
				$arr_args['itens_nao_conferidos_count'] = $this->pedido_model->get_count_itens_nao_conferidos($idpedido);
				
				$this->load->view('pedido/' . $view_page, $arr_args);
			} else {
				redirect('/home/');
			}
		} else {
			$buttons = ($this->session->userdata('tipoUsuario') == 4) ? '<div style="margin-top:20px"><hr /><div class="inline top"><button onclick="window.location.href=\'' . URL_EXEC . 'pdv/pedidoslivro\'">OK</button></div><div class="inline middle" style="padding:7px 0 0 5px">ou <a href="' . URL_EXEC . 'pdv/pedidoslivro">voltar</a></div></div>' : '<div style="margin-top:20px"><hr /><div class="inline top"><button onclick="window.location.href=\'' . URL_EXEC . 'pedido/lista\'">OK</button></div><div class="inline middle" style="padding:7px 0 0 5px">ou <a href="' . URL_EXEC . 'pedido/lista">voltar</a></div></div>';
			$this->load->view('general_message', array('msg' => get_mensagem('warning', '', 'Não é possível realizar a conferência de pedidos pois a data atual está fora do período para esta ação.') . $buttons));
		}
    }
	
	/**
	* load_documentos_pedido()
	* Carrega bloco html que contem os documentos de um pedido de id encaminhado como param.
	* O segundo parametro define se é para carregar comprovantes de entrega ou notas fiscais.
	* @param integer idpedido
	* @param string mode
	* @return void
	*/
	function load_documentos_pedido($idpedido = 0, $mode = '')
	{
		$this->load->model('pedido_model');
		$arr_args['tipo_usuario'] = $this->session->userdata('tipoUsuario');
		$arr_args['idpedido'] = $idpedido;
		$arr_args['mode'] = $mode;
		$arr_args['pedido'] = $this->pedido_model->get_dados_pedido($idpedido);
		$arr_args['pedido_documentos'] = $this->pedido_model->get_documentos_pedido($idpedido, $mode);
		$this->parser->parse('pedido/load_documentos_pedido', $arr_args);
	}
	
	/**
	* modal_inserir_documento()
	* Exibe html da tela de documentos para um determinado pedido encaminhado
	* como parâmetro.
	* @param integer idpedido
	* @return void
	*/
	function modal_inserir_documento($idpedido = 0)
	{
		// Carrega classes necessárias para funcionamento
		$this->load->model('pedido_documento_model');
		$this->load->model('pedido_model');
		
		// Dados do pedido e usuário atual
		$args['pedido'] = $this->pedido_model->get_dados_pedido($idpedido);
		$args['idpedido'] = $idpedido;
		$args['idusuariotipo'] = $this->session->userdata('tipoUsuario');
		$args['entregas_finalizadas'] = $this->pedido_model->get_max_numero_entrega_doc($idpedido);
		
		// DEBUG
		// echo $args['entregas_finalizadas'];
		
		// Parse do html da modal
		$this->parser->parse('pedido/modal_inserir_documento', $args);
	}
	
	/**
	* modal_inserir_documento_proccess()
	* Processa o envio de formulário feito pela dialog ajax de inserção de documentos,
	* na conferencia do pedido.
	* @return void
	**/
	function modal_inserir_documento_proccess()
	{
		// Seta o tempo limite para upload de imagens
		set_time_limit(0);
		
		// Somente ADM, biblioteca ou pdv podem entrar nesta tela
        if($_FILES['arquivo']['name']) {
			
			// Tenta fazer o upload do arquivo (5MB)
			list($file, $error) = upload_file('arquivo', 'application/uploads/', 'jpg,jpeg,gif,png,bmp,pdf', 656000);

			// Verifica se já existe um arquivo com o mesmo numero de documento, para este pedido
			if(!$error)
			{  
				$this->load->model('pedido_documento_model');
				$id_documento = $this->pedido_documento_model->get_documento_by_numero($this->input->post('numero_doc'), $this->input->post('idpedido'), $this->input->post('id_pedido_documento_tipo'));
				$error = ($id_documento != 0) ? 'Já existe um documento deste tipo cadastrado com este número. Verifique o número inserido e tente novamente.' : '';
			}
			
			// Argumentos p/ view
			$arr_args['idpedido'] = $this->input->post('idpedido');
			$arr_args['error'] = '';
			$arr_args['option'] = '';
			
			// Se nao tiver erro, entao faz insert no banco de dados	
			if($error)
			{
				$arr_args['error'] = $error;
				$this->load->library('logsis', Array('INSERT', 'ERRO', 'pedido_documento', $this->input->post('idpedido'), 'Erro ao inserir documento em pedido', $error));
			} else {
				// Carrega model global, para insert
				$this->load->library('logsis');
				$this->load->model('global_model');
				$this->load->model('pedido_model');
				$this->load->model('pedido_documento_model');
				
				// Monta array de inserção
				$args['id_pedido_documento_tipo'] = $this->input->post('id_pedido_documento_tipo');
				$args['idpedido'] = $this->input->post('idpedido');
				$args['idusuariotipo'] = $this->input->post('idusuariotipo');
				$args['arquivo'] = $file;
				$args['arquivo_original'] = $_FILES['arquivo']['name'];
				$args['numero_doc'] = trim($this->input->post('numero_doc'), ' ');
				$args['qtde_itens'] = $this->input->post('qtde_itens');
				$args['valor_doc'] = to_float($this->input->post('valor_doc'));
				$args['numero_entrega'] = $this->input->post('numero_entrega');
				
				// Insert
				$this->global_model->insert('pedido_documento', $args);
				$this->logsis->insereLog(array('INSERT', 'OK', 'pedido_documento', $this->input->post('idpedido'), 'Inserção de documento em pedido', var_export($args, true)));
				
				// Insere observação do pedido (já lida)
				$entidade = ($this->session->userdata('tipoUsuario') == 2) ? 'BIBLIOTECA' : 'PDV';
				$args_obs['idpedido'] = $this->input->post('idpedido');
				$args_obs['idusuario'] = $this->session->userdata('idUsuario');
				$args_obs['categoria'] = 'ACOES_' . $entidade;
				$args_obs['observacao'] = 'Inserção de documento (Número DOC. ' . $args['numero_doc'] . ') feita por ' . $entidade . '.';
				$args_obs['lido'] = 'S';
				$this->global_model->insert('pedido_observacao', $args_obs);
				$this->logsis->insereLog(array('INSERT', 'OK', 'pedido_observacao', $this->input->post('idpedido'), 'Observação de pedido inserida', var_export($args_obs, true)));
				
				// Caso o número da entrega seja maior que zero, então é um documento que está sendo
				// inserido através da liberação de documentos feita pelo administrador (neste caso deve-se recalcular)
				// O valor da entrega para liberação de crédito.
				if($args['numero_entrega'] > 0 && $this->input->post('idusuariotipo') == 2)
				{	
					$vlr_entrega_original = $this->pedido_model->get_vlr_total_doc_entrega($args['idpedido'], $args['numero_entrega']);
					$vlr_entrega = $vlr_entrega_original + $args['valor_doc'];
					$this->global_model->update('pedido_valor_entrega', array('valor' => $vlr_entrega), 'IDPEDIDO = ' . $args['idpedido'] . ' AND NUMERO_ENTREGA = ' . $args['numero_entrega']);
					$this->logsis->insereLog(array('UPDATE', 'OK', 'pedido_valor_entrega', $this->input->post('idpedido'), 'Valor de entrega alterado', $vlr_entrega_original . ' para ' . $vlr_entrega));
		
					// Insere observação do pedido (não lida, informando que valor da entrega foi alterado)
					$args_obs['idpedido'] = $this->input->post('idpedido');
					$args_obs['idusuario'] = $this->session->userdata('idUsuario');
					$args_obs['categoria'] = 'CONFERENCIA_ADMINISTRATIVA';
					$args_obs['observacao'] = 'Valor da ' . $args['numero_entrega'] . 'ª entrega alterado automaticamente de ' . $vlr_entrega_original . ' para ' . $vlr_entrega . ' em função de recalculo causado por inserção manual de documento.';
					$args_obs['lido'] = 'N';
					$this->global_model->insert('pedido_observacao', $args_obs);
					$this->logsis->insereLog(array('INSERT', 'OK', 'pedido_observacao', $this->input->post('idpedido'), 'Observação de pedido inserida', var_export($args_obs, true)));
				}
				
				// Coleta o ultimo ID inserido para coletar os dados
				// e envia para model process que popula os combos dos pedidos
				$arr_args['option'] = $this->pedido_documento_model->get_dados_documento($this->global_model->get_insert_id());
			}
			
			// Parse na tela que fecha a dialog, e chama o load da tabela de documentos
			$this->load->view('pedido/modal_inserir_documento_proccess', $arr_args);
		}
    }
	
	/**
	* modal_form_update_documento()
	* Exibe formulario de edicao de documento.
	* @param integer id_pedido_Documento
	* @return void
	*/
	function modal_form_update_documento($id_pedido_documento = 0)
	{
		// Coleta options do de tipos de documento
		$this->load->model('pedido_documento_model');
		$this->load->model('pedido_model');
		$args['options_tp_docs'] = monta_options_array($this->pedido_documento_model->get_tipos_documentos());
		$args['idusuariotipo'] = $this->session->userdata('tipoUsuario');
		$args['documento'] = $this->pedido_documento_model->get_dados_documento($id_pedido_documento);
		$args['id_pedido_documento'] = $id_pedido_documento;
		$args['pedido'] = $this->pedido_model->get_dados_pedido(get_value($args['documento'], 'IDPEDIDO'));
		
		// Parse do html da modal
		$this->parser->parse('pedido/modal_form_update_documento', $args);
	}
	
	/**
	* modal_form_update_documento_proccess()
	* Processa o envio de formulário feito pela dialog ajax de edição de documentos,
	* na conferencia do pedido.
	* @return void
	**/
	function modal_form_update_documento_proccess()
	{
		// Seta o tempo limite para upload de imagens
		set_time_limit(0);
		
		// Carrega model global, para update
		$this->load->library('logsis');
		$this->load->model('global_model');
		$this->load->model('pedido_documento_model');
		
		// Argumentos p/ view
		$arr_args['idpedido'] = $this->input->post('idpedido');
		$arr_args['id_pedido_documento'] = $this->input->post('id_pedido_documento');
		$arr_args['error'] = '';
		$arr_args['option'] = '';
		
		// Se arquivo foi encaminhado na edição, então tenta alterá-lo
        if($_FILES['arquivo']['name']) 
		{
			// Coleta o arquivo para Remove-lo posteriormente
			$arquivo = $this->pedido_documento_model->get_dados_documento($this->input->post('id_pedido_documento'));
			
			// Tenta fazer o upload do arquivo (5MB)
			list($file, $error) = upload_file('arquivo', 'application/uploads/', 'jpg,jpeg,gif,png,bmp,pdf', 656000);

			// Se nao tiver erro, entao faz insert no banco de dados	
			if($error)
			{
				$arr_args['error'] = $error;
				$this->load->library('logsis', Array('UPDATE', 'ERRO', 'pedido_documento', $this->input->post('idpedido'), 'Erro ao atualizar documento em pedido', $error));
			} else {
				// Seta o nome do arquivo para que seja feito update
				$args['arquivo'] = $file;
				$args['arquivo_original'] = $_FILES['arquivo']['name'];
				
				// Remove arquivo fisicamente da app
				unlink('application/uploads/' . get_value($arquivo, 'ARQUIVO'));
			}
		}
		
		// Podem ocorrer erros no upload do arquivo, então só faz update após nao terem erros
		if($arr_args['error'] == '')
		{
			// Monta array de edição
			// $args['numero_entrega'] = to_float($this->input->post('numero_entrega'));
			$args['numero_doc'] = trim($this->input->post('numero_doc'), ' ');
			$args['qtde_itens'] = $this->input->post('qtde_itens');
			$args['valor_doc'] = to_float($this->input->post('valor_doc'));
			
			// Update
			$this->global_model->update('pedido_documento', $args, "ID_PEDIDO_DOCUMENTO = " . $this->input->post('id_pedido_documento'));
			$this->logsis->insereLog(array('UPDATE', 'OK', 'pedido_documento', $this->input->post('idpedido'), 'Edição de documento em pedido ', var_export($args, true)));
			
			// Insere observação do pedido (não lida caso liberar edição = 'S')
			// Coleta dados do documento
			$documento = $this->pedido_documento_model->get_dados_documento($this->input->post('id_pedido_documento'));
			$entidade = ($this->session->userdata('tipoUsuario') == 2) ? 'BIBLIOTECA' : 'PDV';
			$args_obs['idpedido'] = $this->input->post('idpedido');
			$args_obs['idusuario'] = $this->session->userdata('idUsuario');
			$args_obs['categoria'] = 'ACOES_' . $entidade;
			$args_obs['observacao'] = 'Edição de documento (ID Documento ' . $this->input->post('id_pedido_documento') . ') feita por ' . $entidade . '.';
			$args_obs['lido'] = (get_value($documento, 'LIBERAR_EDICAO') == 'S') ? 'N' : 'S';
			$this->global_model->insert('pedido_observacao', $args_obs);
			$this->logsis->insereLog(array('INSERT', 'OK', 'pedido_observacao', $this->input->post('idpedido'), 'Observação de pedido inserida', var_export($args_obs, true)));
			
			// Coleta o ultimo ID inserido para coletar os dados
			// e envia para model process que popula os combos dos pedidos
			$arr_args['option'] = $this->pedido_documento_model->get_dados_documento($this->global_model->get_insert_id());
		}
	
		// Parse na tela que fecha a dialog, e chama o load da tabela de documentos
		$this->load->view('pedido/modal_form_update_documento_proccess', $arr_args);
    }
	
	/**
	* modal_deletar_documento()
	* Exibe html da tela de confirmação de deleção de documento determinado documento encaminhado como parâmetro.
	* @param integer iddocumento
	* @return void
	*/
	function modal_deletar_documento($iddocumento = 0, $idpedido = 0)
	{
		$this->parser->parse('pedido/modal_deletar_documento', array('iddocumento' => $iddocumento, 'idpedido' => $idpedido));
	}
	
	/**
	* modal_deletar_documento_proccess()
	* Processa o envio de formulário feito pela dialog ajax de deleção de documentos,
	* na conferencia do pedido. Além disso, desmarca nos itens do pedido todos os registros
	* que tenham sido conferidos com o id deste documento que está sendo deletado.
	* @return void
	**/
	function modal_deletar_documento_proccess()
	{
		$this->load->model('global_model');
		$this->load->model('pedido_model');
		$this->load->model('pedido_documento_model');
		
		// Coleta dados do documento para remove o arquivo fisicamente do banco
		$arquivo = $this->pedido_documento_model->get_dados_documento($this->input->post('id_pedido_documento'));
		
		// Update na tabela de pedidolivro, tirando todos os itens marcados neste documento
		$this->global_model->update('pedidolivro', array('id_pedido_documento' => NULL), 'id_pedido_documento = ' . $this->input->post('id_pedido_documento'));
		$this->load->library('logsis', Array('UPDATE', 'OK', 'pedidolivro', $this->input->post('id_pedido'), 'Edição de item de pedido ', 'Desmarcado todos os itens marcados para o documento ' . $this->input->post('id_pedido_documento')));
		
		// Deleção
		$this->global_model->delete('pedido_documento', array('id_pedido_documento' => $this->input->post('id_pedido_documento')));
		$this->load->library('logsis', Array('DELETE', 'OK', 'pedido_documento', $this->input->post('id_pedido'), 'Remoção de documento em pedido ', var_export($arquivo, false)));
		
		// Update na tabela de itens, desmarcando todos os itens que 
		// foram até então conferidos para esta nota / documento
		$this->global_model->update('pedido_documento', array('id_pedido_documento' => 'NULL'), array('id_pedido_documento' => $this->input->post('id_pedido_documento')));
		
		// Insere observação do pedido (não lida caso liberar edição = 'S')
		$entidade = ($this->session->userdata('tipoUsuario') == 2) ? 'BIBLIOTECA' : 'PDV';
		$args_obs['idpedido'] = $this->input->post('idpedido');
		$args_obs['idusuario'] = $this->session->userdata('idUsuario');
		$args_obs['categoria'] = 'ACOES_' . $entidade;
		$args_obs['observacao'] = 'Deleção de documento (ID Documento ' . $this->input->post('id_pedido_documento') . ') feita por ' . $entidade . '.';
		$args_obs['lido'] = (get_value($arquivo, 'LIBERAR_EDICAO') == 'S') ? 'N' : 'S';
		$this->global_model->insert('pedido_observacao', $args_obs);
		$this->logsis->insereLog(array('INSERT', 'OK', 'pedido_observacao', $this->input->post('idpedido'), 'Observação de pedido inserida', var_export($args_obs, true)));
		
		// Remove arquivo fisicamente da app
		unlink('application/uploads/' . get_value($arquivo, 'ARQUIVO'));
		
		// Parse na tela que fecha a dialog, e chama o load da tabela de documentos
		$this->load->view('pedido/modal_deletar_documento_proccess', array('idpedido' => $this->input->post('id_pedido'), 'id_pedido_documento' => $this->input->post('id_pedido_documento')));
    }
	
	/**
	* save_conferencia()
	* Processa o envio de formulário dos itens da conferencia do pedido.
	* @param integer idpedido
	* @return void
	**/
	function save_conferencia($idpedido = 0)
	{
		// Seta o tempo limite e extensao de limite de post
		set_time_limit(0);
		if(extension_loaded('suhosin')){ /*echo('suhosin habilitado.');*/ ini_set('suhosin.post.max_array_index_length', '10000'); ini_set('suhosin.post.max_vars', 10000); }
		
		// Somente ADM ou biblioteca podem passar por aqui
		if($this->session->userdata('tipoUsuario') == 2 || $this->session->userdata('tipoUsuario') == 1)
		{
			// Carrega controllers
			$this->load->model('pedido_model');
			$this->load->model('global_model');
			
			// Coleta dados do pedido
			$pedido = $this->pedido_model->get_dados_pedido($idpedido);
			
			// Array post
			$i = 0;
			$post = $this->input->post();
			foreach($post as $index => $value)
			{
				// echo($i . ' - [' . $index . '] => ' . $value . '<br />');
				if(stristr($index, 'qtd_entregue_'))
				{
					// Coleta o idlivro
					$idlivro = str_replace('qtd_entregue_', '', $index);
					
					// Coleta quantidade entregue e nota fiscal
					$qtde_solicitado = $this->input->post('qtd_original_entregue_'.$idlivro);
					$qtde_entregue   = $this->input->post('qtd_entregue_'.$idlivro);
					$nota_fiscal     = $this->input->post('nota_'.$idlivro);
					$nota_original   = $this->input->post('nota_original_'.$idlivro);
					
					// Faz update do livro atual
					if(($qtde_entregue != $qtde_solicitado) || ($nota_fiscal != $nota_original))
					{
						$this->pedido_model->update_item_conferencia($idpedido, $idlivro, $qtde_entregue, $nota_fiscal);
						$this->load->library('logsis', Array('UPDATE', 'OK', 'pedidolivro', $idpedido, 'Atualização de item para conferência', 'idlivro => ' . $idlivro . ', qtde_entregue => ' . $qtde_entregue . ', nota_fiscal => ' . $nota_fiscal));
						
						/*
						echo($index . ' => ' . $value . '<br />');
						echo('idlivro => ' . $idlivro . '<br />');
						echo('nota => ' .$this->input->post('nota_'.$idlivro) . '<br />');
						echo('nota_original => ' .$this->input->post('nota_original'.$idlivro) . '<br />');
						echo('qtd_entregue => ' .$this->input->post('qtd_entregue_'.$idlivro) . '<br />');
						echo('qtd_original_entregue => ' .$this->input->post('qtd_original_entregue_'.$idlivro) . '<br />');
						echo('------------------------------------<br /><br />');
						*/
					}
				}
				$i++;
			}
			
			// die();
			// DEBUG
			// echo("did it $i times");
			// die();
			
			// Atualiza pedido para o valor anterior (para nao disparar trigger no banco)
			$this->global_model->update('pedido', array('VALORTOTAL' => get_value($pedido, 'VALORTOTAL')), "IDPEDIDO = $idpedido");
			$this->load->library('logsis', Array('UPDATE', 'OK', 'pedidolivro', $idpedido, 'Atualização de valor total para pedido', 'valor_total => ' . get_value($pedido, 'VALORTOTAL')));
			
			// redirect para pagina de conferencia
			redirect('/pedido/conferencia/' . $idpedido);
		}
    }
	
	/**
	* load_table_itens_pedido()
	* Carrega bloco html que contem os itens de um pedido de id encaminhado como param, para conferencia.
	* @param integer idpedido
	* @return void
	*/
	function load_table_itens_pedido($idpedido = 0, $title = '', $is_pdv = 0)
	{
		// Carrega Models
		$this->load->model('livro_model');
		$this->load->model('pedido_model');
		$this->load->model('pedido_documento_model');
		
		// Carrega dados para o view
		$arr_args['pedido'] = $this->pedido_model->get_dados_pedido($idpedido);
		$arr_args['idpedido'] = $idpedido;
		$arr_args['pedido_documento_model'] = $this->pedido_documento_model;
		$arr_args['livro_model'] = $this->livro_model;
		
		// Coleta a soma dos itens para o subtotal
		// $arr_args['subtotal_itens'] = $this->pedido_model->sum_vlr_itens($idpedido);
		$arr_args['subtotal_itens_conferidos'] = $this->pedido_model->sum_vlr_itens_conferidos($idpedido);
		
		// Verifica que entrega que está sendo realizada
		$arr_args['entrega_atual'] = ($this->pedido_model->get_quantidade_entregas($idpedido, $this->session->userdata('tipoUsuario')) + 1);
		
		// Carrega os dados dos itens conforme o titulo, ou nada caso title == ''
		$arr_args['pedido_itens'] = ($title != '') ? $this->pedido_model->get_itens_pedido($idpedido, urldecode($title)) : array();
		$arr_args['options_doc'] = $this->pedido_model->get_documentos_pedido_options($idpedido, 1);
		$arr_args['tipo_usuario'] = $this->session->userdata('tipoUsuario');
		
		// Verifica se a tabela carregada é para pdv (view é diferente)
		$url = ($is_pdv != 0) ? 'load_table_itens_pedido_pdv' : 'load_table_itens_pedido';
		$this->load->view('pedido/' . $url, $arr_args);
	}
	
	/**
	* modal_titulos_nao_conferidos()
	* Exibe html da tabela de titulos nao conferidos.
	* @param integer idpedido
	* @return void
	*/
	function modal_titulos_nao_conferidos($idpedido = 0)
	{
		// Carrega controllers e models necessários
        $this->load->library('array_table');
        $this->load->model('pedido_model');

        // Seta configs da tabela
        $this->array_table->set_id('module_table');
        $this->array_table->set_data($this->pedido_model->get_itens_nao_conferidos($idpedido));
        $this->array_table->set_columns(array(1, 14));

        // Coleta dados da tabela de arquivos
        $args['table_generic'] = $this->array_table->get_html();
		
		$this->load->view('pedido/modal_titulos_nao_conferidos', $args);
	}
	
	/**
	* finaliza_conferencia()
	* Processa a finalização da conferência. Verifica o tipo de usuário que está fazendo
	* a finalização, e dependendo do tipo, faz finalização de pdv ou biblioteca. O resultado
	* é impresso em dialog modal.
	* @param integer idpedido
	* @param integer tipo_usuario
	* @return void
	**/
	function finaliza_conferencia($idpedido = 0, $tipo_usuario = 0)
	{
		// Carrega classes necessárias para a utilização
		$this->load->library('logsis');
		$this->load->model('global_model');
		$this->load->model('pedido_model');
		$this->load->model('pedido_documento_model');
		
		// Seta msg de retorno
		$args['msg'] = '';
		$args['error'] = false;
		$args['id_nota_erro'] = array();
		
		// Carrega dados do pedido
		$pedido = $this->pedido_model->get_dados_pedido($idpedido);
		
		// Finalização para PDV
		if($tipo_usuario == 4)
		{
			// Verifica que entrega que está sendo realizada
			$args['entrega_atual'] = ($this->pedido_model->get_quantidade_entregas($idpedido, $tipo_usuario) + 1);
			
			// Coleta os documentos da entrega atual
			$docs_entrega = $this->global_model->selectx('pedido_documento', "IDPEDIDO = $idpedido AND ID_PEDIDO_DOCUMENTO_TIPO = 2 AND NUMERO_ENTREGA = 0", '1');
	
			// Entrega deve ter documentos
			if(count($docs_entrega) == 0)
			{
				$args['msg'] = get_mensagem('warning', '', 'Não existem comprovantes de entrega cadastrados para a entrega que está sendo salva. Verifique-os e tente novamente.');
				$args['error'] = true;
			} elseif($args['entrega_atual'] == 1) {
				// Soma valor dos documentos (comprovantes) da entrega
				$vlr_entrega = 0;
				foreach($docs_entrega as $doc){ $vlr_entrega += get_value($doc, 'VALOR_DOC'); }
				
				// Caso o tipo de atendimento seja 1 (valor total da soma dos titulos 
				// obrigatórios, isto é, exluindo os inexigíveis) entao faz o calculo em cima somente destes itens
				// Verifica se a soma bate o percentual estipulado (default 80%) do pedido
				$vlr_pedido = (get_value($pedido, 'TIPO_ATENDIMENTO') == 1) ? $this->pedido_model->get_sum_itens_obrigatorios($idpedido) : get_value($pedido, 'VALORTOTAL');
				$vlr_perc_pedido = $vlr_pedido * get_value($pedido, 'PERCENTUAL_ATENDIMENTO');
				if($vlr_entrega <= $vlr_perc_pedido)
				{
					$args['msg'] = get_mensagem('warning', '', 'A soma do valor dos comprovantes de entrega cadastrados para a primeira entrega não corresponde ao percentual mínimo de atendimento do pedido (percentual: ' . (get_value($pedido, 'PERCENTUAL_ATENDIMENTO') * 100) . '%).');
					$args['error'] = true;
				}
			}
			
			// Verifica se n existem erros/avisos (USADO PARA CONFERENCIA PDV)
			if(!$args['error'])
			{
				// Marca comprovantes com a entrega atual
				$this->global_model->update('pedido_documento', array('NUMERO_ENTREGA' => $args['entrega_atual']), "IDPEDIDO = $idpedido AND ID_PEDIDO_DOCUMENTO_TIPO = 2 AND NUMERO_ENTREGA = 0");
			
				// Mensagem de sucesso
				$args['msg'] = get_mensagem('success', 'Sucesso', $args['entrega_atual'] . 'ª entrega finalizada com sucesso. O pagamento do pedido será liberado somente após as duas partes (biblioteca e pdv) realizarem suas respectivas atividades relacionadas à entrega.');
			}
			
			// Carrega tela de informações da finalização da conferência
			$this->load->view('pedido/finaliza_conferencia_pdv', $args);
		
		
		// FINALIZAÇÃO BIBLIOTECA
		} elseif($tipo_usuario == 2 || $tipo_usuario == 1) {
			// Inicializa o valor da entrega
			$vlr_entrega = 0;
			
			// Verifica que entrega que está sendo realizada
			$args['entrega_atual'] = ($this->pedido_model->get_quantidade_entregas($idpedido, $tipo_usuario) + 1);
			
			// Comprovantes correspondentes de entrega do PDV
			$args['comprovantes'] = $this->global_model->selectx('pedido_documento', "idpedido = $idpedido AND id_pedido_documento_tipo = 2 AND numero_entrega = " . $args['entrega_atual'], "NUMERO_DOC");
			
			// Coleta a soma dos docs da entrega, no caso de Primeira entrega
			if($args['entrega_atual'] == 1)
			{
				if(count($args['comprovantes']) == 0)
				{
					$args['msg'] = '&bull;&nbsp;O PDV parceiro ainda não cadastrou comprovantes de entrega para a primeira entrega.<br />';
				} else {
					// Calcula o valor da entrega
					$vlr_entrega = 0;
					foreach($args['comprovantes'] as $doc){ $vlr_entrega += get_value($doc, 'VALOR_DOC'); }
				
					// Caso o tipo de atendimento seja 1 (valor total da soma dos titulos 
					// obrigatórios, isto é, exluindo os inexigíveis) entao faz o calculo em cima somente destes itens
					// Verifica se a soma bate o percentual estipulado (default 80%) do pedido
					$vlr_pedido = (get_value($pedido, 'TIPO_ATENDIMENTO') == 1) ? $this->pedido_model->get_sum_itens_obrigatorios($idpedido) : get_value($pedido, 'VALORTOTAL');
					$vlr_perc_pedido = $vlr_pedido * get_value($pedido, 'PERCENTUAL_ATENDIMENTO');
					if($vlr_entrega <= $vlr_perc_pedido)
					{
						$args['msg'] .= '&bull;&nbsp;A soma do valor dos comprovantes de entrega cadastrados para a primeira entrega não corresponde ao percentual mínimo de atendimento do pedido (percentual: ' . (get_value($pedido, 'PERCENTUAL_ATENDIMENTO') * 100) . '%).<br />';
					}
				}
			}
			
			// Monta a tabela que mostra os itens da 'entrega atual'
			$args['notas_fiscais'] = $this->global_model->selectx('pedido_documento', "idpedido = $idpedido AND id_pedido_documento_tipo = 1 AND numero_entrega = 0", "NUMERO_DOC");
			
			// Verifica se é primeira entrega e numero_notas == 0
			if(count($args['notas_fiscais']) > 0)
			{
				$vlr_entrega_notas = 0;
				
				// Verifica, por nota, se nota fiscal possui valor diferente do valor da soma dos itens
				// Tambem verifica, se para cada nota existe um comprovante de mesmo numero cadastrado
				foreach($args['notas_fiscais'] as $notas)
				{
					// Soma o valor da nota ao valor total pois o valor da soma das notas
					// tem de ser também maior que o percentual de entrega
					$vlr_entrega_notas += get_value($notas, 'VALOR_DOC');
					
					// Compara se a soma dos itens daquele documento é diferente do valor informado pelo usuário na nota
					if($this->pedido_documento_model->get_sum_vlr_itens_documento(get_value($notas, 'ID_PEDIDO_DOCUMENTO')) != get_value($notas, 'VALOR_DOC'))
					{
						$args['msg'] .= "&bull;&nbsp;A nota fiscal de <strong>número " . get_value($notas, 'NUMERO_DOC') . "</strong> possui um valor diferente do valor do total dos itens desta mesma nota.<br />";
						$args['id_nota_erro'][] = get_value($notas, 'ID_PEDIDO_DOCUMENTO');
						$args['error'] = true;
					}
					
					// Verifica se existe comprovante equivalente
					$id_comprovante = $this->pedido_documento_model->existe_doc_equivalente(get_value($notas, 'NUMERO_DOC'), 2, $args['entrega_atual'], $idpedido);
					if($id_comprovante == 0)
					{
						$args['msg'] .= "&bull;&nbsp;A nota fiscal de <strong>número " . get_value($notas, 'NUMERO_DOC') . "</strong> não possui um comprovante de entrega com o mesmo número de documento.<br />";
						$args['id_nota_erro'][] = get_value($notas, 'ID_PEDIDO_DOCUMENTO');
						$args['error'] = true;
					} else {
						// Caso exista comprovante equivalente, testa o valor da 
						// nota com o do comprovante cadastrado, que deve ser o mesmo
						$comprovante = $this->pedido_documento_model->get_dados_documento($id_comprovante);
						if(get_value($comprovante, 'VALOR_DOC') != get_value($notas, 'VALOR_DOC'))
						{
							$args['msg'] .= "&bull;&nbsp;A nota fiscal de <strong>número " . get_value($notas, 'NUMERO_DOC') . "</strong> possui um valor diferente do comprovante de entrega de mesmo número.<br />";
							$args['id_nota_erro'][] = get_value($notas, 'ID_PEDIDO_DOCUMENTO');
							$args['error'] = true;
						}
					}
				}
				
				// Caso o tipo de atendimento seja 1 (valor total da soma dos titulos 
				// obrigatórios, isto é, exluindo os inexigíveis) entao faz o calculo em cima somente destes itens
				// Verifica se a soma bate o percentual estipulado (default 80%) do pedido
				$vlr_pedido = (get_value($pedido, 'TIPO_ATENDIMENTO') == 1) ? $this->pedido_model->get_sum_itens_obrigatorios($idpedido) : get_value($pedido, 'VALORTOTAL');
				$vlr_perc_pedido = $vlr_pedido * get_value($pedido, 'PERCENTUAL_ATENDIMENTO');
				if(round($vlr_entrega_notas, 2) <= round($vlr_perc_pedido, 2) && $args['entrega_atual'] == 1)
				{
					$args['msg'] .= '&bull;&nbsp;A soma do valor das notas fiscais cadastradas para a primeira entrega não corresponde ao percentual mínimo de atendimento do pedido (percentual: ' . (get_value($pedido, 'PERCENTUAL_ATENDIMENTO') * 100) . '%).<br />';
				}
				
				// Caso a soma dos totais das notas nao coincida com o total dos comprovantes cadastrados, erro
				if(round($vlr_entrega_notas, 2) != round($vlr_entrega, 2))
				{
					$args['msg'] .= '&bull;&nbsp;A soma dos totais das notas fiscais não confere com a soma do total dos comprovantes de entrega.<br />';
				}
				
				// Verifica se usuario atualé resp financeiro.
				if($this->session->userdata('idFinanceiro') == 0)
				{
					$args['msg'] .= '&bull;&nbsp;Somente o responsável financeiro pode finalizar a entrega.<br />';
					$args['error'] = true;
				}
			} else {
				$args['msg'] .= "&bull;&nbsp;Não existem notas fiscais cadastradas para esta entrega.<br />";
				$args['error'] = true;
			}
			
			// Finalização biblitoeca
			if(!$args['error'])
			{
				// Seta todos as notas com o numero da entrega atual
				$this->pedido_documento_model->set_notas_conferidas($args['notas_fiscais'], $args['entrega_atual']);
				$this->logsis->insereLog(array('UPDATE', 'OK', 'pedido_documento', $idpedido, 'Atualização de numero de entrega (' . $args['entrega_atual'] . ') para notas fiscais', var_export($args['notas_fiscais'], true)));
				
				// Mensagem de sucesso (varia conforme o numero da entrega)
				$args['msg'] = get_mensagem('success', 'Sucesso', $args['entrega_atual'] . 'ª entrega informada com sucesso. O valor desta entrega entrará na lista de liberação de recursos realizada por nossos administradores.');
				$this->logsis->insereLog(array('UPDATE', 'OK', 'pedido', $idpedido, 'Atualização de pedido, primeira entrega realizada por biblioteca'), $args['msg']);
			} else {
				// Finalização com problemas
				$args['idpedido'] = $idpedido;
				$args['pedido_model'] = $this->pedido_model;
				
				// Altera conteudo da mensagem
				$label_erro  = (isset($args['id_nota_erro'])) ? '<div style="margin:5px 0;">Linhas de tabelas coloridas em tom de vermelho identificam problemas.</div>' : '';
				$args['msg'] = $label_erro . get_mensagem('warning', 'Atenção', '<span class="font_11">Identificamos as seguintes incoerências nas informações de conferência do pedido:<br />' . $args['msg'] . '</span>'); 
				
				// insere log de finalização (erro)
				$this->logsis->insereLog(array('UPDATE', 'ERROR', 'pedido', $idpedido, 'Falha na finalização de entrega/conferência de pedido', $args['msg']));
			}
			
			// Utiliza na finalização view
			$args['pedido_documento_model'] = $this->pedido_documento_model;
			
			// Carrega view, passando resumo da finalização
			$this->load->view('pedido/finaliza_conferencia_biblioteca', $args);
		}
    }
	
	/**
	* save_qtde_entregas()
	* Salva a informação de quantas entregas serão feitas para o pedido atual.
	* @param integer idpedido
	* @param integer qtd_entregas
	* @return void
	**/
	function save_qtde_entregas($idpedido = 0, $qtd_entregas = 0)
	{
		$this->load->model('global_model');
		
		// Update na tabela de pedidolivro, tirando todos os itens marcados neste documento
		$this->global_model->update('pedido', array('MAX_QTDE_ENTREGAS' => $qtd_entregas), 'IDPEDIDO = ' . $idpedido);
		
		// Se nova quantidade de entregas for igual a 1, entao os documentos da entrega 2 serão jogados para a 1ª entrega
		if($qtd_entregas == 1)
		{
			$this->global_model->update('pedido_documento', array('NUMERO_ENTREGA' => 1), 'idpedido = ' . $idpedido . ' AND NUMERO_ENTREGA = 2 AND ID_PEDIDO_DOCUMENTO_TIPO = 2');
			$this->load->library('logsis', Array('UPDATE', 'OK', 'pedido_documento', $idpedido, 'Número da entrega do documento alterado', 'max_qtde_entregas => ' . $qtd_entregas));
		}
    }
	
	/**
	* download_excel()
	* Download de excel da listagem geral de módulos.
	* @param integer idusuario
	* @return void
	**/
	function download_excel() 
	{
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Carrega classe excel e model pedido
			$this->load->library('excel');
			$this->load->model('pedido_model');
			
			// Executa download do excel
			$this->excel->set_file_name('PEDIDOS_' . date('Y-m-d_His'));
			$this->excel->set_data($this->pedido_model->get_lista_modulo());
			$this->excel->download_file();
		}
	}
	
	/**
	* download_excel_pedido()
	* Download de excel do pedido em questão. Não foi possível utilizar
	* a library excel pois este xls mostra dados do pedido e seus itens
	* @param integer idpedido
	* @return void
	**/
	function download_excel_pedido($idpedido) 
	{
		// Seta nome do arquivo e faz controles com idpedido
		$idpedido = (is_null($idpedido)) ? '000000' : $idpedido;
		$filename = "Pedido_" . $idpedido . "_" . date('YmdHis') . ".xls";
		$contents = '';
		
		if ($this->session->userdata('tipoUsuario') == 1 || $this->session->userdata('tipoUsuario') == 2 || $this->session->userdata('tipoUsuario') == 4) //se for administrador ou biblioteca ou pdv
		{
			// Carrega models necessários
			$this->load->model('biblioteca_model');
			$this->load->model('pedido_model');
			$this->load->model('pdv_model');
			$this->load->model('global_model');
			
			$pedido = $this->pedido_model->getDadosPedido(IDPROGRAMA, 0, $idpedido);
			
			if(count($pedido) > 0)
			{
				// Prepara variável de contagem de linha (nitem)
				$i = 1;
				
				// Coleta os itens do pedido
				// print_r($itens);
				$itens = $this->pedido_model->getItensPedido(IDPROGRAMA, 0, $idpedido);
				
				// Monta o cabeçalho do excel - dados gerais do pedido
				$contents .= "CÓDIGO\t" . $pedido[0]['IDPEDIDO'] . "\n";
				$contents .= "BIBLIOTECA\t" . $pedido[0]['RAZAOSOCIAL_B'] . "\n";
				$contents .= "STATUS\t" . $pedido[0]['DESCSTATUS'] . "\n";
				$contents .= "PROGRAMA\t" . $pedido[0]['DESCPROGRAMA'] . "\n";
				$contents .= "PONTO DE VENDA\t" . $pedido[0]['RAZAOSOCIAL_P'] . "\n";
				$contents .= "ULT. ATUALIZAÇÃO\t" . $pedido[0]['DATA_ULT_ATU'] . "\n";
				
				//print_r($itens);
				
				// Adiciona n linhas para itens do pedido
				if(count($itens) > 0)
				{
					// Adiciona cabeçalho para itens
					$contents .= "\nITEM\tISBN\tTITULO\tEDIÇÃO\tANO\tAUTOR\tEDITORA\tQTD.\tVALOR UNIT.\tSUBTOTAL\t\n";
					foreach($itens as $item)
					{
						$contents .= $i . "\t";
						$contents .= '="' . $item['ISBN'] . "\"\t";
						$contents .= $item['TITULO'] . "\t";
						$contents .= $item['EDICAO'] . "\t";
						$contents .= $item['ANO'] . "\t";
						$contents .= $item['AUTOR'] . "\t";
						$contents .= $item['EDITORA'] . "\t";
						$contents .= $item['QTD_ENTREGUE'] . "\t";
						$contents .= $item['PRECO_UNIT'] . "\t";
						$contents .= $item['SUBTOTAL'] . "\t";
						$contents .= "\n";
						
						// Aumenta nlinha
						$i++;
					}
				} else {
					$contents .= "\nITEM\tISBN\tDADOS DO LIVRO\tQTD.\tVALOR UNIT.\tSUBTOTAL\t\nNenhum livro encontrado.";
				}
			} else {
				$contents = "Pedido não encontrado.";
			}
		} else {
			$contents = "Acesso não permitido.";
		}
		
		// Exporta efetivamente, o xls para tela
		header('Content-Description: File Transfer');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename='.$filename);
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		echo chr(255) . chr(254) . mb_convert_encoding($contents, 'UTF-16LE', 'UTF-8');
		exit;
	}
	
	/**
	* search()
	* Função de entrada/pesquisa do modulo.
	* @return void
	*/
	function search($actual_page = 0)
	{
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Coleta o formulário de filtros 
			$args['filtros'] = $this->input->post();
			
			// Carrega tabela de dados, seta o array de dados e limita a paginação
			$this->load->model('pedido_model');
			$this->load->library('array_table');
			$this->array_table->set_id('module_table');
			$this->array_table->set_data($this->pedido_model->get_lista_modulo($args['filtros']));
			$this->array_table->set_actual_page($actual_page);
			$this->array_table->set_page_link('pedido/search');
			
			// Adiciona colunas à tabela e seta as colunas de exibição
			$this->array_table->add_column('<a href="' . URL_EXEC . 'pedido/gerencia_administrativa/{0}"><img src="' . URL_IMG . 'icon_controle_pedido.png" title="Gerência Administrativa" /></a>');
			$this->array_table->add_column('<a href="' . URL_EXEC . 'pedido/conferencia_administrativa_pedido/{0}"><img src="' . URL_IMG . 'icon_conferencia_adm_pedido.png" title="Conferência Administrativa" /></a>');
			$this->array_table->add_column('<a href="' . URL_EXEC . 'pedido/conferencia/{0}"><img src="' . URL_IMG . 'icon_pedido_conferencia.png" title="Conferência (Simulação Biblioteca)" /></a>');
			$this->array_table->add_column('<a href="javascript:void(0)" onclick="iframe_modal(\'Observações do Pedido\', \'' . URL_EXEC . 'pedido/modal_observacoes/{0}\', \'' . URL_IMG . 'icon_observacoes.png\', 600, 750);"><img src="' . URL_IMG . 'icon_observacoes.png" title="Observações do Pedido" /></a>');
			$this->array_table->set_columns(array('#', 'SITUACAO', 'BIBLIOTECA', 'CPF/CNPJ BIBLIOTECA', 'PONTO DE VENDA', 'UF PDV', 'CIDADE PDV', 'VALORTOTAL', 'DT. ATUALIZAÇÃO'));
			
			// Processa tabela do modulo
			$args['module_table'] = $this->array_table->get_html();
			
			// Monta options de status de pedido
			$args['option_status'] = monta_options_array($this->pedido_model->getAllStatus(), get_value($args['filtros'], 'p__idpedidostatus'));
			
			$this->load->view('pedido/search', $args);
		} else {
			redirect('/home/');
		}
	}
	
	/**
	* conferencia_administrativa_pedido()
	* Tela de conferência administrativa do pedido. Caso idpedido nao seja encaminhado,
	* entao todos sao exibidos.
	* @param integer idpedido
	* @return void
	*/
	function conferencia_administrativa_pedido($idpedido = 0)
	{
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Load do models e libraries necessários
			$this->load->library('programa');
			$this->load->model('pedido_model');
			
			// Coleta filtros (ORDER BY)
			$args['filter'] = $this->input->post();
			$order_by = (get_value($args['filter'], 'filter_order_by') != '') ? get_value($args['filter'], 'filter_order_by') : 'TEM_RECURSO_LIBERADO';
			
			// Coleta todos os pedidos válidos (que tenham pelo menos um documento tipo nota fiscal com numero de entrega marcado)
			$args['pedidos'] = $this->pedido_model->get_pedidos_conferencia_adm($idpedido, $order_by);
			$args['pedido_model'] = $this->pedido_model;
			
			// Load do view
			$this->load->view('pedido/conferencia_administrativa_pedido', $args);
		} else {
			redirect('/home');
		}
	}
	
	/**
	* conferencia_administrativa_pedido_proccess()
	* Processa envio de formulário de salvamento de liberação de recurso.
	* @return void
	*/
	function conferencia_administrativa_pedido_proccess()
	{
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Load do models e libraries necessários
			$this->load->model('global_model');
			$this->load->model('pedido_model');
			
			// Coleta IDPEDIDO
			$idpedido = $this->input->post('idpedido');
			
			// Carrega post e faz inserts ou deletes necessários
			$post = $this->input->post();
			foreach($post as $index => $value)
			{
				if(valida_integer($index) && $value != '')
				{
					if(!$this->pedido_model->is_recurso_liberado($idpedido, $index))
					{
						$this->global_model->insert('pedido_valor_entrega', array('idpedido' => $idpedido, 'idusuario_liberacao' => $this->session->userdata('idUsuario'), 'numero_entrega' => $index, 'valor' => $value));
						$this->load->library('logsis', array('INSERT', 'OK', 'pedido_valor_entrega', $this->input->post('idpedido'), 'Inserção de valor de entrega', var_export($post, true)));
						
						// Marca todos os documentos do pedido da entrega atual com o liberar_edicao = 'N'
						// (pedido do sac) quando um recurso é liberado todos os documentos tem de ser desmarcados para edição
						$this->global_model->update('pedido_documento', array('LIBERAR_EDICAO' => 'N'), "IDPEDIDO = $idpedido AND NUMERO_ENTREGA = $index");
					}
				} elseif($index != 'idpedido') {
					$this->global_model->delete('pedido_valor_entrega', "IDPEDIDO = $idpedido AND numero_entrega = $index");
					$this->load->library('logsis', array('DELETE', 'OK', 'pedido_valor_entrega', $this->input->post('idpedido'), 'Delete de valor de entrega', var_export($post, true)));
				}
			}
		} else {
			redirect('/home');
		}
	}
	
	/**
	* modal_observacoes()
	* Modal de dados de observações do pedido (mini módulo). Funciona como um componente a parte,
	* bastando encaminhar o idpedido para ele.
	* @param integer idpedido
	* @return void
	*/
	function modal_observacoes($idpedido = 0)
	{
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Load do models e libraries necessários
			$this->load->model('pedido_model');
			
			// Coleta os dados das observações (necessários para outras validações em view)
			$args['observacoes'] = $this->pedido_model->get_observacoes($idpedido);
			
			// Monta tabela de dados de observações de pedidos
			$this->load->library('array_table');
			$this->array_table->set_id('module_table');
			$this->array_table->set_data($args['observacoes']);
			
			// Adiciona colunas à tabela e seta as colunas de exibição
			$this->array_table->add_column('<input type="checkbox" name="{0}" value="S" {LIDO_CHECKED} title="Observação Lida / Não lida"/>');
			$this->array_table->set_columns(array('#', 'LOGIN', 'CATEGORIA', 'OBSERVACAO', 'DATA INS.'));
			
			// Processa tabela do modulo
			$args['module_table'] = $this->array_table->get_html();
			$args['idpedido'] = $idpedido;
			
			// Load VIEW
			$this->load->view('pedido/modal_observacoes', $args);
		} else {
			redirect('/home');
		}
	}
	
	/**
	* modal_inserir_observacao()
	* Modal html formulário de insercao de observacao do pedido.
	* @param integer idpedido
	* @return void
	*/
	function modal_inserir_observacao($idpedido = 0)
	{
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Load do models e libraries necessários
			$this->load->model('pedido_model');
			$this->load->model('global_model');
			
			// Coleta dados do pedido
			$args['idpedido'] = $idpedido;
			$args['pedido'] = $this->pedido_model->get_dados_pedido($idpedido);
			
			// Load VIEW
			$this->load->view('pedido/modal_inserir_observacao', $args);
		} else {
			redirect('/home');
		}
	}
	
	/**
	* modal_inserir_observacao_proccess()
	* Processa o envio de formulário feito pela dialog de inserção de observacoes de pedidos.
	* @return void
	**/
	function modal_inserir_observacao_proccess()
	{
		// Somente ADM, biblioteca ou pdv podem entrar nesta tela
        if($this->session->userdata('tipoUsuario') == 1) {
			// Carrega model global, para insert
			$this->load->model('global_model');
			
			// Monta array de inserção
			$args['idpedido'] = $this->input->post('idpedido');
			$args['idusuario'] = $this->session->userdata('idUsuario');
			$args['categoria'] = $this->input->post('categoria');
			$args['observacao'] = $this->input->post('observacao');
			
			// Insert
			$this->global_model->insert('pedido_observacao', $args);
			$this->load->library('logsis', Array('INSERT', 'OK', 'pedido_observacao', $this->input->post('idpedido'), 'Inserção de observação de pedido', var_export($args, true)));
			
			// Parse na tela que fecha a dialog, e chama o load da tabela de documentos
			redirect('/pedido/modal_observacoes/' . $this->input->post('idpedido'));
		} else {
			redirect('/home');
		}
    }
	
	/**
	* modal_marcar_observacao_lida()
	* Processa o envio de formulário feito para marcar obs como lidas.
	* @return void
	**/
	function modal_marcar_observacao_lida()
	{
		// Somente ADM, biblioteca ou pdv podem entrar nesta tela
        if($this->session->userdata('tipoUsuario') == 1) {
			// Carrega model global, para insert e logsis
			$this->load->model('global_model');
			$this->load->library('logsis');
			
			// Marca todas as mensagens do pedido como nao lidas, marcando uma a uma das selecionadas, posteriormente
			$idpedido = $this->input->post('idpedido');
			$this->global_model->update('pedido_observacao', array('LIDO' => 'N'), "IDPEDIDO = $idpedido");
			$this->logsis->insereLog(Array('UPDATE', 'OK', 'pedido_observacao', $idpedido, 'Atualização de leitura de observações (Todas marcadas como \'\'N\'\')', var_export($this->input->post(), true)));
			
			// Coleta o post e marca como lidas somente as selecionadas
			$post = $this->input->post();
			foreach($post as $index => $value)
			{
				// Valida inteiro para pegar somente as observações (idpedido não)
				if(valida_integer($index)){ $this->global_model->update('pedido_observacao', array('LIDO' => 'S'), "ID_PEDIDO_OBSERVACAO = $index"); }
				$this->logsis->insereLog(Array('UPDATE', 'OK', 'pedido_observacao', $idpedido, 'Atualização de leitura de observação (marcada como \'\'S\'\')', 'ID_PEDIDO_OBSERVAÇÃO: ' . $index));
			}
			
			// Parse na tela que fecha a dialog, e chama o load da tabela de documentos
			redirect('/pedido/modal_observacoes/' . $idpedido);
		} else {
			redirect('/home');
		}
    }
	
	/**
	* lista_pedidos_pdv()
	* Função de pesquisa de pedidos do pdv (tela de pedidos do pdv).
	* @return void
	*/
	function lista_pedidos_pdv($actual_page = 0)
	{
		if($this->session->userdata('tipoUsuario') == 4)
		{
			// Coleta o formulário de filtros e idusuario
			$args['filtros'] = $this->input->post();
			$args['idusuario'] = $this->session->userdata('idUsuario');
			$args['filtros']['p__idpdv'] = $args['idusuario'];
			
			// Carrega tabela de dados, seta o array de dados e limita a paginação
			$this->load->model('pedido_model');
			$this->load->library('array_table');
			$this->array_table->set_id('module_table');
			$this->array_table->set_data($this->pedido_model->get_lista_pedidos_pdv($args['filtros']));
			$this->array_table->set_actual_page($actual_page);
			$this->array_table->set_page_link('pedido/lista_pedidos_pdv');
			
			// Adiciona colunas à tabela e seta as colunas de exibição
			$this->array_table->add_column('<a href="' . URL_EXEC . 'pedido/form_view_pedido/{0}?url=pedido/lista_pedidos_pdv"><img src="' . URL_IMG . 'icon_view.png" title="Visualizar Detalhes" /></a>');
			$this->array_table->add_column('<a href="' . URL_EXEC . 'pedido/conferencia/{0}"><img src="' . URL_IMG . 'icon_pedido_conferencia.png" title="Conferência" /></a>');
			$this->array_table->add_column('<img src="' . URL_IMG . 'icon_excel.gif" title="Exportar para Excel" onclick="download_excel(\'' . URL_EXEC . 'pedido/download_excel_pedido/{0}\')" style="cursor:pointer" />');
			$this->array_table->add_column('{EDITAL_IMG}', false);
			$this->array_table->set_columns(array('#', 'SITUAÇÃO', 'BIBLIOTECA', 'QTDE.', 'VALORTOTAL'));
			
			// Processa tabela do modulo
			$args['module_table'] = $this->array_table->get_html();
			
			// Monta options de status de pedido
			$args['option_status'] = monta_options_array($this->pedido_model->getAllStatus(), get_value($args['filtros'], 'p__idpedidostatus'));
			
			$this->load->view('pedido/lista_pedidos_pdv', $args);
		} else {
			redirect('/home/');
		}
	}
	
	/**
	* lista_pedidos_biblioteca()
	* Função de pesquisa de pedidos da biblioteca (tela de pedidos da biblioteca).
	* @return void
	*/
	function lista_pedidos_biblioteca($actual_page = 0)
	{
		if($this->session->userdata('tipoUsuario') == 2)
		{
			// Coleta o formulário de filtros e idusuario
			$args['filtros'] = $this->input->post();
			$args['idusuario'] = $this->session->userdata('idUsuario');
			$args['filtros']['p__idbiblioteca'] = $args['idusuario'];
			
			// Carrega tabela de dados, seta o array de dados e limita a paginação
			$this->load->model('pedido_model');
			$this->load->library('array_table');
			$this->array_table->set_id('module_table');
			$this->array_table->set_data($this->pedido_model->get_lista_pedidos_biblioteca($args['filtros']));
			$this->array_table->set_actual_page($actual_page);
			$this->array_table->set_page_link('pedido/lista_pedidos_biblioteca');
			
			// Adiciona colunas à tabela e seta as colunas de exibição
			$this->array_table->add_column('<a href="' . URL_EXEC . 'pedido/form_view_pedido/{0}?url=pedido/lista_pedidos_biblioteca"><img src="' . URL_IMG . 'icon_view.png" title="Visualizar Detalhes" /></a>');
			$this->array_table->add_column('<a href="' . URL_EXEC . 'pedido/conferencia/{0}"><img src="' . URL_IMG . 'icon_pedido_conferencia.png" title="Conferência" /></a>');
			$this->array_table->add_column('<img src="' . URL_IMG . 'icon_excel.gif" title="Exportar para Excel" onclick="download_excel(\'' . URL_EXEC . 'pedido/download_excel_pedido/{0}\')" style="cursor:pointer" />');
			$this->array_table->add_column('{EDITAL_IMG}', false);
			$this->array_table->set_columns(array('#', 'SITUAÇÃO', 'PONTO DE VENDA', 'QTDE.', 'VALORTOTAL'));
			
			// Processa tabela do modulo
			$args['module_table'] = $this->array_table->get_html();
			
			// Monta options de status de pedido
			$args['option_status'] = monta_options_array($this->pedido_model->getAllStatus(), get_value($args['filtros'], 'p__idpedidostatus'));
			
			$this->load->view('pedido/lista_pedidos_biblioteca', $args);
		} else {
			redirect('/home/');
		}
	}
	
	/**
	* download_excel_lista_itens_pedidos_total_pdv()
	* Download de excel da listagem geral de itens de pedidos geral de um determinado pdv.
	* @param integer idusuario
	* @return void
	*/
	function download_excel_lista_itens_pedidos_total_pdv($idusuario = 0)
	{
		if($this->session->userdata('tipoUsuario') == 4)
		{
			// Carrega classe excel e model pedido
			$this->load->library('excel');
			$this->load->model('pedido_model');
			
			// Executa download do excel
			$this->excel->set_file_name('PEDIDOS_' . date('Y-m-d_His'));
			$this->excel->set_data($this->pedido_model->get_lista_itens_pedidos_total_pdv($idusuario));
			$this->excel->download_file();
		}
	}
	
	/**
	* form_view_pedido()
	* Html do formulário padrão de visualização de pedido.
	* @return void
	**/
	function form_view_pedido($idpedido = 0)
	{
		// Somente PDV, ADM ou BIBLIOTECA podem ver pedidos
		if($this->session->userdata('tipoUsuario') == 1 || $this->session->userdata('tipoUsuario') == 2 || $this->session->userdata('tipoUsuario') == 4)
		{
			// Carrega classes necessárias para utilização
			$this->load->model('pedido_model');
			$this->load->library('array_table');
			$table_docs = $this->array_table->get_instance();
			$table_item = $this->array_table->get_instance();
			
			// Dados da entidade (pedido)
			$args['dados'] = $this->pedido_model->get_dados_pedido($idpedido);
			$args['pedido_itens_count'] = $this->pedido_model->get_count_itens($idpedido);
			$args['pedido_titulos_count'] = $this->pedido_model->get_count_titulos($idpedido);
			$args['pedido_model'] = $this->pedido_model;
			
			// Tabela de documentos
			$table_docs->set_id('documentos');
			$table_docs->set_items_per_page(50000);
			$table_docs->set_data($this->pedido_model->get_documentos_pedido($idpedido));
			$table_docs->add_column('<img src="' . URL_IMG . 'icon_img_info.png" title="Nome do Arquivo: {ARQUIVO}" />', false);
			$table_docs->add_column('<a href="' . URL_EXEC . 'application/uploads/{ARQUIVO}" target="_blank"><img src="' . URL_IMG . 'icon_anexo_img.png" title="Clique para visualizar a imagem" /></a>', false);
			$table_docs->set_columns(array('#', 'NUM. DOC', 'EXEMPLARES', 'VALOR DOC', 'NÚMERO ENTREGA', 'INSERIDO POR'));
			$args['table_documentos'] = $table_docs->get_html();
			
			// Coleta dados do comitê de acervo
			$table_item->set_id('itens_pedido');
			$table_item->set_items_per_page(50000);
			$table_item->set_data($this->pedido_model->get_itens_pedido($idpedido));
			$table_item->add_column('{AUTOR_IMG}', false);
			$table_item->set_columns(array('#', 'EDITORA_LINK', 'ISBN', 'TITULO', 'EDIÇÃO', 'QTD', 'VLR. UNIT.', 'SUBTOTAL'));
			$args['table_itens_pedido'] = $table_item->get_html();
			
			// Processa url_redirect (para redirecionamento do botão aplicar, ok e voltar do form)
			$args['url_redirect'] = ($this->input->get('url') == '') ? 'biblioteca/search' : $this->input->get('url');
			
			// Load da camada view
			$this->load->view('pedido/form_view_pedido', $args);
		}
    }
	
	/**
	* gerencia_administrativa()
	* Html do painel de gerência do pedido.
	* @param integer idpedido
	* @return void
	*/
	function gerencia_administrativa($idpedido = 0)
	{
		// Somente ADM podem ver pedidos
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Carrega classes necessárias para utilização
			$this->load->model('pedido_model');
			$this->load->library('array_table');
			$this->load->model('biblioteca_model');
			$table_docs = $this->array_table->get_instance();
			$table_item = $this->array_table->get_instance();
			
			// Dados da entidade (pedido)
			$args['dados'] = $this->pedido_model->get_dados_pedido($idpedido);
			$args['pedido_itens_count'] = $this->pedido_model->get_count_itens($idpedido);
			$args['pedido_titulos_count'] = $this->pedido_model->get_count_titulos($idpedido);
			$args['pedido_model'] = $this->pedido_model;
			$args['tem_recurso_liberado'] = $this->pedido_model->tem_recurso_liberado($idpedido);
			
			// Dados do box lateral de informações financeiras
			$args['resp_financeiro'] = $this->biblioteca_model->get_dados_responsavel_financeiro_by_id_biblioteca(get_value($args['dados'], 'IDBIBLIOTECA'));
			$args['entregas_finalizadas'] = $this->pedido_model->get_max_entregas_finalizadas($idpedido);
			
			// Tabela de documentos
			$table_docs->set_id('documentos');
			$table_docs->set_items_per_page(50000);
			$table_docs->set_data($this->pedido_model->get_documentos_pedido($idpedido));
			$table_docs->add_column('<a href="javascript:void(0);" onclick="iframe_modal(\'Anexos\', \'' . URL_EXEC . 'pedido_documento/modal_anexos_documento/{0}\', \'' . URL_IMG . 'icon_anexos.png\', 600, 700);" ><img src="' . URL_IMG . 'icon_anexos.png" title="Anexos" /></a>');
			$table_docs->add_column('<a href="javascript:void(0);" onclick="iframe_modal(\'Editar\', \'' . URL_EXEC . 'pedido/modal_form_update_documento/{0}\', \'' . URL_IMG . 'icon_edit.png\', 600, 700);" ><img src="' . URL_IMG . 'icon_edit.png" title="Editar" /></a>');
			$table_docs->add_column('<a href="javascript:void(0);" onclick="iframe_modal(\'Liberar Edição\', \'' . URL_EXEC . 'pedido_documento/modal_liberar_edicao_documento/{0}\', \'' . URL_IMG . 'icon_liberar_edicao.png\', 600, 700);" ><img src="' . URL_IMG . 'icon_liberar_edicao.png" title="Liberar Edição" /></a>');
			$table_docs->add_column('<img src="' . URL_IMG . 'icon_img_info.png" title="Nome do Arquivo: {ARQUIVO}" />', false);
			$table_docs->add_column('<a href="' . URL_EXEC . 'application/uploads/{ARQUIVO}" target="_blank"><img src="' . URL_IMG . 'icon_anexo_img.png" title="Clique para visualizar a imagem" /></a>', false);
			$table_docs->set_columns(array('#', 'NUM. DOC', 'EXEMPLARES', 'VALOR DOC', 'NÚMERO ENTREGA', 'INSERIDO POR'));
			$args['table_documentos'] = $table_docs->get_html();
			
			// Coleta dados do comitê de acervo
			$table_item->set_id('itens_pedido');
			$table_item->set_items_per_page(50000);
			$table_item->set_data($this->pedido_model->get_itens_pedido($idpedido));
			$table_item->add_column('{AUTOR_IMG}', false);
			$table_item->set_columns(array('#', 'EDITORA_LINK', 'ISBN', 'TITULO', 'EDIÇÃO', 'QTD', 'VLR. UNIT.', 'SUBTOTAL'));
			$args['table_itens_pedido'] = $table_item->get_html();
			
			// Processa url_redirect (para redirecionamento do botão aplicar, ok e voltar do form)
			$args['url_redirect'] = ($this->input->get('url') == '') ? 'biblioteca/search' : $this->input->get('url');
			
			// Load da camada view
			$this->load->view('pedido/gerencia_administrativa', $args);
		}
    }
	
	/**
	* ajax_save_config_pedido()
	* Ajax que salva configurações do pedido.
	* @param integer idpedido
	* @return void
	*/
	function ajax_save_config_pedido($idpedido = 0)
	{
		// Somente ADM pode mexer em configs do pedido
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Carrega classes necessários
			$this->load->library('logsis');
			$this->load->model('global_model');
			
			// Coleta flag liberação ins. documento
			// echo $this->input->post('liberar_ins_documento');
			$liberar_ins_documento = ($this->input->post('liberar_ins_documento') == '') ? 'N' : $this->input->post('liberar_ins_documento');
			
			// Update no pedido
			$this->global_model->update('pedido', array('liberar_ins_documento' => $liberar_ins_documento), "IDPEDIDO = $idpedido");
			$this->logsis->insereLog(array('UPDATE', 'OK', 'pedido', $idpedido, 'Atualização de configuração de pedido', var_export($this->input->post(), true)));
		}
	}
}