<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pdv extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('session');
		
		if ($this->session->userdata('acessoSis') == FALSE)
		{
			redirect('/login/');
			exit;
		}
		
		// Older: outros usuários devem ser capazes de acessar o controller PDV
		/*
		if ($this->session->userdata('tipoUsuario') != 4)
		{
			redirect('/login/');
			exit;
		}
		*/
	}
	
	/*************************************************************************
	 *
	 * CONTROLLERS DE PONTO DE VENDA
	 *
	 * form_alt            : formulario de alteração de cadastro
     * pedidosLivro        : listar pedidos de livros
	 * pedido              : detalhar pedido
     *
	 *************************************************************************/
	
	function index()
	{
		$arrErro['msg']      = 'Página não existe.';
		$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
		$this->load->view('erro_view', $arrErro);
	}
	
	function form_alt($acao = 'edit')
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('usuario_class');
		$this->load->model('global_model');
		$this->load->model('pdv_model');
		$this->load->model('usuario_model');
		$this->load->model('naturezajur_model');
		
		$arrDados   = Array();
		$idUsuario  = $this->session->userdata('idUsuario');
		$tipopessoa = '';
		
		if ($acao == 'save')
		{
			$tipopessoa = $this->input->post('tipopessoa');
			$labelAux   = ($tipopessoa == 'PJ' ? 'Razão Social' : 'Nome');
			
			$this->form_validation->set_rules('tipopessoa',      'Tipo Pessoa',                          'required');
			$this->form_validation->set_rules('cnpjcpf',         'CNPJ/CPF',                             'trim|required|max_length[18]|valida_cnpj_cpf|xss_clean');
			$this->form_validation->set_rules('iestadual',       'Inscrição Estadual',                   'trim|max_length[20]|xss_clean');
			$this->form_validation->set_rules('razaosocial',     $labelAux,                              'trim|required|max_length[100]|xss_clean');
			$this->form_validation->set_rules('nomefantasia',    'Nome Fantasia',                        'trim|max_length[100]|xss_clean');
			$this->form_validation->set_rules('naturezajur',     'Natureza Jurídica',                    'valida_combo|xss_clean');
			$this->form_validation->set_rules('telefone1',       'Telefone Geral',                       'trim|required|max_length[15]|xss_clean');
			$this->form_validation->set_rules('email',           'E-mail Geral',                         'trim|required|max_length[100]|valid_email');
			$this->form_validation->set_rules('site',            'Site',                                 'trim|max_length[100]|xss_clean');
			$this->form_validation->set_rules('login',           'Login',                                '');
			$this->form_validation->set_rules('idlogradouro',    'Endereço',                             'is_numeric');
			$this->form_validation->set_rules('cep',             '',                                     '');
			$this->form_validation->set_rules('uf',              '',                                     '');
			$this->form_validation->set_rules('cidade',          '',                                     '');
			$this->form_validation->set_rules('bairro',          '',                                     '');
			$this->form_validation->set_rules('logradouro',      '',                                     '');
			$this->form_validation->set_rules('logcomplemento',  '',                                     '');
			$this->form_validation->set_rules('end_numero',      'Endereço - Número',                    'trim|required|max_length[8]|xss_clean');
			$this->form_validation->set_rules('end_complemento', 'Complemento',                          'trim|max_length[30]|xss_clean');
			$this->form_validation->set_rules('area_total',      'Área total de vendas',                 'trim|max_length[10]|xss_clean');
			$this->form_validation->set_rules('area_livros',     'Área de vendas para livros',           'trim|max_length[10]|xss_clean');
			$this->form_validation->set_rules('assoc_ent_sind',  'Associado a entidade sindical',        'trim|max_length[100]|xss_clean');
			$this->form_validation->set_rules('fil_ent_assoc',   'Filiada a entidade(s) associativa(s)', 'trim|max_length[100]|xss_clean');
			$this->form_validation->set_rules('banco',           'Banco',                                'trim|max_length[20]|xss_clean');
			$this->form_validation->set_rules('agencia',         'Agência',                              'trim|max_length[20]|xss_clean');
			$this->form_validation->set_rules('conta',           'Conta',                                'trim|max_length[20]|xss_clean');
			$this->form_validation->set_rules('nomeresp',        'Nome Responsável',                     'trim|required|max_length[100]|xss_clean');
			$this->form_validation->set_rules('telefoneresp',    'Telefone do Responsável',              'trim|required|max_length[15]|xss_clean');
			$this->form_validation->set_rules('skyperesp',       'Skype do Responsável',                 'trim|max_length[30]|xss_clean');
			$this->form_validation->set_rules('emailresp',       'E-mail do Responsável',                'trim|required|max_length[100]|valid_email');
			$this->form_validation->set_rules('socios',          'Sócios',                               'trim|valida_socios|xss_clean');
			
			if ($this->form_validation->run() == TRUE)
			{
				if ($tipopessoa != 'PJ' && $tipopessoa != 'PF')
				{
					$arrDados['outrosErros'] = 'Tipo pessoa inválido.';
					$acao = '';
				}
				elseif ($this->input->post('idlogradouro') == '')
				{
					$arrDados['outrosErros'] = 'Informe o endereço.';
					$acao = '';
				}
				else
				{
					$where = sprintf("IDUSUARIO = %s", $idUsuario);
					
					$this->db->trans_start();
					
					$arrUpd = Array();
					$arrUpd['IDLOGRADOURO']       = ($this->input->post('idlogradouro') != '' ? $this->input->post('idlogradouro') : NULL);
					$arrUpd['IDNATUREZAJUR']      = $this->input->post('naturezajur');
					$arrUpd['IESTADUAL']          = $this->input->post('iestadual');
					$arrUpd['RAZAOSOCIAL']        = removeAcentos($this->input->post('razaosocial'), TRUE);
					$arrUpd['NOMEFANTASIA']       = removeAcentos($this->input->post('nomefantasia'), TRUE);
					$arrUpd['TELEFONEGERAL']      = $this->input->post('telefone1');
					$arrUpd['EMAILGERAL']         = $this->input->post('email');
					$arrUpd['SITE']               = $this->input->post('site');
					$arrUpd['AREATOTAL_VENDAS']   = $this->input->post('area_total');
					$arrUpd['AREATOTAL_LIVROS']   = $this->input->post('area_livros');
					$arrUpd['NOMERESP']           = removeAcentos($this->input->post('nomeresp'), TRUE);
					$arrUpd['TELEFONERESP']       = $this->input->post('telefoneresp');
					$arrUpd['EMAILRESP']          = $this->input->post('emailresp');
					$arrUpd['SKYPERESP']          = $this->input->post('skyperesp');
					$arrUpd['END_NUMERO']         = $this->input->post('end_numero');
					$arrUpd['END_COMPLEMENTO']    = removeAcentos($this->input->post('end_complemento'), TRUE);
					$arrUpd['BANCO']              = $this->input->post('banco');
					$arrUpd['AGENCIA']            = $this->input->post('agencia');
					$arrUpd['CONTA']              = $this->input->post('conta');
					$arrUpd['ASSOC_ENT_SINDICAL'] = $this->input->post('assoc_ent_sind');
					$arrUpd['FILIACAO_ENT_ASSOC'] = $this->input->post('fil_ent_assoc');
					$arrUpd['LATITUDE']           = NULL;
					$arrUpd['LONGITUDE']          = NULL;
					$arrUpd['DATA_UPD']           = date('Y/m/d H:i:s');
					
					$this->global_model->update('cadpdv', $arrUpd, $where);
					
					$this->usuario_class->delete_socio($idUsuario);
					
					if ($this->input->post('socios') != '')
					{
						$this->usuario_class->save_socio($idUsuario, $this->input->post('socios'));
					}
					
					$this->db->trans_complete();
					
					if ($this->db->trans_status() === FALSE)
					{
						//salva log
						$this->load->library('logsis', Array('UPDATE', 'ERRO', 'cadpdv', $idUsuario, 'Atualização de cadastro de PDV', var_export($arrUpd, TRUE)));
						
						$arrErro['msg']      = 'Ocorreu um erro na tentativa de atualizar o cadastro.';
						$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
						$this->load->view('erro_view', $arrErro);
					}
					else
					{
						//geocodificar endereço
						$arrEnd      = $this->pdv_model->getDadosCadastro($idUsuario);
						$address     = str_replace(' ','+',$arrEnd[0]['NOMELOGRADOURO2']) . ',+' . str_replace(' ','+',$arrEnd[0]['END_NUMERO']) . ',+' . str_replace(' ','+',$arrEnd[0]['NOMEBAIRRO']) . ',+' . str_replace(' ','+',$arrEnd[0]['NOMECIDADESUB']) . '+' . $arrEnd[0]['IDUF'];
						$request_url = MAPS_HOST . "&address=" . $address;
						
						$arqXml = simplexml_load_file($request_url);
						
						if ($arqXml)
						{
							$status = $arqXml->status;
							
							if ($status == 'OK')
							{
								$dadosLatLong = $arqXml->xpath('/GeocodeResponse/result[1]/geometry/location');
								
								$arrGeoCod = Array();
								$arrGeoCod['LATITUDE']  = $dadosLatLong[0]->lat;
								$arrGeoCod['LONGITUDE'] = $dadosLatLong[0]->lng;
								
								$this->global_model->update('cadpdv', $arrGeoCod, $where);
								
								//aprnas para log
								$arrUpd['LATITUDE']  = $dadosLatLong[0]->lat;
								$arrUpd['LONGITUDE'] = $dadosLatLong[0]->lng;
							}
						}
						
						//salva log
						$this->load->library('logsis', Array('UPDATE', 'OK', 'cadpdv', $idUsuario, 'Atualização de cadastro de PDV', var_export($arrUpd, TRUE)));
						
						$arrSucesso['msg']      = 'Cadastro atualizado com sucesso!';
						$arrSucesso['link']     = 'home';
						$arrSucesso['txtlink']  = 'Ir para Home';
						$arrSucesso['tipoUser'] = $this->session->userdata('tipoUsuario');
						$this->load->view('sucesso_view', $arrSucesso);
					}
				}
			}
			else
			{
				$acao = '';
			}
		}
		
		//informações zeradas para nao dar erro no parse
		$arrDados['tipopessoa2']     = $tipopessoa;
		$arrDados['tipopessoa_aux']  = '';
		$arrDados['cnpjcpf']         = '';
		$arrDados['iestadual']       = '';
		$arrDados['razaosocial']     = '';
		$arrDados['nomefantasia']    = '';
		$arrDados['naturezajur_aux'] = 0;
		$arrDados['telefone1']       = '';
		$arrDados['email']           = '';
		$arrDados['site']            = '';
		$arrDados['login']           = '';
		$arrDados['idlogradouro']    = '';
		$arrDados['cep']             = '';
		$arrDados['uf']              = '';
		$arrDados['cidade']          = '';
		$arrDados['bairro']          = '';
		$arrDados['logradouro']      = '';
		$arrDados['logcomplemento']  = '';
		$arrDados['end_numero']      = '';
		$arrDados['end_complemento'] = '';
		$arrDados['area_total']      = '';
		$arrDados['area_livros']     = '';
		$arrDados['assoc_ent_sind']  = '';
		$arrDados['fil_ent_assoc']   = '';
		$arrDados['banco']           = '';
		$arrDados['agencia']         = '';
		$arrDados['conta']           = '';
		$arrDados['nomeresp']        = '';
		$arrDados['telefoneresp']    = '';
		$arrDados['skyperesp']       = '';
		$arrDados['emailresp']       = '';
		$arrDados['socios']          = '';
		$arrDados['loadDadosEdit']   = 0;
		
		if ($acao == 'edit')
		{
			$dados = $this->pdv_model->getDadosCadastro($idUsuario);
			
			$arrDados['tipopessoa2']     = $dados[0]['TIPOPESSOA'];
			$arrDados['tipopessoa_aux']  = $dados[0]['TIPOPESSOA'];
			$arrDados['cnpjcpf']         = $dados[0]['CPF_CNPJ'];
			$arrDados['iestadual']       = $dados[0]['IESTADUAL'];
			$arrDados['razaosocial']     = $dados[0]['RAZAOSOCIAL'];
			$arrDados['nomefantasia']    = $dados[0]['NOMEFANTASIA'];
			$arrDados['naturezajur_aux'] = $dados[0]['IDNATUREZAJUR'];
			$arrDados['telefone1']       = $dados[0]['TELEFONEGERAL'];
			$arrDados['email']           = $dados[0]['EMAILGERAL'];
			$arrDados['site']            = $dados[0]['SITE'];
			$arrDados['login']           = $dados[0]['LOGIN'];
			$arrDados['idlogradouro']    = $dados[0]['IDLOGRADOURO'];
			$arrDados['cep']             = $dados[0]['CEP'];
			$arrDados['uf']              = $dados[0]['IDUF'];
			$arrDados['cidade']          = $dados[0]['NOMECIDADESUB'];
			$arrDados['bairro']          = $dados[0]['NOMEBAIRRO'];
			$arrDados['logradouro']      = $dados[0]['NOMELOGRADOURO2'];
			$arrDados['logcomplemento']  = $dados[0]['COMPLEMENTO'];
			$arrDados['end_numero']      = $dados[0]['END_NUMERO'];
			$arrDados['end_complemento'] = $dados[0]['END_COMPLEMENTO'];
			$arrDados['area_total']      = $dados[0]['AREATOTAL_VENDAS'];
			$arrDados['area_livros']     = $dados[0]['AREATOTAL_LIVROS'];
			$arrDados['assoc_ent_sind']  = $dados[0]['ASSOC_ENT_SINDICAL'];
			$arrDados['fil_ent_assoc']   = $dados[0]['FILIACAO_ENT_ASSOC'];
			$arrDados['banco']           = $dados[0]['BANCO'];
			$arrDados['agencia']         = $dados[0]['AGENCIA'];
			$arrDados['conta']           = $dados[0]['CONTA'];
			$arrDados['nomeresp']        = $dados[0]['NOMERESP'];
			$arrDados['telefoneresp']    = $dados[0]['TELEFONERESP'];
			$arrDados['skyperesp']       = $dados[0]['SKYPERESP'];
			$arrDados['emailresp']       = $dados[0]['EMAILRESP'];
			$arrDados['socios']          = $this->usuario_class->get_socio($idUsuario, 'string');
			$arrDados['loadDadosEdit']   = 1;
			
			$acao = '';
		}
		
		if ($acao == '')
		{
			$arrDados['natjur'] = $this->naturezajur_model->getCombo($arrDados['tipopessoa2']);
			
			$this->parser->parse('pdv/cadastro_formalt_view', $arrDados);
		}
	}

    function pedidosLivro($nPag = 1)
	{
        $this->load->library('form_validation');
		$this->load->model('pedido_model');
        $this->load->model('global_model');

		$arrDados = Array();
		$arrDados['nPag']      = (is_numeric($nPag) && $nPag > 0 ? $nPag : 1);
		$arrDados['exibir_pp'] = 15;
		$arrDados['limit_de']  = ($arrDados['nPag'] - 1) * $arrDados['exibir_pp'];

        //select do programa
		$arrDados['programas'] = $this->global_model->selectx('programa', 'ATIVO = \'S\'', 'DESCPROGRAMA');
        
        //cria array para filtros
        $where = Array();
        array_push($where, "p.IDPDV = " . $this->session->userdata('idUsuario'));
        //pedido diferente de EM CONFECÇÃO
        array_push($where, "p.IDPEDIDOSTATUS != 1");

        $this->form_validation->set_rules('filtro_status','Status','trim|xss_clean');
        $this->form_validation->set_rules('filtro_codpedido','Cod. Pedido','trim|integer|xss_clean');
        $this->form_validation->set_rules('filtro_biblioteca','Biblioteca','trim|xss_clean');
        $this->form_validation->set_rules('filtro_programa','Edital','trim|xss_clean');

        if($this->form_validation->run() == TRUE || $this->input->post() == FALSE)
        {
            $status      = $this->input->post('filtro_status');
            $biblioteca  = $this->input->post('filtro_biblioteca');
            $codpedido   = $this->input->post('filtro_codpedido');
            $programa    = $this->input->post('filtro_programa');

            // Filtro para programa
            if($programa != '')
            {                
               $arrDados['programa'] = " AND p.IDPROGRAMA = " . trim($programa);
            }
            else
            {
               $arrDados['programa'] = ""; 
            }

            // Filtro para status pedido
            if($status != '')
            {
                array_push($where, "p.IDPEDIDOSTATUS = '" . $status . "'");
            }

            // Filtro para Biblioteca
            if($codpedido != '')
            {
                array_push($where, "p.IDPEDIDO = '" . $codpedido . "'");
            }

            // Filtro para Biblioteca
            if($biblioteca != '')
            {
                array_push($where, "(cb.RAZAOSOCIAL LIKE '%" . $biblioteca . "%' OR cb.NOMEFANTASIA LIKE '%" . $biblioteca . "%')");
            }
            
        }

        // set filtros para pesquisa.
        $arrDados['where'] = implode(" AND ", $where);

		//pesquisa
		$arrDados['pedidos'] = $this->pedido_model->lista($arrDados, FALSE);
		$tamArr             = count($arrDados['pedidos']);
		$arrDados['vazio']  = ($tamArr == 0 ? TRUE : FALSE);

		for ($i = 0; $i < $tamArr; $i++)
		{
			$arrDados['pedidos'][$i]['NLINHA'] = $i + 1 + $arrDados['limit_de'];
            $ped = $this->pedido_model->listaLivrosPedidoById($arrDados['pedidos'][$i]['IDPEDIDO'], TRUE);
            $arrDados['pedidos'][$i]['QTDTOTAL'] = $ped[0]['QTDTOTAL'];
            $arrDados['pedidos'][$i]['VALORTOTAL'] = "R$ " . masc_real($arrDados['pedidos'][$i]['VALORTOTAL']);
            $arrDados['pedidos'][$i]['NOMEFANTASIA'] = ($arrDados['pedidos'][$i]['NOMEFANTASIA'] != '' ? '/ ' . $arrDados['pedidos'][$i]['NOMEFANTASIA'] : '');
            $arrDados['pedidos'][$i]['PROGRAMA'] = $this->global_model->get_dado("PROGRAMA","DESCPROGRAMA","IDPROGRAMA = ".$arrDados['pedidos'][$i]['IDPROGRAMA']);
		}
		
		$arrDados['pedidos'] = add_corLinha($arrDados['pedidos'], 1);
		
		//dados da paginação
		$qtdReg = $this->pedido_model->lista($arrDados, TRUE);
		$nPaginas = $qtdReg[0]['CONT'] / $arrDados['exibir_pp'];

		if ($qtdReg[0]['CONT'] % $arrDados['exibir_pp'] != 0)
		{
			$nPaginas = (int)$nPaginas + 1;
		}
		
        //monta filtro do status
        $arrDados['statusFilt'] = $this->global_model->selectx('pedidostatus', 'IDPEDIDOSTATUS != 1', 'IDPEDIDOSTATUS');

		$arrDados['nPaginas'] = $nPaginas;
		$arrDados['qtdReg']   = $qtdReg[0]['CONT'];

		$this->parser->parse('pdv/pedido_lista_view', $arrDados);
    }
	
    function pedido($idPedido = null, $acao = 'view')
    {
        $this->load->library('form_validation');
        $this->load->model('pedido_model');
        $this->load->model('global_model');
        
		if ($idPedido && is_numeric($idPedido))
		{
			$arrDados = Array();
			
			$arrParam = Array();
			$arrParam['limit_de'] = 0;
			$arrParam['exibir_pp'] = 1;

            $arrParam['programa']  = " AND p.IDPROGRAMA = " . $this->session->userdata('programa');
			$arrParam['where']  = " p.IDPDV = " . $this->session->userdata('idUsuario') . " ";
			$arrParam['where'] .= " AND p.IDPEDIDO = '" . $idPedido . "' ";
			$arrParam['where'] .= " AND p.IDPEDIDOSTATUS != 1";
			
			$arrDados['pedido'] = $this->pedido_model->lista($arrParam, FALSE);
			
			if (count($arrDados['pedido']) > 0)
			{
				$viewSucess = 0;
				
				$ped = $this->pedido_model->listaLivrosPedidoById($idPedido, TRUE);
				$arrDados['pedido'][0]['QTDTOTAL'] = $ped[0]['QTDTOTAL'];
				$arrDados['pedido'][0]['VALORTOTAL'] = "R$ " . masc_real($arrDados['pedido'][0]['VALORTOTAL']);
				
				$arrDados['pedido'][0]['livros'] = $this->pedido_model->listaLivrosPedidoById($idPedido);
				 
				$tamArr = count($arrDados['pedido'][0]['livros']);
				
				for ($i = 0; $i < $tamArr; $i++)
				{
					// subtotal
					$arrDados['pedido'][0]['livros'][$i]['SUBTOTAL']         = "R$ " . masc_real($arrDados['pedido'][0]['livros'][$i]['QTD'] * $arrDados['pedido'][0]['livros'][$i]['PRECO_UNIT']);
					$arrDados['pedido'][0]['livros'][$i]['PRECO_UNIT']       = "R$ " . masc_real($arrDados['pedido'][0]['livros'][$i]['PRECO_UNIT']);
					// dados do livro
					$arrDados['pedido'][0]['livros'][$i]['DADOSLIVRO']       = '<b>' . $arrDados['pedido'][0]['livros'][$i]['TITULO'] . '</b>'; 
					$arrDados['pedido'][0]['livros'][$i]['DADOSLIVRO']      .= '<br />Edição: ' . ucwords(strtolower($arrDados['pedido'][0]['livros'][$i]['EDICAO']));
					$arrDados['pedido'][0]['livros'][$i]['DADOSLIVRO']      .= ' - ' . ucwords(strtolower($arrDados['pedido'][0]['livros'][$i]['ANO']));
					$arrDados['pedido'][0]['livros'][$i]['DADOSLIVRO']      .= '<br />Autor: ' . ucwords(strtolower($arrDados['pedido'][0]['livros'][$i]['AUTOR']));
					$arrDados['pedido'][0]['livros'][$i]['DADOSLIVRO']      .= '<br />Editora: ' . ucwords(strtolower($arrDados['pedido'][0]['livros'][$i]['RAZAOSOCIAL']));
				}
				
				$arrDados['pedido'][0]['livros'] = add_corLinha($arrDados['pedido'][0]['livros'], 1);
				
                $arrDados['pedido'][0]['historico'] = $this->pedido_model->getHistoricoPedido($idPedido);

				if($acao == 'view' && $arrDados['pedido'][0]['IDPEDIDOSTATUS'] == 2)
				{
					$this->db->trans_start();
					
					$arrParam = Array();
					$arrParam['DATA_ULT_ATU']   = date('Y/m/d H:i:s');
					$arrParam['IDPEDIDOSTATUS'] = 3;
					$table = 'pedido';
					
					$opLog   = 'UPDATE';
					$descLog = 'Visualizou pedido.';
					
					// logar alteração no pedido
					$this->global_model->update($table, $arrParam, 'IDPEDIDO = ' . $idPedido);
					
					$arrParam = Array();
					$arrParam['IDPEDIDO']       = $idPedido;
					$arrParam['IDPEDIDOSTATUS'] = 3;
					$arrParam['IDUSUARIO']      = $this->session->userdata('idUsuario');
					$arrParam['DATAHORA']       = date('Y/m/d H:i:s');
					$table = 'pedidolog';
					
					// logar alteração na pedidologs.
					$this->global_model->insert($table, $arrParam);
					
					$this->db->trans_complete();
					
					if ($this->db->trans_status() === FALSE)
					{
						//salva log
						$this->load->library('logsis', Array($opLog, 'ERRO', $table, $idPedido, $descLog, var_export($arrParam, TRUE)));
					}
					else
					{
						//salva log
						$this->load->library('logsis', Array($opLog, 'OK', $table, $idPedido, $descLog, var_export($arrParam, TRUE)));
					}
				}
				
				if($acao == 'save')
				{
					// finalizar pedido
					
					// importação da nota fiscal
					if($arrDados['pedido'][0]['IDPEDIDOSTATUS'] == 2 || $arrDados['pedido'][0]['IDPEDIDOSTATUS'] == 3)
					{
						$config = Array();
						$config['upload_path']   = './application/doc_pdv/';
						$config['allowed_types'] = 'jpg|jpeg|png';
						$config['max_size']      = '500';
						$config['max_filename']  = '50';
						
						$this->load->library('upload', $config);
						
						// importação da nota fiscal
						if( ! file_exists('./application/doc_pdv/' . md5('nf' . $idPedido) . '.jpg'))
						{
							if( ! $this->upload->do_upload('nota_fiscal'))
							{
								$arrDados['outrosErros']= $this->upload->display_errors();
							}
							else
							{
								$arquivo = $this->upload->data();
								rename("./application/doc_pdv/" . $arquivo['file_name'] ,"./application/doc_pdv/" . md5("nf". $idPedido) . ".jpg");
							}
						}
						
						// importação do canhoto
						if( ! file_exists('./application/doc_pdv/' . md5('ce' . $idPedido) . '.jpg'))
						{
							if( ! $this->upload->do_upload('canhoto_entrega'))
							{
								$arrDados['outrosErros']= $this->upload->display_errors();
							}
							else
							{
								$arquivo2 = $this->upload->data();
								rename("./application/doc_pdv/" . $arquivo2['file_name'] ,"./application/doc_pdv/" . md5("ce". $idPedido) . ".jpg");
							}
						}
						
						if(file_exists('./application/doc_pdv/' . md5('ce' . $idPedido) . '.jpg') && file_exists('./application/doc_pdv/' . md5('nf' . $idPedido) . '.jpg'))
						{
							$this->db->trans_start();
							
							$arrParam = Array();
							$arrParam['DATA_ULT_ATU']   = date('Y/m/d H:i:s');
							$arrParam['IDPEDIDOSTATUS'] = 4;
							$table = 'pedido';
							
							$opLog   = 'UPDATE';
							$descLog = 'Anexou nota fiscal e canhoto de entrega ao pedido.';
							
							// logar alteração no pedido
							$this->global_model->update($table, $arrParam, 'IDPEDIDO = ' . $idPedido);
							
							$arrParam = Array();
							$arrParam['IDPEDIDO']       = $idPedido;
							$arrParam['IDPEDIDOSTATUS'] = 4;
							$arrParam['IDUSUARIO']      = $this->session->userdata('idUsuario');
							$arrParam['DATAHORA']       = date('Y/m/d H:i:s');
							$table = 'pedidolog';
							
							// logar alteração na pedidologs.
							$this->global_model->insert($table, $arrParam);
							
							$this->db->trans_complete();

							if ($this->db->trans_status() === FALSE)
							{
								//salva log
								$this->load->library('logsis', Array($opLog, 'ERRO', $table, $idPedido, $descLog, var_export($arrParam, TRUE)));
							}
							else
							{
								//salva log
								$this->load->library('logsis', Array($opLog, 'OK', $table, $idPedido, $descLog, var_export($arrParam, TRUE)));
								
								$viewSucess = 1;
								
								$arrSucesso['msg']      = 'Pedido finalizado com sucesso!';
								$arrSucesso['link']     = 'pdv/pedidosLivro';
								$arrSucesso['txtlink']  = 'Ir para Pedidos';
								$arrSucesso['tipoUser'] = $this->session->userdata('tipoUsuario');
								$this->load->view('sucesso_view', $arrSucesso);
							}
						}
					}
					else
					{
						$arrDados['outrosErros'] = 'Já foi confirmada a entrega do pedido a biblioteca.';
					}
				}
				
				if ($viewSucess == 0)
				{
					$idstatuspedido = $this->global_model->get_dado("pedido","IDPEDIDOSTATUS","IDPEDIDO = ".$idPedido);
					
					if($idstatuspedido == 3 || $idstatuspedido == 2)
					{
						$arrDados['finalizar'] = TRUE;
					}
					else
					{
						$arrDados['finalizar'] = FALSE;
					}
					
					$this->parser->parse('pdv/pedido_formcad_view', $arrDados);
				}
			}
			else
			{
				$arrErro['msg']      = 'Pedido inválido.';
				$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
				$this->load->view('erro_view', $arrErro);
			}
		}
		else
		{
			$arrErro['msg']      = 'Pedido inválido.';
			$arrErro['tipoUser'] = $this->session->userdata('tipoUsuario');
			$this->load->view('erro_view', $arrErro);
		}
    }
	
	/**
	* modal_dados_pdv()
	* Exibe html de formulario padrao de dados do pdv.
	* @param integer idusuario
	* @return void
	*/
	function modal_dados_pdv($idusuario = 0)
	{
		// Carrega model biblioteca
		$this->load->model('pdv_model');
		$pdv = $this->pdv_model->get_dados_pdv($idusuario);
		
		// Load view
		$this->parser->parse('pdv/modal_dados_pdv', array('pdv' => $pdv));
	}
	
	/**
	* search()
	* Função de entrada/pesquisa do modulo.
	* @return void
	*/
	function search($actual_page = 0)
	{
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Coleta o formulário de filtros 
			$args['filtros'] = $this->input->post();
			
			// Carrega tabela de dados, seta o array de dados e limita a paginação
			$this->load->model('pdv_model');
			$this->load->model('cidade_model');
			$this->load->library('array_table');
			$this->array_table->set_id('module_table');
			$this->array_table->set_data($this->pdv_model->get_lista_modulo($args['filtros']));
			$this->array_table->set_actual_page($actual_page);
			$this->array_table->set_page_link('pdv/search');
			
			// Adiciona colunas à tabela e seta as colunas de exibição
			// $this->array_table->add_column('<a href="' . URL_EXEC . 'pedido/gerencia/' . $this->session->userdata('programa') . '/{0}"><img src="' . URL_IMG . 'icon_controle_pedido.png" title="Gerência do Pedido" /></a>');
			// $this->array_table->add_column('<a href="' . URL_EXEC . 'pedido/conferencia_administrativa_pedido/{0}"><img src="' . URL_IMG . 'icon_conferencia_adm_pedido.png" title="Conferência Administrativa" /></a>');
			// $this->array_table->add_column('<a href="' . URL_EXEC . 'pedido/conferencia/{0}"><img src="' . URL_IMG . 'icon_pedido_conferencia.png" title="Conferência" /></a>');
			$this->array_table->add_column('{ATIVO_IMG}', false);		
			$this->array_table->add_column('<a href="' . URL_EXEC . 'pdv/form_update_pdv/{0}/?url=pdv/search"><img src="' . URL_IMG . 'icon_edit.png" title="Editar" /></a>');
			$this->array_table->add_column('<a href="javascript:void(0);" onclick="ajax_modal(\'Visualizar Dados\', \'' . URL_EXEC . 'pdv/modal_form_view_pdv/{0}\', \'' . URL_IMG . 'icon_view.png\', 600, 800);"><img src="' . URL_IMG . 'icon_view.png" title="Visualizar" /></a>');
			$this->array_table->set_columns(array('#', 'RAZÃO SOCIAL', 'CPF/CNPJ', 'TELEFONE', 'EMAIL', 'RESPONSÁVEL', 'TEL. RESP.', 'EMAIL RESP.', 'LOGIN'));
			
			// Processa tabela do modulo
			$args['module_table'] = $this->array_table->get_html();
			
			// Monta options de ufs
			$args['options_uf'] = monta_options_array($this->pdv_model->get_ufs_pdvs(), get_value($args['filtros'], 'c__iduf'));
			$args['options_cidades'] = monta_options_array($this->cidade_model->get_cidades_by_uf(get_value($args['filtros'], 'c__iduf')), get_value($args['filtros'], 'c__idcidade'));
			
			$this->load->view('pdv/search', $args);
		} else {
			redirect('/home/');
		}
	}
	
	/**
	* download_excel()
	* Download de excel de ponto de venda.
	* @param
	* @return void
	**/
	function download_excel() 
	{
		// Carrega classe excel e model pedido
		$this->load->library('excel');
		$this->load->model('pdv_model');
		
		// Executa download do excel
		$this->excel->set_file_name('PDV_' . date('Y-m-d_His'));
		$this->excel->set_data($this->pdv_model->get_lista_modulo());
		$this->excel->download_file();
	}
	
	/**
	* form_update_pdv()
	* Formulário html de edição de Ponto de vendas.
	* @return void
	*/
	function form_update_pdv($idusuario = 0)
	{
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Carrega classes necessárias para utilização
			$this->load->model('pdv_model');
			
			// Dados da entidade
			$args['dados'] = $this->pdv_model->get_dados_pdv($idusuario);
			
			// Processa url_redirect (para redirecionamento do botão aplicar, ok e voltar do form)
			$args['url_redirect'] = ($this->input->get('url') == '') ? 'pdv/search' : $this->input->get('url');
			
			// Load da camada view
			$this->load->view('pdv/form_update_pdv', $args);
		}
	}
	
	/**
	* form_update_pdv_proccess()
	* Processa formulário de atualização de cadastro de entidade.
	* @return void
	*/
	function form_update_pdv_proccess()
	{
		// Por enquanto, somente administrador poderá acessar esta tela
		if($this->session->userdata('tipoUsuario') == 1)
		{
			// Carrega classes necessárias
			$this->load->library('logsis');
			$this->load->model('global_model');
			
			// Array de dados do cadastro de entidade
			$idusuario = $this->input->post('idusuario');
			$arr_dados['iestadual'] = $this->input->post('iestadual');
			$arr_dados['razaosocial'] = $this->input->post('razaosocial');
			$arr_dados['nomefantasia'] = $this->input->post('nomefantasia');
			$arr_dados['emailgeral'] = $this->input->post('emailgeral');
			$arr_dados['site'] = $this->input->post('site');
			$arr_dados['telefonegeral'] = $this->input->post('telefonegeral');
			$arr_dados['nomeresp'] = $this->input->post('nomeresp');
			$arr_dados['emailresp'] = $this->input->post('emailresp');
			$arr_dados['telefoneresp'] = $this->input->post('telefoneresp');
			$arr_dados['skyperesp'] = $this->input->post('skyperesp');
			$arr_dados['idlogradouro'] = $this->input->post('idlogradouro');
			$arr_dados['end_numero'] = $this->input->post('end_numero');
			$arr_dados['end_complemento'] = $this->input->post('end_complemento');			
			$this->global_model->update('cadpdv', $arr_dados, "IDUSUARIO = $idusuario");
			$this->logsis->insereLog(array('UPDATE', 'OK', 'cadpdv', $this->session->userdata('idUsuario'), 'Atualização de cadastro de Ponto de venda', var_export($this->input->post(), true)));
			
			// Array de dados do cadastro de usuario
			$arr_usuario['cpf_cnpj'] = $this->input->post('cpf_cnpj');
			$arr_usuario['ativo'] = $this->input->post('ativo');
			$arr_usuario['tipopessoa'] = $this->input->post('tipopessoa');
			$this->global_model->update('usuario', $arr_usuario, "IDUSUARIO = $idusuario");
			$this->logsis->insereLog(array('UPDATE', 'OK', 'usuario', $this->session->userdata('idUsuario'), 'Atualização de cadastro de Usuário', var_export($this->input->post(), true)));
			
			// Redirect para formulário
			$action = $this->input->post('redirect_action');
			if($action == 'ok'){ redirect($this->input->post('redirect_ok')); } else {redirect( $this->input->post('redirect_aplicar') . '/?url=' . $this->input->post('redirect_ok')); }
		} else {
			redirect('/home/');
		}
	}
	
	/**
	* modal_form_view_pdv()
	* Exibe html de formulario padrao de dados da Ponto de venda.
	* @param integer idusuario
	* @return void
	*/
	function modal_form_view_pdv($idusuario = 0)
	{
		// Carrega classes necessárias
		$this->load->model('pdv_model');
		$args['dados'] = $this->pdv_model->get_dados_pdv($idusuario);
		
		// Load view
		$this->parser->parse('pdv/modal_form_view_pdv', $args);
	}
}
