<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* pesquisa_agencia_bb
*
* Abstra��o do modulo de Pesquisa de Agencias do banco do Brasil
* 
* @author		Ricardo Neves Becker
* @package		application
* @subpackage	controllers.endere�o
* @since		2012-07-20
*
*/
class Pesquisa_agencia_bb extends CI_Controller {
	
	/**
	* __construct()
	* @return void
	*/
	
	function __construct()
	{
		parent::__construct();
	}
	
	/**
	* search()
	* Monta a tabela com os dados da pequisa de agencias do banco do Brasil
	* @param $actual_page
	*/
	function search($actual_page = 0)
	{
		$this->load->model('pesquisa_agencia_bb_model');
		// Carrega a biblioteca para o grid
		$this->load->library('array_table');
		
		// Coleta o formul�rio de filtros e array de dados da tabela de logradouros
		$args['filtros'] = $this->input->post();		
		
		// Inicializa o filtro sem valores.
		if
		(
			get_value($args['filtros'], 'ag__uf') == '' && 
			get_value($args['filtros'], 'ag__municipio') == '' && 
			get_value($args['filtros'], 'ag__nomeagencia') == '' && 
			get_value($args['filtros'], 'ag__codagenciacompleto') == ''
		){
			$data = array();
		} else {
			$data = $this->pesquisa_agencia_bb_model->get_lista_modulo($args['filtros']);
		}
		
		$this->array_table->set_id('module_table');
		$this->array_table->set_data($data);
		$this->array_table->set_actual_page($actual_page);
		$this->array_table->set_items_per_page(10);
		$this->array_table->set_page_link('pesquisa_agencia_bb/search');
		$this->array_table->add_column('<input type="radio" name="idagencia" id="idagencia" value="{0}|{1}|{2}" >');
		//$this->array_table->add_column('<a href="' . URL_EXEC . 'pesquisa_agencia_bb/form_new_agencia/{0}/"><img src="' . URL_IMG . 'building_add.png" title="Adicionar nova Ag�ncia" /></a>');
		$this->array_table->set_columns(array('#', 'COD. AGENCIA', 'NOMEAGENCIA', 'UF', 'MUNICIPIO', 'BAIRRO', 'LOGRADOURO'));

		// Processa tabela do modulo
		$args['module_table'] = $this->array_table->get_html();
		
		// Monta options de ufs
		$args['options_uf']      = monta_options_array($this->pesquisa_agencia_bb_model->get_all_ufs_agenciabb(), get_value($args['filtros'], 'ag__uf'));
		$args['options_cidades'] = monta_options_array($this->pesquisa_agencia_bb_model->get_all_cidades_by_uf_agenciabb(get_value($args['filtros'], 'ag__uf')), get_value($args['filtros'], 'ag__municipio'));

		$this->load->view('pesquisa_agencia_bb/search', $args);
	}
	
	/**
	* ajax_get_options_cidade_by_uf_bb()
	* M�todo para buscar bairros das cidades
	* @param $actual_page
	*/	
	function ajax_get_options_cidade_by_uf_bb($uf)
	{
		$this->load->model('pesquisa_agencia_bb_model');
		echo monta_options_array($this->pesquisa_agencia_bb_model->get_all_cidades_by_uf_agenciabb($uf));
	}
	
	/**
	* form_new_endereco()
	* M�todo para cadastrar novo logradouro
	* @param $actual_page
	*/
/* 	function form_new_agencia()
	{
		// Carrega a camada model
		$this->load->model('endereco_model');
		
		$args['dados']  	  = $this->endereco_model->get_logradouro_nomes($idlogradouro);
		$args['cidade'] 	  = $args['dados'][0]['CIDADE'];
		$args['estado'] 	  = $args['dados'][0]['ESTADO'];
		$args['cep']    	  = $args['dados'][0]['CEP'];
		$args['bairro'] 	  = $args['dados'][0]['BAIRRO'];
		$args['idlogradouro'] = $idlogradouro;
		
		$args['options_logradourotipo'] = monta_options_array($this->endereco_model->get_logradourotipo());
		
		$this->load->view('endereco/form_new_endereco', $args);
		
	} */
	/**
	* form_new_agencia_process()
	* M�todo para cadastrar novo logradouro
	* @param $actual_page
	*/	
	/* function form_new_endereco_process()
	{		
		$args['dados_formulario'] = $this->input->post();
		
		// Carrega a camada model
		$this->load->model('endereco_model');
		// Carrega a camada model global_model. 
		$this->load->model('global_model');
		// Pega os ids de cidade, bairro, cep.
		$args['dados'] = $this->endereco_model->get_ids_logradouro($this->input->post('idlogradouro'));

		$arr_dados['idcidade'] = $args['dados'][0]['IDCIDADE'];
		$arr_dados['idbairro'] = $args['dados'][0]['IDBAIRRO'];
		$arr_dados['idlogradourotipo'] = $this->input->post('idlogradourotipo');
		$arr_dados['cep'] = $args['dados'][0]['CEP'];
		$arr_dados['nomelogradouro'] = strtoupper($this->input->post('nomelogradouro'));
		$arr_dados['datahora_add'] = date('Y-m-d H:i:s');

		$num_rows = $this->global_model->insert('logradouro', $arr_dados);
		if($num_rows > 0)
		{
			// Retorna o �ltimo Idlogradouro.
			$args['dados_logradouro_lastid'] = $this->endereco_model->get_last_idlogradouro();
			// carrega a camada de visualiza��o.
			$this->load->view('endereco/form_new_endereco_sucess', $args);
		}		
	} */
}
