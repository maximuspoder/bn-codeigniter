<script language="javascript">
	$("motivo").focus();
</script>
<div id="container" style="overflow-y: auto; ">
    <div class="main_content_left">
        <form name="form_cad" id="form_cad" method="post" action="javascript:if(validaForm_GravaLogBloqueio()){ submitGravaLogBloqueio(<?php echo($idbiblioteca)?>); }">
            <fieldset style="width:540px; margin-left:20px;height:100%;">
                <legend><b>Bloqueia Biblioteca</b></legend>
                <div id="fs_div">
					<div style="width: 500px; display:inline-block !important;margin:0 0 0 20px;">
						<b>Blblioteca:</b> {nome}
					</div>
					<div style="width: 500px; display:inline-block !important;margin:0 0 0 20px;">
						<b>Cidade:</b> {cidade}
					</div>
					<div style="width: 500px; display:inline-block !important;margin:0 0 0 20px;">
						<b>IMPORTANTE:</b> Ao bloquear este registro, você estará impedindo que o usuário correspondente entre no sistema. Esta ação não poderá ser desfeita. 
					</div>
					<div align="center" style="width: 500px; display:inline-block !important;margin:10px 0 0 0;">
						<label for="motivo">Motivo do bloqueio (aparecerá para o usuário quando ele tentar entrar no sistema):</label><br />
						<textarea name="motivo" id="motivo" style="width:450px; height: 120px;" onfocus="focusLiga(this)" onblur="focusDesliga(this)"></textarea>
					</div>
					<div align="center" style="width: 500px; display:inline-block !important;margin:10px 0 0 0">
						<input class="buttonPadrao" type="button" value="  Cancelar  " onclick="$('ligthbox_bg').setStyle({display : 'none'});" />
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input class="buttonPadrao" id="btnSaveForm" name="btnSaveForm" type="button" value="  Salvar  " onclick="if(validaForm_GravaLogBloqueio()){ submitGravaLogBloqueio(<?php echo($idbiblioteca)?>); }"/>
					</div>
                </div>
            </fieldset>
            <input type="hidden" name="origem" id="origem" value="{origem}" />
            <input type="hidden" name="nPag" id="nPag" value="{nPag}" />
        </form>
    </div>
</div><!-- #container-->

