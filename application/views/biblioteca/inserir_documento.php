<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			// Validação e máscaras
			$("#form_default").validationEngine({ inlineValidation:false , promptPosition : "centerRight", scroll : false });
			
			// Attacha função no onsubmit do form
			$('#form_default').submit(function(){
				$('#form_default').attr('target', 'form_target_iframe'); // 'form_target_iframe' is the name of the iframe
			});
		});
	</script>
	<!--
    <script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>biblioteca.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
	-->
</head>
<body>
	<div>Utilize o formulário abaixo para inserir um novo documento ao pedido referenciado. Campos com (*) são obrigatórios.</div>
	<br />
	<form action="<?php echo URL_EXEC; ?>pedido/inserir_documento_proccess" name="form_default" id="form_default" enctype="multipart/form-data" method="post">
		<input type="hidden" name="idpedido" id="idpedido" value="<?php echo($idpedido);?>" />
		<div class="form_label">*Tipo DOC:</div>
		<div class="form_field"><select name="tipo_doc" id="tipo_doc" style="width:162px;"><?php //echo($options_doc);?></select></div>
		<br />
		<div class="form_label">*Número DOC:</div>
		<div class="form_field"><input type="text" name="numero_doc" id="numero_doc" style="width:150px;"  /></div>
		<br />
		<div class="form_label">*Valor DOC:</div>
		<div class="form_field"><input type="text" name="valor_doc" id="valor_doc" style="width:150px;"  /></div>
		<br />
		<div class="form_label">*Arquivo:</div>
		<div class="form_field"><input type="file" name="arquivo" id="arquivo" style="width:280px;" /></div>
		<div style="margin-top:30px">
			<hr />
			<div class="inline top"><input type="submit" value="Enviar" /></div>
			<div class="inline middle" style="padding:7px 0 0 5px">ou <a href="javascript:void(0)" onclick="$('#form_default').validationEngine('hide');close_modal();">cancelar</a></div>
		</div>
	</form>
	<iframe src="" id="form_target_iframe" name="form_target_iframe">ss</iframe>
</body>
</html>