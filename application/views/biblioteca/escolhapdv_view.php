<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" /> <!-- mapas -->
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>biblioteca.js" type="text/javascript"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&region=BR"></script> <!-- mapas -->
	<script src="<?php echo URL_JS; ?>mapas.js" type="text/javascript"></script> <!-- mapas -->
	<script language="javascript">
		window.onpageshow = function(evt){ $('btnSavePP').disabled = false; }
	</script>
</head>
<body onload="initialize()">
	<div id="wrapper">
		<?php monta_header(1); ?>
		<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
		<?php add_elementos_CONFIG(); ?>
		
		<div class="divtitulo">
			<div class="divtitulo2">
				>> ESCOLHER PONTO DE VENDA PARCEIRO
			</div>
		</div>
		
		<div id="middle">
			<div id="container" style="text-align:center;">
				<div class="main_content_center" style="margin-top:5px;margin-bottom:10px;">
					<table style="width:100%;">
						<tr>
							<td style="width:670px;">
								<center><div id="map_canvas" style="width:650px; height:400px;"></div></center>
							</td>
							<td style="text-align:left; vertical-align:top;">
								<img src="<?php echo URL_IMG; ?>info.png" style="padding-right:10px;" /> <b>ATENÇÃO!</b><br /><br />
								Localize o seu Ponto de Venda Parceiro na lista abaixo. Uma vez localizado, clique em <b>"Selecionar"</b> na linha desejada. Após escolhido e salvo, não é possível alterá-lo.
								<br /><br />
								{mensagem}
								<br /><br />
								<form name="form_cad" id="form_cad" method="post" action="<?php echo URL_EXEC; ?>biblioteca/escolhapdv/setRaio" onsubmit="return validaForm_setRaio()">
									Desejo visualizar todos os estabelecimentos cadastrados em um raio de <input class="inputText" type="text" name="raio" id="raio" style="width:50px;" maxlength="4" onfocus="focusLiga(this)" onblur="focusDesliga(this)" autocomplete="off" onkeypress="return maskInteger(this, event)" value="{raio}" /> Km a partir da minha biblioteca.
									<input class="buttonPadrao" id="btnSetForm" name="btnSetForm" type="submit"  style="height:17px;" value="Visualizar" />
								</form>
								<br /><br />
								<div id="divPdvSel" style="background-color:#EFEFEF; padding:10px;margin-right:10px; display:none;">
									<img src="<?php echo URL_IMG; ?>marker.png" style="float:left;" /> <div style="margin-top:10px;"><b>Ponto de Venda selecionado:</b></div><br />
									<span id="npdv">NOME DO ESTABELECIMENTO . COM</span><br />
									<a id="linkverSel" href="" target="_blank">visualizar informações</a>
									<br /><br />
									<input type="button" value="Salvar" class="buttonPadrao" id="btnSavePP" style="width:100px;" onclick="savePdvParc()" />
								</div>
							</td>
						</tr>
					</table>
				</div>
				
				<input type="hidden" name="txtpdv" id="txtpdv" value="{txtpdv}" />
				<input type="hidden" name="psel" id="psel" value="0" />
				
				<div class="main_content_center" style="margin-top:5px;margin-bottom:20px;">
					<table cellspacing="0" align="center" cellpadding="0" class="extensions t95" style="margin-top:30px;">
						<thead>
							<tr class="header">
								<th>Ponto de Venda</th>
								<th>Endereço</th>
								<th>Bairro</th>
								<th>Cidade</th>
								<th>UF</th>
								<th>Visualizar</th>
								<th class="w100">Selecionar</th>
							</tr>
							<tr class="header-separator">
								<td colspan="7">&nbsp;</td>
							</tr>
						</thead>
						<tbody>
							{enderecos}
							<tr class="{CORLINHA}">
								<td class="no-wrap pr_20 tdleft">{RAZAOSOCIAL}</td>
								<td class="no-wrap pr_20 tdleft">{NOMELOGRADOURO2} {END_NUMERO} {END_COMPLEMENTO}</td>
								<td class="no-wrap pr_20 tdleft">{NOMEBAIRRO}</td>
								<td class="no-wrap pr_20 tdleft">{NOMECIDADESUB}</td>
								<td class="no-wrap tdcenter">{IDUF}</td>
								<td class="no-wrap tdcenter">
									<a class="red" href="javascript:void(0);" onclick="zoomPdv('{LATITUDE}','{LONGITUDE}')">Mapa</a>
								</td>
								<td class="w100 pr_20 tdleft">
									<a class="red" href="javascript:void(0);" onclick="selPdvNew('{LATITUDE}','{LONGITUDE}','{RAZAOSOCIAL}',{IDUSUARIO})">Selecionar</a>
								</td>
							</tr>
							{/enderecos}
							{enderecos2}
							<tr class="{CORLINHA}">
								<td class="no-wrap pr_20 tdleft">{RAZAOSOCIAL}</td>
								<td class="no-wrap pr_20 tdleft">{NOMELOGRADOURO2} {END_NUMERO} {END_COMPLEMENTO}</td>
								<td class="no-wrap pr_20 tdleft">{NOMEBAIRRO}</td>
								<td class="no-wrap pr_20 tdleft">{NOMECIDADESUB}</td>
								<td class="no-wrap tdcenter">{IDUF}</td>
								<td class="no-wrap tdcenter">
									&nbsp;
								</td>
								<td class="w100 pr_20 tdleft">
									<a class="red" href="javascript:void(0);" onclick="selPdvNew(0,0,'{RAZAOSOCIAL}',{IDUSUARIO})">Selecionar</a>
								</td>
							</tr>
							{/enderecos2}
						</tbody>
					</table>
				</div>
			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
</body>
</html>