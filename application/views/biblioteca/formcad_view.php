<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title><?php echo TITLE_SISTEMA; ?></title>
        <link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
        <script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
        <script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
        <script src="<?php echo URL_JS; ?>autocomplete.js" type="text/javascript"></script>
        <script src="<?php echo URL_JS; ?>biblioteca.js" type="text/javascript"></script>
        <script src="<?php echo URL_JS; ?>cep.js" type="text/javascript"></script>
        <script src="<?php echo URL_JS; ?>powercombo.js" type="text/javascript"></script>
        <script language="javascript">
            window.onpageshow = function(evt){ $('btnSaveForm').disabled = false; }
        </script>
    </head>
    <body>
        <div id="wrapper">
            <?php
            add_elementos_CONFIG();
            add_div_CEP();
            monta_header(1);
            monta_menu($this->session->userdata('tipoUsuario'));
            add_div_ligthbox('660', '270');
            ?>
            <div id="middle">
                <div id="container">
                    <div class="main_content_left" style="margin-top:10px; margin-bottom:20px;">
                        <?php
                        $erros = Array();

                        if (isset($outrosErros)) {
                            if (is_array($outrosErros)) {
                                for ($i = 0; $i < count($outrosErros); $i++) {
                                    array_push($erros, $outrosErros[$i]);
                                }
                            } else {
                                array_push($erros, $outrosErros);
                            }
                        }

                        exibe_validacao_erros($erros);
                        ?>
                        <form name="form_cad" id="form_cad" method="post" action="<?php echo URL_EXEC; ?>biblioteca/respFinanceiroFormcad/save" onsubmit="return validaForm_BibliotecaFinan()">
                            <fieldset style="width:680px; margin-left:20px;">
                                <legend><b>Cadastro de Responsável Financeiro</b></legend>
                                <div id="fs_div">
                                    <table id="tabelaCadastros2" align="center" border="0">
                                        <tr>
                                            <td colspan="4" class="areaCadastro2"><img src="<?php echo URL_IMG; ?>geral.png" style="padding-right:10px;" /> Dados gerais</td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top:10px;">CPF*:</td>
                                            <td style="padding-top:10px; text-align: left;" colspan="3">
                                                <input class="inputText" type="text" name="cpf" id="cpf" style="width:100px;" maxlength="14" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('cpf', $cpf); ?>" autocomplete="off" onkeypress="return maskInteger(this, event)" onKeyUp="maskCpf(this)" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Nome*:</td>
                                            <td colspan="3"><input class="inputText" type="text" name="nome" id="nome" style="width:450px;" maxlength="250" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('nome', $nome); ?>" autocomplete="off" /></td>
                                        </tr>
                                        <tr>
                                            <td>Nome da Mãe*:</td>
                                            <td colspan="3">
                                                <input class="inputText" type="text" name="nomemae" id="nomemae" style="width: 450px;" maxlength="250" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('nomemae', $nomemae); ?>" autocomplete="off" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Telefone*:</td>
                                            <td colspan="3">
                                                <input class="inputText" type="text" name="telefone" id="telefone" style="width:100px;" maxlength="14" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('telefone', $telefone); ?>" autocomplete="off" onKeyUp="maskx(this, msk_telefone)" /> 
                                                <span style="font-size:11px;">(xx) xxxx.xxxx</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Data de Nascimento*:</td>
                                            <td colspan="3">
                                                <input class="inputText" type="text" name="dataNasc" id="dataNasc" style="width: 100px;" maxlength="10" onfocus="focusLiga(this)" onblur="focusDesliga(this)" onkeypress="return maskx(this, msk_date)" value="<?php echo set_value('dataNasc', $dataNasc); ?>" autocomplete="off" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Login*:</td>
                                            <td colspan="3">
                                                <?php if ($login == '') { ?>
                                                    <input class="inputText" type="text" name="login" id="login" style="text-transform:none; width: 100px;" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)"  value="<?php echo set_value('login', $login); ?>" autocomplete="off" />
                                                <?php } else { ?>
                                                    <input class="inputText inputDisab" readonly="readonly" type="text" name="login" id="login" style="text-transform:none; width:100px;" value="<?php echo set_value('login', $login); ?>" autocomplete="off" />
                                                <?php } ?>
                                                <span style="font-size:11px;">(nome que será usado para acessar o sistema)</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>E-mail*:</td>
                                            <td colspan="3">
                                                <input class="inputText" type="text" name="email" id="email" style="text-transform:none; width: 450px;" maxlength="250" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('email', $email); ?>" autocomplete="off" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Confirmação E-mail*:</td>
                                            <td colspan="3">
                                                <input class="inputText" type="text" name="email2" id="email2" style="text-transform:none; width: 450px;" maxlength="250" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('email2', $email2); ?>" autocomplete="off" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" class="areaCadastro2" style="padding-top:10px;"><img src="<?php echo URL_IMG; ?>catalog.png" style="padding-right:10px;" /> Dados Bancários</td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top:10px;">Agência Banco do Brasil*:</td>
                                            <td style="padding-top:10px;" colspan="3">
                                                <input type="hidden" name="agenciabb" id="agenciabb" style="text-transform:none; width:0px;" readonly="true" maxlength="5" value="<?php echo set_value('agenciabb', $agenciabb); ?>" autocomplete="off" />
                                                <input class="inputText" type="text" name="agenciabb_nome" id="agenciabb_nome" style="text-transform:none; width:150px;" readonly="true" maxlength="5" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('agenciabb_nome', $agenciabb_nome); ?>" autocomplete="off" />
                                                <?php if ($boolPeriodoEdicao && $isFinanceiro) { ?>
                                                    &nbsp;&nbsp;<button onclick="modalAssociarAgenciaBB('agenciabb', 'ajaxGetNomeAgencia($(\'agenciabb\').value, \'agenciabb_nome\');');return false;" class="buttonPadrao">Pesquisar...</button>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" class="areaCadastro2" style="padding-top:10px;"><img src="<?php echo URL_IMG; ?>endereco.png" style="padding-right:10px;" /> Endereço do Responsável Financeiro</td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top:10px;">CEP*:</td>
                                            <td style="padding-top:10px;" colspan="3">
                                                <input type="hidden" name="idlogradouro" id="idlogradouro" value="<?php echo set_value('idlogradouro', $idlogradouro); ?>" />
                                                <input class="inputText" type="text" name="cep" id="cep" style="width:80px;" maxlength="8" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('cep', $cep); ?>" autocomplete="off" onkeypress="return maskInteger(this, event)" />
                                                <img alt="Pesquisar CEP" title="Pesquisar CEP" src="<?php echo URL_IMG; ?>pesq1.gif" style="cursor:pointer;" onclick="pesquisaCep('cep','cep#idlogradouro#uf#cidade#bairro#logradouro#logcomplemento#end_numero');" />
                                                <img alt="Limpar endereço" title="Limpar endereço" src="<?php echo URL_IMG; ?>x2.gif" style="cursor:pointer;" onclick="apagaCep('cep','cep#idlogradouro#uf#cidade#bairro#logradouro#logcomplemento');" />
                                                &nbsp;&nbsp;<b>ATENÇÃO:</b> <i>preencha o CEP e clique no botão</i> <img alt="Pesquisar CEP" title="Pesquisar CEP" src="<?php echo URL_IMG; ?>pesq1.gif" style="cursor:pointer;" onclick="pesquisaCep('cep','cep#idlogradouro#uf#cidade#bairro#logradouro#logcomplemento#end_numero');" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>UF:</td>
                                            <td colspan="3">
                                                <input class="inputText inputDisab" readonly="readonly" type="text" name="uf" id="uf" style="width:40px;" value="<?php echo set_value('uf', $uf); ?>" autocomplete="off" />
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                Cidade:
                                                <input class="inputText inputDisab" readonly="readonly" type="text" name="cidade" id="cidade" style="width:300px;" value="<?php echo set_value('cidade', $cidade); ?>" autocomplete="off" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Bairro:</td>
                                            <td colspan="3">
                                                <input class="inputText inputDisab" readonly="readonly" type="text" name="bairro" id="bairro" style="width:410px;" value="<?php echo set_value('bairro', $bairro); ?>" autocomplete="off" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Logradouro:</td>
                                            <td colspan="3">
                                                <input class="inputText inputDisab" readonly="readonly" type="text" name="logradouro" id="logradouro" style="width:410px;" value="<?php echo set_value('logradouro', $logradouro); ?>" autocomplete="off" />
                                                <input class="inputText inputDisab" readonly="readonly" type="hidden" name="logcomplemento" id="logcomplemento" size="20" value="<?php echo set_value('logcomplemento', $logcomplemento); ?>" autocomplete="off" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Número*:</td>
                                            <td colspan="3">
                                                <input class="inputText" type="text" name="end_numero" id="end_numero" style="width:93px;" maxlength="8" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('end_numero', $end_numero); ?>" autocomplete="off" />
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                Complemento:
                                                <input class="inputText" type="text" name="end_complemento" id="end_complemento" style="width:210px;" maxlength="30" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('end_complemento', $end_complemento); ?>" autocomplete="off" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="text-align: center; padding-top:15px;">
                                                <input class="buttonPadrao" type="button" value="  Voltar  " onclick="history.back();" />
                                                <?php
                                                if ($boolPeriodoEdicao && $isFinanceiro) {
                                                    ?>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <input class="buttonPadrao" id="btnSaveForm" name="btnSaveForm" type="submit" value="  Salvar  " />
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="text-align: center; padding-top:5px; font-size:11px;">
                                                (*) Campos de preenchimento obrigatório.
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div><!-- #container-->
            </div><!-- #middle-->
        </div><!-- #wrapper -->
        <?php monta_footer(); ?>
    </body>
</html>