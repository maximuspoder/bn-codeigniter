<table cellspacing="0" align="center" cellpadding="0" class="extensions" style="width:100%">
	<thead>
		<tr class="header">
			<th>Cod. Agência</th>
			<th>Nome Agência</th>
			<th class="w100">Logradouro</th>
			<th></th>
		</tr>
		<tr class="header-separator">
			<td colspan="4">&nbsp;</td>
		</tr>
	</thead>
	<tbody>
		<?php foreach($agencias as $agencia) {?>
		<tr class="{CORLINHA}">
			<td class="tdcenter"><?php echo($agencia['CODAGENCIACOMPLETO']); ?></td>
			<td class="tdleft"><?php echo($agencia['NOMEAGENCIA']); ?></td>
			<td class="w100 tdleft"><?php echo($agencia['LOGRADOURO']); ?></td>
			<td class="tdcenter"><button class="buttonPadrao" onclick="setAgencia('<?php echo($agencia['CODAGENCIACOMPLETO']); ?> - <?php echo($agencia['NOMEAGENCIA']); ?>', '<?php echo($agencia['IDAGENCIA']); ?>');">Selecionar</button></td>
		</tr>
		<?php } ?>
	</tbody>
</table>