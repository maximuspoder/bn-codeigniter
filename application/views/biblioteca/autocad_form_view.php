<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title><?php echo TITLE_SISTEMA; ?></title>
        <link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
        <script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
        <script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
        <script src="<?php echo URL_JS; ?>autocad.js" type="text/javascript"></script>
        <script src="<?php echo URL_JS; ?>cep.js" type="text/javascript"></script>
        <script src="<?php echo URL_JS; ?>powercombo.js" type="text/javascript"></script>
        <script language="javascript">
            window.onpageshow = function(evt){ $('btnSaveForm').disabled = false; }
			
			function esconde(elem)
		   {
			obj = document.getElementById(elem);
			/*obj.style.visibility = "hidden";*/
			obj.style.display = "none";
			obj.disabled = true;
		   }

		   function mostra(elem)
		   {
			obj = document.getElementById(elem);		   
			/*obj.style.visibility = "visible"; */
			obj.style.display = "block";
			obj.disabled = false;
		   }
        </script>
    </head>
    <body onload="biblioteca_onload();">
        <div id="wrapper">
            <?php
            add_elementos_CONFIG();
            add_div_CEP();
            monta_header(0);
            ?>
            <div id="middle">
                <div id="container" style="text-align:center;">
                    <div class="main_content_center" style="margin-top:10px; margin-bottom:20px;">
                        <?php
                        $erros = Array();

                        if (isset($outrosErros)) {
                            if (is_array($outrosErros)) {
                                for ($i = 0; $i < count($outrosErros); $i++) {
                                    array_push($erros, $outrosErros[$i]);
                                }
                            } else {
                                array_push($erros, $outrosErros);
                            }
                        }

                        exibe_validacao_erros($erros);
                        ?>
                        <form name="form_cad" id="form_cad" method="post" action="<?php echo URL_EXEC; ?>autocad/biblioteca/save<?php echo $migrar == 1 ? '/PJ/0/0/1' : ''; ?>" onsubmit="return validaForm_cadastroBiblioteca(<?php echo $fromSniic;
                        echo isset($migrar) == true ? ',' . $migrar : ''; ?>)">
                            <div style="text-align: left; margin-left: 20px; width: 730px;">
                        <?php
                        if (isset($migrar)) {
                            if ($migrar != 1) {
                                ?>
                                        <center>
                                            <img src="<?php echo URL_IMG; ?>atencao.png" /><br/>
                                            <b style="font-size: 14px;" >Atenção:</b><br/>

                                            O Cadastramento de Bibliotecas nesta data já não permite a participação no <i>Edital de Aquisição de Livros de Baixo Preço</i>.<br/>
                                            Faça o seu cadastramento agora e facilite sua participação em novos programas e ações da FBN!

                                        </center>
								<?php
							}
						}
						?>
                            </div>
                            <br/>
                            <fieldset style="width:730px; margin-left:20px;">
                                <legend><b>Cadastro de Biblioteca</b></legend>
                                <div id="fs_div">
                                    <table id="tabelaCadastros" align="center" border="0">
									<?php
									if (isset($migrar)) {
										if ($migrar == 1) {
											if (strlen($options_cadbib) > 0) {
												?>
                                                    <tr>
                                                        <td colspan="4" class="areaCadastro2"><img src="<?php echo URL_IMG; ?>library.png" style="padding-right:10px;" /> Selecione uma biblioteca correspondente no cadastro novo</td>
                                                    </tr>
                                                    <tr id="linhasniic">
                                                        <td>Bilbioteca:</td>
                                                        <td colspan="3">
                                                            <select class="select" name="idbiblioteca" id="idbiblioteca" style="width:480px;" onchange="ajaxGetDadosBiblioteca(this.value);">
															<?php echo $options_cadbib; ?>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td colspan="3" style="padding-bottom: 20px;">
                                                            <input type="checkbox" name="habilitado" id="habilitado" value="S" checked="checked" />
                                                            Habilitar biblioteca ao associar
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" style="padding-bottom: 15px;"><i>Ou preencha os dados abaixo (um novo cadastro será gerado):</i></td>
                                                    </tr>
												<?php
											}
										}
									}
									?>
                                        <tr>
                                            <td colspan="4" class="areaCadastro2"><img src="<?php echo URL_IMG; ?>geral.png" style="padding-right:10px;" /> Dados gerais</td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top:10px;"></td>
                                            <td style="padding-top:10px; text-align: right;" colspan="3">
                                                <input type="radio" name="tipopessoa" id="tipopessoa" <?php if ($fromSniic == 1) {
                                            echo 'onclick="setTipoBiblioteca(\'PJ\')"';
                                        } else {
                                            echo ($tipopessoa_aux != 'PJ' ? 'disabled="disabled"' : '');
                                        } ?> value="PJ" <?php echo set_radio('tipopessoa', 'PJ'); ?> /> Pessoa Jurídica<img src="<?php echo URL_IMG; ?>questIcon.jpg" title="Entidade coletiva com existência e responsabilidade jurídicas, legalmente organizada. Classifica-se como pessoa jurídica de direito público ou pessoa jurídica de direito privado." style="padding-right:10px;cursor:help; width:20px" />
                                                <input type="radio" name="tipopessoa" id="tipopessoa" <?php if ($fromSniic == 1) {
                                            echo 'onclick="setTipoBiblioteca(\'PF\')"';
                                        } else {
                                            echo ($tipopessoa_aux != 'PF' ? 'disabled="disabled"' : '');
                                        } ?> value="PF" <?php echo set_radio('tipopessoa', 'PF'); ?> /> Pessoa Física<img src="<?php echo URL_IMG; ?>questIcon.jpg" title="É a pessoa natural; indivíduo dotado de direitos e obrigações." style="padding-right:10px;cursor:help; width:20px" /> 
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <span id="spantipopessoa">CNPJ</span>*: 
                                                <input class="inputText" type="text" name="cnpjcpf" id="cnpjcpf" style="width:130px;" maxlength="18" <?php if ($fromSniic == 0) {
                                            echo 'readonly="readonly"';
                                        } ?> onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('cnpjcpf', $cnpjcpf); ?>" autocomplete="off" onkeypress="return maskInteger(this, event)" onKeyUp="maskCnpj(this)" />
                                            </td>
                                        </tr>
                                        <tr id="linhaie">
                                            <td  style="cursor:help" title="Campo não aplicável a biblioteca">Inscrição Estadual:</td>
                                            <td colspan="3"><input class="inputText" type="text" name="iestadual" id="iestadual" style="width:170px;" maxlength="20" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('iestadual', $iestadual); ?>" autocomplete="off" /><img src="<?php echo URL_IMG; ?>questIcon.jpg" title="Campo não aplicável a biblioteca" style="padding-right:10px;cursor:help; width:20px" /></td>											
                                        </tr>
                                        <tr>
                                            <td>Nome da Biblioteca*:</td>
                                            <td colspan="3"><input class="inputText" type="text" name="razaosocial" id="razaosocial" style="width:475px;" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('razaosocial', $razaosocial); ?>" autocomplete="off" /><img src="<?php echo URL_IMG; ?>questIcon.jpg" title="Campo não aplicável a biblioteca" style="padding-right:10px;cursor:help; width:20px" /></td>
                                        </tr>
                                        <tr id="linhanf">
                                            <td style="cursor:help" title="Nome pelo qual uma empresa é reconhecida no mercado.">Nome Fantasia:</td>
                                            <td colspan="3"><input class="inputText" type="text" name="nomefantasia" id="nomefantasia" style="width:475px;" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('nomefantasia', $nomefantasia); ?>" autocomplete="off" /><img src="<?php echo URL_IMG; ?>questIcon.jpg" title="Campo não aplicável a biblioteca" style="padding-right:10px;cursor:help; width:20px" /></td>
                                        </tr>
                                        <tr>
                                            <td style="cursor:help" title="Relação de uma instituição com uma grande categoria jurídica, na qual pode ser classificada.">Natureza Jurídica*:</td>
                                            <td colspan="3">
                                                <select class="select" name="naturezajur" id="naturezajur" style="width:480px;">
                                                    <option value="0" <?php echo set_select('naturezajur', 0); ?> >Selecione</option>
                                                    <?php
                                                    $auxPai = 0;
                                                    foreach ($natjur as $row) {
                                                        if ($row['IDPAI'] != $auxPai) {
                                                            if ($auxPai != 0) {
                                                                echo '</optgroup>';
                                                            }

                                                            echo '<optgroup label="' . $row['NOMEPAI'] . '">';

                                                            $auxPai = $row['IDPAI'];
                                                        }

                                                        echo '<option value="' . $row['IDNATUREZAJUR'] . '" ' . set_select('naturezajur', $row['IDNATUREZAJUR']) . ' >' . $row['NOMENATUREZAJUR'] . '</option>';
                                                    }
                                                    echo '</optgroup>';
                                                    ?>
                                                </select>
                                            <img src="<?php echo URL_IMG; ?>questIcon.jpg" title="Relação de uma instituição com uma grande categoria jurídica, na qual pode ser classificada." style="padding-right:10px;cursor:help; width:20px" /></td>
                                        </tr>
                                        <tr>
                                            <td>Telefone Geral*:</td>
                                            <td colspan="3">
                                                <input class="inputText" type="text" name="telefone1" id="telefone1" style="width:130px;" maxlength="14" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('telefone1', $telefone1); ?>" autocomplete="off" onKeyUp="maskx(this, msk_telefone)" /> 
                                                <span style="font-size:11px;">(xx) xxxx.xxxx</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>E-mail Geral*:</td>
                                            <td colspan="3"><input class="inputText" type="text" name="email" id="email" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('email', $email); ?>" autocomplete="off" style="text-transform:none;width:323px;" /></td>
                                        </tr>
                                        <tr>
                                            <td>Site:</td>
                                            <td colspan="3"><input class="inputText" type="text" name="site" id="site" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('site', $site); ?>" autocomplete="off" style="text-transform:none;width:323px;" /></td>
                                        </tr>
                                        <tr>
                                            <td>Login*:</td>
                                            <td colspan="3">
                                                <input class="inputText" type="text" name="login" id="login" maxlength="20" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('login', $login); ?>" autocomplete="off" style="text-transform:none;width:130px;" /> 
                                                <span style="font-size:11px;">(nome que será usado para acessar o sistema)</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" class="areaCadastro2" style="padding-top:10px;"><img src="<?php echo URL_IMG; ?>endereco.png" style="padding-right:10px;" /> Endereço</td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top:10px;">CEP*:</td>
                                            <td style="padding-top:10px;" colspan="3">
                                                <input type="hidden" name="idlogradouro" id="idlogradouro" value="{idlogradouro}<?php echo set_value('idlogradouro'); ?>" />
                                                <input class="inputText" type="text" name="cep" id="cep" style="width:80px;" maxlength="8" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('cep', $cep); ?>" autocomplete="off" onkeypress="return maskInteger(this, event)" />
                                                <img alt="Pesquisar CEP" title="Pesquisar CEP" src="<?php echo URL_IMG; ?>pesq1.gif" style="cursor:pointer;" onclick="pesquisaCep('cep','cep#idlogradouro#uf#cidade#bairro#logradouro#logcomplemento#end_numero');" />
                                                <img alt="Limpar endereço" title="Limpar endereço" src="<?php echo URL_IMG; ?>x2.gif" style="cursor:pointer;" onclick="apagaCep('cep','cep#idlogradouro#uf#cidade#bairro#logradouro#logcomplemento');" />
                                                &nbsp;&nbsp;<b>ATENÇÃO:</b> <i>preencha o CEP e clique no botão</i> <img alt="Pesquisar CEP" title="Pesquisar CEP" src="<?php echo URL_IMG; ?>pesq1.gif" style="cursor:pointer;" onclick="pesquisaCep('cep','cep#idlogradouro#uf#cidade#bairro#logradouro#logcomplemento#end_numero');" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>UF:</td>
                                            <td colspan="3">
                                                <input class="inputText inputDisab" readonly="readonly" type="text" name="uf" id="uf" style="width:40px;" value="{uf}<?php echo set_value('uf'); ?>" autocomplete="off" />
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                Cidade:
                                                <input class="inputText inputDisab" readonly="readonly" type="text" name="cidade" id="cidade" style="width:300px;" value="{cidade}<?php echo set_value('cidade'); ?>" autocomplete="off" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Bairro:</td>
                                            <td colspan="3">
                                                <input class="inputText inputDisab" readonly="readonly" type="text" name="bairro" id="bairro" style="width:410px;" value="{bairro}<?php echo set_value('bairro'); ?>" autocomplete="off" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Logradouro:</td>
                                            <td colspan="3">
                                                <input class="inputText inputDisab" readonly="readonly" type="text" name="logradouro" id="logradouro" style="width:410px;" value="{logradouro}<?php echo set_value('logradouro'); ?>" autocomplete="off" />
                                                <input class="inputText inputDisab" readonly="readonly" type="hidden" name="logcomplemento" id="logcomplemento" size="20" value="{logcomplemento}<?php echo set_value('logcomplemento'); ?>" autocomplete="off" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Número*:</td>
                                            <td colspan="3">
                                                <input class="inputText" type="text" name="end_numero" id="end_numero" style="width:93px;" maxlength="8" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('end_numero', $end_numero); ?>" autocomplete="off" />
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                Complemento:
                                                <input class="inputText" type="text" name="end_complemento" id="end_complemento" style="width:210px;" maxlength="30" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('end_complemento', $end_complemento); ?>" autocomplete="off" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td title="Nome pelo qual uma empresa é reconhecida no mercado."colspan="4" class="areaCadastro2" style="padding-top:15px;cursor:help"><img src="<?php echo URL_IMG; ?>resp.png" style="padding-right:10px;" />Dirigente da Biblioteca</td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top:10px;">Nome*:</td>
                                            <td style="padding-top:10px;" colspan="3"><input class="inputText" type="text" name="nomeresp" id="nomeresp" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('nomeresp', $nomeresp); ?>" autocomplete="off" style="width:414px;" /></td>
                                        </tr>
                                        <tr>
                                            <td>Telefone*:</td>
                                            <td colspan="3">
                                                <input class="inputText" type="text" name="telefoneresp" id="telefoneresp" style="width:130px;" maxlength="14" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('telefoneresp', $telefoneresp); ?>" autocomplete="off" onKeyUp="maskx(this, msk_telefone)" /> 
                                                <span style="font-size:11px;">(xx) xxxx.xxxx</span>
                                                &nbsp;&nbsp;&nbsp;
                                                Skype: 
                                                <input class="inputText" type="text" name="skyperesp" id="skyperesp" maxlength="30" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('skyperesp', $skyperesp); ?>" autocomplete="off" style="text-transform:none;width:140px;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>E-mail*:</td>
                                            <td colspan="3"><input class="inputText" type="text" name="emailresp" id="emailresp" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('emailresp', $emailresp); ?>" autocomplete="off" style="text-transform:none;width:323px;" /></td>
                                        </tr>
                                        <tr>
                                            <td>Confirma E-mail*:</td>
                                            <td colspan="3"><input class="inputText" type="text" name="emailresp2" id="emailresp2" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('emailresp2', $emailresp2); ?>" autocomplete="off" style="text-transform:none;width:323px;" /></td>
                                        </tr>

                                        <?php
                                        if (isset($migrar)) {
                                            if ($migrar == 1) {
                                                ?>
                                                <tr>
                                                    <td colspan="4" class="areaCadastro2" style="padding-top:15px;"><img src="<?php echo URL_IMG; ?>resp.png" style="padding-right:10px;" /> Observação </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-top:10px;">Motivo da Migração*:</td>
                                                    <td style="padding-top:10px;" colspan="3">
                                                        <textarea class="inputText" name="observacao" id="observacao" onfocus="focusLiga(this)" onblur="focusDesliga(this)" style="height:125px; width:450px;"></textarea>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>


										<?php if ($fromSniic == 0) { ?>

                                            <!-- QUESTIONARIO -->

                                            <tr>
                                                <td colspan="4" class="areaCadastro2" style="padding-top:15px;padding-bottom:10px;"><img src="<?php echo URL_IMG; ?>questIcon.jpg" style="padding-right:10px;" /> Questionário</td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:5px;padding-bottom:5px; background-color:#EFEFEF;" colspan="4">
                                                    <b>Informações Básicas</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>1 - Tipo de Biblioteca*</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <label style="cursor:help" title="Tem por objetivo atender, por meio do seu acervo e de seus serviços, os diferentes interesses de leitura e informação da comunidade em que está localizada, colaborando para ampliar o acesso à informação, à leitura e ao livro, de forma gratuita. Atende a todos os públicos, bebes, crianças, jovens, adultos, pessoas da melhor idade e pessoas com necessidades especiais. É criada e mantida pelo Estado (Município, Estado ou Federação)."><input type="radio" name="qx1" id="qx1" value="1" <?php echo set_radio('qx1', '1'); ?> /> Biblioteca Pública Municipal</label><img src="<?php echo URL_IMG; ?>questIcon.jpg" title="Tem por objetivo atender, por meio do seu acervo e de seus serviços, os diferentes interesses de leitura e informação da comunidade em que está localizada, colaborando para ampliar o acesso à informação, à leitura e ao livro, de forma gratuita. Atende a todos os públicos, bebes, crianças, jovens, adultos, pessoas da melhor idade e pessoas com necessidades especiais. É criada e mantida pelo Estado (Município, Estado ou Federação)." style="padding-right:10px;cursor:help; width:20px" /> 	<br />
                                                    <label style="cursor:help" title=""><input type="radio" name="qx1" id="qx1" value="2" <?php echo set_radio('qx1', '2'); ?> /> Biblioteca Pública Estadual</label><img src="<?php echo URL_IMG; ?>questIcon.jpg" title="" style="padding-right:10px;cursor:help; width:20px" /><br />
                                                    <label style="cursor:help" title="Tem por objetivo apoiar as atividades de ensino, pesquisa e extensão por meio de seu acervo e dos seus serviços. Atende alunos, professores, pesquisadores e comunidade acadêmica em geral. É vinculada a uma unidade de ensino superior, podendo ser uma instituição pública ou privada."><input type="radio" name="qx1" id="qx1" value="3" <?php echo set_radio('qx1', '3'); ?> /> Biblioteca Universitária - vinculada ou não à Órgão Público</label><img src="<?php echo URL_IMG; ?>questIcon.jpg" title="Tem por objetivo apoiar as atividades de ensino, pesquisa e extensão por meio de seu acervo e dos seus serviços. Atende alunos, professores, pesquisadores e comunidade acadêmica em geral. É vinculada a uma unidade de ensino superior, podendo ser uma instituição pública ou privada." style="padding-right:10px;cursor:help; width:20px" /><br />
                                                    <label style="cursor:help" title="Voltada a um campo específico do conhecimento. Seu acervo e seus serviços atendem às necessidades de informação e pesquisa de usuários interessados em uma ou mais áreas específicas do conhecimento. É vinculada a uma instituição pública, ou privada podendo também se caracterizar como uma biblioteca universitária, quando vinculada a uma unidade de ensino superior."><input type="radio" name="qx1" id="qx1" value="4" <?php echo set_radio('qx1', '4'); ?> /> Biblioteca Especializada - vinculada ou não à Órgão Público</label><img src="<?php echo URL_IMG; ?>questIcon.jpg" title="Voltada a um campo específico do conhecimento. Seu acervo e seus serviços atendem às necessidades de informação e pesquisa de usuários interessados em uma ou mais áreas específicas do conhecimento. É vinculada a uma instituição pública, ou privada podendo também se caracterizar como uma biblioteca universitária, quando vinculada a uma unidade de ensino superior." style="padding-right:10px;cursor:help; width:20px" /><br />
                                                    <label style="cursor:help" title="Espaço de incentivo à leitura e acesso ao livro. É criada e mantida pela comunidade local, sem vinculo direto com o Estado.."><input type="radio" name="qx1" id="qx1" value="5" <?php echo set_radio('qx1', '5'); ?> /> Biblioteca Comunitária</label><img src="<?php echo URL_IMG; ?>questIcon.jpg" title="Espaço de incentivo à leitura e acesso ao livro. É criada e mantida pela comunidade local, sem vinculo direto com o Estado." style="padding-right:10px;cursor:help; width:20px" /><br />
                                                    <label><input type="radio" name="qx1" id="qx1" value="6" <?php echo set_radio('qx1', '6'); ?> /> Biblioteca Comunitária Rural</label><br />
                                                    <label style="cursor:help" title="Tem por objetivo atender os interesses de leitura e informação da sua comunidade e trabalha em consonância com o projeto pedagógico da escola a qual está inserida. Atende prioritariamente alunos, professores, funcionários da unidade de ensino, podendo também ampliar sua ação para atender os familiares de alunos e a comunidade moradora do entorno. Esta localizada dentro de uma unidade de ensino pré-escolar, fundamental e/ou médio."><input type="radio" name="qx1" id="qx1" value="7" <?php echo set_radio('qx1', '7'); ?> /> Biblioteca Escolar</label><img src="<?php echo URL_IMG; ?>questIcon.jpg" title="Tem por objetivo atender os interesses de leitura e informação da sua comunidade e trabalha em consonância com o projeto pedagógico da escola a qual está inserida. Atende prioritariamente alunos, professores, funcionários da unidade de ensino, podendo também ampliar sua ação para atender os familiares de alunos e a comunidade moradora do entorno. Esta localizada dentro de uma unidade de ensino pré-escolar, fundamental e/ou médio." style="padding-right:10px;cursor:help; width:20px" /><br />
                                                    <label style="cursor:help" title="Espaços de incentivo à leitura e acesso ao livro, criados em comunidades, fabricas, hospitais, presídios e instituições em geral, em sua maioria sua criação contou com o apoio do Programa Mais Cultura. É um estímulo à criação de bibliotecas comunitárias nas comunidades."><input type="radio" name="qx1" id="qx1" value="8" <?php echo set_radio('qx1', '8'); ?> /> Pontos de Leitura: espaços de leitura não vinculados a escolas</label><img src="<?php echo URL_IMG; ?>questIcon.jpg" title="Espaços de incentivo à leitura e acesso ao livro, criados em comunidades, fabricas, hospitais, presídios e instituições em geral, em sua maioria sua criação contou com o apoio do Programa Mais Cultura. É um estímulo à criação de bibliotecas comunitárias nas comunidades." style="padding-right:10px;cursor:help; width:20px" /><br />
                                                    <label style="cursor:help" title="Espaços de incentivo à leitura e acesso ao livro vinculados a escolas."><input type="radio" name="qx1" id="qx1" value="9" <?php echo set_radio('qx1', '9'); ?> /> Salas de leitura: espaços de leitura vinculados a escolas</label><img src="<?php echo URL_IMG; ?>questIcon.jpg" title="Espaços de incentivo à leitura e acesso ao livro vinculados a escolas." style="padding-right:10px;cursor:help; width:20px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>2 - Situação de Funcionamento*</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <label style="cursor:help" title="Todas as áreas da biblioteca estão em funcionamento" ><input type="radio" name="qx2" id="qx2" value="1" <?php echo set_radio('qx2', '1'); ?> /> Em funcionamento normal</label><img src="<?php echo URL_IMG; ?>questIcon.jpg" title="Todas as áreas da biblioteca estão em funcionamento." style="padding-right:10px;cursor:help; width:20px" /><br />
                                                    <label style="cursor:help" title="Uma ou mais áreas da biblioteca não está em funcionamento" ><input type="radio" name="qx2" id="qx2" value="2" <?php echo set_radio('qx2', '2'); ?> /> Em funcionamento parcial<img src="<?php echo URL_IMG; ?>questIcon.jpg" title="Uma ou mais áreas da biblioteca não está em funcionamento." style="padding-right:10px;cursor:help; width:20px" /></label><br />
                                                    <label style="cursor:help" title="Biblioteca não encontra-se em funcionamento" ><input type="radio" name="qx2" id="qx2" value="3" <?php echo set_radio('qx2', '3'); ?> /> Fechada<img src="<?php echo URL_IMG; ?>questIcon.jpg" title="Biblioteca não encontra-se em funcionamento." style="padding-right:10px;cursor:help; width:20px" /></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>3 - A biblioteca está localizada em zona*</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" style="padding-bottom:10px;">
                                                    <label style="cursor:help" title="Caracterizada por edificações contínuas, centros comerciais, governamentais, financeiros e culturais." ><input type="radio" name="qx3" id="qx3" value="1" <?php echo set_radio('qx3', '1'); ?> /> Urbana<img src="<?php echo URL_IMG; ?>questIcon.jpg" title="Caracterizada por edificações contínuas, centros comerciais, governamentais, financeiros e culturais." style="padding-right:10px;cursor:help; width:20px" /></label><br />
                                                    <label style="cursor:help" title="Área não-urbanizada; campo. Onde são desenvolvidos empreendimentos e atividades agrícolas." ><input type="radio" name="qx3" id="qx3" value="2" <?php echo set_radio('qx3', '2'); ?> /> Rural<img src="<?php echo URL_IMG; ?>questIcon.jpg" title="Área não-urbanizada; campo. Onde são desenvolvidos empreendimentos e atividades agrícolas.." style="padding-right:10px;cursor:help; width:20px" /></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:5px;padding-bottom:5px; background-color:#EFEFEF;" colspan="4">
                                                    <b>Público</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>4 - Restrição de Uso: Se o uso da biblioteca é restrito à determinados grupos, quais são esses grupos?*</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" id="qx4">
                                                    <label><input type="checkbox" name="qx4_1" id="qx4_1" value="1" <?php echo set_checkbox('qx4_1', '1'); ?> /> Não há restrição, a biblioteca é aberta ao público em geral</label><br />
                                                    <label><input type="checkbox" name="qx4_2" id="qx4_2" value="1" <?php echo set_checkbox('qx4_2', '1'); ?> /> Estudantes, professores e funcionários de uma determinada instituição de ensino</label><br />
                                                    <label><input type="checkbox" name="qx4_3" id="qx4_3" value="1" <?php echo set_checkbox('qx4_3', '1'); ?> /> Funcionários de uma determinada organização</label><br />
                                                    <label><input type="checkbox" name="qx4_4" id="qx4_4" value="1" <?php echo set_checkbox('qx4_4', '1'); ?> /> Especialistas no assunto</label><br />
                                                    <label><input type="checkbox" name="qx4_5" id="qx4_5" value="1" <?php echo set_checkbox('qx4_5', '1'); ?> /> Outro: </label>
                                                    <input class="inputText" type="text" name="qx4_outro" id="qx4_outro" style="width:200px;" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx4_outro'); ?>" autocomplete="off" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>5 - Faixa etária predominante dos usuários* </b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <label><input type="radio" name="qx5" id="qx5" value="1" <?php echo set_radio('qx5', '1'); ?> /> Até 12 anos</label><br />
                                                    <label><input type="radio" name="qx5" id="qx5" value="2" <?php echo set_radio('qx5', '2'); ?> /> De 13 até 18 anos</label><br />
                                                    <label><input type="radio" name="qx5" id="qx5" value="3" <?php echo set_radio('qx5', '3'); ?> /> De 19 até 29 anos</label><br />
                                                    <label><input type="radio" name="qx5" id="qx5" value="4" <?php echo set_radio('qx5', '4'); ?> /> De 30 até 39 anos</label><br />
                                                    <label><input type="radio" name="qx5" id="qx5" value="5" <?php echo set_radio('qx5', '5'); ?> /> De 40 até 59 anos</label><br />
                                                    <label><input type="radio" name="qx5" id="qx5" value="6" <?php echo set_radio('qx5', '6'); ?> /> 60 anos ou mais</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>6 - Comunidades ou grupos específicos que a biblioteca trabalha*</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <label><input type="checkbox" name="qx6_1" id="qx6_1" value="1" <?php echo set_checkbox('qx6_1', '1'); ?> /> Quilombolas</label><br />
                                                    <label><input type="checkbox" name="qx6_2" id="qx6_2" value="1" <?php echo set_checkbox('qx6_2', '1'); ?> /> Indígenas</label><br />
                                                    <label><input type="checkbox" name="qx6_3" id="qx6_3" value="1" <?php echo set_checkbox('qx6_3', '1'); ?> /> Imigrantes</label><br />
                                                    <label><input type="checkbox" name="qx6_4" id="qx6_4" value="1" <?php echo set_checkbox('qx6_4', '1'); ?> /> Nenhum</label><br />
                                                    <label><input type="checkbox" name="qx6_5" id="qx6_5" value="1" <?php echo set_checkbox('qx6_5', '1'); ?> /> Outro: </label>
                                                    <input class="inputText" type="text" name="qx6_outro" id="qx6_outro" style="width:200px;" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx6_outro'); ?>" autocomplete="off" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>7 - Freqüência média mensal dos usuários*</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" style="padding-bottom:10px;">
                                                    <input class="inputText" type="text" name="qx7_outro" id="qx7_outro" style="width:200px;" maxlength="6" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx7_outro'); ?>" autocomplete="off" onkeypress="return maskInteger(this, event)" />
                                                    <i>(Somente valores numéricos)</i>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:5px;padding-bottom:5px; background-color:#EFEFEF;" colspan="4">
                                                    <b>Acervo</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>8 - Total aproximado de itens do acervo*</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <input class="inputText" type="text" name="qx8_outro" id="qx8_outro" style="width:200px;" maxlength="10" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx8_outro'); ?>" autocomplete="off" onkeypress="return maskInteger(this, event)" />
                                                    <i>(Considere como item a unidade física do documento.)</i>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>9 - Assuntos mais Procurados*</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <label><input type="checkbox" name="qx9_1" id="qx9_1" value="1" <?php echo set_checkbox('qx9_1', '1'); ?> /> Artes</label><br />
                                                    <label><input type="checkbox" name="qx9_2" id="qx9_2" value="1" <?php echo set_checkbox('qx9_2', '1'); ?> /> Biografias</label><br />
                                                    <label><input type="checkbox" name="qx9_3" id="qx9_3" value="1" <?php echo set_checkbox('qx9_3', '1'); ?> /> Ciências Exatas</label><br />
                                                    <label><input type="checkbox" name="qx9_4" id="qx9_4" value="1" <?php echo set_checkbox('qx9_4', '1'); ?> /> Ciências Sociais</label><br />
                                                    <label><input type="checkbox" name="qx9_5" id="qx9_5" value="1" <?php echo set_checkbox('qx9_5', '1'); ?> /> Filosofia e Psicologia</label><br />
                                                    <label><input type="checkbox" name="qx9_6" id="qx9_6" value="1" <?php echo set_checkbox('qx9_6', '1'); ?> /> História e Geografia</label><br />
                                                    <label><input type="checkbox" name="qx9_7" id="qx9_7" value="1" <?php echo set_checkbox('qx9_7', '1'); ?> /> Literatura em geral</label><br />
                                                    <label><input type="checkbox" name="qx9_8" id="qx9_8" value="1" <?php echo set_checkbox('qx9_8', '1'); ?> /> Literatura infantil e juvenil</label><br />
                                                    <label><input type="checkbox" name="qx9_9" id="qx9_9" value="1" <?php echo set_checkbox('qx9_9', '1'); ?> /> Religião</label><br />
                                                    <label><input type="checkbox" name="qx9_10" id="qx9_10" value="1" <?php echo set_checkbox('qx9_10', '1'); ?> /> Outro: </label>
                                                    <input class="inputText" type="text" name="qx9_outro" id="qx9_outro" style="width:200px;" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx9_outro'); ?>" autocomplete="off" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>10 - Formas de Aquisição do Acervo*</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <label><input type="checkbox" name="qx10_1" id="qx10_1" value="1" <?php echo set_checkbox('qx10_1', '1'); ?> /> Compra</label><br />
                                                    <label><input type="checkbox" name="qx10_2" id="qx10_2" value="1" <?php echo set_checkbox('qx10_2', '1'); ?> /> Doação</label><br />
                                                    <label><input type="checkbox" name="qx10_3" id="qx10_3" value="1" <?php echo set_checkbox('qx10_3', '1'); ?> /> Permuta</label><br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>11 - Tipos de documentos e suportes que compõem o acervo*</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <label><input type="checkbox" name="qx11_1" id="qx11_1" value="1" <?php echo set_checkbox('qx11_1', '1'); ?> /> Livros, teses, dissertações</label><br />
                                                    <label><input type="checkbox" name="qx11_2" id="qx11_2" value="1" <?php echo set_checkbox('qx11_2', '1'); ?> /> Periódicos (Jornais, revistas, etc.)</label><br />
                                                    <label><input type="checkbox" name="qx11_3" id="qx11_3" value="1" <?php echo set_checkbox('qx11_3', '1'); ?> /> Multimídia (CD, DVD, microfilme, etc.)</label><br />
                                                    <label><input type="checkbox" name="qx11_4" id="qx11_4" value="1" <?php echo set_checkbox('qx11_4', '1'); ?> /> Iconográficos (fotografias, gravuras, etc.)</label><br />
                                                    <label><input type="checkbox" name="qx11_5" id="qx11_5" value="1" <?php echo set_checkbox('qx11_5', '1'); ?> /> Cartográficos (mapas, etc.)</label><br />
                                                    <label><input type="checkbox" name="qx11_6" id="qx11_6" value="1" <?php echo set_checkbox('qx11_6', '1'); ?> /> Partituras</label><br />
                                                    <label><input type="checkbox" name="qx11_7" id="qx11_7" value="1" <?php echo set_checkbox('qx11_7', '1'); ?> /> Obras raras</label><br />
                                                    <label><input type="checkbox" name="qx11_8" id="qx11_8" value="1" <?php echo set_checkbox('qx11_8', '1'); ?> /> Manuscritos</label><br />
                                                    <label><input type="checkbox" name="qx11_9" id="qx11_9" value="1" <?php echo set_checkbox('qx11_9', '1'); ?> /> Folhetos</label><br />
                                                    <label><input type="checkbox" name="qx11_10" id="qx11_10" value="1" <?php echo set_checkbox('qx11_10', '1'); ?> /> Recortes de jornais</label><br />
                                                    <label><input type="checkbox" name="qx11_11" id="qx11_11" value="1" <?php echo set_checkbox('qx11_11', '1'); ?> /> Publicações eletrônicas</label><br />
                                                    <label><input type="checkbox" name="qx11_12" id="qx11_12" value="1" <?php echo set_checkbox('qx11_12', '1'); ?> /> Estampas</label><br />
                                                    <label><input type="checkbox" name="qx11_13" id="qx11_13" value="1" <?php echo set_checkbox('qx11_13', '1'); ?> /> Obras de referência</label><br />
                                                    <label><input type="checkbox" name="qx11_14" id="qx11_14" value="1" <?php echo set_checkbox('qx11_14', '1'); ?> /> Outro: </label>
                                                    <input class="inputText" type="text" name="qx11_outro" id="qx11_outro" style="width:200px;" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx11_outro'); ?>" autocomplete="off" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>12 - Sistema de classificação utilizado pela biblioteca*</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <label><input type="radio" name="qx12" id="qx12" value="1" <?php echo set_radio('qx12', '1'); ?> /> Nenhum</label><br />
                                                    <label><input type="radio" name="qx12" id="qx12" value="2" <?php echo set_radio('qx12', '2'); ?> /> Classificação Decimal Universal – CDU</label><br />
                                                    <label><input type="radio" name="qx12" id="qx12" value="3" <?php echo set_radio('qx12', '3'); ?> /> Classificação Decimal de Dewey – CDD</label><br />
                                                    <label><input type="radio" name="qx12" id="qx12" value="4" <?php echo set_radio('qx12', '4'); ?> /> Outro: </label>
                                                    <input class="inputText" type="text" name="qx12_outro" id="qx12_outro" style="width:200px;" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx12_outro'); ?>" autocomplete="off" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>13 - Código de catalogação utilizado pela biblioteca*</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <label><input type="radio" name="qx13" id="qx13" value="1" <?php echo set_radio('qx13', '1'); ?> /> Nenhum</label><br />
                                                    <label><input type="radio" name="qx13" id="qx13" value="2" <?php echo set_radio('qx13', '2'); ?> /> International Standard Bibliographic Description - ISBD</label><br />
                                                    <label><input type="radio" name="qx13" id="qx13" value="3" <?php echo set_radio('qx13', '3'); ?> /> Anglo-American Cataloguing Rules - AACR</label><br />
                                                    <label><input type="radio" name="qx13" id="qx13" value="4" <?php echo set_radio('qx13', '4'); ?> /> Outro: </label>
                                                    <input class="inputText" type="text" name="qx13_outro" id="qx13_outro" style="width:200px;" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx13_outro'); ?>" autocomplete="off" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>14 - Formato do catálogo bibliográfico</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <label><input type="radio" name="qx14" id="qx14" value="1" <?php echo set_radio('qx14', '1'); ?> onclick="esconde('qx15_outro')" /> Manual</label><br />
                                                    <label><input type="radio" name="qx14" id="qx14" value="2" <?php echo set_radio('qx14', '2'); ?> onclick="mostra('qx15_outro'); "/> Eletrônico</label><br />
                                                    <label><input type="radio" name="qx14" id="qx14" value="3" <?php echo set_radio('qx14', '3'); ?> onclick="esconde('qx15_outro')"/> Não possui catálogo</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>15 - Se o formato do catálogo bibliográfico é eletrônico, qual o sistema de gerenciamento eletrônico?</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <input class="inputText" type="text" name="qx15_outro" id="qx15_outro" style="width:200px; display:none" disabled="true" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx15_outro'); ?>" autocomplete="off" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>16 - Se o formato do catálogo bibliográfico é eletrônico, possui acesso online?*</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <label><input type="radio" name="qx16" id="qx16" value="1" <?php echo set_radio('qx16', '1'); ?> onclick="mostra('qx17_outro'); " /> Sim</label><br />
                                                    <label><input type="radio" name="qx16" id="qx16" value="2" <?php echo set_radio('qx16', '2'); ?> onclick="esconde('qx17_outro')"/> Não</label><br />
                                                    <label><input type="radio" name="qx16" id="qx16" value="3" <?php echo set_radio('qx16', '3'); ?> onclick="esconde('qx17_outro')"/> Não se aplica</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>17 - Se sim, qual a URL para acesso online?</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <input class="inputText" type="text" name="qx17_outro" id="qx17_outro" style="width:200px; display:none;" disabled="true" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx17_outro'); ?>" autocomplete="off" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>18 - Se o formato do catálogo bibliográfico é eletrônico, a biblioteca participa de programa de catalogação cooperativa?*</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <label><input type="radio" name="qx18" id="qx18" value="1" <?php echo set_radio('qx18', '1'); ?> onclick="mostra('qx19_outro')"; /> Sim</label><br />
                                                    <label><input type="radio" name="qx18" id="qx18" value="2" <?php echo set_radio('qx18', '2'); ?> onclick="esconde('qx19_outro')"/> Não</label><br />
                                                    <label><input type="radio" name="qx18" id="qx18" value="3" <?php echo set_radio('qx18', '3'); ?> onclick="esconde('qx19_outro')"/> Não se aplica</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>19 - Se sim, qual o nome do programa de catalogação cooperativa?</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <input class="inputText" type="text" name="qx19_outro" id="qx19_outro" style="width:200px; display:none;" disabled="true" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx19_outro'); ?>" autocomplete="off" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>20 - O acesso às estantes pelo usuário é:*</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" style="padding-bottom:10px;">
                                                    <label><input type="radio" name="qx20" id="qx20" value="1" <?php echo set_radio('qx20', '1'); ?> /> Aberto</label><br />
                                                    <label><input type="radio" name="qx20" id="qx20" value="2" <?php echo set_radio('qx20', '2'); ?> /> Restrito</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:5px;padding-bottom:5px; background-color:#EFEFEF;" colspan="4">
                                                    <b>Serviços e Atividades</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>21 - Serviços oferecidos na biblioteca*</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <label><input type="checkbox" name="qx21_1" id="qx21_1" value="1" <?php echo set_checkbox('qx21_1', '1'); ?> /> Empréstimo</label><br />
                                                    <label><input type="checkbox" name="qx21_2" id="qx21_2" value="1" <?php echo set_checkbox('qx21_2', '1'); ?> /> Informação e atendimento presencial</label><br />
                                                    <label><input type="checkbox" name="qx21_3" id="qx21_3" value="1" <?php echo set_checkbox('qx21_3', '1'); ?> /> Informação e atendimento a distância</label><br />
                                                    <label><input type="checkbox" name="qx21_4" id="qx21_4" value="1" <?php echo set_checkbox('qx21_4', '1'); ?> /> Orientação e apoio à pesquisa escolar</label><br />
                                                    <label><input type="checkbox" name="qx21_5" id="qx21_5" value="1" <?php echo set_checkbox('qx21_5', '1'); ?> /> Orientação e apoio à pesquisa acadêmica</label><br />
                                                    <label><input type="checkbox" name="qx21_6" id="qx21_6" value="1" <?php echo set_checkbox('qx21_6', '1'); ?> /> Disseminação seletiva da informação</label><br />
                                                    <label><input type="checkbox" name="qx21_7" id="qx21_7" value="1" <?php echo set_checkbox('qx21_7', '1'); ?> /> Acesso à base de dados</label><br />
                                                    <label><input type="checkbox" name="qx21_8" id="qx21_8" value="1" <?php echo set_checkbox('qx21_8', '1'); ?> /> Orientação ao uso pedagógico da internet</label><br />
                                                    <label><input type="checkbox" name="qx21_9" id="qx21_9" value="1" <?php echo set_checkbox('qx21_9', '1'); ?> /> Serviço de impressão</label><br />
                                                    <label><input type="checkbox" name="qx21_10" id="qx21_10" value="1" <?php echo set_checkbox('qx21_10', '1'); ?> /> Serviço de reprografia</label><br />
                                                    <label><input type="checkbox" name="qx21_11" id="qx21_11" value="1" <?php echo set_checkbox('qx21_11', '1'); ?> /> Serviços de digitalização</label><br />
                                                    <label><input type="checkbox" name="qx21_12" id="qx21_12" value="1" <?php echo set_checkbox('qx21_12', '1'); ?> /> Outro: </label>
                                                    <input class="inputText" type="text" name="qx21_outro" id="qx21_outro" style="width:200px;" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx21_outro'); ?>" autocomplete="off" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>22 - Serviços oferecidos para portadores de necessidades especiais*</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <label><input type="checkbox" name="qx22_1" id="qx22_1" value="1" <?php echo set_checkbox('qx22_1', '1'); ?> /> Livro em Braille</label><br />
                                                    <label><input type="checkbox" name="qx22_2" id="qx22_2" value="1" <?php echo set_checkbox('qx22_2', '1'); ?> /> Áudio Livro</label><br />
                                                    <label><input type="checkbox" name="qx22_3" id="qx22_3" value="1" <?php echo set_checkbox('qx22_3', '1'); ?> /> Impressão em Braille</label><br />
                                                    <label><input type="checkbox" name="qx22_4" id="qx22_4" value="1" <?php echo set_checkbox('qx22_4', '1'); ?> /> Mobiliário adaptado</label><br />
                                                    <label><input type="checkbox" name="qx22_5" id="qx22_5" value="1" <?php echo set_checkbox('qx22_5', '1'); ?> /> Tecnologia assistida</label><br />
                                                    <label><input type="checkbox" name="qx22_6" id="qx22_6" value="1" <?php echo set_checkbox('qx22_6', '1'); ?> /> Software para acessibilidade</label><br />
                                                    <label><input type="checkbox" name="qx22_7" id="qx22_7" value="1" <?php echo set_checkbox('qx22_7', '1'); ?> /> Nenhum</label><br />
                                                    <label><input type="checkbox" name="qx22_8" id="qx22_8" value="1" <?php echo set_checkbox('qx22_8', '1'); ?> /> Outro: </label>
                                                    <input class="inputText" type="text" name="qx22_outro" id="qx22_outro" style="width:200px;" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx22_outro'); ?>" autocomplete="off" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>23 - Conveniências</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <label><input type="checkbox" name="qx23_1" id="qx23_1" value="1" <?php echo set_checkbox('qx23_1', '1'); ?> /> Lanchonete, cafés, etc.</label><br />
                                                    <label><input type="checkbox" name="qx23_2" id="qx23_2" value="1" <?php echo set_checkbox('qx23_2', '1'); ?> /> Restaurante</label><br />
                                                    <label><input type="checkbox" name="qx23_3" id="qx23_3" value="1" <?php echo set_checkbox('qx23_3', '1'); ?> /> Livraria</label><br />
                                                    <label><input type="checkbox" name="qx23_4" id="qx23_4" value="1" <?php echo set_checkbox('qx23_4', '1'); ?> /> Estacionamento</label><br />
                                                    <label><input type="checkbox" name="qx23_5" id="qx23_5" value="1" <?php echo set_checkbox('qx23_5', '1'); ?> /> Outro: </label>
                                                    <input class="inputText" type="text" name="qx23_outro" id="qx23_outro" style="width:200px;" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx23_outro'); ?>" autocomplete="off" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>24 - Serviços de Extensão*</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <label><input type="checkbox" name="qx24_1" id="qx24_1" value="1" <?php echo set_checkbox('qx24_1', '1'); ?> /> Caixa-estante</label><br />
                                                    <label><input type="checkbox" name="qx24_2" id="qx24_2" value="1" <?php echo set_checkbox('qx24_2', '1'); ?> /> Ônibus biblioteca</label><br />
                                                    <label><input type="checkbox" name="qx24_3" id="qx24_3" value="1" <?php echo set_checkbox('qx24_3', '1'); ?> /> Atividades de leitura fora da biblioteca</label><br />
                                                    <label><input type="checkbox" name="qx24_4" id="qx24_4" value="1" <?php echo set_checkbox('qx24_4', '1'); ?> /> Empréstimos em domicílio</label><br />
                                                    <label><input type="checkbox" name="qx24_5" id="qx24_5" value="1" <?php echo set_checkbox('qx24_5', '1'); ?> /> Nenhum</label><br />
                                                    <label><input type="checkbox" name="qx24_6" id="qx24_6" value="1" <?php echo set_checkbox('qx24_6', '1'); ?> /> Outro: </label>
                                                    <input class="inputText" type="text" name="qx24_outro" id="qx24_outro" style="width:200px;" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx24_outro'); ?>" autocomplete="off" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>25 - Atividades e eventos culturais promovidos ou com participação da biblioteca</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" style="padding-bottom:10px;">
                                                    <label><input type="checkbox" name="qx25_1" id="qx25_1" value="1" <?php echo set_checkbox('qx25_1', '1'); ?> /> Mediação de leitura</label><br />
                                                    <label><input type="checkbox" name="qx25_2" id="qx25_2" value="1" <?php echo set_checkbox('qx25_2', '1'); ?> /> Grupos de leitura</label><br />
                                                    <label><input type="checkbox" name="qx25_3" id="qx25_3" value="1" <?php echo set_checkbox('qx25_3', '1'); ?> /> Exposições</label><br />
                                                    <label><input type="checkbox" name="qx25_4" id="qx25_4" value="1" <?php echo set_checkbox('qx25_4', '1'); ?> /> Oficinas e cursos</label><br />
                                                    <label><input type="checkbox" name="qx25_5" id="qx25_5" value="1" <?php echo set_checkbox('qx25_5', '1'); ?> /> Encontros</label><br />
                                                    <label><input type="checkbox" name="qx25_6" id="qx25_6" value="1" <?php echo set_checkbox('qx25_6', '1'); ?> /> Saraus</label><br />
                                                    <label><input type="checkbox" name="qx25_7" id="qx25_7" value="1" <?php echo set_checkbox('qx25_7', '1'); ?> /> Feiras de livro</label><br />
                                                    <label><input type="checkbox" name="qx25_8" id="qx25_8" value="1" <?php echo set_checkbox('qx25_8', '1'); ?> /> Outro: </label>
                                                    <input class="inputText" type="text" name="qx25_outro" id="qx25_outro" style="width:200px;" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx25_outro'); ?>" autocomplete="off" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:5px;padding-bottom:5px; background-color:#EFEFEF;" colspan="4">
                                                    <b>Infraestrutura</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>26 - O prédio da biblioteca é:*</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <label><input type="radio" name="qx26" id="qx26" value="1" <?php echo set_radio('qx26', '1'); ?> /> Próprio</label><br />
                                                    <label><input type="radio" name="qx26" id="qx26" value="2" <?php echo set_radio('qx26', '2'); ?> /> Alugado</label><br />
                                                    <label><input type="radio" name="qx26" id="qx26" value="3" <?php echo set_radio('qx26', '3'); ?> /> Cedido</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>27 - Área construída em metros quadrados da biblioteca*</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <input class="inputText" type="text" name="qx27_outro" id="qx27_outro" style="width:200px;" maxlength="6" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx27_outro'); ?>" autocomplete="off" onkeypress="return maskInteger(this, event)" />
                                                    <i>(Digite somente números. Você pode encontrar essa informação, por exemplo, no carnê do IPTU.)</i>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>28 - Acessibilidade Física*</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <label><input type="checkbox" name="qx28_1" id="qx28_1" value="1" <?php echo set_checkbox('qx28_1', '1'); ?> /> Banheiros adaptados</label><br />
                                                    <label><input type="checkbox" name="qx28_2" id="qx28_2" value="1" <?php echo set_checkbox('qx28_2', '1'); ?> /> Rampa de acesso</label><br />
                                                    <label><input type="checkbox" name="qx28_3" id="qx28_3" value="1" <?php echo set_checkbox('qx28_3', '1'); ?> /> Elevador</label><br />
                                                    <label><input type="checkbox" name="qx28_4" id="qx28_4" value="1" <?php echo set_checkbox('qx28_4', '1'); ?> /> Sinalização tátil</label><br />
                                                    <label><input type="checkbox" name="qx28_5" id="qx28_5" value="1" <?php echo set_checkbox('qx28_5', '1'); ?> /> Nenhum</label><br />
                                                    <label><input type="checkbox" name="qx28_6" id="qx28_6" value="1" <?php echo set_checkbox('qx28_6', '1'); ?> /> Outro: </label>
                                                    <input class="inputText" type="text" name="qx28_outro" id="qx28_outro" style="width:200px;" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx28_outro'); ?>" autocomplete="off" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>29 - Selecione os espaços diferenciados que a biblioteca possui*</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <label><input type="checkbox" name="qx29_1" id="qx29_1" value="1" <?php echo set_checkbox('qx29_1', '1'); ?> /> Espaço de atendimento</label><br />
                                                    <label><input type="checkbox" name="qx29_2" id="qx29_2" value="1" <?php echo set_checkbox('qx29_2', '1'); ?> /> Espaços coletivos para leitura</label><br />
                                                    <label><input type="checkbox" name="qx29_3" id="qx29_3" value="1" <?php echo set_checkbox('qx29_3', '1'); ?> /> Cabines para trabalho em grupo</label><br />
                                                    <label><input type="checkbox" name="qx29_4" id="qx29_4" value="1" <?php echo set_checkbox('qx29_4', '1'); ?> /> Salas multimídia</label><br />
                                                    <label><input type="checkbox" name="qx29_5" id="qx29_5" value="1" <?php echo set_checkbox('qx29_5', '1'); ?> /> Salas para acesso à internet</label><br />
                                                    <label><input type="checkbox" name="qx29_6" id="qx29_6" value="1" <?php echo set_checkbox('qx29_6', '1'); ?> /> Espaços infantis</label><br />
                                                    <label><input type="checkbox" name="qx29_7" id="qx29_7" value="1" <?php echo set_checkbox('qx29_7', '1'); ?> /> Espaços para públicos com necessidades especiais</label><br />
                                                    <label><input type="checkbox" name="qx29_8" id="qx29_8" value="1" <?php echo set_checkbox('qx29_8', '1'); ?> /> Auditórios</label><br />
                                                    <label><input type="checkbox" name="qx29_9" id="qx29_9" value="1" <?php echo set_checkbox('qx29_9', '1'); ?> /> Salas para cursos e oficinas</label><br />
                                                    <label><input type="checkbox" name="qx29_10" id="qx29_10" value="1" <?php echo set_checkbox('qx29_10', '1'); ?> /> Espaços de sociabilidade</label><br />
                                                    <label><input type="checkbox" name="qx29_11" id="qx29_11" value="1" <?php echo set_checkbox('qx29_11', '1'); ?> /> Espaços para exposições</label><br />
                                                    <label><input type="checkbox" name="qx29_12" id="qx29_12" value="1" <?php echo set_checkbox('qx29_12', '1'); ?> /> Espaços para serviços técnicos administrativos</label><br />
                                                    <label><input type="checkbox" name="qx29_13" id="qx29_13" value="1" <?php echo set_checkbox('qx29_13', '1'); ?> /> Nenhum</label><br />
                                                    <label><input type="checkbox" name="qx29_14" id="qx29_14" value="1" <?php echo set_checkbox('qx29_14', '1'); ?> /> Outro: </label>
                                                    <input class="inputText" type="text" name="qx29_outro" id="qx29_outro" style="width:200px;" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx29_outro'); ?>" autocomplete="off" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>30 - Equipamentos disponíveis para os USUÁRIOS*</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <label><input type="checkbox" name="qx30_1" id="qx30_1" value="1" <?php echo set_checkbox('qx30_1', '1'); ?> /> Ar condicionado</label><br />
                                                    <label><input type="checkbox" name="qx30_2" id="qx30_2" value="1" <?php echo set_checkbox('qx30_2', '1'); ?> /> Computadores com internet</label><br />
                                                    <label><input type="checkbox" name="qx30_3" id="qx30_3" value="1" <?php echo set_checkbox('qx30_3', '1'); ?> /> Computadores sem internet</label><br />
                                                    <label><input type="checkbox" name="qx30_4" id="qx30_4" value="1" <?php echo set_checkbox('qx30_4', '1'); ?> /> Rede Wi-Fi</label><br />
                                                    <label><input type="checkbox" name="qx30_5" id="qx30_5" value="1" <?php echo set_checkbox('qx30_5', '1'); ?> /> Projetor de imagens digital (data show)</label><br />
                                                    <label><input type="checkbox" name="qx30_6" id="qx30_6" value="1" <?php echo set_checkbox('qx30_6', '1'); ?> /> Mesas para estudo individual com baia</label><br />
                                                    <label><input type="checkbox" name="qx30_7" id="qx30_7" value="1" <?php echo set_checkbox('qx30_7', '1'); ?> /> Mesas para estudo individual sem baia</label><br />
                                                    <label><input type="checkbox" name="qx30_8" id="qx30_8" value="1" <?php echo set_checkbox('qx30_8', '1'); ?> /> Mesas coletivas</label><br />
                                                    <label><input type="checkbox" name="qx30_9" id="qx30_9" value="1" <?php echo set_checkbox('qx30_9', '1'); ?> /> Retroprojetor</label><br />
                                                    <label><input type="checkbox" name="qx30_10" id="qx30_10" value="1" <?php echo set_checkbox('qx30_10', '1'); ?> /> Aparelho de TV</label><br />
                                                    <label><input type="checkbox" name="qx30_11" id="qx30_11" value="1" <?php echo set_checkbox('qx30_11', '1'); ?> /> Aparelho de som</label><br />
                                                    <label><input type="checkbox" name="qx30_12" id="qx30_12" value="1" <?php echo set_checkbox('qx30_12', '1'); ?> /> Reprodutor de Vídeo Cassete</label><br />
                                                    <label><input type="checkbox" name="qx30_13" id="qx30_13" value="1" <?php echo set_checkbox('qx30_13', '1'); ?> /> Reprodutor de DVD</label><br />
                                                    <label><input type="checkbox" name="qx30_14" id="qx30_14" value="1" <?php echo set_checkbox('qx30_14', '1'); ?> /> Reprodutor de Blue-Ray</label><br />
                                                    <label><input type="checkbox" name="qx30_15" id="qx30_15" value="1" <?php echo set_checkbox('qx30_15', '1'); ?> /> Fones de ouvido</label><br />
                                                    <label><input type="checkbox" name="qx30_16" id="qx30_16" value="1" <?php echo set_checkbox('qx30_16', '1'); ?> /> Máquina para leitura de microfilme </label><br />
                                                    <label><input type="checkbox" name="qx30_17" id="qx30_17" value="1" <?php echo set_checkbox('qx30_17', '1'); ?> /> Outro: </label>
                                                    <input class="inputText" type="text" name="qx30_outro" id="qx30_outro" style="width:200px;" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx30_outro'); ?>" autocomplete="off" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>31 - Equipamentos disponíveis para os FUNCIONÁRIOS da biblioteca*</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" style="padding-bottom:10px;">
                                                    <label><input type="checkbox" name="qx31_1" id="qx31_1" value="1" <?php echo set_checkbox('qx31_1', '1'); ?> /> Ar condicionado</label><br />
                                                    <label><input type="checkbox" name="qx31_2" id="qx31_2" value="1" <?php echo set_checkbox('qx31_2', '1'); ?> /> Computadores com internet</label><br />
                                                    <label><input type="checkbox" name="qx31_3" id="qx31_3" value="1" <?php echo set_checkbox('qx31_3', '1'); ?> /> Computadores sem internet</label><br />
                                                    <label><input type="checkbox" name="qx31_4" id="qx31_4" value="1" <?php echo set_checkbox('qx31_4', '1'); ?> /> Rede Wi-Fi</label><br />
                                                    <label><input type="checkbox" name="qx31_5" id="qx31_5" value="1" <?php echo set_checkbox('qx31_5', '1'); ?> /> Projetor de imagens digital (data show)</label><br />
                                                    <label><input type="checkbox" name="qx31_6" id="qx31_6" value="1" <?php echo set_checkbox('qx31_6', '1'); ?> /> Retroprojetor</label><br />
                                                    <label><input type="checkbox" name="qx31_7" id="qx31_7" value="1" <?php echo set_checkbox('qx31_7', '1'); ?> /> Aparelho de TV</label><br />
                                                    <label><input type="checkbox" name="qx31_8" id="qx31_8" value="1" <?php echo set_checkbox('qx31_8', '1'); ?> /> Aparelho de som</label><br />
                                                    <label><input type="checkbox" name="qx31_9" id="qx31_9" value="1" <?php echo set_checkbox('qx31_9', '1'); ?> /> Reprodutor de Vídeo Cassete</label><br />
                                                    <label><input type="checkbox" name="qx31_10" id="qx31_10" value="1" <?php echo set_checkbox('qx31_10', '1'); ?> /> AReprodutor de DVD</label><br />
                                                    <label><input type="checkbox" name="qx31_11" id="qx31_11" value="1" <?php echo set_checkbox('qx31_11', '1'); ?> /> Reprodutor de Blue-Ray</label><br />
                                                    <label><input type="checkbox" name="qx31_12" id="qx31_12" value="1" <?php echo set_checkbox('qx31_12', '1'); ?> /> Fones de ouvido</label><br />
                                                    <label><input type="checkbox" name="qx31_13" id="qx31_13" value="1" <?php echo set_checkbox('qx31_13', '1'); ?> /> Máquina para leitura de microfilme</label><br />
                                                    <label><input type="checkbox" name="qx31_14" id="qx31_14" value="1" <?php echo set_checkbox('qx31_14', '1'); ?> /> Máquina de reprografia</label><br />
                                                    <label><input type="checkbox" name="qx31_15" id="qx31_15" value="1" <?php echo set_checkbox('qx31_15', '1'); ?> /> Outro: </label>
                                                    <input class="inputText" type="text" name="qx31_outro" id="qx31_outro" style="width:200px;" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx31_outro'); ?>" autocomplete="off" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:5px;padding-bottom:5px; background-color:#EFEFEF;" colspan="4">
                                                    <b>Gestão</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>32 - Selecione os elementos de gestão que a biblioteca possui*</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <label><input type="checkbox" name="qx32_1" id="qx32_1" value="1" <?php echo set_checkbox('qx32_1', '1'); ?> /> Comissão ou conselho de biblioteca</label><br />
                                                    <label><input type="checkbox" name="qx32_2" id="qx32_2" value="1" <?php echo set_checkbox('qx32_2', '1'); ?> /> Regulamento</label><br />
                                                    <label><input type="checkbox" name="qx32_3" id="qx32_3" value="1" <?php echo set_checkbox('qx32_3', '1'); ?> /> Política de formação, desenvolvimento e tratamento de coleções</label><br />
                                                    <label><input type="checkbox" name="qx32_4" id="qx32_4" value="1" <?php echo set_checkbox('qx32_4', '1'); ?> /> Política de acesso à Internet</label><br />
                                                    <label><input type="checkbox" name="qx32_5" id="qx32_5" value="1" <?php echo set_checkbox('qx32_5', '1'); ?> /> Orçamento Próprio</label><br />
                                                    <label><input type="checkbox" name="qx32_6" id="qx32_6" value="1" <?php echo set_checkbox('qx32_6', '1'); ?> /> Nenhum</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>33 - Selecione a principal organização mantenedora da biblioteca*</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <label><input type="radio" name="qx33" id="qx33" value="1" <?php echo set_radio('qx33', '1'); ?> /> Órgão Público Federal</label><br />
                                                    <label><input type="radio" name="qx33" id="qx33" value="2" <?php echo set_radio('qx33', '2'); ?> /> Órgão Público Estadual ou Distrital</label><br />
                                                    <label><input type="radio" name="qx33" id="qx33" value="3" <?php echo set_radio('qx33', '3'); ?> /> Órgão Público Municipal</label><br />
                                                    <label><input type="radio" name="qx33" id="qx33" value="4" <?php echo set_radio('qx33', '4'); ?> /> Empresa</label><br />
                                                    <label><input type="radio" name="qx33" id="qx33" value="5" <?php echo set_radio('qx33', '5'); ?> /> Indivíduo</label><br />
                                                    <label><input type="radio" name="qx33" id="qx33" value="6" <?php echo set_radio('qx33', '6'); ?> /> Associação</label><br />
                                                    <label><input type="radio" name="qx33" id="qx33" value="7" <?php echo set_radio('qx33', '7'); ?> /> Fundação</label><br />
                                                    <label><input type="radio" name="qx33" id="qx33" value="8" <?php echo set_radio('qx33', '8'); ?> /> OSCIP</label><br />
                                                    <label><input type="radio" name="qx33" id="qx33" value="9" <?php echo set_radio('qx33', '9'); ?> /> ONG</label><br />
                                                    <label><input type="radio" name="qx33" id="qx33" value="10" <?php echo set_radio('qx33', '10'); ?> /> Outro: </label>
                                                    <input class="inputText" type="text" name="qx33_outro" id="qx33_outro" style="width:200px;" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx33_outro'); ?>" autocomplete="off" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>34 - Quantidade de funcionários da biblioteca que possuem ensino fundamental</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <input class="inputText" type="text" name="qx34_outro" id="qx34_outro" style="width:200px;" maxlength="6" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx34_outro'); ?>" autocomplete="off" onkeypress="return maskInteger(this, event)" />
                                                    <i>(Digite somente números.)</i>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>35 - Quantidade de funcionários da biblioteca que possuem ensino médio</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <input class="inputText" type="text" name="qx35_outro" id="qx35_outro" style="width:200px;" maxlength="6" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx35_outro'); ?>" autocomplete="off" onkeypress="return maskInteger(this, event)" />
                                                    <i>(Digite somente números.)</i>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>36 - Quantidade de funcionários da biblioteca que possuem ensino superior em biblioteconomia</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <input class="inputText" type="text" name="qx36_outro" id="qx36_outro" style="width:200px;" maxlength="6" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx36_outro'); ?>" autocomplete="off" onkeypress="return maskInteger(this, event)" />
                                                    <i>(Digite somente números.)</i>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>37 - Quantidade de funcionários da biblioteca que possuem ensino superior em outra área</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <input class="inputText" type="text" name="qx37_outro" id="qx37_outro" style="width:200px;" maxlength="6" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx37_outro'); ?>" autocomplete="off" onkeypress="return maskInteger(this, event)" />
                                                    <i>(Digite somente números.)</i>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>38 - Quantidade de funcionários da biblioteca que possuem pós-graduação</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" style="padding-bottom:10px;">
                                                    <input class="inputText" type="text" name="qx38_outro" id="qx38_outro" style="width:200px;" maxlength="6" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx38_outro'); ?>" autocomplete="off" onkeypress="return maskInteger(this, event)" />
                                                    <i>(Digite somente números.)</i>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:5px;padding-bottom:5px; background-color:#EFEFEF;" colspan="4">
                                                    <b>Relações Institucionais</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>39 - Programas Governamentais dos quais a biblioteca já se beneficiou*</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <label><input type="checkbox" name="qx39_1" id="qx39_1" value="1" <?php echo set_checkbox('qx39_1', '1'); ?> /> Agentes de Leitura – Ministério da Cultura</label><br />
                                                    <label><input type="checkbox" name="qx39_2" id="qx39_2" value="1" <?php echo set_checkbox('qx39_2', '1'); ?> /> Implantação – Ministério da Cultura</label><br />
                                                    <label><input type="checkbox" name="qx39_3" id="qx39_3" value="1" <?php echo set_checkbox('qx39_3', '1'); ?> /> Modernização – Ministério da Cultura</label><br />
                                                    <label><input type="checkbox" name="qx39_4" id="qx39_4" value="1" <?php echo set_checkbox('qx39_4', '1'); ?> /> Proler – Ministério da Cultura</label><br />
                                                    <label><input type="checkbox" name="qx39_5" id="qx39_5" value="1" <?php echo set_checkbox('qx39_5', '1'); ?> /> Sala Verde – Ministério do Meio Ambiente</label><br />
                                                    <label><input type="checkbox" name="qx39_6" id="qx39_6" value="1" <?php echo set_checkbox('qx39_6', '1'); ?> /> Telecentro – Ministério das Comunicações</label><br />
                                                    <label><input type="checkbox" name="qx39_7" id="qx39_7" value="1" <?php echo set_checkbox('qx39_7', '1'); ?> /> Arca das Letras - Ministério do Desenvolvimento Agrário</label><br />
                                                    <label><input type="checkbox" name="qx39_8" id="qx39_8" value="1" <?php echo set_checkbox('qx39_8', '1'); ?> /> Pontos de Cultura - Ministério da Cultura</label><br />
                                                    <label><input type="checkbox" name="qx39_9" id="qx39_9" value="1" <?php echo set_checkbox('qx39_9', '1'); ?> /> Pontos de Leitura - Ministério da Cultura</label><br />
                                                    <label><input type="checkbox" name="qx39_10" id="qx39_10" value="1" <?php echo set_checkbox('qx39_10', '1'); ?> /> PAC - Programa de Aceleração do Crescimento</label><br />
                                                    <label><input type="checkbox" name="qx39_11" id="qx39_11" value="1" <?php echo set_checkbox('qx39_11', '1'); ?> /> Nenhum</label><br />
                                                    <label><input type="checkbox" name="qx39_12" id="qx39_12" value="1" <?php echo set_checkbox('qx39_12', '1'); ?> /> Outro: </label>
                                                    <input class="inputText" type="text" name="qx39_outro" id="qx39_outro" style="width:200px;" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx39_outro'); ?>" autocomplete="off" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:10px;" colspan="4">
                                                    <b>40 - A biblioteca participa ou participou de:</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <label><input type="checkbox" name="qx40_1" id="qx40_1" value="1" <?php echo set_checkbox('qx40_1', '1'); ?> /> Rede e/ou sistema de biblioteca</label><br />
                                                    <label><input type="checkbox" name="qx40_2" id="qx40_2" value="1" <?php echo set_checkbox('qx40_2', '1'); ?> /> Programas de estágio supervisionado junto as Escolas de Biblioteconomia da região</label><br />
                                                    <label><input type="checkbox" name="qx40_3" id="qx40_3" value="1" <?php echo set_checkbox('qx40_3', '1'); ?> /> Processo de implantação do Plano Municipal do Livro e da Leitura (PMLL) no seu município</label><br />
                                                    <label><input type="checkbox" name="qx40_4" id="qx40_4" value="1" <?php echo set_checkbox('qx40_4', '1'); ?> /> Outro: </label>
                                                    <input class="inputText" type="text" name="qx40_outro" id="qx40_outro" style="width:200px;" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo set_value('qx40_outro'); ?>" autocomplete="off" />
                                                </td>
                                            </tr>

                                            <!-- FIM QUESTIONARIO -->

										<?php } ?>

                                        <tr>
                                            <td colspan="4" style="text-align: center; padding-top:15px; font-size:11px;">
                                                <label><input type="checkbox" name="aceite" id="aceite" value="S" <?php echo set_checkbox('aceite', 'S'); ?> /> <b>Atesto que as informações prestadas acima são verdadeiras e podem ser conferidas in loco.</b></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="text-align: center; padding-top:15px;">
                                                <input type="hidden" name="idsniic" id="idsniic" value="{idsniic}" />
                                                <input type="hidden" name="tipopessoa_aux" id="tipopessoa_aux" value="{tipopessoa_aux}" />
										<?php
										if ($nPag > 0)
											echo "<input type='hidden' name='nPag' id='nPag' value='$nPag' />\n";
										?>
                                                <input class="buttonPadrao" id="btnSaveForm" name="btnSaveForm" type="submit" value="  Salvar  " />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="text-align: center; padding-top:5px; font-size:11px;">
                                                (*) Campos de preenchimento obrigatório.
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div><!-- #container-->
            </div><!-- #middle-->
        </div><!-- #wrapper -->
<?php monta_footer(); ?>
    </body>
</html>