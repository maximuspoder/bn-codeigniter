<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>autocad.js" type="text/javascript"></script>
	<script language="javascript">
		window.onpageshow = function(evt){ $('btnSaveForm').disabled = false; }
	</script>
</head>
<body>
	<div id="wrapper">
		
		<?php monta_header(0); ?>
		
		<div id="middle">
			<div id="container" style="text-align:center;">
				<div class="main_content_center" style="margin-top:10px; margin-bottom:20px;">
					<?php
						$erros = Array();
						
						if (isset($outrosErros))
						{
							if (is_array($outrosErros))
							{
								for ($i = 0; $i < count($outrosErros); $i++)
								{
									array_push($erros, $outrosErros[$i]);
								}
							}
							else
							{
								array_push($erros, $outrosErros);
							}
						}
						
						exibe_validacao_erros($erros);
					?>
					<form name="form_cad" id="form_cad" method="post" action="<?php echo URL_EXEC; ?>autocad/preautocad/verifica/cnb" onsubmit="return validaForm_preCad()">
						<table id="tabelaCadastros" align="center" border="0">
							<tr>
								<td style="font-size: 22px; font-weight: bold; padding-top: 15px; padding-bottom:20px;">- Cadastro Nacional de Bibliotecas -</td>
							</tr>
						</table>
						
						<table id="tabelaCadastros" align="center" border="0" style="width:400px;">
							<tr id="linhatitulo">
								<td colspan="4" class="areaCadastro2" style="padding-top:30px;">Informe o tipo de pessoa</td>
							</tr>
							<tr id="linhatipopessoa">
								<td style="padding-top:15px; text-align: center;" colspan="2">
									<input type="radio" name="tipopessoa" id="pjur" value="PJ" onclick="setTipoPessoa('PJ')" <?php echo set_radio('tipopessoa', 'PJ'); ?> /><br /><label for="pjur">Pessoa Jurídica</label>
								</td>
								<td style="padding-top:15px; text-align: center;" colspan="2">
									<input type="radio" name="tipopessoa" id="pfis" value="PF" onclick="setTipoPessoa('PF')" <?php echo set_radio('tipopessoa', 'PF'); ?> /><br /><label for="pfis">Pessoa Física</label>
								</td>
							</tr>
							<tr id="linhacpfcnpj" style="display:none;">
								<td style="padding-top:15px; text-align:center;" colspan="4">
									<span id="spantipopessoa">CNPJ</span>*: 
									<input class="inputText" type="text" name="cnpjcpf" id="cnpjcpf" style="width:130px;" maxlength="18" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{cnpjcpf}<?php echo set_value('cnpjcpf'); ?>" autocomplete="off" onkeypress="return maskInteger(this, event)" onKeyUp="maskCnpj(this)" />
								</td>
							</tr>
							<tr id="linhacont" style="display:none;">
								<td colspan="4" style="text-align: center; padding-top:30px;">
									<input type="hidden" name="tipocadastro" id="tipocadastro" value="2" />
                                    <input type="hidden" name="verifica" id="verifica" value="{verifica}" />
									<input type="hidden" name="tipopessoa_aux" id="tipopessoa_aux" value="{tipopessoa_aux}" />
									<input class="buttonPadrao" id="btnSaveForm" name="btnSaveForm" type="submit" value="  Continuar  " />
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
</body>
</html>