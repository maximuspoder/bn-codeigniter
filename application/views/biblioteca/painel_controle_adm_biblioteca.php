<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.tablesorter.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
</head>
<body>
	<?php monta_header(1); ?>
	<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
	<?php add_elementos_CONFIG(); ?>
	<div id="page_content">
		<div id="inside_content">
			<div>
				<div class="inline"><h1><a href="<?php echo URL_EXEC;?>biblioteca/search" class="black font_shadow_gray">Bibliotecas</a></h1></div>
				<div class="inline"><h3 class="font_shadow_gray">&nbsp;> Painel de Controle Biblioteca</h3></div>
			</div>
			
			<br />
			<br />
			<div class="font_shadow_gray" style="margin-bottom:5px;"><h4>Dados do Cadastro</h4></div>
			<hr class="left" style="width:563px;margin-left:0;" />
			<div id="box_group_view" class="inline top" style="display:block;width:560px;margin-bottom:30px;">
				<div style="float:right;margin:7px 10px 0 0;"><img src="<?php echo URL_IMG ?><?php echo (get_value($dados, 'HABILITADO') == 'S') ? 'icon_flag_green.png' : 'icon_flag_red.png'; ?>"       <?php echo (get_value($dados, 'HABILITADO') == 'S') ? ' title="HABILITADO PARA ESTE EDITAL"' :  ' title="NÃO HABILITADO PARA ESTE EDITAL"'; ?> /></div>
				<div style="float:right;margin:7px 10px 0 0;"><img src="<?php echo URL_IMG ?><?php echo (get_value($dados, 'ATIVO')      == 'S') ? 'icon_bullet_green_2.png' : 'icon_bullet_red.png'; ?>" <?php echo (get_value($dados, 'ATIVO')      == 'S') ? ' title="USUÁRIO ATIVO"' :  ' title="USUÁRIO NÃO ATIVO"'; ?> /></div>
				<div class="odd">
					<div id="label_view">ID Usuario:</div>
					<div id="field_view" style="width:350px"><?php echo(get_value($dados, 'IDBIBLIOTECA'));?></div>
				</div>
				<div>
					<div id="label_view">Nome Biblioteca:</div>
					<div id="field_view" style="width:420px;"><?php echo(get_value($dados, 'RAZAOSOCIAL'));?></div>
				</div>
				<div class="odd">
					<div id="label_view">CPF / CNPJ:</div>
					<div id="field_view" style="width:420px;"><?php echo(get_value($dados, 'CPF_CNPJ'));?></div>
				</div>
				<div>
					<div id="label_view">Endereço:</div>
					<div id="field_view" style="width:420px;"><?php echo get_value($dados, 'NOMELOGRADOUROTIPO') . ' ' . get_value($dados, 'NOMELOGRADOURO') . ', ' . get_value($dados, 'END_NUMERO') . ' ' . get_value($dados, 'END_COMPLEMENTO') . ' - ' . get_value($dados, 'NOMEBAIRRO') . ' - ' . get_value($dados, 'NOMECIDADE') . ' - ' . get_value($dados, 'IDUF') . ' - CEP: ' . get_value($dados, 'CEP');?></div>
				</div>
				<div class="odd">
					<div id="label_view">Telefone:</div>
					<div id="field_view" style="width:420px;"><?php echo(get_value($dados, 'TELEFONEGERAL'));?></div>
				</div>
				<div>
					<div id="label_view">Email Geral:</div>
					<div id="field_view" style="width:420px;"><?php echo(get_value($dados, 'EMAILGERAL'));?></div>
				</div>
				<div class="odd">
					<div id="label_view">Nome do Responsável:</div>
					<div id="field_view" style="width:420px;"><?php echo(get_value($dados, 'NOMERESP'));?></div>
				</div>
				<div>
					<div id="label_view">Email do Responsável:</div>
					<div id="field_view" style="width:420px;"><?php echo(get_value($dados, 'EMAILRESP'));?></div>
				</div>
			</div>
			<div class="inline top" style="width:340px;margin:-35px 0 0 30px;">
				<h5 style="margin-bottom:14px;">Pendências para este Programa/Edital</h5>
				<?php echo $pendencias; ?>
				
				<br />
				<h5 style="margin-bottom:5px;">Ações para esta Biblioteca</h5>
				<?php //mensagem('warning', '', 'Em construção. Liberação nas próximas versões.'); ?>
				<!--
				<button style="width:340px;" onclick=""><div style="margin:0 0 0 -5px;"><img src="<?php echo URL_IMG;?>icon_edit.png" style="margin:6px 3px 0 0;" /><div><h4>Editar dados Biblioteca</h4></div></div></button><br /><br />
				<button style="width:340px;" onclick=""><div style="margin:0 0 0 -5px;"><img src="<?php echo URL_IMG;?>icon_cancelar.png" style="margin:6px 3px 0 0;" /><div><h4>Cancelar PDV Parceiro</h4></div></div></button><br /><br />
				<a href="javascript:void(0)">Desbloquear biblioteca</a><br />
				<a href="javascript:void(0)">Habilitar biblioteca (para este edital)</a><br />
				<a href="javascript:void(0)">Reativar pedido</a><br />
				<a href="javascript:void(0)">Cancelar ponto de venda parceiro</a><br />
				-->
				<a style="margin-botton:5px" href="<?php echo URL_EXEC?>biblioteca/form_update_biblioteca/<?php echo get_value($dados, 'IDBIBLIOTECA')?>/?url=biblioteca/painel_controle_adm_biblioteca/<?php echo get_value($dados, 'IDBIBLIOTECA')?>">Editar dados desta Biblioteca</a><br />
				<a href="javascript:void(0)" onclick="ajax_modal('Visualizar Dados', '<?php echo URL_EXEC?>biblioteca/modal_form_view_biblioteca/<?php echo get_value($dados, 'IDBIBLIOTECA')?>', '<?php echo URL_IMG?>icon_view.png', 900, 800);">Visualizar dados deste Biblioteca</a><br />
			</div>
			
			
			<br />
			<br />
			<div style="margin-bottom:5px;">
				<img class="inline" id="box_resp_financ_img" src="<?php echo(URL_IMG)?>icon_bullet_minus.png" onclick="show_area('box_resp_financ');" style="float:left;cursor:pointer;margin:5px 5px 0 0;" />
				<div class="inline font_shadow_gray"><h4 style="cursor:pointer" onclick="show_area('box_resp_financ')">Responsável(is) Financeiro(s)</h4></div>
			</div>
			<hr />
			<div id="box_resp_financ" style="margin-top:5px;display:block;"><?php echo $table_resp_financ; ?></div>
			
			
			<br />
			<br />
			<br />
			<div style="margin-bottom:5px;">
				<img class="inline" id="box_comite_acervo_img" src="<?php echo(URL_IMG)?>icon_bullet_minus.png" onclick="show_area('box_comite_acervo');" style="float:left;cursor:pointer;margin:5px 5px 0 0;" />
				<div class="inline font_shadow_gray"><h4 style="cursor:pointer" onclick="show_area('box_comite_acervo')">Comitê de Acervo</h4></div>
			</div>
			<hr />
			<div id="box_comite_acervo" style="margin-top:5px;display:block;"><?php echo $table_comite_acervo; ?></div>
			
			
			<br />
			<br />
			<br />
			<div style="margin-bottom:5px;">
				<img class="inline" id="box_pdv_parceiro_img" src="<?php echo(URL_IMG)?>icon_bullet_minus.png" onclick="show_area('box_pdv_parceiro');" style="float:left;cursor:pointer;margin:5px 5px 0 0;" />
				<div class="inline font_shadow_gray"><h4 style="cursor:pointer" onclick="show_area('box_pdv_parceiro')">Ponto de Venda Parceiro</h4></div>
			</div>
			<hr />
			<div id="box_pdv_parceiro" style="margin-top:5px;display:block;"><?php echo $table_pdv_parceiro; ?></div>
			
			
			<br />
			<br />
			<br />
			<div style="margin-bottom:5px;">
				<img class="inline" id="box_pedidos_img" src="<?php echo(URL_IMG)?>icon_bullet_minus.png" onclick="show_area('box_pedidos');" style="float:left;cursor:pointer;margin:5px 5px 0 0;" />
				<div class="inline font_shadow_gray"><h4 style="cursor:pointer" onclick="show_area('box_pedidos')">Pedidos para este Programa/Edital</h4></div>
			</div>
			<hr />
			<div id="box_pedidos" style="margin-top:5px;display:block;"><?php echo $table_pedidos;?></div>
			
			
			<div class="middle" style="margin:30px 0 10px 0;">
				<hr />
				<div class="inline top" style="margin:10px 5px 0 0;"><button onclick="window.location.href='<?php echo URL_EXEC?>biblioteca/search'">OK</button></div>
				<div class="inline top" style="padding:17px 0 0 0;">ou&nbsp;&nbsp;<a href="<?php echo URL_EXEC;?>biblioteca/search">voltar</a></div>
			</div>
			<br />
		</div>
	</div>
</body>
</html>