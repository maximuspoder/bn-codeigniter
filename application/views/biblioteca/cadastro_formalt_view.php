<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>biblioteca.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>cep.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>powercombo.js" type="text/javascript"></script>
	<script language="javascript">
		window.onpageshow = function(evt){ $('btnSaveForm').disabled = false; }
	</script>
</head>
<body onload="biblioteca_formalt_onload();">
	<div id="wrapper">
		<?php 
		add_elementos_CONFIG();
		add_div_CEP();
		monta_header(1);
		monta_menu($this->session->userdata('tipoUsuario'));
		?>
		<div id="middle">
			<div id="container">
				<div class="main_content_left" style="margin-top:10px; margin-bottom:20px;">
					<?php
						$erros = Array();
						
						if (isset($outrosErros))
						{
							if (is_array($outrosErros))
							{
								for ($i = 0; $i < count($outrosErros); $i++)
								{
									array_push($erros, $outrosErros[$i]);
								}
							}
							else
							{
								array_push($erros, $outrosErros);
							}
						}
						
						exibe_validacao_erros($erros);
					?>
					<form name="form_cad" id="form_cad" method="post" action="<?php echo URL_EXEC; ?>biblioteca/form_alt/save" onsubmit="return validaFormAlt_biblioteca()">
						<fieldset style="width:630px; margin-left:20px;">
							<legend><b>Atualização de Cadastro</b></legend>
							<div id="fs_div">
								<table id="tabelaCadastros" border="0">
									<tr>
										<td colspan="4" class="areaCadastro2"><img src="<?php echo URL_IMG; ?>geral.png" style="padding-right:10px;" /> Dados gerais</td>
									</tr>
									<tr>
										<td style="padding-top:10px;"></td>
										<td style="padding-top:10px; text-align: right;" colspan="3">
											<input type="radio" <?php echo ($tipopessoa2 != 'PJ' ? 'disabled="disabled"' : ''); ?> name="tipopessoa" id="tipopessoa" value="PJ" <?php echo set_radio('tipopessoa', 'PJ', TRUE); ?> /> Pessoa Jurídica
											<input type="radio" <?php echo ($tipopessoa2 != 'PF' ? 'disabled="disabled"' : ''); ?> name="tipopessoa" id="tipopessoa" value="PF" <?php echo set_radio('tipopessoa', 'PF'); ?> /> Pessoa Física 
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<span id="spantipopessoa">CNPJ</span>*: 
											<input class="inputText" type="text" name="cnpjcpf" id="cnpjcpf" style="width:130px;" maxlength="18" readonly="readonly" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{cnpjcpf}<?php echo set_value('cnpjcpf'); ?>" autocomplete="off" onkeypress="return maskInteger(this, event)" onKeyUp="maskCnpj(this)" />
										</td>
									</tr>
									<tr id="linhaie">
										<td>Inscrição Estadual:</td>
										<td colspan="3"><input class="inputText" type="text" name="iestadual" id="iestadual" style="width:170px;" maxlength="20" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{iestadual}<?php echo set_value('iestadual'); ?>" autocomplete="off" /></td>
									</tr>
									<tr>
										<td><span id="spannome">Razão Social</span>*:</td>
										<td colspan="3"><input class="inputText" type="text" name="razaosocial" id="razaosocial" style="width:475px;" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{razaosocial}<?php echo set_value('razaosocial'); ?>" autocomplete="off" /></td>
									</tr>
									<tr id="linhanf">
										<td>Nome Fantasia:</td>
										<td colspan="3"><input class="inputText" type="text" name="nomefantasia" id="nomefantasia" style="width:475px;" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{nomefantasia}<?php echo set_value('nomefantasia'); ?>" autocomplete="off" /></td>
									</tr>
									<tr>
										<td>Natureza Jurídica*:</td>
										<td colspan="3">
											<select class="select" name="naturezajur" id="naturezajur" style="width:480px;">
												<option value="0" <?php echo set_select('naturezajur', 0); ?> >Selecione</option>
												<?php
												$auxPai = 0;
												foreach($natjur as $row)
												{
													if ($row['IDPAI'] != $auxPai)
													{
														if ($auxPai != 0)
														{
															echo '</optgroup>';
														}
														
														echo '<optgroup label="' . $row['NOMEPAI'] . '">';
														
														$auxPai = $row['IDPAI'];
													}
													
													echo '<option value="' . $row['IDNATUREZAJUR'] . '" ' . set_select('naturezajur', $row['IDNATUREZAJUR']) . ' >' . $row['NOMENATUREZAJUR'] . '</option>';
												}
												echo '</optgroup>';
												?>
											</select>
										</td>
									</tr>
									<tr>
										<td>Telefone Geral*:</td>
										<td colspan="3">
											<input class="inputText" type="text" name="telefone1" id="telefone1" style="width:130px;" maxlength="14" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{telefone1}<?php echo set_value('telefone1'); ?>" autocomplete="off" onKeyUp="maskx(this, msk_telefone)" /> 
											<span style="font-size:11px;">(xx) xxxx.xxxx</span>
										</td>
									</tr>
									<tr>
										<td>E-mail Geral*:</td>
										<td colspan="3"><input class="inputText" type="text" name="email" id="email" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{email}<?php echo set_value('email'); ?>" autocomplete="off" style="text-transform:none;width:323px;" /></td>
									</tr>
									<tr>
										<td>Site:</td>
										<td colspan="3"><input class="inputText" type="text" name="site" id="site" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{site}<?php echo set_value('site'); ?>" autocomplete="off" style="text-transform:none;width:323px;" /></td>
									</tr>
									<tr>
										<td>Login:</td>
										<td colspan="3">
											<input class="inputText" type="text" name="login" id="login" maxlength="20" value="{login}<?php echo set_value('login'); ?>" style="text-transform:none;width:130px;" readonly="readonly" /> 
										</td>
									</tr>
									<tr>
										<td colspan="4" class="areaCadastro2" style="padding-top:10px;"><img src="<?php echo URL_IMG; ?>endereco.png" style="padding-right:10px;" /> Endereço</td>
									</tr>
									<tr>
										<td style="padding-top:10px;">CEP*:</td>
										<td style="padding-top:10px;" colspan="3">
											<input type="hidden" name="idlogradouro" id="idlogradouro" value="{idlogradouro}<?php echo set_value('idlogradouro'); ?>" />
											<input class="inputText" type="text" name="cep" id="cep" style="width:80px;" maxlength="8" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{cep}<?php echo set_value('cep'); ?>" autocomplete="off" onkeypress="return maskInteger(this, event)" />
											<img alt="Pesquisar CEP" title="Pesquisar CEP" src="<?php echo URL_IMG; ?>pesq1.gif" style="cursor:pointer;" onclick="pesquisaCep('cep','cep#idlogradouro#uf#cidade#bairro#logradouro#logcomplemento#end_numero');" />
											<img alt="Limpar endereço" title="Limpar endereço" src="<?php echo URL_IMG; ?>x2.gif" style="cursor:pointer;" onclick="apagaCep('cep','cep#idlogradouro#uf#cidade#bairro#logradouro#logcomplemento');" />
										</td>
									</tr>
									<tr>
										<td>UF:</td>
										<td colspan="3">
											<input class="inputText inputDisab" readonly="readonly" type="text" name="uf" id="uf" style="width:40px;" value="{uf}<?php echo set_value('uf'); ?>" autocomplete="off" />
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											Cidade:
											<input class="inputText inputDisab" readonly="readonly" type="text" name="cidade" id="cidade" style="width:300px;" value="{cidade}<?php echo set_value('cidade'); ?>" autocomplete="off" />
										</td>
									</tr>
									<tr>
										<td>Bairro:</td>
										<td colspan="3">
											<input class="inputText inputDisab" readonly="readonly" type="text" name="bairro" id="bairro" style="width:410px;" value="{bairro}<?php echo set_value('bairro'); ?>" autocomplete="off" />
										</td>
									</tr>
									<tr>
										<td>Logradouro:</td>
										<td colspan="3">
											<input class="inputText inputDisab" readonly="readonly" type="text" name="logradouro" id="logradouro" style="width:410px;" value="{logradouro}<?php echo set_value('logradouro'); ?>" autocomplete="off" />
											<input class="inputText inputDisab" readonly="readonly" type="hidden" name="logcomplemento" id="logcomplemento" size="20" value="{logcomplemento}<?php echo set_value('logcomplemento'); ?>" autocomplete="off" />
										</td>
									</tr>
									<tr>
										<td>Número*:</td>
										<td colspan="3">
											<input class="inputText" type="text" name="end_numero" id="end_numero" style="width:93px;" maxlength="8" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{end_numero}<?php echo set_value('end_numero'); ?>" autocomplete="off" />
											&nbsp;&nbsp;&nbsp;&nbsp;
											Complemento:
											<input class="inputText" type="text" name="end_complemento" id="end_complemento" style="width:210px;" maxlength="30" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{end_complemento}<?php echo set_value('end_complemento'); ?>" autocomplete="off" />
										</td>
									</tr>
									<tr>
										<td colspan="4" class="areaCadastro2" style="padding-top:15px;"><img src="<?php echo URL_IMG; ?>resp.png" style="padding-right:10px;" /> Responsável junto à Biblioteca Nacional</td>
									</tr>
									<tr>
										<td style="padding-top:10px;">Nome*:</td>
										<td style="padding-top:10px;" colspan="3"><input class="inputText" type="text" name="nomeresp" id="nomeresp" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{nomeresp}<?php echo set_value('nomeresp'); ?>" autocomplete="off" style="width:414px;" /></td>
									</tr>
									<tr>
										<td>Telefone*:</td>
										<td colspan="3">
											<input class="inputText" type="text" name="telefoneresp" id="telefoneresp" style="width:130px;" maxlength="14" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{telefoneresp}<?php echo set_value('telefoneresp'); ?>" autocomplete="off" onKeyUp="maskx(this, msk_telefone)" /> 
											<span style="font-size:11px;">(xx) xxxx.xxxx</span>
											&nbsp;&nbsp;&nbsp;
											Skype: 
											<input class="inputText" type="text" name="skyperesp" id="skyperesp" maxlength="30" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{skyperesp}<?php echo set_value('skyperesp'); ?>" autocomplete="off" style="text-transform:none;width:140px;" />
										</td>
									</tr>
									<tr>
										<td>E-mail*:</td>
										<td colspan="3"><input class="inputText" type="text" name="emailresp" id="emailresp" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{emailresp}<?php echo set_value('emailresp'); ?>" autocomplete="off" style="text-transform:none;width:323px;" /></td>
									</tr>
									<tr>
										<td colspan="4" style="text-align: center; padding-top:15px;">
											<input type="hidden" name="loadDadosEdit" id="loadDadosEdit" value="{loadDadosEdit}" />
											<input type="hidden" name="tipopessoa_aux" id="tipopessoa_aux" value="{tipopessoa_aux}" />
											<input type="hidden" name="naturezajur_aux" id="naturezajur_aux" value="{naturezajur_aux}" />
											<input class="buttonPadrao" type="button" value="  Voltar  " onclick="window.location.href='<?php echo URL_EXEC; ?>home'" />
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input class="buttonPadrao" id="btnSaveForm" name="btnSaveForm" type="submit" value="  Atualizar  " />
										</td>
									</tr>
									<tr>
										<td colspan="4" style="text-align: center; padding-top:5px; font-size:11px;">
											(*) Campos de preenchimento obrigatório.
										</td>
									</tr>
								</table>
							</div>
						</fieldset>
					</form>
				</div>
			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
</body>
</html>