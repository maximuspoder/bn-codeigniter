<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>biblioteca.js" type="text/javascript"></script>
</head>
<body>
	<div id="wrapper">
		<?php add_elementos_CONFIG(); ?>
		<?php monta_header(1); ?>
		<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
		
		<div class="divtitulo">
			<div class="divtitulo2">
				>> COMITÊ DE ACERVO - de 3 a 5 membros
			</div>
		</div>
		
		<div id="middle">
			<div id="container" style="text-align:center;">
				<div class="main_content_center" style="margin-top:10px;">
					<center>
						<input class="buttonPadrao" type="button" value="Cadastrar Novo Membro" onclick="window.location.href='<?php echo URL_EXEC; ?>biblioteca/comiteform'" />
					</center>
					
					<table cellspacing="0" align="center" cellpadding="0" class="extensions t95" style="margin-top:30px;">
						<thead>
							<tr class="header">
								<th>Status</th>
								<th>Nome</th>
								<th>CPF</th>
								<th class="w100">Opções</th>
							</tr>
							<tr class="header-separator">
								<td colspan="4">&nbsp;</td>
							</tr>
						</thead>
						<tbody>
							{comite}
							<tr class="{CORLINHA}">
								<td class="no-wrap tdcenter"><img src="<?php echo URL_IMG; ?>{IMGSTATUS}" title="{DESC_STATUS}" alt="{DESC_STATUS}" /></td>
								<td class="no-wrap pr_20 tdleft" style="padding-right:150px;">{NOMERESP}</td>
								<td class="no-wrap pr_20 tdleft">{CPF_CNPJ}</td>
								<td class="w100 pr_20 tdleft">
									<a class="red" href="<?php echo URL_EXEC;?>biblioteca/comiteform/edit/{IDUSUARIO}">Visualizar</a>
									&nbsp;&nbsp;&nbsp;
									<a class="red" href="javascript:void(0);" onclick="confDelComite({IDUSUARIO})">Excluir</a>
								</td>
							</tr>
							{/comite}
							
							<?php
							if ($vazio == TRUE)
							{
								?>
								<tr>
									<td colspan="4" class="no-wrap tdcenter">Nenhum membro cadastrado.</td>
								</tr>
								<?php 
							}
							?>
						</tbody>
					</table>
				</div>
			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
</body>
</html>