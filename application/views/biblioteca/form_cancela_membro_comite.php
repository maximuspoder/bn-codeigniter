<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			// Validação e máscaras
			$("#form_default").validationEngine({ inlineValidation:false , promptPosition : "centerRight", scroll : false });
			$("input:text").setMask();
		});
	</script>
</head>
<body>
	<?php monta_header(1); ?>
	<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
	<?php add_elementos_CONFIG(); ?>
	<div id="page_content">
		<div id="inside_content">
			<div>
				<div class="inline"><h1><a href="<?php echo URL_EXEC;?>biblioteca/lista_comite_acervo" class="black font_shadow_gray">Comitê de Acervo</a></h1></div>
				<div class="inline"><h3 class="font_shadow_gray">&nbsp;> Desativação de Cadastro</h3></div>
			</div>
			<br />
			<?php $ativo = get_value($dados, 'ativo');
				if($ativo == 'S')
				{
					mensagem('warning', '', 'O cadastro do usuário que está sendo visualizado será desativado. Esta ação não tem volta, e caso você precise reativa-lo novamente, terá de solicitar para nosso serviço de atendimento. Para continuar clique em ok, para desistir clique em cancelar');
			?>
			<form name="form_default" id="form_default" method="post" action="<?php echo URL_EXEC; ?>biblioteca/cancela_membro_comite_proccess/">
				<input type="hidden" name="idusuario" id="idusuario" value="<?php echo get_value($dados, 'IDUSUARIO'); ?>" />	
				<h4 class="font_shadow_gray" style="margin-top:35px;">Dados do Membro</h4>
				<hr>
				<div style="float:right;margin:-30px 0 0 0;"><?php echo get_value($dados, 'ATIVO_IMG'); ?></div>
				<div id="box_group_view" class="modal">
					
					<div class="odd">
						<div id="label_view">ID Usuário (#):</div>
						<div id="field_view"><?php echo(get_value($dados, 'IDUSUARIO'));?></div>
					</div>
					<div>
						<div id="label_view">Nome do Membro:</div>
						<div id="field_view"><?php echo(get_value($dados, 'NOME RESPONSAVEL'));?></div>
					</div>
					
					<div class="odd">
						<div id="label_view">Membro da Biblioteca:</div>
						<div id="field_view"><?php echo(get_value($dados, 'BIBLIOTECA'));?></div>
					</div>
					
					<div>
						<div id="label_view">CPF / CNPJ:</div>
						<div id="field_view"><?php echo(get_value($dados, 'CPF_CNPJ'));?></div>
					</div>
					<div class="odd">
						<div id="label_view">Email</div>
						<div id="field_view"><?php echo(get_value($dados, 'EMAIL'));?></div>
					</div>
					<div>
						<div id="label_view">Telefone:</div>
						<div id="field_view"><?php echo(get_value($dados, 'TELEFONE'));?></div>
					</div>
				</div>
				<div class="font_shadow_gray" style="margin:35px 0 5px 0;"><h6>Dados do Usuário</h6></div>
				<hr />
				<div id="box_group_view" class="modal">
					<div class="odd">
						<div id="label_view">Login:</div>
						<div id="field_view"><?php echo(get_value($dados, 'LOGIN'));?></div>
					</div>
					<div>
						<div id="label_view">Tipo Pessoa:</div>
						<div id="field_view"><?php echo (get_value($dados, 'TIPOPESSOA') == 'PJ') ? 'PESSOA JURÍDICA' : 'PESSOA FÍSICA';?></div>
					</div>
					<div class="odd">
						<div id="label_view">Cadastro Ativo:</div>
						<div id="field_view"><?php echo (get_value($dados, 'ATIVO') == 'S') ? 'ATIVADO (SIM)' : 'DESATIVADO (NÃO)';?></div>
					</div>
				</div>
				<div style="margin-top:35px">
					<hr />
					<div class="inline top"><button onclick="set_form_redirect('<?php echo $url_redirect; ?>', '', 'form_default', 'ok');">OK</button></div>&nbsp;&nbsp;&nbsp;&nbsp;
					<div class="inline top" style="padding:8px 0 0 5px">ou&nbsp;&nbsp;&nbsp;<a href="<?php echo URL_EXEC; ?><?php echo $url_redirect; ?>">cancelar</a></div>
				</div>
			</form>
			<?php 
			}else
			{
				mensagem('warning', '', 'Este usuário está desativado, para ativação do mesmo entre em contato com nosso setor de atendimento para maiores informações.');
			}
			?>
				<div style="margin-top:35px">
					<?php if($ativo == 'N'){?>
					<hr>
					<div class="inline top"><button onclick="redirect('<?php echo URL_EXEC . $url_redirect; ?>');">OK</button></div>&nbsp;&nbsp;&nbsp;&nbsp;
					<div class="inline top" style="padding:8px 0 0 5px">ou&nbsp;&nbsp;&nbsp;<a href="<?php echo URL_EXEC; ?><?php echo $url_redirect; ?>">voltar</a></div> 
					<?php }?>
				</div>
		</div>
	</div>
</body>
</html>