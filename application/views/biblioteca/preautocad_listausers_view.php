<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>autocad.js" type="text/javascript"></script>
</head>
<body>
	<div id="wrapper">
		
		<?php monta_header(0); ?>
		
		<div id="middle">
			<div id="container" style="text-align:center;">
				<div class="main_content_center" style="margin-top:10px;">
					
					<img src="<?php echo URL_IMG; ?>atencao.png" style="margin-top:25px;" /><br /><b>ATENÇÃO!</b> 
                    Os cadastros abaixo possuem o mesmo CPF/CNPJ. 
                    <br />
                    Clique na biblioteca que deseja cadastrar.
					
					<table cellspacing="0" align="center" cellpadding="0" class="extensions t600" style="margin-top:25px;">
						<thead>
							<tr class="header">
								<th>Razão Social</th>
								<th class="w100">Responsável</th>
							</tr>
							<tr class="header-separator">
								<td colspan="2">&nbsp;</td>
							</tr>
						</thead>
						<tbody>
							{usuarios}
                            <tr style="color: {cor};" onclick="{urlcadastro}">
								<td class="w100 no-wrap pr_20 tdleft">{NOME}</td>
								<td class="no-wrap pr_20 tdleft">{NOME_DIRIGENTE}</td>
							</tr>
							{/usuarios}
						</tbody>
					</table>
                    <br/>
                    <input type="button" value="Cadastrar Outra" class="buttonPadrao" onclick="<?php echo $cadoutro; ?>" /> 
				</div>
			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
</body>
</html>