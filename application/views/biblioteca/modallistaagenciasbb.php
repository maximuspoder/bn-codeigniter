<div id="container" style="overflow-y: auto; ">
    <div class="main_content_left">
        <fieldset style="width:600px; margin-left:20px;">
			<legend><b>Selecionar Agência BB</b></legend>
			<div id="fs_div">
				<div style="display:inline-block !important">
					UF: <select name="agencias_uf" id="agencias_uf" class="select" style="width:60px;" onchange="ajaxGetCidadesAgenciaBB(this.value);"><option value=""></option><?php echo($optionsUf); ?></select>
				</div>
				<div style="display:inline-block !important;margin:0 0 0 20px;">
					Cidade: <select name="agencias_cidade" id="agencias_cidade" class="select" style="width:130px;" onchange="ajaxGetBairrosAgenciaBB(this.value);"></select>
				</div>
				<div style="display:inline-block !important;margin:0 0 0 20px;">
					Bairro: <select name="agencias_bairro" id="agencias_bairro" class="select" style="width:130px;"></select>
				</div>
				<div style="display:inline-block !important;margin:0 0 0 20px;">
					<button class="buttonPadrao" onclick="ajaxGetAgencias();">Pesquisar</button>
				</div>
			</div>
		</fieldset>
		<div id="return_agencias_ajax" style="margin:10px 0 0 20px;width:618px;max-height:150px;overflow-y:auto;"></div>
    </div>
</div><!-- #container-->

