<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
</head>
<body>
	<div id="wrapper">
		<?php monta_header(1); ?>
		<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
		
		<div id="middle">
			<div id="container">
				<div class="main_content_left" style="margin-top:10px; margin-bottom:20px;">
					<?php
						$erros = Array();
						
						if (isset($outrosErros))
						{
							if (is_array($outrosErros))
							{
								for ($i = 0; $i < count($outrosErros); $i++)
								{
									array_push($erros, $outrosErros[$i]);
								}
							}
							else
							{
								array_push($erros, $outrosErros);
							}
						}
						
						exibe_validacao_erros($erros);
					?>
					<form name="form_edit_senha" id="form_edit_senha" method="post" action="<?php echo URL_EXEC; ?>home/form_alt_senha/save" onsubmit="return validaForm_altSenha()">
						<fieldset style="width:320px; margin-left:20px;">
							<legend>Alterar Senha de login</legend>
							<div id="fs_div">
								<table id="tabelaCadastros" border="0">
									<tr>
										<td>Senha Atual*:</td>
										<td>
											<input class="inputText" type="password" name="senhaatual" id="senhaatual" size="16" maxlength="20" onfocus="focusLiga(this)" onblur="focusDesliga(this)" style="text-transform:none;" />
										</td>
									</tr>
									<tr>
										<td>Nova Senha*:</td>
										<td>
											<input class="inputText" type="password" name="senhanova" id="senhanova" size="16" maxlength="20" onfocus="focusLiga(this)" onblur="focusDesliga(this)" style="text-transform:none;" />
										</td>
									</tr>
									<tr>
										<td>Confirme a Nova Senha*:</td>
										<td>
											<input class="inputText" type="password" name="senhanova2" id="senhanova2" size="16" maxlength="20" onfocus="focusLiga(this)" onblur="focusDesliga(this)" style="text-transform:none;" />
										</td>
									</tr>
									<tr>
										<td colspan="2" style="text-align: center; padding-top:15px;">
											<input class="buttonPadrao" type="button" value="  Voltar  " onclick="window.location.href='<?php echo URL_EXEC; ?>home'" />
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input class="buttonPadrao" type="submit" value="  Salvar  " />
										</td>
									</tr>
								</table>
							</div>
						</fieldset>
					</form>
				</div>
			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
</body>
</html>