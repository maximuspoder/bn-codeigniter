<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			// Validação e máscaras
			$("#form_default").validationEngine({ inlineValidation:false , promptPosition : "centerRight", scroll : false });
			$("input:text").setMask();
		});
	</script>
</head>
<body>
		<?php monta_header(1); ?>
		<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
	
	<div id="page_content">		
		<div id="inside_content">
			<div>
				<div class="inline"><h1><a href="<?php echo URL_EXEC;?>usuario/search" class="black font_shadow_gray">Usuário</a></h1></div>
				<div class="inline"><h3 class="font_shadow_gray">&nbsp;> Alterar Senha</h3></div>
			<br />
			<br />
			<?php mensagem('note', '', 'Utilize o formulário abaixo para realizar a alteração de senha do cadastro de usuário que está sendo visualizado. Campos com (*) são obrigatórios.'); ?>
			<br />
			<?php if(isset($senha_errada)){ mensagem('error', '', 'A senha digitada não confere com a atual.'); } ?>
			<br />
			<div class="font_shadow_gray" style="margin-bottom:5px;"><h4 style="cursor:pointer" onclick="show_area('box_group_view')">Dados do Usuário</h4></div>
			<hr />
			<div id="box_group_view" style="display:block;">
				<div class="odd">
					<div id="label_view">ID Usuário (#):</div>
					<div id="field_view"><?php echo(get_value($dados, '#'));?></div>
				</div>
				<div>
					<div id="label_view">Login:</div>
					<div id="field_view"><?php echo(get_value($dados, 'LOGIN'));?></div>
				</div>
				<div class="odd">
					<div id="label_view">CPF / CNPJ:</div>
					<div id="field_view"><?php echo(get_value($dados, 'CPF / CNPJ'));?></div>
				</div>
			</div>
			
			<br />
			<br />
			<form name="form_default" id="form_default" method="post" action="<?php echo URL_EXEC; ?>usuario/form_update_senha_proccess">
			<input type="hidden" name="idusuario" id="idusuario" value="<?php echo $idusuario; ?>" style="width:150px;" />
				<h4 class="font_shadow_gray">Nova Senha</h4>
				<hr />
				<br />
				<div class="form_label">Senha atual*:</div>
				<div class="form_field"><input type="password" name="senha_atual" id="senha_atual" class="validate[required,minSize[6]]" style="width:150px;" /></div>
				<br />
				<div class="form_label">Nova senha*:</div>
				<div class="form_field"><input type="password" name="nova_senha" id="nova_senha" class="validate[required,minSize[6]]"  style="width:150px;" /></div>
				<br />
				<div class="form_label">Confirme a Senha*:</div>
				<div class="form_field"><input type="password" name="nova_senha_confirma" id="nova_senha_confirma"  class="validate[required,minSize[6],equals[nova_senha]]" style="width:150px;" /></div>
				<br />				
					<hr />
					<div class="inline top"><button onclick="set_form_redirect('<?php echo $url_redirect; ?>', 'usuario/form_update_senha_process/<?php echo $idusuario; ?>', 'form_default', 'ok');">Ok</button></div>
					<div class="inline top" style="padding:8px 0 0 5px">ou&nbsp;&nbsp;&nbsp;<a href="<?php echo URL_EXEC;?>usuario/search">voltar</a></div> 
				</div>			
			</form>
			</div>		
		</div><!-- inside_content -->
	</div><!-- page_content -->
</body>
</html>