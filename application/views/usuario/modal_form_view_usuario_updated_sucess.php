<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
</head>
<body>
	<?php add_elementos_CONFIG(); ?>
	<div class="font_shadow_gray" style="margin-bottom:5px;"><h6>Nova Senha de Usuário</h6></div>
	<hr />
	<br />
	<?php 
	if(isset($sucesso) && $sucesso == false)
	{
		mensagem('error', 'Erro', "Aconteceu algo de errado e o email não pode ser enviado ao usuário.");
	?>
	<div style="margin-top:25px">
		<hr />
		<div class="inline top"><button onclick="parent.close_modal();">OK</button></div>
		<div class="inline top" style="margin:7px 0 0 5px">ou <a href="javascript:void(0);" onclick="parent.close_modal();">cancelar</a></div> 
	</div>	
	<?php
	} else {
		mensagem('success', 'Sucesso', "Um email com a nova senha foi encaminhado para o usuário."); 
	?>
	<div style="margin-top:25px">
		<hr />
		<div class="inline top"><button onclick="parent.close_modal();">OK</button></div>
		<div class="inline top" style="margin:7px 0 0 5px">ou <a href="javascript:void(0);" onclick="parent.close_modal();">cancelar</a></div> 
	</div>
	<?php
	}
	?>

</body>
</html>