<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
</head>
<body>
	<?php add_elementos_CONFIG(); ?>
	<div class="font_shadow_gray" style="margin-bottom:5px;"><h6>Dados do Usuário</h6></div>
	<hr />
	<div id="box_group_view" class="modal">
		<div style="float:right;margin:-30px 0 0 0;"><?php  ?></div>
		<div class="odd">
			<div id="label_view">ID Usuário (#):</div>
			<div id="field_view"><?php echo (get_value($dados, '#')); ?></div>
		</div>
		<div>
			<div id="label_view">Login:</div>
			<div id="field_view"><?php echo (get_value($dados, 'LOGIN')); ?></div>
		</div>

		<div>
			<div id="label_view">Tipo Usuário</div>
			<div id="field_view">
				<?php echo (get_value($dados, 'IDUSUARIOTIPO') == 1 ? 'Administrador' : ''); ?>
				<?php echo (get_value($dados, 'IDUSUARIOTIPO') == 2 ? 'Biblioteca' : ''); ?>
				<?php echo (get_value($dados, 'IDUSUARIOTIPO') == 3 ? 'Distribuidor' : ''); ?>
				<?php echo (get_value($dados, 'IDUSUARIOTIPO') == 4 ? 'Ponto de Venda' : ''); ?>
				<?php echo (get_value($dados, 'IDUSUARIOTIPO') == 5 ? 'Editora' : ''); ?>
				<?php echo (get_value($dados, 'IDUSUARIOTIPO') == 6 ? 'Financeiro - Biblioteca' : ''); ?>
				<?php echo (get_value($dados, 'IDUSUARIOTIPO') == 6 ? 'Comitê - Biblioteca' : ''); ?>
			</div>
		</div>
		<div class="odd">
			<div id="label_view">Tipo Pessoa:</div>
			<div id="field_view"><?php echo (get_value($dados, 'TIPO PESSOA')); ?></div>
		</div>		
		<div>
			<div id="label_view">CPF / CNPJ:</div>
			<div id="field_view"><?php echo (get_value($dados, 'CPF / CNPJ')); ?></div>
		</div>
		<div class="odd">
			<div id="label_view">Data de cadastro:</div>
			<div id="field_view"><?php echo format_date(get_value($dados, 'DT. CADASTRO'));  ?></div>
		</div>
		<div>
			<div id="label_view">Data de ativação:</div>
			<div id="field_view"><?php echo format_date(get_value($dados, 'DT. ATIVAÇÃO')); ?></div>
		</div>
	</div>
	<div style="margin-top:25px">
		<hr />
		<div class="inline top"><button onclick="parent.close_modal();">OK</button></div>
		<div class="inline top" style="margin:7px 0 0 5px">ou <a href="javascript:void(0);" onclick="parent.close_modal();">cancelar</a></div> 
	</div>	

</body>
</html>