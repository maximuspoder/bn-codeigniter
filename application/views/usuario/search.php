<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.tablesorter.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			// Validação e máscaras
			$("input:text").setMask();
		});
	</script>
</head>
<body>
	<?php monta_header(1); ?>
	<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
	<?php add_elementos_CONFIG(); ?>
	<div id="page_content_wide">
		<div id="inside_content">
			<h1 class="font_shadow_gray inline top" style="margin:10px 15px 0 0;">Usuários</h1>
			<div id="module_menu" class="inline top">
				<div class="inline">
					<img src="<?php echo URL_IMG;?>icon_excel.gif" style="margin-top:2px;" />
					<a href="javascript:void(0);" onclick="download_excel('<?php echo URL_EXEC?>usuario/download_excel');" class="black inline" title="Exporte a lista total de registros para o formato CSV"><h5>Exportar para Excel</h5></a>
				</div>
			</div>		
			<form id="form_filter" action="<?php echo(URL_EXEC);?>usuario/search" method="post">
				<div id="content">
					<div class="inline">
						<img src="<?php echo URL_IMG;?>icon_filter.png" style="float:left;margin:4px 5px 0 0" />
						<h5 class="black inline" style="margin:3px 0 0 0;">Filtros</h5>
					</div>
					<div id="label">ID(#): </div>
					<div id="field"><input type="text" name="u__idusuario" id="u__idusuario" alt="integer" class="mini" value="<?php echo get_value($filtros, 'u__idusuario'); ?>" style="width:50px" /></div>
					<div id="label">Ativo: </div>
					<div id="field">
						<select class="mini"  name="u__ativo" id="u__ativo" style="width:55px;">
							<option value="" <?php echo (get_value($filtros, 'u__ativo') == '') ? 'selected="selected"' : ''; ?>></option>
							<option value="S" <?php echo (get_value($filtros, 'u__ativo') == 'S') ? 'selected="selected"' : ''; ?>>SIM</option>
							<option value="N" <?php echo (get_value($filtros, 'u__ativo') == 'N') ? 'selected="selected"' : '';?>>NÃO</option>
						</select>
					</div>
					<div id="label">Login: </div>
					<div id="field"><input type="text" name="u__login" id="u__login" alt="" class="mini" value="<?php echo get_value($filtros, 'u__login');  ?>" style="width:150px" /></div>
					<div id="label">Tipo Usuário: </div>
					<div id="field"><input type="text" name="ut__nomeusuariotipo" id="ut__nomeusuariotipo" alt="" class="mini" value="<?php echo get_value($filtros, 'ut__nomeusuariotipo'); ?>" style="width:150px" /></div>					
					<div id="label"></div>
					<div id="field"><input type="submit" class="mini" value="Enviar" /></div>
				</div>				
			</form>
			<?php echo($module_table); ?>

		</div>
	</div>
</body>
</html>