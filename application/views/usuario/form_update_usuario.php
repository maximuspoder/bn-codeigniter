<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			// Validação e máscaras
			$("#form_default").validationEngine({ inlineValidation:false , promptPosition : "centerRight", scroll : false });
			$("input:text").setMask();
		});
	</script>
</head>
<body>
	<?php monta_header(1); ?>
	<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
	<?php add_elementos_CONFIG(); ?>
	<div id="page_content">
		<div id="inside_content">
			<div>
				<div class="inline"><h1><a href="<?php echo URL_EXEC;?>usuario/search" class="black font_shadow_gray">Usuários</a></h1></div>
				<div class="inline"><h3 class="font_shadow_gray">&nbsp;> Edição de Cadastro</h3></div>
			</div>
			<br />
			<?php mensagem('note', '', 'Utilize o formulário abaixo para editar os dados do cadastro que está sendo visualizado. Campos com (*) são obrigatórios.'); ?>
			<br />
			<br />
			<form name="form_default" id="form_default" method="post" action="<?php echo URL_EXEC; ?>usuario/form_update_usuario_proccess/">
				<input type="hidden" name="idusuario" id="idusuario" value="<?php echo get_value($dados, '#'); ?>" />
				<h4 class="font_shadow_gray">Dados do Usuário</h4>
				<hr />
				<div style="float:right;margin:-30px 0 0 0;"><?php echo get_value($dados, 'ATIVO_IMG'); ?></div>
				<div class="form_label">ID Usuario (#):</div>
				<div class="form_field" style="padding-top:6px;"><?php echo get_value($dados, '#'); ?></div>
				<br />
				<div class="form_label">Tipo Usuário:</div>
				<div class="form_field">
					<select name="idtipousuario" id="idtipousuario" style="width:162px;"><?php echo $options_status?></select>
				</div>
				<br />
				<div class="form_label">Login:</div>
				<div class="form_field"><input type="text" disabled="disabled" name="login" id="login" value="<?php echo get_value($dados, 'LOGIN');?>" style="width:162px;" /></div>
				<br />
				<div class="form_label">*CPF/CNPJ:</div>
				<div class="form_field"><input type="text" name="cpf_cnpj" id="cpf_cnpj" value="<?php echo get_value($dados, 'CPF / CNPJ');?>" class="validate[required,custom[<?php echo (get_value($dados, 'TIPOPESSOA') == 'PJ') ? 'cnpj' : 'cpf';?>]]" alt="<?php echo (get_value($dados, 'TIPOPESSOA') == 'PJ') ? 'cnpj' : 'cpf';?>" style="width:162px;" /></div>
				<br />	
				<div class="form_label">*Tipo Pessoa:</div>
				<div class="form_field">
					<select name="tipopessoa" id="tipopessoa" style="width:162px;" onchange="change_doc_mask($(this).val())">
						<option value="PF" <?php echo (get_value($dados, 'tipo pessoa') == 'PF') ? 'selected="selected"' : '';?>>PESSOA FÍSICA</option>
						<option value="PJ" <?php echo (get_value($dados, 'tipo pessoa') == 'PJ') ? 'selected="selected"' : '';?>>PESSOA JURÍDICA</option>
					</select>
				</div>
				<br />
				<div class="form_label">*Cadastro Ativo:</div>
				<div class="form_field">
					<select name="ativo" id="ativo" style="width:162px;">
						<option value="S" <?php echo((get_value($dados, 'USR_ATIVO') == 'S') ? 'selected="selected"' : ''); ?>>ATIVADO (SIM)</option>
						<option value="N" <?php echo((get_value($dados, 'USR_ATIVO') == 'N') ? 'selected="selected"' : ''); ?>>DESATIVADO (NÃO)</option>
					</select>
				</div>
				<div style="margin-top:35px">
					<hr />
					<div class="inline top"><button onclick="set_form_redirect('<?php echo $url_redirect; ?>', '', 'form_default', 'ok');">OK</button></div>&nbsp;&nbsp;&nbsp;&nbsp;
					<div class="inline top"><button onclick="set_form_redirect('<?php echo $url_redirect; ?>', 'usuario/form_update_usuario/<?php echo get_value($dados, '#')?>', 'form_default', 'aplicar');">Aplicar</button></div>
					<div class="inline top" style="padding:8px 0 0 5px">ou&nbsp;&nbsp;&nbsp;<a href="<?php echo URL_EXEC; ?><?php echo $url_redirect; ?>">voltar</a></div> 
				</div>				
			</form>				
		</div>
	</div>
</body>
</html>