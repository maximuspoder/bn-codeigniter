<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			// Validação e máscaras
			$("#form_default").validationEngine({ inlineValidation:false , promptPosition : "centerRight", scroll : false });
			$("input:text").setMask();
		});
	</script>
</head>
<body>
	<?php monta_header(1); ?>
	<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
	<?php add_elementos_CONFIG(); ?>
	<div id="page_content">
		<div id="inside_content">
			<div>
				<div class="inline"><h1><a href="<?php echo URL_EXEC;?>biblioteca/search_resp_financeiros" class="black font_shadow_gray">Responsáveis Financeiros</a></h1></div>
				<div class="inline"><h3 class="font_shadow_gray">&nbsp;> Deleção de Cadastro</h3></div>
			</div>
			<br />
			<?php if($msg_erro != ''){?>
			<?php mensagem('warning', 'Atenção', 'Não é possível deletar este registro pois identificamos os seguintes problemas: <br />' . $msg_erro); ?>
			<div style="margin-top:35px">
				<hr />
				<div class="inline top"><button onclick="window.location.href='<?php echo URL_EXEC; ?><?php echo $url_redirect; ?>';">Voltar</button></div>&nbsp;&nbsp;&nbsp;&nbsp;
				<div class="inline top" style="margin:8px 0 0 0px">ou&nbsp;&nbsp;&nbsp;<a href="<?php echo URL_EXEC; ?><?php echo $url_redirect; ?>">cancelar</a></div> 
			</div>
			<?php } else {?>
			<?php mensagem('warning', '', 'Atenção! O registro que está sendo visualizado será deletado. Para continuar clique em ok, para desistir clique em cancelar.'); ?>
			<br />
			<form name="form_default" id="form_default" method="post" action="<?php echo URL_EXEC; ?>biblioteca/delete_responsavel_financeiro_proccess/">
				<input type="hidden" name="idusuario" id="idusuario" value="<?php echo get_value($dados, 'IDUSUARIO');?>" />
				<div class="font_shadow_gray" style="margin-bottom:5px;"><h4>Dados do Cadastro</h4></div>
				<div id="box_group_view" class="inline top">
					<div class="odd">
						<div id="label_view">ID Usuario:</div>
						<div id="field_view" style="width:800px"><?php echo(get_value($dados, 'IDUSUARIO'));?></div>
						<div style="float:right;margin:-25px 10px 0 0;"><img src="<?php echo URL_IMG ?><?php echo (get_value($dados, 'ATIVO') == 'S') ? 'icon_bullet_green_2.png' : 'icon_bullet_red.png'; ?>" <?php echo (get_value($dados, 'ATIVO') == 'S') ? ' title="USUÁRIO ATIVO"' :  ' title="USUÁRIO NÃO ATIVO"'; ?> /></div>
					</div>
					<div>
						<div id="label_view">Nome:</div>
						<div id="field_view"><?php echo(get_value($dados, 'RAZAOSOCIAL'));?></div>
					</div>
					<div class="odd">
						<div id="label_view">CPF / CNPJ:</div>
						<div id="field_view"><?php echo(get_value($dados, 'CPF_CNPJ'));?></div>
					</div>
					<div>
						<div id="label_view">Data Nascimento:</div>
						<div id="field_view"><?php echo(get_value($dados, 'DATANASC'));?></div>
					</div>
					<div class="odd">
						<div id="label_view">Endereço:</div>
						<div id="field_view"><?php echo get_value($dados, 'NOMELOGRADOUROTIPO') . ' ' . get_value($dados, 'NOMELOGRADOURO') . ', ' . get_value($dados, 'END_NUMERO') . ' ' . get_value($dados, 'END_COMPLEMENTO') . ' - ' . get_value($dados, 'NOMEBAIRRO') . ' - ' . get_value($dados, 'NOMECIDADE') . ' - ' . get_value($dados, 'IDUF') . ' - CEP: ' . get_value($dados, 'CEP');?></div>
					</div>
					<div>
						<div id="label_view">Telefone:</div>
						<div id="field_view"><?php echo(get_value($dados, 'TELEFONERESP'));?></div>
					</div>
					<div class="odd">
						<div id="label_view">Email:</div>
						<div id="field_view"><?php echo(get_value($dados, 'EMAILRESP'));?></div>
					</div>
					<div>
						<div id="label_view">Login:</div>
						<div id="field_view"><?php echo(get_value($dados, 'LOGIN'));?></div>
					</div>
				</div>
				<div style="margin-top:35px">
					<hr />
					<div class="inline top"><button onclick="set_form_redirect('<?php echo $url_redirect; ?>', 'form_default');">OK</button></div>&nbsp;&nbsp;&nbsp;&nbsp;
					<div class="inline top" style="margin:8px 0 0 0px">ou&nbsp;&nbsp;&nbsp;<a href="<?php echo URL_EXEC; ?><?php echo $url_redirect; ?>">cancelar</a></div> 
				</div>
			</form>
			<?php } ?>
		</div>
	</div>
</body>
</html>