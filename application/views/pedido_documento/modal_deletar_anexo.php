<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
</head>
<body>
	<div><?php mensagem('warning', 'Atenção', 'O registro de documento de ID <strong>' . $id_documento_anexo . '</strong> será deletado. Para continuar clique em ok, para sair clique em cancelar.');?>.</div>
	<form action="<?php echo URL_EXEC; ?>pedido_documento/modal_deletar_anexo_proccess" name="form_default" id="form_default" enctype="multipart/form-data" method="post">
		<input type="hidden" name="id_pedido_documento_anexo" id="id_pedido_documento_anexo" value="<?php echo($id_documento_anexo);?>" />
		<input type="hidden" name="id_pedido_documento" id="id_pedido_documento" value="<?php echo($id_pedido_documento);?>" />
		<div style="margin-top:-5px">
			<hr />
			<div class="inline top"><input type="submit" value="OK" /></div>
			<div class="inline middle" style="padding:7px 0 0 5px">ou <a href="<?php echo URL_EXEC?>pedido_documento/modal_anexos_documento/<?php echo $id_pedido_documento; ?>">cancelar</a></div> 
		</div>
	</form>
</body>
</html>