<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			// Validação e máscaras
			$("#form_default").validationEngine({ inlineValidation:false , promptPosition : "centerRight", scroll : false });
			
			// máscaras
			$('input:text').setMask();
		});
	</script>
	<!--
    <script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>biblioteca.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
	-->
</head>
<body>
	<div>Utilize o formulário abaixo para inserir um novo anexo ao documento referenciado. Campos com (*) são obrigatórios.</div>
	<br />
	<form action="<?php echo URL_EXEC; ?>pedido_documento/modal_inserir_anexo_proccess" name="form_default" id="form_default" enctype="multipart/form-data" method="post">
		<input type="hidden" name="id_pedido_documento" id="id_pedido_documento" value="<?php echo($id_pedido_documento);?>" />
		<div class="form_label">*Número DOC:</div>
		<div class="form_field"><input type="text" name="numero_doc" id="numero_doc" class="validate[required]" style="width:150px;" /></div>
		<br />
		<div class="form_label">Valor DOC:</div>
		<div class="form_field">
			<input type="text" name="valor_doc" id="valor_doc" class="validate[required]" style="width:150px;" alt="decimal" />
			<span class="comment" style="margin: 0 0 0 5px">Valor em reais do documento (opcional)</span>
		</div>
		<br />
		<div class="form_label">*Arquivo:</div>
		<div class="form_field" style="width:445px;">
			<input type="file" name="arquivo" id="arquivo" class="validate[required]" style="width:280px;" /><br />
			<span class="comment">Extensões suportadas: JPG, JPEG, PNG, GIF, BMP e PDF.</span><br />
			<span class="fnt_error font_09">Atenção! O arquivo a ser anexado deve possuir no máximo 5MB. Caso o arquivo possua um tamanho maior, deve ser reduzido utilizando algum programa específico para tal. As informações devem estar legíveis.</span>
		</div>
		<div style="margin-top:30px">
			<hr />
			<div class="inline top"><input type="submit" value="Enviar" /></div>
			<div class="inline middle" style="padding:7px 0 0 5px">ou <a href="<?php echo URL_EXEC?>pedido_documento/modal_anexos_documento/<?php echo $id_pedido_documento; ?>">cancelar</a></div> 
		</div>
	</form>
</body>
</html>