<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
</head>
<body>
	<h5 class="inline">Informações sobre a Conferência (para Biblioteca)</h5>
	<a class="inline" href="<?php echo URL_EXEC ?>application/files/help_conferencia_biblioteca.pdf" target="_blank" style="margin:0 0 0 5px;">Tutorial Conferência Biblioteca</a>
	<div>
	Realize nesta tela a conferência de pedidos. Abaixo você encontrará algumas dicas e informações importantes sobre como realizá-la.
	
	<br />
	<br />
	<strong>Dados do Pedido</strong><br />
	Nesta área estão os dados gerais do pedido da biblioteca indicando se já houve a finalização pelo PDV e Biblioteca. Para acessar os dados cadastrais da biblioteca/PDV clique no respectivo link.
	
	<br />
	<br />
	<strong>Notas fiscais (preenchimento pela Biblioteca)</strong><br />
	Insira aqui as notas fiscais entregues pelo PDV, referente aos livros solicitados:<br />
	1. Clique em inserir nota.<br />
	2. Informe o número da nota fiscal (certifique-se de que esse número deve estar igual ao que o PDV inserir).<br />
	3. Informe a quantidade de exemplares entregues nesta nota fiscal.<br />
	4. Informe o valor total desta nota fiscal.<br />
	5. Clique no botão disponível para anexar uma imagem. Respeite o tamanho limite de 5MB para cada arquivo inserido.<br />
	7. Clique em enviar.
	
	<br />
	<br />
	Conforme os arquivos são inseridos, ficam disponíveis para visualização através do ícone à direita da tabela.<br />
	À esquerda constam os ícones para apagar, visualizar os anexos do comprovante(nota de devolução, por exemplo), e editar as informações inseridas.
	
	<br />
	<br />
	<strong>Itens do pedido (preenchimento pela Biblioteca)</strong><br />
	Preencha aqui a quantidade de itens efetivamente entregue pelo PDV, indicando a respectiva nota fiscal onde este item está discriminado.<br />
	1. Deixe o campo de busca vazio e clique em enviar para visualizar o pedido completo, ou digite uma parte do título para localizar o título dentro do pedido (indicado para conexões lentas).<br />
	2. Preencha a quantidade recebida dos títulos (Ex.: se recebeu apenas um, de dois exemplares). O sistema irá calcular o valor a ser liberado conforme a quantidade recebida informada.<br />
	3. Clique em salvar caso pretenda terminar em outro momento a conferência das notas inseridas, ou por precaução para possíveis falhas na conexão durante o preenchimento dos dados.<br />
	4. Para desmarcar um item de uma nota apenas deixe o campo Nota em branco. Não altere a quantidade.
	
	<br />
	<br />
	<strong>Finalizar (para finalização das informações da biblioteca)</strong><br />
	1. Depois de preenchidos todos os itens das notas fiscais cadastradas, clique no botão Finalizar entrega. <br />
	2. Marque a caixa de seleção para confirmar as informações e clique em OK. <br />
	Ao finalizar, biblioteca e PDV estarão concordando com os dados informados.
	
	<br />
	<br />
	<strong>Atenção!</strong> Fique atento para as possíveis divergências que o sistema poderá apontar (Ex.: falta de comprovante de entrega para nota, divergências nos totais)
	
	<br />
	<br />
	<strong>Atenção!</strong> Não será possível efetuar alterações depois deste procedimento. Caso seja necessário reverter ou alterar alguma informação cadastrada anteriormente, entre em contato com nossa Central de Atendimento.
	</div>
</body>
</html>