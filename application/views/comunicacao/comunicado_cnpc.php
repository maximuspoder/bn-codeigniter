﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
</head>
<body>
	<div style="font-family:arial, tahoma, verdana;font-size:13px;">
	Prezados Senhores:<br /><br />
	O Ministério da Cultura deu início ao processo de renovação dos <a href="http://camaradolivro.mailee.me/go/click/219590772?key=4d9cd4&url=http%3A%2F%2Fwww.cultura.gov.br%2Fsetoriais%2F%3Futm_source%3DMailee%26utm_medium%3Demail%26utm_campaign%3Dmensagem%26utm_term%3D%26utm_content%3DColegiados%2BSetoriais%2Bdo%2BConselho%2BNacional%2Bde%2BPol%25C3%25ADtica%2BCultural%2B31%252F07" target="_blank">Colegiados Setoriais do Conselho Nacional de Política Cultural</a>.<br /><BR />
	Assim, os cidadãos que atuam em áreas culturais, a exemplo da área do livro, podem participar debatendo as temáticas do seu segmento e elegendo delegados estaduais, que por sua vez formarão o colégio eleitoral nacional para a escolha dos membros do Colegiados Setoriais do Conselho Nacional de Política Cultural – CNPC.<br /><BR />
	O número de delegados estaduais depende do número de eleitores inscritos, pelo que pedimos aos Associados que façam seu cadastro como eleitores. O prazo (já prorrogado) <strong>termina em 8 de agosto</strong>. Nossa Área de Atuação é a do Livro e Leitura.<br /><BR />
	A inscrição é toda pela internet. Veja abaixo como fazer sua inscrição:<br />
	1) Tenha seu RG digitalizado em formato PDF;<br />
	2) Tenha seu CPF digitalizado em formato PDF;<br />
	3) comprovante de atuação de três anos no setor:<br />
	&nbsp;&nbsp;&nbsp;&nbsp;3.1 -  pode ser um currículo, ou Diploma Profissional ou Registro Profissional no Ministério do Trabalho (DRT) ou<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;3.2 - participação em Entidade/Comunidade representativa da área ou segmento no formato .pdf com no máximo 1Mb:<br />
	4) Prepare uma breve apresentação de suas atividades ligadas à cultura (por exemplo: de 19_ _ a 2012 – atuação na Livraria Tal, ou Editora Tal, ou Distribuidora Tal, exercendo tal função; ou livreiro desde tal ano, ou participação na diretoria da entidade , etc.)<br />
	5) Após estar com os itens acima preparados acesse <a href="http://www.cultura.gov.br/setoriais/livro/cadastro/" target="_blank">http://www.cultura.gov.br/setoriais/livro/cadastro/</a> e preencha o cadastro.<br /><BR />
	Caso seja de seu interesse inscrever-se como Candidato, acesse o formulário próprio, ao final do Cadastro de Eleitor. Neste caso há mais alguns documentos que serão necessários. Veja a portaria completa aqui.<br /><BR />
	<div style="color:red">Lembre-se que essa é uma oportunidade para todos poderem expressar suas opiniões e fazermos valer nossas posições. Se nós, do setor do Livro e Leitrua do RS não participarmos, outros levarão suas bandeiras e nós ficaremos de fora dos espaços de decisão.</div><br /><br />
	<strong>Contamos com sua participação,</strong><br />
	Fundação Biblioteca Nacional
	</DIV>
	<div style="margin-top:20px;"><a href="javascript:void(0);" onclick="close_modal();">Clique aqui para fechar esta janela</a></div>
</body>
</html>