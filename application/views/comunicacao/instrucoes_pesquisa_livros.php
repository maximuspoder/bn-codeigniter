﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
</head>
<body>
	<div>Esta pesquisa busca resultados onde o termo digitado esteja contido no nome do livro, autor, editora, ISBN, ano ou edição.</div>
	<div style="margin-top:10px;">
		<b>Exemplo 1</b><br />
		Digamos que você queira buscar o livro "<em>IRACEMA</em>", então digita este termo no campo de pesquisa. A pesquisa pelo termo "<em>IRACEMA</em>" também será feita para autor e editora, por exemplo. Caso o nome de algum autor ou editora contenha o termo "<em>IRACEMA</em>", este registro também será retornado.
	</div>
	<div style="margin-top:10px;">Além disso, a pesquisa também tenta retornar os resultados que contenham no ISBN, ano ou edição exatamente o que foi digitado no campo de pesquisa.</div>
	<div style="margin-top:10px;">
		<b>Exemplo 2</b><br />
		Digamos que você queira buscar títulos do ano de 2005. Você digita este termo no campo de pesquisa e clica em pesquisar. A pesquisa varre todos os títulos que tenham em seu título a palavra "<em>2005</em>" ou então onde o ano seja exatamente igual a este mesmo termo.
	</div>
	<div style="margin-top:20px;"><a href="javascript:void(0);" onclick="close_modal();">Clique aqui para fechar esta janela</a></div>
</body>
</html>