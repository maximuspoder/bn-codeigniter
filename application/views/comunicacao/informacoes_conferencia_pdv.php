<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
</head>
<body>
	<h5 class="inline">Informações sobre a Conferência (para Ponto de Venda)</h5>
	<a class="inline" href="<?php echo URL_EXEC ?>application/files/help_conferencia_pdv.pdf" target="_blank" style="margin:0 0 0 5px;">Tutorial Conferência PDV</a>
	<div>
	Realize nesta tela a conferência de pedidos. Abaixo você encontrará algumas dicas e informações importantes sobre como realizá-la.
	
	<br />
	<br />
	<strong>Dados do Pedido</strong><br />
	Nesta área estão os dados gerais do pedido da biblioteca indicando se já houve a finalização pelo PDV e Biblioteca. Para acessar os dados cadastrais da biblioteca/PDV clique no respectivo link.
	
	<br />
	<br />
	<strong>Quantidade de Entregas (bloco informativo)</strong><br />
	Nesta área você encontra informações referentes às quantidades de entregas que poderá efetuar.

	<br />
	<br />
	<strong>Comprovantes de entrega (preenchimento pelo PDV)</strong><br />
	Depois de efetuar a entrega dos livros à biblioteca, insira aqui os canhotos de entrega assinados por um representante da biblioteca.<br />
	1. Clique em inserir comprovante.<br />
	2. Informe o número do comprovante (certifique-se de que esse número deve estar igual ao que a biblioteca inserir).<br />
	3. Informe a quantidade de exemplares entregues neste comprovante.<br />
	4. Informe o valor total deste comprovante.<br />
	5. Clique no botão disponível para anexar uma imagem. Respeite o tamanho limite de 5 MB para cada arquivo inserido. Sugerimos que o arquivo tenha formato PNG, em baixa resolução.<br />
	6. Clique em enviar.
	
	<br />
	<br />
	Conforme os arquivos são inseridos, ficam disponíveis para visualização clicando no ícone à direita da tabela.<br />
	À esquerda estão os ícones para apagar, visualizar os anexos do comprovante (nota de devolução, por exemplo), e para editar as informações inseridas.
	
	<br />
	<br />
	<strong>Finalizar (para finalização das informações do PDV)</strong><br />
	1. Depois de inserir os comprovantes referentes à 1ª entrega, não esqueça de clicar em "Finalizar entrega" para confirmar o envio dos comprovantes inseridos. Faça o mesmo depois que inserir os comprovantes da 2ª entrega.<br />
	2. Marque a caixa de seleção para confirmar as informações e clique em OK.
	
	<br />
	<br />
	<strong>Atenção!</strong> Não será possível efetuar alterações depois deste procedimento. Caso seja necessário reverter ou alterar alguma informação cadastrada anteriormente, entre em contato com nossa Central de Atendimento.
	</div>
</body>
</html>