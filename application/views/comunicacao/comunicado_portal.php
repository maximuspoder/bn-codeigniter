<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
</head>
<body>
	<h3 class="font_shadow_gray">Atenção!</h3><br />
	<h4 class="font_shadow_gray">O prazo final de entrega está prorrogado!</h4><br />
	Face ao grande número de pedidos ainda não entregues e visando garantir o recebimento dos títulos pelas bibliotecas, o prazo final das entregas foi prorrogado. A data final será informada nos próximos dias.<br /><br />
	Nosso objetivo é garantir o recebimento pelas bibliotecas do máximo de títulos solicitados.<br /><br />
	<strong>Pontos de Venda</strong> – Pedidos com mais de 50% dos títulos exigíveis devem ser entregues imediatamente. Contate-nos através do e-mail <a href="mailto:editais2011@bn.br">editais2011@bn.br</a> em caso de eventuais dificuldades na obtenção dos títulos. Veja a lista dos livros inexigíveis em sua área de trabalho.<br /><br />
	<strong>Bibliotecas</strong> – Mantenham contato com seu PDV parceiro, questionando sobre situação do seu pedido.<br /><br />
	<strong>Editores</strong> – Informem-nos através do e-mail <a href="mailto:editais2011@bn.br">editais2011@bn.br</a> eventuais dificuldades na entrega de títulos.
	<div style="margin-top:20px;"><a href="javascript:void(0);" onclick="close_modal();">Clique aqui para fechar esta janela</a></div>
</body>
</html>