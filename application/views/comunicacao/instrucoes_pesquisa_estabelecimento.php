﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
</head>
<body>
	<div>Esta pesquisa busca resultados onde o termo digitado esteja contido na razão social, nome fantasia ou CNPJ/CPF da entidade a ser procurada (bibliotecas, pontos de venda, editoras e distribuidores).</div>
	<div style="margin-top:10px;">Para esta consulta você poderá utilizar filtros de pesquisa, como UF, Cidade e Tipo de entidade onde serão procurados os registros. Para este último filtro, caso você selecione a opção bibliotecas então a pesquisa será feita com o termo que foi digitado, mas limitando apenas as bibliotecas cadastradas no sistema.</div>
	<div style="margin-top:10px;">
		<b>Exemplo 1</b><br />
		Digamos que você queira buscar a editora "<em>XYZ</em>", então digita este termo no campo de pesquisa e não realiza alterações nos filtros da pesquisa. A pesquisa pelo termo "<em>XYZ</em>" será feita para razão social, nome fantasia ou CNPJ/CPF de todos os tipos de entidade do sistema. O resultado não será apenas as editoras que contenham "<em>XYZ</em>" em seu nome, mas também bibliotecas, pontos de venda e distribuidores, caso exista algum com o mesmo termo contido em seu nome.
	</div>
	<div style="margin-top:10px;">
		<b>Exemplo 2</b><br />
		Digamos que você queira buscar somente bibliotecas do município do Rio de Janeiro contendo o termo "<em>PUB</em>" em sua razão social. Você então digita esta palavra no campo de pesquisa, altera a UF no filtro para RJ e seleciona BIBLIOTECAS na opção "Pesquisar Em". O resultado será todas as bibliotecas da UF RJ que contenham a o termo "<em>PUB</em>" em sua razão social, nome fantasia ou CPF/CNPJ.
	</div>
	<div style="margin-top:20px;"><a href="javascript:void(0);" onclick="close_modal();">Clique aqui para fechar esta janela</a></div>
</body>
</html>