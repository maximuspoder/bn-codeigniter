<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
</head>
<body>
	<?php monta_header(1); ?>
	<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
	<?php add_elementos_CONFIG(); ?>
	<div id="page_content">
		<div id="inside_content">
			<h1 class="font_shadow_gray"><u>Atenção!</u></h1>
			<img src="<?php echo URL_IMG?>icon_warning.png" style="float:left;margin:30px 6px 0 0" /><h3 class="font_shadow_gray inline" style="margin-top:30px;">Módulo em manutenção!</h3>
			<div style="margin-top:30px;" class="font_15">Com o intuito de melhorar o uso do Portal do Livro FBN, estamos realizando modificações e melhorias neste módulo. Em breve você terá acesso à este módulo, portanto, pedimos que você tente novamente mais tarde.</div>
			<div style="margin-top:30px;">Em caso de dúvidas, entre em contato conosco através dos seguintes números:</div>
			<div>
				&bull;&nbsp;(51) 3254-3241<br />
				&bull;&nbsp;(51) 3254-3242<br />
				&bull;&nbsp;(51) 3254-3243<br />
				&bull;&nbsp;(51) 3254-3244<br />
				&bull;&nbsp;(51) 3254-3245
				<br />
				Ou envie um email para:<br />
				<a href="mailto:editais2011@bn.br?subject=Dúvida Portal do Livro FBN">editais2011@bn.br</a>
			</div>
			<div style="margin-top:30px;"><a href="<?php echo URL_EXEC ?>home">Clique aqui para ir para a home</a></div>
		</div>
	</div>
</body>
</html>