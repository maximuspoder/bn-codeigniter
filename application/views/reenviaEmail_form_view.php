<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
        <link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>autocad.js" type="text/javascript"></script>
	<script language="javascript">
		window.onpageshow = function(evt){ $('btnSaveForm').disabled = false; }
	</script>
</head>
    <body onload="$('cpfcnpj').focus();">
	<div id="wrapper">
		<?php 
		monta_header(0);
		?>
		<div id="middle">
			<div id="container" style="text-align:center;">
                <?php
                    $erros = Array();

                    if (isset($outrosErros))
                    {
                        if (is_array($outrosErros))
                        {
                            for ($i = 0; $i < count($outrosErros); $i++)
                            {
                                array_push($erros, $outrosErros[$i]);
                            }
                        }
                        else
                        {
                            array_push($erros, $outrosErros);
                        }
                    }

                    exibe_validacao_erros($erros);
                ?>
				<div class="main_content_left" style="margin-top:10px; margin-bottom:20px;">
                                    
                    <fieldset style="width:350px; margin-left:20px;">
                            <legend><b>Reenviar o e-mail de validação</b></legend>

                            <form name="form_cad_cpfcnpj" id="form_cad_cpfcnpj" method="post" action="<?php echo URL_EXEC; ?>autocad/renviaEmail/save" onsubmit="return validaForm_enviaEmail()">
                                <div id="fs_div">
                                    <table id="tabelaCadastros" align="center" border="0">
                                        <tr>
                                            <td class="areaCadastro2">Informe seu CPF ou CNPJ</td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top:10px; text-align: center;">
                                                <input class="inputText" type="text" name="cpfcnpj" id="cpfcnpj" maxlength="14" value="<?php echo set_value('cpfcnpj'); ?>" autocomplete="off" onkeypress="return maskInteger(this, event)" onfocus="focusLiga(this)" onblur="focusDesliga(this)" style="text-transform:none; width:120px;" />
                                                <p style="font-size: 10px;">(Somente números)</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="text-align: center; padding-top:5px;">
                                                <input class="buttonPadrao" id="btnSaveForm" name="btnSaveForm" type="submit" value="  Enviar  " />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </form>
                    </fieldset>
				</div>
					<?php if(isset($listaCadastro)) {?>
					<div class="main_content_left" style="margin-left:20px; margin-top:10px; margin-bottom:20px;">
						<img src="<?php echo URL_IMG; ?>info.png" style="padding-right:10px;" /> Foram encontrados <?php echo count($listaCadastro); ?> usuários para o CPF / CNPJ informado. Clique em reenviar para receber um novo email de validação.
						<table class="extensions t800" align="center" border="0" style="margin-top:10px;">
							<thead>
								<tr>
									<th>Tipo Cadastro</th>
									<th class="w80">Razão Social</th>
                                    <th class="w80">E-mail</th>
									<th>Login</th>
									<th>Opções</th>
								</tr>
								<tr class="header-separator">
									<td colspan="4">&nbsp;</td>
								</tr>
							</thead>
							<tbody>
								{listaCadastro}
								<tr>
										<td class="no-wrap pr_20 tdleft">{tipoCadastro}</td>
										<td class="no-wrap pr_20 tdleft">{razaoSocial}</td>
                                        <td class="no-wrap pr_20 tdleft">{email}</td>
										<td class="no-wrap pr_20 ">{login}</td>
										<td class="no-wrap pr_20 ">
											<form method="post" action="<?php echo URL_EXEC; ?>autocad/renviaEmail/save/1" onsubmit="return confirmForm_enviaEmail('{login}','{email}')" >
												<input type="hidden" value="{login}" name="login" id="login" />
												<input class="buttonPadrao" style="height:17px; width:70px;" id="{login}" type="submit" value=" Reenviar " />
											</form>
										</td>
								</tr>
								{/listaCadastro}
							</tbody>
						</table>
					</div>
				<?php } ?>
			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
</body>
</html>