<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
</head>
<body>

	<div id="wrapper">

		<?php monta_header($tipoUser); ?>
		<?php if ($tipoUser != 0) { monta_menu($tipoUser); } ?>
		
		<div id="middle">

			<div id="container" style="text-align:center;">
				
				<img src="<?php echo URL_IMG; ?>atencao.png" style="padding-top:50px;" />
				<br />
				<?php 
				if (is_array($msg))
				{
					echo '<b>ATENÇÃO!</b><br /><br />';
					
					for ($i = 0; $i < count($msg); $i++)
					{
						echo "- " . $msg[$i] . "<br />";
					}
				}
				else
				{
					echo $msg;
				}
				?>
				
			</div><!-- #container-->
			
		</div><!-- #middle-->

	</div><!-- #wrapper -->

	<?php monta_footer(); ?>

</body>
</html>