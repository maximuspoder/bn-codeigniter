<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>pdv.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>auxcad.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>cep.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>powercombo.js" type="text/javascript"></script>
	<script language="javascript">
		window.onpageshow = function(evt){ $('btnSaveForm').disabled = false; }
	</script>
</head>
<body onload="pdv_formalt_onload();">
	<div id="wrapper">
		<?php 
		add_elementos_CONFIG();
		add_div_CEP();
		monta_header(1);
		monta_menu($this->session->userdata('tipoUsuario'));
		?>
		<div id="middle">
			<div id="container">
				<div class="main_content_left" style="margin-top:10px; margin-bottom:20px;">
					<?php
						$erros = Array();
						
						if (isset($outrosErros))
						{
							if (is_array($outrosErros))
							{
								for ($i = 0; $i < count($outrosErros); $i++)
								{
									array_push($erros, $outrosErros[$i]);
								}
							}
							else
							{
								array_push($erros, $outrosErros);
							}
						}
						
						exibe_validacao_erros($erros);
					?>
					<form name="form_cad" id="form_cad" method="post" action="<?php echo URL_EXEC; ?>pdv/form_alt/save" onsubmit="return validaFormAlt_Pdv()">
						<fieldset style="width:580px; margin-left:20px;">
							<legend><b>Atualização de Cadastro</b></legend>
							<div id="fs_div">
								<table id="tabelaCadastros2" align="center" border="0">
									<tr>
										<td colspan="4" class="areaCadastro2"><img src="<?php echo URL_IMG; ?>geral.png" style="padding-right:10px;" /> Dados gerais</td>
									</tr>
									<tr>
										<td style="padding-top:10px;"></td>
										<td style="padding-top:10px; text-align: left;" colspan="3">
											<input type="radio" <?php echo ($tipopessoa2 != 'PJ' ? 'disabled="disabled"' : ''); ?> name="tipopessoa" id="tipopessoa" value="PJ" <?php echo set_radio('tipopessoa', 'PJ'); ?> /> Pessoa Jurídica
											<input type="radio" <?php echo ($tipopessoa2 != 'PF' ? 'disabled="disabled"' : ''); ?> name="tipopessoa" id="tipopessoa" value="PF" <?php echo set_radio('tipopessoa', 'PF'); ?> /> Pessoa Física 
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<span id="spantipopessoa">CNPJ</span>*: 
											<input class="inputText" type="text" name="cnpjcpf" id="cnpjcpf" style="width:130px;" maxlength="18" readonly="readonly" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{cnpjcpf}<?php echo set_value('cnpjcpf'); ?>" autocomplete="off" onkeypress="return maskInteger(this, event)" onKeyUp="maskCnpj(this)" />
										</td>
									</tr>
									<tr id="linhaie">
										<td>Inscrição Estadual:</td>
										<td colspan="3"><input class="inputText" type="text" name="iestadual" id="iestadual" style="width:170px;" maxlength="20" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{iestadual}<?php echo set_value('iestadual'); ?>" autocomplete="off" /></td>
									</tr>
									<tr>
										<td><span id="spannome">Razão Social</span>*:</td>
										<td colspan="3"><input class="inputText" type="text" name="razaosocial" id="razaosocial" style="width:414px;" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{razaosocial}<?php echo set_value('razaosocial'); ?>" autocomplete="off" /></td>
									</tr>
									<tr id="linhanf">
										<td>Nome Fantasia:</td>
										<td colspan="3"><input class="inputText" type="text" name="nomefantasia" id="nomefantasia" style="width:414px;" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{nomefantasia}<?php echo set_value('nomefantasia'); ?>" autocomplete="off" /></td>
									</tr>
									<tr>
										<td>Natureza Jurídica*:</td>
										<td colspan="3">
											<select class="select" name="naturezajur" id="naturezajur" style="width:480px;">
												<option value="0" <?php echo set_select('naturezajur', 0); ?> >Selecione</option>
												<?php
												$auxPai = 0;
												foreach($natjur as $row)
												{
													if ($row['IDPAI'] != $auxPai)
													{
														if ($auxPai != 0)
														{
															echo '</optgroup>';
														}
														
														echo '<optgroup label="' . $row['NOMEPAI'] . '">';
														
														$auxPai = $row['IDPAI'];
													}
													
													echo '<option value="' . $row['IDNATUREZAJUR'] . '" ' . set_select('naturezajur', $row['IDNATUREZAJUR']) . ' >' . $row['NOMENATUREZAJUR'] . '</option>';
												}
												echo '</optgroup>';
												?>
											</select>
										</td>
									</tr>
									<tr>
										<td>Telefone Geral*:</td>
										<td colspan="3">
											<input class="inputText" type="text" name="telefone1" id="telefone1" style="width:130px;" maxlength="14" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{telefone1}<?php echo set_value('telefone1'); ?>" autocomplete="off" onKeyUp="maskx(this, msk_telefone)" /> 
											<span style="font-size:11px;">(xx) xxxx.xxxx</span>
										</td>
									</tr>
									<tr>
										<td>E-mail Geral*:</td>
										<td colspan="3"><input class="inputText" type="text" name="email" id="email" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{email}<?php echo set_value('email'); ?>" autocomplete="off" style="text-transform:none;width:323px;" /></td>
									</tr>
									<tr>
										<td>Site:</td>
										<td colspan="3"><input class="inputText" type="text" name="site" id="site" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{site}<?php echo set_value('site'); ?>" autocomplete="off" style="text-transform:none;width:323px;" /></td>
									</tr>
									<tr>
										<td>Login:</td>
										<td colspan="3">
											<input class="inputText" type="text" name="login" id="login" maxlength="20" value="{login}<?php echo set_value('login'); ?>" style="text-transform:none;width:130px;" readonly="readonly" /> 
										</td>
									</tr>
									<tr>
										<td colspan="4" class="areaCadastro2" style="padding-top:10px;"><img src="<?php echo URL_IMG; ?>endereco.png" style="padding-right:10px;" /> Endereço</td>
									</tr>
									<tr>
										<td style="padding-top:10px;">CEP*:</td>
										<td style="padding-top:10px;" colspan="3">
											<input type="hidden" name="idlogradouro" id="idlogradouro" value="{idlogradouro}<?php echo set_value('idlogradouro'); ?>" />
											<input class="inputText" type="text" name="cep" id="cep" style="width:80px;" maxlength="8" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{cep}<?php echo set_value('cep'); ?>" autocomplete="off" onkeypress="return maskInteger(this, event)" />
											<img alt="Pesquisar CEP" title="Pesquisar CEP" src="<?php echo URL_IMG; ?>pesq1.gif" style="cursor:pointer;" onclick="pesquisaCep('cep','cep#idlogradouro#uf#cidade#bairro#logradouro#logcomplemento#end_numero');" />
											<img alt="Limpar endereço" title="Limpar endereço" src="<?php echo URL_IMG; ?>x2.gif" style="cursor:pointer;" onclick="apagaCep('cep','cep#idlogradouro#uf#cidade#bairro#logradouro#logcomplemento');" />
										</td>
									</tr>
									<tr>
										<td>UF:</td>
										<td colspan="3">
											<input class="inputText inputDisab" readonly="readonly" type="text" name="uf" id="uf" style="width:40px;" value="{uf}<?php echo set_value('uf'); ?>" autocomplete="off" />
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											Cidade:
											<input class="inputText inputDisab" readonly="readonly" type="text" name="cidade" id="cidade" style="width:300px;" value="{cidade}<?php echo set_value('cidade'); ?>" autocomplete="off" />
										</td>
									</tr>
									<tr>
										<td>Bairro:</td>
										<td colspan="3">
											<input class="inputText inputDisab" readonly="readonly" type="text" name="bairro" id="bairro" style="width:410px;" value="{bairro}<?php echo set_value('bairro'); ?>" autocomplete="off" />
										</td>
									</tr>
									<tr>
										<td>Logradouro:</td>
										<td colspan="3">
											<input class="inputText inputDisab" readonly="readonly" type="text" name="logradouro" id="logradouro" style="width:410px;" value="{logradouro}<?php echo set_value('logradouro'); ?>" autocomplete="off" />
											<input class="inputText inputDisab" readonly="readonly" type="hidden" name="logcomplemento" id="logcomplemento" size="20" value="{logcomplemento}<?php echo set_value('logcomplemento'); ?>" autocomplete="off" />
										</td>
									</tr>
									<tr>
										<td>Número*:</td>
										<td colspan="3">
											<input class="inputText" type="text" name="end_numero" id="end_numero" style="width:93px;" maxlength="8" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{end_numero}<?php echo set_value('end_numero'); ?>" autocomplete="off" />
											&nbsp;&nbsp;&nbsp;&nbsp;
											Complemento:
											<input class="inputText" type="text" name="end_complemento" id="end_complemento" style="width:210px;" maxlength="30" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{end_complemento}<?php echo set_value('end_complemento'); ?>" autocomplete="off" />
										</td>
									</tr>
									<tr>
										<td colspan="4">
											<table align="center">
												<tr>
													<td style="text-align:right; padding-right:5px;">Área total de vendas em m²: </td>
													<td><input class="inputText" type="text" name="area_total" id="area_total" style="width:93px;" maxlength="10" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{area_total}<?php echo set_value('area_total'); ?>" autocomplete="off" /></td>
												</tr>
												<tr>
													<td style="text-align:right; padding-right:5px;">Área de vendas para livros em m²: </td>
													<td><input class="inputText" type="text" name="area_livros" id="area_livros" style="width:93px;" maxlength="10" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{area_livros}<?php echo set_value('area_livros'); ?>" autocomplete="off" /></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td colspan="4" class="areaCadastro2" style="padding-top:15px;"><img src="<?php echo URL_IMG; ?>socio.png" style="padding-right:10px;" /> Informações sobre sócios <span style="font-size:11px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(informe os dados do sócio e clique em "Adicionar")</span></td>
									</tr>
									<tr>
										<td style="padding-top:10px;" colspan="4">
											<table id="tabSocios" cellspacing="0" cellpadding="0" class="extensions t100" align="center">
												<thead>
													<tr style="height:20px;">
														<th>Nome</th>
														<th>CPF</th>
														<th>% Participação</th>
														<th title="Sócio Administrador" alt="Sócio Administrador">Sócio Admin.</th>
														<th style="width:80px;">Opções</th>
													</tr>
													<tr class="header-separator">
														<td colspan="4">&nbsp;</td>
													</tr>
												</thead>
												<tbody>
													<tr class="l_i2">
														<td class="tdcenter"><input class="inputText" maxlength="100" type="text" name="socionome" id="socionome" style="width:230px;" onfocus="focusLiga(this)" onblur="focusDesliga(this)" /></td>
														<td class="tdcenter"><input class="inputText" type="text" name="sociocpf" id="sociocpf" style="width:110px;" maxlength="14" onfocus="focusLiga(this)" onblur="focusDesliga(this)" autocomplete="off" onkeypress="return maskInteger(this, event)" onKeyUp="maskCpf(this)" /></td>
														<td class="tdcenter"><input class="inputText" maxlength="6" type="text" name="sociopartic" id="sociopartic" style="width:80px;" onfocus="focusLiga(this)" onblur="focusDesliga(this)" /></td>
														<td class="tdcenter">
															<select class="select" name="socioadmin" id="socioadmin" style="width:80px;">
																<option value="N">NÃO</option>
																<option value="S">SIM</option>
															</select>
														</td>
														<td class="tdcenter"><input class="buttonPadrao" type="button" value="Adicionar" id="btnAddSocio" onclick="addSocio();" style="height:17px; width:70px;" /></td>
													</tr>
												</tbody>
											</table>
											<input type="hidden" name="socios" id="socios" value="{socios}<?php echo set_value('socios'); ?>" />
										</td>
									</tr>
									<tr>
										<td colspan="4" class="areaCadastro2" style="padding-top:15px;"><img src="<?php echo URL_IMG; ?>outrasinfo.png" style="padding-right:10px;" /> Outras informações</td>
									</tr>
									<tr>
										<td style="padding-top:10px;" colspan="4">
											<table align="center">
												<tr>
													<td style="text-align:right; padding-right:5px;">Associada a entidade sindical? Qual?</td>
													<td><input class="inputText" type="text" name="assoc_ent_sind" id="assoc_ent_sind" style="width:380px;" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{assoc_ent_sind}<?php echo set_value('assoc_ent_sind'); ?>" autocomplete="off" /></td>
												</tr>
												<tr>
													<td style="text-align:right; padding-right:5px;">Filiada a entidade(s) associativa(s)? Quais?</td>
													<td><input class="inputText" type="text" name="fil_ent_assoc" id="fil_ent_assoc" style="width:380px;" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{fil_ent_assoc}<?php echo set_value('fil_ent_assoc'); ?>" autocomplete="off" /></td>
												</tr>
												<tr>
													<td colspan="2">
														Banco: 
														<input class="inputText" type="text" name="banco" id="banco" style="width:150px;" maxlength="20" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{banco}<?php echo set_value('banco'); ?>" autocomplete="off" /> 
														&nbsp;&nbsp;&nbsp;
														Agência: 
														<input class="inputText" type="text" name="agencia" id="agencia" maxlength="20" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{agencia}<?php echo set_value('agencia'); ?>" autocomplete="off" style="width:90px;" />
														&nbsp;&nbsp;&nbsp;
														Conta: 
														<input class="inputText" type="text" name="conta" id="conta" maxlength="20" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{conta}<?php echo set_value('conta'); ?>" autocomplete="off" style="width:90px;" />
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td colspan="4" class="areaCadastro2" style="padding-top:15px;"><img src="<?php echo URL_IMG; ?>resp.png" style="padding-right:10px;" /> Responsável junto à Biblioteca Nacional</td>
									</tr>
									<tr>
										<td style="padding-top:10px;">Nome*:</td>
										<td style="padding-top:10px;" colspan="3"><input class="inputText" type="text" name="nomeresp" id="nomeresp" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{nomeresp}<?php echo set_value('nomeresp'); ?>" autocomplete="off" style="width:414px;" /></td>
									</tr>
									<tr>
										<td>Telefone*:</td>
										<td colspan="3">
											<input class="inputText" type="text" name="telefoneresp" id="telefoneresp" style="width:130px;" maxlength="14" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{telefoneresp}<?php echo set_value('telefoneresp'); ?>" autocomplete="off" onKeyUp="maskx(this, msk_telefone)" /> 
											<span style="font-size:11px;">(xx) xxxx.xxxx</span>
											&nbsp;&nbsp;&nbsp;
											Skype: 
											<input class="inputText" type="text" name="skyperesp" id="skyperesp" maxlength="30" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{skyperesp}<?php echo set_value('skyperesp'); ?>" autocomplete="off" style="text-transform:none;width:140px;" />
										</td>
									</tr>
									<tr>
										<td>E-mail*:</td>
										<td colspan="3"><input class="inputText" type="text" name="emailresp" id="emailresp" maxlength="100" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{emailresp}<?php echo set_value('emailresp'); ?>" autocomplete="off" style="text-transform:none;width:323px;" /></td>
									</tr>
									<tr>
										<td colspan="4" style="text-align: center; padding-top:15px;">
											<input type="hidden" name="loadDadosEdit" id="loadDadosEdit" value="{loadDadosEdit}" />
											<input type="hidden" name="tipopessoa_aux" id="tipopessoa_aux" value="{tipopessoa_aux}" />
											<input type="hidden" name="naturezajur_aux" id="naturezajur_aux" value="{naturezajur_aux}" />
											<input class="buttonPadrao" type="button" value="  Voltar  " onclick="window.location.href='<?php echo URL_EXEC; ?>home'" />
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input class="buttonPadrao" id="btnSaveForm" name="btnSaveForm" type="submit" value="  Atualizar  " />
										</td>
									</tr>
									<tr>
										<td colspan="4" style="text-align: center; padding-top:5px; font-size:11px;">
											(*) Campos de preenchimento obrigatório.
										</td>
									</tr>
								</table>
							</div>
						</fieldset>
					</form>
				</div>
			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
</body>
</html>