<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>pedido.js?<?php echo time();?>" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>pdv.js" type="text/javascript"></script>
</head>
<body>
	<div id="wrapper">
		<?php monta_header(1); ?>
		<?php monta_menu($this->session->userdata('tipoUsuario')); ?>	
		<?php add_elementos_CONFIG(); ?>		
		<div class="divtitulo">
			<div class="divtitulo2">
				>> GERENCIAR PEDIDOS
			</div>
		</div>
		
		<div id="middle">
			<div id="container" style="text-align:center;">
				<div class="main_content_center" style="margin-top:10px; margin-bottom:20px;">
                    <center>
						<form name="filtros_lista" id="filtros_lista" method="post" class="form_filtro" action="<?php echo URL_EXEC; ?>pdv/pedidosLivro">
							<fieldset style="width:94%">
								<legend>Filtros</legend>
								<div class="filtro_campo">
									<span>Status:</span>
									<select name="filtro_status" id="filtro_status" class="select" style="width:200px;">
                                        <option value="" <?php echo set_select('filtro_status',  '', TRUE); ?>></option>
                                        <?php foreach ($statusFilt as $key => $val) { ?>
                                            <option value="<?php echo $val['IDPEDIDOSTATUS']; ?>" <?php echo set_select('filtro_status',   $val['IDPEDIDOSTATUS']); ?>><?php echo  $val['DESCSTATUS']; ?></option>
                                        <?php } ?>
									</select>
								</div>
                                <div class="filtro_campo">
									<span class="margin">Edital:</span>
									<select name="filtro_programa" id="filtro_programa" class="select" style="width:150px;">
										<option value=""  <?php echo set_select('filtro_programa',  '', TRUE); ?>></option>
										<?php
                                        foreach($programas as $row)
                                        {
                                            echo '<option value="' . $row['IDPROGRAMA'] . '" ' . set_select('filtro_programa', $row['IDPROGRAMA']) . ' >' . $row['DESCPROGRAMA'] . '</option>';
                                        }
                                        ?>
									</select>
								</div>
                                <div class="filtro_campo">
									<span class="margin">Cod. Pedido:</span>
									<input type="text" name="filtro_codpedido" id="filtro_codpedido" class="inputText" style="width:100px;" value="<?php echo set_value('filtro_codpedido'); ?>" onkeypress="return maskInteger(this, event)" />
								</div>
								<div class="filtro_campo">
									<span class="margin">Biblioteca:</span>
									<input type="text" name="filtro_biblioteca" id="filtro_biblioteca" class="inputText" style="width:150px;" value="<?php echo set_value('filtro_biblioteca'); ?>" />
								</div>
								<div class="filtro_campo"><span class="margin"><input type="submit" value="Enviar" class="buttonPadrao" /></span></div>
							</fieldset>
						</form>
					</center>
					<table cellspacing="0" align="center" cellpadding="0" class="extensions t95">
						<thead>
							<tr class="header">
								<th>Cod. Pedido</th>
                                <th>Status</th>
								<th class="w100">Biblioteca</th>
                                <th>Qtd. Total</th>
                                <th>Valor Total</th>
                                <th>Edital</th>
								<th>Data Atualização</th>
								<th>Opções</th>
							</tr>
							<tr class="header-separator">
								<td colspan="8">&nbsp;</td>
							</tr>
						</thead>
						<tbody>
							{pedidos}
							<tr class="{CORLINHA}">
								<td class="no-wrap tdcenter">{IDPEDIDO}</td>
                                <td class="no-wrap pr_20 tdleft">{DESCSTATUS}</td>
								<td class="w100 pr_20 tdleft">{RAZAOSOCIAL} {NOMEFANTASIA}</td>
								<td class="no-wrap tdcenter">{QTDTOTAL}</td>
                                <td class="no-wrap tdright">{VALORTOTAL}</td>
                                <td class="no-wrap tdright">{PROGRAMA}</td>
								<td class="no-wrap pr_20 tdleft">{DATA_ATU}</td>
								<td class="no-wrap tdleft">
									<a href="<?php echo(URL_EXEC);?>/pedido/conferencia/{IDPEDIDO}"><img src="<?php echo(URL_IMG)?>icon_pedido_conferencia.png" title="Conferência do Pedido" /></a>
									&nbsp;
									<a href="<?php echo URL_EXEC; ?>pdv/pedido/{IDPEDIDO}/view"><img src="<?php echo URL_IMG; ?>search_16.png" title="VISUALIZAR" border="0" alt="VISUALIZAR" style="cursor:pointer;" /></a>
									&nbsp;
									<a href="javascript:void(0);" onclick="downloadExcelPedido({IDPEDIDO});"><img src="<?php echo URL_IMG; ?>icon_excel.gif" title="Exportar para Excel" border="0" style="cursor:pointer;" /></a>
								</td>
							</tr>
							{/pedidos}
							
							<?php
							if ($vazio == TRUE)
							{
								?>
								<tr>
									<td colspan="8" class="no-wrap tdcenter">Nenhum pedido encontrado.</td>
								</tr>
								<?php 
							}
							else
							{
								?>
								<tr class="noEfeito">
									<td colspan="8" class="no-wrap tdcenter">
										<div style="display: inline; float: left;">Total Registros: <?php echo $qtdReg; ?></div>
										<?php montaPaginacaoPesquisa($nPaginas, $nPag, $limit_de, $exibir_pp, URL_EXEC . 'pdv/pedidosLivro/', 'filtros_lista'); ?>
									</td>
								</tr>
								<?php
							}
							?>
						</tbody>
					</table>
				</div>
			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
	<iframe id="download_excel_pedido" src="" style="display:none;"></iframe>
</body>
</html>