<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
    <link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.tablesorter.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
</head>
<body>
    <?php monta_header(1); ?>
	<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
	<?php add_elementos_CONFIG(); ?>
    <div id="page_content">        
		<div id="inside_content">
			<h5 class="comment">Seja bem-vindo(a), <?php echo(@$nome_usuario);?>.</h5>
            <div class="inline top" style="width:550px;">
				<div class="inline" style="margin:20px 10px 10px 0"><h3>Avisos, comunicados e mensagens <!--(0)--></h3></div>
                <!--<span><a href="javascript:void(0);" class="black">Nova mensagem</a></span>-->
                <?php 
					$notmsg = true;
					// mensagem('info', 'Seleção de ponto de venda', 'Você deve selecionar um ponto de venda para o 4º edital. Caso você queira permanecer utilizando o mesmo, clique <a href="javascript:void(0);">aqui</a>, caso contrário entre na <a href="javascript:void(0);">tela de seleção de ponto de venda</a> e informe o ponto de venda parceiro para este edital.', false, 'width:500px;');
                    
					// Caso HABILITADO
					if($habilitado == 'S')
					{
						mensagem('note', '', 'Você está habilitado e logado no Edital <b>' . $NOME_EDITAL . '</b>.', false, 'width:500px;margin-bottom:20px;');
						$notmsg = false;
					}
					
					// Comunicado CNPC
					if(true)
					{
						mensagem('info', 'Comunicado CNPC', 'Prezados Senhores: O Ministério da Cultura deu início ao processo de renovação dos Colegiados... <a href="javascript:void(0);" onclick="ajax_modal(\'Comunicado CNPC - Cadastramento de Eleitores e Candidatos\', \'' . URL_EXEC . 'comunicacao/comunicado_cnpc\', \'' . URL_IMG . 'icon_info.png\', 610, 700);">leia mais</a>', false, 'width:500px;margin-bottom:20px;');
						$notmsg = false;
					}
					
					// Tem arquivo marcado para ser editado
					if(count($pedidos_com_docs_liberar_edicao) > 0){
						mensagem('warning', 'Edição de Documentos', 'Nossos administradores identificaram que é necessário a alteração em alguns documentos da listagem de pedidos relacionada abaixo. Entre na conferência do pedido relacionado para realizar edições em seus documentos.<br /><br />Listagem de pedidos: ' . $pedidos_conferencia, false, 'width:500px;margin-bottom:20px;');
						$notmsg = false;
					}
					
					// XLS DE todos os pedidos e itens
					if(true)
					{
						mensagem('info', 'Excel itens de pedidos', 'Já está disponível o arquivo xls contendo todos os pedidos e seus itens (caso você tenha algum), com o nome da biblioteca vinculada. Você pode fazer download deste arquivo clicando no link <strong>Exportar itens de pedidos</strong>, da sua nova listagem de pedidos (menu Pedidos > Meus Pedidos).', false, 'width:500px;margin-bottom:20px;');
						$notmsg = false;
					}
					
					// Periodo Conferencia
					if($conferencia)
					{
						mensagem('info', '<a href="' . URL_EXEC .'pedido/lista_pedidos_pdv" class="black">Conferência de Pedido</a>', '<strong>ATENÇÃO!</strong><br />A conferência do pedido pela biblioteca só deverá ser feita após a entrega dos livros (e notas fiscais) pelo PDV. Você deve realizar a conferência para que o valor do pedido seja liberado.<br /><br />Abaixo você encontrará um tutorial de ajuda para a conferência de pedido.<br /><a href="' . URL_EXEC .'application/files/help_conferencia_pdv.pdf" target="_blank">Tutorial para conferência</a>', false, 'width:500px;margin-bottom:20px;');
						$notmsg = false;
					}
					
					// Livros Inexigíveis (3º Edital somente)
					if(count($livros_inexigiveis) > 0)
					{					
						mensagem('info', 'Livros Inexigíveis', 'clique no link abaixo e faça o download da listagem de <strong>títulos inexigíveis por editoras</strong>.<br /><br /><strong>Atenção!</strong> Se itens não entregues pelo PDV estiverem indicados como inexigíveis, sua entrega não é considerada obrigatória, logo, a conferência poderá ser finalizada normalmente para liberação de recursos. Itens exigíveis deverão ser entregues pelo PDV à biblioteca até o dia 30 de julho. Verifique orientações: <a href="http://www.bn.br/portal/arquivos/pdf/Orientacoes%20para%20pagamento%20Edital%20FBN%203.pdf" target="_blank">COMUNICADO OFICIAL NO PORTAL DO LIVRO/EDITAL FBN DE 2011</a><br /><br /><img src="' . URL_IMG . 'icon_excel.gif" style="margin:1px 5px 0 0" /><a href="javascript:void(0);" onclick="download_excel(\'' . URL_EXEC . 'livro/download_excel_livros_inexigiveis\');">Download do arquivo XLS</a>', false, 'width:500px;margin-bottom:20px;');
						$notmsg = false;
					}
					
					// Habilitação no edital
					foreach ($boolPeriodoAceite as $key => $value )
					{
						if((datediff(date('Y/m/d'), $value['DATA_DE']) >= 0) && (datediff(date('Y/m/d'), $value['DATA_ATE']) <= 0))
                        {
							mensagem('info', 'Habilitação do Ponto de Venda', 'O período para habilitação de pontos de venda no edital <b>' . $value['NOME_EDITAL'] . '</b> já está em aberto e você tem até dia ' . eua_to_br($value['DATA_ATE']) . ' para habilitar o seu ponto de venda.<br />
														<div style="width:410px;text-align:left;height:100px;overflow-y:auto;border:1px solid #aaa;padding:10px;margin:10px 0 0 0;">' . $value['TEXTO_EDITAL'] . '</div>
														<div style="display:inline-block !important;width:85%;width:410px;margin:10px 0 0 0;"><input type="checkbox" name="habilitar_cad_' . $value['IDPROGRAMA'] . '" id="habilitar_cad_' . $value['IDPROGRAMA'] . '" value="1" />&nbsp;Declaramos ter pleno conhecimento dos termos do Edital ' . $value['NOME_EDITAL'] . ', sujeitando-nos aos seus termos.</div>
														<div style="text-align:right;margin-right:10px;">&nbsp;&nbsp;<a href="javascript:void(0);" onclick="habilita_edital(\'' . $value['IDPROGRAMA'] .'\');">Habilitar</a></div>', 
														false, 'width:500px;margin-bottom:20px;');
							$notmsg = false;
						}
					}
					
					// Caso não tenha msg para exibir
					if($notmsg)	
					{
						mensagem('note', '', 'Você não tem nenhum aviso, mensagem ou comunicado a ser exibido.', false, 'width:500px;margin-bottom:20px;');
					}
				?>
            </div>
			<div class="inline top" style="width:370px;margin:15px 0 0 15px;">
				<?php box_edital(); ?>
				<?php box_minhas_informacoes();	?>
            </div>
        </div>
	</div>
</body>
</html>