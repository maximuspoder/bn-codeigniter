<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
	<!--
    <script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>biblioteca.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
	-->
</head>
<body>
	<?php // monta_header(1); ?>
	<?php // monta_menu($this->session->userdata('tipoUsuario')); ?>
	<?php add_elementos_CONFIG(); ?>
	<div class="font_shadow_gray" style="margin-bottom:5px;"><h6>Dados do Ponto de Venda</h6></div>
	<hr />
	<div id="box_group_view" class="modal">
		<div class="odd">
			<div id="label_view">ID PDV:</div>
			<div id="field_view"><?php echo(get_value($pdv, 'IDPDV'));?></div>
		</div>
		<div>
			<div id="label_view">Nome do Ponto de Venda:</div>
			<div id="field_view"><?php echo(get_value($pdv, 'RAZAOSOCIAL'));?></div>
		</div>
		<div class="odd">
			<div id="label_view">CPF / CNPJ:</div>
			<div id="field_view"><?php echo(get_value($pdv, 'CPF_CNPJ'));?></div>
		</div>
		<div>
			<div id="label_view">Email</div>
			<div id="field_view"><?php echo(get_value($pdv, 'EMAILGERAL'));?></div>
		</div>
		<div class="odd">
			<div id="label_view">Telefone:</div>
			<div id="field_view"><?php echo(get_value($pdv, 'TELEFONEGERAL'));?></div>
		</div>
		<div>
			<div id="label_view">Endereço:</div>
			<div id="field_view"><?php echo(get_value($pdv, 'NOMELOGRADOUROTIPO') . ' ' . get_value($pdv, 'NOMELOGRADOURO') . ', ' . get_value($pdv, 'END_NUMERO') . ', ' . get_value($pdv, 'NOMEBAIRRO') . ' - ' . get_value($pdv, 'NOMECIDADE') . ' - ' . get_value($pdv, 'IDUF') . ' CEP: ' . get_value($pdv, 'CEP'));?></div>
		</div>
	</div>
	
	<br />
	<br />
	<div class="font_shadow_gray" style="margin-bottom:5px;"><h6>Responsável pelo Ponto de Venda</h6></div>
	<hr />
	<div id="box_group_view" class="modal">
		<div class="odd">
			<div id="label_view">Nome Responsável:</div>
			<div id="field_view"><?php echo(get_value($pdv, 'NOMERESP'));?></div>
		</div>
		<div>
			<div id="label_view">Email:</div>
			<div id="field_view"><?php echo(get_value($pdv, 'EMAILRESP'));?></div>
		</div>
		<div class="odd">
			<div id="label_view">Telefone:</div>
			<div id="field_view"><?php echo(get_value($pdv, 'TELEFONERESP'));?></div>
		</div>
	</div>
</body>
</html>