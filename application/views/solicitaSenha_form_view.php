<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
        <link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>autocad.js" type="text/javascript"></script>
	<script language="javascript">
		window.onpageshow = function(evt){ $('btnSaveForm').disabled = false; }
	</script>
</head>
<body>
	<div id="wrapper">
		<?php 
		monta_header(0);
		?>
		<div id="middle">
			<div id="container" style="text-align:center;">
                            <?php
                                        $erros = Array();

                                        if (isset($outrosErros))
                                        {
                                                if (is_array($outrosErros))
                                                {
                                                        for ($i = 0; $i < count($outrosErros); $i++)
                                                        {
                                                                array_push($erros, $outrosErros[$i]);
                                                        }
                                                }
                                                else
                                                {
                                                        array_push($erros, $outrosErros);
                                                }
                                        }

                                        exibe_validacao_erros($erros);
                                ?>
				<div class="main_content_left" style="margin-top:10px; margin-bottom:20px;">
                                    
				<fieldset style="width:350px; margin-left:20px;">
						<legend><b>Solicitar nova senha</b></legend>
						<table id="tabelaCadastros" align="center" border="0" style="width:150px;">						
							<tr>
								<td style="padding-top:15px; text-align: center;">
									<input type="radio" name="tipo_solicita_senha" id="rd_login" onclick="tipo_solicitaSenha('login')" {checkFromLogin} /><br /><label for="rd_login">Login</label> 
								</td>
								<td style="padding-top:15px; text-align: center;">
									<input type="radio" name="tipo_solicita_senha" id="rd_cnpj_cpf" onclick="tipo_solicitaSenha('cpfcnpj')" {checkFromCPFCNPJ} /><br /><label for="rd_cnpj_cpf">CPF/CNPJ</label>
								</td>
                            </tr>
						</table>
                                                
                                                <form name="form_cad_login" id="form_cad_login" style="display:{displayFromLogin};" method="post" action="<?php echo URL_EXEC; ?>autocad/solicitaSenha/save/login" onsubmit="return validaForm_solicitaSenha('login')">
                                                        <div id="fs_div">
                                                                <table id="tabelaCadastros" align="center" border="0">
                                                                        <tr>
                                                                                <td class="areaCadastro2">Informe seu login</td>
                                                                        </tr>
                                                                        <tr>
                                                                                <td style="padding-top:10px; text-align: center;">
                                                                                        <input class="inputText" type="text" name="login" id="login" maxlength="20" value="<?php echo set_value('login'); ?>" onfocus="focusLiga(this)" onblur="focusDesliga(this)" style="text-transform:none; width:120px;" />
                                                                                </td>
                                                                        </tr>
                                                                        <tr>
                                                                                <td class="areaCadastro2">Informe seu e-mail</td>
                                                                        </tr>
                                                                        <tr>
                                                                                <td style="padding-top:10px; text-align: center;">
                                                                                        <input class="inputText" type="text" name="email_login" id="email_login" maxlength="100" value="<?php echo set_value('email_login'); ?>" onfocus="focusLiga(this)" onblur="focusDesliga(this)" style="text-transform:none; width:220px;" />
                                                                                </td>
                                                                        </tr>
                                                                        <tr>
                                                                                <td colspan="4" style="text-align: center; padding-top:15px;">
                                                                                        <input class="buttonPadrao" id="btnSaveForm" name="btnSaveForm" type="submit" value="  Enviar  " />
                                                                                </td>
                                                                        </tr>
                                                                </table>
                                                        </div>
                                                  
                                                </form>
                                    
                                                <form name="form_cad_cpfcnpj" id="form_cad_cpfcnpj" style="display:{displayFromCPFCNPJ};" method="post" action="<?php echo URL_EXEC; ?>autocad/solicitaSenha/save/cpfcnpj" onsubmit="return validaForm_solicitaSenha('cpfcnpj')">
                                                        <div id="fs_div">
                                                                <table id="tabelaCadastros" align="center" border="0">
                                                                        <tr>
                                                                                <td class="areaCadastro2">Informe seu CPF ou CNPJ</td>
                                                                        </tr>
                                                                        <tr>
                                                                                <td style="padding-top:10px; text-align: center;">
                                                                                        <input class="inputText" type="text" name="cpfcnpj" id="cpfcnpj" maxlength="14" value="<?php echo set_value('cpfcnpj'); ?>" autocomplete="off" onkeypress="return maskInteger(this, event)" onfocus="focusLiga(this)" onblur="focusDesliga(this)" style="text-transform:none; width:120px;" />
                                                                                        <p style="font-size: 10px;">(Somente números)</p>
                                                                                </td>
                                                                        </tr>
                                                                        <tr>
                                                                                <td class="areaCadastro2">Informe seu e-mail</td>
                                                                        </tr>
                                                                        <tr>
                                                                                <td style="padding-top:10px; text-align: center;">                                                                                    
                                                                                        <input class="inputText" type="text" name="email_cpfcnpj" id="email_cpfcnpj" maxlength="100" value="<?php echo set_value('email_cpfcnpj'); ?>" onfocus="focusLiga(this)" onblur="focusDesliga(this)" style="text-transform:none; width:220px;" />
                                                                                </td>
                                                                        </tr>
                                                                        <tr>
                                                                                <td colspan="4" style="text-align: center; padding-top:15px;">
                                                                                        <input class="buttonPadrao" id="btnSaveForm" name="btnSaveForm" type="submit" value="  Enviar  " />
                                                                                </td>
                                                                        </tr>
                                                                </table>
                                                        </div>

                                                </form>
                                        </fieldset>
				</div>
					<?php if(isset($listaLogin)) {?>
					<div class="main_content_left" style="margin-left:20px; margin-top:10px; margin-bottom:20px;">
						<img src="<?php echo URL_IMG; ?>info.png" style="padding-right:10px;" /> Foram encontrados <?php echo count($listaLogin); ?> usuários com os dados informados. Clique em redefinir para alterar a senha.
						<table class="extensions t800" align="center" border="0" style="margin-top:10px;">
							<thead>
								<tr>
									<th>Tipo Cadastro</th>
									<th class="w100">Razão Social</th>
									<th>Login</th>
									<th>Opções</th>
								</tr>
								<tr class="header-separator">
									<td colspan="4">&nbsp;</td>
								</tr>
							</thead>
							<tbody>
								{listaLogin}
								<tr>
										<td class="no-wrap pr_20 tdleft">{tipoCadastro}</td>
										<td class="no-wrap pr_20 tdleft">{razaoSocial}</td>
										<td class="no-wrap pr_20 ">{login}</td>
										<td class="no-wrap pr_20 ">
											<form method="post" action="<?php echo URL_EXEC; ?>autocad/solicitaSenha/save/login" onsubmit="$('{login}').disabled = true;" >
												<input type="hidden" value="{login}" name="login" id="login" />
												<input type="hidden" value="{email}" name="email_login" id="email_login"/>
												<input class="buttonPadrao" style="height:17px; width:70px;" id="{login}" type="submit" value=" Redefinir " />
											</form>
										</td>
								</tr>
								{/listaLogin}
							</tbody>
						</table>
					</div>
				<?php } ?>
			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
</body>
</html>