<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
</head>
<body>
	<?php monta_header(0); ?>
	<div id="page_content">
		<div id="inside_content">
			<!--<h5 class="comment">Seja bem-vindo(a), <?php echo($nome_usuario);?>.</h5>-->
			<div style="margin:20px 0 30px 0;">Por favor, selecione em qual edital você gostaria de entrar.&nbsp;&nbsp;&nbsp;<a href="<?php echo URL_EXEC; ?>home/edital/0">Não gostaria de entrar em nenhum edital</a></div>
			{editais}
			<div id="box_selecao_edital" >
				<img src="<?php echo URL_IMG; ?>{ICONE_EDITAL}" style="float:left;margin:20px 0 0 20px;" />
				<div id="conteudo">
					<h4>{NOME_EDITAL}</h4>
					<div style="margin:10px 0 0 0;">{TEXTO_EDITAL} <span class="inline">&nbsp;&nbsp;<!--<a href="javascript:void(0);">Saiba mais</a>--></span></div>
					<button style="float:right;margin-top:10px;" onclick="window.location.href='<?php echo URL_EXEC; ?>home/edital/{IDPROGRAMA}'">Acessar Edital</button>
				</div>
			</div>
			{/editais}
		</div>
	</div>
</body>
</html>