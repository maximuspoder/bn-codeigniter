<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>autocad.js" type="text/javascript"></script>
	<script language="javascript">
		window.onpageshow = function(evt){ $('btnSaveForm').disabled = false; }
	</script>
</head>
<body>
	<div id="wrapper">
		
		<?php monta_header(0); ?>
		
		<div id="middle">
			<div id="container" style="text-align:center;">
				<div class="main_content_center" style="margin-top:10px; margin-bottom:20px;">
					<?php
						$erros = Array();
						
						if (isset($outrosErros))
						{
							if (is_array($outrosErros))
							{
								for ($i = 0; $i < count($outrosErros); $i++)
								{
									array_push($erros, $outrosErros[$i]);
								}
							}
							else
							{
								array_push($erros, $outrosErros);
							}
						}
						
						exibe_validacao_erros($erros);
					?>
					<form name="form_cad" id="form_cad" method="post" action="<?php echo URL_EXEC; ?>biblioteca/migrar/{nPag}/0/verifica">
						<table id="tabelaCadastros" align="center" border="0" style="width:400px;">
							<tr>
								<td colspan="4" class="areaCadastro2" style="padding-top:30px;"><b>Informe sua chave de acesso</b></td>
							</tr>
							<tr>
								<td style="padding-top:20px; text-align:center;" colspan="4">
									Chave: <input class="inputText" type="text" name="chave" id="chave" style="width:150px;" maxlength="13" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{chave}<?php echo set_value('chave'); ?>" autocomplete="off" />
								</td>
							</tr>
							<tr>
								<td colspan="4" style="text-align: center; padding-top:20px;">
									<input class="buttonPadrao" id="btnSaveForm" name="btnSaveForm" type="submit" value="  Continuar  " />
								</td>
							</tr>
						</table>
						<input type="hidden" name="nPag" id="nPag" value="{nPag}" />
					</form>
				</div>
			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
</body>
</html>