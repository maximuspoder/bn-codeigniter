<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			// Validação e máscaras
			$("#form_default").validationEngine({ inlineValidation:false , promptPosition : "centerRight", scroll : false });
			$("input:text").setMask();		
		});		
	</script>
</head>
<body>
	<?php add_elementos_CONFIG(); ?>
			<div>
				<div class="inline"><h1><a href="<?php echo URL_EXEC;?>endereco/search" class="black font_shadow_gray">Endereço</a></h1></div>
				<div class="inline"><h3 class="font_shadow_gray">&nbsp;> Cadastro de Logradouro</h3></div>
			</div>
			<br />
			<?php mensagem('note', '', 'Utilize o formulário abaixo para cadastrar um novo logradouro. Campos com (*) são obrigatórios.'); ?>
			<br />
			<br />
			<h4 class="font_shadow_gray">Novo Logradouro</h4>
			<hr />
			<br />
			<form name="form_default" id="form_default" method="post" action="<?php echo URL_EXEC; ?>endereco/form_new_endereco_process">
			<input type="hidden" name="idlogradouro" value="<?php echo $idlogradouro; ?>">
			<input type="hidden" name="cep" value="<?php echo $cep; ?>">
			<input type="hidden" name="cidade" value="<?php echo $cidade; ?>">
			<input type="hidden" name="estado" value="<?php echo $estado; ?>">
			<input type="hidden" name="bairro" value="<?php echo $bairro; ?>">
				<div class="form_label" style="margin-left:20px;">Estado:</div>
				<div class="form_field" style="padding-top:6px;">
				<input type="text" name="l__estado" id="l__estado" value="<?php echo $estado?>" disabled="disabled" style="width:150px;"/>
				</div>
				<br />
				<div class="form_label" style="margin-left:20px;">Cidade:</div>
				<div class="form_field" style="padding-top:6px;">
				<input type="text" name="l__cidade" id="l__l__cidadecep" value="<?php echo $cidade; ?>" disabled="disabled" style="width:250px;"/>
				</div>
				<br />				
				<div class="form_label" style="margin-left:20px;">Bairro:</div>
				<div class="form_field" style="padding-top:6px;">
				<input type="text" name="l__bairro" id="l__bairro" value="<?php echo $bairro; ?>" disabled="disabled" style="width:250px;"/>
				</div>
				<br />
				<div class="form_label" style="margin-left:20px;">*Tipo Logradouro:</div>
				<div class="form_field" style="padding-top:6px;">				
				<select name="idlogradourotipo" id="ativo" style="width:262px;" class="validate[required]"><?php echo $options_logradourotipo; ?></select>
				</div>
				<br />				
				<div class="form_label" style="margin-left:20px;">*Logradouro:</div>
				<div class="form_field" style="padding-top:6px;">
				<input type="text" name="nomelogradouro" id="l__nomelogradouro" style="width:250px;" class="validate[required]"/>
				</div>
				<br />				
				<div class="form_field" style="padding-top:6px; margin-left:20px; margin-bottom:10px;">				
				<input type="submit" value="Cadastrar" >
				</div>	

			</form>				
</body>
</html>