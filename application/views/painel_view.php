<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<!--
    <script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>biblioteca.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
	-->
	<script type="text/javascript" language="javascript">
		$(document).ready(function() {
			// Verifica compatibilidade de browsers
			check_browser_compatibilty();
			
			// Seta focus no campo de usuario (login)
			$("#userLogin").focus();
		});
	</script>
</head>
<body>
	<?php monta_header(0); ?>
	<?php add_elementos_CONFIG(); ?>
	<div id="page_content">
		<div id="inside_content">
			<?php // mensagem('info', 'Informações Adicionais', 'Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen ')?>
			<div class="inline top" style="width:570px;">
				<h1 class="font_shadow_gray">Portal do Livro FBN</h1>
				<hr />
				<div class="inline top" style="width:550px;">
					<div style="margin:15px 0;"><h4>Bem vindo ao <strong>Portal do Livro</strong> da Fundação Biblioteca Nacional!</h4></div>
					<h6>O que é o Portal do Livro FBN?</h6>
					<div>Aqui você terá acesso aos dados da sua Biblioteca, Livros de baixo preço, Ponto de Venda, e Distribuidora cadastrados para acesso aos programas governamentais de apoio às bibliotecas. A aquisição dos livros será efetuada por meio de ações de ampliação e atualização dos acervos promovidas pela Fundação Biblioteca Nacional, no âmbito do Sistema Nacional de Bibliotecas Públicas - SNBP.</div><br />
					<div>É necessário que sua entidade faça o cadastro junto ao Portal do Livro FBN. Apenas as bibliotecas que estiverem com os dados atualizados no <strong>Cadastro Nacional de Bibliotecas</strong> estarão em condições de participar dos programas de modernização e atualização de acervos, coordenada pelo Sistema Nacional de Bibliotecas Públicas - SNBP.</div><br />
					<div>Acesse <a href="http://www.bn.br/portal/arquivos/pdf/Edital-Cadastro-Nacional-de-Bibliotecas.pdf" target="_blank">aqui</a> o edital de convocação para inscrição no Cadastro.</div><br />
				</div>
				<div class="inline top" style="width:580px;">
					<h6>Ainda não possui cadastro?</h6>
					<div style="margin-bottom:10px;">Cadastre-se e fique atento para participar dos próximos editais!</div>
					<div id="box_cadastro_login">
						<div class="inline top white" id="left_area" style="height:113px;">
							<img src="<?php echo(URL_IMG);?>icon_biblioteca_big.png" />
							<div style="margin-left:45px;"><h6>Cadastro Nacional de Bibliotecas</h6></div>
						</div>
						<div class="inline top font_11" id="right_area">
							<div>O objetivo deste cadastro é mapear de maneira abrangente todas as bibliotecas existentes no país, levantando dados sobre a relação institucional, público, acervo, serviços, infraestrutura e gestão. Devem cadastrar-se bibliotecas: públicas estaduais e municipais, comunitárias urbanas ou rurais, pontos de leitura, escolares e universitárias.</div>
							<div class="font_12" style="margin-top:5px;float:right"><a href="javascript:void(0);" style="z-index:9999;" onclick="open_modal('Saiba Mais - Cadastro Nacional de Bibliotecas', '<strong>Cadastro Nacional de Bibliotecas:</strong> Para inserir seus dados as bibliotecas públicas, escolares, universitárias e especializadas devem ser cadastradas pelos órgãos públicos ou privados aos quais estão vinculadas, <strong>a partir do CNPJ da instituição</strong>. <br /><br />Já as bibliotecas comunitárias e os pontos de leitura poderão ser cadastrados a partir do CNPJ da instituição mantenedora, ou pelo CPF do responsável pela biblioteca / ponto de leitura.', '<?php echo URL_IMG; ?>icon_help.png', 300, 500);">Saiba mais</a>&nbsp;&nbsp;&nbsp;<a href="<?php echo URL_EXEC;?>autocad/preautocad/cnb">Cadastrar</a></div>
						</div>
					</div>
					<div id="box_cadastro_login">
						<div class="inline top white" id="left_area" style="height:113px;">
							<img src="<?php echo(URL_IMG);?>icon_livro_baixo_preco_big.png" />
							<div style="margin-left:45px;"><h6>Cadastro Nacional de Livros a Baixo Preço</h6></div>
						</div>
						<div class="inline top font_11" id="right_area">
							<div>Acesse este portal caso você seja um(a) <strong>editor(a) (pessoa física ou jurídica)</strong> ou <strong>autor idependente</strong> interessado em inscrever seus livros nos programas governamentais de fomento à produção e comercialização de livros a baixo preço da Fundação da Biblioteca Nacional.</div>
							<div class="font_12" style="margin-top:5px;float:right"><a href="<?php echo URL_EXEC;?>autocad/preautocad">Cadastrar</a></div>
						</div>
					</div>
					<div id="box_cadastro_login">
						<div class="inline top white" id="left_area">
							<img src="<?php echo(URL_IMG);?>icon_pdv_big.png" />
							<div style="margin-left:45px;"><h6>Cadastro Nacional de Pontos de Venda</h6></div>
						</div>
						<div class="inline top font_11" id="right_area" style="height:78px;">
							<div>Acesse e cadastre-se neste portal caso você seja uma empresa interessada em comercializar os livros de baixo preço.</div>
							<div class="font_12" style="margin-top:5px;float:right"><a href="<?php echo URL_EXEC;?>autocad/preautocad">Cadastrar</a></div>
						</div>
					</div>
					<div id="box_cadastro_login">
						<div class="inline top white" id="left_area" style="height:70px;">
							<img src="<?php echo(URL_IMG);?>icon_distribuidor_big.png" />
							<div style="margin-left:45px;"><h6>Cadastro Nacional de Distribuidores</h6></div>
						</div>
						<div class="inline top font_11" id="right_area">
							<div>Acesse e cadastre-se neste portal caso você seja uma empresa interessada em distribuir os livros de baixo preço.</div>
							<div class="font_12" style="margin-top:5px;float:right"><a href="<?php echo URL_EXEC;?>autocad/preautocad">Cadastrar</a></div>
						</div>
					</div>
				</div>
				<!--
				Aqui você poderá cadastrar sua biblioteca de acesso público (estadual, municipal, comunitária urbana ou rural e os pontos de leitura) para fazer parte do <a class="red" href="<?php echo URL_EXEC; ?>login/exec/2">Cadastro Nacional de Bibliotecas</a>, e assim participar dos programas governamentais de apoio às bibliotecas.
				<br/> <br/> 
				Este portal também é voltado para as editoras e autores independentes que queiram inscrever seus livros ao valor máximo de R$ 10,00 no <a class="red" href="<?php echo URL_EXEC; ?>login/exec/1">Cadastro Nacional de Livros de Baixo Preço</a> de forma a participar dos programas governamentais de aquisição de acervo da Fundação Biblioteca Nacional.
				<br/> <br/> 
				Editoras, livrarias e todas as demais instituições e empresas interessadas em participar do programa, seja comercializando ou mesmo distribuindo os livros de baixo preço, devem se inscrever no <a class="red" href="<?php echo URL_EXEC; ?>login/exec/3">Cadastro Nacional de Pontos de Venda </a> ou no <a class="red" href="<?php echo URL_EXEC; ?>login/exec/4">Cadastro Nacional de Distribuidoras.</a>
				<br/> <br/> 
				Para saber mais sobre o Programa de Ampliação e Atualização dos Acervos das Bibliotecas de Acesso Público clique <a class="red" href="http://www.bn.br/portal/arquivos/pdf/3%20EDITAL%20DE%20CHAMADA%20PUBLICA%20PLP.pdf">aqui.</a>
				-->
			</div>
			<div class="inline top" style="width:350px;margin:0px 0 0 10px;">
				<div style="width:300px;float:right">
					<?php start_box('Área de Login', 'icon_login.png'); ?>
						<span class="inline" style="width:70px;height:35px;">Sou um(a)</span>
						<span class="inline">
							<select name="tipo_entidade" style="width:200px;">
								<option value="2">Biblioteca</option>
								<option value="3">Distribuidor</option>
								<option value="4">Ponto de Venda</option>
								<option value="5">Editor(a)</option>
							</select>
						</span>
						<div class="inline" style="padding-top:2px;width:70px;height:35px;">Usuário</div>
						<div class="inline"><input type="text" name="userLogin" id="userLogin" maxlength="20" style="width:188px;" /></div>
						<div class="inline top" style="padding-top:7px;width:70px;">Senha</div>
						<div class="inline"><input type="password" name="userSenha" id="userSenha" maxlength="32" style="width:188px;" /><br /></div>
						<div class="inline" style="padding-top:7px;width:70px;"></div>
						<div class="inline" style="margin:10px 0 0 0;text-align:right;width:200px;"><input type="submit" value="Entrar" /></div>
						<div style="height:10px;width:275px;border-bottom:1px dotted #aaa;margin:0 0 15px 0;"></div>
						<div class="inline">
							<?php mensagem('info', '', '<h6 style="margin:-1px 0 0 0;">Não está conseguindo acesso?</h6>
														&bull;&nbsp;<a href="'. URL_EXEC . 'autocad/solicitasenha">Esqueceu sua senha?</a><br />
														&bull;&nbsp;<a href="'. URL_EXEC . 'autocad/renviaemail">Reenviar o e-mail de validação</a>', false, 'width:272px;');?>
						</div>
					<?php end_box('Login'); ?>
					<div style="margin-top:20px;">
						<h5>Editais em andamento</h5>
						<hr />
						<?php foreach($programas as $programa){?>
							<div id="box_edital_login" class="with_shadow">
								<img src="<?php echo(URL_IMG . get_value($programa, 'ICONE_EDITAL'))?>" />
								<div class="bold"><?php echo(get_value($programa, 'NOME_EDITAL'));?></div>
								<div style="margin-top:5px;"><a href="javascript:void(0);" onclick="open_modal('Texto do Edital', '<strong><?php echo(get_value($programa, 'NOME_EDITAL'));?></strong><br /><?php echo(get_value($programa, 'TEXTO_EDITAL'));?>', '<?php echo(URL_IMG . get_value($programa, 'ICONE_EDITAL'));?>', 300, 400);">Veja o texto do edital</a></div>
								<div><a href="javascript:void(0);" onclick="iframe_modal('Lista de Contemplados', '<?php echo(URL_EXEC);?>programa/modal_lista_contemplados/<?php echo(get_value($programa, 'IDPROGRAMA'));?>', '<?php echo(URL_IMG . get_value($programa, 'ICONE_EDITAL'));?>', 800, 800);">Veja a lista de contemplados</a></div>
							</div>
						<?php } ?>
						<!--
						<div class="white with_shadow" style="border:1px solid #104E8B;background-color:#1874CD;height:70px;padding:10px;margin-bottom:10px;">
							<h6>Edital de aquisição de livros</h6>
						</div>
						-->
					</div>
					<hr/>
					<div style="margin-top:20px;">
						<div style="background-color:#ae332d;border:1px solid #7c110c" id="box_question_login" class="with_shadow">
							<img src="<?php echo(URL_IMG);?>icon_question.png" style="margin-top:2px" />
							<h5 class="white" style="margin:0;">Dúvidas? Fale conosco!</h5>
							<div style="margin:5px 0 0 22px;" class="white">
							Entre em contato atrávés dos números:<br />
							&bull;&nbsp;(51) 3254-3241<br />
							&bull;&nbsp;(51) 3254-3242<br />
							&bull;&nbsp;(51) 3254-3243<br />
							&bull;&nbsp;(51) 3254-3244<br />
							&bull;&nbsp;(51) 3254-3245
							<br />
							Ou envie um email para:<br />
							<a style="color:white" href="mailto:editais2011@bn.br?subject=Dúvida Portal do Livro FBN">editais2011@bn.br</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>

<!--
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
		<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
</head>
<body >

	<div id="wrapper">
		<?php add_elementos_CONFIG(); ?>
		<?php add_div_ligthbox('600', '350'); ?>
		<?php monta_header(0); ?>
		
		<div id="middle">

			<div id="container" style="text-align:center;">
			
				<div class="main_content_center" style="margin-top:40px; margin-bottom:40px; position: relative;  width: 100%;">
					
					<table id="tabelaCadastros" align="center" border="0">
						<tr>
							<td style="font-size: 30px; font-weight: bold; padding-bottom:20px;">:: Portal do Livro FBN ::</td>
						</tr>
					</table>
					
					<table border='0' style="margin-top:30px; width: 900px; height: 100%" align="center">
				
					<tr>
						<td style="font-size: 14px;">						
							Bem vindo ao <b>Portal do Livro</b> da Fundação Biblioteca Nacional. 
							<br/> <br/> 
							
                            <br/> <br/> 
						</td>										
					</tr>
                    <tr>
                        <td>
							
							<div style=" color: #363636; font-weight:500; background-color: #c5dbec; border: 1px solid #96bcd9; text-align: center; padding: 20px 20px 20px 20px; font-size: 14px;">
								<i>
                                    Veja <a class="red" href="http://sistemas.conectait.com.br:8097/bn/application/doc_pdv/EDITAL4_2011.htm">AQUI</a>
                                    a lista das bibliotecas habilitadas de acordo com o <b>Edital 4/2011</b> da FBN do Programa de Ampliação e Atualização dos Acervos das Bibliotecas de Acesso Público.
                                </i>
							</div>
							
							<div style="margin-top: 20px; color: #363636; font-weight:500; background-color: #ffe45c; border: 1px solid #fed22f; text-align: center; padding: 20px 20px 20px 20px; font-size: 14px;">
                                <i>
                                    Veja <a class="red" href="http://sistemas.conectait.com.br:8097/bn/application/doc_pdv/EDITAL3_2011.htm">AQUI</a>
                                    a lista das bibliotecas habilitadas de acordo com o <b>Edital 3/2011</b> da FBN do Programa de Ampliação e Atualização dos Acervos das Bibliotecas de Acesso Público.
                                </i>
							</div>
							
							<div style="margin-top: 20px; color: #363636; font-weight:500; background-color: #c5dbec; border: 1px solid #96bcd9; text-align: center; padding: 20px 20px 20px 20px; font-size: 14px;">
								<i>
                                    Veja <a class="red" href="<?php echo URL_EXEC; ?>publico/listaLivros">AQUI</a>
                                    a lista dos livros participantes do <b>Edital 3/2011</b>. Colabore com a biblioteca de sua preferência indicando livros para ela adquirir.
                                </i>
							</div>
							
							
                        </td>
                    </tr>
					<tr>
						<td style="text-align: center; padding-top:30px;">
							Para mais informações, <a target="_blank" class="red" href="http://sistemas.conectait.com.br:8097/bn/application/doc_pdv/Passo_a_Passo-Selecao_de_Livros.pdf">clique aqui</a> e consulte o passo-a-passo para seleção de livros. Caso ainda restem dúvidas, escreva para editais2011@bn.br.
							<br>
							<br>
							<a class="red" style="font-size: 18px;" href="<?php echo URL_EXEC; ?>login/exec/2">
							Cadastro Nacional de Bibliotecas
							</a><br><br>
							<a class="red" style="font-size: 18px;" href="<?php echo URL_EXEC; ?>login/exec/1">
							Cadastro Nacional de Livros de Baixo Preço
							</a><br><br>
							<a class="red" style="font-size: 18px;" href="<?php echo URL_EXEC; ?>login/exec/3">
							Cadastro Nacional de Pontos de Venda
							</a><br><br>
							<a class="red" style="font-size: 18px;" href="<?php echo URL_EXEC; ?>login/exec/4">
							Cadastro Nacional de Distribuidoras
							</a>
						</td>						
					</tr>
					<tr>
						<td style="text-align: center; padding-top:40px;">
							<a class="semefeito corPadrao" style="font-size: 14px;" href="<?php echo URL_EXEC; ?>autocad/preautocad"><b>Se você ainda não tem acesso, cadastre-se aqui.</b></a>
						</td>
					</tr>
					<tr>
						<td  style="text-align: center; padding-top:30px;">
							<!--<i>Fale Conosco através do e-mail: editais2011@bn.br</i>->
						</td>
					</tr>					
					</table>
					
				</div>
				
			</div><!-- #container->

		</div><!-- #middle->

	</div><!-- #wrapper -->

	<?php  //monta_footer(); ?>

</body>
</html>