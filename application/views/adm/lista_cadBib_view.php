<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>biblioteca.js" type="text/javascript"></script>
</head>
<body>
	<div id="wrapper">
		<?php monta_header(1); ?>
		<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
		<?php add_elementos_CONFIG(); ?>
		
		<div class="divtitulo">
			<div class="divtitulo2">
				>>  UNIFICADOR DE CADASTROS
			</div>
		</div>
		
		<div id="middle">
			<div id="container" style="text-align:center;">
				<div class="main_content_center" style="margin-top:10px;">
					<center>
						<form name="filtros_lista" id="filtros_lista" method="post" class="form_filtro" action="<?php echo URL_EXEC; ?>adm/unificadorcadastros">
							<fieldset style="width:94%">
								<legend>Filtros</legend>
                                <div class="filtro_campo">
									<span class="margin">Estado:</span>
									<select name="filtro_estado" id="filtro_estado" class="select" style="width:50px;" onchange="ajaxGetCidadesBibUnificacao(this.value);"><?php echo($options_estados); ?></option>
									</select>
								</div>
                                <div class="filtro_campo">
									<span class="margin">Cidade:</span>
									<!-- <select name="filtro_cidade" id="filtro_cidade" class="select" style="width:200px;"><?php /* echo($options_cidades); */ ?></select> -->
									<select name="filtro_cidade" id="filtro_cidade" class="select" style="width:200px;" onchange="ajaxGetidspaiunificacao(this.value);"><?php echo($options_cidades); ?></select>
								</div>
                                 <div class="filtro_campo">
									<span class="margin">Biblioteca Pai:</span>
									<select name="filtro_bibpai" id="filtro_bibpai" class="select" style="width:250px;" ><?php echo($options_idspais); ?></select>
								</div>
                                <div class="filtro_campo"><span class="margin"><input id="filtro_submit" type="submit" value="Enviar" class="buttonPadrao" /></span></div>
								<div style="float:right;">
									<div class="filtro_campo"><span class="margin"><input type="button" value="Biblioteca Pai Anterior" class="buttonPadrao" onclick="window.location.href = '<?php echo URL_EXEC . "adm/unificadorcadastros/$anterior"; ?>'" ></span></div>
									<div class="filtro_campo"><span class="margin"><input type="button" value="Próxima Biblioteca Pai" class="buttonPadrao" onclick="window.location.href = '<?php echo URL_EXEC . "adm/unificadorcadastros/$proxima"; ?>'" ></span></div>
								</div>
							</fieldset>
						</form>
					</center>
					
					<form action="<?php echo URL_EXEC; ?>adm/unificadorcadastros_processa" method="post">
					<table cellspacing="0" align="center" cellpadding="0" class="extensions t95">
						<thead>
							<tr class="header">
								<th width="2%">ID</th>
								<th width="26%">Razão Social / Nome</th>
								<th width="10%">CNPJ/CPF</th>
								<th width="19%">Responsável</th>
								<th width="21%">Logradouro</th>
								<th width="2%">Validado</th>
								<th width="2%">Habilitado</th>
								<th width="2%">Resp. Fin.</th>
								<th width="6%">Comitê</th>
								<!-- th>Bloqueio</th>
								<th>Restrição</th -->
								<th width="5%">Crédito 3º Ed</th>
								<th width="5%">Crédito 4º Ed</th>
							</tr>
                            
							<tr class="header-separator">
								<td colspan="8">&nbsp;</td>
							</tr>
						</thead>
						<tbody>
							<input type="hidden" name="IDPAI" 		 id="IDPAI"			value="{idpai}"		   />
							<input type="hidden" name="CONTPEDIDOS"  id="CONTPEDIDOS"	value="{contpedidos}"  />
							<input type="hidden" name="CONTENVIADOS" id="CONTENVIADOS"	value="{contenviados}" />
							<input type="hidden" name="CONTPDV" 	 id="CONTPDV"		value="{contpdv}" />
							<input type="hidden" name="PROXIMA" 	 id="PROXIMA"		value="{proxima}" />
							<?php 
							foreach($bibliotecas as $biblioteca){
							?>
							<tr class="<?php echo($biblioteca['CORLINHA'])?>">
								<td class="no-wrap tdright">
								<?php 
								if ($biblioteca['IDUSUARIO'] == $idpai)
									echo "<b>". $biblioteca['IDUSUARIO'] . "</b>";
								else
									echo $biblioteca['IDUSUARIO'];
								?>
								</td>
								<td class="pr_20 tdleft"><input 		type="radio" id="RAZAOSOCIAL"				name="RAZAOSOCIAL"					value="<?php echo($biblioteca['RAZAOSOCIAL'])?>"	<?php echo(($biblioteca['IDUSUARIO'] == $idpai) ? ' checked="checked"' : '');?> /><?php echo($biblioteca['RAZAOSOCIAL'])?></td>
								<td class="no-wrap pr_20 tdleft"><input type="radio" id="CPF_CNPJ"					name="CPF_CNPJ"						value="<?php echo($biblioteca['CPF_CNPJ'])?>"		<?php echo(($biblioteca['IDUSUARIO'] == $idpai) ? ' checked="checked"' : '');?> /><?php echo($biblioteca['CPF_CNPJ'])?></td>
								<td class="no-wrap pr_20 tdleft"><input type="radio" id="NOMERESP"					name="NOMERESP"						value="<?php echo($biblioteca['NOMERESP'])?>"		<?php echo(($biblioteca['IDUSUARIO'] == $idpai) ? ' checked="checked"' : '');?> /><?php echo($biblioteca['NOMERESP'])?></td>
								<td class="no-wrap pr_20 tdleft"><input type="radio" id="LOGRADOURO"				name="LOGRADOURO"					value="<?php echo($biblioteca['IDUSUARIO'])?>"		<?php echo(($biblioteca['IDUSUARIO'] == $idpai) ? ' checked="checked"' : '');?> /><div title="<?php echo($biblioteca['LOGRADOURO'])?>" style="border-bottom:1px dotted #000;display:inline-block !important;cursor:default;"><?php echo(substr($biblioteca['LOGRADOURO'], 0, 40) . '...');?></div></td>
								<td class="no-wrap tdcenter"><input 	type="radio" id="DESC_STATUS"				name="DESC_STATUS"					value="<?php echo($biblioteca['IDUSUARIO'])?>"		<?php echo(($biblioteca['IDUSUARIO'] == $idpai) ? ' checked="checked"' : '');?> /><?php echo($biblioteca['DESC_STATUS'])?></td>
								<td class="no-wrap tdcenter"><input 	type="radio" id="USER_HABILITADO" 			name="USER_HABILITADO"				value="<?php echo($biblioteca['IDUSUARIO'])?>"		<?php echo(($biblioteca['IDUSUARIO'] == $idpai) ? ' checked="checked"' : '');?> /><?php echo($biblioteca['USER_HABILITADO'])?></td>
								<td class="no-wrap tdcenter"><input 	type="radio" id="RESPFIN" 					name="RESPFIN"						value="<?php echo($biblioteca['IDUSUARIO'])?>"		<?php echo(($biblioteca['IDUSUARIO'] == $idpai) ? ' checked="checked"' : '');?> /><?php echo($biblioteca['RESPONSAVEL_FINANCEIRO'])?></td>
								<td class="no-wrap tdcenter"><input 	type="radio" id="COMITE" 					name="COMITE"						value="<?php echo($biblioteca['IDUSUARIO']);?>"		<?php echo(($biblioteca['IDUSUARIO'] == $idpai) ? ' checked="checked"' : '');?> /><?php echo($biblioteca['COMITE'])?>&nbsp;&nbsp;[<?php echo($biblioteca['COMITE_ATIVO'])?> validado(s)]</td>
								<!-- td class="no-wrap tdcenter"><input 	type="radio" id="BLOQUEIO_LOGGERENCIAMENTO" name="BLOQUEIO_LOGGERENCIAMENTO"	value="<?php echo($biblioteca['BLOQUEIO_LOGGERENCIAMENTO'])?>"		<?php echo(($biblioteca['IDUSUARIO'] == $idpai) ? ' checked="checked"' : '');?> /><?php echo($biblioteca['BLOQUEIO_LOGGERENCIAMENTO'])?></td>
								<td class="no-wrap tdcenter"><input 	type="radio" id="BLOQUEIO_RESTRICAO"		name="BLOQUEIO_RESTRICAO"			value="<?php echo($biblioteca['BLOQUEIO_RESTRICAO'])?>"				<?php echo(($biblioteca['IDUSUARIO'] == $idpai) ? ' checked="checked"' : '');?> /><?php echo($biblioteca['BLOQUEIO_RESTRICAO'])?></td-->
								
								
								<td class="no-wrap tdright">
								<?php
								$valor3ed = $biblioteca['VLRCREDITO_INICIAL_3ED'] > 0 ? $biblioteca['VLRCREDITO_INICIAL_3ED'] : '0.00';
								
								if ($contenviados >= 1) {
									if ($biblioteca['STATUSPED'] >= 2) {
										echo "<b>" . $valor3ed . "</b>";
										echo "<input type='hidden' id='CREDITO_3ED' name='CREDITO_3ED' value='". $biblioteca['IDUSUARIO'] ."' />";
									} else {
										echo $valor3ed;
									}
								?>
								<?php } else { 
								echo $valor3ed;
								?>
								<input type="radio" id="CREDITO_3ED"	name="CREDITO_3ED"		value="<?php echo($biblioteca['IDUSUARIO'])?>" <?php echo(($biblioteca['IDUSUARIO'] == $idpai) ? ' checked="checked"' : '');?> />
								<?php } ?>
								</td>
								<td class="no-wrap tdright">
								<?php
								$valor4ed = $biblioteca['VLRCREDITO_INICIAL_4ED'] > 0 ? $biblioteca['VLRCREDITO_INICIAL_4ED'] : '0.00';
								echo $valor4ed;
								?><input type="radio" id="CREDITO_4ED"	name="CREDITO_4ED" 		value="<?php echo($biblioteca['IDUSUARIO'])?>" <?php echo(($biblioteca['IDUSUARIO'] == $idpai) ? ' checked="checked"' : '');?> /></td>
							</tr>
							<?php } ?>
							<?php
							if ($vazio == TRUE)
							{
								?>
								<tr>
									<td colspan="14" class="no-wrap tdcenter">Nenhuma biblioteca encontrada.</td>
								</tr>
								<?php 
							}
							?>
						</tbody>
					</table>
					<br />
					<input type="submit" value="Unificar Cadastros" class="buttonPadrao" />
					</form>
				</div>
			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
</body>
</html>