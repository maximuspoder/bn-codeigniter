<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta http-equiv="pragma" content="no-cache" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>autocad.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>biblioteca.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>pedido.js" type="text/javascript"></script>
	<script language="javascript">
		if($('btnSaveForm') != null){window.onpageshow = function(evt){ $('btnSaveForm').disabled = false; }}
	</script>
</head>
<body>
	<div id="wrapper">
		<?php 
            add_elementos_CONFIG(); 
            add_div_ligthbox('650', '400');
            monta_header(1);
            monta_menu($this->session->userdata('tipoUsuario'));
		?>
		<div class="divtitulo">
			<div class="divtitulo2">
				>> PAINEL DE CONTROLE - {NOME}
			</div>
		</div>
		<div id="middle">
			<div id="container">
				<table cellspacing="0" align="center" cellpadding="0" class="extensions" width="60%">
					<thead>
						<tr class="header">
							<th colspan="3"><div align="center">Valores de Crédito para o 3º Edital</div></th>
						</tr>
						<tr class="header">
							<th class="subtitle" width="33%"><div align="right">Valor Inicial</div></th>
							<th class="subtitle" width="33%"><div align="right">Valor Gasto</div></th>
							<th class="subtitle" width="33%"><div align="right">Saldo</div></th>
						</tr>
						<tr class="header-separator">
							<td colspan="3">&nbsp;</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<?php if (isset($VLR3ED)) { ?>
							<td class="tdright">{VLRINI3ED}</td>
							<td class="tdright">{VLRGASTO3ED}</td>
							<td class="tdright">{VLR3ED}</td>
							<?php } else { ?>
							<td colspan="3" class="tdcenter">Não foram encontrados valores para o 3º edital</td>
							<?php } ?>
						</tr>
					</tbody>
				</table>
				<br />
				<table cellspacing="0" align="center" cellpadding="0" class="extensions" width="60%">
					<thead>
						<tr class="header">
							<th colspan="3"><div align="center">Valores de Crédito para o 4º Edital</div></th>
						</tr>
						<tr class="header">
							<th class="subtitle" width="33%"><div align="right">Valor Inicial</div></th>
							<th class="subtitle" width="33%"><div align="right">Valor Gasto</div></th>
							<th class="subtitle" width="33%"><div align="right">Saldo</div></th>
						</tr>
						<tr class="header-separator">
							<td colspan="3">&nbsp;</td>
						</tr>
					</thead>
					<tbody>
						<tr>
						<?php if (isset($VLR4ED)) { ?>
						<td class="tdright">{VLRINI4ED}</td>
						<td class="tdright">{VLRGASTO4ED}</td>
						<td class="tdright">{VLR4ED}</td>
						<?php } else { ?>
						<td colspan="3" class="tdcenter">Não foram encontrados valores para o 4º edital</td>
						<?php } ?>
						</tr>
					</tbody>
				</table>
				<br />
				<?php if (!empty($SNIIC)) { ?>
				<table cellspacing="0" align="center" cellpadding="0" class="extensions" width="95%">
					<thead>
						<tr class="header">
							<th colspan="7"><div align="center">Cadastro SNIIC</div></th>
						</tr>
						<tr class="header">
							<th class="subtitle" width="5%" ><div align="right">ID</div></th>
							<th class="subtitle" width="25%"><div align="left">Nome</div></th>
							<th class="subtitle" width="15%"><div align="left">CPF/CNPJ</div></th>
							<th class="subtitle" width="15%"><div align="left">E-mail</div></th>
							<th class="subtitle" width="15%"><div align="right">Cred. 3º Ed</div></th>
							<th class="subtitle" width="15%"><div align="right">Cred. 4º Ed</div></th>
							<th class="subtitle" width="10%"><div align="center">Desfazer Associação</div></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="tdright"><?php echo $SNIIC[0]['ID_BIBLIOTECA']; ?></td>
							<td class="tdleft"><?php  echo $SNIIC[0]['NOME']; ?></td>
							<td class="tdleft"><?php  echo $SNIIC[0]['CPF_CNPJ']; ?></td>
							<td class="tdleft"><?php  echo $SNIIC[0]['EMAIL']; ?></td>
							<td class="tdright"><?php  echo $SNIIC[0]['VLR3ED']; ?></td>
							<td class="tdright"><?php  echo $SNIIC[0]['VLR4ED']; ?></td>
							<td class="tdcenter"><a href="javascript:void(0);" onclick="javascript:ajaxDesfazAssociacaoSniic('{IDRESP}');">Clique aqui</a></td>
						</tr>
					</tbody>
				</table>
				<br />
				<?php } ?>
				<table cellspacing="0" align="center" cellpadding="0" class="extensions t95">
					<thead>
						<tr class="header">
							<th width="10%">Partícipe</th>
							<th width="5%">ID</td>
							<th width="10%">CNPJ/CPF</th>
                            <th width="25%">Nome Responsável</th>
                            <th width="10%">Login</th>
							<th width="15%">E-mail cadastrado</th>
							<th width="5%">Validado</th>
							<th width="10%">Reenvia E-mail de Validação</th>
							<th width="10%">Envia E-mail Esqueceu Senha</th>
						</tr>
						<tr class="header-separator">
							<td colspan="9">&nbsp;</td>
						</tr>

					</thead>
					<tbody>
						<tr>
							<td class="tdleft">Da Biblioteca</td>
							<td class="tdleft">{IDRESP}</td>
							<td class="tdleft">{CPF_CNPJ}</td>
							<td class="tdleft">{NOMERESP}</td>
							<td class="tdleft">{LOGIN}</td>
							<td class="no-wrap tdleft">
								<input class="inputText" type="text" name="email{IDRESP}" id="email{IDRESP}" value="{EMAILRESP}" style="text-transform:none;width:280px;" />
								<a href="javascript:void(0)" onclick="ajaxAlteraEmail('cadbiblioteca',{IDRESP},$('email{IDRESP}').value);"><img align="absmiddle" src="<?php echo URL_IMG; ?>bt_ok.png" border="0" /></a>
							</td>
							<td class="no-wrap tdcenter"><img src="<?php echo URL_IMG; ?>{IMGSTATUS}" title="{DESC_STATUS}" alt="{DESC_STATUS}" /></td>
							<td class="tdcenter">
							<?php if ($ATIVO == 'N') { ?>
							<a href="javascript:void(0);" onclick="javascript:ajaxReenviaEmailValicadao('{LOGIN}');">Clique aqui</a>
							<?php } ?>
							</td>
							<td class="tdcenter">
							<?php if ($ATIVO == 'S') { ?>
							<a href="javascript:void(0);" onclick="javascript:ajaxEnviaRedefinicaoSenha('{LOGIN}','{EMAILRESP}');">Clique aqui</a>
							<?php } ?>
							</td>
						</tr>
						<?php 
						foreach ($RESPFIN as $rf) {
						?>
						<tr>
							<td class="tdleft">Do Responsável Financeiro</td>
							<td class="tdleft"><?php echo $rf['RF_IDRESP']?></td>
							<td class="tdleft"><?php echo $rf['RF_CPF_CNPJ']?></td>
							<td class="tdleft"><?php echo $rf['RF_NOMERESP']?></td>
							<td class="tdleft"><?php echo $rf['RF_LOGIN']?></td>
							<td class="no-wrap tdleft">
								<input class="inputText" type="text" name="email<?php echo $rf['RF_IDRESP']?>" id="email<?php echo $rf['RF_IDRESP']?>" value="<?php echo $rf['RF_EMAILRESP']?>" style="text-transform:none;width:280px;" />
								<a href="javascript:void(0)" onclick="ajaxAlteraEmail('cadbibliotecafinan',<?php echo $rf['RF_IDRESP']?>,$('email<?php echo $rf['RF_IDRESP']?>').value);"><img align="absmiddle" src="<?php echo URL_IMG; ?>bt_ok.png" border="0" /></a>
							</td>
							<td class="no-wrap tdcenter"><img src="<?php echo URL_IMG; ?><?php echo $rf['RF_IMGSTATUS']?>" title="<?php echo $rf['RF_DESC_STATUS']?>" alt="<?php echo $rf['RF_DESC_STATUS']?>" /></td>
							<td class="tdcenter">
							<?php if ($rf['RF_ATIVO'] == 'N') { ?>
							<a href="javascript:void(0);" onclick="javascript:ajaxReenviaEmailValicadao('<?php echo $rf['RF_LOGIN']?>');">Clique aqui</a>
							<?php } ?>
							</td>
							<td class="tdcenter">
							<?php if ($rf['RF_ATIVO'] == 'S') { ?>
							<a href="javascript:void(0);" onclick="javascript:ajaxEnviaRedefinicaoSenha('<?php echo $rf['RF_LOGIN']?>','<?php echo $rf['RF_EMAILRESP']?>');">Clique aqui</a>
							<?php } ?>
							</td>
						</tr>
						<?php } ?>
						<?php 
						foreach ($COMITE as $ca) {
						?>
						<tr>
							<td class="tdleft">Do Comitê de Acervo</td>
							<td class="tdleft"><?php echo $ca['CA_IDRESP']?></td>
							<td class="tdleft"><?php echo $ca['CA_CPF_CNPJ']?></td>
							<td class="tdleft"><?php echo $ca['CA_NOMERESP']?></td>
							<td class="tdleft"><?php echo $ca['CA_LOGIN']?></td>
							<td class="no-wrap tdleft">
								<input class="inputText" type="text" name="email<?php echo $ca['CA_IDRESP']?>" id="email<?php echo $ca['CA_IDRESP']?>" value="<?php echo $ca['CA_EMAILRESP']?>" style="text-transform:none;width:280px;" />
								<a href="javascript:void(0)" onclick="ajaxAlteraEmail('cadbibliotecacomite',<?php echo $ca['CA_IDRESP']?>,$('email<?php echo $ca['CA_IDRESP']?>').value);"><img align="absmiddle" src="<?php echo URL_IMG; ?>bt_ok.png" border="0" /></a>
							</td>
							<td class="no-wrap tdcenter"><img src="<?php echo URL_IMG; ?><?php echo $ca['CA_IMGSTATUS']?>" title="<?php echo $ca['CA_DESC_STATUS']?>" alt="<?php echo $ca['CA_DESC_STATUS']?>" /></td>
							<td class="tdcenter">
							<?php if ($ca['CA_ATIVO'] == 'N') { ?>
							<a href="javascript:void(0);" onclick="javascript:ajaxReenviaEmailValicadao('<?php echo $ca['CA_LOGIN']?>');">Clique aqui</a>
							<?php } ?>
							</td>
							<td class="tdcenter">
							<?php if ($ca['CA_ATIVO'] == 'S') { ?>
							<a href="javascript:void(0);" onclick="javascript:ajaxEnviaRedefinicaoSenha('<?php echo $ca['CA_LOGIN']?>','<?php echo $ca['CA_EMAILRESP']?>');">Clique aqui</a>
							<?php } ?>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
				<br /><br />
				<table cellspacing="0" align="center" cellpadding="0" class="extensions" width="30%">
					<thead>
						<tr class="header">
							<th><div align="center">Ações</div></th>
						</tr>
						<tr class="header-separator">
							<td>&nbsp;</td>
						</tr>
					</thead>
					<tbody>
						<?php
						if ($TEMBLOQUEIO == "1") {
							$bloqdisabled = '';
						} else {
							$bloqdisabled = 'disabled="disabled"';
						}
						?>
						<tr>
							<td class="tdcenter"><input class="buttonPadrao" type="button" value="Desbloqueia Biblioteca" style="width: 300px;" onclick="ajaxDesbloqueiaBiblioteca({IDRESP});" <?php echo $bloqdisabled ?> /></td>
						</tr>
						<tr>
							<td class="tdcenter">
							<?php if ($HABILITADO == "S") { ?>
							<input class="buttonPadrao" type="button" value="Desabilita Biblioteca" style="width: 300px;" onclick="ajaxSetHabilitado({IDRESP},'N');" />
							<?php } else { ?>
							<input class="buttonPadrao" type="button" value="Habilita Biblioteca" style="width: 300px;" onclick="ajaxSetHabilitado({IDRESP},'S');" />
							<?php } ?>
							</td>
						</tr>
						<?php
						if ($IDPEDIDOSTATUS == "2" || $IDPEDIDOSTATUS == 3) { 
							$peddisabled = '';
						} else {
							$peddisabled = 'disabled="disabled"';
						}
						?>
						<tr>
							<td class="tdcenter"><input class="buttonPadrao" type="button" value="Reativa Pedido" style="width: 300px;" onclick="alteraStatus({IDPEDIDO});" <?php echo $peddisabled ?> /></td>
						</tr>
						<?php
						if ($TEMPDV == "1") {
							$pdvdisabled = '';
						} else {
							$pdvdisabled = 'disabled="disabled"';
						}
						?>
						<tr>
							<td class="tdcenter"><input class="buttonPadrao" type="button" value="Cancela Ponto de Venda" style="width: 300px;" onclick="ajaxCancelaPDV({IDRESP});" <?php echo $pdvdisabled ?> /></td>
						</tr>		
					</tbody>
				</table>
			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
</body>
</html>