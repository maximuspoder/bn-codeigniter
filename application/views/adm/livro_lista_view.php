<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
</head>
<body>
	<div id="wrapper">
		<?php monta_header(1); ?>
		<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
		<?php 
            // istancia o CI;
            $CI =& get_instance();
            $CI->load->library('session');
        ?>
		<div class="divtitulo">
			<div class="divtitulo2">
				>> LIVROS
			</div>
		</div>
		
		<div id="middle">
			<div id="container" style="text-align:center;">
				<div class="main_content_center" style="margin-top:10px;">
					<center>
						<form name="filtros_lista" id="filtros_lista" method="post" class="form_filtro" action="<?php echo URL_EXEC; ?>adm/listaLivro">
							<fieldset style="width:94%">
								<legend>Filtros</legend>
								<div class="filtro_campo">
									<span>Status:</span>
									<select name="filtro_status" id="filtro_status" class="select" style="width:100px;">
										<option value="" <?php echo set_select('filtro_status',  '', TRUE); ?>></option>
										<option value="S" <?php echo set_select('filtro_status',  'S'); ?>>Confirmado</option>
										<option value="N" <?php echo set_select('filtro_status',  'N'); ?>>Não Confirmado</option>
									</select>
								</div>
                                <div class="filtro_campo">
									<span class="margin">Edital:</span>
									<select name="filtro_programa" id="filtro_programa" class="select" style="width:150px;">
										<option value=""  <?php echo set_select('filtro_programa',  '', TRUE); ?>></option>
										<?php
                                        foreach($programas as $row)
                                        {
                                            echo '<option value="' . $row['IDPROGRAMA'] . '" ' . set_select('filtro_programa', $row['IDPROGRAMA']) . ' >' . $row['DESCPROGRAMA'] . '</option>';
                                        }
                                        ?>
									</select>
								</div>
                                <div class="filtro_campo">
									<span class="margin">Situação no edital:</span>
									<select name="filtro_livro_popular" id="filtro_livro_popular" class="select" style="width:100px;">
										<option value=""  <?php echo set_select('filtro_livro_popular',  '', TRUE); ?>></option>
										<option value="S" <?php echo set_select('filtro_livro_popular',  'S'); ?>>Incluído</option>
										<option value="N" <?php echo set_select('filtro_livro_popular',  'N'); ?>>Não Incluído</option>
									</select>
								</div>
								<div class="filtro_campo">
									<span class="margin">ISBN:</span>
									<input type="text" name="filtro_isbn" id="filtro_isbn" class="inputText" style="width:100px;" value="<?php echo set_value('filtro_isbn'); ?>" />
								</div>
								<div class="filtro_campo">
									<span class="margin">Título:</span>
									<input type="text" name="filtro_titulo" id="filtro_titulo" class="inputText" style="width:100px;" value="<?php echo set_value('filtro_titulo'); ?>" />
								</div>
								<div class="filtro_campo">
									<span class="margin">Autor:</span>
									<input type="text" name="filtro_autor" id="filtro_autor" class="inputText" style="width:100px;" value="<?php echo set_value('filtro_autor'); ?>" />
								</div>
								<div class="filtro_campo">
									<span class="margin">Editora:</span>
									<input type="text" name="filtro_editora" id="filtro_editora" class="inputText" style="width:150px;" value="<?php echo set_value('filtro_editora'); ?>" />
								</div>
								<div class="filtro_campo"><span class="margin"><input type="submit" value="Enviar" class="buttonPadrao" /></span></div>
							</fieldset>
						</form>
					</center>
					
					<table cellspacing="0" align="center" cellpadding="0" class="extensions t95" style="margin-bottom: 20px;">
						<thead>
							<tr class="header">
                                <th title="CONFIRMAÇÃO" alt="CONFIRMAÇÃO">Status</th>
								<th class="tdright">ISBN</th>
								<th>Editora</th>
								<th>Autor</th>
								<th class="w100">Título</th>
								<th>Edição</th>
								<th>Ano</th>
                                <th>Edital</th>
								<th>Atualizado em</th>
								<th>Opções</th>
							</tr>
							<tr class="header-separator">
								<td colspan="10">&nbsp;</td>
							</tr>
						</thead>
						<tbody>
							{livros}
							<tr class="{CORLINHA}">
                                <td class="no-wrap tdcenter"><img src="<?php echo URL_IMG; ?>{IMGSTATUS}" title="{DESC_STATUS}" alt="{DESC_STATUS}" /></td>
								<td class="no-wrap tdright">{ISBN_MOSTRAR}</td>
								<td class="no-wrap pr_20 tdleft">{EDITORA}</td>
								<td class="no-wrap pr_20 tdleft">{AUTOR}</td>
								<td class="w100 pr_20 tdleft">{TITULO}</td>
								<td class="no-wrap tdcenter">{EDICAO}</td>
								<td class="no-wrap tdcenter">{ANO}</td>
                                <td class="no-wrap tdcenter"><span id="lp{IDLIVRO}">{PROGRAMA}</span></td>
								<td class="no-wrap pr_20 tdleft">{DATA_ATU}</td>
								<td class="tdleft">
                                    <a href="<?php echo URL_EXEC; ?>bnglobal/detalharLivro/{IDLIVRO}"><img src="<?php echo URL_IMG; ?>search_16.png" title="VISUALIZAR" border="0" alt="VISUALIZAR" style="cursor:pointer;" /></a>
                                </td>
							</tr>
							{/livros}
							
							<?php
							if ($vazio == TRUE)
							{
								?>
								<tr>
									<td colspan="10" class="no-wrap tdcenter">Nenhum livro encontrado.</td>
								</tr>
								<?php 
							}
							else
							{
								?>
								<tr class="noEfeito">
									<td colspan="10" class="no-wrap tdcenter">
										<div style="display: inline; float: left;">Total Registros: <?php echo $qtdReg; ?></div>
										<?php montaPaginacaoPesquisa($nPaginas, $nPag, $limit_de, $exibir_pp, URL_EXEC . 'adm/listaLivro/', 'filtros_lista'); ?>
									</td>
								</tr>
								<?php
							}
							?>
						</tbody>
					</table>
				</div>
			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
</body>
</html>