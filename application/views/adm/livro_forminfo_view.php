<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
</head>
<body>
	<div id="wrapper">
		<?php monta_header(1); ?>
		<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
		
		<div class="divtitulo">
			<div class="divtitulo2">
				>> DADOS LIVRO
			</div>
		</div>
		
        <div id="middle">
            
            <div id="container" style="padding-bottom: 10px; ">
                <div class="main_content_left" style="margin-top:10px; margin-bottom:20px;">
                    <form name="form_cad" id="form_cad" method="post" >
                        <fieldset style="width:680px; margin-left:20px;">
                            <legend><b>Informações do Livro</b></legend>
                            <div id="fs_div">
                                <table id="tabelaCadastros2" align="center" border="0">
                                    <tr>
                                        <td colspan="4" class="areaCadastro2"><img src="<?php echo URL_IMG; ?>geral.png" style="padding-right:10px;" /> Dados gerais</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top:10px;">Código ISBN*:</td>
                                        <td style="padding-top:10px; text-align: left;" colspan="3">
                                            <input class="inputText inputDisab" type="text" style="width:170px; text-transform:none;" value="{isbn}" readonly="readonly" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Título:</td>
                                        <td colspan="3"><input class="inputText inputDisab" type="text" style="width:450px;" readonly="readonly" value="{titulo}" /></td>
                                    </tr>
                                    <tr>
                                        <td>Autor:</td>
                                        <td colspan="3"><input class="inputText inputDisab" type="text" style="width:450px;" readonly="readonly" value="{autor}" /></td>
                                    </tr>
                                    <tr>
                                        <td>Suporte:</td>
                                        <td colspan="3"><input class="inputText inputDisab" type="text" style="width:450px;" readonly="readonly" value="{suporte_aux}" /></td>
                                    </tr>
                                    <tr>
                                        <td>Ficha Catalográfica:</td>
                                        <td colspan="3"><input class="inputText inputDisab" type="text" style="width:450px;" readonly="readonly" value="{fichacat_aux}" /></td>
                                    </tr>
                                    <tr>
                                        <td>Número de Páginas:</td>
                                        <td colspan="3">
                                            <input class="inputText inputDisab" type="text" style="width: 70px;" readonly="readonly" value="{npaginas}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Dimensão:</td>
                                        <td colspan="3">
                                            Menor: <input class="inputText inputDisab" type="text" style="text-align:right; width:50px;" readonly="readonly" value="{formato_a}"/> cm
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            Maior: <input class="inputText inputDisab" type="text" style="text-align:right; width:50px;" readonly="readonly" value="{formato_b}"/> cm
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Papel:</td>
                                        <td colspan="3">
                                            Miolo: <input class="inputText inputDisab" type="text" style="text-align:right; width:50px;" readonly="readonly" value="{papel_miolo}" /> g/m²
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            Capa: <input class="inputText inputDisab" type="text" style="text-align:right; width:50px;" readonly="readonly" value="{papel_capa}" /> g/m²
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Peso:</td>
                                        <td colspan="3">
                                            <input class="inputText inputDisab" type="text" style="text-align:right; width:75px;" readonly="readonly" value="{peso}" /> gramas
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Orelha:</td>
                                        <td colspan="3"><input class="inputText inputDisab" type="text" style="width:450px;" readonly="readonly" value="{orelha_aux}" /></td>
                                    </tr>
                                    <tr>
                                        <td>Tipo de Capa:</td>
                                        <td colspan="3"><input class="inputText inputDisab" type="text" style="width:450px;" readonly="readonly" value="{tipocapa_aux}" /></td>
                                    </tr>
                                    <tr>
                                        <td>Acabamento:</td>
                                        <td colspan="3"><input class="inputText inputDisab" type="text" style="width:450px;" readonly="readonly" value="{acabamento_aux}" /></td>
                                    </tr>
                                    <tr>
                                        <td>Idioma Original:</td>
                                        <td colspan="3"><input class="inputText inputDisab" type="text" style="width:450px;" readonly="readonly" value="{idioma_aux}" /></td>
                                    </tr>
                                    <tr>
                                        <td>Idioma Tradução:</td>
                                        <td colspan="3"><input class="inputText inputDisab" type="text" style="width:450px;" readonly="readonly" value="{idiomatrad_aux}" /></td>
                                    </tr>
                                    <tr>
                                        <td>Assunto:</td>
                                        <td colspan="3">
                                            <input class="inputText inputDisab" type="text" readonly="readonly" value="{assunto}" style="width: 450px;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Edição:</td>
                                        <td colspan="3">
                                            <input class="inputText inputDisab" type="text" size="8" readonly="readonly" value="{edicao}" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            Ano: <input class="inputText inputDisab" type="text" size="8" readonly="readonly" value="{ano}" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Palavras-Chave:</td>
                                        <td colspan="3">
                                            <input class="inputText inputDisab" type="text" style="width: 450px;" readonly="readonly" value="{palavraschave}" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sinopse:</td>
                                        <td colspan="3">
                                            <div class="inputText inputDisab" style="width: 450px; height: 60px; overflow-y: scroll;">{sinopse}</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Link para Dados:</td>
                                        <td colspan="3"><input class="inputText inputDisab" type="text" style="width:450px; text-transform:none;" readonly="readonly" value="{linkdados}" /></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="text-align: center; padding-top:15px;">
                                            <input class="buttonPadrao" type="button" value="  Voltar  " onclick="history.back();" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div><!-- #container-->
        </div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
</body>
</html>