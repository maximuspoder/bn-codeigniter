<div id="container" style="overflow-y: auto; ">
    <div class="main_content_left">
            <fieldset style="width:540px; margin-left:20px;height:100%;">
                <legend><b>Informações de Log de Gerenciamento</b></legend>
                <div id="fs_div">
                    <table id="tabelaCadastros2" align="center" border="0" width="100%">
                    
                    	<tr>
                    		<th align="left" width="30%">Data</th>
                    		<th align="left" width="15%">Ação</th>
                    		<th align="left" width="55%">Motivo</th>
                    	</tr>
                    	<?php if (count($log) > 0) { ?>
						{log}
                        <tr>
                            <td>{DATA_LOG_FORMAT}</td>
                            <td>{TIPOLOG}</td>
                            <td>{MOTIVO}</td>
                        </tr>
                        {/log}
                        <?php } else { ?>
                        <tr>
                            <td colspan="3">Não foram encontradas informações para esta biblioteca.</td>
                        </tr>
                        <?php } ?>
                        <tr>
                            <td colspan="3" style="text-align: center; padding-top:15px;">
                                <input class="buttonPadrao" type="button" value="  Fechar  " onclick="$('ligthbox_bg').setStyle({display : 'none'});" />
                             </td>
                        </tr>
                    </table>
                </div>
            </fieldset>
        </form>
    </div>
</div><!-- #container-->

