<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
</head>
<body>
	<div id="wrapper">
		<?php monta_header(1); ?>
		<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
		
		<div class="divtitulo">
			<div class="divtitulo2">
				>> CONFIGURAÇÕES
			</div>
		</div>
		
		<div id="middle">
			<div id="container" style="text-align:center;">
				<div class="main_content_center" style="margin-top:10px;">
					<div align="center">
					<div style="width:95%;margin-bottom:20px;">
					<?php
						$erros = Array();
						
						if (isset($outrosErros))
						{
							if (is_array($outrosErros))
							{
								for ($i = 0; $i < count($outrosErros); $i++)
								{
									array_push($erros, $outrosErros[$i]);
								}
							}
							else
							{
								array_push($erros, $outrosErros);
							}
						}
						
						exibe_validacao_erros($erros);
					?>
					</div>
					</div>
					<?php 
                        if($acao == 'save' && count($outrosErros) <= 0 && $this->form_validation->run() == true)
                        {
                            echo '<div style="margin: 10px">';
                            echo    '<img src="' . URL_IMG . 'v_peq.png" style="margin:0; padding: 0;" />';
                            echo    '<b> Salvo com Sucesso! </b>';
                            echo '</div>';
                        }
                    ?>
                    <form name="form_configuracao_list" id="form_configuracao_list" method="post" action="<?php echo URL_EXEC; ?>adm/listaConfiguracao">
                        <fieldset style="width:94%; text-align: left; margin: auto;">
								<legend>Filtros</legend>
								<div class="filtro_campo">
									<span>Edital:</span>
                                    <select class="select" name="programa" id="programa" style="min-width:150px;" onchange="document.forms['form_configuracao_list'].submit();">
                                        <?php
                                        foreach($programas as $row)
                                        {
                                            echo '<option value="' . $row['IDPROGRAMA'] . '" ' . set_select('programa', $row['IDPROGRAMA']) . ' >' . $row['DESCPROGRAMA'] . '</option>';
                                        }
                                        ?>
                                    </select>
								</div>
                        </fieldset>
                    </form>
					<form name="form_configuracao" id="form_configuracao" method="post" action="<?php echo URL_EXEC; ?>adm/form_configuracao/save/{programa}">
                        
					<table cellspacing="0" align="center" cellpadding="0" class="extensions t95" style="margin-bottom:20px;">
						<thead>
							<tr class="header">
								<th>ID</th>
								<th>Descrição</th>
								<th>Valor</th>
							</tr>
							<tr class="header-separator">
								<td colspan="4">&nbsp;</td>
							</tr>
						</thead>
						<tbody>
							<?php foreach($configuracoes as $row) { ?>
							<tr class="{CORLINHA}">
								<td class="no-wrap tdcenter" style="width:01%"><?php echo($row['IDCONFIGURACAO']);?></td>
								<td class="no-wrap pr_20 tdleft"><?php echo($row['DESCCONFIG']);?></td>
								<td class="no-wrap tdright" style="width:01%">
								<?php
									$readOnly = '';
									
									if ($row['IDCONFIGURACAO'] == 12 && $movimentoPedido> 0)
									{
										$readOnly = 'readonly="readonly"';
									}
									
									if (($row['IDCONFIGURACAO'] == 13 || $row['IDCONFIGURACAO'] == 14) && $movimentoPrograma > 0)
									{
										$readOnly = 'readonly="readonly"';
									}
									
									switch(strtoupper($row['TIPOCONFIG']))
									{
										case 'PERIODO':
											echo('
												<div style="display:inline-block !important;width:22px;height:22px;text-align:left;">De</div>
												<div style="display:inline-block !important;"><input type="text" class="inputText" name="periodo_de_' . $row['IDCONFIGURACAO'] . '" id="periodo_de_' . $row['IDCONFIGURACAO'] . '" style="width:80px;" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="' . $formData['periodo_de_' . $row['IDCONFIGURACAO']] . '" autocomplete="off" onKeyUp="maskx(this, msk_date)" maxlength="10" /></div>
												<div style="display:inline-block !important;width:22px;height:20px;text-align:left;">Até</div>
												<div style="display:inline-block !important;"><input type="text" class="inputText" name="periodo_ate_' . $row['IDCONFIGURACAO'] . '" id="periodo_ate_' . $row['IDCONFIGURACAO'] . '" style="width:80px;" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="' . $formData['periodo_ate_' . $row['IDCONFIGURACAO']] . '" autocomplete="off" onKeyUp="maskx(this, msk_date)" maxlength="10" /></div>');
										break;
										
										case 'INTEIRO':
											echo('<div style="margin-left:24px;"><input type="text" class="inputText" name="inteiro_' . $row['IDCONFIGURACAO'] . '" id="inteiro_' . $row['IDCONFIGURACAO'] . '" style="width:80px; text-align:center;" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="' . $formData['inteiro_' . $row['IDCONFIGURACAO']] . '" autocomplete="off" ' . $readOnly . ' onkeypress="return maskInteger(this, event)" /></div>');
										break;
										
										case 'DECIMAL':
											echo('<div style="margin-left:9px;">R$ <input type="text" class="inputText" name="decimal_' . $row['IDCONFIGURACAO'] . '" id="decimal_' . $row['IDCONFIGURACAO'] . '" style="width:80px;" dir="rtl" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="' . $formData['decimal_' . $row['IDCONFIGURACAO']] . '" autocomplete="off" ' . $readOnly . ' onkeypress="return maskMoeda(this, event)" /></div>');
										break;
									}
								?>
								</td>
							</tr>
							<?php } 
							if ($vazio == TRUE)
							{
								?>
								<tr>
									<td colspan="7" class="no-wrap tdcenter">Nenhuma configuração encontrada.</td>
								</tr>
								<?php 
							}
							else
							{
								?>
                                <tr>
                                    <td colspan="2"></td>
                                    <td style="width:01%"><input type="submit" class="buttonPadrao" value="Salvar" /></td>
                                </tr>
							
								<tr class="noEfeito">
									<td colspan="7" class="no-wrap tdcenter">
										<div style="display: inline; float: left;">Total Registros: <?php echo $qtdReg; ?></div>
										<?php montaPaginacao($nPaginas, $nPag, $limit_de, $exibir_pp, URL_EXEC . 'adm/listaConfiguracao/'); ?>
									</td>
								</tr>
								<?php
							}
							?>
						</tbody>
					</table>  
					</form>
				</div>
			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
</body>
</html>