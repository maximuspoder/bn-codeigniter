<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>pesquisa.js" type="text/javascript"></script>
	<script language="javascript">
	function limpar() {
		$('filtro_titulo').value = '';
		$('filtro_autor').value = '';
	}
	
	</script>
</head>
<body>
	<div id="wrapper">
		<?php add_elementos_CONFIG(); ?>
        <?php add_div_ligthbox('560', '270'); ?>
		<?php monta_header(1); ?>
		<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
		
		<br />
		<br />

		<div id="middle">
			<table cellpadding="0" cellspacing="0" border="0" width="98%" style="margin: 0 30px 20px 20px;">
			<tr>
				<td id="td_livros">
					<div id="container_livros">
						<div class="divtitulo2" style="text-align: center;">
							>> BLOQUEIO INDICAÇÃO
						</div>

						<div id="filtros" style="text-align: center; margin-top: 20px;">
							<form name="filtros_lista" id="filtros_lista" method="post" class="form_filtro" action="<?php echo URL_EXEC; ?>adm/bloqueioLivroIndicacao">
								<div id="filtro_agrupador" style="height:55px; border: none;">
									<div id="titulo_filtro" class="titulo" style="padding-bottom: 5px;">FILTRE SUA BUSCA</div>
									<div class="filtro_campo">
										<span class="margin">Título:</span>
										<input type="text" name="filtro_titulo" id="filtro_titulo" class="inputText" style="width:100px;" value="<?php echo set_value('filtro_titulo'); ?>" />
									</div>
									<div class="filtro_campo">
										<span class="margin">Autor:</span>
										<input type="text" name="filtro_autor" id="filtro_autor" class="inputText" style="width:100px;" value="<?php echo set_value('filtro_autor'); ?>" />
									</div>
									<div class="filtro_campo"><span class="margin"><input type="submit" value="Pesquisar" class="buttonPadrao" /> &nbsp;&nbsp;
									<input type="button" value="Limpar" class="buttonPadrao" onclick="limpar();" /></span></div>
									<div align="right" style="float:right;">
										<input type="hidden" name="visualizacao" id="visualizacao" value="lista" />
									</div>
								</div>
							</form>
						</div>
						<div><hr style="border-top:dotted #ccc 1px;border-bottom:none;border-left:none;border-right: none;" /></div>
						<?php if ($pesquisa == false){ ?>
						<div id="msg_sem_dados"><img src="<?php echo(URL_IMG);?>/atencao_peq.PNG" style="margin:0 10px;float:left" />Atenção! Informe acima título e/ou autor para pesquisa.</div>
						<?php } elseif ($vazio == true){ ?>
						<div id="msg_sem_dados"><img src="<?php echo(URL_IMG);?>/atencao_peq.PNG" style="margin:0 10px;float:left" />Atenção! Nenhum livro localizado com os critérios especificados.</div>
						<?php } else { ?>
					</td>
				</tr>
				<?php if ($pesquisa == true && $vazio == false) { ?>
				<tr><td>
				<div style="margin:-10px 0 30px 25px;">
					<div style="float: left; padding-left: 50px;">Páginas: <?php montaPaginacaoPesquisa($nPaginas, $nPag, $limit_de, $exibir_pp, URL_EXEC . 'adm/bloqueioLivroIndicacao/', 'filtros_lista'); ?></div>
					<div style="float: right; padding-right: 50px;"><?php echo $qtdReg; ?> registro<?php echo $qtdReg != 1 ? 's':''; ?> encontrado<?php echo $qtdReg != 1 ? 's':''; ?>.</div>
				</div>
				</td></tr>
				<?php } ?>
				<tr>
					<td>						
						<div id="gridbusca" style="display: none;">
						<?php foreach($livros as $livro) {?>
						<div id="box_livro_pesquisa">
							<div style="margin:10px;">
								<div style="min-height:115px;">
									<div style="font-size:11px;max-height:35px;overflow-y:hidden;overflow-x:hidden;cursor:default;" title="<?php echo($livro['TITULO']);?>"><strong><?php echo($livro['TITULO']);?></strong></div>
									<div style="font-size:11px;"><strong>Autor:</strong> <?php echo($livro['AUTOR']);?></div>
                                    <div style="text-align: center; width: 40px; cursor: pointer;" onclick="indicarLivro(<?php echo $livro['IDLIVRO'] .",". $nPag;?>);">
                                        <img src="<?php echo(URL_IMG);?>/like.png" style="margin:0 10px;float:left" />
                                        <div>Indicar</div>
                                    </div>
								</div>
							</div>
						</div>
						<?php } ?>
						</div>
					</div>

					<div id="listabusca" style="padding-left: 25px; display: block;">
					<table cellspacing="0" cellpadding="3" border="0" width="90%" class="extensions t95">
						<tr class="header" style="height:22px;">
							<th width="100"><div align="center">Ação</div></th>
							<th width="40%">Título</th>
							<th width="40%">Autor</th>
                            <th width="10%"><div align="center">N° de Indicações</div></th>
						</tr>
					<?php 
					$i = 0;
					foreach($livros as $livro) {
					?>
						<tr class="<?php echo $i%2==0?"l_i":"l_p"; ?>">
							<td align="left">
							<?php if($livro['BLOQUEADO'] == "N") { ?>
                            	<div style="text-align: left; width: 80px; cursor: pointer;" onclick="livroIdicacaoBloqueio(<?php echo $nPag;?>, <?php echo $livro['IDLIVRO'];?>, 1 );">
                                	<img src="<?php echo(URL_IMG);?>/add.png" style="margin:1px 7px 1px 5px;float:left" align="absmiddle" title="Clique para bloquear este título" />Bloquear
                                </div>
                            <?php } else { ?>
                           	    <div style="text-align: left; width: 100px; cursor: pointer;" onclick=" livroIdicacaoBloqueio(<?php echo $nPag;?>, <?php echo $livro['IDLIVRO'];?>, 2 );">
                                	<img src="<?php echo(URL_IMG);?>/x_mini.gif" style="margin:2px 7px;float:left;" align="absmiddle" title="Clique para desfazer o bloqueio deste título" />Desbloquear
                                </div>
                           	<?php } ?>
							</td>
							<td align="left"><b><?php echo($livro['TITULO']);?></b></td>
							<td align="left"><?php echo($livro['AUTOR']);?></td>
                            <td align="right"><?php echo($livro['N_INDICACAO']);?></td>
						</tr>
					<?php
					$i++;
					} 
					?>
					</table>
					<br /><br />
					</div>
					<?php } ?>
				</td>
			</tr>
			<?php if ($pesquisa == true && $vazio == false) { ?>
				<tr><td>
				<div style="margin:-10px 0 30px 25px;">
					<div style="float: left; padding-left: 50px;">Páginas: <?php montaPaginacaoPesquisa($nPaginas, $nPag, $limit_de, $exibir_pp, URL_EXEC . 'adm/bloqueioLivroIndicacao/', 'filtros_lista'); ?></div>
					<div style="float: right; padding-right: 50px;"><?php echo $qtdReg; ?> registro<?php echo $qtdReg != 1 ? 's':''; ?> encontrado<?php echo $qtdReg != 1 ? 's':''; ?>.</div>
				</div>
				</td></tr>
			<?php } ?>
			</table>
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
</body>
</html>