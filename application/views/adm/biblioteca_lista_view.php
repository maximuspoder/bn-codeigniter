<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>biblioteca.js" type="text/javascript"></script>
</head>
<body>
	<div id="wrapper">
		<?php monta_header(1); ?>
		<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
		<?php add_elementos_CONFIG(); ?>
		
		<div class="divtitulo">
			<div class="divtitulo2">
				>> BIBLIOTECAS
			</div>
		</div>
		
		<div id="middle">
			<div id="container" style="text-align:center;">
				<div class="main_content_center" style="margin-top:10px;">
					<center>
						<form name="filtros_lista" id="filtros_lista" method="post" class="form_filtro" action="<?php echo URL_EXEC; ?>adm/listaBiblioteca">
							<fieldset style="width:94%">
								<legend>Filtros</legend>
								<div class="filtro_campo">
									<span>Status:</span>
									<select name="filtro_status" id="filtro_status" class="select" style="width:100px;">
										<option value=""  <?php echo(($filtros['filtro_status'] == '')   ? 'selected="selected"' : ''); ?>></option>
										<option value="S" <?php echo(($filtros['filtro_status'] == 'S')  ? 'selected="selected"' : ''); ?>>Validado</option>
										<option value="N" <?php echo(($filtros['filtro_status'] == 'N')  ? 'selected="selected"' : ''); ?>>Não Validado</option>
									</select>
								</div>
								<div class="filtro_campo">
									<span class="margin">Habilitado:</span>
									<select name="filtro_habilitado" id="filtro_habilitado" class="select" style="width:50px;">
										<option value=""  <?php echo(($filtros['filtro_habilitado'] == '')  ? 'selected="selected"' : ''); ?>></option>
										<option value="S" <?php echo(($filtros['filtro_habilitado'] == 'S') ? 'selected="selected"' : ''); ?>>Sim</option>
										<option value="N" <?php echo(($filtros['filtro_habilitado'] == 'N') ? 'selected="selected"' : ''); ?>>Não</option>
									</select>
								</div>
								<div class="filtro_campo">
									<span class="margin">Razão Social / Nome Fantasia:</span>
									<input type="text" name="filtro_razao_nome_fantasia" id="filtro_razao_nome_fantasia" class="inputText" style="width:150px;" value="<?php echo($filtros['filtro_razao_nome_fantasia']);?>" />
								</div>
								
                                
                                <div class="filtro_campo">
									<span class="margin">Estado:</span>
									<select name="filtro_estado" id="filtro_estado" class="select" style="width:50px;" onchange="ajaxGetCidades(this.value);"><?php echo($options_estados); ?></option>
									</select>
								</div>
                                <div class="filtro_campo">
									<span class="margin">Cidade:</span>
									<select name="filtro_cidade" id="filtro_cidade" class="select" style="width:200px;">
										<option>-- Selecione um Estado --</option>
										<?php echo($options_cidades); ?>
									</select>
								</div>
                                
                                
								<div class="filtro_campo"><span class="margin"><input type="submit" value="Enviar" class="buttonPadrao" /></span></div>
							</fieldset>
						</form>
					</center>
					
					<table cellspacing="0" align="center" cellpadding="0" class="extensions t95">
						<thead>
							<tr class="header">
								<th>Status</th>
								<th class="w100" style="width:250px">Razão Social / Nome</th>
								<th>CNPJ/CPF</th>
								<th>Telefone Geral</th>
								<th>Responsável</th>
								<th>E-mail Resp.</th>
								<th >Ativação em</th>
								<th>Habilitado</th>
							</tr>
							<tr class="header-separator">
								<td colspan="8">&nbsp;</td>
							</tr>
						</thead>
						<tbody>
							{bibliotecas}
							<tr class="{CORLINHA}">
								<td class="no-wrap tdcenter"><img src="<?php echo URL_IMG; ?>{IMGSTATUS}" title="{DESC_STATUS}" alt="{DESC_STATUS}" /></td>
								<td class="pr_20 tdleft" style="width:100px">{RAZAOSOCIAL}</td>
								<td class="no-wrap pr_20 tdleft">{CPF_CNPJ}</td>
								<td class="no-wrap pr_20 tdleft">{TELEFONEGERAL}</td>
								<td class="no-wrap pr_20 tdleft">{NOMERESP}</td>
								<td class="no-wrap pr_20 tdleft">{EMAILRESP}</td>
								<td class="no-wrap pr_20 tdleft">{ATIVACAO}</td>
								<td class="no-wrap tdcenter">{USER_HABILITADO}</td>
							</tr>
							{/bibliotecas}
							
							<?php
							if ($vazio == TRUE)
							{
								?>
								<tr>
									<td colspan="8" class="no-wrap tdcenter">Nenhuma biblioteca encontrada.</td>
								</tr>
								<?php 
							}
							else
							{
								?>
								<tr class="noEfeito">
									<td colspan="8" class="no-wrap tdcenter">
										<div style="display: inline; float: left;">Total Registros: <?php echo $qtdReg; ?></div>
										<?php montaPaginacaoPesquisa($nPaginas, $nPag, $limit_de, $exibir_pp, URL_EXEC . 'adm/listaBiblioteca/', 'filtros_lista'); ?>
									</td>
								</tr>
								<?php
							}
							?>
						</tbody>
					</table>
				</div>
			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
</body>
</html>