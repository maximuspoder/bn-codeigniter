<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>biblioteca.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>sniic.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>cep.js" type="text/javascript"></script>
</head>
<body>
    <div id="wrapper">
		<?php monta_header(1); ?>
		<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
		<?php add_div_ligthbox('800', '400'); ?>
		<?php add_elementos_CONFIG(); ?>
		
		<div class="divtitulo">
			<div class="divtitulo2">
				>> GERENCIADOR SNIIC - INTEGRAÇÃO DE CADASTROS
			</div>
		</div>
		
		<div id="middle">
			<div id="container" style="text-align:center;">
				<div class="main_content_center" style="margin-top:10px;">
					<center>
						<form name="filtros_lista" id="filtros_lista" method="post" class="form_filtro" action="<?php echo URL_EXEC; ?>adm/gerenciadorSniicIntegracao">
							<fieldset style="width:94%">
								<legend>Filtros</legend>
								<div class="filtro_campo">
									<span>CPF / CNPJ:</span>
									<input type="text" name="filtro_cpf_cnpj" id="filtro_cpf_cnpj" class="inputText" style="width:150px;" value="<?php echo($filtros['filtro_cpf_cnpj']);?>" />
								</div>
								<div class="filtro_campo">
									<span class="margin">Razão Social / Nome Fantasia:</span>
									<input type="text" name="filtro_razao_nome_fantasia" id="filtro_razao_nome_fantasia" class="inputText" style="width:150px;" value="<?php echo($filtros['filtro_razao_nome_fantasia']);?>" />
								</div>
								<div class="filtro_campo">
									<span class="margin">Tipo Cadastro:</span>
									<select name="filtro_tipo_cad" id="filtro_tipo_cad" class="select" style="width:50px;">
										<option value=""       <?php echo(($filtros['filtro_tipo_cad'] == '')      ? 'selected="selected"' : ''); ?> > - </option>
										<option value="ANTIGO" <?php echo(($filtros['filtro_tipo_cad'] == 'ANTIGO')? 'selected="selected"' : ''); ?> >ANTIGO</option>
										<option value="NOVO"   <?php echo(($filtros['filtro_tipo_cad'] == 'NOVO')  ? 'selected="selected"' : ''); ?>   >NOVO</option>
									</select>
								</div>
								<div class="filtro_campo">
									<span class="margin">Estado:</span>
									<select name="filtro_estado" id="filtro_estado" class="select" style="width:50px;" onchange="ajaxGetCidadesSniic(this.value);"><?php echo($options_estados); ?></option>
									</select>
								</div>
								<div class="filtro_campo">
									<span class="margin">Cidade:</span>
									<select name="filtro_cidade" id="filtro_cidade" class="select" style="width:200px;">
										<option>-- Selecione um Estado --</option>
										<?php echo($options_cidades); ?></option>
									</select>
								</div>											
								<div class="filtro_campo"><span class="margin"><input type="submit" value="Enviar" class="buttonPadrao" /></span></div>
								<div style="float:right">
									<div class="filtro_campo"><span class="margin"><input type="checkbox" id="filtro_cidade_verificada" onclick="ajax_set_cidade_verificada_sniic();" <?php echo(($cidade_verificada) ? 'checked="true"' : '');?> /><div style="margin-top:5px;display:inline-block;">Cidade já verificada</div></span></div>
									<div class="filtro_campo"><span class="margin"><input type="button" value="Cidade Anterior" class="buttonPadrao" onclick="window.location.href = '<?php echo URL_EXEC . "adm/gerenciadorSniicIntegracao/$nPag/$anterior"; ?>'" /></span></div>
									<div class="filtro_campo"><span class="margin"><input type="button" value="Próxima Cidade" class="buttonPadrao" onclick="window.location.href = '<?php echo URL_EXEC . "adm/gerenciadorSniicIntegracao/$nPag/$proxima"; ?>'" /></span></div>
								</div>
							</fieldset>
						</form>
					</center>
					
                    <table cellspacing="0" align="center" cellpadding="0" class="extensions t95">
						<thead>
							<tr class="header">		
								<th>COD</th> 				
								<th>CNPJ/CPF</th> 			
                                <th>Razão Social / Nome</th>
								<th>Cidade</th> 			
								<th>UF</th> 				
								<th>Ativação em</th>
								<th>Habilitado</th> 		
								<th>Resp. Financeiro</th> 	
								<th>Comite</th> 			
								<th>Tipo Cad</th>
								<th>Crédito 3º Ed</th>
								<th>Crédito 4º Ed</th>				
								<th colspan="3">Ação</th>			
							</tr>
							<tr class="header-separator">
								<td colspan="10">&nbsp;</td>
							</tr>
						</thead>
						<tbody>							
							<?php
							$i = 1;
							foreach($bibliotecas as $bibli){ ?>
                                                    
                                                    <tr  class="<?php echo $i%2==0?"l_i":"l_p"; ?>" onclick="javascript:listaLogs('<?php echo $bibli['COD'] ?>|<?php echo $bibli['TIPO_CAD'] ?>')">
								<td class="no-wrap pr_20 tdleft"><?php echo $bibli['COD'] ?></td>
								<td class="pr_20 tdleft"><?php echo $bibli['CPF_CNPJ'] ?></td>
								<?php if ($bibli['TIPO_CAD'] == 'ANTIGO') { ?>
								<td class="no-wrap pr_20 tdleft"><?php echo $bibli['NOME_BIBLIOTECA'] ?></td>
								<?php } else { ?>
								<td class="no-wrap pr_20 tdleft"><a href="<?php echo URL_EXEC . "adm/painelControleBibliotecaDetalhe/" . $bibli['COD'] ?>"><?php echo $bibli['NOME_BIBLIOTECA'] ?></a></td>
								<?php } ?>
                                                                <td class="no-wrap pr_20 tdleft"><?php echo $bibli['CIDADE'] ?></td>
								<td class="no-wrap pr_20 tdleft"><?php echo $bibli['UF'] ?></td>
								<td class="no-wrap pr_20 tdleft"><?php echo $bibli['ATIVO'] ?></td>
								<td class="no-wrap tdcenter"><?php echo $bibli['HABILITADO'] ?></td>
								<td class="no-wrap tdcenter"><?php echo $bibli['RESPONSAVEL_FINANCEIRO'] ?></td>
								<td class="no-wrap tdcenter"><?php echo $bibli['COMITE'] ?></td>
								<td class="no-wrap tdcenter"><?php echo $bibli['TIPO_CAD'] ?></td>
								<td class="no-wrap tdright"><?php echo $bibli['VLRCREDITO_INICIAL_3ED'] ?></td>
								<td class="no-wrap tdright"><?php echo $bibli['VLRCREDITO_INICIAL_4ED'] ?></td>									
								<td class="no-wrap tdcenter">
								<?php if($bibli['TIPO_CAD']=='NOVO'){ ?>
								<a href="<?php  echo URL_EXEC . "biblioteca/modaleditbiblioteca/{nPag}/".$bibli['COD'] ?> "><img width="16" height="16" src="<?php echo URL_IMG; ?>Book-Edit-32.png" title="EDITAR" border="0" alt="EDITAR" /></a>
								<?php } elseif($bibli['TIPO_CAD']=='ANTIGO'){ ?>
								<a href="<?php  echo URL_EXEC . "biblioteca/migrar/{nPag}/".$bibli['CHAVE_VALIDACAO'] ?> "><img width="16" height="16" src="<?php echo URL_IMG; ?>Migrate_Arrow_32.png" title="MIGRAR"   border="0" alt="MIGRAR" /></a>
								<?php } ?>
								</td>
								<td  class="no-wrap tdcenter">
								<?php if($bibli['CONTBLOQUEIO']==0) { ?>
								<a href="javascript:bloquearCadastro(<?php echo $bibli['COD'] ?>, '<?php echo $bibli['TIPO_CAD'] ?>', '{nPag}');" ><img width="16" height="16" src="<?php echo URL_IMG; ?>Block_32.png" title="BLOQUEAR" border="0" alt="BLOQUEAR" /></a></td>
								<?php } ?>

							</tr>
                                                    
							<?php
							$i++;
							} 
							?>
							
							<?php
							if ($vazio == TRUE)
							{
								?>
								<tr>
									<td colspan="13" class="no-wrap tdcenter">Nenhuma biblioteca encontrada.</td>
								</tr>
								<?php 
							}
							else
							{
								?>
								<tr class="noEfeito">
									<td colspan="8" class="no-wrap tdcenter">
										<div style="display: inline; float: left;">Total Registros: <?php echo $qtdReg; ?></div>
										<?php montaPaginacaoPesquisa($nPaginas, $nPag, $limit_de, $exibir_pp, URL_EXEC . 'adm/gerenciadorSniic/', 'filtros_lista'); ?>
									</td>
								</tr>
								<?php
							}
							?>
                                                   
						</tbody>
					</table>
				</div>
			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
</body>
</html>