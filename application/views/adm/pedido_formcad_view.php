<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>pdv.js" type="text/javascript"></script>
	<script language="javascript">
		if($('btnSaveForm') != null){window.onpageshow = function(evt){ $('btnSaveForm').disabled = false; }}
	</script>
</head>
<body>
	<div id="wrapper">
		<?php 
            add_elementos_CONFIG(); 
            add_div_ligthbox('650', '400');
            monta_header(1);
            monta_menu($this->session->userdata('tipoUsuario'));
		?>
		<div id="middle">
			<div id="container">
				<div class="main_content_left" style="margin-top:10px; margin-bottom:20px;">
					<?php
						$erros = Array();
						
						if (isset($outrosErros))
						{
							if (is_array($outrosErros))
							{
								for ($i = 0; $i < count($outrosErros); $i++)
								{
									array_push($erros, $outrosErros[$i]);
								}
							}
							else
							{
								array_push($erros, $outrosErros);
							}
						}
						
						exibe_validacao_erros($erros);
					?>
                    {pedido}
					<form enctype="multipart/form-data" method="post" name="form_importa" id="form_importa" action="<?php echo URL_EXEC; ?>pdv/pedido/{IDPEDIDO}/save" onsubmit="return valida_FormImportaPDV();">
						<fieldset style="width:680px; margin-left:20px;">
							<legend><b>Informações do Pedido</b></legend>
							<div id="fs_div">
                                
                                <table  align="left" border="0" width="100%">
                                    <tr>
										<td>Código Pedido:</td>
										<td style="text-align: left;" colspan="3">
											<input class="inputText inputDisab" type="text" style="width:80px; text-transform:none;" value="{IDPEDIDO}" readonly="readonly" />
										</td>
									</tr>
									<tr>
										<td colspan="4" class="areaCadastro2" style="padding-top:10px;"><img src="<?php echo URL_IMG; ?>library.png" style="padding-right:10px;" /> Dados da Biblioteca</td>
                                    </tr>
									<tr>
										<td style="padding-top:10px;">Razão Social:</td>
										<td style="padding-top:10px; text-align: left;" colspan="3">
											<input class="inputText inputDisab" type="text" style="width:300px; text-transform:none;" value="{RAZAOSOCIAL}" readonly="readonly"  />
										</td>
									</tr>
									<tr>
										<td>Nome Fantasia:</td>
										<td colspan="3">
                                            <input class="inputText inputDisab" type="text" style="width:300px; text-transform:none;" value="{NOMEFANTASIA}" readonly="readonly" />
                                        </td>
									</tr>
									<tr>
										<td>Telefone:</td>
										<td colspan="3">
                                            <input class="inputText inputDisab" type="text" style="width:300px; text-transform:none;" value="{TELEFONEGERAL}" readonly="readonly" />
                                        </td>
									</tr>
									<tr>
										<td>E-mail:</td>
										<td colspan="3">
                                            <input class="inputText inputDisab" type="text" style="width:300px; text-transform:none;" value="{EMAILGERAL}" readonly="readonly" />
                                        </td>
									</tr>
                                    <tr>
										<td>Site:</td>
										<td colspan="3">
                                            <input class="inputText inputDisab" type="text" style="width:300px; text-transform:none;" value="{SITE}" readonly="readonly" />
                                        </td>
									</tr>
                                    <tr>
										<td colspan="4" class="areaCadastro2" style="padding-top:10px;"><img src="<?php echo URL_IMG; ?>listbook.png" style="padding-right:10px;" /> Itens do Pedido</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style=" text-align: center; padding-top:10px;">
                                            <table cellspacing="0" cellpadding="0" class="extensions t100" align="center">
												<thead>
													<tr style="height:20px;">
                                                        <th>ISBN</th>
														<th>Dados do Livro</th>
                                                        <th>Quantidade</th>
                                                        <th>Preço</th>
                                                        <th>Subtotal</th>
													</tr>
													<tr class="header-separator">
														<td colspan="6">&nbsp;</td>
													</tr>
												</thead>
                                                <tbody>
                                                    {livros}
                                                    <tr class="{CORLINHA}" onclick="detalharLivro('{IDLIVRO}');">
                                                        <td class="tdcenter">{ISBN}</td>
                                                        <td class="tdleft w100 pr_20">{DADOSLIVRO}</td>
                                                        <td class="tdcenter">{QTD}</td>
                                                        <td class="tdcenter no-wrap">{PRECO_UNIT}</td>
                                                        <td class="tdcenter no-wrap">{SUBTOTAL}</td>
                                                    </tr>
                                                    {/livros}
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
									<tr>
										<td style="padding-top:10px;" class="bold">Quantidade Total:</td>
										<td style="padding-top:10px; text-align: left;" colspan="3">
											<input class="inputText inputDisab" type="text" style="width:110px; text-transform:none;" value="{QTDTOTAL}" readonly="readonly" />
										</td>
									</tr>
                                    <tr>
										<td style="padding-top:10px;" class="bold">Preço Total:</td>
										<td style="padding-top:10px; text-align: left;" colspan="3">
											<input class="inputText inputDisab" type="text" style="width:110px; text-transform:none;" value="{VALORTOTAL}" readonly="readonly" />
										</td>
									</tr>
									<tr>
										<td colspan="4" class="areaCadastro2" style="padding-top:0px;"><img src="<?php echo URL_IMG; ?>catalog.png" style="padding-top:10px; padding-right:10px;" />Dados do Fechamento de Pedido</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top:10px;">Nota fiscal*:</td>
                                        <td style="padding-top:10px; text-align: left;" colspan="3">
                                        <?php if(file_exists('./application/doc_pdv/' . md5('nf' . $pedido[0]['IDPEDIDO']) . '.jpg')){ ?>
                                            <a target="_blank" href="<?php echo BASE_URL_SISTEMA . 'application/doc_pdv/' . md5('nf' . $pedido[0]['IDPEDIDO']) . '.jpg'; ?>" >
                                                <img src="<?php echo URL_IMG; ?>photo-cd.png" style="padding-right:10px;" />Digitalização
                                            </a>
                                        <?php } else {?>
                                            <i>Arquivo não encontrado.</i>
                                         <?php }?>
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td style="padding-top:10px;">Canhoto de Entrega*:</td>
										<td style="padding-top:10px; text-align: left;" colspan="3">
                                            <?php if(file_exists('./application/doc_pdv/' . md5('ce' . $pedido[0]['IDPEDIDO']) . '.jpg')){ ?>
                                                <a target="_blank" href="<?php echo BASE_URL_SISTEMA . 'application/doc_pdv/' . md5('ce' . $pedido[0]['IDPEDIDO']) . '.jpg'; ?>" >
                                                    <img src="<?php echo URL_IMG; ?>photo-cd.png" style="padding-right:10px;" />Digitalização
                                                </a>
                                            <?php } else {?>
                                                <i>Arquivo não encontrado.</i>
                                            <?php }?>
										</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="padding-top: 10px;">
                                            <table cellspacing="0" align="left" cellpadding="0" class="extensions" style="width: 100%;">
                                                <tbody>
                                                    <tr class="l_i2">
                                                        <td class="tdleft bold"><img src="<?php echo URL_IMG; ?>info.png" style="padding-left:10px; padding-right:10px; float:left;" /> Histórico do Pedido</td>
                                                        <td class="no-wrap tdcenter bold" style="width:100px;">Data/Hora</td>
                                                    </tr>
                                                    {historico}
                                                    <tr>
                                                        <td class="pr_20 tdleft">{DESCSTATUSLOG}</td>
                                                        <td class="no-wrap tdcenter" style="width:100px;">{DATAHORALOG}</td>
                                                    </tr>
                                                    {/historico}
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
									<tr>
										<td colspan="4" style="text-align: center; padding-top:15px;">
											<input class="buttonPadrao" type="button" value="  Voltar  " onclick="history.back();" />
										</td>
                                    </tr>
								</table>
							</div>
						</fieldset>
					</form>
                    {/pedido}
				</div>
			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
</body>
</html>