<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
</head>
<body>
	<div id="wrapper">
		<?php monta_header(1); ?>
		<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
		
		<div class="divtitulo">
			<div class="divtitulo2">
				>> LOG DE ATIVIDADES
			</div>
		</div>
		
		<div id="middle">
			<div id="container" style="text-align:center;">
				<div class="main_content_center" style="margin-top:10px; margin-bottom:20px;">
                    <?php
                        $erros = Array();

                        if (isset($outrosErros))
                        {
                            if (is_array($outrosErros))
                            {
                                for ($i = 0; $i < count($outrosErros); $i++)
                                {
                                    array_push($erros, $outrosErros[$i]);
                                }
                            }
                            else
                            {
                                array_push($erros, $outrosErros);
                            }
                        }

                        exibe_validacao_erros($erros);
                    ?>
                    <center>
						<form name="filtros_lista" id="filtros_lista" method="post" class="form_filtro" action="<?php echo URL_EXEC; ?>adm/logsis" onsubmit="return validaForm_pesqLogsis()" >
							<fieldset style="width:94%">
								<legend>Filtros</legend>
								<div class="filtro_campo">
									<span>Situação:</span>
									<select name="filtro_resultado" id="filtro_resultado" class="select" style="width:70px;">
										<option value="" <?php echo set_select('filtro_resultado',  '', TRUE); ?> ></option>
										<option value="OK" <?php echo set_select('filtro_resultado', 'OK'); ?> >OK</option>
										<option value="ERRO" <?php echo set_select('filtro_resultado', 'ERRO'); ?> >ERRO</option>
									</select>
								</div>
                                <strong style="margin-left: 10px;">De</strong>
                                <div class="filtro_campo" style=" padding: 5px; background-color: #eee;">
                                    
                                    <div class="filtro_campo">
                                        <span>Data:</span>
                                        <input type="text" name="filtro_data_de" id="filtro_data_de" class="inputText" onKeyUp="maskx(this, msk_date)" maxlength="10" style="width:80px;" value="<?php echo set_value('filtro_data_de', date("d/m/Y")); ?>" />
                                    </div>
                                    <div class="filtro_campo">
                                        <span style="margin-left: 5px;">Hora:</span>
                                        <input type="text" name="filtro_hora_de" id="filtro_hora_de" class="inputText" maxlength="5" onKeyUp="maskHora(this)" style="width:40px;" value="<?php echo set_value('filtro_hora_de'); ?>" />
                                    </div>
                                </div>
                                <strong style="margin-left: 10px;">Até</strong>
                                <div class="filtro_campo" style=" padding: 5px; background-color: #eee;">
                                    <div class="filtro_campo">
                                        <span>Data:</span>
                                        <input type="text" name="filtro_data_ate" id="filtro_data_ate" class="inputText" onKeyUp="maskx(this, msk_date)" maxlength="10" style="width:80px;" value="<?php echo set_value('filtro_data_ate', date("d/m/Y")); ?>" />
                                    </div>
                                    <div class="filtro_campo">
                                        <span style="margin-left: 5px;">Hora:</span>
                                        <input type="text" name="filtro_hora_ate" id="filtro_hora_ate" class="inputText" maxlength="5" onKeyUp="maskHora(this)" style="width:40px;" value="<?php echo set_value('filtro_hora_ate'); ?>" />
                                    </div>
                                </div>
								<div class="filtro_campo"><span class="margin"><input type="submit" value="Pesquisar" class="buttonPadrao" /> &nbsp;&nbsp; <input type="reset" value="Limpar" class="buttonPadrao" /></span></div>
							</fieldset>
						</form>
					</center>
                    
					<table cellspacing="0" align="center" cellpadding="0" class="extensions t95">
						<thead>
							<tr class="header">
								<th>Perfil</th>
								<th class="w100">Nome Usuário</th>
								<th>Ação</th>
								<th>Situação</th>
								<th>Data / Hora</th>
								<th>Ativação em</th>
							</tr>
							<tr class="header-separator">
								<td colspan="6">&nbsp;</td>
							</tr>
						</thead>
						<tbody>
							{logs}
							<tr class="{CORLINHA}">
								<td class="no-wrap pr_20 tdleft">{NOMEUSUARIOTIPO}</td>
								<td class="w100 pr_20 tdleft">{NOMEUSUARIO}</td>
								<td class="no-wrap pr_20 tdleft">{DESCRICAO}</td>
								<td class="no-wrap pr_20 tdleft">{RESULTADO}</td>
								<td class="no-wrap pr_20 tdleft">{DATALOG} {HORALOG}</td>
								<td class="no-wrap tdleft">{IPLOG}</td>
							</tr>
							{/logs}
							
							<?php
							if ($vazio == TRUE)
							{
								?>
								<tr>
									<td colspan="6" class="no-wrap tdcenter">Nenhuma atividade encontrada.</td>
								</tr>
								<?php 
							}
							else
							{
								?>
								<tr class="noEfeito">
									<td colspan="6" class="no-wrap tdcenter">
										<div style="display: inline; float: left;">Total Registros: <?php echo $qtdReg; ?></div>
										<?php montaPaginacaoPesquisa($nPaginas, $nPag, $limit_de, $exibir_pp, URL_EXEC . 'adm/logsis/','filtros_lista'); ?>
									</td>
								</tr>
								<?php
							}
							?>
						</tbody>
					</table>
				</div>
			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
</body>
</html>