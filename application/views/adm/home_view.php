<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
    <head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>biblioteca.js" type="text/javascript"></script>
    </head>
<body>
	<?php monta_header(1); ?>
	<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
    <?php add_elementos_CONFIG(); ?>
    <?php add_div_ligthbox('660', '270'); ?>
    <div id="page_content">
		<div id="inside_content">
			<h5 class="comment">Seja bem-vindo(a), <?php echo($nome_usuario);?>.</h5>
			<div class="inline top" style="width:550px;">
				<div class="inline" style="margin:20px 10px 10px 0"><h3>Avisos, comunicados e mensagens <!--(0)--></h3></div>
				<!--<span><a href="javascript:void(0);" class="black">Nova mensagem</a></span>-->
				<?php 
					$notmsg = true;
					// mensagem('info', 'Seleção de ponto de venda', 'Você deve selecionar um ponto de venda para o 4º edital. Caso você queira permanecer utilizando o mesmo, clique <a href="javascript:void(0);">aqui</a>, caso contrário entre na <a href="javascript:void(0);">tela de seleção de ponto de venda</a> e informe o ponto de venda parceiro para este edital.', false, 'width:500px;');
					
					// Caso não tenha msg para exibir
					if($notmsg)
					{
						mensagem('note', '', 'Você não tem nenhum aviso, mensagem ou comunicado a ser exibido.', false, 'width:500px;margin-bottom:20px;');
					}
				?>
			</div>
			<div class="inline top" style="width:370px;margin:15px 0 0 15px;">
				<?php // box_edital(); ?>
				<?php box_minhas_informacoes();	?>
			</div>
		</div>            
	</div>
        
	<!--
	<div id="wrapper"><!-- #wrapper
	<div id="middle"><!-- #middle->
		<div id="container" style="text-align:center;"><!-- #container->
			<div class="main_content_center" style="margin-top:30px;"></div>
		</div><!-- #container->
	</div><!-- #middle->
	</div><!-- #wrapper -->
	<?php //monta_footer(); ?>
</body>
</html>