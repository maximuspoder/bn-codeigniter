<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			// Validação e máscaras
			$("#form_default").validationEngine({ inlineValidation:false , promptPosition : "centerRight", scroll : false });
			$('input:text').setMask();
		});
	</script>
</head>
<body>
	<?php monta_header(1); ?>
	<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
	<?php add_elementos_CONFIG(); ?>
	<div id="page_content">
		<div id="inside_content">
			<div>
				<div class="inline font_shadow_gray"><h1>Administração</h1></div>
				<div class="inline"><h3 class="font_shadow_gray">&nbsp;> Painel Financeiro</h3></div>
			</div>
			<br />
			<?php mensagem('info', '', 'Visualize neste painel de administração financeira, dados de créditos já emitidos, prontos para emissão, créditos bloqueados, pendências e afins.'); ?>
			<br />
			<br />
			<br />
			<br />
			<div style="margin-bottom:90px;">
				<div style="float:left;margin:-22px 0 0 0;">
					<a href="javascript:void(0);" onclick="">Visualizar detalhes</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="javascript:void(0);" onclick="">Saiba mais</a>
				</div>
				<div style="float:right;margin:-30px 0 0 0;"><h4 style="color:green">R$ 20.960.000,00</h4></div>
				<div style="float:right;margin:-30px 170px 0 0;"><h2>Valor do Edital:</h2></div>
				<hr />
			</div>
			<div style="margin-bottom:90px;">
				<div style="float:left;margin:-22px 0 0 0;">
					<a href="javascript:void(0);" onclick="">Visualizar detalhes</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="javascript:void(0);" onclick="">Saiba mais</a>
				</div>
				<div style="float:right;margin:-30px 0 0 0;"><h4 style="color:green">R$ 201.927.224,47</h4></div>
				<div style="float:right;margin:-30px 170px 0 0;"><h4>Total pago até o momento:</h4></div>
				<hr />
			</div>
			<div style="margin-bottom:90px;">
				<div style="float:left;margin:-22px 0 0 0;">
					<a href="javascript:void(0);" onclick="">Visualizar detalhes</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="javascript:void(0);" onclick="">Saiba mais</a>
				</div>
				<div style="float:right;margin:-30px 0 0 0;"><h4 style="color:green">R$ 201.927.224,47</h4></div>
				<div style="float:right;margin:-30px 170px 0 0;"><h4>Valor total em processo de crédito no cartão:</h4></div>
				<hr />
			</div>
			<div style="margin-bottom:90px;">
				<div style="float:left;margin:-22px 0 0 0;">
					<a href="javascript:void(0);" onclick="">Visualizar detalhes</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="javascript:void(0);" onclick="">Saiba mais</a>
				</div>
				<div style="float:right;margin:-30px 0 0 0;"><h4 style="color:green">R$ 201.927.224,47</h4></div>
				<div style="float:right;margin:-30px 170px 0 0;"><h4>Valor total de créditos prontos para serem pagos:</h4></div>
				<hr />
			</div>
			<div style="margin-bottom:90px;">
				<div style="float:left;margin:-22px 0 0 0;">
					<a href="javascript:void(0);" onclick="">Visualizar detalhes</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="javascript:void(0);" onclick="">Saiba mais</a>
				</div>
				<div style="float:right;margin:-30px 0 0 0;"><h4 style="color:red">R$ 201.927.224,47</h4></div>
				<div style="float:right;margin:-30px 170px 0 0;"><h4>Total de créditos bloqueados:</h4></div>
				<hr />
			</div>
		</div>
	</div>
	<iframe id="form_target" name="form_target" src="" style="display:none;"></iframe>
</body>
</html>