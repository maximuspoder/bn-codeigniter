<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.tablesorter.js" type="text/javascript"></script> 
</head>
<body>
	<div id="page_content_wide">
		<div id="inside_content">
			<h1 class="font_shadow_gray inline top" style="margin:10px 15px 0 0;"><?php echo get_value($dados, 'titulo');?></h1>
			<div id="module_menu" class="inline top" style="float:right; margin: 0px -25px 0px 0px">
				<div class="inline">
					<img src="<?php echo URL_IMG;?>icon_excel.gif" style="margin-top:2px;" />
					<a href="javascript:void(0);" onclick="download_excel('<?php echo URL_EXEC?>relatorio/lista_excel_result/<?php echo get_value($dados, '#')?>');" class="black inline" title="Exporte a lista total de registros para o formato CSV"><h5>Exportar para Excel</h5></a>
				</div>
				<div class="inline">
					<img src="<?php echo URL_IMG;?>icon_print.png" style="margin-top:2px;" />
					<a href="javascript:void();" class="black inline" title="Imprimir" onClick="window.print()"><h5>Imprimir</h5></a>
				</div>
			</div>
			<br />
			<h5 class="font_shadow_gray inline top" style="margin:20px 15px 0 0;"><?php echo get_value($dados, 'DESCRICAO');?></h5>
			<?php echo($module_table); ?>
		</div>
	</div>
</body>
</html>