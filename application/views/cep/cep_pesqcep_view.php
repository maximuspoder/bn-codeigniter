<table cellspacing="0" cellpadding="0" id="tabelaListagem1" align="center">
	<thead>
		<tr>
			<th>CEP</th>
			<th>Cidade</th>
			<th>Bairro</th>
			<th>Logradouro</th>
			<th alt="Complemento" title="Complemento">Compl.</th>
			<th style="width:40px;" alt="Selecionar" title="Selecionar">Sel.</th>
		</tr>
	</thead>
	<tbody class="<?php echo $class_tbody; ?>">
		{logradouros}
		<tr class="{CORLINHA}">
			<td class="no-wrap">{CEP}</td>
			<td>{NOMECIDADESUB}</td>
			<td>{NOMEBAIRRO}</td>
			<td>{NOMELOGRADOUROTIPO} {NOMELOGRADOURO}</td>
			<td>{COMPLEMENTO}</td>
			<td>
				<input type="hidden" name="cep_{IDLOGRADOURO}" id="cep_{IDLOGRADOURO}" value="{CEP}" />
				<input type="hidden" name="uf_{IDLOGRADOURO}" id="uf_{IDLOGRADOURO}" value="{IDUF}" />
				<input type="hidden" name="cidade_{IDLOGRADOURO}" id="cidade_{IDLOGRADOURO}" value="{NOMECIDADESUB}" />
				<input type="hidden" name="bairro_{IDLOGRADOURO}" id="bairro_{IDLOGRADOURO}" value="{NOMEBAIRRO}" />
				<input type="hidden" name="logradouro_{IDLOGRADOURO}" id="logradouro_{IDLOGRADOURO}" value="{NOMELOGRADOUROTIPO} {NOMELOGRADOURO}" />
				<input type="hidden" name="complemento_{IDLOGRADOURO}" id="complemento_{IDLOGRADOURO}" value="{COMPLEMENTO}" />
				<img src="<?php echo URL_IMG; ?>v_peq.png" style="cursor:pointer;" onclick="selecionaCep('{IDLOGRADOURO}','cep#idlogradouro#uf#cidade#bairro#logradouro#logcomplemento#end_numero');" alt="Selecionar" title="Selecionar" />
			</td>
		</tr>
		{/logradouros}
	</tbody>
</table>
