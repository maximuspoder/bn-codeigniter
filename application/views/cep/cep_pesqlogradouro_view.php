<table cellspacing="0" cellpadding="0" id="tabelaListagem1" align="center">
	<thead>
		<tr>
			<th style="text-align:center; padding-bottom:10px;" colspan="5">
				<img src="<?php echo URL_IMG; ?>pesq1.gif">
				RESULTADO DA BUSCA PELO CEP: <span style="font-size:14px;"><?php echo $cep; ?></span>
				<br />
				<span style="font-style:italic; font-weight:normal;"><?php echo $cont; ?></span>
			</th>
			<th style="text-align:center; padding-bottom:10px;">
				<img alt="Fechar Resultado" title="Fechar Resultado" src="<?php echo URL_IMG; ?>close1.png" style="cursor:pointer;" onclick="fechaPesquisaCep();" />
			</th>
		</tr>
		<tr>
			<th>UF</th>
			<th>Cidade</th>
			<th>Bairro</th>
			<th>Logradouro</th>
			<th alt="Complemento" title="Complemento">Compl.</th>
			<th alt="Selecionar" title="Selecionar" style="width:40px;">Sel.</th>
		</tr>
	</thead>
	<tbody class="<?php echo $class_tbody; ?>">
		{logradouros}
		<tr class="{CORLINHA}">
			<td class="no-wrap">{IDUF}</td>
			<td class="no-wrap">{NOMECIDADESUB}</td>
			<td class="no-wrap">{NOMEBAIRRO}</td>
			<td class="no-wrap">{NOMELOGRADOUROTIPO} {NOMELOGRADOURO}</td>
			<td class="no-wrap">{COMPLEMENTO}</td>
			<td>
				<input type="hidden" name="cep_{IDLOGRADOURO}" id="cep_{IDLOGRADOURO}" value="{CEP}" />
				<input type="hidden" name="uf_{IDLOGRADOURO}" id="uf_{IDLOGRADOURO}" value="{IDUF}" />
				<input type="hidden" name="cidade_{IDLOGRADOURO}" id="cidade_{IDLOGRADOURO}" value="{NOMECIDADESUB}" />
				<input type="hidden" name="bairro_{IDLOGRADOURO}" id="bairro_{IDLOGRADOURO}" value="{NOMEBAIRRO}" />
				<input type="hidden" name="logradouro_{IDLOGRADOURO}" id="logradouro_{IDLOGRADOURO}" value="{NOMELOGRADOUROTIPO} {NOMELOGRADOURO}" />
				<input type="hidden" name="complemento_{IDLOGRADOURO}" id="complemento_{IDLOGRADOURO}" value="{COMPLEMENTO}" />
				<img src="<?php echo URL_IMG; ?>v_peq.png" style="cursor:pointer;" onclick="selecionaCep('{IDLOGRADOURO}','<?php echo $elemCompletar; ?>');" alt="Selecionar" title="Selecionar" />
			</td>
		</tr>
		{/logradouros}
	</tbody>
</table>
