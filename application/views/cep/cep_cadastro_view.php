<table cellspacing="0" cellpadding="0" align="center">
	<thead>
		<tr>
			<th style="text-align:center;">
				<img src="<?php echo URL_IMG; ?>pesq1.gif">
				RESULTADO DA BUSCA PELO CEP: <span style="font-size:14px;"><?php echo $dadosCad['CEP']; ?></span>
				<br />
				<span style="font-style:italic; font-weight:normal;"><?php echo $dadosCad['OBS']; ?></span>
			</th>
			<th style="text-align:center; width: 30px;">
				<img alt="Fechar Resultado" title="Fechar Resultado" src="<?php echo URL_IMG; ?>close1.png" style="cursor:pointer;" onclick="fechaPesquisaCep();" />
			</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td colspan="2">
				<table cellspacing="0" cellpadding="0" id="tabelaCadastros" align="center">
					<tr>
						<td>CEP*:</td>
						<td colspan="3">
							<input class="inputText inputDisab" type="text" readonly="readonly" name="cep_cad" id="cep_cad" size="11" maxlength="8" value="<?php echo $dadosCad['CEP']; ?>" autocomplete="off" onkeypress="return maskInteger(this, event)" />
						</td>
					</tr>
					<tr>
						<td>UF*:</td>
						<td colspan="3">
							<?php
							if ($dadosCad['IDCIDADE'] != '')
							{
								?>
								<input class="inputText inputDisab" type="text" readonly="readonly" name="uf_cad" id="uf_cad" size="5" maxlength="2" value="<?php echo $dadosCad['IDUF']; ?>" />
								&nbsp;&nbsp;&nbsp;
								Cidade*:
								<input type="hidden" name="cidade_cad" id="cidade_cad" value="<?php echo $dadosCad['IDCIDADE']; ?>" />
								<input class="inputText inputDisab" type="text" readonly="readonly" name="cidade_cad2" id="cidade_cad2" style="width: 280px;" value="<?php echo $dadosCad['NOMECIDADESUB']; ?>" />
								<?php
							}
							else
							{
								?>
								<select class="select" name="uf_cad" id="uf_cad" onchange="carregaCidades('uf_cad','cidade_cad','bairro_cad')">
									<option value="">---</value>
									{combouf}
									<option value="{IDUF}">{IDUF}</value>
									{/combouf}
								</select>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								Cidade*:
								<select class="select" name="cidade_cad" id="cidade_cad" onchange="carregaBairros('cidade_cad','bairro_cad',1)" style="width: 250px;">
								</select> <img id="img_cidade_cad" src="<?php echo URL_IMG; ?>loading2.gif" style="visibility:hidden;" alt="Carregando..." title="Carregando..." />
								<?php
							}
							?>
						</td>
					</tr>
					<tr>
						<td>Bairro*:</td>
						<td colspan="3">
							<?php
							if ($dadosCad['IDBAIRRO'] != '')
							{
								?>
								<input type="hidden" name="bairro_cad" id="bairro_cad" value="<?php echo $dadosCad['IDBAIRRO']; ?>" />
								<input class="inputText inputDisab" type="text" readonly="readonly" name="bairro_cad2" id="bairro_cad2" style="width: 280px;" value="<?php echo $dadosCad['NOMEBAIRRO']; ?>" />
								<?php
							}
							else
							{
								?>
								<select class="select" name="bairro_cad" id="bairro_cad" onchange="selecionaBairro('bairro_cad')" style="width: 180px;">
								</select> <img id="img_bairro_cad" src="<?php echo URL_IMG; ?>loading2.gif" style="visibility:hidden;" alt="Carregando..." title="Carregando..." />
								<?php
							}
							?>
							<span id="spanNovoBairro" style="display:none;">
								Nome*:
								<input class="inputText" type="text" name="novoBairro" id="novoBairro" size="25" maxlength="70" onfocus="focusLiga(this)" onblur="focusDesliga(this)" autocomplete="off" />
							</span>
						</td>
					</tr>
					<tr>
						<td>Tipo Lograd.*:</td>
						<td colspan="3">
							<select class="select" name="tipologra_cad" id="tipologra_cad" style="width: 100px;">
								<option value="0">---</value>
								{combotipologra}
								<option value="{IDLOGRADOUROTIPO}">{NOMELOGRADOUROTIPO}</value>
								{/combotipologra}
							</select>
							&nbsp;
							Logradouro*:
							<input class="inputText" type="text" name="logradouro_cad" id="logradouro_cad" size="33" maxlength="60" onfocus="focusLiga(this)" onblur="focusDesliga(this)" autocomplete="off" />
						</td>
					</tr>
					<tr>
						<td colspan="4" style="text-align:center; padding-top:10px;">
							<button class="buttonPadrao" onclick="saveCep()">Salvar</button>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</tbody>
</table>
<script type="text/javascript">
completaCadastroCEP('<?php echo $dadosCad['IDCIDADE']; ?>','<?php echo $dadosCad['IDBAIRRO']; ?>');
</script>