<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			// Validação e máscaras
			$("#form_default").validationEngine({ inlineValidation:false , promptPosition : "centerRight", scroll : false });
			$("input:text").setMask();
		});
	</script>
</head>
<body>
	<?php monta_header(1); ?>
	<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
	<?php add_elementos_CONFIG(); ?>
	<div id="page_content">
		<div id="inside_content">
			<div>
				<div class="inline"><h1><a href="<?php echo URL_EXEC;?>comite_acervo/search" class="black font_shadow_gray">Comitê de Acervo</a></h1></div>
				<div class="inline"><h3 class="font_shadow_gray">&nbsp;> Edição de Cadastro</h3></div>
			</div>
			<br />
			<?php mensagem('note', '', 'Utilize o formulário abaixo para editar os dados do cadastro que está sendo visualizado. Campos com (*) são obrigatórios.'); ?>
			<br />
			<br />
			
			<form name="form_default" id="form_default" method="post" action="<?php echo URL_EXEC; ?>comite_acervo/form_update_comite_acervo_proccess/">
				<input type="hidden" name="idusuario" id="idusuario" value="<?php echo get_value($dados, 'IDUSUARIO'); ?>" />
				<input type="hidden" name="idlogradouro" id="idlogradouro" value="<?php echo get_value($dados, 'IDLOGRADOURO'); ?>" />
				<h4 class="font_shadow_gray">Dados do Cadastro</h4>
				<hr />
				<div style="float:right;margin:7px 10px 0 0;"><img src="" /></div>
				<div class="form_label">Membro da Biblioteca:</div>
				<div class="form_field" style="padding-top:6px;"><?php echo get_value($dados, 'BIBLIOTECA');?></div>
		
				<div style="float:right;margin:7px 10px 0 0;"><img src=""  /></div>
				<br />
				<div class="form_label">ID Usuario (#):</div>
				<div class="form_field" style="padding-top:6px;"><?php echo get_value($dados, 'IDUSUARIO'); ?></div>
				<br />
				<div class="form_label">*Razão Social:</div>
				<div class="form_field">
					<input type="text" name="razaosocial" id="razaosocial" value="<?php echo get_value($dados, 'NOME');?>" class="validate[required]" style="width:350px;" /><br />
					<span class="comment">Este campo é exibido como o nome do membro do comitê em outras partes do sistema</span>
				</div>
				<br />
				<br />	
				<div class="form_label">*Nome Responsável:</div>
				<div class="form_field"><input type="text" name="nomeresp" id="nomeresp" value="<?php echo get_value($dados, 'NOME RESPONSAVEL');?>" class="validate[required]" style="width:350px;" /></div>
				<br />
				<div class="form_label">*Nome Mãe:</div>
				<div class="form_field"><input type="text" name="nomemae" id="nomemae" value="<?php echo get_value($dados, 'NOME DA MAE');?>" class="validate[required]" style="width:350px;" /></div>
				<br />
				<div class="form_label">*CPF/CNPJ:</div>
				<div class="form_field"><input type="text" name="cpf_cnpj" id="cpf_cnpj" value="<?php echo get_value($dados, 'CPF_CNPJ');?>" class="validate[required]" alt="cpf" style="width:150px;" /></div>
				<br />
				<div class="form_label">*Data Nascimento:</div>
				<div class="form_field">
					<input type="text" name="datanasc" id="datanasc" value="<?php echo format_date(get_value($dados, 'DATA DE NASCIMENTO'));?>" class="validate[required,custom[date-pt_BR]]" alt="date-pt_BR" style="width:100px;" />
					<span class="comment">&nbsp;&nbsp;Formato DD/MM/AAAA</span>
				</div>
				<br />
				<div class="form_label">*Telefone:</div>
				<div class="form_field">
					<input type="text" name="telefoneresp" id="telefoneresp" value="<?php echo get_value($dados, 'TELEFONE');?>" class="validate[required]" alt="phone" style="width:100px;" />
					<span class="comment">&nbsp;&nbsp;Formato (XX) 9999.9999</span>
				</div>
				<br />
				<div class="form_label">*Email:</div>
				<div class="form_field"><input type="text" name="emailresp" id="emailresp" value="<?php echo get_value($dados, 'EMAIL');?>" class="validate[required,custom[email]]" style="width:250px;" /></div>
				
				<h4 class="font_shadow_gray" style="margin-top:35px;">Dados do Endereço</h4>
				<hr />
				<br>
				<div class="form_label">Endereço:</div>
				<div class="form_field" style="padding-top:6px;">
					<div class="inline top" id="label_endereco"><?php echo "RUA " . get_value($dados, 'NOMELOGRADOURO') .  " - " . get_value($dados, 'NOMECIDADE') . ", " . get_value($dados, 'NOMEBAIRRO') . ", " . get_value($dados, 'CEP_LOGRADOURO') . " - " .  get_value($dados, 'NOMEUF');?></div>
					<div class="inline top">&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onClick="iframe_modal('Busca de Endereços','<?php echo URL_EXEC; ?>endereco/search/{0}/','<?php echo URL_IMG . 'icon_view.png'; ?>', 870, 910)">Alterar</a></div>
					<span class="fnt_error italic font_14" id="aviso_alteracao_endereco"></span>
				</div>
				<br />
				<div class="form_label">Complemento:</div>
				<div class="form_field"><input type="text" name="end_complemento" id="end_complemento" value="<?php echo get_value($dados, 'END_COMPLEMENTO');?>" style="width:350px;" /></div>
				<br />				
				<div class="form_label">Número:</div>
				<div class="form_field"><input type="text" name="end_numero" id="end_numero" value="<?php echo get_value($dados, 'END_NUMERO');?>" style="width:150px;" /></div>
				<br />
				
				<h4 class="font_shadow_gray" style="margin-top:35px;">Dados do Usuário</h4>
				<hr />
				<br />
				<div class="form_label">Login:</div>
				<div class="form_field"><input type="text" disabled="disabled" name="login" id="login" value="<?php echo get_value($dados, 'LOGIN');?>" style="width:150px;" /></div>
				<br />
				<div class="form_label">*Tipo Pessoa:</div>
				<div class="form_field">
					<select name="tipopessoa" id="tipopessoa" style="width:162px;">
						<option value="PF" <?php echo (get_value($dados, 'tipopessoa') == 'PF') ? 'selected="selected"' : '';?>>PESSOA FÍSICA</option>
						<option value="PJ" <?php echo (get_value($dados, 'tipopessoa') == 'PJ') ? 'selected="selected"' : '';?>>PESSOA JURÍDICA</option>
					</select>
				</div>
				<br />
				<div class="form_label">*Cadastro Ativo:</div>
				<div class="form_field">
					<select name="ativo" id="ativo" style="width:162px;">
						<option value="S" <?php echo (get_value($dados, 'ativo_img') == 'S') ? 'selected="selected"' : '';?>>ATIVADO (SIM)</option>
						<option value="N" <?php echo (get_value($dados, 'ativo_img') == 'N') ? 'selected="selected"' : '';?>>DESATIVADO (NÃO)</option>
					</select>
				</div>
				<div style="margin-top:35px">
					<hr />
					<div class="inline top"><button onclick="set_form_redirect('<?php echo $url_redirect; ?>', '', 'form_default', 'ok');">OK</button></div>&nbsp;&nbsp;&nbsp;&nbsp;
					<div class="inline top"><button onclick="set_form_redirect('<?php echo $url_redirect; ?>', 'comite_acervo/form_update_comite_acervo/<?php echo get_value($dados, 'IDUSUARIO')?>', 'form_default', 'aplicar');">Aplicar</button></div>
					<div class="inline top" style="padding:8px 0 0 5px">ou&nbsp;&nbsp;&nbsp;<a href="<?php echo URL_EXEC; ?><?php echo $url_redirect; ?>">voltar</a></div> 
				</div>
			</form>			
			
		
		</div>
	</div>
</body>
</html>