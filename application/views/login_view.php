<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function() {
			// Verifica compatibilidade de browsers
			if(check_browser_compatibilty())
			{
				// Abre janela de boas vindas, conteudo, etc.
				ajax_modal('Portal do Livro FBN - Comunicado', $('#URL_EXEC').val() + 'comunicacao/comunicado_portal', $('#URL_IMG').val() + 'icon_info.png', 550, 700);
				
				// Seta focus no campo de usuario (login)
				$("#userLogin").focus();
			}
		});
	</script>
</head>
<body>
	<?php monta_header(0); ?>
	<?php add_elementos_CONFIG(); ?>
	<div id="page_content">
		<div id="inside_content">
			<?php // mensagem('info', 'Informações Adicionais', 'Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen Lorem ipsum lorem sit amen ')?>
			<div class="inline top" style="width:570px;">
				<h1 class="font_shadow_gray">Portal do Livro FBN</h1>
				<hr />
				<div class="inline top" style="width:550px;">
					<div style="margin:15px 0;"><h4>Bem vindo ao <strong>Portal do Livro</strong> da Fundação Biblioteca Nacional!</h4></div>
					<h6>O que é o Portal do Livro FBN?</h6>
					<div>Aqui você terá acesso aos dados da sua Biblioteca, Livros de baixo preço, Ponto de Venda, e Distribuidora cadastrados para acesso aos programas governamentais de apoio às bibliotecas. A aquisição dos livros será efetuada por meio de ações de ampliação e atualização dos acervos promovidas pela Fundação Biblioteca Nacional, no âmbito do Sistema Nacional de Bibliotecas Públicas - SNBP.</div><br />
					<div>É necessário que sua entidade faça o cadastro junto ao Portal do Livro FBN. Apenas as bibliotecas que estiverem com os dados atualizados no <strong>Cadastro Nacional de Bibliotecas</strong> estarão em condições de participar dos programas de modernização e atualização de acervos, coordenada pelo Sistema Nacional de Bibliotecas Públicas - SNBP.</div><br />
					<div>Acesse <a href="http://www.bn.br/portal/arquivos/pdf/Edital-Cadastro-Nacional-de-Bibliotecas.pdf" target="_blank">aqui</a> o edital de convocação para inscrição no Cadastro.</div><br />
				</div>
				<div class="inline top" style="width:580px;">
					<h6>Ainda não possui cadastro?</h6>
					<div style="margin-bottom:10px;">Cadastre-se e fique atento para participar dos próximos editais!</div>
					<div id="box_cadastro_login">
						<div class="inline top white" id="left_area" style="height:113px;">
							<img src="<?php echo(URL_IMG);?>icon_biblioteca_big.png" />
							<div style="margin-left:45px;"><h6>Cadastro Nacional de Bibliotecas</h6></div>
						</div>
						<div class="inline top font_11" id="right_area">
							<div>O objetivo deste cadastro é mapear de maneira abrangente todas as bibliotecas existentes no país, levantando dados sobre a relação institucional, público, acervo, serviços, infraestrutura e gestão. Devem cadastrar-se bibliotecas: públicas estaduais e municipais, comunitárias urbanas ou rurais, pontos de leitura, escolares e universitárias.</div>
							<div class="font_12" style="margin-top:5px;float:right"><a href="javascript:void(0);" style="z-index:9999;" onclick="open_modal('Saiba Mais - Cadastro Nacional de Bibliotecas', '<strong>Cadastro Nacional de Bibliotecas:</strong> Para inserir seus dados as bibliotecas públicas, escolares, universitárias e especializadas devem ser cadastradas pelos órgãos públicos ou privados aos quais estão vinculadas, <strong>a partir do CNPJ da instituição</strong>. <br /><br />Já as bibliotecas comunitárias e os pontos de leitura poderão ser cadastrados a partir do CNPJ da instituição mantenedora, ou pelo CPF do responsável pela biblioteca / ponto de leitura.', '<?php echo URL_IMG; ?>icon_help.png', 300, 500);">Saiba mais</a>&nbsp;&nbsp;&nbsp;<a href="<?php echo URL_EXEC;?>autocad/preautocad/cnb">Cadastrar</a></div>
						</div>
					</div>
					<div id="box_cadastro_login">
						<div class="inline top white" id="left_area" style="height:150px;">
							<img src="<?php echo(URL_IMG);?>icon_livro_baixo_preco_big.png" />
							<div style="margin-left:45px;"><h6>Cadastro Nacional de Livros a Baixo Preço</h6></div>
						</div>
						<div class="inline top font_11" id="right_area">
							<div><a href="<?php echo URL_EXEC;?>autocad/preautocad">Clique aqui</a> para cadastrar-se, caso você seja um <strong>editor(a) (pessoa física ou jurídica)</strong> ou <strong>autor idependente</strong> interessado em inscrever seus livros nos programas governamentais de fomento à produção e comercialização de livros a baixo preço da Fundação da Biblioteca Nacional.<br><strong>Atenção: </strong> É indispensável que o CNPJ ou CPF a ser cadastrado tenha pelo menos, um título cadastrado no <a href="http://www.isbn.bn.br" target="_blank">ISBN</a>.</div>
							<div class="font_12" style="margin-top:5px;float:right"><a href="<?php echo URL_EXEC;?>autocad/preautocad">Cadastrar</a></div>
						</div>
					</div>
					<div id="box_cadastro_login">
						<div class="inline top white" id="left_area">
							<img src="<?php echo(URL_IMG);?>icon_pdv_big.png" />
							<div style="margin-left:45px;"><h6>Cadastro Nacional de Pontos de Venda</h6></div>
						</div>
						<div class="inline top font_11" id="right_area" style="height:78px;">
							<div>Acesse e cadastre-se neste portal caso você seja uma empresa interessada em comercializar os livros de baixo preço.</div>
							<div class="font_12" style="margin-top:5px;float:right"><a href="<?php echo URL_EXEC;?>autocad/preautocad">Cadastrar</a></div>
						</div>
					</div>
					<div id="box_cadastro_login">
						<div class="inline top white" id="left_area" style="height:70px;">
							<img src="<?php echo(URL_IMG);?>icon_distribuidor_big.png" />
							<div style="margin-left:45px;"><h6>Cadastro Nacional de Distribuidores</h6></div>
						</div>
						<div class="inline top font_11" id="right_area">
							<div>Acesse e cadastre-se neste portal caso você seja uma empresa interessada em distribuir os livros de baixo preço.</div>
							<div class="font_12" style="margin-top:5px;float:right"><a href="<?php echo URL_EXEC;?>autocad/preautocad">Cadastrar</a></div>
						</div>
					</div>
				</div>
				<!--
				Aqui você poderá cadastrar sua biblioteca de acesso público (estadual, municipal, comunitária urbana ou rural e os pontos de leitura) para fazer parte do <a class="red" href="<?php echo URL_EXEC; ?>login/exec/2">Cadastro Nacional de Bibliotecas</a>, e assim participar dos programas governamentais de apoio às bibliotecas.
				<br/> <br/> 
				Este portal também é voltado para as editoras e autores independentes que queiram inscrever seus livros ao valor máximo de R$ 10,00 no <a class="red" href="<?php echo URL_EXEC; ?>login/exec/1">Cadastro Nacional de Livros de Baixo Preço</a> de forma a participar dos programas governamentais de aquisição de acervo da Fundação Biblioteca Nacional.
				<br/> <br/> 
				Editoras, livrarias e todas as demais instituições e empresas interessadas em participar do programa, seja comercializando ou mesmo distribuindo os livros de baixo preço, devem se inscrever no <a class="red" href="<?php echo URL_EXEC; ?>login/exec/3">Cadastro Nacional de Pontos de Venda </a> ou no <a class="red" href="<?php echo URL_EXEC; ?>login/exec/4">Cadastro Nacional de Distribuidoras.</a>
				<br/> <br/> 
				Para saber mais sobre o Programa de Ampliação e Atualização dos Acervos das Bibliotecas de Acesso Público clique <a class="red" href="http://www.bn.br/portal/arquivos/pdf/3%20EDITAL%20DE%20CHAMADA%20PUBLICA%20PLP.pdf">aqui.</a>
				-->
			</div>
			<div class="inline top" style="width:350px;margin:0px 0 0 10px;">
				<div style="width:300px;float:right">
					<?php start_box('Área de Login', 'icon_login.png'); ?>
						<form name="formLogin" id="formLogin" method="post" action="<?php echo URL_EXEC; ?>login/exec">
							<?php if($msg_login != '') { mensagem('error', '', $msg_login, true, 'margin-bottom:20px;width:272px;'); } ?>
							<div class="inline" style="padding-top:2px;width:70px;height:35px;">Usuário</div>
							<div class="inline"><input type="text" name="userLogin" id="userLogin" maxlength="20" style="width:188px;" /></div>
							<div class="inline top" style="padding-top:7px;width:70px;">Senha</div>
							<div class="inline"><input type="password" name="userSenha" id="userSenha" maxlength="32" style="width:188px;" /><br /></div>
							<div class="inline" style="padding-top:7px;width:70px;"></div>
							<div class="inline" style="margin:10px 0 0 0;text-align:right;width:200px;"><input type="submit" value="Entrar" /></div>
							<div style="height:10px;width:275px;border-bottom:1px dotted #aaa;margin:0 0 15px 0;"></div>
							<div class="inline">
								<?php mensagem('info', '', '<h6 style="margin:-1px 0 0 0;">Não está conseguindo acesso?</h6>
															&bull;&nbsp;<a href="'. URL_EXEC . 'acesso_externo/email_atualizacao_senha">Esqueceu sua senha?</a><br />
															&bull;&nbsp;<a href="'. URL_EXEC . 'acesso_externo/email_reativacao_cadastro">Reenviar o e-mail de validação</a>', false, 'width:272px;');?>
							</div>
						</form>
					<?php end_box('Login'); ?>
					
					<div style="margin-top:20px;background-color:#E6E6FA;padding:10px 20px 20px 20px;border:1px solid #B0C4DE;">
						<img src="<?php echo URL_IMG?>icon_biblioteca.png" style="float:left;margin:5px 7px 0 0" />
						<h4>Pesquisa de Bibliotecas</h4>
						<div style="margin-top:10px;">O Portal do Livro Popular agora possui uma nova pesquisa de cadastros de bibliotecas externa, onde você poderá encontrar bibliotecas cadastradas no programa. <a href="<?php echo URL_EXEC?>acesso_externo/pesquisar_bibliotecas">Clique aqui</a> e confira!</div>
					</div>
					
					<div style="margin-top:20px;background-color:#E6E6FA;padding:10px 20px 20px 20px;border:1px solid #B0C4DE;">
						<img src="<?php echo URL_IMG?>icon_pesquisa_livros.png" style="float:left;margin:5px 7px 0 0" />
						<h4>Nova pesquisa de Livros!</h4>
						<div style="margin-top:10px;">O Portal do Livro Popular agora possui uma nova pesquisa de livros externa, onde você poderá encontrar os títulos cadastrados no programa. <a href="<?php echo URL_EXEC?>acesso_externo/pesquisar_livros">Clique aqui</a> e confira!</div>
					</div>
					
					<div style="margin-top:20px;">
						<h5>Editais em andamento</h5>
						<hr />
						<?php foreach($programas as $programa){?>
							<div id="box_edital_login" class="with_shadow">
								<img src="<?php echo(URL_IMG . get_value($programa, 'ICONE_EDITAL'))?>" />
								<div class="bold"><?php echo(get_value($programa, 'NOME_EDITAL'));?></div>
								<div style="margin-top:5px;"><a href="<?php echo URL_EXEC ?>application/files/<?php echo(get_value($programa, 'ARQUIVO_EDITAL'));?>" target="_blank">Arquivo PDF do Edital</a></div>
								<div style="margin-top:5px;"><a href="javascript:void(0);" onclick="open_modal('Texto do Edital', '<strong><?php echo(get_value($programa, 'NOME_EDITAL'));?></strong><br /><?php echo(get_value($programa, 'TEXTO_EDITAL'));?>', '<?php echo(URL_IMG . get_value($programa, 'ICONE_EDITAL'));?>', 300, 400);">Descrição do Edital</a></div>
								<div style="margin-top:5px;"><a href="javascript:void(0);" onclick="iframe_modal('Lista de Contemplados', '<?php echo(URL_EXEC);?>programa/modal_lista_contemplados/<?php echo(get_value($programa, 'IDPROGRAMA'));?>', '<?php echo(URL_IMG . get_value($programa, 'ICONE_EDITAL'));?>', 600, 800);">Veja a lista de contemplados</a></div>
							</div>
						<?php } ?>
					</div>
					
					<hr/>
					<div style="margin-top:20px;">
						<div style="background-color:#ae332d;border:1px solid #7c110c" id="box_question_login" class="with_shadow">
							<img src="<?php echo(URL_IMG);?>icon_question.png" style="margin-top:2px" />
							<h5 class="white" style="margin:0;">Dúvidas? Fale conosco!</h5>
							<div style="margin:5px 0 0 22px;" class="white">
								Entre em contato atrávés dos números:<br />
								&bull;&nbsp;(51) 3254-3241<br />
								&bull;&nbsp;(51) 3254-3242<br />
								&bull;&nbsp;(51) 3254-3243<br />
								&bull;&nbsp;(51) 3254-3244<br />
								&bull;&nbsp;(51) 3254-3245<br />
								Horário de atendimento:<br />
								09h às 18h<br />
								2ª à 6ª feira
								<br />
								Ou envie um email para:<br />
								<a style="color:white" href="mailto:editais2011@bn.br?subject=Dúvida Portal do Livro FBN">editais2011@bn.br</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>