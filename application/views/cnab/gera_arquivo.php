<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			// Validação e máscaras
			$("#form_default").validationEngine({ inlineValidation:false , promptPosition : "centerRight", scroll : false });
			$('input:text').setMask();
		});
	</script>
</head>
<body>
	<?php monta_header(1); ?>
	<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
	<?php add_elementos_CONFIG(); ?>
	<div id="page_content">
		<div id="inside_content">
			<div>
				<div class="inline"><h1><a href="<?php echo URL_EXEC;?>cnab/gerenciadorcpb" class="black font_shadow_gray">Gerenciador CPB</a></h1></div>
				<div class="inline"><h3 class="font_shadow_gray">&nbsp;> Gerador de Arquivos de Remessa</h3></div>
			</div>
			<br />
			<?php 
				mensagem('info', 'Informações sobre a geração de arquivos', '
				Utilize esta página para realizar emissões de arquivos de remessa. Abaixo, informações sobre tipos de arquivo:<br /><br />
				<strong><u>Cadastro:</u></strong> Selecione esta opção para geração de um novo arquivo de cadastro.<br />
				<strong><u>Crédito:</u></strong> Selecione esta opção para geração de um novo arquivo de crédito.<br />
				<strong><u>Bloqueio:</u></strong> Selecione esta opção para geração de um novo arquivo bloqueio de cartão.<br />
				<strong><u>Desbloqueio:</u></strong> Selecione esta opção para geração de um novo arquivo desbloqueio de um cartão.<br /><br />
				Campos com (*) são obrigatórios.'); 
			?>
			<br />
			<br />
			<form name="form_default" id="form_default" method="post" action="<?php echo URL_EXEC; ?>cnab/geraarquivo_ok/" target="form_target">
				<div class="form_label">*Edital:</div>
				<div class="form_field">
					<select name="idprograma" id="idprograma" style="width:200px;" class="validate[required]">
						<option value=""></option>
						<option value="1">3° EDITAL</option>
						<option value="2">4° EDITAL</option>
					</select>
				</div>
				<br />
				<div class="form_label">*Tipo de Arquivo:</div>
				<div class="form_field">
					<select name="tipoarquivo" id="tipoarquivo" style="width:200px;" class="validate[required]">
						<option value=""></option>
						{id}
						<option value="{ACAOREMESSA}">{DESCARQUIVOTIPO}</option>
						{/id}
					</select>
				</div>
				<br />
				<div class="form_label">Limite de Registros:</div>
				<div class="form_field"><input type="text" name="limite" id="limite" style="width:55px;" alt="integer" dir="rtl" /></div>
				<div style="margin-top:15px">
					<hr />
					<div class="inline top"><input type="submit" value="Enviar" onclick="/*enable_loading();*/" /></div>
					<div class="inline top" style="padding:8px 0 0 5px">ou&nbsp;&nbsp;&nbsp;<a href="<?php echo URL_EXEC;?>cnab/gerenciadorcpb">voltar</a></div> 
				</div>
			</form>
		</div>
	</div>
	<iframe id="form_target" name="form_target" src="" style="display:none;"></iframe>
</body>
</html>