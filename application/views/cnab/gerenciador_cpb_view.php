<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.tablesorter.js" type="text/javascript"></script> 
</head>
<body>
	<?php monta_header(1); ?>
	<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
	<?php add_elementos_CONFIG(); ?>
	<div id="page_content_wide">
		<div id="inside_content">
			<h1 class="font_shadow_gray inline top" style="margin:10px 15px 0 0;">Gerenciador CPB</h1>
			<div id="module_menu" class="inline top">
				<div class="inline">
					<img src="<?php echo URL_IMG;?>icon_arquivo_retorno.png" />
					<a href="<?php echo URL_EXEC; ?>cnab/recebe_arquivo" class="black inline"><h5>Importar arquivo de Retorno</h5></a>
				</div>
				<div class="inline">
					<img src="<?php echo URL_IMG;?>icon_arquivo_remessa.png" style="margin-top:2px;" />
					<a href="<?php echo URL_EXEC; ?>cnab/geraarquivo" class="black inline"><h5>Gerar arquivo de Remessa</h5></a>
				</div>
			</div>
			<?php echo($module_table); ?>
		</div>
	</div>
</body>
</html>