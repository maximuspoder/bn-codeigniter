<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<!--<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>-->
	<!--<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script> -->
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript" language="javascript"></script>
	<script src="<?php echo URL_JS; ?>cnab.js" type="text/javascript"></script>
</head>
<body>
	<div id="wrapper">
		<?php monta_header(1); ?>
		<?php monta_menu($this->session->userdata('tipoUsuario')); ?>	
		<?php add_elementos_CONFIG(); ?>
		<div class="divtitulo">
			<div class="divtitulo2">
                >> GERADOR DE CARTÃO
			</div>
		</div>
		
		<div id="middle">
			<div id="container">                
                <form name="geraArquivo" id="geraArquivo" method="post" class="form_filtro" action="<?php echo URL_EXEC; ?>cnab/geraArquivo_Ok/" >
                    <div class="main_content_left" style="margin-top:10px; margin-bottom:20px;">
                    <table cellspacing="0" cellpadding="4" border="0" align="center">
                    	<tr>
                    		<td align="left"><b>Escolha para qual edital sera gerado o cartão:</b></td>
                    		<td align="left"><select name="idprograma" id="idprograma" class="select" style="width: 120px;">
                                <option value="">--</option>                           
                                <option value="1">3° EDITAL</option>
                                <option value="2">4° EDITAL</option>                      
                            </select></td>
						</tr>
						<tr>
                            <td align="left">Escolha o tipo de arquivo a ser gerado:</td>
                    		<td align="left"><select name="tipoarquivo" id="tipoarquivo" class="select" style="width: 120px;">
                                <option value="">--</option>
                                {id}
                                <option value="{ACAOREMESSA}">{DESCARQUIVOTIPO}</option>
                                {/id}
                              </select>
							</td>
                        </tr>
                    </table>
                    </div>
                        <div class="filtro_campo" >
                            <span class="margin">
                                <input type="submit" name="btnSaveForm" id="btnSaveForm" value="Gerar Arquivo" onclick="window.location.href='<?php echo URL_EXEC; ?>cnab/gerenciadorCpb'"/>
                            </span>
                        </div>
                   
                </form>
                </div>  
                <div>
                    <h2>PASSOS PARA GERAÇÃO DO ARQUIVO</h2>
                    <p>
                        <b>Cadastro:</b>
                        Selecione esta opção para geração de um novo arquivo de cadastro...
                    </p>
                    <p>
                        <b>Crédito:</b>
                        Selecione esta opção para geração de um novo arquivo de crédito...
                    </p>
                    <p>
                        <b>Bloqueio:</b>
                        Selecione esta opção para geração de um novo arquivo bloqueio de um cartão...
                    </p>
                    <p>
                        <b>Desbloqueio:</b>
                        Selecione esta opção para geração de um novo arquivo desbloqueio de um cartão...
                    </p>
                </div>
			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
</body>
</html>