
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	
</head>
<body>
	<div id="wrapper">
		<?php monta_header(1); ?> 
		<?php monta_menu($this->session->userdata('tipoUsuario')); ?>	
		<div class="divtitulo">
			<div class="divtitulo2">
                            >> LISTA DE ARQUIVOS CPB
			</div>
		</div>
		
		<div id="middle">
			<div id="container">
                <div class="main_content_center" style="margin-top:10px; margin-bottom:20px;">
				<table cellspacing="0" align="center" cellpadding="0" class="extensions t95">
                    <thead>
                       <tr class="header">
                            <th style="width: 60px">ID </th>
                            <th style="width: 60px" >LOTE </th>
                            <th>DESCRICAO </th>
                            <th>NOME</th>
                            <th style="width: 60px">DATA </th>
                            
                       </tr>
                          <tr class="header-separator">
                             <td colspan="8">&nbsp;</td>
                          </tr>
                   </thead>
                    <tbody>  
                    {pedido}
                        <tr class="{CORLINHA}">
                                  <td class="no-wrap" style="text-align: left" >{ID}</td>
                                  <td class="no-wrap tdcenter" style="text-align: left">{LOTE}</td>
                                  <td class="no-wrap tdcenter" style="text-align: left">{DESCRICAO}</td>
                                  <td class="no-wrap tdcenter" style="text-align: left">{NOME}</td>
                                  <td class="no-wrap tdcenter" style="text-align: left">{DATA}</td>
                                  
                        </tr>
                    {/pedido}
                    
                    </tbody>  
                </table>
                    
			</div>
              
			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
</body>
</html>