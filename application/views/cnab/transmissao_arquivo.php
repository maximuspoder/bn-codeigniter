﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript" language="javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript" language="javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript" language="javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript" language="javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript" language="javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript" language="javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			// Validação e máscaras
			$("#form_default").validationEngine({ inlineValidation:false , promptPosition : "centerRight", scroll : false });
			
			// máscaras
			$('input:text').setMask(); 
   
			// Focus no primeiro campo
			$("#data").focus();
		});
	</script>
	<!--
    <script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>biblioteca.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
	-->
</head>
<body>
	<?php if(get_value($arquivo, 'IDSTATUS') == '1' || get_value($arquivo, 'IDSTATUS') == '2'){?>
	<div>Informe a data em que o arquivo foi transmitido / retransmitido pelo Internet Banking do Banco do Brasil.</div>
	<br />
	<br />
	<form action="<?php echo URL_EXEC; ?>cnab/transmissao_arquivo_proccess" name="form_default" id="form_default" method="post">
	<input type="hidden" name="idarquivo" id="idarquivo" value="<?php echo get_value($arquivo, 'ID')?>" />
	<div class="inline top" style="width:60px; padding-top:2px">Data:</div>
	<div class="inline top"><input type="text" name="data" id="data" style="width:80px" alt="date" class="validate[required,custom[date-pt_BR]]" value="<?php echo format_date(get_value($arquivo ,'DATATRANS'))?>" /></div>
	<div style="margin-top:65px">
		<hr />
		<div class="inline top"><input type="submit" value="Enviar" /></div>
		<div class="inline middle" style="padding:7px 0 0 5px">ou <a href="javascript:void(0)" onclick="$('#form_default').validationEngine('hide');parent.close_modal();">cancelar</a></div>
	</div>
	</form>
	<?php } else { ?>
		<?php mensagem('warning' , 'Atenção', 'A informação de transmissão / retransmissão de arquivo não pode ser realizada pois o status deste arquivo não comporta este tipo de ação.'); ?>
		<br />
		<button onclick="parent.close_modal();">Fechar</button>
	<?php } ?>
</body>
</html>
<!--
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    </head>
    <body>	
        <div id="container" style="overflow-y: auto; ">
            <div class="main_content_left">
                 <table>	
                        <tr>
                            <td colspan="3" style="text-align: rigth; padding-top:15px;">
                                <p><strong>Informe a data em que o arquivo foi transmitido pelo Internet Banking do Banco do Brasil.</strong></p>
                                <p><label>Data:</label><input type="text" name="data" id="data" ></p>
                                 <?php
                                if ($status == 1) {									
                                    mensagem('warning', '', 'Você ainda não transmitiu seu arquivo.', false, 'width:500px;margin-bottom:20px;');
                                }else
									
                                    mensagem('note', '', 'Você já transmitiu seu arquivo.', false, 'width:500px;margin-bottom:20px;');
                                 ?>
                                <div style="">
                                    <input class="button" type="button" value="  Ok  " />
                                    <a href = "">Cancelar</a>
                                </div>
                            </td>
                        </tr>
                    </table>
                </fieldset>			
            </div>        
        </div>
    </body>
</html> -->