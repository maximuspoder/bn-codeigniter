<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>cnab.js" type="text/javascript"></script>
</head>
<body>
	<div id="wrapper">
		<?php monta_header(1); ?>
		<?php monta_menu($this->session->userdata('tipoUsuario')); ?>	
		<div class="divtitulo">
			<div class="divtitulo2">
                >> RECEBE ARQUIVO CPB 
			</div>
		</div>
		
		<div id="middle">
			<div id="container">
                
                <form name ="recebeArquivo"  method="post" class="form_filtro" enctype="multipart/form-data" action="<?php echo URL_EXEC; ?>cnab/recebeArquivo/" onsubmit="return validaForm_recebeArquivo()">
                    <div class="main_content_left" style="margin-top:10px; margin-bottom:20px;">
                    <?php 
                    if (isset($mensagem)) echo "<span style='color:#990000; font-weight:bold; font-size: 12px;'>"  . $mensagem . "</span>";
                    ?>
                    <table cellspacing="0" cellpadding="4" border="0" align="center">
                    	<tr>
                    		<td align="left"><label for="tipoarquivo" style="width: 220px;">Escolha o tipo de arquivo:</label></td>
                            <td align="left"><select name="tipoarquivo" id="tipoarquivo" class="select">
                                    <option value="">--</option>
                                    {tipo}
                                        <option value="{ACAORETORNO}">{DESCARQUIVOTIPO}</option>
                                    {/tipo}
                                </select></td>
						</tr>
						<tr>
                            <td align="left">Selecione o arquivo para Upload: &nbsp;&nbsp;&nbsp; </td>
                            <td align="left"><input type="file" value="" name="arquivo" id="arquivo" /></td>
                        </tr>
                    </table>
                    </div>
                    
                    <div class="filtro_campo">
                        <span class="margin">
                        	<input type="hidden" name="acao" id="acao" value="enviar" />
                            <input type="submit" value="Enviar" name="btnSaveForm" id="btnSaveForm" class="buttonPadrao"  />
                        </span>
                    </div>
                    
                </form>

			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
</body>
</html>
