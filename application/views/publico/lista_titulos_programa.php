<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title><?php echo TITLE_SISTEMA; ?></title>
        <link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
        <script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
        <script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
    </head>
    <body>
        <div id="wrapper">
            <?php monta_header(0); ?>

            <div class="divtitulo">
                <div class="divtitulo2">
                    >> LIVROS PARTICIPANTES DO EDITAL 3/2011
                </div>
            </div>

            <div id="middle">
                <div id="container" style="text-align:center;">
                    <div class="main_content_center" style="margin-top:10px;">
                        <center>
                            <form name="filtros_lista" id="filtros_lista" method="post" class="form_filtro" action="<?php echo URL_EXEC; ?>publico/listaLivros">
                                <fieldset style="width:94%">
                                    <legend>Filtros</legend>
                                    <div class="filtro_campo">
                                        <span class="margin">ISBN:</span>
                                        <input type="text" name="filtro_isbn" id="filtro_isbn" class="inputText" style="width:100px;" value="<?php echo set_value('filtro_isbn'); ?>" />
                                    </div>
                                    <div class="filtro_campo">
                                        <span class="margin">Título:</span>
                                        <input type="text" name="filtro_titulo" id="filtro_titulo" class="inputText" style="width:100px;" value="<?php echo set_value('filtro_titulo'); ?>" />
                                    </div>

                                    <div class="filtro_campo">
                                        <span class="margin">Autor:</span>
                                        <input type="text" name="filtro_autor" id="filtro_autor" class="inputText" style="width:100px;" value="<?php echo set_value('filtro_autor'); ?>" />
                                    </div>
                                    <div class="filtro_campo">
                                        <span class="margin">Editora:</span>
                                        <input type="text" name="filtro_editora" id="filtro_editora" class="inputText" style="width:150px;" value="<?php echo set_value('filtro_editora'); ?>" />
                                    </div>
                                    <div class="filtro_campo"><span class="margin"><input type="submit" value="Enviar" class="buttonPadrao" /></span></div>
                                </fieldset>
                            </form>
                        </center>

                        <table cellspacing="0" align="center" cellpadding="0" class="extensions t95" style="margin-bottom: 20px;">
                            <thead>
                                <tr class="header">
                                    <th>ISBN</th>
                                    <th class="w100">Título</th>
                                    <th>Autor</th>
                                    <th>Preço</th>
                                    <th>Editora - N. Fantasia</th>
                                </tr>
                                <tr class="header-separator">
                                    <td colspan="5">&nbsp;</td>
                                </tr>
                            </thead>
                            <tbody>
                                {livros}
                                <tr class="{CORLINHA}">
                                    <td class="no-wrap tdcenter">{ISBN_MOSTRAR}</td>
                                    <td class="w100 pr_20 tdleft"><a href="{LINK}" target="_blank" style="text-decoration: none; color: {COR}">{TITULO}</a></td>
                                    <td class="no-wrap pr_20 tdleft">{AUTOR}</td>
                                    <td class="no-wrap pr_20 tdleft" >{PRECO}</td>
                                    <td class="no-wrap pr_20 tdleft">{EDITORA_FANTASIA}</td>
                                </tr>
                                {/livros}



                                <?php
                                if ($vazio == TRUE) {
                                    ?>
                                    <tr>
                                        <td colspan="5" class="no-wrap tdcenter">Nenhum livro encontrado.</td>
                                    </tr>
                                    <?php
                                } else {
                                    ?>
                                    <tr class="noEfeito">
                                        <td colspan="5" class="no-wrap tdcenter">
                                            <div style="display: inline; float: left;">Total Registros: <?php echo $qtdReg; ?></div>
                                            <?php montaPaginacaoPesquisa($nPaginas, $nPag, $limit_de, $exibir_pp, URL_EXEC . 'publico/listaLivros/', 'filtros_lista'); ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div><!-- #container-->
            </div><!-- #middle-->
        </div><!-- #wrapper -->
        <?php monta_footer(); ?>
    </body>
</html>