﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js?<?php echo time();?>" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js?<?php echo time();?>" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.tablesorter.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function() {
			// Verifica a compatibilidade de browser
			if(check_browser_compatibilty())
			{
				// Validação e máscaras
				// $("#form_filter").validationEngine({ inlineValidation:false , promptPosition : "centerRight", scroll : false });
				
				// Seta focus no campo de usuario (login)
				$("#campo_pesquisa").focus();
			}
		});
	</script>
</head>
<body>
	<?php monta_header(0); ?>
	<?php add_elementos_CONFIG(); ?>
	<div id="page_content">
		<div id="inside_content">
			<div>
				<div class="inline" style="float:right;margin-top:10px">
					<img src="<?php echo URL_IMG?>icon_help.png" style="float:left;margin:1px 5px 0 0"	/>
					<a href="javascript:void(0)" onclick="ajax_modal('Instruções para Pesquisa', '<?php echo URL_EXEC ?>comunicacao/instrucoes_pesquisa_bibliotecas', '<?php echo URL_IMG ?>icon_help.png', 450 , 600);">instruções para pesquisa</a>
				</div>
				<div class="inline font_shadow_gray"><h1>Pesquisa</h1></div>
				<div class="inline"><h3 class="font_shadow_gray">&nbsp;> Cadastros de Bibliotecas</h3></div>
			</div>
			<br />
			<br />
			<?php mensagem('info', '', 'Localize nesta tela as bibliotecas constantes do Cadastro Nacional de Bibliotecas.<br />Utilize os filtros de UF (unidade da federação), cidade ou campo de pesquisa (CPF/CNPJ, Razão Social ou Nome Fantasia da biblioteca) para limitar a consulta.<br />Você pode visualizar os detalhes do cadastro clicando no ícone à esquerda, no resultado da pesquisa.<br />Caso a biblioteca procurada ainda não conste no cadastro, indique ao responsável para que faça o cadastro <a href="' . URL_EXEC . 'autocad/preautocad/cnb">clicando aqui</a>.')?>
			<br />
			<form name="form_filter" id="form_filter" action="<?php echo URL_EXEC?>acesso_externo/pesquisar_bibliotecas" method="post" style="background-color:#fff;border:none" class="no_shadow">
				<div class="bold" style="color:#777;"><h6>Nome da Biblioteca</h6></div>
				<div>
					<div class="inline "><input type="text" style="font-size:20px;color:#333;width:500px;margin:0 20px 0 0;" name="campo_pesquisa" id="campo_pesquisa" value="<?php echo get_value($post, 'campo_pesquisa')?>" /></div>
					<input type="submit" value="Pesquisar" />
					<div class="inline top" style="margin:8px 0 0 5px">ou&nbsp;&nbsp;&nbsp;<a href="<?php echo URL_EXEC?>login">voltar</a></a>
				</div>
				<div class="inline top" style="float:right;margin:-2px 0 0 10px;">
					<div style="float:left;margin:7px 0px 0 0;">
						<img src="<?php echo URL_IMG ?>icon_filter.png" style="float:left;margin:5px 5px 0 0"/>
						<h3 class="inline"><a class="black" href="javascript:void(0);" onclick="show_area('box_filter')">Filtros da Pesquisa</a></h3>
					</div>
				</div>
				<div id="box_filter" style="width:100%;float:left;margin:15px 0 30px 0;display:block;">
					<img src="<?php echo URL_CSS ?>img_css/bg_arrow_up.png" style="float:right;margin:-11px 85px 0 0" />
					<img src="<?php echo URL_IMG ?>icon_close.png" style="float:right;margin:7px 7px 0 0;width:10px;cursor:pointer;" onclick="show_area('box_filter')" />
					<img src="<?php echo URL_IMG ?>icon_bullet_minus.png" style="display:none;" id="box_filter_img" />
					<div id="content">
						<div id="label" style="margin-top:3px;">UF: </div>
						<div id="field"><select name="iduf" id="iduf" style="width:55px;" onchange="ajax_get_cidades_by_uf_externo(this.value, 'idcidade');"><?php echo $options_uf; ?></select></div>
						<div id="label" style="margin-top:3px;">Cidade: </div>
						<div id="field"><select name="idcidade" id="idcidade" style="width:150px;"><?php echo $options_cidades; ?></select></div>
						<div id="label" style="margin-top:3px;">Tipo de Biblioteca: </div>
						<div id="field">
							<select name="tipo_biblioteca" id="tipo_biblioteca" style="width:250px;">
								<option value="" <?php echo get_value($post, 'tipo_biblioteca') == '' ? 'selected="selected"' : ''; ?>>TODOS OS TIPOS</option>
								<option value="1" <?php echo get_value($post, 'tipo_biblioteca') == '1' ? 'selected="selected"' : ''; ?>>BIBLIOTECA PÚBLICA MUNICIPAL</option>
								<option value="2" <?php echo get_value($post, 'tipo_biblioteca') == '2' ? 'selected="selected"' : ''; ?>>BIBLIOTECA PÚBLICA ESTADUAL</option>
								<option value="3" <?php echo get_value($post, 'tipo_biblioteca') == '3' ? 'selected="selected"' : ''; ?>>BIBLIOTECA UNIVERSITÁRIA</option>
								<option value="4" <?php echo get_value($post, 'tipo_biblioteca') == '4' ? 'selected="selected"' : ''; ?>>BIBLIOTECA ESPECIALIZADA</option>
								<option value="5" <?php echo get_value($post, 'tipo_biblioteca') == '5' ? 'selected="selected"' : ''; ?>>BIBLIOTECA COMUNITÁRIA</option>
								<option value="6" <?php echo get_value($post, 'tipo_biblioteca') == '6' ? 'selected="selected"' : ''; ?>>BIBLIOTECA COMUNITÁRIA RURAL</option>
								<option value="7" <?php echo get_value($post, 'tipo_biblioteca') == '7' ? 'selected="selected"' : ''; ?>>BIBLIOTECA ESCOLAR</option>
								<option value="8" <?php echo get_value($post, 'tipo_biblioteca') == '8' ? 'selected="selected"' : ''; ?>>PONTO DE LEITURA</option>
								<option value="9" <?php echo get_value($post, 'tipo_biblioteca') == '9' ? 'selected="selected"' : ''; ?>>SALA DE LEITURA</option>
							</select>
						</div>
						<div id="label"></div>
						<div id="field" style="margin-top:3px;"><input type="submit" class="mini" value="Enviar" /></div>
					</div>				
				</div>
			</form>
			<div id="box_resultado_pesquisa_livro"><?php echo $header_pesquisa. $module_table; ?></div>
			<br />
		</div>
	</div>
</body>
</html>