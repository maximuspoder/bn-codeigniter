 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			$("input:text").setMask();
		});
	</script>
</head>
<body>
	<?php monta_header(0); ?>	
	<div id="page_content">		
		<div id="inside_content">
			<div>
				<div class="inline"><h1><a href="<?php echo URL_EXEC;?>acesso_externo/email_reativacao_cadastro" class="black font_shadow_gray">Reativação de Cadastro</a></h1></div>
			<br />
			<br />
			<?php
				if(isset($sucesso) && $sucesso == false)
				{
					mensagem('error', '', 'Nenhum registro foi encontrado com os dados inseridos!');
			?>
			<form name="form_default" id="form_default" method="post" action="<?php echo URL_EXEC; ?>acesso_externo/email_reativacao_cadastro/">
				<div style="margin-top:20px">
					<div class="inline top"><button onclick="set_form_redirect('<?php echo $url_redirect; ?>', '', 'form_default', 'ok');">OK</button></div>&nbsp;&nbsp;&nbsp;&nbsp;
					<div class="inline middle" style="padding:7px 0 0 5px">ou <a href="<?php echo URL_EXEC; ?>">cancelar</a></div>
				</div>
			</form>				
			<?php
				} else {				
			?>
			<?php mensagem('info', '', 'Os seguintes cadastros foram encontrados, para habilitar o cadastro desejado selecione e clique em <b>Escolher Selecionado</b> após você recebera no email do cadastro
			um email de reativação de cadastro.'); ?>
			<form name="form_default" id="form_default" method="post" action="<?php echo URL_EXEC; ?>acesso_externo/email_reativacao_cadastro_process/" onSubmit="return verificaMarcado(get_checked_value('usuario'));">
			<?php echo $module_table; ?>
			<br />
			<span class="fnt_error italic font_14" id="aviso_email_encaminhamento"></span>
				<div style="margin-top:20px">
					<input type="submit" id="btnEnviar" value="Escolher Selecionado"  />
					<div class="inline middle" style="padding:7px 0 0 5px">ou <a href="<?php echo URL_EXEC; ?>">cancelar</a></div>
				</div>
			</form>
			<?php } ?>
			</div>		
		</div><!-- inside_content -->
	</div><!-- page_content -->
</body>
</html>