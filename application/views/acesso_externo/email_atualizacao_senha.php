<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			// Validação e máscaras
			$("#form_default").validationEngine({ inlineValidation:false , promptPosition : "centerRight", scroll : false });
			$("input:text").setMask();
		});
	</script>
</head>
<body>
	<?php monta_header(0); ?>
	<div id="page_content">		
		<div id="inside_content">
			<div>
				<div class="inline"><h1><a href="<?php echo URL_EXEC;?>acesso_externo/email_atualizacao_senha" class="black font_shadow_gray">Gerar Nova Senha</a></h1></div>
			<br />
			<br />
			<?php mensagem('info', '', 'Informe seu login para enviar para seu email uma nova senha. Campos com (*) são obrigatórios.'); ?>
			<br />
			<br />
			<form name="form_default" id="form_default" method="post" action="<?php echo URL_EXEC; ?>acesso_externo/email_atualizacao_senha_process">
				<div class="form_label">*Login:</div>
				<div class="form_field"><input type="text" name="login" id="login" class="validate[required]" style="width:150px;" /></div>
				<br />				
					<hr />
					<div class="inline top"><input type="submit" value="Ok"></div>
					<div class="inline top" style="padding:8px 0 0 5px">ou&nbsp;&nbsp;&nbsp;<a href="<?php echo URL_EXEC;?>usuario/search">voltar</a></div> 
			</form>
			</div>		
		</div><!-- inside_content -->
	</div><!-- page_content -->
</body>
</html>