﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.tablesorter.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function() {
			// Verifica a compatibilidade de browser
			if(check_browser_compatibilty())
			{
				// Validação e máscaras
				$("#form_filter").validationEngine({ inlineValidation:false , promptPosition : "centerRight", scroll : false });
				
				// Seta focus no campo de usuario (login)
				$("#campo_pesquisa").focus();
			}
		});
	</script>
</head>
<body>
	<?php monta_header(0); ?>
	<?php add_elementos_CONFIG(); ?>
	<div id="page_content">
		<div id="inside_content">
			<div>
				<div class="inline"><h1><a href="<?php echo URL_EXEC ?>home/" class="black font_shadow_gray">Início</a></h1></div>
				<div class="inline"><h3 class="font_shadow_gray">&nbsp;> Pesquisa de Livros</h3></div>
				<div class="inline"><h3 class="font_shadow_gray">&nbsp;<a href="javascript:void(0)" onclick="ajax_modal('Instruções para Pesquisa', '<?php echo URL_EXEC ?>comunicacao/instrucoes_pesquisa_livros', '<?php echo URL_IMG ?>icon_help.png', 450 , 600);">instruções para pesquisa</a></h3></div>
			</div>
			<br />
			<br />
			<?php mensagem('info', '', 'Localize nesta tela os títulos cadastrados no Portal do Livro Popular. Para cada título encontrado também é possível visualizar os dados da editora correspondente.')?>
			<br />
			<form name="form_filter" id="form_filter" action="<?php echo URL_EXEC?>acesso_externo/pesquisar_livros" method="post" style="background-color:#fff;border:none" class="no_shadow">
				<div class="inline bold" style="color:#777;"><h4>Digite o nome do livro, autor ou editora a ser pesquisado.</h4></div>
				<div>
					<div class="inline "><input type="text" style="font-size:20px;color:#333;width:500px;margin:0 20px 0 0;" name="campo_pesquisa" id="campo_pesquisa" value="<?php echo get_value($post, 'campo_pesquisa')?>" class="validate[required]" /></div>
					<input type="submit" value="Pesquisar" />
				</div>
			</form>
			<div id="box_resultado_pesquisa_livro"><?php echo $header_pesquisa. $module_table; ?></div>
			<br />
		</div>
	</div>
</body>
</html>