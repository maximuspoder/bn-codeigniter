<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			$("input:text").setMask();		
		});			
	</script>
</head>
<body>
	<?php add_elementos_CONFIG(); ?>

			<div>
				<div class="inline"><h1><a href="<?php echo URL_EXEC;?>comite_acervo/search" class="black font_shadow_gray">Endereço</a></h1></div>
				<div class="inline"><h3 class="font_shadow_gray">&nbsp;> Cadastro de Logradouro</h3></div>
			</div>
			<br />
			<br />
			<?php mensagem('success', '', 'O logradouro foi cadastrado com sucesso! Clique em retornar selecionado para usar o novo logradouro.'); ?>
			<br />
			<br />
			<?php 
				// Monta os valore para o Split.
				$value = $dados_logradouro_lastid[0]['IDLOGRADOURO'] . "|" . get_value($dados_formulario, 'cep') . "|" . get_value($dados_formulario, 'estado') . "|" .get_value($dados_formulario, 'cidade') . "|" . get_value($dados_formulario, 'bairro') . "|" . strtoupper(get_value($dados_formulario, 'nomelogradouro')); 
			echo $value;
			?>
			<button onClick="AlteraEndereco('<?php echo $value; ?>')">Escolher Selecionado</button>
</body>
</html>