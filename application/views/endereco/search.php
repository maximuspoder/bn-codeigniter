<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.tablesorter.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			// Máscaras
			$("input:text").setMask();
		});			
	</script>
</head>
<body>
	<?php add_elementos_CONFIG(); ?>
	<h4 class="font_shadow_gray" style="margin-top:10px; padding-bottom:5px;">Endereços</h4>
	Utilize o filtro abaixo para pesquisar um novo logradouro. 'Caso não encontre um logradouro, 
	localize o endereço mais próximo e adicione um novo logradouro clicando no ícone Adicionar Novo Logradouro <img src="<?php echo URL_IMG; ?>building_add.png" title="Adicionar Novo Logradouro" />.
				<form id="form_filter" action="<?php echo(URL_EXEC);?>endereco/search" method="post" style="margin-left:0px; width:880px;">
				<div id="content">
					<div class="inline">
						<img src="<?php echo URL_IMG;?>icon_filter.png" style="float:left;margin:4px 5px 0 0" />
						<h5 class="black inline" style="margin:3px 0 0 0;">Filtros</h5>
					</div>
					<div id="label" style="; margin-left:7px;">UF: </div>
					<div id="field"><select class="mini"  name="c__iduf" id="c__iduf" style="width:55px; " onchange="ajax_get_cidades_by_uf(this.value, 'c__idcidade');"><?php echo $options_uf; ?></select></div>
					<div id="label"style="; margin-left:7px;">Cidade: </div>
					<div id="field"><select class="mini"  name="c__idcidade" id="c__idcidade" style="width:100px"><?php echo $options_cidades; ?></select></div>
					<div id="label"style="; margin-left:7px;">Bairro: </div>
					<div id="field"><input type="text" name="b__nomebairro" id="b__nomebairro" value="<?php echo get_value($filtros, 'b__nomebairro'); ?>" class="mini" style="width:100px;"></div>
					<div id="label"style="; margin-left:7px;">Log: </div>
					<div id="field"><input type="text" name="l__nomelogradouro" id="l__nomelogradouro" value="<?php echo get_value($filtros, 'l__nomelogradouro'); ?>" class="mini"></div>
					<div id="label"style="; margin-left:7px;">CEP: </div>
					<div id="field"><input type="text" name="l__cep" id="l__cep" value="<?php echo get_value($filtros, 'l__cep'); ?>" class="mini" alt="cep_sem_separador" style="width:80px"></div>
					<div id="label" style="margin-left:0px;"></div>
					<div id="field"><input type="submit" class="mini" value="Enviar" /></div>
				</div>	
				</form>
				<?php echo $module_table; ?>
				<br />
				<div style="margin-top:20px">
					<input id="btnRadio" type="button" value="Escolher Selecionado" onclick="AlteraEndereco(get_checked_value('valores'))" />
					<div class="inline middle" style="padding:7px 0 0 5px">ou <a href="javascript:void(0)" onclick="parent.close_modal();">cancelar</a></div>
				</div>
</body>
</html>