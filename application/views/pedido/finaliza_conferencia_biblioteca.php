<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
</head>
<body>
	<?php add_elementos_CONFIG(); ?>
	<div class="inline top font_shadow_gray"><h3>Resumo da <?php echo $entrega_atual ?>ª Entrega</h3></div>
	<?php echo($msg) ?>
	<?php if($error){ ?>
	<?php if(count($notas_fiscais) > 0) {?>
	<br />
	<br />
	<h5  class="inline font_shadow_gray">Notas fiscais desta entrega</h5>
	<div class="inline italic fnt_error" style="margin:0 0 0 5px;">Clique nas linhas para expandi-las</div>
	<table id="table_itens" class="table_sorter">
		<thead>
			<tr>
				<th style="width:96%">Arquivo (clique para abrir)</th>
				<th style="width:01%;white-space:nowrap;padding-right:15px">Num. DOC</th>
				<th style="width:01%;white-space:nowrap;padding-right:15px">Exemplares</th>
				<th style="width:01%;white-space:nowrap;text-align:right;padding-right:7px">Valor da Soma Itens da Nota</th>
				<th style="width:01%;white-space:nowrap;text-align:right;padding-right:7px">Valor DOC</th>
			</tr>
		</thead>
		<tbody>
			<?php $i = 0; $total_entrega_notas = 0;?>
			<?php foreach($notas_fiscais as $item){?>
			<?php $class = ($i % 2 == 0) ? 'odd' : '';?>
			<?php $class = (in_array(get_value($item, 'ID_PEDIDO_DOCUMENTO'), $id_nota_erro)) ? 'error_line' : '';?>
			<tr <?php echo(($class != "") ? 'class="' . $class . '"' : '');?> style="cursor:pointer;" onclick="show_area('itens_documento_<?php echo(get_value($item, 'ID_PEDIDO_DOCUMENTO'))?>');">
				<td>
					<img src="<?php echo URL_IMG ?>icon_bullet_plus.png" id="itens_documento_<?php echo(get_value($item, 'ID_PEDIDO_DOCUMENTO'))?>_img" style="display:none;" />
					<img src="<?php echo URL_IMG ?>icon_img_info.png" style="margin:0 5px 0 0;" title="Nome do Arquivo: <?php echo get_value($item, 'ARQUIVO')?>" />
					<a href="<?php echo URL_EXEC?>application/uploads/<?php echo get_value($item, 'ARQUIVO')?>" target="_blank"><img src="<?php echo URL_IMG ?>icon_anexo_img.png" title="Clique para visualizar a Imagem" /></a>
				</td>
				<td><?php echo(get_value($item, 'NUMERO_DOC'))?></td>
				<td><?php echo(get_value($item, 'QTDE_ITENS'))?></td>
				<td class="right nowrap">R$ <?php echo(to_moeda($pedido_model->sum_vlr_itens_conferidos($idpedido, get_value($item, 'ID_PEDIDO_DOCUMENTO'))))?></td>
				<td class="right nowrap">R$ <?php echo(to_moeda(get_value($item, 'VALOR_DOC')))?></td>
			</tr>
			<tr id="itens_documento_<?php echo(get_value($item, 'ID_PEDIDO_DOCUMENTO'))?>" style="display:none;">
				<td colspan="5" style="background-color:#FFFFE0">
				<?php 
					$itens_doc = $pedido_documento_model->get_itens_documento(get_value($item, 'ID_PEDIDO_DOCUMENTO'));
					if(count($itens_doc) > 0 ){
				?>
					<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin-top:10px;" class="table_sorter">
						<thead>
						<tr>
							<th>ID Livro (#)</th>
							<th>Título</th>
							<!--<th>Qtde. Solicitada</th>-->
							<th>Qtde. Entregue</th>
							<th>Valor Item</th>
							<th>Subtotal</th>
						</tr>
						</thead>
						<tbody>
						<?php 
						$subtotal_livros = 0;
						foreach($itens_doc as $livro){
						?>
						<tr>
							<td><?php echo(get_value($livro, 'IDLIVRO'));?></td>
							<td><?php echo(get_value($livro, 'TITULO'));?></td>
							<!--<td><?php echo(get_value($livro, 'QTD'));?></td>-->
							<td><?php echo(get_value($livro, 'QTD_ENTREGUE'));?></td>
							<td><?php echo(to_moeda(get_value($livro, 'PRECO_UNIT')));?></td>
							<td class="right"><?php echo(to_moeda(get_value($livro, 'SUBTOTAL')));?></td>
						</tr>
						<?php $subtotal_livros += get_value($livro, 'SUBTOTAL'); } ?>
						</tbody>
						<tfoot><tr><th colspan="4" class="right bold">Valor total de exemplares da Nota: R$</th><th class="bold right"><?php echo(to_moeda($subtotal_livros));?></th></tr></tfoot>
					</table>
				<?php } else { mensagem('info', '', 'Nenhum item/livro conferido para esta nota.'); } ?>
				<br />
				<br />
				</td>
			</tr>
			<?php $i++; $total_entrega_notas += get_value($item, 'VALOR_DOC'); } ?>
		</tbody>
		<tfoot><tr><th class="bold nowrap right" colspan="5" style="padding-right:7px;"><span id="subtotal_itens">Total da Entrega: R$ <?php echo(to_moeda($total_entrega_notas));?></span></th></tr></tfoot>
	</table>
	<?php } ?>
	
	<?php if(count($comprovantes) > 0) {?>
	<br />
	<br />
	<h5 class="font_shadow_gray">Comprovantes cadastrados pelo PDV para esta entrega</h5>
	<table id="table_itens" class="table_sorter">
		<thead>
			<tr>
				<th style="width:96%">Arquivo (clique para abrir)</th>
				<th style="width:01%;white-space:nowrap;padding-right:15px">Num. DOC</th>
				<th style="width:01%;white-space:nowrap;padding-right:15px">Exemplares</th>
				<th style="width:01%;white-space:nowrap;text-align:right;padding-right:7px">Valor da Soma Itens da Nota</th>
				<th style="width:01%;white-space:nowrap;text-align:right;padding-right:7px">Valor DOC</th>
			</tr>
		</thead>
		<tbody>
			<?php $i = 0; $total_entrega_notas = 0;?>
			<?php foreach($comprovantes as $item){?>
			<?php $class = ($i % 2 == 0) ? 'odd' : '';?>
			<tr <?php echo(($class != "") ? 'class="' . $class . '"' : '');?>>
				<td>
					<img src="<?php echo URL_IMG ?>icon_img_info.png" style="margin:0 5px 0 0;" title="Nome do Arquivo: <?php echo get_value($item, 'ARQUIVO')?>" />
					<a href="<?php echo URL_EXEC?>application/uploads/<?php echo get_value($item, 'ARQUIVO')?>" target="_blank"><img src="<?php echo URL_IMG ?>icon_anexo_img.png" title="Clique para visualizar a Imagem" /></a>
				</td>
				<td><?php echo(get_value($item, 'NUMERO_DOC'))?></td>
				<td><?php echo(get_value($item, 'QTDE_ITENS'))?></td>
				<td class="right nowrap">R$ -</td>
				<td class="right nowrap">R$ <?php echo(to_moeda(get_value($item, 'VALOR_DOC')))?></td>
			</tr>
			<?php $i++; $total_entrega_notas += get_value($item, 'VALOR_DOC'); } ?>
		</tbody>
		<tfoot>
			<tr>
				<th class="bold nowrap right" colspan="5">
					<span id="subtotal_itens">Total da Entrega: R$ <?php echo(to_moeda($total_entrega_notas));?></span>	
				</th>
			</tr>
		</tfoot>
	</table>
	<?php } ?>
	<?php } ?>
	
	<div style="margin-top:30px">
		<hr />
		<div class="inline top"><button onclick="parent.close_modal();parent.window.location.reload();">OK</button></div>
		<div class="inline middle" style="padding:7px 0 0 5px">ou <a href="javascript:void(0)" onclick="parent.close_modal();parent.window.location.reload();">cancelar</a></div> 
	</div>
</body>
</html>