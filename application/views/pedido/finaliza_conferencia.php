<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
</head>
<body>
	<?php add_elementos_CONFIG(); ?>
	<div class="inline top"><h3>Resumo da <?php echo $numero_entrega ?>ª Entrega</h3></div>
	<?php 
		if($msg != '') 
		{ 
			echo (isset($id_nota_erro)) ? '<div style="margin-top:10px;">Linhas de tabelas coloridas em tom de vermelho identificam problemas.</div>' : '';
			mensagem('warning', 'Atenção', '<span class="font_10">Identificamos as seguintes incoerências nas informações de conferência do pedido:<br />' . $msg . '</span>', false, 'margin-top:10px;'); 
		} else { 
			echo "Até o momento, não identificamos nenhum problema com as informações cadastradas. Confira novamente os dados abaixo e finalize esta entrega da conferência."; 
		} 
	?>
	<?php if(count($notas_fiscais) > 0) {?>
	<br />
	<br />
	<h6>Notas fiscais desta entrega</h6>
	<table id="table_itens" class="table_sorter">
		<thead>
			<tr>
				<th style="width:01%;"></th>
				<th>ID</th>
				<th>Número</th>
				<th>Qtde. Itens</th>
				<th style="width:01%;white-space:nowrap">Vlr. total dos exemplares</th>
				<th style="width:01%;white-space:nowrap">Vlr. da nota</th>
			</tr>
		</thead>
		<tbody>
			<?php $i = 0; $total_entrega_notas = 0;?>
			<?php foreach($notas_fiscais as $item){?>
			<?php $class = ($i % 2 == 0) ? 'odd' : '';?>
			<?php $class = (in_array(get_value($item, 'ID_PEDIDO_DOCUMENTO'), $id_nota_erro)) ? 'error_line' : '';?>
			<tr <?php echo(($class != "") ? 'class="' . $class . '"' : '');?> style="cursor:pointer;" onclick="show_area('itens_documento_<?php echo(get_value($item, 'ID_PEDIDO_DOCUMENTO'))?>');">
				<td class="center"><img src="<?php echo URL_IMG; ?>icon_bullet_plus.png" id="itens_documento_<?php echo(get_value($item, 'ID_PEDIDO_DOCUMENTO'))?>_img"style="cursor:pointer;" onclick="show_area('itens_documento_<?php echo(get_value($item, 'ID_PEDIDO_DOCUMENTO'))?>');" title="Visualize os Itens desta Nota" /></td>
				<td><?php echo(get_value($item, 'ID_PEDIDO_DOCUMENTO'))?></td>
				<td><?php echo(get_value($item, 'NUMERO_DOC'))?></td>
				<td><?php echo(get_value($item, 'QTDE_ITENS'))?></td>
				<td class="right nowrap">R$ <?php echo(to_moeda($pedido_model->sum_vlr_itens_conferidos($idpedido, get_value($item, 'ID_PEDIDO_DOCUMENTO'))))?></td>
				<td class="right nowrap">R$ <?php echo(to_moeda(get_value($item, 'VALOR_DOC')))?></td>
			</tr>
			<tr id="itens_documento_<?php echo(get_value($item, 'ID_PEDIDO_DOCUMENTO'))?>" style="display:none;">
				<td colspan="6" style="background-color:#FFFFE0">
				<?php 
					$itens_doc = $pedido_documento_model->get_itens_documento(get_value($item, 'ID_PEDIDO_DOCUMENTO'));
					if(count($itens_doc) > 0 ){
				?>
					<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin-top:10px;" class="table_sorter">
						<thead>
						<tr>
							<th>ID</th>
							<th>Título</th>
							<!--<th>Qtde. Solicitada</th>-->
							<th>Qtde. Entregue</th>
							<th>Valor Item</th>
							<th>Subtotal</th>
						</tr>
						</thead>
						<tbody>
						<?php 
						$subtotal_livros = 0;
						foreach($itens_doc as $livro){
						?>
						<tr>
							<td><?php echo(get_value($livro, 'IDLIVRO'));?></td>
							<td><?php echo(get_value($livro, 'TITULO'));?></td>
							<!--<td><?php echo(get_value($livro, 'QTD'));?></td>-->
							<td><?php echo(get_value($livro, 'QTD_ENTREGUE'));?></td>
							<td><?php echo(to_moeda(get_value($livro, 'PRECO_UNIT')));?></td>
							<td class="right"><?php echo(to_moeda(get_value($livro, 'SUBTOTAL')));?></td>
						</tr>
						<?php $subtotal_livros += get_value($livro, 'SUBTOTAL'); } ?>
						</tbody>
						<tfoot><tr><th colspan="4" class="right bold">Valor total de exemplares da Nota: R$</th><th class="bold right"><?php echo(to_moeda($subtotal_livros));?></th></tr></tfoot>
					</table>
				<?php } else { mensagem('info', '', 'Nenhum item/livro conferido para esta nota.'); } ?>
				<br />
				<br />
				<br />
				</td>
			</tr>
			<?php $i++; $total_entrega_notas += get_value($item, 'VALOR_DOC'); } ?>
		</tbody>
		<tfoot>
			<tr>
				<th class="bold nowrap right" colspan="6">
					<span id="subtotal_itens">Total da Entrega: R$ <?php echo(to_moeda($total_entrega_notas));?></span>	
				</th>
			</tr>
		</tfoot>
	</table>
	<?php } ?>
	
	<?php if(count($comprovantes) > 0) {?>
	<br />
	<br />
	<h6>Comprovantes cadastrados pelo PDV para esta entrega</h6>
	<table id="table_itens" class="table_sorter">
		<thead>
			<tr>
				<th></th>
				<th>ID</th>
				<th>Número</th>
				<th>Qtde. Itens</th>
				<th style="width:01%;">Valor</th>
			</tr>
		</thead>
		<tbody>
			<?php $i = 0; $total_entrega_notas = 0;?>
			<?php foreach($comprovantes as $item){?>
			<?php $class = ($i % 2 == 0) ? 'odd' : '';?>
			<tr <?php echo(($class != "") ? 'class="' . $class . '"' : '');?>>
				<td><img src="<?php echo URL_IMG; ?>icon_bullet_plus.png" style="cursor:pointer;display:none" /></td>
				<td><?php echo(get_value($item, 'ID_PEDIDO_DOCUMENTO'))?></td>
				<td><?php echo(get_value($item, 'NUMERO_DOC'))?></td>
				<td><?php echo(get_value($item, 'QTDE_ITENS'))?></td>
				<td class="right nowrap">R$ <?php echo(to_moeda(get_value($item, 'VALOR_DOC')))?></td>
			</tr>
			<?php $i++; $total_entrega_notas += get_value($item, 'VALOR_DOC'); } ?>
		</tbody>
		<tfoot>
			<tr>
				<th class="bold nowrap right" colspan="5">
					<span id="subtotal_itens">Total da Entrega: R$ <?php echo(to_moeda($total_entrega_notas));?></span>	
				</th>
			</tr>
		</tfoot>
	</table>
	<?php } ?>
	
	<!--
	<div style="margin-top:30px">
		<hr />
		<div class="inline top"><button <?php echo(($error) ? 'disabled="disabled"' : '');?>>Enviar</button></div>
		<div class="inline middle" style="padding:7px 0 0 5px">ou <a href="javascript:void(0)" onclick="parent.close_modal();">cancelar</a></div> 
	</div>
	-->
</body>
</html>