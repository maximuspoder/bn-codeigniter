<?php if(count($pedido_itens) > 0){?>
<script type="text/javascript" language="javascript">
	// Desabilita botão de salvar e coloca layer na tela
	$('#form_itens_conferencia').submit(function(){
		// alert('foi');
		$('input[type="submit"]', this).attr('disabled', 'disabled');
		// enable_loading();
	});
</script>
<div style="margin:15px 0;">
	<div class="inline" style="height:16px;width:16px;background-color:#d6edce;border:1px solid #5d9d49;"></div><div class="inline top" style="margin: 1px 0 0 4px;">Item já conferido</div>
	<div class="inline" style="margin-left:20px;height:16px;width:16px;background-color:#f2f2f2;border:1px solid #ccc;"></div>
	<div class="inline" style="margin:0;height:16px;width:16px;background-color:#fff;border:1px solid #ccc;"></div>
	<div class="inline top" style="margin: 1px 0 0 4px;">Item a conferir</div>
	<div class="inline" style="margin-left:20px;"><img src="<?php echo URL_IMG;?>icon_bullet_red.png" /></div>
	<div class="inline top" style="margin: 1px 0 0 4px;">Item inexigível</div>
</div>
<form name="form_itens_conferencia" id="form_itens_conferencia" action="<?php echo URL_EXEC?>pedido/save_conferencia/<?php echo(get_value($pedido, 'IDPEDIDO'));?>" method="post">
	<table id="table_itens" class="table_sorter">
		<thead>
			<tr>
				<th>ID</th>
				<th>ISBN</th>
				<th>Título</th>
				<th>Qtde. Pedida</th>
				<th>Qtde. Entregue</th>
				<th>Nota</th>
				<th class="nowrap">Valor Item (R$)</th>
				<th class="center">Subtotal</th>
				<th style="width:01%;"></th>
			</tr>
		</thead>
		<tbody>
			<?php $i = 0; $subtotal_geral = 0;?>
			<?php foreach($pedido_itens as $item){?>
			<?php $class = ($i % 2 == 0) ? 'odd' : '';?>
			<?php $color = ($pedido_documento_model->get_id_pedido_documento_item(get_value($item, 'IDLIVRO'), get_value($pedido, 'IDPEDIDO')) != "") ? 'conferido' : '';?>
			<?php $documento = $pedido_documento_model->get_dados_documento($pedido_documento_model->get_id_pedido_documento_item(get_value($item, 'IDLIVRO'), get_value($pedido, 'IDPEDIDO')));?>
			<tr <?php echo(($class != "" || $color != "") ? 'class="' . $class . ' ' . $color . '"' : '');?>>
				<td class="center"><?php echo(get_value($item, 'IDLIVRO'))?></td>
				<td><?php echo(get_value($item, 'ISBN'))?></td>
				<td><?php echo(get_value($item, 'TITULO'))?></td>
				<td class="center">
					<?php echo(get_value($item, 'QTD'))?>
					<input type="hidden" name="qtd_original_entregue_<?php echo(get_value($item, 'IDLIVRO'))?>" id="qtd_original_entregue_<?php echo(get_value($item, 'IDLIVRO'))?>" value="<?php echo(get_value($item, 'QTD_ENTREGUE'));?>" />
				</td>
				<td class="center"><input type="text" class="mini" name="qtd_entregue_<?php echo(get_value($item, 'IDLIVRO'))?>" id="qtd_entregue_<?php echo(get_value($item, 'IDLIVRO'))?>" <?php echo(get_value($documento, 'NUMERO_ENTREGA') != 0 ? 'disabled="disabled"' : '')?> value="<?php echo(get_value($item, 'QTD_ENTREGUE'));?>" onblur="recalcula_vlr_total('<?php echo(get_value($item, 'IDLIVRO'));?>', <?php echo(((get_value($pedido, 'TIPO_ATENDIMENTO') == 1 && $livro_model->is_inexigivel(get_value($item, 'IDLIVRO'))) || (get_value($pedido, 'TIPO_ATENDIMENTO') == 2)) ? 'true' : 'false' );?>)" style="width:30px;" alt="integer" title="<?php echo(get_value($item, 'QTD'));?>" lang="<?php echo(get_value($item, 'QTD_ENTREGUE'));?>" /></td>
				<td class="center">
					<input type="hidden" id="nota_original_<?php echo(get_value($item, 'IDLIVRO'));?>" name="nota_original_<?php echo(get_value($item, 'IDLIVRO'));?>" value="<?php $id_doc = $pedido_documento_model->get_id_pedido_documento_item(get_value($item, 'IDLIVRO'), get_value($pedido, 'IDPEDIDO')); echo ($id_doc == 0) ? "" : $id_doc; ?>" />
					<select id="nota_<?php echo(get_value($item, 'IDLIVRO'));?>" name="nota_<?php echo(get_value($item, 'IDLIVRO'));?>" class="mini" style="width:80px;" <?php echo(get_value($documento, 'NUMERO_ENTREGA') != 0 ? 'disabled="disabled"' : '')?>>
					<?php echo(monta_options_array($options_doc, $pedido_documento_model->get_id_pedido_documento_item(get_value($item, 'IDLIVRO'), get_value($pedido, 'IDPEDIDO'))));?>
					</select>
				</td>
				<td class="center">
					<?php echo(to_moeda(get_value($item, 'PRECO_UNIT')))?>
					<input type="text" id="input_preco_unit_<?php echo(get_value($item, 'IDLIVRO'));?>" value="<?php echo(get_value($item, 'PRECO_UNIT'))?>" style="display:none;" />
				</td>
				<td class="center">
					<div id="subtotal_<?php echo(get_value($item, 'IDLIVRO'))?>">
						<?php $subtotal = get_value($item, 'QTD_ENTREGUE') * get_value($item, 'PRECO_UNIT'); echo(to_moeda($subtotal)); $subtotal_geral += $subtotal;?>
						<input type="hidden" id="input_subtotal_<?php echo(get_value($item, 'IDLIVRO'));?>" value="<?php echo($subtotal)?>" />
					</div>
				</td>
				<td><?php echo(($livro_model->is_inexigivel(get_value($item, 'IDLIVRO')) && get_value($pedido, 'TIPO_ATENDIMENTO') == 1) ? '<img src="' . URL_IMG . 'icon_bullet_red.png" title="Item inexigível" />' : '');?></td>
			</tr>
			<?php $i++; } ?>
		</tbody>
		<tfoot>
			<tr>
				<th class="bold nowrap right" colspan="9">
					<!--<span id="subtotal_itens">Total: R$ <?php echo(to_moeda($subtotal_geral))?><input type="text" id="input_subtotal_itens" value="<?php echo($subtotal_geral)?>" style="display:none;" /></span><br /><br />-->
					<span id="subtotal_itens_conferidos" style="color:green;"><span id="label_total_conferido"></span>&nbsp;Total conferido até o momento: R$ <?php echo(to_moeda($subtotal_itens_conferidos))?><input type="text" id="input_subtotal_geral" value="<?php echo($subtotal_itens_conferidos)?>" style="display:none;" /></span><br /><br />
					<span style="color:blue;">Valor do Pedido: R$ <?php echo(to_moeda(get_value($pedido, 'VALORTOTAL')));?></span>	
				</th>
			</tr>
		</tfoot>
	</table>
	<?php if($entrega_atual <= get_value($pedido, 'MAX_QTDE_ENTREGAS')){ ?>
	<div class="middle" style="margin:20px 0 10px 0;"><div class="inline top" style="margin:0 10px 0 0;"><input type="submit" value="Salvar" onclick="enable_loading();" /></div></div>
	<?php } ?>
</form>
<?php } else { mensagem('note', '', 'Nenhum livro localizado. Tente refazer sua consulta alterando os filtros disponíveis.'); } ?>