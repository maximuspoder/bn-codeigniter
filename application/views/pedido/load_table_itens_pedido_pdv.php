<?php if(count($pedido_itens) > 0){?>
<div style="margin:15px 0;">
	<div class="inline" style="height:16px;width:16px;background-color:#d6edce;border:1px solid #5d9d49;"></div><div class="inline top" style="margin: 1px 0 0 4px;">Item já conferido</div>
	<div class="inline" style="margin-left:20px;height:16px;width:16px;background-color:#f2f2f2;border:1px solid #ccc;"></div>
	<div class="inline" style="margin:0;height:16px;width:16px;background-color:#fff;border:1px solid #ccc;"></div>
	<div class="inline top" style="margin: 1px 0 0 4px;">Item a conferir</div>
	<div class="inline" style="margin-left:20px;"><img src="<?php echo URL_IMG;?>icon_bullet_red.png" /></div>
	<div class="inline top" style="margin: 1px 0 0 4px;">Item inexigível</div>
</div>
<table id="table_itens" class="table_sorter">
	<thead>
		<tr>
			<th>ID</th>
			<th>ISBN</th>
			<th>Título</th>
			<th>Qtde. Pedida</th>
			<th>Qtde. Entregue</th>
			<th class="center">Nota</th>
			<th class="nowrap">Valor Item (R$)</th>
			<th class="center">Subtotal</th>
			<th style="width:01%;"></th>
		</tr>
	</thead>
	<tbody>
		<?php $i = 0; $subtotal_geral = 0;?>
		<?php foreach($pedido_itens as $item){?>
		<?php $class = ($i % 2 == 0) ? 'odd' : '';?>
		<?php $color = ($pedido_documento_model->get_id_pedido_documento_item(get_value($item, 'IDLIVRO'), get_value($pedido, 'IDPEDIDO')) != "") ? 'conferido' : '';?>
		<tr <?php echo(($class != "" || $color != "") ? 'class="' . $class . ' ' . $color . '"' : '');?>>
			<td class="center"><?php echo(get_value($item, 'IDLIVRO'))?></td>
			<td><?php echo(get_value($item, 'ISBN'))?></td>
			<td><?php echo(get_value($item, 'TITULO'))?></td>
			<td class="center"><?php echo(get_value($item, 'QTD'))?></td>
			<td class="center"><?php echo(get_value($item, 'QTD_ENTREGUE'));?></td>
			<td class="center">
				<?php 
					$id_documento = $pedido_documento_model->get_id_pedido_documento_item(get_value($item, 'IDLIVRO'), get_value($pedido, 'IDPEDIDO'));
					if($id_documento != 0)
					{
						// Tem documento (nota) já vinculada
						$documento = $pedido_documento_model->get_dados_documento($id_documento); 
						echo(get_value($documento, 'NUMERO_DOC'));
					} else {
						echo('<span class="italic" style="color:red;">Nota ainda não informada pela biblioteca</span>');
					}
				?>
			</td>
			<td class="center"><?php echo(to_moeda(get_value($item, 'PRECO_UNIT')))?></td>
			<td class="center">
				<?php $subtotal = get_value($item, 'QTD_ENTREGUE') * get_value($item, 'PRECO_UNIT'); echo(to_moeda($subtotal)); $subtotal_geral += $subtotal;?>
				<?php $subtotal = get_value($item, 'QTD_ENTREGUE') * get_value($item, 'PRECO_UNIT'); echo(to_moeda($subtotal)); $subtotal_geral += $subtotal;?>
			</td>
			<td><?php echo(($livro_model->is_inexigivel(get_value($item, 'IDLIVRO'))) ? '<img src="' . URL_IMG . 'icon_bullet_red.png" title="Item inexigível" />' : '');?></td>
		</tr>
		<?php $i++; } ?>
	</tbody>
	<tfoot>
		<tr>
			<th class="bold nowrap right" colspan="9">
				<!--<span id="subtotal_itens">Total: R$ <?php echo(to_moeda($subtotal_geral))?><input type="text" id="input_subtotal_itens" value="<?php echo($subtotal_geral)?>" style="display:none;" /></span><br /><br />-->
				<span id="subtotal_itens_conferidos" style="color:green;"><span id="label_total_conferido"></span>&nbsp;Total conferido até o momento: R$ <?php echo(to_moeda($subtotal_itens_conferidos))?><input type="text" id="input_subtotal_geral" value="<?php echo($subtotal_itens_conferidos)?>" style="display:none;" /></span><br /><br />
				<span style="color:blue;">Valor do Pedido: R$ <?php echo(to_moeda(get_value($pedido, 'VALORTOTAL')));?></span>	
			</th>
		</tr>
	</tfoot>
</table>
<?php } else { mensagem('note', '', 'Nenhum livro localizado. Tente refazer sua consulta alterando os filtros disponíveis.'); } ?>