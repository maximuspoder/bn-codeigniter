<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>pedido.js" type="text/javascript"></script>
</head>
<body>
	<div id="wrapper">
		<?php monta_header(1); ?>
		<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
		<?php add_elementos_CONFIG(); ?>
		
		<div class="divtitulo">
			<div class="divtitulo2">
				>> PEDIDOS
			</div>
		</div>
		
		<div id="middle">
			<div id="container" style="text-align:center;">
				<div class="main_content_center" style="margin-top:10px;">
					<center>
						<form name="filtros_lista" id="filtros_lista" method="post" class="form_filtro" action="<?php echo URL_EXEC; ?>pedido/lista">
							<fieldset style="width:94%">
								<legend>Filtros</legend>
								<div class="filtro_campo">
									<span class="margin">Programa:</span>
									<select name="filtro_programa" id="filtro_programa" class="select" style="width:150px;"><?php echo($optionsPrograma); ?></option>
									</select>
								</div>
								<div class="filtro_campo">
									<span class="margin">Status:</span>
									<select name="filtro_status" id="filtro_status" class="select" style="width:150px;"><?php echo($optionsStatus); ?></option>
									</select>
								</div>
								<div class="filtro_campo">
									<span class="margin">PDV:</span>
									<input type="text" name="filtro_pdv" id="filtro_pdv" class="inputText" style="width:100px;" value="<?php echo($filtros['filtro_pdv']);?>" />
								</div>
								<div class="filtro_campo"><span class="margin"><input type="submit" value="Enviar" class="buttonPadrao" /></span></div>
							</fieldset>
						</form>
					</center>
					
					<table cellspacing="0" align="center" cellpadding="0" class="extensions t95">
						<thead>
							<tr class="header">
								<th>Programa</th>
								<th class="w100">Ponto de Venda</th>
								<th>Status</th>
								<th>Valor Total</th>
								<th>Opções</th>
							</tr>
							<tr class="header-separator">
								<td colspan="5">&nbsp;</td>
							</tr>
						</thead>
						<tbody>
							{pedidos}
							<tr class="{CORLINHA}">
								<td class="no-wrap tdleft">{DESCPROGRAMA}</td>
								<td class="w100 tdleft">{PDV}</td>
								<td class="no-wrap pr_20 tdleft">{DESCSTATUS}</td>
								<td class="no-wrap tdright">{VALORTOTAL}</td>
								<td class="no-wrap tdcenter">
									<a href="<?php echo(URL_EXEC);?>/pedido/conferencia/{IDPEDIDO}"><img src="<?php echo(URL_IMG)?>icon_pedido_conferencia.png" title="Conferência do Pedido" /></a>
									&nbsp;
									<a href="<?php echo(URL_EXEC);?>/pedido/gerencia/{IDPROGRAMA}/{IDPEDIDO}"><img src="<?php echo(URL_IMG)?>/cog_edit.png" title="Gerenciar Pedido" /></a>
									&nbsp;
									<a href="javascript:void(0);" onclick="downloadExcelPedido({IDPEDIDO});"><img src="<?php echo(URL_IMG)?>/icon_excel.gif" title="Exportar para Excel" /></a>
								</td>
							</tr>
							{/pedidos}
							
							<?php
							if ($vazio == TRUE)
							{
								?>
								<tr>
									<td colspan="5" class="no-wrap tdcenter">Nenhum pedido encontrado.</td>
								</tr>
								<?php 
							}
							else
							{
								?>
								<tr class="noEfeito">
									<td colspan="5" class="no-wrap tdcenter">
										<div style="display: inline; float: left;">Total Registros: <?php echo $qtdReg; ?></div>
										<?php montaPaginacaoPesquisa($nPaginas, $nPag, $limit_de, $exibir_pp, URL_EXEC . 'pedido/lista/', 'filtros_lista'); ?>
									</td>
								</tr>
								<?php
							}
							?>
						</tbody>
					</table>
				</div>
			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
	<iframe id="download_excel_pedido" src="" style="display:none;"></iframe>
</body>
</html>