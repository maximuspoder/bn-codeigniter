<div id="container" style="overflow-y: auto; ">
    <div class="main_content_left">
        <form name="form_cad" id="form_cad" method="post" action="javascript:submitRemovePedidoItem(<?php echo($idLivro)?>);">
            <fieldset style="width:500px; margin-left:20px;">
                <legend><b>Remover Livro da Lista</b></legend>
                <div id="fs_div">
                    <table id="tabelaCadastros2" align="center" border="0">
                        <tr>
                            <td><img src="<?php echo URL_IMG; ?>atencao_peq.png" style="padding-right:10px;" /></td>
							<td>Atenção! O livro que está sendo visualizado será removido da lista de aquisições. Para prosseguir clique em OK, para desistir clique em cancelar.</td>
                        </tr>
                        <tr>
                            <td class="bold">Livro:</td>
                            <td><?php echo($titulo);?></td>
                        </tr>
						<tr>
                            <td class="bold">Autor:</td>
                            <td><?php echo($autor);?></td>
                        </tr>
						<tr>
                            <td class="bold">Editora:</td>
                            <td><?php echo($editora);?> (<?php echo(($edicao != '') ? $edicao . 'ª Edição, ' : '');?><?php echo($ano);?>)</td>
                        </tr>
						<tr>
                            <td class="bold">Preço:</td>
                            <td>R$ <?php echo(masc_real($preco));?></td>
                        </tr>
                        <tr>
                            <td class="bold">Quantidade Atual:</td>
                            <td><?php echo($qtd);?></td>
                        </tr>
                        <tr>
                            <td colspan="4" style="text-align: center; padding-top:15px;">
                                <input class="buttonPadrao" type="button" value="  Cancelar  " onclick="$('ligthbox_bg').setStyle({display : 'none'});" />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input class="buttonPadrao" id="btnSaveForm" name="btnSaveForm" type="button" value="  OK  " onclick="submitRemovePedidoItem(<?php echo($idLivro)?>);"/>
                            </td>
                        </tr>
                        <tr><td colspan="4" style="text-align: center; padding-top:5px; font-size:11px;"></td></tr>
                    </table>
                </div>
            </fieldset>
        </form>
    </div>
</div><!-- #container-->

