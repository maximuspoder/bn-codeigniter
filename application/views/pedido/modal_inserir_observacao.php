<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			// Validação e máscaras
			$("#form_default").validationEngine({ inlineValidation:false , promptPosition : "centerRight", scroll : false });
			
			// máscaras
			$('input:text').setMask();
		});
	</script>
</head>
<body>
	<div>Utilize o formulário abaixo para inserir uma nova observação ao pedido referenciado. Campos com (*) são obrigatórios.</div>
	<br />
	<form action="<?php echo URL_EXEC; ?>pedido/modal_inserir_observacao_proccess" name="form_default" id="form_default" method="post">
		<input type="hidden" name="idpedido" id="idpedido" value="<?php echo($idpedido);?>" />
		<div class="form_label">*Categoria:</div>
		<div class="form_field">
			<select name="categoria" id="categoria" style="width:250px;" class="validate[required]">
				<option value="GERAL">GERAL</option>
				<option value="CONFERENCIA_ADMINISTRATIVA">CONFERENCIA ADMINISTRATIVA</option>
				<option value="ACOES_BIBLIOTECA">AÇÕES DA BIBLIOTECA</option>
				<option value="ACOES_PDV">AÇÕES DO PDV</option>
			</select>
		</div>
		<br />
		<div class="form_label">*Observação:</div>
		<div class="form_field">
			<textarea name="observacao" id="observacao" style="width:350px;height:150px;" class="validate[required]"></textarea>
		</div>
		<div style="margin-top:30px">
			<hr />
			<div class="inline top"><input type="submit" value="OK" /></div>
			<div class="inline middle" style="padding:7px 0 0 5px">ou <a href="<?php echo URL_EXEC?>pedido/modal_observacoes/<?php echo $idpedido; ?>">cancelar</a></div> 
		</div>
	</form>
</body>
</html>