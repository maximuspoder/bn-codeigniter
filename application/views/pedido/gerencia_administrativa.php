<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			// Validação e máscaras
			$("#form_default").validationEngine({ inlineValidation:false , promptPosition : "centerRight", scroll : false });
			$("input:text").setMask();
		});
	</script>
</head>
<body>
	<?php monta_header(1); ?>
	<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
	<?php add_elementos_CONFIG(); ?>
	<div id="page_content">
		<div id="inside_content">
			<div>
				<div class="inline"><h1><a href="<?php echo URL_EXEC . $url_redirect;?>" class="black font_shadow_gray">Pedidos</a></h1></div>
				<div class="inline"><h3 class="font_shadow_gray">&nbsp;> Gerência Administrativa</h3></div>
			</div>
			<br />
			<br />
			<div class="inline top left" style="width:565px;">
				<h4 class="font_shadow_gray">Dados do Pedido</h4>
				<hr />
				<div id="box_group_view" style="display:block;">
					<div class="odd">
						<div id="label_view">ID Pedido (#):</div>
						<div id="field_view" style="width:400px;"><?php echo(get_value($dados, 'IDPEDIDO'));?></div>
					</div>
					<div>
						<div id="label_view">Situação:</div>
						<div id="field_view" style="width:400px;"><?php echo(get_value($dados, 'DESCSTATUS'));?></div>
					</div>
					<div class="odd">
						<div id="label_view">Valor:</div>
						<div id="field_view" style="width:400px;">R$ <?php echo(to_moeda(get_value($dados, 'VALORTOTAL')));?></div>
					</div>
					<div>
						<div id="label_view">Qtde. Total Itens:</div>
						<div id="field_view" style="width:400px;"><?php echo($pedido_itens_count);?></div>
					</div>
					<div class="odd">
						<div id="label_view">Biblioteca:</div>
						<div id="field_view" style="width:400px;"><?php echo(get_value($dados, 'BIBLIOTECA'));?>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="ajax_modal('Visualizar Detalhes', '<?php echo URL_EXEC; ?>biblioteca/modal_form_view_biblioteca/<?php echo(get_value($dados, 'IDBIBLIOTECA'));?>', '<?php echo URL_IMG; ?>icon_detalhes.png', 600, 750);">Visualizar detalhes</a></div>
					</div>
					<div>
						<div id="label_view">Ponto de Venda:</div>
						<div id="field_view" style="width:400px;">
							<?php if(get_value($dados, 'PDV') == '') {?>
								<div class="comment italic">Nenhum Ponto de Venda parceiro</div>
							<?php } else { ?>
								<?php echo(get_value($dados, 'PDV'));?>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="ajax_modal('Visualizar Detalhes', '<?php echo URL_EXEC; ?>pdv/modal_dados_pdv/<?php echo(get_value($dados, 'IDPDV'));?>', '<?php echo URL_IMG; ?>icon_detalhes.png', 600, 750);">Visualizar detalhes</a>
							<?php } ?>
						</div>
					</div>
					<div class="odd">
						<div id="label_view">Entregas finalizadas pela Biblioteca:</div>
						<div id="field_view" style="width:400px;"><?php echo $pedido_model->get_quantidade_entregas(get_value($dados, 'IDPEDIDO'), 2); ?></div>
					</div>
					<div>
						<div id="label_view">Entregas finalizadas pelo Ponto de Venda:</div>
						<div id="field_view" style="width:400px;"><?php echo $pedido_model->get_quantidade_entregas(get_value($dados, 'IDPEDIDO'), 4);?></div>
					</div>
				</div>
			</div>
			
			<div class="inline" style="display:inline-block;margin:-10px 0 0 50px">
				<?php start_box('Informações Financeiras', 'icon_info_financeiro.png', 'margin-bottom:30px;'); ?>
					<div class="bold">Responsável Financeiro que irá realizar pagamentos:</div>
					<?php if(count($resp_financeiro) > 0){?>
					<div style="margin-top:5px">
						<img src="<?php echo URL_IMG ?>icon_user.png" style="float:left;margin:1px 5px 0 0;" />
						<div><?php echo get_value($resp_financeiro, 'RAZAOSOCIAL'); ?></div>
					</div>
					<div style="margin-top:5px">
						<div style="float:left;margin:1PX 5px 0 0;"><?php echo get_value($resp_financeiro, 'CARTAO_IMG'); ?></div>
						<div><?php echo get_value($resp_financeiro, 'CARTAO_SITUACAO'); ?></div>
					</div>
					<?php } else { mensagem('warning', '', '<span class="font_11">Biblioteca sem Responsável Financeiro.</span>', false, 'width:298px;margin-top:5px'); } ?>
					<div class="bold" style="margin-top:20px">Entregas Finalizadas (valores a receber):</div>
					<div>
						<?php if($entregas_finalizadas > 0){ ?>
						<div>&bull;&nbsp;1ª Entrega: <span style="color:green">R$ 4223,25</span></div>
						<div style="margin-bottom:10px;">&nbsp;&nbsp;(Crédito ainda não liberado. <a href="">saiba mais</a>)</div>
						<div>&bull;&nbsp;1ª Entrega: <span style="color:green">R$ 4223,25</span></div>
						<div style="margin-bottom:10px;">&nbsp;&nbsp;(Crédito ainda não liberado. <a href="">saiba mais</a>)</div>
						<?php } else { mensagem('warning', '', '<span class="font_11">Nenhuma entrega finalizada.</span>', false, 'width:298px;'); } ?>
					</div>
				<?php end_box();?>
				
				<h5 class="font_shadow_gray">Links do Pedido</h5>
				<div style="margin-top:5px">
					<img src="<?php echo URL_IMG ?>icon_conferencia_adm_pedido.png" style="float:left;margin:1px 5px 0 0"  />
					<a href="<?php echo URL_EXEC?>pedido/conferencia_administrativa_pedido/<?php echo get_value($dados, 'IDPEDIDO')?>">Conferência Administrativa do Pedido</a>
				</div>
				<div style="margin-top:5px">
					<img src="<?php echo URL_IMG ?>icon_observacoes.png" style="float:left;margin:1px 5px 0 0"  />
					<a href="javascript:void(0)" onclick="iframe_modal('Observações do Pedido', '<?php echo URL_EXEC?>pedido/modal_observacoes/<?php echo get_value($dados, 'IDPEDIDO')?>', '<?php echo URL_IMG ?>icon_observacoes.png', 600, 750);">Observações do Pedido</a>
				</div>
			</div>
			
			<div onclick="show_area('box_config_pedido')" style="cursor:pointer;margin-top:50px;">
				<img src="<?php echo URL_IMG?>icon_bullet_plus.png" id="box_config_pedido_img" style="float:left;margin:5px 5px 0 0;"/>
				<h4 class="inline top font_shadow_gray">Liberação de Inserção de Documentos</h4>
			</div>
			<hr />
			<div id="box_config_pedido" style="display:none;">
				<div>
					Neste bloco você poderá liberar a inserção de documentos 'livre' no pedido, fazendo com que o usuário ao inserir um documento informe também a qual entrega este documento pertence. 
					Caso exista um valor de entrega já liberado para este pedido não será possível fazer a liberação uma vez que a inserção de novos documentos poderá alterar o valor total da entrega. 
					Novos documentos inseridos já com o número da entrega informado alterarão o valor total desta mesma entrega, caso ela não tenha sido liberada ainda.
				</div>
				<div style="margin-top:15px">
					<input type="checkbox" name="liberar_ins_documento" id="liberar_ins_documento" style="margin:0 0 0 1px" <?php echo((get_value($dados, 'LIBERAR_INS_DOCUMENTO') == 'S') ? 'checked="checked"' : ''); ?> <?php echo(($tem_recurso_liberado) ? 'disabled="disabled"' : '')?> />
					<div class="inline top" style="margin:-2px 0 0 5px;">Liberar Inserção de Documentos <span class="fnt_error italic"><?php if($tem_recurso_liberado){ echo '(Este pedido possui recurso liberado, portanto esta configuração não pode ser alterada.)'; }?></span></div>
				</div>
				<div class="inline top" style="margin:15px 0 5px 0px;"><button onclick="ajax_save_config_pedido(<?php echo get_value($dados, 'IDPEDIDO')?>);">Salvar</button></div>
				<div class="inline top" style="margin:22px 0 5px 5px;">
					<span id="ajax_loading" style="display:none;"><img src="<?php echo URL_IMG?>ajax_loader_16.gif" /></span>
					<span style="display:none;" class="fnt_success" id="ajax_complete">
						<img src="<?php echo URL_IMG ?>icon_success.png" class="inline top" style="margin:1px 2px 0 0;" />
						<div class="inline top font_11">Salvo com sucesso.</div>
					</span>
				</div>
			</div>
			
			
			<div onclick="show_area('box_docs_pedido')" style="cursor:pointer;margin-top:50px;">
				<img src="<?php echo URL_IMG?>icon_bullet_plus.png" id="box_docs_pedido_img" style="float:left;margin:5px 5px 0 0;"/>
				<h4 class="inline top font_shadow_gray">Documentos do Pedido</h4>
			</div>
			<hr />
			<div id="box_docs_pedido" style="display:none;"><?php echo $table_documentos; ?></div>
			
			
			<div onclick="show_area('box_itens_pedido')" style="cursor:pointer;margin-top:50px;">
				<img src="<?php echo URL_IMG?>icon_bullet_plus.png" id="box_itens_pedido_img" style="float:left;margin:5px 5px 0 0;" />
				<h4 class="inline top font_shadow_gray">Itens do Pedido (<?php echo $pedido_titulos_count; ?> títulos)</h4><div class="inline top comment italic" style="margin:5px 0 0 5px;">Clique para expandir</div>
			</div>
			<hr />
			<div id="box_itens_pedido" style="display:none;"><?php echo $table_itens_pedido; ?></div>
			
			
			<div style="margin-top:35px">
				<hr />
				<div class="inline top"><button onclick="redirect('<?php echo URL_EXEC . $url_redirect; ?>');">OK</button></div>
				<div class="inline top" style="padding:8px 0 0 5px">ou&nbsp;&nbsp;&nbsp;<a href="<?php echo URL_EXEC . $url_redirect; ?>">voltar</a></div> 
			</div>
		</div>
	</div>
</body>
</html>