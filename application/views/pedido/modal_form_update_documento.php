<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			// Validação e máscaras
			$("#form_default").validationEngine({ inlineValidation:false , promptPosition : "centerRight", scroll : false });
			
			// máscaras
			$('input:text').setMask();
		});
	</script>
</head>
<body>
	<div>Utilize o formulário abaixo para editar um documento do pedido referenciado. Campos com (*) são obrigatórios.</div>
	<br />
	<form action="<?php echo URL_EXEC; ?>pedido/modal_form_update_documento_proccess" name="form_default" id="form_default" enctype="multipart/form-data" method="post">
		<input type="hidden" name="id_pedido_documento" id="id_pedido_documento" value="<?php echo($id_pedido_documento);?>" />
		<input type="hidden" name="idpedido" id="idpedido" value="<?php echo(get_value($documento, 'IDPEDIDO'));?>" />
		<div class="form_label">*Tipo DOC:</div>
		<div class="form_field">
			<select name="id_pedido_documento_tipo" id="id_pedido_documento_tipo" style="width:162px;" class="validate[required]">
				<?php if($idusuariotipo == 2) { ?>
				<option value="1" <?php echo(get_value($documento, 'ID_PEDIDO_DOCUMENTO_TIPO') == 1 ? 'selected="selected"' : '');?>>NOTA FISCAL</option>
				<?php } else { ?>
				<option value="2" <?php echo(get_value($documento, 'ID_PEDIDO_DOCUMENTO_TIPO') == 2 ? 'selected="selected"' : '');?>>COMPROVANTE DE ENTREGA</option>
				<?php } ?>
			</select>
		</div>
		<!--
		<?php // if($idusuariotipo == 4 || get_value($pedido, 'MAX_QTDE_ENTREGAS') >= 1){?>
		<?php if($idusuariotipo == 4){?>
		<br />
		<div class="form_label">*Entrega:</div>
		<div class="form_field">
			<select name="numero_entrega" id="numero_entrega" style="width:162px;" class="validate[required]">
				<?php if(get_value($pedido, 'MAX_QTDE_ENTREGAS') == 1) { ?>
				<option value="1" <?php echo(get_value($documento, 'NUMERO_ENTREGA') == 1 ? 'selected="selected"' : '');?>>1ª ENTREGA</option>
				<?php } elseif(get_value($pedido, 'MAX_QTDE_ENTREGAS') == 2) { ?>
				<option value="1" <?php echo(get_value($documento, 'NUMERO_ENTREGA') == 1 ? 'selected="selected"' : '');?>>1ª ENTREGA</option>
				<option value="2" <?php echo(get_value($documento, 'NUMERO_ENTREGA') == 2 ? 'selected="selected"' : '');?>>2ª ENTREGA</option>
				<?php } ?>
			</select>
			<div class="comment" style="margin:0 0 10px 0;width:380px;">Caso nenhuma opção esteja aparecendo, você deve primeiramente informar a quantidade de entregas para a contemplação total dos títulos do pedido.</div>
		</div>
		<?php  } ?>
		-->
		<br />
		<div class="form_label">*Número DOC:</div>
		<div class="form_field"><input type="text" name="numero_doc" id="numero_doc" class="validate[required]" style="width:150px;" value="<?php echo(get_value($documento, 'NUMERO_DOC'));?>" /></div>
		<br />
		<div class="form_label">*Qtde. Exemplares:</div>
		<div class="form_field"><input type="text" name="qtde_itens" id="qtde_itens" class="validate[required]" alt="integer" style="width:150px;" value="<?php echo(get_value($documento, 'QTDE_ITENS'));?>" /></div>
		<br />
		<div class="form_label">*Valor DOC:</div>
		<div class="form_field">
			<input type="text" name="valor_doc" id="valor_doc" class="validate[required]" style="width:150px;" alt="decimal" value="<?php echo(to_moeda(get_value($documento, 'VALOR_DOC')));?>" />
			<span class="comment" style="margin: 0 0 0 5px">Valor em reais do documento</span>
		</div>
		<br />
		<div class="form_label">*Arquivo:</div>
		<div class="form_field" style="padding:5px 0 0 0;width:445px;">
			<span><?php echo get_value($documento, 'ARQUIVO'); ?></span>
			<span style="margin:0 0 0 15px;"><a href="javascript:void(0);" onclick="show_area('arquivo')">Alterar</a></span>
			<br />
			<img src="<?php echo URL_IMG?>icon_bullet_plus.png" id="arquivo_img" style="display:none" />
			<input type="file" name="arquivo" id="arquivo" class="validate[required]" style="display:none;width:280px;" /><br />
			<span class="comment">Extensões suportadas: JPG, JPEG, PNG, GIF, BMP e PDF.</span><br />
			<span class="fnt_error font_09">Atenção! O arquivo a ser inserido referente a NF deve possuir no máximo 5MB. Caso o arquivo possua um tamanho maior, deve ser reduzido utilizando algum programa específico para tal. As informações devem estar legíveis.</span>
			<?php if($idusuariotipo == 2){?>
			<br />
			<br />
			<span class="fnt_error font_09">Se existe mais de um arquivo da NF, com o mesmo número da nota, somente a folha nº 1 deve ser inserida como NF, o restante dos arquivos será inserido como anexo dentro deste documento. Para listagens anexadas à nota fiscal, faça o mesmo procedimento. Para inserir anexos dentro da NF clique no ícone Anexos e insira os arquivos.</span><br />
			<?php } ?>
		</div>
		<div style="margin-top:30px">
			<hr />
			<div class="inline top"><input type="submit" value="Enviar" /></div>
			<div class="inline middle" style="padding:7px 0 0 5px">ou <a href="javascript:void(0)" onclick="$('#form_default').validationEngine('hide');parent.close_modal();">cancelar</a></div> 
		</div>
	</form>
</body>
</html>