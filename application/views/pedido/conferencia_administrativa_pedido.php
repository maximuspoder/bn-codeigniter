<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
</head>
<body>
	<?php monta_header(1); ?>
	<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
	<?php add_elementos_CONFIG(); ?>
	<div id="page_content">
		<div id="inside_content">
			<div>
				<div class="inline"><h1><a href="<?php echo URL_EXEC;?>pedido/search" class="black font_shadow_gray">Pedidos</a></h1></div>
				<div class="inline"><h3 class="font_shadow_gray">&nbsp;> Conferência Administrativa de Pedido</h3></div>
			</div>
			<br />
			<?php mensagem('note', '', 'Realize nesta tela a conferência de pedidos que já possuem pelo menos uma entrega realizada. Para cada pedido conferido, o valor de uma ou outra entrega pode ser liberado.');?>
			<br />
			<br />
			<div style="float:right;margin:-8px 0 0 0">
				<form id="form_filter_generic" action="<?php echo URL_EXEC?>pedido/conferencia_administrativa_pedido/" method="post">
				<span>Ordenado por:&nbsp;&nbsp;&nbsp;</span>
				<select name="filter_order_by" style="width:260px;" onchange="$('#form_filter_generic').submit();">
					<option value="TEM_RECURSO_LIBERADO" <?php echo get_value($filter, 'filter_order_by') == 'TEM_RECURSO_LIBERADO' ? 'selected="selected"' : '';?>>COM RECURSO AINDA NÃO LIBERADO</option>
					<option value="TEM_RECURSO_LIBERADO DESC" <?php echo get_value($filter, 'filter_order_by') == 'TEM_RECURSO_LIBERADO DESC' ? 'selected="selected"' : '';?>>COM RECURSO LIBERADO</option>
				</select>
				</form>
			</div>
			<div class="bold">Exibindo pedidos que possuam pelo menos uma entrega realizada pela Biblioteca.</div>
			<br />
			<br />
			<?php if(count($pedidos) > 0) {?>
			<table id="table_itens" class="table_sorter border_none no_shadow">
				<thead>
					<tr>
						<th style="border-left:1px solid #888;"></th>
						<th></th>
						<th>#</th>
						<th class="nowrap">Valor Total</th>
						<th>Biblioteca</th>
						<th>Ponto de Venda</th>
						<th style="border-right:1px solid #888;">Entregas</th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 0; ?>
					<?php foreach($pedidos as $pedido){?>
					<?php 
						$count = $pedido_model->count_observacoes_nao_lidas(get_value($pedido, 'IDPEDIDO'));
						$max_entregas_efetuadas = $this->pedido_model->get_max_entregas_finalizadas(get_value($pedido, 'IDPEDIDO'));
						$class = ($count > 0) ? 'steelblue' : 'odd';
						$class = (get_value($pedido, 'TEM_RECURSO_LIBERADO') > 0) ? 'conferido' : $class;
						$image = ($count > 0) ? 'icon_observacoes_novo.png' : 'icon_observacoes.png';
						$newob = ($count > 0) ? ' (Observações não lidas)' : '';
					?>
					<tr class="<?php echo $class;?>">
						<td class="center"  style="border-top:1px solid #888;border-bottom:1px solid #888;border-left:1px solid #888;"><img id="<?php echo get_value($pedido, 'IDPEDIDO'); ?>_img" src="<?php echo URL_IMG; ?><?php echo (get_value($pedido, 'TEM_RECURSO_LIBERADO') > 0) ? "icon_bullet_plus.png" : "icon_bullet_minus.png";?>" onclick="show_area('<?php echo get_value($pedido, 'IDPEDIDO'); ?>');" style="cursor:pointer;" /></td>
						<td class="center"  style="border-top:1px solid #888;border-bottom:1px solid #888;"><a href="javascript:void(0)" onclick="iframe_modal('Observações do Pedido', '<?php echo URL_EXEC; ?>pedido/modal_observacoes/<?php echo get_value($pedido, 'IDPEDIDO'); ?>', '<?php echo URL_IMG?>icon_observacoes.png', 600, 750);"><img src="<?php echo URL_IMG . $image; ?>" title="Observações do Pedido<?php echo $newob; ?>" /></a></td>
						<td class="center"  style="border-top:1px solid #888;border-bottom:1px solid #888;"><?php echo get_value($pedido, 'IDPEDIDO'); ?></td>
						<td class="right"   style="border-top:1px solid #888;border-bottom:1px solid #888;"><?php echo to_moeda(get_value($pedido, 'VALORTOTAL')); ?></td>
						<td class="font_10" style="border-top:1px solid #888;border-bottom:1px solid #888;"><a href="javascript:void(0);" onclick="ajax_modal('Visualizar Detalhes - Biblioteca', '<?php echo URL_EXEC; ?>biblioteca/modal_form_view_biblioteca/<?php echo(get_value($pedido, 'IDBIBLIOTECA'));?>', '<?php echo URL_IMG; ?>icon_detalhes.png', 600, 750);" title="Visualizar Detalhes"><?php echo get_value($pedido, 'BIBLIOTECA')?></a></td>
						<td class="font_10" style="border-top:1px solid #888;border-bottom:1px solid #888;"><a href="javascript:void(0);" onclick="ajax_modal('Visualizar Detalhes - Ponto de Venda', '<?php echo URL_EXEC; ?>pdv/modal_dados_pdv/<?php echo(get_value($pedido, 'IDPDV'));?>', '<?php echo URL_IMG; ?>icon_detalhes.png', 600, 750);" title="Visualizar Detalhes"><?php echo get_value($pedido, 'PDV')?></a></td>
						<td class="center"  style="border-top:1px solid #888;border-bottom:1px solid #888;border-right:1px solid #888"><?php echo get_value($pedido, 'MAX_QTDE_ENTREGAS')?></td>
					</tr>
					<tr id="<?php echo get_value($pedido, 'IDPEDIDO')?>" <?php echo (get_value($pedido, 'TEM_RECURSO_LIBERADO') > 0) ? 'style="display:none"' : "";?>>
					<td colspan="7" style="border-bottom:1px solid #999;border-top:1px solid #999;border-left:1px solid #999;border-right:1px solid #999;">
						<?php for($n = 1; $n <= $max_entregas_efetuadas; $n++){ ?>
						<?php $pedido_documentos  = $pedido_model->get_documentos_pedido(get_value($pedido, 'IDPEDIDO'), '', $n); ?>
						<?php ${"vlr_entrega_$n"} = $pedido_model->get_vlr_total_doc_entrega(get_value($pedido, 'IDPEDIDO'), $n); ?>
							<h6 style="margin:15px 0 0 0;"><?php echo $n ?>ª Entrega</h6>
							<?php if(count($pedido_documentos) > 0){?>
								<table class="table_sorter">
									<thead>
										<tr>
											<th></th>
											<th></th>
											<th></th>
											<th>#</th>
											<th>Tipo DOC</th>
											<th>Número DOC</th>
											<th>Exemplares</th>
											<th class="right" style="padding-right:7px">Valor DOC</th>
											<th>Inserido Por</th>
											<th></th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($pedido_documentos as $doc){ ?>
										<tr>
											<td style="width:01%"><a href="javascript:void(0);" onclick="iframe_modal('Anexos', '<?php echo URL_EXEC; ?>pedido_documento/modal_anexos_documento/<?php echo(get_value($doc, 'ID_PEDIDO_DOCUMENTO'))?>', '<?php echo URL_IMG?>icon_anexos.png', 600, 700);" ><img src="<?php echo(URL_IMG);?>icon_anexos.png" title="Anexos" /></a></td>
											<td style="width:01%"><a href="javascript:void(0);" onclick="iframe_modal('Editar', '<?php echo URL_EXEC; ?>pedido/modal_form_update_documento/<?php echo(get_value($doc, 'ID_PEDIDO_DOCUMENTO'))?>', '<?php echo URL_IMG?>icon_edit.png', 600, 700);" ><img src="<?php echo(URL_IMG);?>icon_edit.png" title="Editar" /></a></td>
											<td style="width:01%"><a href="javascript:void(0);" onclick="iframe_modal('Liberar Edição', '<?php echo URL_EXEC; ?>pedido_documento/modal_liberar_edicao_documento/<?php echo(get_value($doc, 'ID_PEDIDO_DOCUMENTO'))?>', '<?php echo URL_IMG?>icon_liberar_edicao.png', 600, 700);" ><img src="<?php echo(URL_IMG);?>icon_liberar_edicao.png" title="Liberar Edição" /></a></td>
											<td><?php echo(get_value($doc, '#'))?></td>
											<td><?php echo(get_value($doc, 'NOME'))?></td>
											<td><?php echo(get_value($doc, 'NUM. DOC'))?></td>
											<td><?php echo(get_value($doc, 'EXEMPLARES'))?></td>
											<td class="right"><?php echo(to_moeda(get_value($doc, 'VALOR DOC')))?></td>
											<td><?php echo((get_value($doc, 'IDUSUARIOTIPO') == 2) ? 'BIBLIOTECA' : 'PONTO DE VENDA')?></td>
											<td class="center"><img src="<?php echo URL_IMG?>icon_img_info.png" title="Nome do arquivo: <?php echo(get_value($doc, 'ARQUIVO'))?>" /></td>
											<td class="center"><a href="<?php echo URL_EXEC; ?>application/uploads/<?php echo(get_value($doc, 'ARQUIVO'));?>" target="_blank"><img src="<?php echo URL_IMG;?>icon_anexo_img.png" title="Clique para visualizar a Imagem" /></a></td>
										</tr>
										<?php } ?>
									</tbody>
								</table>
							<?php } ?>
						<?php } ?>
						
						<div style="margin-top:15px;height:10px;border-top:2px dotted #999;"></div>
						<div style="margin-left:3px;">
							<img src="<?php echo URL_EXEC ?>icon_bullet_plus.png" id="msg_saiba_mais_<?php echo get_value($pedido, 'IDPEDIDO')?>_img" style="display:none;" />
							<a class="font_11" href="javascript:void(0);" onclick="show_area('msg_saiba_mais_<?php echo get_value($pedido, 'IDPEDIDO')?>');">Saiba mais detalhes sobre a liberação de recursos</a>
						</div>
						<div style="display:none;" id="msg_saiba_mais_<?php echo get_value($pedido, 'IDPEDIDO')?>"><?php mensagem('warning', '', 'A liberação de recursos consiste em uma aprovação do recurso para que este seja emitido em um arquivo de créditos ao banco. A liberação só poderá ser alterável até que um arquivo de remessa de créditos seja emitido. Após isto, alterações em liberações não é possível (crédito já gerado).', false, 'margin-top:5px;') ?></div>
						<form action="" method="post" id="form_pedido_<?php echo get_value($pedido, 'IDPEDIDO')?>"  style="margin-top:10px;" onsubmit="return false">
							<input type="hidden" name="idpedido" id="idpedido" value="<?php echo get_value($pedido, 'IDPEDIDO')?>" />
							<?php for($n = 1; $n <= $max_entregas_efetuadas; $n++){ ?>
							<?php if(${"vlr_entrega_$n"} != 0) {?>
							<div style="height:30px;margin-left:3px;">
								<div class="inline top" style="margin:2px 3px 0 0;"><input type="checkbox" name="<?php echo $n ?>" id="<?php echo $n ?>" value="<?php echo ${"vlr_entrega_$n"}; ?>" <?php echo ($pedido_model->is_recurso_liberado(get_value($pedido, 'IDPEDIDO'), $n)) ? 'checked="checked"' : '';?> <?php echo ($pedido_model->is_credito_emitido(get_value($pedido, 'IDPEDIDO'), $n)) ? 'disabled="disabled"' : '';?> /></div>
								<div class="inline top">Liberar recurso no valor de R$ <?php echo to_moeda(${"vlr_entrega_$n"}) ?> referente a <?php echo $n ?>ª Entrega.</div>
							</div>
							<?php } } ?>
							<div class="inline top" style="margin:3px 0 5px 1px;"><button id="ajax_button_<?php echo get_value($pedido, 'IDPEDIDO')?>" onclick="ajax_save_conferencia_administrativa(<?php echo get_value($pedido, 'IDPEDIDO')?>);">Salvar</button></div>
							<div class="inline top" style="margin:10px 0 5px 5px;">
								<span id="ajax_loading_<?php echo get_value($pedido, 'IDPEDIDO')?>" style="display:none;"><img src="<?php echo URL_IMG?>ajax_loader_16.gif" /></span>
								<span style="display:none;" class="fnt_success" id="ajax_complete_<?php echo get_value($pedido, 'IDPEDIDO')?>">
									<img src="<?php echo URL_IMG ?>icon_success.png" class="inline top" style="margin:1px 2px 0 0;" />
									<div class="inline top font_11">Salvo com sucesso.</div>
								</span>
							</div>
						</form>
					</td>
					</tr>
					<tr><td colspan="8" style="height:50px;border:none;border-left:1px solid white;"></tr>
					<?php $i++; } ?>
				</tbody>
			</table>
			<?php } else { mensagem('warning', '', 'Pedido inválido. Provável que não exista nenhuma entrega já realizada para este pedido.'); } ?>
			<div style="margin-top:15px">
				<hr />
				<div class="inline top"><button onclick="window.location.href='<?php echo URL_EXEC;?>pedido/search'">OK</button></div>
				<div class="inline top" style="margin:8px 0 0 5px">ou <a href="<?php echo URL_EXEC;?>pedido/search">cancelar</a></div> 
			</div>
		</div>
	</div>
</body>
</html>