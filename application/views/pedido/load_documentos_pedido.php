<?php if(count($pedido_documentos) > 0){?>
<table id="table_documentos" class="table_sorter">
	<thead>
		<tr>
			<!-- DEBUG <th>#</th> -->
			<th></th>
			<th></th>
			<th></th>
			<th>Num. DOC</th>
			<th>Exemplares</th>
			<th>Valor DOC</th>
			<th>Número Entrega</th>
			<th>Inserido Por</th>
			<th></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php $i = 0;?>
		<?php foreach($pedido_documentos as $doc){ ?>
		<?php $class = ($i % 2 == 0) ? 'class="odd"' : '';?>
			<tr <?php echo($class);?>>
				<!-- DEBUG <td><?php echo(get_value($doc, 'ID_PEDIDO_DOCUMENTO'))?></td> -->
				<td style="width:01%">
					<?php if((get_value($doc, 'IDUSUARIOTIPO') == $tipo_usuario && get_value($doc, 'NUMERO_ENTREGA') == 0) || (get_value($doc, 'IDUSUARIOTIPO') == $tipo_usuario && get_value($doc, 'LIBERAR_EDICAO') == 'S')){ ?>
					<a href="javascript:void(0);" onclick="iframe_modal('Deletar Documento', '<?php echo URL_EXEC; ?>pedido/modal_deletar_documento/<?php echo(get_value($doc, 'ID_PEDIDO_DOCUMENTO'))?>/<?php echo($idpedido);?>', '<?php echo URL_IMG?>icon_delete.png', 260, 600);" ><img src="<?php echo(URL_IMG);?>icon_delete.png" title="Deletar" /></a>
					<?php } else { ?>
					<img src="<?php echo(URL_IMG);?>icon_delete_off.png" title="Deletar" />
					<?php } ?>
				</td>
				<td style="width:01%">
					<?php if((get_value($doc, 'IDUSUARIOTIPO') == $tipo_usuario && get_value($doc, 'NUMERO_ENTREGA') == 0) || (get_value($doc, 'IDUSUARIOTIPO') == $tipo_usuario && get_value($doc, 'LIBERAR_EDICAO') == 'S')){ ?>
					<a href="javascript:void(0);" onclick="iframe_modal('Editar <?php echo((get_value($doc, 'ID_PEDIDO_DOCUMENTO_TIPO') == 1) ? 'Nota Fiscal' : 'Comprovante')?>', '<?php echo URL_EXEC; ?>pedido/modal_form_update_documento/<?php echo(get_value($doc, 'ID_PEDIDO_DOCUMENTO'))?>', '<?php echo URL_IMG?>icon_edit.png', 600, 700);" ><img src="<?php echo(URL_IMG);?>icon_edit.png" title="Editar" /></a>
					<?php } else { ?>
					<img src="<?php echo(URL_IMG);?>icon_edit_off.png" title="Editar" />
					<?php } ?>
				</td>
				<td style="width:01%"><a href="javascript:void(0);" onclick="iframe_modal('Anexos', '<?php echo URL_EXEC; ?>pedido_documento/modal_anexos_documento/<?php echo(get_value($doc, 'ID_PEDIDO_DOCUMENTO'))?>', '<?php echo URL_IMG?>icon_anexos.png', 600, 700);" ><img src="<?php echo(URL_IMG);?>icon_anexos.png" title="Anexos" /></a></td>
				<td style="width:10%"><?php echo(get_value($doc, 'NUMERO_DOC'))?></td>
				<td style="width:10%"><?php echo(get_value($doc, 'QTDE_ITENS'))?></td>
				<td style="width:10%"><?php echo(to_moeda(get_value($doc, 'VALOR_DOC')))?></td>
				<td style="width:20%">
				<?php if(get_value($doc, 'NUMERO_ENTREGA') == 0){ ?>
					<?php if(get_value($doc, 'IDUSUARIOTIPO') == $tipo_usuario){ ?>
						<span class="italic fnt_error">Entrega ainda não finalizada</span>
					<?php } else { ?>
						<span class="italic fnt_error">Entrega ainda não finalizada <?php echo((get_value($doc, 'IDUSUARIOTIPO') == 2) ? 'pela biblioteca' : 'pelo ponto de venda');?></span>
					<?php } ?>
				<?php } else { echo get_value($doc, 'NUMERO_ENTREGA') . 'ª Entrega'; } ?>
				</td>
				<td><?php echo((get_value($doc, 'IDUSUARIOTIPO') == 2) ? 'BIBLIOTECA' : 'PONTO DE VENDA')?></td>
				<td style="width:01%" class="center"><img src="<?php echo URL_IMG;?>icon_img_info.png" title="<?php echo(get_value($doc, 'NOME'));?>" /></td>
				<td style="width:01%" class="center"><a href="<?php echo URL_EXEC; ?>application/uploads/<?php echo(get_value($doc, 'ARQUIVO'));?>" target="_blank"><img src="<?php echo URL_IMG;?>icon_anexo_img.png" title="Clique para visualizar a Imagem" /></a></td>
			</tr>	
		<?php $i++; } ?>		
	</tbody>
</table>
<?php } else { mensagem('note', '', 'Nenhum documento deste tipo ainda foi cadastrado.'); } ?>
