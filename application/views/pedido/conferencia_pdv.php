<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			// Carrega ajax que faz load na tabela de documentos
			ajax_load_documentos(<?php echo(get_value($pedido, 'IDPEDIDO'));?>);
			
			// Carrega ajax que faz load na tabela de documentos
			ajax_load_itens_pedido(<?php echo(get_value($pedido, 'IDPEDIDO'));?>);
			
			$('input:text').setMask();
			// Variável de total para a tabela de itens
			// var total = $("#input_subtotal_geral").val();
			// alert(total);
		});
		
		// Recalcula o valor total para rodape, conforme vlr qtd_entregue
		// preenchido. roda no onblur de cada campo qte_entregue
		function recalcula_vlr_total(idlivro)
		{
			total = 0;
			var total_geral = $("#input_subtotal_geral").val();
			var qtde_entregue = parseInt($('#qtd_entregue_' + idlivro).val());
			var qtde_pedida   = parseInt($('#qtd_entregue_' + idlivro).attr('title'));
			
			// Seta o subtotal por linha
			if(qtde_entregue <= qtde_pedida && qtde_entregue > 0)
			{
				// Calcula o subtotal da linha do livro
				var subtotal = $('#input_preco_unit_' + idlivro).val() * $('#qtd_entregue_' + idlivro).val();
				
				// Calcula o total geral dos itens
				if(parseFloat($('#input_subtotal_' + idlivro).val()) >= parseFloat(subtotal))
				{
					diferenca = parseFloat($('#input_subtotal_' + idlivro).val()) - parseFloat(subtotal);
					total_geral = parseFloat(total_geral) - parseFloat(diferenca);
				} else {
					diferenca = parseFloat(subtotal) - parseFloat($('#input_subtotal_' + idlivro).val());
					total_geral = parseFloat(total_geral) + parseFloat(diferenca);
				}
				
				$('#input_subtotal_' + idlivro).val(subtotal);
				$('#subtotal_' + idlivro).html(to_moeda(subtotal) + '<input type="hidden" id="input_subtotal_' + idlivro + '" value="' + subtotal + '" />');
				
				// Varre todos os inputs, somando o valor e adiciona o option do doc gerado
				$('#form_itens_conferencia div > input').each(function() {
					if($(this).attr("type") == "hidden")
					{
						// alert($(this).val());
						// alert($(this).attr('id'));
						total = parseFloat(total) + parseFloat($(this).val());
					}
				});
				
				// Debug
				// alert(to_moeda(total));
				
				// Total geral, rodapé
				$('#subtotal_itens').html('Total: R$ ' + to_moeda(total) + '<input type="hidden" id="input_subtotal_itens" value="' + total +'" />');
				$('#subtotal_geral').html('Total Geral dos Itens da Nota: R$ ' + to_moeda(total_geral) + '<input type="hidden" id="input_subtotal_geral" value="' + total_geral +'" />');
			} else {
				alert("ATENÇÃO!\n\nA quantidade entregue não pode ser maior do que a quantidade solicitada.");
				$('#qtd_entregue_' + idlivro).val($('#qtd_entregue_' + idlivro).attr('lang'));
				$('#qtd_entregue_' + idlivro).focus();
			}
		}
	</script>
</head>
<body>
	<?php monta_header(1); ?>
	<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
	<?php add_elementos_CONFIG(); ?>
	<div id="page_content">
		<div id="inside_content">
			<div>
				<div class="inline"><h1><a href="<?php echo URL_EXEC;?>pedido/lista_pedidos_pdv" class="black font_shadow_gray">Meus Pedidos</a></h1></div>
				<div class="inline"><h3 class="font_shadow_gray">&nbsp;> Conferência de Pedido</h3></div>
				<div style="float:right;margin:3px 0 0 0;">
					<div class="inline top" style="margin:6px 0 0 0;" ><img src="<?php echo URL_IMG ?>icon_help.png" /></div>
					<h4  class="inline"><a href="javascript:void(0);" onclick="ajax_modal('Informações sobre a Conferência', '<?php echo URL_EXEC ?>comunicacao/informacoes_conferencia_pdv', '<?php echo URL_IMG ?>icon_help.png', 600, 700);">Ajuda para Conferência</a></h4>
				</div>
			</div>
			<br />
			<?php if(get_value($pedido, 'IDPEDIDOSTATUS') >= 3){ ?>
			<br />
			<img class="inline" id="box_group_view_img" src="<?php echo(URL_IMG)?>icon_bullet_minus.png" onclick="show_area('box_group_view');" style="float:left;cursor:pointer;margin:5px 5px 0 0;" />
			<div class="font_shadow_gray" style="margin-bottom:5px;"><h4 style="cursor:pointer" onclick="show_area('box_group_view')">Dados do Pedido</h4></div>
			<hr />
			<div id="box_group_view" style="display:block;">
				<div class="odd">
					<div id="label_view">ID Pedido:</div>
					<div id="field_view"><?php echo(get_value($pedido, 'IDPEDIDO'));?></div>
				</div>
				<div>
					<div id="label_view">Situação:</div>
					<div id="field_view"><?php echo(get_value($pedido, 'DESCSTATUS'));?></div>
				</div>
				<div class="odd">
					<div id="label_view">Valor:</div>
					<div id="field_view">R$ <?php echo(to_moeda(get_value($pedido, 'VALORTOTAL')));?></div>
				</div>
				<div>
					<div id="label_view">Qtde. Total Itens:</div>
					<div id="field_view"><?php echo($pedido_itens_count);?></div>
				</div>
				<div class="odd">
					<div id="label_view">Biblioteca:</div>
					<div id="field_view"><?php echo(get_value($pedido, 'BIBLIOTECA'));?>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="ajax_modal('Visualizar Detalhes - Biblioteca', '<?php echo URL_EXEC; ?>biblioteca/modal_form_view_biblioteca/<?php echo(get_value($pedido, 'IDBIBLIOTECA'));?>', '<?php echo URL_IMG; ?>icon_detalhes.png', 750, 750);">Visualizar detalhes</a></div>
				</div>
				<div>
					<div id="label_view">Ponto de Venda:</div>
					<div id="field_view">
						<?php if(get_value($pedido, 'PDV') == '') {?>
							<div class="comment italic">Nenhum Ponto de Venda parceiro</div>
						<?php } else { ?>
							<?php echo(get_value($pedido, 'PDV'));?>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="ajax_modal('Visualizar Detalhes - Biblioteca', '<?php echo URL_EXEC; ?>pdv/modal_dados_pdv/<?php echo(get_value($pedido, 'IDPDV'));?>', '<?php echo URL_IMG; ?>icon_detalhes.png', 750, 750);">Visualizar detalhes</a>
						<?php } ?>
					</div>
				</div>
				<div class="odd">
					<div id="label_view">Entregas finalizadas pela Biblioteca:</div>
					<div id="field_view"><?php echo $pedido_model->get_quantidade_entregas(get_value($pedido, 'IDPEDIDO'), 2); ?></div>
				</div>
				<div>
					<div id="label_view">Entregas finalizadas pelo Ponto de Venda:</div>
					<div id="field_view"><?php echo $pedido_model->get_quantidade_entregas(get_value($pedido, 'IDPEDIDO'), 4);?></div>
				</div>
			</div>
			
			
			<br />
			<br />
			<br />
			<div style="margin-bottom:5px;">
				<img class="inline" id="box_entregas_img" src="<?php echo(URL_IMG)?>icon_bullet_minus.png" onclick="show_area('box_entregas');" style="float:left;cursor:pointer;margin:5px 5px 0 0;" />
				<div class="inline font_shadow_gray"><h4 style="cursor:pointer" onclick="show_area('box_entregas')">Quantidade de Entregas</h4></div>
				<div>Neste bloco você encontra informações referentes às quantidades de entregas que poderá efetuar.</div>
			</div>
			<hr />
			<div id="box_entregas" style="margin-top:5px;display:block;">
				<div style="line-height:23px;">
				A entrega dos itens do pedido pode ser feita das seguintes maneiras:<br />
				&bull;&nbsp;Uma única entrega, excluindo-se os inexigíveis.<br />
				&bull;&nbsp;Duas entregas, com a primeira entrega contemplando o mínimo de <?php echo (get_value($pedido, 'PERCENTUAL_ATENDIMENTO') * 100)?>% do total de exemplares obrigatórios (excluindo-se os inexigíveis. Faça o download da lista de inexigíveis em sua página inicial de trabalho).<br />
				<?php if(get_value($pedido, 'TIPO_ATENDIMENTO') == 1){?>
				&bull;&nbsp;<?php echo (get_value($pedido, 'PERCENTUAL_ATENDIMENTO') * 100)?>% do valor do total de títulos obrigatórios: <strong><u>R$ <?php echo to_moeda($vlr_total)?></u></strong>
				<?php } else { ?>
				&bull;&nbsp;<?php echo (get_value($pedido, 'PERCENTUAL_ATENDIMENTO') * 100)?>% do valor do total de títulos do pedido: <strong><u>R$ <?php echo to_moeda($vlr_total);?></u></strong>
				<?php } ?>
				</div>
				<div style="margin-top:10px">
					<strong>Atenção!</strong> A conferência pelo PDV foi atualizada.<br />
					Depois de inserir os comprovantes referentes à 1ª entrega, não esqueça de clicar em "Finalizar entrega" para confirmar o envio dos comprovantes inseridos. Faça o mesmo depois que inserir os comprovantes da 2ª entrega.
				</div>
			</div>
			
			
			<br />
			<br />
			<br />
			<div style="margin-bottom:5px;">
				<img class="inline" id="box_documentos_img" src="<?php echo(URL_IMG)?>icon_bullet_minus.png" onclick="show_area('box_documentos');" style="float:left;cursor:pointer;margin:5px 5px 0 0;" />
				<div class="inline font_shadow_gray"><h4 style="cursor:pointer" onclick="show_area('box_documentos')">Comprovantes de Entrega</h4></div>
				<?php if(($entrega_atual <= get_value($pedido, 'MAX_QTDE_ENTREGAS')) || get_value($pedido, 'LIBERAR_INS_DOCUMENTO') == 'S'){?>
					<div class="inline">&nbsp;&nbsp;<a href="javascript:void(0);" onclick="iframe_modal('Novo Comprovante', '<?php echo URL_EXEC; ?>pedido/modal_inserir_documento/<?php echo(get_value($pedido, 'IDPEDIDO'));?>', '<?php echo URL_IMG?>icon_inserir_documento.png', 580, 630);">Inserir comprovante</a></div>
				<?php } ?>
				<div>Visualize aqui os comprovantes de entrega cadastrados, por entrega.</div>
			</div>
			<hr />
			<div id="box_documentos" style="margin-top:5px;display:block;"><!-- Carregado via ajax no onload da página --></div>
			
			
			<br />
			<br />
			<br />
			<div style="margin-bottom:5px;">
				<img class="inline" id="box_notas_img" src="<?php echo(URL_IMG)?>icon_bullet_plus.png" onclick="show_area('box_notas');" style="float:left;cursor:pointer;margin:5px 5px 0 0;" />
				<div class="inline font_shadow_gray"><h4 style="cursor:pointer" onclick="show_area('box_notas')">Notas Fiscais</h4></div>
				<div>Visualize aqui as notas fiscais cadastradas pela biblioteca até o momento.</div>
			</div>
			<hr />
			<div id="box_notas" style="margin-top:5px;display:none;"><!-- Carregado via ajax no onload da página --></div>
	
	
			<br />
			<br />
			<br />
			<img class="inline" id="box_itens_img" src="<?php echo(URL_IMG)?>icon_bullet_plus.png" onclick="show_area('box_itens');" style="float:left;cursor:pointer;margin:5px 5px 0 0;" />
			<div class="inline font_shadow_gray" style="margin-bottom:5px;"><h4 style="cursor:pointer" onclick="show_area('box_itens')">Itens do Pedido</h4></div>
			<?php if($itens_nao_conferidos_count <= 50 && $itens_nao_conferidos_count > 0 && $pedido_itens_count >= 50){ ?><div class="inline">&nbsp;&nbsp;<a href="javascript:void(0);" onclick="ajax_modal('Títulos Não Conferidos', '<?php echo URL_EXEC; ?>pedido/modal_titulos_nao_conferidos/<?php echo(get_value($pedido, 'IDPEDIDO'));?>', '<?php echo URL_IMG?>icon_livros.png', 400, 600);">Existem <?php echo $itens_nao_conferidos_count; ?> títulos não conferidos</a></div><?php } ?>
			<div>Visualize neste bloco como anda a conferência dos itens do pedido realizada pela biblioteca.</div>
			<hr />
			<div id="box_itens" style="margin-top:15px;display:none">
				<div style="width:100%;">
					<div class="fnt_error italic" style="margin:15px 0;">Digite uma parte do título para buscar um livro ou deixe em branco e clique em enviar para visualizar todo o pedido.</div>
					<div class="font_11 bold inline left" style="margin:-5px 15px 0 0;">Título:</div>
					<input type="text" name="filter_titulo" id="filter_titulo" class="mini" style="width:250px;margin:0 15px 0 0;" onkeyup="get_enter(event, '$(\'#filter_enter\').click();');" />
					<button id="filter_enter" class="mini" onclick="ajax_load_itens_pedido(<?php echo(get_value($pedido, 'IDPEDIDO'));?>, ($('#filter_titulo').val() == '' ? 'true' : $('#filter_titulo').val()), 'pdv');">Enviar</button><br /><br />
				</div>
				<div id="box_table_itens"><!-- Aqui fica a tabela que é Carregada via ajax no onload da página --></div>
			</div>
			
			
			<div class="middle" style="margin:30px 0 10px 0;">
				<hr />
				<div class="inline top" style="margin:10px 5px 0 0;"><button onclick="ajax_finaliza_conferencia_aceite(<?php echo(get_value($pedido, 'IDPEDIDO'));?>, <?php echo $tipo_usuario;?>, <?php echo $entrega_atual;?>);" <?php echo(($entrega_atual > get_value($pedido, 'MAX_QTDE_ENTREGAS')) ? 'disabled="disabled"' : '');?>>Finalizar Entrega</button></div>
				<div class="inline top" style="margin:18px 0 0 0;">ou&nbsp;&nbsp;<a href="<?php echo URL_EXEC;?>pedido/lista_pedidos_pdv">voltar</a></div>
			</div>
			<?php } elseif(get_value($pedido, 'IDPEDIDOSTATUS') == 2) { mensagem('warning', 'Atenção', 'Este pedido está com situação ENTREGUE AO PONTO DE VENDA. Você deve confirmar o recebimento clicando no botão abaixo para continuar com a conferência deste pedido.'); ?>
			<form action="<?php echo URL_EXEC?>pedido/set_pedido_recebido_pdv" method="post">
			<input type="hidden" value="<?php echo get_value($pedido, 'IDPEDIDO'); ?>" name="idpedido" />
			<input type="hidden" value="3" name="idpedidostatus" />
				<div style="margin-top:30px">
					<hr />
					<div class="inline top"><input type="submit" value="Ok, Recebi este Pedido" /></div>
					<div class="inline middle" style="padding:7px 0 0 5px">ou <a href="<?php echo URL_EXEC;?>pedido/lista_pedidos_pdv">cancelar</a></div> 
				</div>
			</form>
			<?php } else { mensagem('warning', 'Atenção', 'Este pedido não está com situação aplicável à conferência.'); }?>
		</div>
	</div>
</body>
</html>