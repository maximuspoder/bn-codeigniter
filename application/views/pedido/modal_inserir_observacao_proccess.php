<?php add_elementos_CONFIG(); ?>
<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
<script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
<script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
	$(document).ready(function(){
		<?php if($error != ''){?>
			alert("ATENÇÃO!\n<?php echo $error;?>");
		<?php } else { ?>
			// Carrega ajax que faz load na tabela de documentos
			parent.ajax_load_documentos(<?php echo($idpedido);?>);
			
			parent.add_option_documento('<?php echo(get_value($option, 'numero_doc'));?>','<?php echo(get_value($option, 'id_pedido_documento'));?>');
		
			// Coleta o dialog do parent anterior
			parent.close_modal();
		<?php }?>
	});
</script>