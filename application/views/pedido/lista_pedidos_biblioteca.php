<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.tablesorter.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			// Validação e máscaras
			$("input:text").setMask();
		});
	</script>
</head>
<body>
	<?php monta_header(1); ?>
	<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
	<?php add_elementos_CONFIG(); ?>
	<div id="page_content">
		<div id="inside_content">
			<h1 class="font_shadow_gray inline top" style="margin:10px 15px 0 0;">Meus Pedidos</h1>
			<div id="module_menu" class="inline top">
				<!--
				<div class="inline">
					<img src="<?php echo URL_IMG;?>icon_excel.gif" style="margin-top:2px;" />
					<a href="javascript:void(0);" onclick="download_excel('<?php echo URL_EXEC?>pedido/download_excel_lista_itens_pedidos_total_biblioteca/<?php echo $idusuario; ?>');" class="black inline" title="Exporte a lista de todos os itens de todos os pedidos deste Ponto de Venda"><h5>Exportar itens de pedidos</h5></a>
				</div>
				-->
			</div>
			<br />
			<?php mensagem('info', '', 'Você está visualizando a lista de pedidos realizados por sua biblioteca. Gerencie-os utilizando os ícones de ação, à esquerda.', false, 'margin:15px 0 30px 0;'); ?>
			<form id="form_filter" action="<?php echo(URL_EXEC);?>pedido/lista_pedidos_biblioteca" method="post">
				<div id="content">
					<div class="inline">
						<img src="<?php echo URL_IMG;?>icon_filter.png" style="float:left;margin:4px 5px 0 0" />
						<h5 class="black inline" style="margin:3px 0 0 0;">Filtros</h5>
					</div>
					<div id="label">ID(#): </div>
					<div id="field"><input type="text" name="p__idpedido" id="p__idpedido" alt="integer" class="mini" value="<?php echo get_value($filtros, 'p__idpedido');?>" style="width:50px" /></div>
					<div id="label">Situação: </div>
					<div id="field">
						<select class="mini"  name="p__idpedidostatus" id="p__idpedidostatus" style="width:150px;"><?php echo $option_status; ?></select>
					</div>
					<div id="label">PDV: </div>
					<div id="field"><input type="text" name="cp__razaosocial" id="cp__razaosocial" alt="" class="mini" value="<?php echo get_value($filtros, 'cp__razaosocial');?>" style="width:150px" /></div>
					<div id="label"></div>
					<div id="field"><input type="submit" class="mini" value="Enviar" /></div>
				</div>				
			</form>
			<?php echo($module_table); ?>
		</div>
	</div>
</body>
</html>