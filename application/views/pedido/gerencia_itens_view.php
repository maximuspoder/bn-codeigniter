<table cellspacing="0" align="center" cellpadding="0" class="extensions t100">
	<tbody>
		{livros}
		<tr class="{CORLINHA}">
			<td class="no-wrap tdcenter" style="width:40px;">{NLINHA}</td>
			<td class="no-wrap tdcenter" style="width:90px;">{ISBN}</td>
			<td class="pr_20 tdleft" style="padding-top:7px;padding-bottom:7px;">{DADOSLIVRO}</td>
			<td class="no-wrap tdcenter" style="width:70px;">{QTD_ENTREGUE}</td>
			<td class="no-wrap tdcenter" style="width:80px;">R$ {PRECO_UNIT}</td>
			<td class="no-wrap tdcenter" style="width:80px;">R$ {SUBTOTAL}</td>
			<td class="no-wrap tdcenter" style="width:70px;">
				<?php
				if ($tipoUser == 2 && $statusPed == 1)
				{
					?>
					<a href="javascript:void(0);" class="red" onclick="showLivro({IDLIVRO});">Visualizar</a><br />
					<a href="javascript:void(0);" class="red" onclick="modalAddPedidoItem({IDLIVRO},2);">Editar</a><br />
					<a href="javascript:void(0);" class="red" onclick="modalRemovePedidoItem({IDLIVRO},2);">Excluir</a>
					<?php
				}
				elseif ($tipoUser == 2 && $statusPed == 4)
				{
					?>
					<a href="javascript:void(0);" class="red" onclick="modalAjustePedidoItem({IDLIVRO});">Editar <br />quantidade <br />recebida</a>
					<?php
				}
				?>
			</td>
		</tr>
		{/livros}
		
		<?php
		if ($vazio == TRUE)
		{
			?>
			<tr>
				<td colspan="7" class="no-wrap tdcenter">Nenhum item encontrado.</td>
			</tr>
			<?php 
		}
		?>
	</tbody>
</table>