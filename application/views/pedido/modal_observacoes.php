<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.tablesorter.js" type="text/javascript"></script>
</head>
<body>
	<?php add_elementos_CONFIG(); ?>
	<div style="margin-bottom:5px;">
		<div class="inline font_shadow_gray"><h4>Observações do Pedido</h4></div>
		<div class="inline">&nbsp;&nbsp;<a href="<?php echo URL_EXEC?>pedido/modal_inserir_observacao/<?php echo $idpedido; ?>">Inserir observação</a></div>
		<div>Insira e visualize aqui observações referentes ao pedido selecionado. A primeira coluna da listagem de observações indica se a observação foi lida ou não.</div>
	</div>
	<hr />
	<form name="form_default" id="form_default" action="<?php echo URL_EXEC?>pedido/modal_marcar_observacao_lida/" method="post">
	<input type="hidden" name="idpedido" value="<?php echo $idpedido; ?>" />
	<?php echo $module_table; ?>
	<?php if(count($observacoes) > 0){?>
	<br />
	<input type="submit" value="Marcar como Lida(s)" />
	<?php } ?>
	</form>
</body>
</html>