<script language="javascript">
	$("quantidade").focus();
</script>
<div id="container" style="overflow-y: auto; ">
    <div class="main_content_left">
        <form name="form_cad" id="form_cad" method="post" action="javascript:if(validaForm_updatePedidoItem()){ submitUpdatePedidoItem(<?php echo($idLivro)?>, $F('quantidade')); }">
            <fieldset style="width:500px; margin-left:20px;">
                <legend><b>Alterar Quantidade Selecionada</b></legend>
                <div id="fs_div">
                    <table id="tabelaCadastros2" align="center" border="0">
                        <tr>
                            <td class="bold">Livro:</td>
                            <td><?php echo($titulo);?></td>
                        </tr>
						<tr>
                            <td class="bold">Autor:</td>
                            <td><?php echo($autor);?></td>
                        </tr>
						<tr>
                            <td class="bold">Editora:</td>
                            <td><?php echo($editora);?> (<?php echo(($edicao != '') ? $edicao . 'ª Edição, ' : '');?><?php echo($ano);?>)</td>
                        </tr>
						<tr>
                            <td class="bold">Preço:</td>
                            <td>R$ <?php echo(masc_real($preco));?></td>
                        </tr>
                        <tr>
                            <td class="bold">Quantidade Atual*:</td>
                            <td><input class="inputText" type="text" name="quantidade" id="quantidade" style="width:70px;" maxlength="10" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="<?php echo($qtd);?>" autocomplete="off" onkeypress="return maskInteger(this, event)" /></td>
                        </tr>
                        <tr>
                            <td colspan="4" style="text-align: center; padding-top:15px;">
                                <input class="buttonPadrao" type="button" value="  Cancelar  " onclick="$('ligthbox_bg').setStyle({display : 'none'});" />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input class="buttonPadrao" id="btnSaveForm" name="btnSaveForm" type="button" value="  Salvar  " onclick="if(validaForm_updatePedidoItem()){ submitUpdatePedidoItem(<?php echo($idLivro)?>, $F('quantidade')); }"/>
                            </td>
                        </tr>
                        <tr><td colspan="4" style="text-align: center; padding-top:5px; font-size:11px;">(*) Campos de preenchimento obrigatório.</td></tr>
                        
                    </table>
                </div>
            </fieldset>
        </form>
    </div>
</div><!-- #container-->

