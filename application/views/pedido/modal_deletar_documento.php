<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
</head>
<body>
	<div><?php mensagem('warning', 'Atenção', 'O registro de documento de ID <strong>' . $iddocumento . '</strong> será deletado. Todos os itens do pedido que já foram conferidos para este documento / nota serão desmarcados também e você terá de refazê-los posteriormente. Para continuar clique em ok, para sair clique em cancelar.');?>.</div>
	<form action="<?php echo URL_EXEC; ?>pedido/modal_deletar_documento_proccess" name="form_default" id="form_default" enctype="multipart/form-data" method="post">
		<input type="hidden" name="id_pedido_documento" id="id_pedido_documento" value="<?php echo($iddocumento);?>" />
		<input type="hidden" name="id_pedido" id="id_pedido" value="<?php echo($idpedido);?>" />
		<div style="margin-top:-5px">
			<hr />
			<div class="inline top"><input type="submit" value="OK" /></div>
			<div class="inline middle" style="padding:7px 0 0 5px">ou <a href="javascript:void(0)" onclick="parent.close_modal();">cancelar</a></div> 
		</div>
	</form>
</body>
</html>