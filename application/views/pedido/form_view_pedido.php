<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			// Validação e máscaras
			$("#form_default").validationEngine({ inlineValidation:false , promptPosition : "centerRight", scroll : false });
			$("input:text").setMask();
		});
	</script>
</head>
<body>
	<?php monta_header(1); ?>
	<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
	<?php add_elementos_CONFIG(); ?>
	<div id="page_content">
		<div id="inside_content">
			<div>
				<div class="inline"><h1><a href="<?php echo URL_EXEC . $url_redirect;?>" class="black font_shadow_gray">Pedidos</a></h1></div>
				<div class="inline"><h3 class="font_shadow_gray">&nbsp;> Visualização de Pedido</h3></div>
			</div>
			<br />
			<br />
			<h4 class="font_shadow_gray">Dados do Pedido</h4>
			<hr />
			<div id="box_group_view" style="display:block;">
				<div class="odd">
					<div id="label_view">ID Pedido (#):</div>
					<div id="field_view"><?php echo(get_value($dados, 'IDPEDIDO'));?></div>
				</div>
				<div>
					<div id="label_view">Situação:</div>
					<div id="field_view"><?php echo(get_value($dados, 'DESCSTATUS'));?></div>
				</div>
				<div class="odd">
					<div id="label_view">Valor:</div>
					<div id="field_view">R$ <?php echo(to_moeda(get_value($dados, 'VALORTOTAL')));?></div>
				</div>
				<div>
					<div id="label_view">Qtde. Total Itens:</div>
					<div id="field_view"><?php echo($pedido_itens_count);?></div>
				</div>
				<div class="odd">
					<div id="label_view">Biblioteca:</div>
					<div id="field_view"><?php echo(get_value($dados, 'BIBLIOTECA'));?>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="ajax_modal('Visualizar Detalhes', '<?php echo URL_EXEC; ?>biblioteca/modal_form_view_biblioteca/<?php echo(get_value($dados, 'IDBIBLIOTECA'));?>', '<?php echo URL_IMG; ?>icon_detalhes.png', 750, 750);">Visualizar detalhes</a></div>
				</div>
				<div>
					<div id="label_view">Ponto de Venda:</div>
					<div id="field_view">
						<?php if(get_value($dados, 'PDV') == '') {?>
							<div class="comment italic">Nenhum Ponto de Venda parceiro</div>
						<?php } else { ?>
							<?php echo(get_value($dados, 'PDV'));?>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="ajax_modal('Visualizar Detalhes', '<?php echo URL_EXEC; ?>pdv/modal_dados_pdv/<?php echo(get_value($dados, 'IDPDV'));?>', '<?php echo URL_IMG; ?>icon_detalhes.png', 750, 750);">Visualizar detalhes</a>
						<?php } ?>
					</div>
				</div>
				<div class="odd">
					<div id="label_view" style="width:160px">Qtde. de entregas informadas como finalizadas pela Biblioteca:</div>
					<div id="field_view"><?php echo $pedido_model->get_quantidade_entregas(get_value($dados, 'IDPEDIDO'), 2); ?></div>
				</div>
				<div>
					<div id="label_view" style="width:160px">Qtde. de entregas informadas como finalizadas pelo Ponto de Venda:</div>
					<div id="field_view"><?php echo $pedido_model->get_quantidade_entregas(get_value($dados, 'IDPEDIDO'), 4);?></div>
				</div>
			</div>
			
			
			<h4 class="font_shadow_gray" style="margin-top:35px;">Documentos do Pedido</h4>
			<hr />
			<?php echo $table_documentos; ?>
			
			
			<img src="<?php echo URL_IMG?>icon_bullet_plus.png" id="box_itens_pedido_img" style="display:none" />
			<div onclick="show_area('box_itens_pedido')" style="cursor:pointer;margin-top:35px;"><h4 class="inline top font_shadow_gray">Itens do Pedido (<?php echo $pedido_titulos_count; ?> títulos)</h4><div class="inline top comment italic" style="margin:5px 0 0 5px;">Clique para expandir</div></div>
			<hr />
			<div id="box_itens_pedido" style="display:none;">
			<?php echo $table_itens_pedido; ?>
			</div>
			
			
			<div style="margin-top:35px">
				<hr />
				<div class="inline top"><button onclick="redirect('<?php echo URL_EXEC . $url_redirect; ?>');">OK</button></div>&nbsp;&nbsp;&nbsp;&nbsp;
				<div class="inline top" style="padding:8px 0 0 5px">ou&nbsp;&nbsp;&nbsp;<a href="<?php echo URL_EXEC . $url_redirect; ?>">voltar</a></div> 
			</div>
		</div>
	</div>
</body>
</html>