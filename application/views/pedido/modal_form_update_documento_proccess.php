<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			<?php if($error == ''){?>
				// Carrega ajax que faz load na tabela de documentos
				parent.ajax_load_documentos(<?php echo($idpedido);?>);
				
				parent.add_option_documento('<?php echo(get_value($option, 'numero_doc'));?>','<?php echo(get_value($option, 'id_pedido_documento'));?>');
			
				// Coleta o dialog do parent anterior
				parent.close_modal();
			<?php }?>
		});
	</script>
</head>
<body>
	<?php add_elementos_CONFIG(); ?>
	<?php if($error != ""){ ?>
	<?php mensagem('warning', 'Atenção', $error); ?>
	<div style="margin-top:30px">
		<hr />
		<div class="inline top"><button onclick="redirect('<?php echo URL_EXEC;?>pedido/modal_form_update_documento/<?php echo $id_pedido_documento;?>')">OK</button>
		<div class="inline middle" style="padding:7px 0 0 5px">ou <a href="javascript:void(0)" onclick="redirect('<?php echo URL_EXEC;?>pedido/modal_form_update_documento/<?php echo $idpedido;?>')">cancelar</a></div> 
	</div>
	<?php } ?>
</body>
</html>