<?php add_elementos_CONFIG(); ?>
<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
<script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
<script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript"></script>
<script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript"></script>
<script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
	$(document).ready(function(){
		// Carrega ajax que faz load na tabela de documentos
		parent.ajax_load_documentos(<?php echo($idpedido);?>);
		
		// Destr�i option da nota atual
		parent.del_option_documento(<?php echo($id_pedido_documento);?>);
		
		// Coleta o dialog do parent anterior
		parent.close_modal();
	});
</script>