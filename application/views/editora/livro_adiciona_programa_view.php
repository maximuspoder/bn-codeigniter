<div id="container" style="overflow-y: auto; ">
    <div class="main_content_left">
        <?php
            $erros = Array();

            if (isset($outrosErros))
            {
                if (is_array($outrosErros))
                {
                    for ($i = 0; $i < count($outrosErros); $i++)
                    {
                        array_push($erros, $outrosErros[$i]);
                    }
                }
                else
                {
                    array_push($erros, $outrosErros);
                }
            }

            exibe_validacao_erros($erros);
            // istancia o CI;
            $CI =& get_instance();
            $CI->load->library('session');
        ?>
        <input type="hidden" name="acao" id="acao" value="{acao}" />
        <input type="hidden" name="precoMin" id="precoMin" value="{precoMin}" />
        <input type="hidden" name="precoMax" id="precoMax" value="{precoMax}" />        
        {livro}
        <input type="hidden" name="idlivro" id="idlivro" value="{IDLIVRO}" />
        <form name="form_cad" id="form_cad" method="post">
            <fieldset style="width:500px; margin-left:20px;">
                <legend><b>Adicionar Livro ao Programa</b></legend>
                <div id="fs_div">
                    <table id="tabelaCadastros2" align="center" border="0">
                        <tr>
                            <td colspan="4" class="areaCadastro2"><img src="<?php echo URL_IMG; ?>geral.png" style="padding-right:10px;" /> Informe os dados para o Programa <?php echo $CI->session->userdata('nomePrograma'); ?></td>
                        </tr>
                        <tr>
                            <td>Livro:</td>
                            <td colspan="3">{TITULO}. De {AUTOR}, {ANO}.</td>
                        </tr>
                        <tr>
                            <td>Quantidade*:</td>
                            <td colspan="3"><input class="inputText" type="text" name="quantidade" id="quantidade" style="width:70px;" maxlength="250" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{QTDP}" autocomplete="off" onkeypress="return maskInteger(this, event)" /></td>
                        </tr>
                        <tr>
                            <td>Preço*:</td>
                            <td colspan="3">
                                <input class="inputText" type="text" name="preco" id="preco" style="width: 70px;" maxlength="10" onfocus="focusLiga(this)" onblur="focusDesliga(this)" onkeypress="return maskMoeda(this, event)" value="{PRECOP}" autocomplete="off" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="text-align: center; padding-top:15px;">
                                <input class="buttonPadrao" type="button" value="  Cancelar  " onclick="$('ligthbox_bg').setStyle({display : 'none'});" />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php if($removeBtn) { ?>
                                    <input class="buttonPadrao" type="button" value="  Remover  " onclick="if(validaRemoveProgramaLivro()){addProgramaLivro('{IDLIVRO}', 'remove','','');}" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php }?>
                                <input class="buttonPadrao" id="btnSaveForm" name="btnSaveForm" type="button" value="  Salvar  " onclick="if(validaForm_addProgramaLivro()){addProgramaLivro('{IDLIVRO}', 'save',$F('preco'),$F('quantidade'));}"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="text-align: center; padding-top:5px; font-size:11px;">
                                (*) Campos de preenchimento obrigatório.
                            </td>
                        </tr>
                    </table>
                </div>
            </fieldset>
        </form>
        {/livro}
    </div>
</div><!-- #container-->

