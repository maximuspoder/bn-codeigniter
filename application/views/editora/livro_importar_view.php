<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>editora.js" type="text/javascript"></script>
    <script language="javascript">
		window.onpageshow = function(evt){ try { $('btnSaveArquivo').disabled = false; } catch(e) {} }
	</script>
</head>
<body>
	<div id="wrapper">
		<?php monta_header(1); ?>
		<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
		
		<div class="divtitulo">
			<div class="divtitulo2">
				>> IMPORTAR LIVROS
			</div>
		</div>
		
		<div id="middle">
			<div id="container" style="text-align:center;">
                 <?php
				$erros = Array();

				if (isset($outrosErros))
				{
					if (is_array($outrosErros))
					{
						for ($i = 0; $i < count($outrosErros); $i++)
						{
							array_push($erros, $outrosErros[$i]);
						}
					}
					else
					{
						array_push($erros, $outrosErros);
					}
				}

				exibe_validacao_erros($erros);
				
				if(!isset($sucessImport) || $sucessImport == 'N')
				{
					?>
					<div class="main_content_center" style="margin-top:10px;">				
						<form enctype="multipart/form-data" method="post" name="form_importa" id="form_importa" action="<?php echo URL_EXEC; ?>editora/uploadImportar/" onsubmit="return validaForm_livroImportar()">
							<fieldset style="width:320px; margin-left:20px;">
								<legend><b>Importar Lista de Livros</b></legend>
								<div id="fs_div">
									<table id="tabelaCadastros" align="center" border="0">
										<tr>
											<td>Arquivo*:</td>
											<td>
												<input type="file" name="arquivo_livro" id="arquivo_livro" size="40"/>
											</td>
										</tr>
										<tr>
											<td colspan="2" style="text-align:center;padding-top:10px; font-size: 10px;">
												<b>Arquivos permitido:</b> .txt (Texto)<br />
												<b>Tamanho permitido:</b> Máx 100 linhas.
											</td>
										</tr>
										<tr>
											<td colspan="2" style="text-align: center; padding-top:15px;">
												<input class="buttonPadrao" id="btnSaveArquivo" type="submit" value=" Enviar " />
											</td>
										</tr>
									</table>
								</div>
							</fieldset>
						</form>          
					</div>
					<div style="margin: 20px 0 20px 20px; text-align: left;">
						<img src="<?php echo URL_IMG; ?>info.png" style="padding-right:10px;" /> 
						<b>INSTRUÇÕES: </b>Os números ISBN devem estar dispostos um abaixo do outro formando apenas uma coluna. <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ISBN estrangeiros não serão importados.<br /><br />
						<table>
							<tr>
								<td style="margin-top: 0">
									<b>Exemplo: </b>
								</td>
								<td>
									<img src="<?php echo URL_IMG; ?>exemploImporta.png" style="padding-left:10px;" />
								</td>
							</tr>
						</table>
					</div>
					
					<?php
				}
				else
				{
					?>
					<img src="<?php echo URL_IMG; ?>v_medio.png" style="padding-top:5px;" />
					<br />
					<?php 
					echo "<b>Importado " . $contSucess . " de " . $contTotal . " códigos ISBN com Sucesso!</b>";
					echo '<br /><br />';
					echo 'Importar mais Livros. ';
					echo '<a href="' . URL_EXEC . '/editora/livroImportar/">Aqui</a>.';
					echo '<br />';
					echo 'Vizualizar lista de Livros. ';
					echo '<a href="' . URL_EXEC . '/editora/livros/">Aqui</a>.';

					if (count($arrLog) > 0)
					{
						?>
						<br /><br />
						<img src="<?php echo URL_IMG; ?>atencao_peq.png" />
						Existem livros que não foram importados pelos motivos abaixo:
						<br /><br />
						<table id="tabelaLog" cellspacing="0" align="center" cellpadding="0" class="extensions t95" style="width: 500px; margin-bottom: 20px;">
							<thead>
								<tr class="header">
									<th class="w20">ISBN</th>
									<th class="w100">Mensagem</th>
								</tr>
								<tr class="header-separator">
									<td colspan="2">&nbsp;</td>
								</tr>
							</thead>
							<tbody>
								<?php
									foreach($arrLog as $key => $log) {
										$log = explode("|", $log);
										echo '<tr>';
										echo '<td class="tdcenter" >' . $log[0] . '</td>';
										echo '<td class="tdleft" >' . $log[1] . '</td>';
										echo '</tr>';
									}
								?>
							</tbody>
						</table>
						<?php
					}
				}
				?>
			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
</body>
</html>