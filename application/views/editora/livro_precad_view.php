<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>editora.js" type="text/javascript"></script>
	<script language="javascript">
		window.onpageshow = function(evt){ $('btnSaveForm').disabled = false; $('isbn_livro').focus(); }
	</script>
</head>
<body>
	<div id="wrapper">
		
		<?php 
		monta_header(1);
		monta_menu($this->session->userdata('tipoUsuario'));
		?>
		
		<div id="middle">
			<div id="container" style="text-align:center;">
				<div class="main_content_center" style="margin-top:10px; margin-bottom:20px;">
					<?php
						$erros = Array();
						
						if (isset($outrosErros))
						{
							if (is_array($outrosErros))
							{
								for ($i = 0; $i < count($outrosErros); $i++)
								{
									array_push($erros, $outrosErros[$i]);
								}
							}
							else
							{
								array_push($erros, $outrosErros);
							}
						}
						
						exibe_validacao_erros($erros);
					?>
					<form name="form_cad" id="form_cad" method="post" action="<?php echo URL_EXEC; ?>editora/livro_precad/valida" onsubmit="return validaForm_preCadLivro()">
						<table id="tabelaCadastros" align="center" border="0" style="width:300px; margin-top:50px;">
							<tr>
								<td class="areaCadastro2" style="text-align: center;">Informe o código ISBN do livro para prosseguir</td>
							</tr>
							<tr>
								<td style="padding-top:15px; text-align: center;">
									<input class="inputText" type="text" name="isbn_livro" id="isbn_livro" style="width:150px; text-align:center; text-transform:none;" maxlength="13" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{isbn_livro}<?php echo set_value('isbn_livro'); ?>" autocomplete="off" onkeypress="return maskInteger(this, event)" />
								</td>
							</tr>
							<tr>
								<td style="text-align: center; padding-top:15px;">
									<input class="buttonPadrao" id="btnSaveForm" name="btnSaveForm" type="submit" value="  Validar Código ISBN  " />
								</td>
							</tr>
							
						</table>
					</form>
				</div>
			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
</body>
</html>