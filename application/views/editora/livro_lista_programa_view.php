<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>editora.js" type="text/javascript"></script>
</head>
<body>
	<div id="wrapper">
		<?php monta_header(1); ?>
		<?php monta_menu($this->session->userdata('tipoUsuario')); ?>		
		<div class="divtitulo">
			<div class="divtitulo2">
				>> LIVROS PARTICIPANTES DO PROGRAMA <?php echo $nomePrograma; ?>
			</div>
		</div>
		
		<div id="middle">
			<div id="container" style="text-align:center;">
				<div class="main_content_center" style="margin-top:10px; margin-bottom:20px;">
					<table cellspacing="0" align="center" cellpadding="0" class="extensions t95">
						<thead>
							<tr class="header">
								<th>ISBN</th>
								<th class="w100">Título</th>
								<th>Autor</th>
								<th>Preço</th>
								<th>Quantidade</th>
							</tr>
							<tr class="header-separator">
								<td colspan="5">&nbsp;</td>
							</tr>
						</thead>
						<tbody>
							{livros}
							<tr class="{CORLINHA}">
								<td class="no-wrap pr_20 tdleft">{ISBN}</td>
								<td class="w100 pr_20 tdleft">{TITULO}</td>
								<td class="no-wrap pr_20 tdleft">{AUTOR}</td>
								<td class="no-wrap pr_20 tdcenter">{PRECOP}</td>
								<td class="no-wrap tdcenter">{QTDP}</td>
							</tr>
							{/livros}
							
							<?php
							if ($vazio == TRUE)
							{
								?>
								<tr>
									<td colspan="5" class="no-wrap tdcenter">Nenhum livro encontrado.</td>
								</tr>
								<?php 
							}
                            ?>
						</tbody>
					</table>
				</div>
			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
</body>
</html>