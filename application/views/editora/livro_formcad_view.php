<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>autocomplete.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>editora.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>powercombo.js" type="text/javascript"></script>
	<script language="javascript">
		window.onpageshow = function(evt){ $('btnSaveForm').disabled = false; }
	</script>
</head>
<body onload="livro_formcad_onload();">
	<div id="wrapper">
		<?php 
		add_elementos_CONFIG();
		monta_header(1);
		monta_menu($this->session->userdata('tipoUsuario'));
		?>
		<div id="middle">
			<div id="container">
				<div class="main_content_left" style="margin-top:10px; margin-bottom:20px;">
					<?php
						$erros = Array();
						
						if (isset($outrosErros))
						{
							if (is_array($outrosErros))
							{
								for ($i = 0; $i < count($outrosErros); $i++)
								{
									array_push($erros, $outrosErros[$i]);
								}
							}
							else
							{
								array_push($erros, $outrosErros);
							}
						}
						
						exibe_validacao_erros($erros);
					?>
					<form name="form_cad" id="form_cad" method="post" action="<?php echo URL_EXEC; ?>editora/livro_formcad/0/save" onsubmit="return validaForm_Livro()">
						<fieldset style="width:680px; margin-left:20px;">
							<legend><b>Cadastro de Livro</b></legend>
							<div id="fs_div">
								<table id="tabelaCadastros2" align="center" border="0">
									<tr>
										<td colspan="4" class="areaCadastro2"><img src="<?php echo URL_IMG; ?>geral.png" style="padding-right:10px;" /> Dados gerais</td>
									</tr>
									<tr>
										<td style="padding-top:10px;">Código ISBN*:</td>
										<td style="padding-top:10px; text-align: left;" colspan="3">
											<input class="inputText inputDisab" type="text" name="isbn" id="isbn" style="width:170px; text-transform:none;" maxlength="20" value="{isbn}<?php echo set_value('isbn'); ?>" autocomplete="off" readonly="readonly" />
										</td>
									</tr>
									<tr>
										<td>Título*:</td>
										<td colspan="3"><input class="inputText" type="text" name="titulo" id="titulo" style="width:450px;" maxlength="250" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{titulo}<?php echo set_value('titulo'); ?>" autocomplete="off" /></td>
									</tr>
									<tr>
										<td>Autor*:</td>
										<td colspan="3"><input class="inputText" type="text" name="autor" id="autor" style="width:450px;" maxlength="200" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{autor}<?php echo set_value('autor'); ?>" autocomplete="off" /></td>
									</tr>
									<tr>
										<td>Suporte*:</td>
										<td colspan="3">
											<select class="select" name="suporte" id="suporte" style="width:350px;">
												<option value="0" <?php echo set_select('suporte', 0); ?> >Selecione</option>
												<?php
												foreach($suporte as $row)
												{
													echo '<option value="' . $row['IDISBNSUPORTE'] . '" ' . set_select('suporte', $row['IDISBNSUPORTE']) . ' >' . $row['DESCSUPORTE'] . '</option>';
												}
												?>
											</select>
										</td>
									</tr>
									<tr>
										<td>Ficha Catalográfica*:</td>
										<td colspan="3">
											<select class="select" name="fichacat" id="fichacat" style="width:80px;">
												<option value="S">SIM</option>
												<option value="N">NÃO</option>
											</select>
										</td>
									</tr>
									<tr>
										<td>Preço*:</td>
										<td colspan="3">
											<input class="inputText" type="text" name="preco" id="preco" style="width: 70px;" maxlength="10" onfocus="focusLiga(this)" onblur="focusDesliga(this)" onkeypress="return maskMoeda(this, event)" value="{preco}<?php echo set_value('preco'); ?>" autocomplete="off" />
										</td>
									</tr>
									<tr>
										<td>Número de Páginas*:</td>
										<td colspan="3">
											<input class="inputText" type="text" name="npaginas" id="npaginas" style="width: 70px;" maxlength="4" onfocus="focusLiga(this)" onblur="focusDesliga(this)" onkeypress="return maskInteger(this, event)" value="{npaginas}<?php echo set_value('npaginas'); ?>" autocomplete="off" />
										</td>
									</tr>
									<tr>
										<td>Dimensão:</td>
										<td colspan="3">
											Menor: <input class="inputText" type="text" name="formato_a" id="formato_a" maxlength="4" style="text-align:right; width:50px;" onfocus="focusLiga(this)" onblur="focusDesliga(this)" onkeypress="return maskInteger(this, event)" value="{formato_a}<?php echo set_value('formato_a'); ?>" autocomplete="off" /> cm
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											Maior: <input class="inputText" type="text" name="formato_b" id="formato_b" maxlength="4" style="text-align:right; width:50px;" onfocus="focusLiga(this)" onblur="focusDesliga(this)" onkeypress="return maskInteger(this, event)" value="{formato_b}<?php echo set_value('formato_b'); ?>" autocomplete="off" /> cm
										</td>
									</tr>
									<tr>
										<td>Papel:</td>
										<td colspan="3">
											Miolo: <input class="inputText" type="text" name="papel_miolo" id="papel_miolo" maxlength="8" style="text-align:right; width:50px;" onfocus="focusLiga(this)" onblur="focusDesliga(this)" onkeypress="return maskInteger(this, event)" value="{papel_miolo}<?php echo set_value('papel_miolo'); ?>" autocomplete="off" /> g/m²
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											Capa: <input class="inputText" type="text" name="papel_capa" id="papel_capa" maxlength="8" style="text-align:right; width:50px;" onfocus="focusLiga(this)" onblur="focusDesliga(this)" onkeypress="return maskInteger(this, event)" value="{papel_capa}<?php echo set_value('papel_capa'); ?>" autocomplete="off" /> g/m²
										</td>
									</tr>
									<tr>
										<td>Peso:</td>
										<td colspan="3">
											<input class="inputText" type="text" name="peso" id="peso" maxlength="10" style="text-align:right; width:75px;" onfocus="focusLiga(this)" onblur="focusDesliga(this)" onkeypress="return maskInteger(this, event)" value="{peso}<?php echo set_value('peso'); ?>" autocomplete="off" /> gramas
										</td>
									</tr>
									<tr>
										<td>Orelha*:</td>
										<td colspan="3">
											<select class="select" name="orelha" id="orelha" style="width:80px;">
												<option value="S">SIM</option>
												<option value="N">NÃO</option>
											</select>
										</td>
									</tr>
									<tr>
										<td>Tipo de Capa*:</td>
										<td colspan="3">
											<select class="select" name="tipocapa" id="tipocapa" style="width:170px;" onchange="setTipoCapa()">
												<option value="0" <?php echo set_select('tipocapa', 0); ?> >Selecione</option>
												<?php
												foreach($tipocapa as $row)
												{
													echo '<option value="' . $row['IDISBNTIPOCAPA'] . '" ' . set_select('tipocapa', $row['IDISBNTIPOCAPA']) . ' >' . $row['DESCTIPOCAPA'] . '</option>';
												}
												?>
											</select>
											&nbsp;&nbsp;&nbsp;
											<div id="divTipoCapaOutra" style="display:none;">Qual: <input class="inputText" type="text" name="tipocapa_outra" id="tipocapa_outra" style="width:150px;" maxlength="50" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{tipocapa_outra}<?php echo set_value('tipocapa_outra'); ?>" autocomplete="off" /></div>
										</td>
									</tr>
									<tr>
										<td>Acabamento*:</td>
										<td colspan="3">
											<select class="select" name="acabamento" id="acabamento" style="width:170px;">
												<option value="0" <?php echo set_select('acabamento', 0); ?> >Selecione</option>
												<?php
												foreach($acabamento as $row)
												{
													echo '<option value="' . $row['IDISBNACABAMENTO'] . '" ' . set_select('acabamento', $row['IDISBNACABAMENTO']) . ' >' . $row['DESCACABAMENTO'] . '</option>';
												}
												?>
											</select>
										</td>
									</tr>
									<tr>
										<td>Idioma Original*:</td>
										<td colspan="3">
											<select class="select" name="idioma" id="idioma" style="width:340px;">
												<option value="0" <?php echo set_select('idioma', 0); ?> >Selecione</option>
												<?php
												foreach($idioma as $row)
												{
													echo '<option value="' . $row['IDISBNIDIOMA'] . '" ' . set_select('idioma', $row['IDISBNIDIOMA']) . ' >' . $row['DESCIDIOMA'] . '</option>';
												}
												?>
											</select>
										</td>
									</tr>
									<tr>
										<td>Idioma Tradução:</td>
										<td colspan="3">
											<select class="select" name="idiomatrad" id="idiomatrad" style="width:340px;">
												<option value="0" <?php echo set_select('idiomatrad', 0); ?> >Selecione</option>
												<?php
												foreach($idioma as $row)
												{
													echo '<option value="' . $row['IDISBNIDIOMA'] . '" ' . set_select('idiomatrad', $row['IDISBNIDIOMA']) . ' >' . $row['DESCIDIOMA'] . '</option>';
												}
												?>
											</select>
										</td>
									</tr>
									<tr>
										<td>Assunto*:</td>
										<td colspan="3">
											<input type="hidden" name="idassunto" id="idassunto" value="{idassunto}<?php echo set_value('idassunto'); ?>" />
											<input class="inputText" type="text" name="assunto" id="assunto" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{assunto}<?php echo set_value('assunto'); ?>" style="width: 450px;" />
											<img src="<?php echo URL_IMG; ?>x_mini.gif" name="imgAssunto" id="imgAssunto" alt="NENHUM ASSUNTO SELECIONADO" title="NENHUM ASSUNTO SELECIONADO" style="cursor:help;" />
										</td>
									</tr>
									<tr>
										<td>Edição:*:</td>
										<td colspan="3">
											<input class="inputText" type="text" name="edicao" id="edicao" size="8" maxlength="10" onfocus="focusLiga(this)" onblur="focusDesliga(this)" onkeypress="return maskInteger(this, event)" value="{edicao}<?php echo set_value('edicao'); ?>" autocomplete="off" />
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											Ano: <input class="inputText" type="text" name="ano" id="ano" size="8" maxlength="4" onfocus="focusLiga(this)" onblur="focusDesliga(this)" onkeypress="return maskInteger(this, event)" value="{ano}<?php echo set_value('ano'); ?>" autocomplete="off" />
										</td>
									</tr>
									<tr>
										<td>Palavras-Chave:</td>
										<td colspan="3">
											<input class="inputText" type="text" name="palavraschave" id="palavraschave" style="width: 450px;" maxlength="50" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{palavraschave}<?php echo set_value('palavraschave'); ?>" autocomplete="off" />
										</td>
									</tr>
									<tr>
										<td>Sinopse:</td>
										<td colspan="3">
											<textarea style="width: 450px; height: 60px;" name="sinopse" id="sinopse">{sinopse}<?php echo set_value('sinopse'); ?></textarea>
										</td>
									</tr>
									<tr>
										<td>Link para Dados:</td>
										<td colspan="3"><input class="inputText" type="text" name="linkdados" id="linkdados" style="width:450px; text-transform:none;" maxlength="200" onfocus="focusLiga(this)" onblur="focusDesliga(this)" value="{linkdados}<?php echo set_value('linkdados'); ?>" autocomplete="off" /></td>
									</tr>
									<tr>
										<td colspan="4" style="text-align: center; padding-top:15px;">
											<input type="hidden" name="loadDadosEdit"  id="loadDadosEdit"  value="{loadDadosEdit}" />
											<input type="hidden" name="idlivro"        id="idlivro"        value="{idlivro}" />
											<input type="hidden" name="idisbn"         id="idisbn"         value="{idisbn}<?php echo set_value('idisbn'); ?>" />
											<input type="hidden" name="suporte_aux"    id="suporte_aux"    value="{suporte_aux}" />
											<input type="hidden" name="fichacat_aux"   id="fichacat_aux"   value="{fichacat_aux}" />
											<input type="hidden" name="orelha_aux"     id="orelha_aux"     value="{orelha_aux}" />
											<input type="hidden" name="tipocapa_aux"   id="tipocapa_aux"   value="{tipocapa_aux}" />
											<input type="hidden" name="acabamento_aux" id="acabamento_aux" value="{acabamento_aux}" />
											<input type="hidden" name="idioma_aux"     id="idioma_aux"     value="{idioma_aux}" />
											<input type="hidden" name="idiomatrad_aux" id="idiomatrad_aux" value="{idiomatrad_aux}" />
											<input class="buttonPadrao" type="button" value="  Voltar  " onclick="history.back();" />
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input class="buttonPadrao" id="btnSaveForm" name="btnSaveForm" type="submit" value="  Salvar  " />
										</td>
									</tr>
									<tr>
										<td colspan="4" style="text-align: center; padding-top:5px; font-size:11px;">
											(*) Campos de preenchimento obrigatório.
										</td>
									</tr>
								</table>
							</div>
						</fieldset>
					</form>
				</div>
			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
</body>
</html>