<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.tablesorter.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
</head>
<body>
	<?php monta_header(1); ?>
	<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
	<?php add_elementos_CONFIG(); ?>
	<div id="page_content">        
		<div id="inside_content">
			<h5 class="comment">Seja bem-vindo(a), <?php echo(@$nome_usuario);?>.</h5>
			<div class="inline top" style="width:550px;">
				<div class="inline" style="margin:20px 10px 10px 0"><h3>Avisos, comunicados e mensagens <!--(0)--></h3></div>
				<!--<span><a href="javascript:void(0);" class="black">Nova mensagem</a></span>-->
				<?php
					$notmsg = true;
					// mensagem('info', 'Seleção de ponto de venda', 'Você deve selecionar um ponto de venda para o 4º edital. Caso você queira permanecer utilizando o mesmo, clique <a href="javascript:void(0);">aqui</a>, caso contrário entre na <a href="javascript:void(0);">tela de seleção de ponto de venda</a> e informe o ponto de venda parceiro para este edital.', false, 'width:500px;');
					
					// Caso HABILITADO
					if($habilitado == 'S')
					{					
						mensagem('note', '', 'Você está habilitado e logado no Edital <b>' . $NOME_EDITAL . '</b>.', false, 'width:500px;margin-bottom:20px;');
						$notmsg = false;
					}
					
					// Comunicado CNPC
					if(true)
					{
						mensagem('info', 'Comunicado CNPC', 'Prezados Senhores: O Ministério da Cultura deu início ao processo de renovação dos Colegiados... <a href="javascript:void(0);" onclick="ajax_modal(\'Comunicado CNPC - Cadastramento de Eleitores e Candidatos\', \'' . URL_EXEC . 'comunicacao/comunicado_cnpc\', \'' . URL_IMG . 'icon_info.png\', 610, 700);">leia mais</a>', false, 'width:500px;margin-bottom:20px;');
						$notmsg = false;
					}
					
					// Mensagem para download de Excel (livros da editora)
					if(count($livros_geral) > 0)
					{					
						mensagem('info', '', 'Clique no no link abaixo para fazer o download do arquivo onde consta o total geral de quantidades solicitadas de cada título cadastrado pela <strong>Editora</strong>.<br /><br /><img src="' . URL_IMG . 'icon_excel.gif" style="margin:1px 5px 0 0" /><a href="javascript:void(0);" onclick="download_excel(\'' . URL_EXEC . 'editora/download_excel_itens_pedido_editora/' . $idusuario . '\');">Download do arquivo XLS</a>', false, 'width:500px;margin-bottom:20px;');
						$notmsg = false;
					}
					
					// Mensagem para download de Excel (livros por pdv)
					if(count($livros_pdv) > 0)
					{					
						mensagem('info', '', 'Clique no no link abaixo para fazer o download do arquivo onde consta a especificação das quantidades solicitadas de cada título, indicando os totais de itens por <strong>Ponto de Venda</strong>.<br /><br /><img src="' . URL_IMG . 'icon_excel.gif" style="margin:1px 5px 0 0" /><a href="javascript:void(0);" onclick="download_excel(\'' . URL_EXEC . 'editora/download_excel_itens_pedido_editora_por_pdv/' . $idusuario . '\');">Download do arquivo XLS</a>', false, 'width:500px;margin-bottom:20px;');
						$notmsg = false;
					}
					
					// Livros Inexigíveis (3º Edital somente)
					if(count($livros_inexigiveis) > 0)
					{					
						mensagem('info', '', '<strong>Livros inexigíveis:</strong> clique no link abaixo e faça o download da listagem de <strong>títulos inexigíveis por editoras</strong>.<br /><strong>Atenção!</strong> Se itens não entregues pelo PDV estiverem indicados como inexigíveis, sua entrega não é considerada obrigatória, logo, a conferência poderá ser finalizada normalmente para liberação de recursos. Itens exigíveis deverão ser entregues pelo PDV à biblioteca até o dia 30 de julho. Verifique orientações: <a href="http://www.bn.br/portal/arquivos/pdf/Orientacoes%20para%20pagamento%20Edital%20FBN%203.pdf" target="_blank">COMUNICADO OFICIAL NO PORTAL DO LIVRO/EDITAL FBN DE 2011</a><br /><br /><img src="' . URL_IMG . 'icon_excel.gif" style="margin:1px 5px 0 0" /><a href="javascript:void(0);" onclick="download_excel(\'' . URL_EXEC . 'livro/download_excel_livros_inexigiveis\');">Download do arquivo XLS</a>', false, 'width:500px;margin-bottom:20px;');
						$notmsg = false;
					}
					
					// Habilitação nos editais
					foreach ($boolPeriodoAceite as $key => $value )
					{
						if((datediff(date('Y/m/d'), $value['DATA_DE']) >= 0) && (datediff(date('Y/m/d'), $value['DATA_ATE']) <= 0))
						{
							mensagem('info', 'Habilitação de Editoras', 'O período para habilitação de editoras no edital <b>' . $value['NOME_EDITAL'] . '</b> já está em aberto e você tem até dia ' . eua_to_br($value['DATA_ATE']) . ' para habilitar sua editora.<br />
														<div style="width:410px;text-align:left;height:100px;overflow-y:auto;border:1px solid #aaa;padding:10px;margin:10px 0 0 0;">' . $value['TEXTO_EDITAL'] . '</div>
														<div style="display:inline-block !important;width:85%;width:410px;margin:10px 0 0 0;"><input type="checkbox" name="habilitar_cad_' . $value['IDPROGRAMA'] . '" id="habilitar_cad_' . $value['IDPROGRAMA'] . '" value="1" />&nbsp;Declaramos ter pleno conhecimento dos termos do Edital ' . $value['NOME_EDITAL'] . ', sujeitando-nos aos seus termos.</div>
														<div style="text-align:right;margin-right:10px;">&nbsp;&nbsp;<a href="javascript:void(0);" onclick="habilitaEdital(\'' . $value['IDPROGRAMA'] .'\');">Habilitar</a></div>', 
														false, 'width:500px;margin-bottom:20px;');
						}
						$notmsg = false;
					}
					
					// Caso não tenha msg para exibir
					if($notmsg)
					{
						mensagem('note', '', 'Você não tem nenhum aviso, mensagem ou comunicado a ser exibido.', false, 'width:500px;margin-bottom:20px;');
					}
				?>
			</div>
			<div class="inline top" style="width:370px;margin:15px 0 0 15px;">
				<?php box_edital(); ?>
				<?php box_minhas_informacoes(); ?>
			</div>		  
		</div>
	</div>

    <!--<div id="wrapper">
		<div id="middle">

			<div id="container" style="text-align:center;">
				<div class="main_content_center" style="margin-top:30px;">
					
					<b>Seja bem-vindo ao sistema da Biblioteca Nacional.</b>
					
				</div>
				<?php if(count($tem_item_pedido) > 0){?>
					<center>
						<div style="width:700px;margin-top:40px;">
							<div style="margin:0 0 20px 0;text-align:left;">
								<img src="<?php echo URL_IMG; ?>info.png" style="padding-right:10px;" />Atenção! A lista de títulos selecionados pelas bibliotecas no Edital de Aquisição de livros já está disponível. Clique no botão ao lado para fazer o download no formato XLS.&nbsp;&nbsp;&nbsp;<button class="buttonPadrao" onclick="download_xls_itens_pedido_editora(<?php echo($idusuario);?>);">Download</button>
							</div>
						</div>
					</center>
					<center>
						<div style="width:700px;margin-top:20px;">
							<div style="margin:0 0 20px 0;text-align:left;">
								<img src="<?php echo URL_IMG; ?>info.png" style="padding-right:10px;" />Atenção! A lista de títulos e quantidades selecionadas por ponto de venda já está disponível. Clique no botão ao lado para fazer o download no formato XLS.&nbsp;&nbsp;&nbsp;<button class="buttonPadrao" onclick="download_xls_itens_pedido_editora_por_pdv(<?php echo($idusuario);?>);">Download</button>
							</div>
						</div>
					</center>
				<?php } ?>
				<?php 
				foreach ($boolPeriodoAceite as $key => $value )
				{
                    if((datediff(date('Y/m/d'), $value['DATA_DE']) >= 0) && (datediff(date('Y/m/d'), $value['DATA_ATE']) <= 0))
                    {
                   
					?>
					<center>
						<div style="width:700px;margin-top:40px;">
							<div style="margin:0 0 20px 0;text-align:left;">
								<img src="<?php echo URL_IMG; ?>info.png" style="padding-right:10px;" /> O período para habilitação de pontos de venda no edital <b><?php echo $value['NOME_EDITAL']; ?></b> já está em aberto e você tem até dia <?php echo eua_to_br($value['DATA_ATE']); ?> para habilitar o seu ponto de venda.
							</div>
                            <div style="width:700px;text-align:left; height: 150px;; overflow-y: scroll; border: 1px solid #666666; padding: 10px;">
                                <?php echo $value['TEXTO_EDITAL'];?>
                            </div>
							<div style="margin:20px 0 0 0;text-align:left;">
								<div style="display:inline-block !important;width:85%;"><input type="checkbox" name="habilitar_cad" id="habilitar_cad" value="1" />&nbsp;Declaramos ter pleno conhecimento dos termos do Edital <?php echo $value['NOME_EDITAL']; ?>, sujeitando-nos aos seus termos.</div>
								<div style="float:right;">&nbsp;&nbsp;<input type="button" value="Habilitar" class="buttonPadrao" onclick="habilitaEdital('<?php echo $value['IDPROGRAMA']; ?>')" /></div>
							</div>
						</div>
					</center>
					<?php
                    }
				}
				
                if($habilitado == 'S')
				{
					?>
					<center>
						<div style="width:700px;margin-top:40px;">
							<div style="margin:0 0 20px 0;text-align:center;">
								<img src="<?php echo URL_IMG; ?>info.png" style="padding-right:10px;" /> Sua editora está habilitada no Edital <b><?php echo $NOME_EDITAL; ?></b>.
							</div>
                            <?php
                            if($boolPeriodoInclusaoLivro)
                            {
                            ?>
                                <div style="margin:0 0 20px 0;text-align:center;">
                                    <img src="<?php echo URL_IMG; ?>info.png" style="padding-right:10px;" /> O período para inclusão de livros no Programa Livro Popular já está em aberto e você tem até o dia <?php echo eua_to_br($dateAteInclusaoLivro); ?> para incluí-los.
                                </div>
                            <?php } ?>
						</div>
					</center>
					<?php
				}
                ?>
				
				<center>
					<div style="width:700px;margin-top:40px;">
						<b>Prezados Usuários</b>, 
						<br/><br/>
						Por motivos de ordem técnica, a escolha do Ponto de Venda e o início da seleção e <br/>
						aquisição dos livros iniciará em <b>03/01/2012</b>.<br/><br/>
						Agradecemos sua compreensão.<br/><br/>
						Fundação Biblioteca Nacional
						</div>
					</div>
				</center>
				
			</div> #container
			
		</div> #middle

	</div> #wrapper -->
	
	<?php //monta_footer(); ?>
	<iframe id="download_excel_pedido" src="" style="display:none"></iframe>
</body>
</html>