<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			// Validação e máscaras
			$("#form_default").validationEngine({ inlineValidation:false , promptPosition : "centerRight", scroll : false });
			$("input:text").setMask();
		});
	</script>
</head>
<body>
	<?php monta_header(1); ?>
	<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
	<?php add_elementos_CONFIG(); ?>
	<div id="page_content">
		<div id="inside_content">
			<div>
				<div class="inline"><h1><a href="<?php echo URL_EXEC;?>editora/search" class="black font_shadow_gray">Distribuidor</a></h1></div>
				<div class="inline"><h3 class="font_shadow_gray">&nbsp;> Edição de Cadastro de Endereço</h3></div>
			</div>
			<br />
			<?php mensagem('note', '', 'Utilize o formulário abaixo para editar os dados do cadastro que está sendo visualizado. Campos com (*) são obrigatórios.'); ?>
			<br />
			<br />
			<form name="form_default" id="form_default" method="post" action="<?php echo URL_EXEC; ?>distribuidor/form_update_distribuidor_proccess/">
				<input type="hidden" name="idusuario" id="idusuario" value="<?php echo get_value($dados, 'IDUSUARIO');?>" />
				<input type="hidden" name="idlogradouro" id="idlogradouro" value="<?php echo get_value($dados, 'IDLOGRADOURO');?>" />
				<h4 class="font_shadow_gray">Dados do Cadastro</h4>
				<hr />
				<div style="float:right;margin:-30px 0 0 0;"><?php echo get_value($dados, 'ATIVO_IMG'); ?></div>
				<div class="form_label">ID Usuario (#):</div>
				<div class="form_field" style="padding-top:6px;"><?php echo get_value($dados, 'IDUSUARIO');?></div>
				<br />
				<div class="form_label">Cep:</div>
				
				<div class="form_field"><input type="text" name="nomeresp" id="nomeresp" value="<?php echo get_value($dados, 'CEP');?>" class="validate[required]" style="width:150px;" />
				<a href="javascript:void(0);"><img src="<?php echo URL_IMG; ?>icon_view.png" onclick="ajax_modal('Pesquisar Cep','<?php echo URL_EXEC; ?>/distribuidor/form_update_distribuidor_endereco',600,800);" title="Pesquisar Cep" /></a>
				</div>
				
				
				
				<br />				
				<div class="form_label">Estado:</div>
				<div class="form_field"><input type="text" name="nomeresp" id="nomeresp" value="<?php echo get_value($dados, 'ESTADO');?>" class="validate[required]" style="width:150px;" /></div>
				<br />
				<div class="form_label">Cidade:</div>
				<div class="form_field"><input type="text" name="nomeresp" id="nomeresp" value="<?php echo get_value($dados, 'CIDADE');?>" class="validate[required]" style="width:150px;" /></div>
				<br />
				<div class="form_label">Cidade sub:</div>
				<div class="form_field"><input type="text" name="nomeresp" id="nomeresp" value="<?php echo get_value($dados, 'CIDADE SUB');?>" class="validate[required]" style="width:150px;" /></div>
				<br />	
				<div class="form_label">Bairro:</div>
				<div class="form_field"><input type="text" name="nomeresp" id="nomeresp" value="<?php echo get_value($dados, 'BAIRRO');?>" class="validate[required]" style="width:150px;" /></div>
				<br />
				<div class="form_label">Número:</div>
				<div class="form_field"><input type="text" name="nomeresp" id="nomeresp" value="<?php echo get_value($dados, 'NUMERO');?>" class="validate[required]" style="width:150px;" /></div>
				<br />	
				<div class="form_label">Complemento:</div>
				<div class="form_field"><input type="text" name="nomeresp" id="nomeresp" value="<?php echo get_value($dados, 'COMPLEMENTO');?>" class="validate[required]" style="width:150px;" /></div>
				<br />						
				<div style="margin-top:35px">
					<hr />
					<div class="inline top"><button onclick="set_form_redirect('<?php echo $url_redirect; ?>', '', 'form_default', 'ok');">OK</button></div>&nbsp;&nbsp;&nbsp;&nbsp;
					<div class="inline top"><button onclick="set_form_redirect('<?php echo $url_redirect; ?>', 'distribuidor/form_update_distribuidor/<?php echo get_value($dados, 'IDUSUARIO')?>', 'form_default', 'aplicar');">Aplicar</button></div>
					<div class="inline top" style="padding:8px 0 0 5px">ou&nbsp;&nbsp;&nbsp;<a href="<?php echo URL_EXEC; ?><?php echo $url_redirect; ?>">voltar</a></div> 
				</div>
				
				
				
				
			</form>
		</div>
	</div>
</body>
</html>