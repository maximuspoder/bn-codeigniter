<div id="container" style="overflow-y: auto; ">
    <div class="main_content_left">
        <fieldset style="width:800px; margin-left:20px;">
			<legend><b>Ajuda</b></legend>
			<div id="fs_div">
				<div style="display:inline-block !important; text-align: justify;">
				<b>Como Pesquisar</b>
				<blockquote>
				Informe pelo menos um dos critérios especificados. 
				<br /><br />
				A pesquisa pode ser feita por título ou nome do autor, ou por parte do título ou nome do autor.
				<br /><br />
				O resultado apresentará registros que contenham o critério especificado no início, no meio ou no fim do nome, separando as
				palavras e considerando apenas partes com 3 (três) letras ou mais.
				<br /><br />
				Exemplo 1: se você informar "CAS" como título, a pesquisa retornará DOM <b>CAS</b>MURRO, QUIN<b>CAS</b> BORBA, 100 CRÔNI<b>CAS</b>,
				entre outros títulos, que contenham o critério especificado. 
				<br /><br />
				Exemplo 2: se você informar "O CONDE DE MONTE CRISTO" como título, a pesquisa retornará resultados contendo as palavras "CONDE", "MONTE" 
				e "CRISTO", obedecendo também ao disposto no Exemplo 1. 
				<br /><br />
				Você pode combinar critérios de título e autor, com os mesmos efeitos relacionados acima. 
				</blockquote>
				
				<b>Visualização</b>
				<blockquote>
				Você deve selecionar se deseja visualizar apenas os títulos que não indicou, apenas os seus indicados ou todos. Em
				caso de busca em todos os títulos, os indicados virão na cor <span style="color: #999999"><b>cinza</b></span>.
				</blockquote>
				
				<b>Indicação de Livros</b>
				<blockquote>
				Para selecionar um livro a ser indicado, clique no botão "Indicar", à esquerda do título. Caso o título conste como 
				editado por mais de uma editora, uma janela auxiliar se abrirá para que você selecione a editora.<br />
				Selecione a editora, clique em "salvar" e sua indicação será concluída. <br/>
				Se você cancelar a escolha de editora, sua indicação não será registrada.
				<br /><br />
				No topo da página à esquerda, você poderá visualizar quantas indicações vocè já fez, quantas ainda pode fazer e o total.
				Ao concluir suas indicações, você não precisa fazer mais nada. Ao término do prazo, as indicações recebidas serão processadas
				para definir o pacote de livros que irá compor o edital. 
				</blockquote>

				<b>Remover uma indicação:</b>
				<blockquote>
				Para excluir uma indicação já feita, basta clicar sobre o botão "Desfazer" que ela será removida e você poderá
				indicar outro título.
				</blockquote>

				<b>Controle da Quantidade de Títulos Indicados</b>
				<blockquote>
				No canto superior esquerdo da tela você poderá acompanhar quantos títulos já indicou.
				</blockquote>

				<b>Suporte ao Usuário</b>
				<blockquote>
				E-mail editais2011@bn.br ou 51 3254-32-43
				</blockquote>
				</div>
				<div style="display:inline-block !important;margin:0 0 0 20px;">
					<input class="buttonPadrao" type="button" value="  Fechar  " onclick="$('ligthbox_bg').setStyle({display : 'none'});" />
				</div>
			</div>
		</fieldset>
    </div>
</div><!-- #container-->