<div id="container" style="overflow-y: auto; ">
    <div class="main_content_left">
        <form name="form_cad" id="form_cad" method="post" action="javascript:if(validaForm_Comentario()){ salvaComentario(<?php echo($idLivro)?>); }">
            <fieldset style="width:500px; margin-left:20px;">
                <legend><b>Comentários</b></legend>
                <div id="fs_div">
                    <table id="tabelaCadastros2" align="center" border="0">
                        <tr>
                            <td class="bold">Livro:</td>
                            <td><?php echo($titulo);?></td>
                        </tr>
						<tr>
                            <td class="bold">Autor:</td>
                            <td><?php echo($autor);?></td>
                        </tr>
						<tr>
                            <td class="bold">Comentário:</td>
                            <td>
								<textarea style="width: 400px; height: 50px;" name="txtcomentario" id="txtcomentario"></textarea>
							</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center; padding-top:15px;">
                                <input class="buttonPadrao" type="button" value="  Cancelar  " onclick="$('ligthbox_bg').setStyle({display : 'none'});" />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input class="buttonPadrao" id="btnSaveForm" name="btnSaveForm" type="button" value="  Salvar  " onclick="if(validaForm_Comentario()){ salvaComentario(<?php echo($idLivro)?>); }"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </fieldset>
        </form>
    </div>
	<div class="main_content_left">
		<table cellspacing="0" cellpadding="3" border="0" style="410px;margin:10px 0 0 19px;" class="extensions t95">
			{comentarios}
				<tr class="{CORLINHA}">
					<td>
						<img src="<?php echo URL_IMG; ?>comment2.png" title="Comentários" alt="Comentários" />&nbsp;&nbsp;
						{COMENTARIO}
						<br />
						{RAZAOSOCIAL} - <i>{DATACOMENTARIO2}</i>
						<hr />
					</td>
				</tr>
			{/comentarios}
		</table>
	</div>
</div><!-- #container-->

