<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>pesquisa.js" type="text/javascript"></script>
	<script language="javascript">
	Event.observe(window, 'load', function() {
		$('gridview').observe('click', function() {
			$('gridbusca').show();
			$('listabusca').hide();
			$('visualizacao').value = "grid";
		});
		
		$('listview').observe('click', function() {
			$('gridbusca').hide();
			$('listabusca').show();
			$('visualizacao').value = "lista";
		});
		
		if ($('visualizacao').value == "grid") {
			$('gridbusca').show();
			$('listabusca').hide();
		} else if ($('visualizacao').value == "lista") {
			$('gridbusca').hide();
			$('listabusca').show();
		}
	});
	</script>
</head>
<body>
	<div id="wrapper">
		<?php add_elementos_CONFIG(); ?>
        <?php add_div_ligthbox('560', '270'); ?>
		<?php monta_header(1); ?>
		<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
		
		<br />
		<br />
		<!--
		<div class="divtitulo">
			<div class="divtitulo2" style="margin-left:-10px;">
				>> PESQUISAR LIVROS
			</div>
		</div>
		-->
		<div id="middle">
			<table cellpadding="0" cellspacing="0" border="0" width="98%" style="margin:0 30px 20px 20px;">
			<tr>
				<td id="td_lista">
					<div id="titulo_lista" class="titulo">
						<span>MINHA LISTA</span> 
						<div class="comment_peq" style="float:right;margin:0 40px 5px 0;">
							<?php echo(($qtdeItens == 1) ? "($qtdeItens item)" : '');?>
							<?php echo(($qtdeItens >= 2) ? "($qtdeItens itens)" : '');?>
						</div>
					</div>
					<div class="comment_peq font_11" style="color:green;font-size:11px">&bull;&nbsp;Saldo: R$ <strong><?php echo($credito);?></strong> disponíveis</div>
					<div><a href="<?php echo(URL_EXEC);?>pedido/gerencia" class="red font_12">&bull;&nbsp;Gerenciar Lista</a></div>
					<div id="box_lista_livros">
						<?php if($qtdeItens <= 0){ ?>
						<div id="box_livro_center">
							<div>Nenhum livro na sua lista de aquisições. Localize livros utilizando a pesquisa ao lado e adicione à sua lista.</div>
						</div>
						<?php } else { ?>
						{pedidoItens}
						<div id="box_livro">
							<div><a href="javascript:void(0);" class="red" title="Visualizar Detalhes - {TITULO}" onclick="showLivro({IDLIVRO});">{TITULO}</a></div>
							<div><strong>Autor:</strong> {AUTOR}</div>
							<div><strong>Editora:</strong> {EDITORA} ({EDICAO}ª Ed., {ANO})</div>
							<div><strong>Quantidade:</strong> {QTD}</div>
							<div><a href="javascript:void(0);" class="red" onclick="modalAddPedidoItem({IDLIVRO},1);">Editar Quantidade</a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="red" onclick="modalRemovePedidoItem({IDLIVRO},1);">Remover</a></div>
						</div>
						{/pedidoItens}
						<?php } ?>
						<br />
					</div>
					<div style="margin-top:10px;text-align:center;"><button class="buttonPadrao" onclick="location.href='<?php echo(URL_EXEC);?>pedido/gerencia';">Finalizar Lista</button></div>
				</td>
				<td id="td_livros">
					<div id="container_livros">
						<form name="filtros_lista" id="filtros_lista" method="post" class="form_filtro" action="<?php echo URL_EXEC; ?>pesquisa/pesquisarLivros">
							<div id="filtro_agrupador" style="height:85px;">
								<div id="titulo_filtro" class="titulo">FILTRE SUA BUSCA</div>
								<div class="filtro_campo" style="display:none !important;">
									<span>Status:</span>
									<select name="filtro_status" id="filtro_status" class="select" style="width:100px;">
										<option value=""  <?php echo(($filtros['filtro_status'] == '')   ? 'selected="selected"' : ''); ?>></option>
										<option value="S" <?php echo(($filtros['filtro_status'] == 'S')  ? 'selected="selected"' : ''); ?>>Confirmado</option>
										<option value="N" <?php echo(($filtros['filtro_status'] == 'N')  ? 'selected="selected"' : ''); ?>>Não Confirmado</option>
									</select>
								</div>
								<div class="filtro_campo">
									<span>ISBN:</span>
									<input type="text" name="filtro_isbn" id="filtro_isbn" class="inputText" style="width:100px;" value="<?php echo($filtros['filtro_isbn']);?>" />
								</div>
								<div class="filtro_campo">
									<span class="margin">Títulos:</span>
									<input type="text" name="filtro_titulo" id="filtro_titulo" class="inputText" style="width:100px;" value="<?php echo($filtros['filtro_titulo']);?>" />
								</div>
								<div class="filtro_campo">
									<span class="margin">Autor:</span>
									<input type="text" name="filtro_autor" id="filtro_autor" class="inputText" style="width:100px;" value="<?php echo($filtros['filtro_autor']);?>" />
								</div>
								<div class="filtro_campo">
									<span class="margin">Editora:</span>
									<input type="text" name="filtro_editora" id="filtro_editora" class="inputText" style="width:100px;" value="<?php echo($filtros['filtro_editora']);?>" />
								</div>
								<br />
								<div class="filtro_campo" style="margin:10px 0 0 0;">
									<span>Palavras-Chave:</span>
									<input type="text" name="filtro_palavraschave" id="filtro_palavraschave" class="inputText" style="width:196px;" value="<?php echo($filtros['filtro_palavraschave']);?>" />
								</div>
								<div class="filtro_campo" style="margin:10px 0 0 0;">
									<span class="margin">Sinopse:</span>
									<input type="text" name="filtro_sinopse" id="filtro_sinopse" class="inputText" style="width:175px;" value="<?php echo($filtros['filtro_sinopse']);?>" />
								</div>
								<div class="filtro_campo"><span class="margin"><input type="submit" value="Enviar" class="buttonPadrao" /></span></div>
								<div align="right" style="float:right;">
									<a href="javascript:void(0);"><img id="gridview" src="<?php echo URL_IMG; ?>grid_view.png" title="Visualizar Lado-a-Lado" alt="Visualizar Lado-a-Lado" /></a>&nbsp;&nbsp;&nbsp;
									<a href="javascript:void(0);"><img id="listview" src="<?php echo URL_IMG; ?>list_view.png" title="Visualizar em Lista" alt="Visualizar em Lista" /></a>
									<input type="hidden" name="visualizacao" id="visualizacao" value="<?php echo($filtros['visualizacao']);?>" />
								</div>
							</div>
						</form>
						<?php if ($vazio == true){ ?>
						<div id="msg_sem_dados"><img src="<?php echo(URL_IMG);?>/atencao_peq.PNG" style="margin:0 10px;float:left" />Atenção! Nenhum livro localizado para o programa selecionado.</div>
						<?php } else { ?>
						<div id="paginacao_top">Páginas: <?php montaPaginacaoPesquisa($nPaginas, $nPag, $limit_de, $exibir_pp, URL_EXEC . 'pesquisa/pesquisarLivros/', 'filtros_lista'); ?></div>
						<div id="gridbusca" style="display: block;">
						<?php foreach($livros as $livro) {?>
						<div id="box_livro_pesquisa">
							<div style="margin:10px;">
								<div style="min-height:115px;">
									<div style="font-size:11px;max-height:35px;text-align: left;overflow-y:hidden;overflow-x:hidden;cursor:default;" title="<?php echo($livro['TITULO']);?>"><strong><?php echo($livro['TITULO']);?></strong></div>
									<div style="font-size:11px;"><strong>Autor:</strong> <?php echo($livro['AUTOR']);?></div>
									<div style="font-size:11px;"><strong>Editora:</strong> <?php echo($livro['EDITORA']);?> (<?php echo($livro['ANO']);?>)</div>
									<!--<div style="font-size:11px;"><strong>Qtde.:</strong> <?php //echo($livro['QTD_DISPONIVEL']);?></div>-->
                                    <div style="font-size:11px;"><strong>Edição:</strong> <?php echo($livro['EDICAO']);?></div>
									<div style="font-size:11px;"><strong>Preço:</strong> R$ <?php echo masc_real($livro['PRECO']); ?></div>
								</div>
								<div style="font-size:11px;">
									<a href="javascript:void(0);" class="red" onclick="showLivro(<?php echo($livro['IDLIVRO']);?>);">Visualizar</a>&nbsp;&nbsp;&nbsp;
									<a href="javascript:void(0);" class="red" onclick="modalAddPedidoItem(<?php echo($livro['IDLIVRO']);?>,1);"><?php echo ($idPedido == $livro['IDPEDIDO'] && $idPedido != '' ? 'Editar' : 'Adicionar'); ?></a>&nbsp;&nbsp;&nbsp;
									<a href="javascript:void(0);" class="red" onclick="modalAddComentario(<?php echo($livro['IDLIVRO']);?>);"><img src="<?php echo URL_IMG; ?>comment2.png" title="Comentários" alt="Comentários" /></a>
									<?php if($idPedido == $livro['IDPEDIDO'] && $idPedido != ''){ ?>
									<div style="float:right;margin-top:2px;"><img src="<?php echo(URL_IMG); ?>/v_mini.gif" title="Este livro já está adicionado na sua lista de aquisições" /></div>
									<?php } ?>
								</div>
							</div>
						</div>
						<?php } ?>
						</div>
					</div>
					<div id="listabusca" style="padding-left: 25px; display: none;">
					<table cellspacing="0" cellpadding="3" border="0" width="90%" class="extensions t95">
						<tr class="header" style="height:22px;">
							<th>Título</th>
							<th>Autor</th>
							<th>Editora</th>
							<th>Edição</th>
							<th>Preço (R$)</th>
							<th></th>
						</tr>
					<?php 
					$i = 1;
					foreach($livros as $livro) {
					?>
						<tr class="<?php echo $i%2==0?"l_i":"l_p"; ?>">
                            <td style="text-align: left;"><b><?php echo($livro['TITULO']);?></b></td>
							<td style="text-align: left;"><?php echo($livro['AUTOR']);?></td>
							<td style="text-align: left;"><?php echo($livro['EDITORA']);?> (<?php echo($livro['ANO']);?>)</td>
							<td style="text-align: right;"><?php echo($livro['EDICAO']);?></td>
							<td align="right"><?php echo(masc_real($livro['PRECO']));?></td>
							<td>
								<a href="javascript:void(0);" class="red" onclick="showLivro(<?php echo($livro['IDLIVRO']);?>);">Visualizar</a>&nbsp;&nbsp;&nbsp;
								<a href="javascript:void(0);" class="red" onclick="modalAddPedidoItem(<?php echo($livro['IDLIVRO']);?>,1);"><?php echo ($idPedido == $livro['IDPEDIDO'] && $idPedido != '' ? 'Editar' : 'Adicionar'); ?></a>&nbsp;&nbsp;&nbsp;
								<a href="javascript:void(0);" class="red" onclick="modalAddComentario(<?php echo($livro['IDLIVRO']);?>);"><img src="<?php echo URL_IMG; ?>comment2.png" title="Comentários" alt="Comentários" /></a>
								<?php if($idPedido == $livro['IDPEDIDO'] && $idPedido != ''){ ?>
								<div style="float:right;margin-top:2px;"><img src="<?php echo(URL_IMG); ?>/v_mini.gif" title="Este livro já está adicionado na sua lista de aquisições" /></div>
								<?php } ?>
							</td>
						</tr>
					<?php
					$i++;
					} 
					?>
					</table>
					<br /><br />
					</div>
					<?php } ?>
				</td>
			</tr>
			<tr><td></td><td><div style="margin:-30px 0 0 25px">Páginas: <?php montaPaginacaoPesquisa($nPaginas, $nPag, $limit_de, $exibir_pp, URL_EXEC . 'pesquisa/pesquisarLivros/', 'filtros_lista'); ?></div></td></tr>
			</table>
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
</body>
</html>