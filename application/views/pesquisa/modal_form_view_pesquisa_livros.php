<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
</head>
<body>        
	<div class="font_shadow_gray" style="margin-bottom:5px;"><h6>Dados do Livro</h6></div>
	<hr />
	<div id="box_group_view" class="modal">
		<div style="float:right;margin:-30px 0 0 0;"><?php echo get_value($dados, 'ATIVO_IMG'); ?></div>
		<div class="odd">
			<div id="label_view">ID Livro (#):</div>
			<div id="field_view"><?php echo(get_value($dados, 'IDLIVRO'));?></div>
		</div>
		<div>
			<div id="label_view">Titulo:</div>
			<div id="field_view"><?php echo(get_value($dados, 'TITULO'));?></div>
		</div>
		<div class="odd">
			<div id="label_view">Edição:</div>
			<div id="field_view"><?php echo(get_value($dados, 'EDICAO'));?></div>
		</div>
		<div>
			<div id="label_view">Autor:</div>
			<div id="field_view"><?php echo(get_value($dados, 'AUTOR'));?></div>
		</div>
		<div class="odd">
			<div id="label_view">Editora:</div>
			<div id="field_view"><?php echo(get_value($dados, 'EDITORA'));?></div>
		</div>
		<div class="odd">
			<div id="label_view">Ano:</div>
			<div id="field_view"><?php echo(get_value($dados, 'ANO'));?></div>
		</div>
	</div>	
	<div style="margin-top:25px">
		<hr />
		<div class="inline top"><button onclick="parent.close_modal();">OK</button></div>
		<div class="inline top" style="margin:7px 0 0 5px">ou <a href="javascript:void(0);" onclick="parent.close_modal();">cancelar</a></div> 
	</div>
</body>
</html>