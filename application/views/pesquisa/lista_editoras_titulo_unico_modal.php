<script>
//
<?php if (count($editoras) == 1) { ?>
	
	// $('btnSaveForm').setAttribute('disabled', 'disabled');
	// $('form_cad').submit();
<?php } ?>
//});

</script>
<div id="container" style="overflow-y: auto; ">
    <div class="main_content_left">
	    <form name="form_cad" id="form_cad" method="post" action="javascript:indicarLivro();">
            <fieldset style="width:540px; margin-left:20px;height:100%;">
                <legend><b>Selecione a editora</b></legend>
                <div id="fs_div">
                    <table id="tabelaCadastros2" align="center" border="0" width="100%">
                    
                    	<tr>
                    		<th align="left" width="5%"></th>
                    		<th align="left" width="95%">Nome da Editora</th>
                    	</tr>
                    	<?php
                    	if (count($editoras) > 0) {
                    		for ($i = 0; $i < count($editoras); $i++) {
                    	?>
                        <tr>
                            <td><input type="radio" name="ideditora" id="ideditora<?php echo $editoras[$i]['IDEDITORA']; ?>" value="<?php echo $editoras[$i]['IDEDITORA']; ?>" <?php //echo $i==0?'':'checked="checked"'; ?>></td>
                            <td><label for="ideditora<?php echo $editoras[$i]['IDEDITORA']; ?>"><?php echo $editoras[$i]['NOMEEDITORA']; ?></label></td>
                        </tr>
                        <?php } ?>
                        <tr>
                            <td colspan="2" style="text-align: center; padding-top:15px;">
                                <input class="buttonPadrao" type="button" value="  Cancelar  " onclick="$('ligthbox_bg').setStyle({display : 'none'});" />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input class="buttonPadrao" id="btnSaveForm" name="btnSaveForm" type="submit" value="  Salvar  " />
                            </td>
                        </tr>
                        <input type="hidden" name="idlivro" id="idlivro" value="{idlivro}" />
                        <input type="hidden" name="nPag" id="nPag" value="{nPag}" />
                        <?php } else { ?>
                        <tr>
                            <td colspan="2">Não foram encontradas informações para esta obra.</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center; padding-top:15px;">
                                <input class="buttonPadrao" type="button" value="  Fechar  " onclick="$('ligthbox_bg').setStyle({display : 'none'});" />
                             </td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </fieldset>
        </form>
    </div>
</div><!-- #container-->

