<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title><?php echo TITLE_SISTEMA; ?></title>
        <link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
        <script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
        <script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
		<script src="<?php echo URL_JS; ?>biblioteca.js" type="text/javascript"></script>
    </head>
    <body>
        <div id="wrapper">
            <?php monta_header(1); ?>
            <?php monta_menu($this->session->userdata('tipoUsuario')); ?>

            <div class="divtitulo">
                <div class="divtitulo2">
                    >> PESQUISAR ESTABELECIMENTOS
                </div>
            </div>

            <div id="middle">
                <div id="container" style="text-align:center;">
                    <div class="main_content_center" style="margin-top:10px;">
                        <?php
                        $erros = Array();

                        if (isset($outrosErros)) {
                            if (is_array($outrosErros)) {
                                for ($i = 0; $i < count($outrosErros); $i++) {
                                    array_push($erros, $outrosErros[$i]);
                                }
                            } else {
                                array_push($erros, $outrosErros);
                            }
                        }

                        exibe_validacao_erros($erros);
                        ?>
                        <form name="form_pesq" id="form_pesq" method="post" action="<?php echo URL_EXEC; ?>pesquisa/pesquisarEstab/" onsubmit="return validaForm_pesqEstab()">
                            <div id="fs_div">
                                <table id="tabelaCadastros" align="center" border="0">
                                    <tr>
                                        <td>
										<!--<div class="filtro_campo">
									<span class="margin">Estado:</span>
									<select name="filtro_estado" id="filtro_estado" class="select" style="width:50px;" onchange="ajaxGetCidades(this.value);"><?php echo($options_estados); ?></option>
									</select>
								</div>
                                <div class="filtro_campo">
									<span class="margin">Cidade:</span>
									<select name="filtro_cidade" id="filtro_cidade" class="select" style="width:200px;">
										<option>-- Selecione um Estado --</option>
										<?php //echo($options_cidades); ?>
									</select>
								</div>-->
                                           Estado: <br />
                                            <select class="select" name="uf" id="uf" style="width:60px;">
                                                <option value="" <?php echo set_select('uf', '', TRUE); ?> ></option>
												<?php
												foreach ($ufs as $key => $uf) {
													echo '<option value="' . $uf['IDUF'] . '" ' . set_select('uf', $uf['IDUF']) . '>' . $uf['IDUF'] . '</option>';
												}
												?>
                                            </select>
                                        </td>
										<td>
                                            Cidade: <br />
                                            <select class="select" name="cidade" id="cidade" style="width:120px;">
                                                <option value="" <?php echo set_select('cidade', '', TRUE); ?> ></option>
												<?php
												foreach ($cidades as $key => $cidade) {
												echo '<option value="' . $cidade['IDCIDADE'] . '" ' . set_select('cidade', $cidade['IDCIDADE']) . '>' . $cidade['IDCIDADE'] . '</option>';
												}
												?>
                                            </select>
                                        </td>
                                        <td style="padding-left:10px;">
                                            Razão Social / Nome Fantasia: <br />
                                            <input class="inputText" type="text" name="pesquisa" id="pesquisa" value="<?php echo set_value('pesquisa'); ?>" onfocus="focusLiga(this)" onblur="focusDesliga(this)" style="text-transform:none; width:320px;" />
                                        </td>
										<td style="padding-left:10px;">
                                            Habilitado no edital atual: <br />
                                            <select class="select" name="habilitado" id="habilitado" style="width:60px;">
                                                <option value=""></option>
                                                <option value="S" <?php echo((get_value($form_data, 'habilitado') == 'S') ? 'selected="selected"' : '');?>>SIM</option>
                                                <option value="N" <?php echo((get_value($form_data, 'habilitado') == 'N') ? 'selected="selected"' : '');?>>NÃO</option>
                                            </select>
                                        </td>
                                        <td><input class="buttonPadrao" style="margin-top: 15px;" id="btnPesq" name="btnPesq" type="submit" value="  Pesquisar  " /></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="text-align: center; padding-top: 10px; padding-bottom: 20px;">
                                            <input type="radio" name="tipo_pesquisa" id="rd_editora" value="cadeditora" <?php echo set_radio('tipo_pesquisa', 'cadeditora', TRUE); ?> /><label for="rd_editora">Editora</label>
                                            <input type="radio" name="tipo_pesquisa" id="rd_biblioteca" value="cadbiblioteca" <?php echo set_radio('tipo_pesquisa', 'cadbiblioteca'); ?> /><label for="rd_biblioteca">Biblioteca</label>
                                            <input type="radio" name="tipo_pesquisa" id="rd_distribuidor" value="caddistribuidor" <?php echo set_radio('tipo_pesquisa', 'caddistribuidor'); ?> /><label for="rd_distribuidor">Distribuidor</label>
                                            <input type="radio" name="tipo_pesquisa" id="rd_ponto_venda" value="cadpdv" <?php echo set_radio('tipo_pesquisa', 'cadpdv'); ?> /><label for="rd_ponto_venda">Ponto de Venda</label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </form>
<?php
if ($search) {
    ?>
                            <table cellspacing="0" align="center" cellpadding="0" class="extensions t95">
                                <thead>
                                    <tr class="header">
                                        <th>UF</th>
                                        <th>Cidade</th>
                                        <th>CPF - CNPJ</th>
                                        <th>Razão Social / Nome</th>
                                        <th>Nome Fantasia</th>
                                        <th>Habilitado</th>
                                        <th>Opções</th>
                                    </tr>
                                    <tr class="header-separator">
                                        <td colspan="6">&nbsp;</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    {dados}
                                    <tr class="{CORLINHA}">
                                        <td class="no-wrap tdcenter">{IDUF}</td>                                        
                                        <td class="no-wrap pr_20 tdleft">{NOMECIDADE}</td>
                                        <td class="no-wrap tdcenter">{CPF_CNPJ}</td>
                                        <td class="w100 pr_20 tdleft">{RAZAOSOCIAL}</td>
                                        <td class="no-wrap pr_20 tdleft">{NOMEFANTASIA}</td>
                                        <td class="no-wrap pr_20 tdcenter">{HABILITADO}</td>
                                        <td class="tdleft">
                                            <a href="<?php echo URL_EXEC; ?>pesquisa/exibeDadosPesquisa/{IDUSUARIOTIPO}/{IDUSUARIO}"><img src="<?php echo URL_IMG; ?>search_16.png" title="VISUALIZAR" border="0" alt="VISUALIZAR" style="cursor:pointer;" /></a>
                                        </td>
                                    </tr>
                                    {/dados}

    <?php
    if ($vazio == TRUE) {
        ?>
                                        <tr>
                                            <td colspan="7" class="no-wrap tdcenter">Nenhum estabelecimento encontrado.</td>
                                        </tr>
                                        <?php
                                    } else {
                                        ?>
                                        <tr class="noEfeito">
                                            <td colspan="7" class="no-wrap tdcenter">
                                                <div style="display: inline; float: left;">Total Registros: <?php echo $qtdReg; ?></div>
                                        <?php montaPaginacaoPesquisa($nPaginas, $nPag, $limit_de, $exibir_pp, URL_EXEC . 'pesquisa/pesquisarEstab/', 'form_pesq'); ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                                        <?php } ?>
                    </div>
                </div><!-- #container-->
            </div><!-- #middle-->
        </div><!-- #wrapper -->
                                <?php monta_footer(); ?>
    </body>
</html>