<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>estilo_z.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>tab_z1.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>menu.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
</head>
<body>
	<div class="popCad" id="divCidades"></div>
	<div class="popCad" id="divEditoras"></div>
	<div id="wrapper">
		<?php 
		monta_header(1);
		monta_menu($this->session->userdata('tipoUsuario'));
		?>
        <div class="divtitulo">
			<div class="divtitulo2">
				>> VISUALIZAÇÃO DE {tipousuario}
			</div>
		</div>
		<div id="middle">
			<div id="container">
				<div class="main_content_left" style="margin-top:10px; margin-bottom:20px;">
					<form>
						<fieldset style="width:580px; margin-left:20px;">
							<legend><b>Informações</b></legend>
							<div id="fs_div">
								<table id="tabelaCadastros2" align="center" border="0">
									<tr>
										<td colspan="4" class="areaCadastro2"><img src="<?php echo URL_IMG; ?>geral.png" style="padding-right:10px;" /> Dados gerais</td>
									</tr>
									<tr>
										<td style="padding-top:10px;"><?php echo ($tipopessoa == 'PJ' ? 'Razão Social:' : 'Nome:'); ?></td>
										<td colspan="3" style="padding-top:10px;"><input class="inputText inputDisab" readonly="readonly" type="text" style="width:410px;" value="{razaosocial}" /></td>
									</tr>
									<?php
									if ($tipopessoa == 'PJ')
									{
										?>
										<tr>
											<td>Nome Fantasia:</td>
											<td colspan="3"><input class="inputText inputDisab" readonly="readonly" type="text" style="width:410px;" value="{nomefantasia}" /></td>
										</tr>
										<?php
									}
									?>
									<tr>
										<td>Telefone Geral:</td>
										<td colspan="3">
											<input class="inputText inputDisab" readonly="readonly" type="text" style="width:130px;" value="{telefone1}" /> 
										</td>
									</tr>
									<tr>
										<td>E-mail Geral:</td>
										<td colspan="3"><input class="inputText inputDisab" readonly="readonly" type="text" value="{email}" style="text-transform:none;width:323px;" /></td>
									</tr>
									<tr>
										<td>Site:</td>
										<td colspan="3"><input class="inputText inputDisab" readonly="readonly" type="text" value="{site}" style="text-transform:none;width:323px;" /></td>
									</tr>
									<!-- ********     MIOLO        *********  -->
									<?php if($tipo == 2){ ?>
									<tr>
										<td colspan="4" class="areaCadastro2" style="padding-top:10px;"><img src="<?php echo URL_IMG; ?>listbook.png" style="padding-right:10px;" /> Dados do responsável financeiro e responsável pela biblioteca</td>
									</tr>
									<tr>
										<td>Nome Responsável:</td>
										<td colspan="3">
											<input class="inputText inputDisab" readonly="readonly" type="text" style="width:410px;" value="{nomeresp}" />
										</td>
									</tr>
									<tr>
										<td>Telefone Responsável:</td>
										<td colspan="3">
											<input class="inputText inputDisab" readonly="readonly" type="text" style="width:410px;" value="{foneresp}" />
										</td>
									</tr>
									<tr>
										<td>Email Responsável:</td>
										<td colspan="3">
											<input class="inputText inputDisab" readonly="readonly" type="text" style="width:410px;" value="{emailresp}" />
										</td>
									</tr>
									<tr>
										<td>Responsável Financeiro:</td>
										<td colspan="3">
											<input class="inputText inputDisab" readonly="readonly" type="text" style="width:410px;" value="{respfinan}" />
										</td>
									</tr>
									<tr>
										<td>Email:</td>
										<td colspan="3">
											<input class="inputText inputDisab" readonly="readonly" type="text" style="width:410px;" value="{emailfinan}" />
										</td>
									</tr>
									<tr>
										<td>Fone:</td>
										<td colspan="3">
											<input class="inputText inputDisab" readonly="readonly" type="text" style="width:410px;" value="{fonefinan}" />
										</td>
									</tr>
									
									
									<?php }elseif($tipo == 4){  ?>
									<tr>
										<td colspan="4" class="areaCadastro2" style="padding-top:10px;"><img src="<?php echo URL_IMG; ?>listbook.png" style="padding-right:10px;" /> Dados do Responsável do Ponto de Venda</td>
									</tr>
									<tr>
										<td>Responsável PDV:</td>
										<td colspan="3">
											<input class="inputText inputDisab" readonly="readonly" type="text" style="width:410px;" value="{nomeresp}" />
										</td>
									</tr>
									<tr>
										<td>Telefone:</td>
										<td colspan="3">
											<input class="inputText inputDisab" readonly="readonly" type="text" style="width:410px;" value="{foneresp}" />
										</td>
									</tr>
									<tr>
										<td>Email:</td>
										<td colspan="3">
											<input class="inputText inputDisab" readonly="readonly" type="text" style="width:410px;" value="{emailresp}" />
										</td>
									</tr>
									
									
									<?php } ?>
									
									<!-- ******************************************** -->
									<tr>
										<td colspan="4" class="areaCadastro2" style="padding-top:10px;"><img src="<?php echo URL_IMG; ?>endereco.png" style="padding-right:10px;" /> Endereço</td>
									</tr>
									<tr>
										<td style="padding-top:10px;">CEP:</td>
										<td style="padding-top:10px;" colspan="3">
											<input class="inputText inputDisab" readonly="readonly" type="text" style="width:80px;" value="{cep}"  />
										</td>
									</tr>
									<tr>
										<td>UF:</td>
										<td colspan="3">
											<input class="inputText inputDisab" readonly="readonly" type="text" style="width:40px;" value="{uf}" />
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											Cidade:
											<input class="inputText inputDisab" readonly="readonly" type="text" style="width:300px;" value="{cidade}" />
										</td>
									</tr>
									<tr>
										<td>Bairro:</td>
										<td colspan="3">
											<input class="inputText inputDisab" readonly="readonly" type="text" style="width:410px;" value="{bairro}" />
										</td>
									</tr>
									<tr>
										<td>Logradouro:</td>
										<td colspan="3">
											<input class="inputText inputDisab" readonly="readonly" type="text" style="width:410px;" value="{logradouro}" />
										</td>
									</tr>
									<tr>
										<td>Número:</td>
										<td colspan="3">
											<input class="inputText inputDisab" readonly="readonly" type="text" style="width:93px;" value="{end_numero}" />
											&nbsp;&nbsp;&nbsp;&nbsp;
											Complemento:
											<input class="inputText inputDisab" readonly="readonly" type="text" style="width:210px;" value="{end_complemento}" />
										</td>
									</tr>
                                    <tr>
                                        <td colspan="4" style="text-align: center; padding-top:30px;">
                                            <input class="buttonPadrao" type="button" value="  Voltar  " onclick="history.back();"/>
                                        </td>
                                    </tr>
								</table>
							</div>
						</fieldset>
					</form>
				</div>
			</div><!-- #container-->
		</div><!-- #middle-->
	</div><!-- #wrapper -->
	<?php monta_footer(); ?>
</body>
</html>