﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript" language="javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript" language="javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript" language="javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.validationengine.js" type="text/javascript" language="javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.validationengine.pt_BR.js" type="text/javascript" language="javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript" language="javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			// Validação e máscaras
			$("#form_default").validationEngine({ inlineValidation:false , promptPosition : "centerRight", scroll : false });
			
			// máscaras
			$('input:text').setMask(); 
   
			// Focus no primeiro campo
			$("#data").focus();
		});
	</script>
</head>
<body>
	<div><?php mensagem('warning', '', 'Ao marcar este livro como inexigível todos os pedidos poderão ser conferidos sem a necessisade de preenchê-lo.', false, 'margin-bottom:10px;');?></div>
	<br />
	<form action="<?php echo URL_EXEC; ?>livro/marca_inexigibilidade_livro_proccess" name="form_default" id="form_default" method="post">
	<input type="hidden" name="idlivro" id="idlivro" value="<?php echo get_value($arq, 'IDLIVRO')?>" />
	<div class="font_shadow_gray" style="margin-bottom:5px;"><h4>Dados do Livro</h4></div>
	<hr />
	<div id="box_group_view" class="inline top" style="margin-bottom:30px;">			
		<div class="odd">
			<div id="label_view">ID Livro(#): </div>
			<div id="field_view" style="width:480px;"><?php echo(get_value($arq, 'IDLIVRO'));?></div>
		</div>
		<div>
			<div id="label_view">Titulo: </div>
			<div id="field_view" style="width:460px;"><?php echo(get_value($arq, 'TITULO'));?></div>
		</div>
		<div class="odd">
			<div id="label_view">Autor: </div>
			<div id="field_view" style="width:460px;"><?php echo(get_value($arq, 'AUTOR'));?></div>
		</div>
		<div>
			<div id="label_view">Editora: </div>
			<div id="field_view" style="width:460px;"><?php echo (get_value($arq, 'RAZAOSOCIAL'));?></div>
		</div>
	</div>
	<br>
	<div class="inline top" style="margin-top:2px">
		<input type="checkbox" name="inexigivel" id="inexigivel" class="inline top" style="margin:2px 5px 0 0;" value="N" <?php echo (get_value($arq, 'PEDIDO_OBRIGATORIO') == "N") ? 'checked="checked"' : '';?> />
	</div>
	<div class="inline top" style="width:100px; padding-top:2px">Livro Inexigível</div>	
	<div style="margin-top:20px">
		<hr />
		<div class="inline top"><input type="submit" value="Salvar" /></div>
		<div class="inline middle" style="padding:7px 0 0 5px">ou <a href="javascript:void(0)" onclick="$('#form_default').validationEngine('hide');parent.close_modal();">cancelar</a></div>
	</div>
	</form>
</body>
</html>
