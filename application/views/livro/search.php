<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>binac.css" />
	<script src="<?php echo URL_JS; ?>jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.tablesorter.js" type="text/javascript"></script>
    <script src="<?php echo URL_JS; ?>jquery.global.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.needed.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>jquery.meiomask.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			// Validacaoo e mascaras
			$("input:text").setMask();
		});
	</script>
</head>
<body>
	<?php monta_header(1); ?>
	<?php monta_menu($this->session->userdata('tipoUsuario')); ?>
	<?php add_elementos_CONFIG(); ?>
	<div id="page_content_wide">
		<div id="inside_content">
			<h1 class="font_shadow_gray inline top" style="margin:10px 15px 0 0;">Livros</h1>
			<div id="module_menu" class="inline top">
				<div class="inline">
					<img src="<?php echo URL_IMG;?>icon_excel.gif" style="margin-top:2px;" />
					<a href="javascript:void(0);" onclick="download_excel('<?php echo URL_EXEC?>livro/download_excel');" class="black inline" title="Exporte a lista total de registros para o formato CSV"><h5>Exportar para Excel</h5></a>
				</div>
				<div class="inline">
					<img src="<?php echo URL_IMG;?>icon_bloqueio.png" style="margin-top:2px;" />
					<a href="<?php echo URL_EXEC;?>adm/bloqueiolivroindicacao" class="black inline"><h5>Bloqueio de Indicação</h5></a>
				</div>
			</div>
			<form id="form_filter" action="<?php echo(URL_EXEC);?>livro/search" method="post">
				<div id="content">
					<div class="inline">
						<img src="<?php echo URL_IMG;?>icon_filter.png" style="float:left;margin:4px 5px 0 0" />
						<h5 class="black inline" style="margin:3px 0 0 0;">Filtros</h5>
					</div>
					<div id="label">ID(#): </div>
					<div id="field"><input type="text" name="l__idlivro" id="l__idlivro" alt="" class="mini" value="<?php echo get_value($filtros, 'l__idlivro');?>" style="width:50px" /></div>
					<div id="label">Status: </div>
					<div id="field">
						<select class="mini" name="l__confirmacao" id="l__confirmacao" style="width:100px;">
							<option value="" <?php echo (get_value($filtros, 'l__confirmacao') == '') ? 'selected="selected"' : '';?>></option>
							<option value="S" <?php echo (get_value($filtros, 'l__confirmacao') == 'S') ? 'selected="selected"' : '';?>>Confirmado</option>
							<option value="N" <?php echo (get_value($filtros, 'l__confirmacao') == 'N') ? 'selected="selected"' : '';?>>Não confirmado</option>
						</select>
					</div>
					<div id="label">Inexigível: </div>
					<div id="field">
						<select class="mini" name="l__pedido_obrigatorio" id="l__pedido_obrigatorio" style="width:55px;">
							<option value="" <?php echo (get_value($filtros, 'l__pedido_obrigatorio') == '') ? 'selected="selected"' : '';?>></option>
							<option value="S" <?php echo (get_value($filtros, 'l__pedido_obrigatorio') == 'S') ? 'selected="selected"' : '';?>>Não</option>
							<option value="N" <?php echo (get_value($filtros, 'l__pedido_obrigatorio') == 'N') ? 'selected="selected"' : '';?>>Sim</option>
						</select>
					</div>
					<div id="label">Edital: </div>
					<div id="field">
					<select class="mini"  name="p__idprograma" id="p__idprograma" style="width:100px;">
							<option value="" <?php echo (get_value($filtros, 'p__idprograma') == '') ? 'selected="selected"' : '';?>></option>
							<option value="1" <?php echo (get_value($filtros, 'p__idprograma') == '1') ? 'selected="selected"' : '';?>>Livro Popular v1</option>
							<option value="2" <?php echo (get_value($filtros, 'p__idprograma') == '2') ? 'selected="selected"' : '';?>>Livro Popular - 4º Edital</option>
						</select>
						
					</div>
					<!--<div id="label">Situação no Edital: </div>
					<div id="field">
						<select class="mini"  name="p__conferido_pdv" id="p__conferido_pdv" style="width:90px;">
							<option value=""  <?php echo (get_value($filtros, 'p__conferido_pdv') == '') ? 'selected="selected"' : '';?>></option>
							<option value="S" <?php echo (get_value($filtros, 'p__conferido_pdv') == 'S') ? 'selected="selected"' : '';?>>Incluido</option>
							<option value="N" <?php echo (get_value($filtros, 'p__conferido_pdv') == 'N') ? 'selected="selected"' : '';?>>Não Incluido</option>
						</select>
					</div>-->
					<div id="label">ISBN: </div>
					<div id="field"><input type="text" name="l__isbn" id="l__isbn" alt="" class="mini" value="<?php echo get_value($filtros, 'l__isbn');?>" style="width:150px" /></div>
					<div id="label">Titulo: </div>
					<div id="field"><input type="text" name="l__titulo" id="l__titulo" alt="" class="mini" value="<?php echo get_value($filtros, 'l__titulo');?>" style="width:150px" /></div>
					<div id="label">Autor: </div>
					<div id="field"><input type="text" name="l__autor" id="l__autor" alt="" class="mini" value="<?php echo get_value($filtros, 'l__autor');?>" style="width:150px" /></div>
					<div id="label">Editora: </div>
					<div id="field"><input type="text" name="e__razaosocial" id="e__razaosocial" alt="" class="mini" value="<?php echo get_value($filtros, 'e__razaosocial');?>" style="width:150px" /></div>
					<div id="label"></div>
					<div id="field"><input type="submit" class="mini" value="Enviar" /></div>
				</div>				
			</form>
			<?php echo($module_table); ?>
		</div>
	</div>
</body>
</html>