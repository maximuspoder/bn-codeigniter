<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE_SISTEMA; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>style.css" />
	<script src="<?php echo URL_JS; ?>prototype.js" type="text/javascript"></script>
	<script src="<?php echo URL_JS; ?>global.js" type="text/javascript"></script>
</head>
<body>

	<div id="wrapper">
	
		<?php monta_header(0); ?>
		
		<div id="middle">

			<div id="container" style="text-align:center;">
                
				<div class="main_content_center" style="margin-top:40px; margin-bottom:40px; position: relative;  width: 100%;">
					
                    <table border='0' cellpadding="0" cellspacing="0" style="width: 900px; height: 100%; margin: 0 auto; " align="center">
                        <thead>
                            <tr>
                                <td>
                                    <div style="font-size: 22px; margin: 20px 0 20px 0; color: #363636;">
                                       <i> SELECIONE UM EDITAL: </i>
                                    </div>   
                                    <a href="<?php echo URL_EXEC; ?>home/sair" style=" font-size: 16px; cursor: pointer; float: right; margin-top: -35px; color: #9F0A04;">Sair</a>
                                </td>
                            </tr>
                        </thead>
                        <tr>
                            <td> 
                                {editais}
                                <div class="edital_home" onclick="window.location.href='<?php echo URL_EXEC; ?>home/edital/{IDPROGRAMA}'">
                                    <img src="<?php echo URL_IMG; ?>edital.png" />
                                    <span style="position: absolute; margin: 25px 0 0 10px ; font-weight: bold; text-transform: uppercase;">{DESCPROGRAMA}</span>
                                </div> 
                                {/editais}
                                <div class="edital_home" style="text-align: center; " onclick="window.location.href='<?php echo URL_EXEC; ?>home/edital/0'">
                                    <span style="margin: 25px 0 0 10px; font-weight: bold; text-transform: uppercase;">Nenhum</span>
                                </div> 
                            </td>                        
                        </tr>
                        <tr>
                            <td> 
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td> 
                                 <p>Para mais informações, escreva para <i>editais2011@bn.br</i>. </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h2>Atenção:</h2>
                                <div>
                                    O portal da FBN agora passa a atender mais de um edital.
                                    Assim,  algumas ações só poderão ser realizadas quando algum edital for selecionado. 
                                    Você deverá fazer esta escolha sempre que entrar no portal. 
                                    Para selecionar um edital basta clicar sobre o edital desejado, na listagem acima.
                                    Para alternar basta, a qualquer instante, clicar sobre o nome do edital que estará no canto superior a direita de sua tela conforme a figura abaixo.
                                    Algumas funcionalidades só estarão disponíveis para quem estiver habilitado.
                                    <br/><br/>
                                </div>
                                <img src="<?php echo URL_IMG; ?>info_editais.png" />
                            </td>
                        </tr>
                    </table>
                   
				</div>
				
			</div><!-- #container-->

		</div><!-- #middle-->

	</div><!-- #wrapper -->

	<?php  monta_footer(); ?>

</body>
</html>