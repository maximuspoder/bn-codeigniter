function habilitaEdital(idPrograma)
{
	if ($('habilitar_cad_' + idPrograma).checked == true)
	{
		window.location.href = $F('URL_EXEC') + 'home/habilita/' + idPrograma;
	}
	else
	{
		alert("ATENÇÃO!\n\nVocê deve confirmar o termo de declaração.");
	}
}

//calcula centro para criar window pwc
function calculaTop(altura)
{
	var tamHeight = windowIntSize('h');
	tamHeight     = (tamHeight - altura) - 160;
	return parseInt(tamHeight / 2);
}

function calculaLeft(largura)
{
	var tamWidth = windowIntSize('w');
	tamWidth     = tamWidth - largura;
	return parseInt(tamWidth / 2);
}

function windowIntSize(type)
{
	var type = (type) ? type : "";
	
	if (document.all)
	{
		//pegando o Altura tela no IE.
		if (!document.documentElement.clientHeight)
			var height = document.body.clientHeight;                        
		else
			var height = document.documentElement.clientHeight;        
			
		//pegando a Largura tela no IE.                
		if (!document.documentElement.clientWidth)
			var width = document.body.clientWidth;                        
		else
			var width = document.documentElement.clientWidth;                        
	}
	else
	{
		//pegando a Altura tela no FF.            
			var height = window.innerHeight;
		//pegando a Largura tela no FF.            
			var width  = window.innerWidth;                
	}    
	
	if (type.toLowerCase() == "w")
	{
		return width;
	}
	else if (type.toLowerCase() == "h")
	{
		return height;
	}
	else
	{
		var arr = new Array(2);
		arr[0] = width;
		arr[1] = height;            
		return arr;
	}
}

//focus, mudança de cor nos inputs
function focusLiga(input)
{
   input.className = 'inputTextFocus';
}

function focusDesliga(input)
{
   input.className = 'inputText';
}

//loading centralizada nos ajax
function LoadingX()
{
	$('divLoadingX').style.display = 'block';
}

function LoadedX()
{
	$('divLoadingX').style.display = 'none';
}

//retorna para login quando sessao expira
function sessaoExpirada()
{
	alert('ATENÇÃO:\n\nVocê precisa estar logado para acessar esta área!');
	
	if(window.opener == null)
	{
		top.location.replace($F('URL_EXEC') + 'home/sair');
	}
	else
	{
		window.opener.top.location.replace($F('URL_EXEC') + 'home/sair');
		setTimeout('self.close();', 200);
	}
}

function validaRetorno(str)
{
	if(str.indexOf('.NO_SESSION.') == -1)
	{
		if(str.indexOf('.NO_PERMISSION.') == -1)
		{
			if(str.indexOf('.ERRO.') == -1)
			{
				return true;
			}
			else
			{
				var arrStr = str.split('||');
				alert('ATENÇÃO:\n\n' + arrStr[1]);
				return false;
			}
		}
		else
		{
			alert('ATENÇÃO:\n\nVocê não possui permissão para realizar esta operação.');
			return false;
		}
	}
	else
	{
		sessaoExpirada();
		return false;
	}
	
	return false;
}

function trim(str, charlist)
{
    str += '';
    var whitespace = " \n\r\t\f\x0b\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000";
    var l = str.length;
	
    for (i = 0; i < l; i++)
	{
        if (whitespace.indexOf(str.charAt(i)) === -1)
		{
			str = str.substring(i);
            break;
        }
    }
    
	l = str.length;
    for (i = l - 1; i >= 0; i--)
	{
        if (whitespace.indexOf(str.charAt(i)) === -1)
		{
            str = str.substring(0, i + 1);
            break;
		}
    }
    
    return whitespace.indexOf(str.charAt(0)) === -1 ? str : '';
}

function getValor(num)
{
	num = num.replace(".","");
	num = num.replace(",",".");
	
	if (!isNaN(num))
	{
		return num;
	}
	else
	{
		return 0;
	}
}

/********************************************************************
 *                              ARRAYS								*
 ********************************************************************/

function in_array(valor, arr)
{
	var i;
	var c = arr.length;
	
	for (i = 0; i < c; i++)
	{
		if (arr[i] == valor)
		{
			return true;
		}
	}
	
	return false;
}

//remove elemento de um array
function removeItemArray(originalArray, itemToRemove)
{
	var j = 0;
	
	while (j < originalArray.length)
	{
		if (originalArray[j] == itemToRemove)
		{
			originalArray.splice(j, 1);
		}
		else
		{
			j++;
		}
	}
	
	return originalArray;
}

/************************************************************************
 *                              VALIDAÇÕES								*
 ************************************************************************/

function validaEmail(e)
{
	var ok = "1234567890qwertyuiop[]asdfghjklzxcvbnm.@-_QWERTYUIOPASDFGHJKLZXCVBNM";
	for(i=0; i < e.length ;i++)
	{
		if(ok.indexOf(e.charAt(i))<0)
		{
			return false;
		}
	}
	
	//valida o formato do e-mail
	return (e.length == 0) || (e.search(/^\w+((-\w+)|(\.\w+)|-)*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) != -1);
}

function validaCPFCNPJ(tipoDoc, s)
{
	s = s.replace(".","");
	s = s.replace(".","");
	s = s.replace("/","");
	s = s.replace("-","");
	
	if(s == '')
	{
		return true;
	}
	
	if (s.length == 14 && tipoDoc == 'PJ')
	{
		soma1 = (s.charAt(0) * 5) +
				(s.charAt(1) * 4) +
				(s.charAt(2) * 3) +
				(s.charAt(3) * 2) +
				(s.charAt(4) * 9) +
				(s.charAt(5) * 8) +
				(s.charAt(6) * 7) +
				(s.charAt(7) * 6) +
				(s.charAt(8) * 5) +
				(s.charAt(9) * 4) +
				(s.charAt(10) * 3) +
				(s.charAt(11) * 2);
		resto = soma1 % 11;
		digito1 = resto < 2 ? 0 : 11 - resto;
		soma2 = (s.charAt(0) * 6) +
				(s.charAt(1) * 5) +
				(s.charAt(2) * 4) +
				(s.charAt(3) * 3) +
				(s.charAt(4) * 2) +
				(s.charAt(5) * 9) +
				(s.charAt(6) * 8) +
				(s.charAt(7) * 7) +
				(s.charAt(8) * 6) +
				(s.charAt(9) * 5) +
				(s.charAt(10) * 4) +
				(s.charAt(11) * 3) +
				(s.charAt(12) * 2);
		resto = soma2 % 11;
		digito2 = resto < 2 ? 0 : 11 - resto;
		return ((s.charAt(12) == digito1) && (s.charAt(13) == digito2));
	}
	else if (s.length == 11 && tipoDoc == 'PF')
	{
		soma1 = (s.charAt(0) * 10) +
				(s.charAt(1) * 9) +
				(s.charAt(2) * 8) +
				(s.charAt(3) * 7) +
				(s.charAt(4) * 6) +
				(s.charAt(5) * 5) +
				(s.charAt(6) * 4) +
				(s.charAt(7) * 3) +
				(s.charAt(8) * 2);
		resto = soma1 % 11;
		digito1 = resto < 2 ? 0 : 11 - resto;
		soma2 = (s.charAt(0) * 11) +
				(s.charAt(1) * 10) +
				(s.charAt(2) * 9) +
				(s.charAt(3) * 8) +
				(s.charAt(4) * 7) +
				(s.charAt(5) * 6) +
				(s.charAt(6) * 5) +
				(s.charAt(7) * 4) +
				(s.charAt(8) * 3) +
				(s.charAt(9) * 2);
		resto = soma2 % 11;
		digito2 = resto < 2 ? 0 : 11 - resto;
		return ((s.charAt(9) == digito1) && (s.charAt(10) == digito2));
	}
	else
	{
		return false;
	}
}

function validaNumero(num, maiorZero)
{
	num = num.replace(".","");
	num = num.replace(",",".");
	
	if (!isNaN(num))
	{
		if (maiorZero == true)
		{
			if (num > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return true;
		}
	}
	else
	{
		return false;
	}
}

function validaData(data)
{
	var ardt   = data.split("/");
	var ExpReg = new RegExp("(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/[12][0-9]{3}");
	var erro   = false;
	
	if (data.search(ExpReg) == -1)
	{
		erro = true;
	}
	else if ((ardt[1] == 4 || ardt[1] == 6 || ardt[1] == 9 || ardt[1] == 11) && (ardt[0] > 30))
	{
		erro = true;
	}
	else if (ardt[1] == 2)
	{
		if (ardt[0] > 28 && (ardt[2] % 4) != 0)
		{
			erro = true;
		}
		
		if (ardt[0] > 29 && (ardt[2] % 4) == 0)
		{
			erro = true;
		}
	}
	
	if (erro)
	{
		return false;
	}
	else
	{
		return true;
	}
}

/************************************************************************
 *                              MÁSCARAS								*
 ************************************************************************/
function maskHora(elem)
{
	var v = elem.value;
	
	if (v.length <= 5)
	{
		v = v.replace(/\D/g, "");
		v = v.replace(/^(\d\d)(\d)/g, "$1:$2");
		v = v.replace(/(\d{2})(\d)/, "$1:$2");
	}
	else
	{
		v = v.substring(0, 5);
	}
	
	elem.value = v;
}

function maskx(element, func)
{
	var value = element.value;
	element.value = func(value);
}

function msk_date(value)
{
	value = value.replace(/\D/g, "");
	value = value.replace(/^(\d{2})(\d)/, "$1/$2");
	value = value.replace(/^(\d{2})\/(\d{2})(\d)/, "$1/$2/$3");
	return value;
}

function msk_telefone(value)
{
	value = value.replace(/\D/g, "");
	value = value.replace(/^(\d{2})(\d)/, "($1) $2");
	value = value.replace(/^\((\d{2})\) (\d{4})(\d)/, "($1) $2.$3");
	return value;
}

function maskInteger(field,event)
{
	var ns = ((document.layers || document.getElementById) && (!document.all));
	var ie = document.all;
	var keySet = '0123456789';
	var actionSet = '0,8,13';
	var keyCode = (ie) ? window.event.keyCode : event.which;
	var key = String.fromCharCode(keyCode);
	if ((keySet.indexOf(key) == -1) && (actionSet.indexOf(keyCode) == -1) && (event.ctrlKey == false))
	{
		return false;
	}
	else
	{
		return true;
	}
}

function maskMoeda(field,event)
{
	var ns = ((document.layers || document.getElementById) && (!document.all));
	var ie = document.all;
	var keySet = '-0123456789';
	var actionSet = '0,8,13';
	var keyCode = (ie) ? window.event.keyCode : event.which;
	var key = String.fromCharCode(keyCode);
	var negative = false;
	var fieldValue = field.value;
	var newValue = "";
	var selStart = (ie) ? 0 : field.selectionStart;
	var selEnd = (ie) ? document.selection.createRange().text.length : field.selectionEnd;
	
	if ((parseFloat(fieldValue) < 0) || (field.value.charAt(0) == "-"))
	{
		negative = true;
	}
	
	if ((keySet.indexOf(key) == -1) && (actionSet.indexOf(keyCode) == -1))
	{
		return false;
	}
	else if (actionSet.indexOf(keyCode) != -1)
	{
		return true;
	}
	else if (field.value.length == field.maxLength)
	{
		return false;
	}
	else if ((key == "-") && (field.value.length > 0))
	{
		return false;
	}
	else if (selEnd > selStart)
	{
		return true;
	}
	else
	{
		fieldValue = field.value.replace(",","");
		fieldValue = fieldValue.replace("-","");
		while (fieldValue.indexOf('.') != -1) fieldValue = fieldValue.replace(".","");
		var inicio   = fieldValue.substr(0,((fieldValue.length-1)%3));
		var centavos = fieldValue.substr(fieldValue.length-1,1);
		var resto    = fieldValue.substr(((fieldValue.length-1)%3),fieldValue.length-1-((fieldValue.length-1)%3));
		
		if (negative)
		{
			newValue = newValue + "-";
		}
		
		if (inicio != "")
		{
			newValue = newValue + inicio;
		}
		
		for (i=0; i<resto.length; i++)
		{
			if (((i>0) && ((i%3)==0)) || ((i==0) && (inicio != "")))
			{
				newValue = newValue + '.';
			}
			
			newValue = newValue + resto.charAt(i);
		}
		
		if (fieldValue.length >= 2)
		{
			newValue = newValue + ',';
		}
		
		newValue = newValue + centavos;
		field.value = newValue;
	}
}

function myMaxLength(textAreaField, limit)
{
    var ta = $(textAreaField);

    if (ta.value.length >= limit)
    {
        ta.value = ta.value.substring(0, limit-1);
    }
}

function number_format(number, decimals, dec_point, thousands_sep) {
    // Formats a number with grouped thousands  
    // 
    // version: 1008.1718
    // discuss at: http://phpjs.org/functions/number_format    // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +     bugfix by: Michael White (http://getsprink.com)
    // +     bugfix by: Benjamin Lupton
    // +     bugfix by: Allan Jensen (http://www.winternet.no)    // +    revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    // +     bugfix by: Howard Yeend
    // +    revised by: Luke Smith (http://lucassmith.name)
    // +     bugfix by: Diogo Resende
    // +     bugfix by: Rival    // +      input by: Kheang Hok Chin (http://www.distantia.ca/)
    // +   improved by: davook
    // +   improved by: Brett Zamir (http://brett-zamir.me)
    // +      input by: Jay Klehr
    // +   improved by: Brett Zamir (http://brett-zamir.me)    // +      input by: Amir Habibi (http://www.residence-mixte.com/)
    // +     bugfix by: Brett Zamir (http://brett-zamir.me)
    // +   improved by: Theriault
    // *     example 1: number_format(1234.56);
    // *     returns 1: '1,235'    // *     example 2: number_format(1234.56, 2, ',', ' ');
    // *     returns 2: '1 234,56'
    // *     example 3: number_format(1234.5678, 2, '.', '');
    // *     returns 3: '1234.57'
    // *     example 4: number_format(67, 2, ',', '.');    // *     returns 4: '67,00'
    // *     example 5: number_format(1000);
    // *     returns 5: '1,000'
    // *     example 6: number_format(67.311, 2);
    // *     returns 6: '67.31'    // *     example 7: number_format(1000.55, 1);
    // *     returns 7: '1,000.6'
    // *     example 8: number_format(67000, 5, ',', '.');
    // *     returns 8: '67.000,00000'
    // *     example 9: number_format(0.9, 0);    // *     returns 9: '1'
    // *    example 10: number_format('1.20', 2);
    // *    returns 10: '1.20'
    // *    example 11: number_format('1.20', 4);
    // *    returns 11: '1.2000'    // *    example 12: number_format('1.2000', 3);
    // *    returns 12: '1.200'
    var n = !isFinite(+number) ? 0 : +number, 
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }    return s.join(dec);
}

function maskCnpj(elem)
{
	var v = elem.value;
	
	v = v.replace(/\D/g, "");
	v = v.replace(/^(\d{2})(\d)/, "$1.$2");
	v = v.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3");
	v = v.replace(/\.(\d{3})(\d)/, ".$1/$2");
	v = v.replace(/(\d{4})(\d)/, "$1-$2");
	
	elem.value = v;
}

function maskCpf(elem)
{
	var v = elem.value;
	
	v = v.replace(/\D/g, "");
	v = v.replace(/(\d{3})(\d)/, "$1.$2");
	v = v.replace(/(\d{3})(\d)/, "$1.$2");
	v = v.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
	
	elem.value = v;
}

/********************************************************************
 *                              GERAL								*
 ********************************************************************/

function validaForm_altSenha()
{
	var msg = '';
	
	if (trim($('senhaatual').value) == '')
		msg += 'A senha atual deve ser informada.\n';
	
	if (trim($('senhanova').value) == '' || $('senhanova').value.length < 6)
		msg += 'A nova senha deve ser informada com no mínimo 6 caracteres.\n';
	
	if (trim($('senhanova').value) != trim($('senhanova2').value))
		msg += 'Nova senha e a sua confirmação estão diferentes, verifique.\n';
	
	if (trim($('senhanova').value) == trim($('senhaatual').value))
		msg += 'A nova senha deve ser diferente da senha atual.\n';
	
	if (msg == '')
		return true;
	else
	{
		alert("ATENÇÃO:\n\n"+msg);
		return false;
	}
}

function validaForm_pesqEstab()
{
    var msg = '';
	
	if (trim($('pesquisa').value) == '' && trim($('uf').value) == '')
		msg += 'Razão Social / Nome Fantasia ou UF deve ser informado.\n';
	
	if ($('rd_editora').checked == false && $('rd_biblioteca').checked == false 
        && $('rd_distribuidor').checked == false && $('rd_ponto_venda').checked == false)
		msg += 'O Tipo da Pesquisa deve ser informado.\n';
    
    if (msg == '')
		return true;
	else
	{
		alert("ATENÇÃO:\n\n"+msg);
		return false;
	}
}

function validaForm_pesqLogsis()
{
    var msg = '';
	
	if ($F('filtro_resultado') == '' && $F('filtro_data_de') == '' && $F('filtro_data_ate') == '' && $F('filtro_hora_de') == '' && $F('filtro_hora_ate') == '')
	{
		msg += 'Informe ao menos um filtro.\n';
	}
	
	if ($F('filtro_data_de') != '' && $F('filtro_data_ate') == '' || $F('filtro_data_de') == '' && $F('filtro_data_ate') != '')
	{
		msg += 'Preencha os dois campos de data.\n';
	}
	
	if ($F('filtro_hora_de') != '' && $F('filtro_hora_ate') == '' || $F('filtro_hora_de') == '' && $F('filtro_hora_ate') != '')
	{
		msg += 'Preencha os dois campos de hora.\n';
	}
    
    if (msg == '')
	{
		if ($F('filtro_data_de') != $F('filtro_data_ate') && ($F('filtro_hora_de') != '' || $F('filtro_hora_ate') != ''))
		{
			$('filtro_hora_de').value = '';
			$('filtro_hora_ate').value = '';
			
			alert("INFO!\n\nPara datas diferentes o filtro hora é desconsiderado.");
		}
		
		return true;
	}
	else
	{
		alert("ATENÇÃO:\n\n"+msg);
		return false;
	}
}

// Ajax para abrir ligthbox com a vizualização da informações do livro
function detalharLivro(idLivro)
{
	if (trim(idLivro) != '')
	{
		var url = $F('URL_EXEC') + 'bnglobal/detalharLivro/' + idLivro + '/1' ;
		var pars = {'ms' : new Date().getTime()};
		var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: detalharLivro_onComplete});
	}
	else
	{
		alert("ATENÇÃO:\n\nLivro não encontrado.");
        $('btnSaveForm').disabled = false;
	}
}

function detalharLivro_onComplete(resp)
{
	var strRet = resp.responseText;

	LoadedX();

	if (validaRetorno(strRet))
	{        
        $('ligthboxContext').update(strRet);
        $('ligthbox_bg').setStyle({display : 'block'});
	}
    else
    {
        $('ligthbox_bg').setStyle({display : 'none'});
        alert('ERRO! \n\n Dados incorretos.');
    }
}

//adicionar ao programa livro
function abreAviso(url)
{	
	var url = $F('URL_EXEC') + url;
	var pars = {'ms' : new Date().getTime()};
	var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: abreAviso_onComplete});
	
}

function abreAviso_onComplete(resp)
{
	var strRet = resp.responseText;

	LoadedX();

	if (validaRetorno(strRet))
	{       
		
        $('ligthboxContext').update(strRet);
		$('ligthbox_bg').setStyle({display : 'block'});
	}
}

/**
* Returns the value of the selected radio button in the radio group, null if
* none are selected, and false if the button group doesn't exist
*
* @param {radio Object} or {radio id} el
* OR
* @param {form Object} or {form id} el
* @param {radio group name} radioGroup
*/
function $RF(el, radioGroup) {
    if($(el).type && $(el).type.toLowerCase() == 'radio') {
        var radioGroup = $(el).name;
        var el = $(el).form;
    } else if ($(el).tagName.toLowerCase() != 'form') {
        return false;
    }
 
    var checked = $(el).getInputs('radio', radioGroup).find(
        function(re) {return re.checked;}
    );
    return (checked) ? $F(checked) : null;
}