function salvarSniic(idBiblioteca)
{
    if($('observ').value != '')
    {
        $('motivoAction').value = $('observ').value;
        document.forms["form_cad"].submit();
    }else
    {
        alert('digite uma observação!');
    }    
    
    alert(document.forms["form_cad"].action);
    
}

function listaLogs(idbiborigem)
{
	var args = idbiborigem.split('|');
	
	var idbiblioteca = args[0];
	var origem = args[1];
	
    var url = $F('URL_EXEC') + 'adm/lista_log_gerenciamento/'+idbiblioteca;
    var pars = {'ms' : new Date().getTime(), 'origem': origem };
    var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: listaLogs_onComplete});
}

function listaLogs_onComplete(resp)
{
	var strRet = resp.responseText;
	LoadedX();
    $('ligthboxContext').update(strRet);
    $('ligthbox').setStyle({width  : '600px'});
    $('ligthbox').setStyle({height  : '560px'});
	$('ligthbox_bg').setStyle({display : 'block'});
}

function bloquearCadastro(idbiblioteca, origem, nPag)
{
    var url = $F('URL_EXEC') + 'biblioteca/bloquear_cadastro/'+idbiblioteca;
    var pars = {'ms' : new Date().getTime(), 'origem': origem, 'nPag': nPag };
    var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: bloquearCadastro_onComplete});
}

function bloquearCadastro_onComplete(resp)
{
	var strRet = resp.responseText;
	LoadedX();
    $('ligthboxContext').update(strRet);
    $('ligthbox').setStyle({width  : '650px'});
    $('ligthbox').setStyle({height  : '360px'});
	$('ligthbox_bg').setStyle({display : 'block'});
}

//Valida Formulário de gravação de log bloqueio
function validaForm_GravaLogBloqueio()
{
	var msg = '';
	
	if (trim($F('motivo')) == '') {
		msg += 'O motivo deve ser informado.';
	}
	
    if (msg == '')
	{
		return true;
	} else {
		alert("ATENÇÃO:\n\n" + msg);
		$('motivo').focus();
		return false;
	}
}


//Submita formulário de gravação da log bloqueio
function submitGravaLogBloqueio(idBiblioteca)
{
	var origem = $F('origem');
	var motivo = $F('motivo');
	var nPag = $F('nPag');
	
	if (trim(idBiblioteca) != '')
	{
		var url = $F('URL_EXEC') + 'biblioteca/bloqueia/' + idBiblioteca;
		var pars = {'ms' : new Date().getTime(), 'origem' : origem, 'motivo' : motivo, 'nPag' : nPag };
		var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: submitGravaLogBloqueio_onComplete});
	} else {
		alert("ATENÇÃO:\n\Biblioteca não encontrada.");
	}
	LoadedX();
}

function submitGravaLogBloqueio_onComplete(resp)
{
	var strRet = resp.responseText;
	
	if (validaRetorno(strRet))
	{
		if (strRet == 'OK')
		{
			alert("Bloqueado com sucesso!");
			$('ligthbox_bg').hide();
			window.location.href = $F('URL_EXEC') + 'adm/gerenciadorSniic/' + $F('nPag');
		}
		else
		{
			alert("Não foi possível enviar o pedido. Tente novamente.");
		}
	}
	else
	{
		LoadedX();
	}
}


function validaFormAlt_sniic()
{
	$('btnSaveForm').disabled = true;
	
	var msg = '';
	
	var tipoPessoa = Form.getInputs("form_cad", "radio", "tipopessoa").find(function(radio) { return radio.checked; }).value;
	
	if (trim($F('cnpjcpf')) == '')
	{
		if (tipoPessoa == 'PJ')
			msg += 'O CNPJ deve ser informado.\n';
		else
			msg += 'O CPF deve ser informado.\n';
	}
	else
	{
		if (! validaCPFCNPJ(tipoPessoa, $F('cnpjcpf')))
		{
			if (tipoPessoa == 'PJ')
				msg += 'CNPJ inválido.\n';
			else
				msg += 'CPF inválido.\n';
		}
	}
	
	if (trim($('razaosocial').value) == '')
	{
		if (tipoPessoa == 'PJ')
			msg += 'A razão social deve ser informada.\n';
		else
			msg += 'O nome deve ser informado.\n';
	}
	
	if ($('naturezajur').value == 0)
	{
		msg += 'O tipo de biblioteca deve ser informado.\n';
	}
	
	if (trim($('telefone1').value) == '')
	{
		msg += 'O telefone geral deve ser informado.\n';
	}
	
	if (trim($('email').value) == '')
	{
		msg += 'O e-mail geral deve ser informado.\n';
	}
	else
	{
		if ( ! validaEmail(trim($('email').value)))
			msg += 'O e-mail geral informado é inválido.\n';
	}
	
	if (trim($('idlogradouro').value) == '')
	{
		msg += 'O endereço deve ser informado.\n';
	}
	
	if (trim($('end_numero').value) == '')
	{
		msg += 'O número do endereço deve ser informado.\n';
	}
	
	if (trim($('nomeresp').value) == '')
	{
		msg += 'O nome do responsável deve ser informado.\n';
	}
	
	if (trim($('telefoneresp').value) == '')
	{
		msg += 'O telefone do responsável deve ser informado.\n';
	}
	
	if (trim($('emailresp').value) == '')
	{
		msg += 'O e-mail do responsável deve ser informado.\n';
	}	
	else
	{
		if ( ! validaEmail(trim($('emailresp').value)))
		{
			msg += 'O e-mail do responsável é inválido.\n';
		}
	}
	if (trim($('observacao').value) == '')
	{
		msg += 'O motivo da edição deve ser informado.\n';
	}	
	
	
	if (msg == '')
	{
		return true;
	}
	else
	{
		$('btnSaveForm').disabled = false;
		alert("ATENÇÃO:\n\n"+msg);
		return false;
	}
}
