/**************************************************************
 *
 *	SOCIOS
 *
 **************************************************************/

ARR_SOCIOS = new Array();

function addSocio()
{
	if (trim($F('socionome')) != '' && trim($F('sociocpf')) != '' &&  trim($F('sociopartic')) != '' && validaCPFCNPJ('PF', $F('sociocpf')))
	{
		var cont   = ARR_SOCIOS.length;
		var arrAux = new Array();
		var i;
		
		for (i = 0; i < cont; i++)
		{
			arrAux = ARR_SOCIOS[i].split('||');
			if (arrAux[0] == $F('sociocpf'))
			{
				alert("ATENÇÃO:\n\nCPF já adicionado.");
				return false;
			}
		}
		
		var dados = $F('sociocpf') + '||' + trim($F('socionome')) + '||' + $F('sociopartic') + '||' + $F('socioadmin');
		ARR_SOCIOS.push(dados);
		
		$('socios').value = ARR_SOCIOS.join('##');
		
		var tabSocios = $('tabSocios');
		var novaLinha = tabSocios.insertRow(-1);
		var classe    = (cont % 2 == 0 ? 'l_p' : 'l_i');
		novaLinha.className = classe;
		
        var novaCelula;
		
		novaCelula = novaLinha.insertCell(0);
		novaCelula.innerHTML = trim($F('socionome').toUpperCase());
		
		novaCelula = novaLinha.insertCell(1);
		novaCelula.innerHTML = $F('sociocpf');
		
		novaCelula = novaLinha.insertCell(2);
		novaCelula.innerHTML = $F('sociopartic');
		
		novaCelula = novaLinha.insertCell(3);
		novaCelula.innerHTML = ($F('socioadmin') == 'N' ? 'NÃO' : 'SIM');
		
		novaCelula = novaLinha.insertCell(4);
		novaCelula.innerHTML = '<img src="' + $F('URL_IMG') + 'x_mini.gif" onclick="delSocio(\'' + $F('sociocpf') + '\',this.parentNode.parentNode.rowIndex)" title="EXCLUIR SOCIO" border="0" alt="EXCLUIR SOCIO" style="cursor:pointer;" />';
		
		$('socionome').value   = '';
		$('sociocpf').value    = '';
		$('sociopartic').value = '';
		$('socioadmin').value  = 'N';
	}
	else
	{
		if ( ! validaCPFCNPJ('PF', $F('sociocpf')))
		{
			alert("ATENÇÃO:\n\nO CPF do sócio é inválido.");
		}
		else
		{
			alert("ATENÇÃO:\n\nInforme corretamente os dados do sócio.");
		}
	}
}

function delSocio(cpfDel,indexLinha)
{
	var cont   = ARR_SOCIOS.length;
	var arrAux = new Array();
	var newArr = new Array();
	var i;
	
	for (i = 0; i < cont; i++)
	{
		arrAux = ARR_SOCIOS[i].split('||');
		
		if (arrAux[0] != cpfDel)
		{
			newArr.push(ARR_SOCIOS[i]);
		}
	}
	
	ARR_SOCIOS = newArr;
	
	$('socios').value = ARR_SOCIOS.join('##');
	
	var tabSocios = $('tabSocios');
	tabSocios.deleteRow(indexLinha);
}

function listaSocios()
{
	ARR_SOCIOS = ($F('socios') != '' ? $F('socios').split('##') : new Array());
	
	var cont   = ARR_SOCIOS.length;
	var arrAux = new Array();
	var i;
	
	var tabSocios = $('tabSocios');
	var novaLinha;
	var novaCelula;
	var classe;
	
	for (i = 0; i < cont; i++)
	{
		arrAux = ARR_SOCIOS[i].split('||');
		
		novaLinha = tabSocios.insertRow(-1);
		classe    = (i % 2 == 0 ? 'l_p' : 'l_i');
		novaLinha.className = classe;
		
		novaCelula = novaLinha.insertCell(0);
		novaCelula.innerHTML = arrAux[1].toUpperCase();
		
		novaCelula = novaLinha.insertCell(1);
		novaCelula.innerHTML = arrAux[0];
		
		novaCelula = novaLinha.insertCell(2);
		novaCelula.innerHTML = arrAux[2];
		
		novaCelula = novaLinha.insertCell(3);
		novaCelula.innerHTML = (arrAux[3] == 'N' ? 'NÃO' : 'SIM');
		
		novaCelula = novaLinha.insertCell(4);
		novaCelula.innerHTML = '<img src="' + $F('URL_IMG') + 'x_mini.gif" onclick="delSocio(\'' + arrAux[0] + '\',this.parentNode.parentNode.rowIndex)" title="EXCLUIR SOCIO" border="0" alt="EXCLUIR SOCIO" style="cursor:pointer;" />';
	}
}

/**************************************************************
 *
 *	ATIVIDADES
 *
 **************************************************************/

ARR_ATUACAO = new Array();

function setAtuacao(idAtividade)
{
	if ($('atua_'+idAtividade).checked == true)
	{
		ARR_ATUACAO.push(idAtividade);
	}
	else
	{
		ARR_ATUACAO = removeItemArray(ARR_ATUACAO, idAtividade);
	}
	
	$('atuacao').value = ARR_ATUACAO.join('##');
}

function populaAtuacao()
{
	ARR_ATUACAO = ($F('atuacao') != '' ? $F('atuacao').split('##') : new Array());
	
	var cont   = ARR_ATUACAO.length;
	var i;
	
	for (i = 0; i < cont; i++)
	{
		$('atua_'+ARR_ATUACAO[i]).checked = true;
	}
}

/**************************************************************
 *
 *	REGIAO DE ATENDIMENTO
 *
 **************************************************************/

ARR_REGIOES = new Array();
ARR_CIDADES = new Array();

function setRegiao(uf, valor)
{
	var arrAux  = new Array();
	var arrAux2 = new Array();
	var cont    = ARR_REGIOES.length;
	var i;
	
	for (i = 0; i < cont; i++)
	{
		arrAux = ARR_REGIOES[i].split('|');
		
		if (arrAux[0] != uf)
		{
			arrAux2.push(ARR_REGIOES[i]);
		}
	}
	
	ARR_REGIOES = arrAux2;
	
	if (valor != 'X')
	{
		var dados = uf + '|' + valor;
		ARR_REGIOES.push(dados);
	}
	
	if (valor == 'N') //parcial
	{
		$('divreg_'+uf).setStyle({display : 'inline'});
	}
	else
	{
		$('divreg_'+uf).setStyle({display : 'none'});
	}
	
	$('regiaoatend').value = ARR_REGIOES.join('#');
}

function populaRegiao()
{
	ARR_REGIOES = ($F('regiaoatend') != '' ? $F('regiaoatend').split('#') : new Array());
	
	var cont   = ARR_REGIOES.length;
	var arrAux = new Array();
	var i;
	var nodes;
	
	for (i = 0; i < cont; i++)
	{
		arrAux = ARR_REGIOES[i].split('|');
		
		nodes = Form.getInputs("form_cad", "radio", "regiao_"+arrAux[0]);
		nodes.each(function(node){
				if (node.value == arrAux[1])
				{
					node.checked = true
				}
				
				if (arrAux[1] == 'N') //parcial
				{
					$('divreg_'+arrAux[0]).setStyle({display : 'inline'});
				}
		});
	}
}

function setCidade(idcidade)
{
	if ($('ckcid_'+idcidade).checked == true)
	{
		ARR_CIDADES.push(idcidade);
	}
	else
	{
		ARR_CIDADES = removeItemArray(ARR_CIDADES, idcidade);
	}
	
	$('cidadeatend').value = ARR_CIDADES.join('#');
}

function populaCidade()
{
	ARR_CIDADES = ($F('cidadeatend') != '' ? $F('cidadeatend').split('#') : new Array());
}

function openPopCidade(uf)
{
	var cidades = $F('cidadeatend');
	
	var url = $F('URL_EXEC') + 'autocad/listaCidades';
	var pars = {'uf' : uf , 'cidades' : cidades , 'ms' : new Date().getTime()};
	var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: openPopCidade_onComplete });
}

function openPopCidade_onComplete(resp)
{
	var strRet = resp.responseText;

	LoadedX();
	
	if (validaRetorno(strRet))
	{
		$('divCidades').setStyle({display : 'block'});
		$('divCidadesCombo').update(strRet);
	}
}

function fechaPopCidade()
{
	$('divCidadesCombo').update('');
	$('divCidades').setStyle({display : 'none'});
}

/**************************************************************
 *
 *	EDITORAS COM QUE TRABALHA
 *
 **************************************************************/

 ARR_EDITORAS = new Array();

function setEditoraTrab(ideditora)
{
	if ($('ckedit_'+ideditora).checked == true)
	{
		ARR_EDITORAS.push(ideditora);
	}
	else
	{
		ARR_EDITORAS = removeItemArray(ARR_EDITORAS, ideditora);
	}
	
	$('editorastrab').value = ARR_EDITORAS.join('#');
}

function populaEditoraTrab()
{
	ARR_EDITORAS = ($F('editorastrab') != '' ? $F('editorastrab').split('#') : new Array());
}

function openPopEditora()
{
	var editoras = $F('editorastrab');
	
	var url = $F('URL_EXEC') + 'autocad/listaEditoras';
	var pars = {'editoras' : editoras , 'ms' : new Date().getTime()};
	var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: openPopEditora_onComplete });
}

function openPopEditora_onComplete(resp)
{
	var strRet = resp.responseText;

	LoadedX();
	
	if (validaRetorno(strRet))
	{
		$('divEditoras').setStyle({display : 'block'});
		$('divEditorasCombo').update(strRet);
	}
}

function fechaPopEditora()
{
	$('divEditorasCombo').update('');
	$('divEditoras').setStyle({display : 'none'});
}

/**************************************************************
 *
 *	OUTROS
 *
 **************************************************************/

