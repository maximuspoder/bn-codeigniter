/**************************************************************
 *
 *	PRE CADASTRO
 *
 **************************************************************/

function onload_preautocad()
{
	if ($F('verifica') == 1)
	{
		$('linhatitulo').setStyle({display : ''});
		$('linhatipopessoa').setStyle({display : ''});
		$('linhacpfcnpj').setStyle({display : ''});
		$('linhacont').setStyle({display : ''});
		
		setTipoPessoa('');
	}
}

function setTipoCad()
{
	$('linhatitulo').setStyle({display : ''});
	$('linhatipopessoa').setStyle({display : ''});
}

function setTipoPessoa(tipo)
{
	if (tipo == '')
	{
		tipo = $F('tipopessoa_aux');
		
		var nodes = Form.getInputs("form_cad", "radio", "tipopessoa");
		nodes.each(function(node){
				if (node.value == tipo)
				{
					node.checked = true
				}
		});
	}
	else
	{
		$('cnpjcpf').value      = '';
	}
	
	if (tipo == 'PJ')
	{
		$('spantipopessoa').update('CNPJ');
		$('cnpjcpf').maxLength = 18;
		$('cnpjcpf').setAttribute("onKeyUp","maskCnpj(this)");
	}
	else
	{
		$('spantipopessoa').update('CPF');
		$('cnpjcpf').maxLength = 14;
		$('cnpjcpf').setAttribute("onKeyUp","maskCpf(this)");
	}
	
	$('linhacpfcnpj').setStyle({display : ''});
	$('linhacont').setStyle({display : ''});
	$('cnpjcpf').focus();
}

function validaForm_preCad()
{
	$('btnSaveForm').disabled = true;
	
	var msg = '';
	
	var tipoPessoa = Form.getInputs("form_cad", "radio", "tipopessoa").find(function(radio) { return radio.checked; }).value;
	
	if (trim($F('cnpjcpf')) == '')
	{
		if (tipoPessoa == 'PJ')
			msg += 'O CNPJ deve ser informado.\n';
		else
			msg += 'O CPF deve ser informado.\n';
	}
	else
	{
		if (! validaCPFCNPJ(tipoPessoa, $F('cnpjcpf')))
		{
			if (tipoPessoa == 'PJ')
				msg += 'CNPJ inválido.\n';
			else
				msg += 'CPF inválido.\n';
		}
	}
	
	if (msg == '')
	{
		return true;
	}
	else
	{
		$('btnSaveForm').disabled = false;
		alert("ATENÇÃO:\n\n"+msg);
		return false;
	}
}

/**************************************************************
 *
 *	BIBLIOTECA
 *
 **************************************************************/
 
function biblioteca_onload()
{
	$('cnpjcpf').focus();
	setTipoBiblioteca('');
}

function setTipoBiblioteca(tipo)
{
	if (tipo == '')
	{
		tipo = $F('tipopessoa_aux');
		
		var nodes = Form.getInputs("form_cad", "radio", "tipopessoa");
		nodes.each(function(node){
				if (node.value == tipo)
				{
					node.checked = true
				}
		});
	}
	else
	{
		$('cnpjcpf').value      = '';
		$('iestadual').value    = '';
		$('nomefantasia').value = '';
	}
	
	if (tipo == 'PJ')
	{
		$('spantipopessoa').update('CNPJ');
		$('cnpjcpf').maxLength = 18;
		$('cnpjcpf').setAttribute("onKeyUp","maskCnpj(this)");
		
		$('spannome').update('Razão Social');
		$('linhaie').setStyle({display : ''});
		$('linhanf').setStyle({display : ''});
	}
	else
	{
		$('spantipopessoa').update('CPF');
		$('cnpjcpf').maxLength = 14;
		$('cnpjcpf').setAttribute("onKeyUp","maskCpf(this)");
		
		$('spannome').update('Nome');
		$('linhaie').setStyle({display : 'none'});
		$('linhanf').setStyle({display : 'none'});
	}
	
	$('cnpjcpf').focus();
}

function validaForm_cadastroBiblioteca(fromSniic, migrar)
{
	$('btnSaveForm').disabled = true;
	
	var msg = '';
	var msgQuest = '';
	
	if (fromSniic == 0)
	{
		msgQuest = validaQuest();
	}
	
	var tipoPessoa = Form.getInputs("form_cad", "radio", "tipopessoa").find(function(radio) { return radio.checked; }).value;
	
	if (trim($F('cnpjcpf')) == '')
	{
		if (tipoPessoa == 'PJ')
			msg += 'O CNPJ deve ser informado.\n';
		else
			msg += 'O CPF deve ser informado.\n';
	}
	else
	{
		if (! validaCPFCNPJ(tipoPessoa, $F('cnpjcpf')))
		{
			if (tipoPessoa == 'PJ')
				msg += 'CNPJ inválido.\n';
			else
				msg += 'CPF inválido.\n';
		}
	}
	
	if (trim($('razaosocial').value) == '')
	{
		if (tipoPessoa == 'PJ')
			msg += 'A razão social deve ser informada.\n';
		else
			msg += 'O nome deve ser informado.\n';
	}
	
	if ($('naturezajur').value == 0)
	{
		msg += 'A natureza jurídica deve ser informada.\n';
	}
	
	if (trim($('telefone1').value) == '')
	{
		msg += 'O telefone geral deve ser informado.\n';
	}
	
	if (trim($('email').value) == '')
	{
		msg += 'O e-mail geral deve ser informado.\n';
	}
	else
	{
		if ( ! validaEmail(trim($('email').value)))
			msg += 'O e-mail geral informado é inválido.\n';
	}
	
	if (trim($('login').value) == '')
	{
		msg += 'O login deve ser informado.\n';
	}
	
	if (trim($('idlogradouro').value) == '')
	{
		msg += 'O endereço deve ser informado.\n';
	}
	
	if (trim($('end_numero').value) == '')
	{
		msg += 'O número do endereço deve ser informado.\n';
	}
	
	if (trim($('nomeresp').value) == '')
	{
		msg += 'O nome do responsável deve ser informado.\n';
	}
	
	if (trim($('telefoneresp').value) == '')
	{
		msg += 'O telefone do responsável deve ser informado.\n';
	}
	
	if (trim($('emailresp').value) == '')
	{
		msg += 'O e-mail do responsável deve ser informado.\n';
	}
	else
	{
		if ( ! validaEmail(trim($('emailresp').value)))
		{
			msg += 'O e-mail do responsável é inválido.\n';
		}
		else
		{
			if (trim($F('emailresp')) != trim($F('emailresp2')))
			{
				msg += 'Confirme o e-mail do responsável.\n';
			}
		}
	}
	
	if ($('aceite').checked == false)
	{
		msg += 'Você deve confirmar o seguinte item: Atesto que as informações prestadas...\n';
	}
	
	if(migrar) 
	{
		if (trim($('observacao').value) == '')
		{
			msg += 'Você deve informar o motivo da Migração.\n';
		}
	}	
	
	if (msg == '' && msgQuest == '')
	{
		return true;
	}
	else
	{
		$('btnSaveForm').disabled = false;
		alert("ATENÇÃO:\n\n"+msg+msgQuest);
		return false;
	}
}

function validaCk(id, qtd)
{
    var valida = false;
    for (var i=1; i <= qtd; i++)
    {
        var idf = id + '_' + i;
        if($(idf).checked == true)
        {
            valida = true;
        }
    }
    
    return valida;
    
}

function validaQuest()
{
    var QUEST_MSG = '';
    var nodes = '';
    
    // PERGUNTA 1
    var qx1 = false;
    nodes = Form.getInputs("form_cad", "radio", "qx1");
    nodes.each(function(node){
        if (node.checked == true)
        {
            qx1 = true;
        }
    });
    
    if(!qx1)
    {
        QUEST_MSG += 'O item 1 deve ser informado.\n';
    }
    
    // PERGUNTA 2
    var qx2 = false;
    nodes = Form.getInputs("form_cad", "radio", "qx2");
    nodes.each(function(node){
        if (node.checked == true)
        {
            qx2 = true;
        }
    });
    
    if(!qx2)
    {
        QUEST_MSG += 'O item 2 deve ser informado.\n';
    }
    
    // PERGUNTA 3
    var qx3 = false;
    nodes = Form.getInputs("form_cad", "radio", "qx3");
    nodes.each(function(node){
        if (node.checked == true)
        {
            qx3 = true;
        }
    });
    
    if(!qx3)
    {
        QUEST_MSG += 'O item 3 deve ser informado.\n';
    }
    
    // PERGUNTA 4
    var qx4 = validaCk('qx4',5);
 
    if (!qx4)
    {    
        QUEST_MSG += 'O item 4 deve ser informado.\n';
    } 
    
    if($('qx4_5').checked == true)
    {
        if(trim($F('qx4_outro')) == '')
        {
            QUEST_MSG += 'O campo Outro do item 4 deve ser informado.\n';
        }
    }
    
    // PERGUNTA 5
    var qx5 = false;
    nodes = Form.getInputs("form_cad", "radio", "qx5");
    nodes.each(function(node){
        if (node.checked == true)
        {
            qx5 = true;
        }
    });
    
    if(!qx5)
    {
        QUEST_MSG += 'O item 5 deve ser informado.\n';
    }
    
    // PERGUNTA 6 
    var qx6 = validaCk('qx6',5);
 
    if (!qx6)
    {    
        QUEST_MSG += 'O item 6 deve ser informado.\n';
    } 
    
    if($('qx6_5').checked == true)
    {
        if(trim($F('qx6_outro')) == '')
        {
            QUEST_MSG += 'O campo Outro do item 6 deve ser informado.\n';
        }
    }

    // PERGUNTA 7 
    if(trim($F('qx7_outro')) == '')
    {
        QUEST_MSG += 'O item 7 deve ser informado.\n';
    }
    
    // PERGUNTA 8 
    if(trim($F('qx8_outro')) == '')
    {
        QUEST_MSG += 'O item 8 deve ser informado.\n';
    }
    
    // PERGUNTA 9
    var qx9 = validaCk('qx9',10);
 
    if (!qx9)
    {    
        QUEST_MSG += 'O item 9 deve ser informado.\n';
    } 
    
    if($('qx9_10').checked == true)
    {
        if(trim($F('qx9_outro')) == '')
        {
            QUEST_MSG += 'O campo Outro do item 9 deve ser informado.\n';
        }
    }
    
    // PERGUNTA 10
    var qx10 = validaCk('qx10',3);
 
    if (!qx10)
    {    
        QUEST_MSG += 'O item 10 deve ser informado.\n';
    } 
    
    // PERGUNTA 11
    var qx11 = validaCk('qx11',14);
 
    if (!qx11)
    {    
        QUEST_MSG += 'O item 11 deve ser informado.\n';
    } 
    
    if($('qx11_14').checked == true)
    {
        if(trim($F('qx11_outro')) == '')
        {
            QUEST_MSG += 'O campo Outro do item 11 deve ser informado.\n';
        }
    }
	
    // PERGUNTA 12
    var qx12 = false;
    nodes = Form.getInputs("form_cad", "radio", "qx12");
    nodes.each(function(node){
        if (node.checked == true) 
        {
            if(node.value == '4')
            {
                if(trim($F('qx12_outro')) == '')
                {
                    QUEST_MSG += 'O campo Outro do item 12 deve ser informado.\n';
                }
            }
            qx12 = true;
        }
    });
    
    if(!qx12)
    {
        QUEST_MSG += 'O item 12 deve ser informado.\n';
    }
    
    // PERGUNTA 13
    var qx13 = false;
    nodes = Form.getInputs("form_cad", "radio", "qx13");
    nodes.each(function(node){
        if (node.checked == true) 
        {
            if (node.value == '4')
            {
                if(trim($F('qx13_outro')) == '')
                {
                    QUEST_MSG += 'O campo Outro do item 13 deve ser informado.\n';
                }
            }
            qx13 = true;
        }
    });

    if(!qx13)
    {
        QUEST_MSG += 'O item 13 deve ser informado.\n';
    }
    
    // PERGUNTA 14
    var qx14 = false;
    nodes = Form.getInputs("form_cad", "radio", "qx14");
    nodes.each(function(node){
        if (node.checked == true)
        {
            qx14 = true;
        }
    });
    
    if(!qx14)
    {
        QUEST_MSG += 'O item 14 deve ser informado.\n';
    }
    
    // PERGUNTA 15 
    nodes = Form.getInputs("form_cad", "radio", "qx14");
    nodes.each(function(node){
        if (node.checked == true && node.value == '2')
        {
            if(trim($F('qx15_outro')) == '')
            {
                QUEST_MSG += 'O item 15 deve ser informado.\n';
            }
        }
    });
    
    // PERGUNTA 16
    var qx16 = false;
    nodes = Form.getInputs("form_cad", "radio", "qx16");
    nodes.each(function(node){
        if (node.checked == true)
        {
            qx16 = true;
        }
    });
    
    if(!qx16)
    {
        QUEST_MSG += 'O item 16 deve ser informado.\n';
    }
    
    // PERGUNTA 17
    nodes = Form.getInputs("form_cad", "radio", "qx16");
    nodes.each(function(node){
        if (node.checked == true && node.value == '1')
        {
            if(trim($F('qx17_outro')) == '')
            {
                QUEST_MSG += 'O item 17 deve ser informado.\n';
            }
        }
    });
    
    // PERGUNTA 18
    var qx18 = false;
    nodes = Form.getInputs("form_cad", "radio", "qx18");
    nodes.each(function(node){
        if (node.checked == true)
        {
            qx18 = true;
        }
    });
    
    if(!qx18)
    {
        QUEST_MSG += 'O item 18 deve ser informado.\n';
    }
    
    // PERGUNTA 19
    nodes = Form.getInputs("form_cad", "radio", "qx18");
    nodes.each(function(node){
        if (node.checked == true && node.value == '1')
        {
            if(trim($F('qx19_outro')) == '')
            {
                QUEST_MSG += 'O item 19 deve ser informado.\n';
            }
        }
    });
    
    // PERGUNTA 20
    var qx20 = false;
    nodes = Form.getInputs("form_cad", "radio", "qx20");
    nodes.each(function(node){
        if (node.checked == true)
        {
            qx20 = true;
        }
    });
    
    if(!qx20)
    {
        QUEST_MSG += 'O item 20 deve ser informado.\n';
    }

    // PERGUNTA 21
    var qx21 = validaCk('qx21',12);
 
    if (!qx21)
    {    
        QUEST_MSG += 'O item 21 deve ser informado.\n';
    } 
    
    if($('qx21_12').checked == true)
    {
        if(trim($F('qx21_outro')) == '')
        {
            QUEST_MSG += 'O campo Outro do item 21 deve ser informado.\n';
        }
    }
    
    // PERGUNTA 22
    var qx22 = validaCk('qx22',8);
 
    if (!qx22)
    {    
        QUEST_MSG += 'O item 22 deve ser informado.\n';
    } 
    
    if($('qx22_8').checked == true)
    {
        if(trim($F('qx22_outro')) == '')
        {
            QUEST_MSG += 'O campo Outro do item 22 deve ser informado.\n';
        }
    }
    
    // PERGUNTA 23 
    if($('qx23_5').checked == true)
    {
        if(trim($F('qx23_outro')) == '')
        {
            QUEST_MSG += 'O campo Outro do item 23 deve ser informado.\n';
        }
    }
    
    // PERGUNTA 24 
    var qx24 = validaCk('qx24',6);
 
    if (!qx24)
    {    
        QUEST_MSG += 'O item 24 deve ser informado.\n';
    } 
    
    if($('qx24_6').checked == true)
    {
        if(trim($F('qx24_outro')) == '')
        {
            QUEST_MSG += 'O campo Outro do item 24 deve ser informado.\n';
        }
    }
    
    // PERGUNTA 25 
    if($('qx25_8').checked == true)
    {
        if(trim($F('qx25_outro')) == '')
        {
            QUEST_MSG += 'O campo Outro do item 25 deve ser informado.\n';
        }
    }
    
    // PERGUNTA 26
    var qx26 = false;
    nodes = Form.getInputs("form_cad", "radio", "qx26");
    nodes.each(function(node){
        if (node.checked == true)
        {
            qx26 = true;
        }
    });
    
    if(!qx26)
    {
        QUEST_MSG += 'O item 26 deve ser informado.\n';
    }
    
    // PERGUNTA 27 
    if(trim($F('qx27_outro')) == '')
    {
        QUEST_MSG += 'O item 27 deve ser informado.\n';
    }
    
    // PERGUNTA 28 
    var qx28 = validaCk('qx28',6);
 
    if (!qx28)
    {    
        QUEST_MSG += 'O item 28 deve ser informado.\n';
    } 
    
    if($('qx28_6').checked == true)
    {
        if(trim($F('qx28_outro')) == '')
        {
            QUEST_MSG += 'O campo Outro do item 28 deve ser informado.\n';
        }
    }
    
    // PERGUNTA 29
    var qx29 = validaCk('qx29',14);
 
    if (!qx29)
    {    
        QUEST_MSG += 'O item 29 deve ser informado.\n';
    } 
    
    if($('qx29_14').checked == true)
    {
        if(trim($F('qx29_outro')) == '')
        {
            QUEST_MSG += 'O campo Outro do item 29 deve ser informado.\n';
        }
    }
    
    // PERGUNTA 30
    var qx30 = validaCk('qx30',17);
 
    if (!qx30)
    {    
        QUEST_MSG += 'O item 30 deve ser informado.\n';
    } 
    
    if($('qx30_17').checked == true)
    {
        if(trim($F('qx30_outro')) == '')
        {
            QUEST_MSG += 'O campo Outro do item 30 deve ser informado.\n';
        }
    }
    
    // PERGUNTA 31
    var qx31 = validaCk('qx31',15);
 
    if (!qx31)
    {    
        QUEST_MSG += 'O item 31 deve ser informado.\n';
    } 
    
    if($('qx31_15').checked == true)
    {
        if(trim($F('qx31_outro')) == '')
        {
            QUEST_MSG += 'O campo Outro do item 31 deve ser informado.\n';
        }
    }
    
    // PERGUNTA 32
    var qx32 = validaCk('qx32',6);
 
    if (!qx32)
    {    
        QUEST_MSG += 'O item 32 deve ser informado.\n';
    }
    
    // PERGUNTA 33
    var qx33 = false;
    nodes = Form.getInputs("form_cad", "radio", "qx33");
    nodes.each(function(node){
        if (node.checked == true) 
        {
            if (node.value == '10')
            {
                if(trim($F('qx33_outro')) == '')
                {
                    QUEST_MSG += 'O campo Outro do item 33 deve ser informado.\n';
                }
            }
            qx33 = true;
        }
    });
    
    if(!qx33)
    {
        QUEST_MSG += 'O item 33 deve ser informado.\n';
    }
    
    // PERGUNTA 39
    var qx39 = validaCk('qx39',12);
 
    if (!qx39)
    {    
        QUEST_MSG += 'O item 39 deve ser informado.\n';
    }
    
    if($('qx39_12').checked == true)
    {
        if(trim($F('qx39_outro')) == '')
        {
            QUEST_MSG += 'O campo Outro do item 39 deve ser informado.\n';
        }
    }
    
    // PERGUNTA 40
    if($('qx40_4').checked == true)
    {
        if(trim($F('qx40_outro')) == '')
        {
            QUEST_MSG += 'O campo Outro do item 40 deve ser informado.\n';
        }
    }
	
	return QUEST_MSG;
}


/**************************************************************
 *
 *	PDV
 *
 **************************************************************/
 
function pdv_onload()
{
	$('cnpjcpf').focus();
	setTipoPdv('');
	
	listaSocios();
}

function setTipoPdv(tipo)
{
	if (tipo == '')
	{
		tipo = $F('tipopessoa_aux');
		
		var nodes = Form.getInputs("form_cad", "radio", "tipopessoa");
		nodes.each(function(node){
				if (node.value == tipo)
				{
					node.checked = true
				}
		});
	}
	else
	{
		$('cnpjcpf').value      = '';
		$('iestadual').value    = '';
		$('nomefantasia').value = '';
	}
	
	if (tipo == 'PJ')
	{
		$('spantipopessoa').update('CNPJ');
		$('cnpjcpf').maxLength = 18;
		$('cnpjcpf').setAttribute("onKeyUp","maskCnpj(this)");
		
		$('spannome').update('Razão Social');
		$('linhaie').setStyle({display : ''});
		$('linhanf').setStyle({display : ''});
	}
	else
	{
		$('spantipopessoa').update('CPF');
		$('cnpjcpf').maxLength = 14;
		$('cnpjcpf').setAttribute("onKeyUp","maskCpf(this)");
		
		$('spannome').update('Nome');
		$('linhaie').setStyle({display : 'none'});
		$('linhanf').setStyle({display : 'none'});
	}
	
	$('cnpjcpf').focus();
}

function validaForm_cadastroPdv()
{
	$('btnSaveForm').disabled = true;
	
	var msg = '';
	
	var tipoPessoa = Form.getInputs("form_cad", "radio", "tipopessoa").find(function(radio) { return radio.checked; }).value;
	
	if (trim($F('cnpjcpf')) == '')
	{
		if (tipoPessoa == 'PJ')
			msg += 'O CNPJ deve ser informado.\n';
		else
			msg += 'O CPF deve ser informado.\n';
	}
	else
	{
		if (! validaCPFCNPJ(tipoPessoa, $F('cnpjcpf')))
		{
			if (tipoPessoa == 'PJ')
				msg += 'CNPJ inválido.\n';
			else
				msg += 'CPF inválido.\n';
		}
	}
	
	if (trim($('razaosocial').value) == '')
	{
		if (tipoPessoa == 'PJ')
			msg += 'A razão social deve ser informada.\n';
		else
			msg += 'O nome deve ser informado.\n';
	}
	
	if ($('naturezajur').value == 0)
	{
		msg += 'A natureza jurídica deve ser informada.\n';
	}
	
	if (trim($('telefone1').value) == '')
	{
		msg += 'O telefone geral deve ser informado.\n';
	}
	
	if (trim($('email').value) == '')
	{
		msg += 'O e-mail geral deve ser informado.\n';
	}
	else
	{
		if ( ! validaEmail(trim($('email').value)))
			msg += 'O e-mail geral informado é inválido.\n';
	}
	
	if (trim($('login').value) == '')
	{
		msg += 'O login deve ser informado.\n';
	}
	
	if (trim($('idlogradouro').value) == '')
	{
		msg += 'O endereço deve ser informado.\n';
	}
	
	if (trim($('end_numero').value) == '')
	{
		msg += 'O número do endereço deve ser informado.\n';
	}
	
	if (trim($('nomeresp').value) == '')
	{
		msg += 'O nome do responsável deve ser informado.\n';
	}
	
	if (trim($('telefoneresp').value) == '')
	{
		msg += 'O telefone do responsável deve ser informado.\n';
	}
	
	if (trim($('emailresp').value) == '')
	{
		msg += 'O e-mail do responsável deve ser informado.\n';
	}
	else
	{
		if ( ! validaEmail(trim($('emailresp').value)))
		{
			msg += 'O e-mail do responsável é inválido.\n';
		}
		else
		{
			if (trim($F('emailresp')) != trim($F('emailresp2')))
			{
				msg += 'Confirme o e-mail do responsável.\n';
			}
		}
	}
	
	/*if ($('aceite').checked == false)
	{
		msg += 'Os termos do programa devem ser aceitos.\n';
	}*/
	
	if (msg == '')
	{
		return true;
	}
	else
	{
		$('btnSaveForm').disabled = false;
		alert("ATENÇÃO:\n\n"+msg);
		return false;
	}
}

/**************************************************************
 *
 *	EDITORA
 *
 **************************************************************/

function editora_onload()
{
	$('cnpjcpf').focus();
	setTipoEditora('');
	
	listaSocios();
	populaAtuacao();
}

function setTipoEditora(tipo)
{
	if (tipo == '')
	{
		tipo = $F('tipopessoa_aux');
		
		var nodes = Form.getInputs("form_cad", "radio", "tipopessoa");
		nodes.each(function(node){
				if (node.value == tipo)
				{
					node.checked = true
				}
		});
	}
	else
	{
		$('cnpjcpf').value      = '';
		$('iestadual').value    = '';
		$('nomefantasia').value = '';
	}
	
	if (tipo == 'PJ')
	{
		$('spantipopessoa').update('CNPJ');
		$('cnpjcpf').maxLength = 18;
		$('cnpjcpf').setAttribute("onKeyUp","maskCnpj(this)");
		
		$('spannome').update('Razão Social');
		$('linhaie').setStyle({display : ''});
		$('linhanf').setStyle({display : ''});
	}
	else
	{
		$('spantipopessoa').update('CPF');
		$('cnpjcpf').maxLength = 14;
		$('cnpjcpf').setAttribute("onKeyUp","maskCpf(this)");
		
		$('spannome').update('Nome');
		$('linhaie').setStyle({display : 'none'});
		$('linhanf').setStyle({display : 'none'});
	}
	
	$('cnpjcpf').focus();
}

function validaForm_cadEditora()
{
	$('btnSaveForm').disabled = true;
	
	var msg = '';
	
	var tipoPessoa = Form.getInputs("form_cad", "radio", "tipopessoa").find(function(radio) { return radio.checked; }).value;
	
	if (trim($F('cnpjcpf')) == '')
	{
		if (tipoPessoa == 'PJ')
			msg += 'O CNPJ deve ser informado.\n';
		else
			msg += 'O CPF deve ser informado.\n';
	}
	else
	{
		if (! validaCPFCNPJ(tipoPessoa, $F('cnpjcpf')))
		{
			if (tipoPessoa == 'PJ')
				msg += 'CNPJ inválido.\n';
			else
				msg += 'CPF inválido.\n';
		}
	}
	
	if (trim($('razaosocial').value) == '')
	{
		if (tipoPessoa == 'PJ')
			msg += 'A razão social deve ser informada.\n';
		else
			msg += 'O nome deve ser informado.\n';
	}
	
	if ($('naturezajur').value == 0)
	{
		msg += 'A natureza jurídica deve ser informada.\n';
	}
	
	if ($('atividadeprinc').value == 0)
	{
		msg += 'A atividade principal deve ser informada.\n';
	}
	
	if (trim($('telefone1').value) == '')
	{
		msg += 'O telefone geral deve ser informado.\n';
	}
	
	if (trim($('email').value) == '')
	{
		msg += 'O e-mail geral deve ser informado.\n';
	}
	else
	{
		if ( ! validaEmail(trim($('email').value)))
			msg += 'O e-mail geral informado é inválido.\n';
	}
	
	if (trim($('login').value) == '')
	{
		msg += 'O login deve ser informado.\n';
	}
	
	if (trim($('idlogradouro').value) == '')
	{
		msg += 'O endereço deve ser informado.\n';
	}
	
	if (trim($('end_numero').value) == '')
	{
		msg += 'O número do endereço deve ser informado.\n';
	}
	
	if (trim($('nomeresp').value) == '')
	{
		msg += 'O nome do responsável deve ser informado.\n';
	}
	
	if (trim($('telefoneresp').value) == '')
	{
		msg += 'O telefone do responsável deve ser informado.\n';
	}
	
	if (trim($('emailresp').value) == '')
	{
		msg += 'O e-mail do responsável deve ser informado.\n';
	}
	else
	{
		if ( ! validaEmail(trim($('emailresp').value)))
		{
			msg += 'O e-mail do responsável é inválido.\n';
		}
		else
		{
			if (trim($F('emailresp')) != trim($F('emailresp2')))
			{
				msg += 'Confirme o e-mail do responsável.\n';
			}
		}
	}
	
	/*if ($('aceite').checked == false)
	{
		msg += 'Os termos do programa devem ser aceitos.\n';
	}*/
	
	if (msg == '')
	{
		return true;
	}
	else
	{
		$('btnSaveForm').disabled = false;
		alert("ATENÇÃO:\n\n"+msg);
		return false;
	}
}

/**************************************************************
 *
 *	DISTRIBUIDOR
 *
 **************************************************************/

function dist_onload()
{
	$('cnpjcpf').focus();
	setTipoDist('');
	
	listaSocios();
	populaAtuacao();
	populaEditoraTrab();
	populaRegiao();
	populaCidade();
}

function setTipoDist(tipo)
{
	if (tipo == '')
	{
		tipo = $F('tipopessoa_aux');
		
		var nodes = Form.getInputs("form_cad", "radio", "tipopessoa");
		nodes.each(function(node){
				if (node.value == tipo)
				{
					node.checked = true
				}
		});
	}
	else
	{
		$('cnpjcpf').value      = '';
		$('iestadual').value    = '';
		$('nomefantasia').value = '';
	}
	
	if (tipo == 'PJ')
	{
		$('spantipopessoa').update('CNPJ');
		$('cnpjcpf').maxLength = 18;
		$('cnpjcpf').setAttribute("onKeyUp","maskCnpj(this)");
		
		$('spannome').update('Razão Social');
		$('linhaie').setStyle({display : ''});
		$('linhanf').setStyle({display : ''});
	}
	else
	{
		$('spantipopessoa').update('CPF');
		$('cnpjcpf').maxLength = 14;
		$('cnpjcpf').setAttribute("onKeyUp","maskCpf(this)");
		
		$('spannome').update('Nome');
		$('linhaie').setStyle({display : 'none'});
		$('linhanf').setStyle({display : 'none'});
	}
	
	$('cnpjcpf').focus();
}

function validaForm_cadDist()
{
	$('btnSaveForm').disabled = true;
	
	var msg = '';
	
	var tipoPessoa = Form.getInputs("form_cad", "radio", "tipopessoa").find(function(radio) { return radio.checked; }).value;
	
	if (trim($F('cnpjcpf')) == '')
	{
		if (tipoPessoa == 'PJ')
			msg += 'O CNPJ deve ser informado.\n';
		else
			msg += 'O CPF deve ser informado.\n';
	}
	else
	{
		if (! validaCPFCNPJ(tipoPessoa, $F('cnpjcpf')))
		{
			if (tipoPessoa == 'PJ')
				msg += 'CNPJ inválido.\n';
			else
				msg += 'CPF inválido.\n';
		}
	}
	
	if (trim($('razaosocial').value) == '')
	{
		if (tipoPessoa == 'PJ')
			msg += 'A razão social deve ser informada.\n';
		else
			msg += 'O nome deve ser informado.\n';
	}
	
	if ($('naturezajur').value == 0)
	{
		msg += 'A natureza jurídica deve ser informada.\n';
	}
	
	if (trim($('telefone1').value) == '')
	{
		msg += 'O telefone geral deve ser informado.\n';
	}
	
	if (trim($('email').value) == '')
	{
		msg += 'O e-mail geral deve ser informado.\n';
	}
	else
	{
		if ( ! validaEmail(trim($('email').value)))
			msg += 'O e-mail geral informado é inválido.\n';
	}
	
	if (trim($('login').value) == '')
	{
		msg += 'O login deve ser informado.\n';
	}
	
	if (trim($('idlogradouro').value) == '')
	{
		msg += 'O endereço deve ser informado.\n';
	}
	
	if (trim($('end_numero').value) == '')
	{
		msg += 'O número do endereço deve ser informado.\n';
	}
	
	if (trim($('nomeresp').value) == '')
	{
		msg += 'O nome do responsável deve ser informado.\n';
	}
	
	if (trim($('telefoneresp').value) == '')
	{
		msg += 'O telefone do responsável deve ser informado.\n';
	}
	
	if (trim($('emailresp').value) == '')
	{
		msg += 'O e-mail do responsável deve ser informado.\n';
	}
	else
	{
		if ( ! validaEmail(trim($('emailresp').value)))
		{
			msg += 'O e-mail do responsável é inválido.\n';
		}
		else
		{
			if (trim($F('emailresp')) != trim($F('emailresp2')))
			{
				msg += 'Confirme o e-mail do responsável.\n';
			}
		}
	}
	
	/*if ($('aceite').checked == false)
	{
		msg += 'Os termos do programa devem ser aceitos.\n';
	}*/
	
	if (msg == '')
	{
		return true;
	}
	else
	{
		$('btnSaveForm').disabled = false;
		alert("ATENÇÃO:\n\n"+msg);
		return false;
	}
}

/**************************************************************
 *
 *	OUTROS
 *
 **************************************************************/
/**
* ajaxEnviaRedefinicaoSenha()
* Envia e-mail de redefinição
* @param string login
* @return void
*/
function ajaxEnviaRedefinicaoSenha(login, email)
{
	if (login != '' && email != '')
	{
		if(window.confirm("ATENÇÃO!\n\nConfirma envio de e-mail para redefinição de senha ao usuário "+login+"?")) {
			var url = $F('URL_EXEC') + 'autocad/solicitaSenha/save/login/true';
			var pars = {'login' : login, 'email_login' : email, 'ms' : new Date().getTime()};
			var myAjax = new Ajax.Request(url, {parameters: pars, onComplete: function(objRequest){ ajaxEnviaRedefinicaoSenha_onComplete(objRequest); }});
			return true;
		} else {
			return false;
		}	
	} else {
		return false;
	}
}

function ajaxEnviaRedefinicaoSenha_onComplete(objreq)
{
	//$(this).reload();
	// window.location = window.location.href;
	//window.location.reload();
	
	//$(idinputname).value = objreq.responseText;
	alert(objreq.responseText);
}
/**
* ajaxReenviaEmailValicadao()
* Reenvia e-mail de validação
* @param string login
* @return void
*/
function ajaxReenviaEmailValicadao(login)
{
	if (login != '')
	{
		if(window.confirm("ATENÇÃO!\n\nConfirma envio de e-mail para validação de cadastro ao usuário "+login+"?")) {
			var url = $F('URL_EXEC') + 'autocad/renviaEmail/save/login/true';
			var pars = {'login' : login, 'ms' : new Date().getTime()};
			var myAjax = new Ajax.Request(url, {parameters: pars, onComplete: function(objRequest){ ajaxReenviaEmailValicadao_onComplete(objRequest); }});
			return true;
		} else {
			return false;
		}	
	} else {
		return false;
	}
}

function ajaxReenviaEmailValicadao_onComplete(objreq)
{
	//$(this).reload();
	// window.location = window.location.href;
	//window.location.reload();
	
	//$(idinputname).value = objreq.responseText;
	alert(objreq.responseText);
}
function tipo_solicitaSenha(tipo)
{
        switch(tipo)
        {
            case 'cpfcnpj':
                $('form_cad_login').setStyle({display : 'none'});
                $('form_cad_cpfcnpj').setStyle({display : ''});
                break;
                
            case 'login':
                $('form_cad_cpfcnpj').setStyle({display : 'none'});
                $('form_cad_login').setStyle({display : ''});
                break;
                
            default:
                alert('ATENÇÃO:\n\nTipo de solicitação não é válido.');
        }
    
}

function validaForm_enviaEmail()
{
    $('btnSaveForm').disabled = true;
	
	var msg = '';

    var tipoPessoa = ($F('cpfcnpj').length == 11 ? 'PF' : 'PJ');

    if (trim($F('cpfcnpj')) == '')
    {
            msg += 'O CNPJ/CPF deve ser informado.\n';
    }
    else
    {
        if (! validaCPFCNPJ(tipoPessoa, $F('cpfcnpj')))
        {
            msg += 'CNPJ/CPF inválido.\n';
        }
    }
    
    if (msg == '')
    {
        return true;
    }
    else
    {
        $('btnSaveForm').disabled = false;
        alert("ATENÇÃO:\n\n"+msg);
        return false;
    }
        
}

function confirmForm_enviaEmail(id,email)
{
    
    var r=confirm('Redefinir sua senha e reenviar o email de validação para o email ' + email + ' ?');
    
    if (r==true)
    {
        $(id).disabled = true;
        return true;
    }
    else
    {
        return false;
    }
  

}

function validaForm_solicitaSenha(tipo)
{
	$('btnSaveForm').disabled = true;
	
	var msg = '';
	
        switch(tipo)
        {
            case 'cpfcnpj':
                var tipoPessoa = ($F('cpfcnpj').length == 11 ? 'PF' : 'PJ');

                if (trim($F('cpfcnpj')) == '')
                {
                        msg += 'O CNPJ/CPF deve ser informado.\n';
                }
                else
                {
                        if (! validaCPFCNPJ(tipoPessoa, $F('cpfcnpj')))
                        {
                                msg += 'CNPJ/CPF inválido.\n';
                        }
                }
                
                if (trim($F('email_cpfcnpj')) == '')
                {
                        msg += 'O e-mail deve ser informado.\n';
                }
                else
                {
                    if ( ! validaEmail(trim($F('email_cpfcnpj'))))
                            msg += 'O e-mail informado é inválido.\n';
                }
                
                break;
                
            case 'login':
                if (trim($F('login')) == '')
                {
                        msg += 'O login deve ser informado.\n';
                }
                
                if (trim($F('email_login')) == '')
                {
                        msg += 'O e-mail deve ser informado.\n';
                }
                else
                {
                    if ( ! validaEmail(trim($F('email_login'))))
                            msg += 'O e-mail informado é inválido.\n';
                }
                
                break;
                
            default:
                alert('ATENÇÃO:\n\nTipo de solicitação não é válido.');
        }


        if (msg == '')
        {
                return true;
        }
        else
        {
                $('btnSaveForm').disabled = false;
                alert("ATENÇÃO:\n\n"+msg);
                return false;
        }
}

/**
* ajaxGetCidadesBib()
* Coleta as cidades de uma determinada uf para a pesquisa.
* @param string uf
* @return void
*/
function ajaxGetDadosBiblioteca(idbib)
{
	if (trim(idbib) != '')
	{
		var url = $F('URL_EXEC') + 'biblioteca/getdadosbib/';
		var pars = {'ms' : new Date().getTime(), 'idbiblioteca': idbib };
		var myAjax = new Ajax.Request(url, { parameters: pars, onComplete: ajaxGetDadosBiblioteca_onComplete });
	} else {
		// Limpa os options, caso seja vazio a UF
		$('form_cad').reset();
		setTipoBiblioteca('');
		return false;
	}
}

function ajaxGetDadosBiblioteca_onComplete(objReq)
{
	dadosbib = objReq.responseText.split('|');
	
	var nodes = Form.getInputs("form_cad", "radio", "tipopessoa");
	nodes.each(function(node){
			if (node.value == dadosbib[0])
			{
				node.checked = true
			}
	});
	
	$('cnpjcpf').value			= dadosbib[1];
	$('iestadual').value		= dadosbib[2];
	$('razaosocial').value		= dadosbib[3];
	$('nomefantasia').value		= dadosbib[4];
	$('naturezajur').value		= dadosbib[5];
	$('telefone1').value		= dadosbib[6];
	$('email').value			= dadosbib[7];
	$('site').value				= dadosbib[8];
	$('login').value			= dadosbib[9];
	$('idlogradouro').value		= dadosbib[10];
	$('cep').value				= dadosbib[11];
	$('uf').value				= dadosbib[12];
	$('cidade').value			= dadosbib[13];
	$('bairro').value			= dadosbib[14];
	$('logradouro').value		= dadosbib[15];
	$('logcomplemento').value	= dadosbib[16];
	$('end_numero').value		= dadosbib[17];
	$('end_complemento').value	= dadosbib[18];
	$('nomeresp').value			= dadosbib[19];
	$('telefoneresp').value		= dadosbib[20];
	$('skyperesp').value		= dadosbib[21];
	$('emailresp').value		= dadosbib[22];
	$('emailresp2').value		= dadosbib[22];
	$('observacao').value		= 'AJUSTES DE CADASTRO SNIIC';
	$('aceite').checked			= true;
	
}
