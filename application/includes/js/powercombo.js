/**
* Classe para manipula��o de campos tipo COMBO do html
* @version		0.1.0 - 16/03/2010
* @since		0.0.1 - 05/06/2006
* @author		Luis Eduardo
* @depends		Prototype Library (http://prototype.conio.net/)
=======================================================
Exemplo:
	var myPowerCombo = new PowerCombo('idcliente', '1|teste1:1:~2|teste2~3|teste3~6|teste6~4|teste4~7|teste7~5|teste5', true, true, 'Selecione um item');
	myPowerCombo.insert();

	Onde:
		PowerCombo('idcliente', '1|teste1:1:&2|teste2&3|teste3&6|teste6&4|teste4&7|teste7&5|teste5', true, true, 'Selecione um item');
                      |                   |                 |                                      |     |             |
                      |                   |                 |                                      |     |             |
                      |                   |                 |                                      |     |             +--> string com o caption do primeiro �tem do combo
                      |                   |                 |                                      |     |                  caso n�o seja definida o primeiro �tem ser� o primeiro registro da string 
                      |                   |                 |                                      |     |
                      |                   |                 |                                      |     +--> boleano que define se o combo
					  |                   |                 |                                      |         ser� ordenado.
					  |                   |                 |                                      |
                      |                   |                 |	                                   +--> boleano que define se o combo
                      |                   |                 |                                           ser� limpo antes de inserir
					  |                   |                 |                                           novos dados
					  |                   |                 |
                      |                   |                 +--> string com os dados
					  |                   +--> informa que       a serem incluidos no combo 
					  |                        este �tem vir�
					  |                        marcado como default
                      |
                      +--> id ou objeto combo
					       a ser manipulado
=======================================================
04/10/2006 - Adicionado o metodo remove(), para deletar um ou mais �tens do combo
02/10/2006 - Adicionado 0 metodo clone(), para clonar um combo para o outro
10/10/2006 - Alterado o metodo remove(), para chamar os metodos remove para cada browser, tem um bug no IE com a fun��o removeChild()
			 Adicionado o metodo _remove_ie()
			 Adicionado o metodo _remove_w3c()
			 Adicionada a propriedade brwIE que define se o browser do usuario � o IEca
06/12/2006 - Alterado o metodo clone(), simplificado o metodo, faz um loop no combo origem e insere os itens no combo destino
12/07/2007 - Removida a propriedade brwIE da classe, ser� usada uma propriedade do pr�prio Prototype
			 Alterado o metodo insert(), quando tiver somente um registro para inserir no combo o mesmo ser� selecionado como default.
			 Alterado o metodo remove(), a detec��o do browser passou a ser feita atrav�s de uma propriedade do Prototype
26/07/2007 - Alterado o metodo insert(), definido que se no nome do item tiver :1:, este �tem ser� definido como default
16/03/2010 - Adicionado o metodo insertJSON()
             Alterado o metodo clear(), para poder limpar um combo com OPTGROUP
=======================================================
*/
var PowerCombo = Class.create();
PowerCombo.prototype = {

	/**
	* Contrutor da classe
	* @access	public
	* @param 	object/string 	cbo			id ou objeto combo
	* @param 	objectstring 	elements	string ou objeto JSON contendo os elementos a serem adicionados no combo
	* @param 	bool 			clearCbo	boleano setando se o combo ser� limpo/zerado
	* @param 	bool 			sortCbo		boleano setando se o combo ser� ordenado
	* @param 	string 			firstItem	string setando qual ser� o primeiro �tem do combo
	* @return 	void
	* @author	Luis Eduardo
	*/
	initialize: function(cbo, elements, clearCbo, sortCbo, firstItem) {

		// verifica se foi passado o id ou o objeto
		this.cbo = cbo;
		if (typeof cbo == 'string')
			this.cbo = $(cbo);

		this.elements = elements;

		// verifica se foi passado o parametro, sen�o aplica o padr�o (true)
		this.clearCbo = clearCbo;
		if(clearCbo == 'undefined' || clearCbo == null)
			this.clearCbo = true;

		// verifica se foi passado o parametro, sen�o aplica o padr�o (true)
		this.sortCbo = sortCbo;
		if(sortCbo == 'undefined' || sortCbo == null)
			this.sortCbo = true;

		// verifica se foi passado o parametro, sen�o aplica o padr�o (vazio)
		this.firstItem = firstItem;
		if(firstItem == 'undefined' || firstItem == null)
			this.firstItem = '';

		// define o padr�o dos separadores
		this.sepRow = '&';
		this.sepOpt = '|';
	},
	
	/**
	* Define os separadores de registro e dados
	* @access	public
	* @param 	string 		sepRow		separador de registro/linha
	* @param 	string 		sepOpt		separador de dados
	* @return 	void
	* @author	Luis Eduardo
	* deprecated - N�O USAR ESTE METODO
	*/
	defsep: function(sepRow, sepOpt) {
		if(sepRow != '')
			this.sepRow = sepRow;
		if(sepOpt != '')
			this.sepOpt = sepOpt;
	},

	/**
	* Define os separadores de registro e dados
	* @access	public
	* @param 	string 		sepRow		separador de registro/linha
	* @param 	string 		sepOpt		separador de dados
	* @return 	void
	* @author	Luis Eduardo
	*/
	defSep: function(sepRow, sepOpt) {
		if(sepRow != '')
			this.sepRow = sepRow;
		if(sepOpt != '')
			this.sepOpt = sepOpt;
	},

	/**
	* Insere os dados no combo
	* @access	public
	* @return 	void
	* @author	Luis Eduardo
	*/
	insert: function() {

			if(this.clearCbo)
				this.clear();
			if(this.firstItem != '')
				this.cbo.options.add(new Option(this.firstItem, 0));
			if(this.elements.length > 0)
			{
				var row = this.elements.split(this.sepRow);
				var len_row = row.length;
				var arrOpt = [];
				var defaultSelected = -1;
				for(var i=0;i<len_row;i++)
				{
					arrOpt = row[i].split(this.sepOpt);

					// verifica se exite um �tem default
					if(arrOpt[1].indexOf(":1:") != -1)
					{
						arrOpt[1] = arrOpt[1].replace(/:1:/ig, '');
						defaultSelected = i;
					}

					this.cbo.options.add(new Option(arrOpt[1], arrOpt[0]));
				}
				// caso tenha somente um registro no combo ent�o este ser� o default
				/*
				if(len_row == 1)
				{
					if(this.firstItem != '')
						this.cbo.options.selectedIndex=1;
					else
						this.cbo.options.selectedIndex=0;
				}
				else
				{
				*/
					// sen�o verifica se tem algum �tem definido como default
					if(defaultSelected >= 0)
					{
						if(this.firstItem != '')
							this.cbo.options.selectedIndex=defaultSelected+1;
						else
							this.cbo.options.selectedIndex=defaultSelected;
					}
				//}

				if(this.sortCbo)
					this.sort();
			}
	},
	
	/**
	* Insere os dados no combo baseados num objeto JSON
	* @access	public
	* @return 	void
	* @author	Luis Eduardo
	*/
	insertJSON: function() {
	
		if(this.clearCbo)
			this.clear();
		if(this.firstItem != '')
			this.cbo.options.add(new Option(this.firstItem, ''));

		if(this.elements.length > 0)
		{
			var optGroup = '';
			var opt      = '';
			var len_row  = this.elements.length;
			var grpOld   = '';
			var strDecode = '';
			var grupoVar  = '';

			for(var i=0;i<len_row;i++)
			{
				grupoVar = this.elements[i].GRUPOVAR;
				if(grupoVar != undefined && grupoVar != grpOld)
				{
					if(grpOld != '')
						this.cbo.appendChild(optGroup);

					strDecode = grupoVar.unescapeHTML();

					optGroup = document.createElement("optgroup");
					optGroup.label = strDecode;

					grpOld = strDecode;
				}
				strDecode = this.elements[i].VALORVAR.unescapeHTML();
				opt = document.createElement("option");
				opt.value = this.elements[i].IDVAR;
				opt.appendChild(document.createTextNode(strDecode));
				
				if(optGroup != '')
					optGroup.appendChild(opt);
				else
					this.cbo.appendChild(opt);
			}
			
			if(optGroup != '')
				this.cbo.appendChild(optGroup);
		}
	},

	/**
	* Deleta �tens do combo
	* @access	public
	* @param 	array 		arrElements		array de elementos a serem excluidos
	* @return 	void
	* @author	Luis Eduardo
	*/
	remove: function(arrElements) {
		if(Prototype.Browser.IE)
		{
			this.remove_ie(arrElements);
		}
		else
		{
			this.remove_w3c(arrElements);
		}
	},

	/**
	* Deleta �tens do combo, para browsers padr�o W3C
	* @access	private
	* @param 	array 		arrElements		array de elementos a serem excluidos
	* @return 	void
	* @author	Luis Eduardo
	*/
	remove_w3c: function(arrElements) {
		if(arrElements.length > 0)
		{
			var len_cbo = this.cbo.length;
			for(var i=len_cbo-1;i>=0;i--)
			{
				if(arrElements.indexOf(this.cbo.options[i].value) != '-1')
					this.cbo.removeChild(this.cbo.options[i]);
			}
			if(this.cbo.length > 0)
			{
				if(this.sortCbo)
					this.sort();
			}
		}
	},
	
	/**
	* Deleta �tens do combo, para browsers N�O PADR�O W3C (IEca)
	* @access	private
	* @param 	array 		arrElements		array de elementos a serem excluidos
	* @return 	void
	* @author	Luis Eduardo
	*/
	remove_ie: function(arrElements) {
		var arrOpt = [];
		if(arrElements.length > 0)
		{
			var len_cbo = this.cbo.length;
			//for(var i=len_cbo-1;i>=0;i--)
			for(var i=0;i<len_cbo;i++)
			{
				if(arrElements.indexOf(this.cbo.options[i].value) == '-1')
					arrOpt[arrOpt.length] = new Option( this.cbo.options[i].text, this.cbo.options[i].value, this.cbo.options[i].defaultSelected, this.cbo.options[i].selected);
			}

			this.clear();
			var len_arrOpt = arrOpt.length;

			for(var i=0; i < len_arrOpt; i++)
			{
				this.cbo.options[i] = new Option(arrOpt[i].text, arrOpt[i].value, arrOpt[i].defaultSelected, arrOpt[i].selected);
			}

			if(this.cbo.length > 0)
			{
				if(this.sortCbo)
					this.sort();
    		}
		}
	},

	/**
	* Ordena os dados do combo
	* @access	private
	* @return 	void
	* @author	Luis Eduardo
	*/
	sort: function() {
		var o = [];
		var obj = this.cbo;
		var len_cbo = obj.options.length;
		for(var i=0; i<len_cbo; i++)
		{
			o[o.length] = new Option( obj.options[i].text, obj.options[i].value, obj.options[i].defaultSelected, obj.options[i].selected) ;
		}
		if(o.length==0)
			return;
		o = o.sort( 
				function(a,b)
				{ 
					if ((a.text+"") < (b.text+"")) { return -1; }
					if ((a.text+"") > (b.text+"")) { return 1; }
					return 0;
				} 
			);

		var len_o = o.length;
		for(var i=0; i<len_o; i++)
		{
			obj.options[i] = new Option(o[i].text, o[i].value, o[i].defaultSelected, o[i].selected);
		}
	},
	
	/**
	* Limpa/Zera todos os dados do combo
	* @access	private
	* @return 	void
	* @author	Luis Eduardo
	*/
	clear: function() {
		while (this.cbo.hasChildNodes())
		{
			this.cbo.removeChild(this.cbo.firstChild);
		}
	},

	/**
	* Retorna a quantidade de itens no combo
	* @access	private
	* @return 	void
	* @author	Luis Eduardo
	*/
	count: function() {
		return this.cbo.length;
	},

	/**
	* Converte os options de um combo em string com os
	* separadores definidos no metodo defSep
	* @access	public
	* @param 	string 	cbo	id do combo
	* @author	Luis Eduardo
	*/
	toString: function(cbo) {
		if (typeof cbo == 'string')
			cbo = $(cbo);

		var cboOptions = cbo.options;
		var qtde = cboOptions.length;
		var arrCbo = [];
		var strCbo = '';

		for(var i=0; i<qtde; i++)
		{
			arrCbo.push(cboOptions[i].value + this.sepOpt + cboOptions[i].text);
		}
		strCbo = arrCbo.join(this.sepRow);
		return strCbo;
	},

	/**
	* Copia os options de um combo para o outro
	* @access	public
	* @param 	string 	cboSrc	id do combo origem
	* @author	Luis Eduardo
	*/
	clone: function(cboSrc) {
		if (typeof cboSrc == 'string')
			cboSrc = $(cboSrc);

		// limpa o combo destino
		this.clear();

		// pega todos os elementos do combo origem a joga no destino
		var c_cboSrc = cboSrc.length;
		for(var i=0; i<c_cboSrc; i++)
		{
			this.cbo.options[i] = new Option(cboSrc[i].text, cboSrc[i].value, cboSrc[i].defaultSelected, cboSrc[i].selected);
		}
	}
};
