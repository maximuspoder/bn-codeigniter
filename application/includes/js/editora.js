/*****************************************************
 * EDITORA
 *****************************************************/

function editora_formalt_onload()
{
	if ($F('loadDadosEdit') == 1)
	{
		setTipoEditora('', 0);
		$('naturezajur').value    = $F('naturezajur_aux');
		$('atividadeprinc').value = $F('atividadep_aux');
		$('tem_rede').value       = $F('tem_rede_aux');
	}
	else
	{
		var tipoPessoa = Form.getInputs("form_cad", "radio", "tipopessoa").find(function(radio) {return radio.checked;}).value;
		setTipoEditora(tipoPessoa, 0);
	}
	
	listaSocios();
	populaAtuacao();
}

function setTipoEditora(tipo, limpar)
{
	if (tipo == '')
	{
		tipo = $F('tipopessoa_aux');
			
		var form  = $('form_cad')
		var nodes = form.getInputs('radio', 'tipopessoa');
		nodes.each(function(node){
				if (node.value == tipo)
					node.checked = true;
				else
					node.checked = false;
		});
	}
	
	if (limpar == 1)
	{
		$('cnpjcpf').value      = '';
		$('iestadual').value    = '';
		$('nomefantasia').value = '';
	}
	
	if (tipo == 'PJ')
	{
		$('spantipopessoa').update('CNPJ');
		$('cnpjcpf').maxLength = 18;
		$('cnpjcpf').setAttribute("onKeyUp","maskCnpj(this)");
		
		$('spannome').update('Razão Social');
		$('linhaie').setStyle({display : ''});
		$('linhanf').setStyle({display : ''});
	}
	else
	{
		$('spantipopessoa').update('CPF');
		$('cnpjcpf').maxLength = 14;
		$('cnpjcpf').setAttribute("onKeyUp","maskCpf(this)");
		
		$('spannome').update('Nome');
		$('linhaie').setStyle({display : 'none'});
		$('linhanf').setStyle({display : 'none'});
	}
	
	$('cnpjcpf').focus();
}

function validaFormAlt_Editora()
{
	$('btnSaveForm').disabled = true;
	
	var msg = '';
	
	var tipoPessoa = Form.getInputs("form_cad", "radio", "tipopessoa").find(function(radio) {return radio.checked;}).value;
	
	if (trim($F('cnpjcpf')) == '')
	{
		if (tipoPessoa == 'PJ')
			msg += 'O CNPJ deve ser informado.\n';
		else
			msg += 'O CPF deve ser informado.\n';
	}
	else
	{
		if (! validaCPFCNPJ(tipoPessoa, $F('cnpjcpf')))
		{
			if (tipoPessoa == 'PJ')
				msg += 'CNPJ inválido.\n';
			else
				msg += 'CPF inválido.\n';
		}
	}
	
	if (trim($('razaosocial').value) == '')
	{
		if (tipoPessoa == 'PJ')
			msg += 'A razão social deve ser informada.\n';
		else
			msg += 'O nome deve ser informado.\n';
	}
	
	if ($('naturezajur').value == 0)
	{
		msg += 'A natureza jurídica deve ser informada.\n';
	}
	
	if ($('atividadeprinc').value == 0)
	{
		msg += 'A atividade principal deve ser informada.\n';
	}
	
	if (trim($('telefone1').value) == '')
	{
		msg += 'O telefone geral deve ser informado.\n';
	}
	
	if (trim($('email').value) == '')
	{
		msg += 'O e-mail geral deve ser informado.\n';
	}
	else
	{
		if ( ! validaEmail(trim($('email').value)))
			msg += 'O e-mail geral informado é inválido.\n';
	}
	
	if (trim($('login').value) == '')
	{
		msg += 'O login deve ser informado.\n';
	}
	
	if (trim($('idlogradouro').value) == '')
	{
		msg += 'O endereço deve ser informado.\n';
	}
	
	if (trim($('end_numero').value) == '')
	{
		msg += 'O número do endereço deve ser informado.\n';
	}
	
	if (trim($('nomeresp').value) == '')
	{
		msg += 'O nome do responsável deve ser informado.\n';
	}
	
	if (trim($('telefoneresp').value) == '')
	{
		msg += 'O telefone do responsável deve ser informado.\n';
	}
	
	if (trim($('emailresp').value) == '')
	{
		msg += 'O e-mail do responsável deve ser informado.\n';
	}
	else
	{
		if ( ! validaEmail(trim($('emailresp').value)))
		{
			msg += 'O e-mail do responsável é inválido.\n';
		}
	}
	
	if (msg == '')
	{
		return true;
	}
	else
	{
		$('btnSaveForm').disabled = false;
		alert("ATENÇÃO:\n\n"+msg);
		return false;
	}
}

/*****************************************************
 * LIVROS
 *****************************************************/

function validaForm_preCadLivro()
{
	$('btnSaveForm').disabled = true;
	
	var msg = '';
	
	if (trim($('isbn_livro').value) == '')
	{
		msg += 'O código ISBN deve ser informado.\n';
	}
    else
    {
        if(trim($('isbn_livro').value).length != 10 && trim($('isbn_livro').value).length != 13)
        {
            msg += 'O código ISBN deve conter 10 ou 13 dígitos.\n';
        }

    }
	
	if (msg == '')
	{
        var isbn;
        if(trim($('isbn_livro').value).length == 10)
        {
            isbn = '978' + trim($('isbn_livro').value);
        }
        else
        {
            isbn = trim($('isbn_livro').value);
        }
        
        var digito = isbn.substr(3,2);
        if (digito != '85')
        {
            $('btnSaveForm').disabled = false;
            return validaEstrangISBN();
        }
        else
        {
           return true; 
        }
	}
	else
	{
		$('btnSaveForm').disabled = false;
		alert("ATENÇÃO:\n\n"+msg);
		return false;
	}
}

function validaEstrangISBN(){
    
    var r=confirm("O código ISBN informado é estrangeiro e deverão ser preenchidos todos os campos do formulário.\n\nDeseja continuar?");
    
    if (r==true)
    {
        return true;
    }
    else
    {
        return false;
    }
}

function livro_formcad_onload()
{
	criaAutoCompItensAssunto();
	
	if ($F('loadDadosEdit') == 1)
	{
		$('suporte').value    = $F('suporte_aux');
		$('fichacat').value   = $F('fichacat_aux');
		$('orelha').value     = $F('orelha_aux');
		$('tipocapa').value   = $F('tipocapa_aux');
		$('acabamento').value = $F('acabamento_aux');
		$('idioma').value     = $F('idioma_aux');
		$('idiomatrad').value = $F('idiomatrad_aux');
	}
	
	if ($F('tipocapa') == 3)
	{
		$('divTipoCapaOutra').setStyle({display : 'inline'});
	}
	
	//tratamento para quando volta na validação do form php ou esta editando
	if ($F('idassunto') != '')
	{
		$('imgAssunto').src   = $F('URL_IMG') + 'v_mini.gif';
		$('imgAssunto').alt   = $F('assunto') + ' SELECIONADO';
		$('imgAssunto').title = $F('assunto') + ' SELECIONADO';
	}
}

function validaForm_Livro()
{
	$('btnSaveForm').disabled = true;
	
	var msg = '';
	
	if (trim($F('isbn')) == '')
	{
		msg += 'O código ISBN deve ser informado.\n';
	}
	
	if (trim($F('titulo')) == '')
	{
		msg += 'O título deve ser informado.\n';
	}
	
	if (trim($F('autor')) == '')
	{
		msg += 'O autor deve ser informado.\n';
	}
	
	if ($('suporte').value == 0)
	{
		msg += 'O suporte deve ser informado.\n';
	}
	
	if (trim($F('preco')) == '')
	{
		msg += 'O preço deve ser informado.\n';
	}
	
	if (trim($F('npaginas')) == '' || $F('npaginas') == '0')
	{
		msg += 'O número de páginas deve ser informado.\n';
	}
	
	if ($F('formato_a') != '' || $F('formato_b') != '')
	{
		if (validaNumero($F('formato_a')) && validaNumero($F('formato_b')))
		{
			var fa = parseInt($F('formato_a'));
			var fb = parseInt($F('formato_b'));
			
			if (fa != 0 || fb != 0)
			{
				if (fa == 0 || fb == 0 || fa > fb)
				{
					msg += 'Dimensão inválida.\n';
				}
			}
		}
		else
		{
			msg += 'Dimensão inválida.\n';
		}
	}
	
	if ($('tipocapa').value == 0)
	{
		msg += 'O tipo de capa deve ser informado.\n';
	}
	else if ($('tipocapa').value == 3 && trim($F('tipocapa_outra')) == '')
	{
		msg += 'O tipo de capa deve ser informado.\n';
	}
	
	if ($('acabamento').value == 0)
	{
		msg += 'O acabamento deve ser informado.\n';
	}
	
	if ($('idioma').value == 0)
	{
		msg += 'O idioma original deve ser informado.\n';
	}
	
	if ($F('idassunto') == 0 || $F('idassunto') == '')
	{
		msg += 'O assunto deve ser informado.\n';
	}
	
	if (trim($F('edicao')) == '')
	{
		msg += 'A edição deve ser informada.\n';
	}
	
	if (trim($F('ano')) == '')
	{
		msg += 'O ano deve ser informado.\n';
	}
	
	if (msg == '')
	{
		return true;
	}
	else
	{
		$('btnSaveForm').disabled = false;
		alert("ATENÇÃO:\n\n"+msg);
		return false;
	}
}

function setTipoCapa()
{
	if ($F('tipocapa') == 3)
	{
		$('divTipoCapaOutra').setStyle({display : 'inline'});
		$('tipocapa_outra').focus();
	}
	else
	{
		$('tipocapa_outra').value = '';
		$('divTipoCapaOutra').setStyle({display : 'none'});
	}
}

function criaAutoCompItensAssunto()
{
	var urlAutoComplete1 = $F('URL_EXEC') + 'editora/assunto_autocomplete';
	new Autocomplete('assunto', {
		serviceUrl     : urlAutoComplete1,
		paramAux       : '',
		minChars       : 3,
		width          : 450,
		maxHeight      : 150,
		deferRequestBy : 100,
		onSelect: function(value, data){
			if (data != '' && data != 0)
			{
				$('imgAssunto').src   = $F('URL_IMG') + 'v_mini.gif';
				$('imgAssunto').alt   = value + ' SELECIONADO';
				$('imgAssunto').title = value + ' SELECIONADO';
				$('idassunto').value  = data;
			}
			else
			{
				$('imgAssunto').src   = $F('URL_IMG') + 'x_mini.gif';
				$('imgAssunto').alt   = 'NENHUM ASSUNTO SELECIONADO';
				$('imgAssunto').title = 'NENHUM ASSUNTO SELECIONADO';
				$('idassunto').value  = '';
				$('assunto').value    = '';
			}
		}
	  });
}


function validaForm_livroImportar()
{
	$('btnSaveArquivo').disabled = true;
	
	var msg = '';
    
	if (trim($('arquivo_livro').value) == '')
	{
		msg += 'O arquivo deve ser informado.\n';
	}
	
	if (msg == '')
	{
		return true;
	}
	else
	{
		$('btnSaveArquivo').disabled = false;
		alert("ATENÇÃO:\n\n"+msg);
		return false;
	}
}

//adicionar ao programa livro
function addProgramaLivro(idLivro, acao, preco, qtd)
{
    
	if (trim(idLivro) != '')
	{
		var url = $F('URL_EXEC') + 'editora/adicionaProgramaLivro/' + idLivro + '/' + acao;
		var pars = {'preco' : preco ,'quantidade' : qtd ,'ms' : new Date().getTime()};
		var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: addProgramaLivro_onComplete});
	}
	else
	{
		alert("ATENÇÃO:\n\nLivro não encontrado.");
        $('btnSaveForm').disabled = false;
	}
}

function addProgramaLivro_onComplete(resp)
{
	var strRet = resp.responseText;

	LoadedX();

	if (validaRetorno(strRet))
	{        
        $('ligthboxContext').update(strRet);
        switch(trim($F('acao')))
        {
            case 'save':
                if($('errosValidacao') == null)
                {
                    $('ligthbox_bg').setStyle({display : 'none'});
                    $('lp'+trim($F('idlivro'))).update('Incluído');
                    alert('Salvo com SUCESSO.');
                }
                break;
            case 'remove':
                if($('errosValidacao') == null)
                {
                    $('ligthbox_bg').setStyle({display : 'none'});
                    $('lp'+trim($F('idlivro'))).update('Não Incluído');
                    alert('Removido com SUCESSO.');
                }
                break;
            case 'list':
                $('ligthbox_bg').setStyle({display : 'block'});
                break;
            default:
                alert('Está operação não pode ser realizada.')
        }
        $('btnSaveForm').disabled = false;
	}
}

function validaForm_addProgramaLivro()
{
	$('btnSaveForm').disabled = true;

	var msg = '';

	if (trim($('quantidade').value) == '')
	{
		msg += 'A quantidade deve ser informada.\n';
	}

    if (trim($('preco').value) == '')
	{
		msg += 'O preço deve ser informada.\n';
	}
    
    if(!validaNumero(trim($('quantidade').value), true))
    {
        msg += 'A quantidade deve ser um numero maior que zero.\n';
    }
    
    if(!validaNumero(trim($('preco').value), true))
    {
        msg += 'O preço deve ser um numero maior que zero.\n';
    }
    
    if(parseFloat(getValor($F('preco'))) < parseFloat(getValor($F('precoMin'))) || parseFloat(getValor($F('preco'))) > parseFloat(getValor($F('precoMax'))))
    {
        msg += 'O preço deve estar entre R$ ' + $F('precoMin') + ' e R$ ' + $F('precoMax') + '.\n';
    }
    
	if (msg == '')
	{
		return true;
	}
	else
	{
		$('btnSaveForm').disabled = false;
		alert("ATENÇÃO:\n\n"+msg);
		return false;
	}
}

function validaRemoveProgramaLivro(){
    
    var r=confirm("Remover o livro do programa?");
    if (r==true)
    {
        return true;
    }
    else
    {
        return false;
    }
    
}