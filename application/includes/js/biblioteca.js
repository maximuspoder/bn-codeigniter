var inputReturn = '';
var javascriptToRun = '';


/**
* unificarCadastros()
* Função inicial para unificação dos cadastros
* @return void
*/
function unificarCadastros()
{
	var idsremover = '';
	var objChecks  = document.getElementsByName('IDUSUARIO');
	for(i = 0; i < objChecks.length; i++)
	{
		idsremover += (objChecks[i].checked == true) ? '' : ',' + objChecks[i].value;
	}
	$('IDSREMOVER').value = idsremover.substr(1);
}

/**
* previousbiblioteca()
* Volta uma biblioteca pai das que esta sendo listada
* @return void
*/
function previousbiblioteca()
{
	var index = document.getElementById('filtro_bibpai').selectedIndex - 1;
	if(index >= 0)
	{
		if(document.getElementById('filtro_bibpai').options[document.getElementById('filtro_bibpai').selectedIndex - 1] != null && document.getElementById('filtro_bibpai').options[document.getElementById('filtro_bibpai').selectedIndex - 1] != undefined)
		{
			document.getElementById('filtro_bibpai').options[document.getElementById('filtro_bibpai').selectedIndex - 1].selected = "1";
			$('filtro_submit').click();
		}
	}
}

/**
* nextbiblioteca()
* Volta uma biblioteca pai das que esta sendo listada
* @return void
*/
function nextbiblioteca()
{
	if(document.getElementById('filtro_bibpai').options[document.getElementById('filtro_bibpai').selectedIndex + 1] != null && document.getElementById('filtro_bibpai').options[document.getElementById('filtro_bibpai').selectedIndex + 1] != undefined)
	{
		document.getElementById('filtro_bibpai').options[document.getElementById('filtro_bibpai').selectedIndex + 1].selected = "1";
		$('filtro_submit').click();
	}
}


/**
* ajax_set_cidade_verificada()
* Seta a cidade para verificada ou não, conforme o que for marcado no campo checkbox.
* @return void
*/
function ajax_set_cidade_verificada()
{
	// Coleta id da cidade e faz tratamento dependendo do que estiver marcado
	var idcidade = $('filtro_cidade').value;
	var situacao = ($('filtro_cidade_verificada').checked == true) ? 'S' : 'N';
	var url = $F('URL_EXEC') + 'cidade/set_cidade_verificada/' + idcidade + '/' + situacao;
	var pars = {'ms' : new Date().getTime()};
	var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: ajax_set_cidade_verificada_onComplete});
}

function ajax_set_cidade_verificada_onComplete(objReq)
{
	LoadedX();
}

/**
* ajax_set_cidade_verificada_sniic()
* Seta a cidade para verificada ou não, conforme o que for marcado no campo checkbox.
* @return void
*/
function ajax_set_cidade_verificada_sniic()
{
	// Coleta id da cidade e faz tratamento dependendo do que estiver marcado
	var cidade = $('filtro_cidade').value;
	var situacao = ($('filtro_cidade_verificada').checked == true) ? 'S' : 'N';
	var url = $F('URL_EXEC') + 'cidade/set_cidade_verificada_sniic/' + cidade + '/' + situacao;
	var pars = {'ms' : new Date().getTime()};
	var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: ajax_set_cidade_verificada_sniic_onComplete});
}

function ajax_set_cidade_verificada_sniic_onComplete(objReq)
{
	LoadedX();
}

/**
* modalAssociarAgenciaBB()
* Abre a modal para associacao de agencia do banco do brasil.
* O parametro encaminhado é o id do field para onde deve ser retornado
* o id da agencia. O segundo parametro é um codigo javascript que deve ser
* executado no retorno, como o disparo de um formulario, etc.
* @param string idinputreturn
* @param string javascript
* @return void
*/
function modalAssociarAgenciaBB(idinputreturn, javascript)
{
	inputReturn = idinputreturn;
	javascriptToRun = javascript;
	
	var url = $F('URL_EXEC') + 'biblioteca/modallistaagenciasbb/';
	var pars = {'ms' : new Date().getTime()};
	var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: modalAssociarAgenciaBB_onComplete});
}

function modalAssociarAgenciaBB_onComplete(objReq)
{
	var strRet = objReq.responseText;
	LoadedX();
    $('ligthboxContext').update(strRet);
	$('ligthbox').setStyle({height  : '270px'});
	$('ligthbox').setStyle({width  : '660px'});
	$('ligthbox_bg').setStyle({display : 'block'});
}

/**
* ajaxGetCidades()
* Coleta as cidades de uma determinada uf para a pesquisa.
* @param string uf
* @return void
*/
function ajaxGetCidades(uf)
{
	if (trim(uf) != '')
	{
		var url = $F('URL_EXEC') + 'biblioteca/getcidades/' + uf;
		var pars = {'ms' : new Date().getTime()};
		var myAjax = new Ajax.Request(url, {parameters: pars, onComplete: ajaxGetCidades_onComplete});
	} else {
		// Limpa os options, caso seja vazio a UF
		$('filtro_cidade').update('');
		return false;
	}
}

function ajaxGetCidades_onComplete(objReq)
{
	// alert(objReq.responseText);
	// $('agencias_cidade').innerHTML = objReq.responseText;
	$('filtro_cidade').update(objReq.responseText);
}

/**
* ajaxGetCidadesBib()
* Coleta as cidades de uma determinada uf para a pesquisa.
* @param string uf
* @return void
*/
function ajaxGetCidadesBib(uf)
{
	if (trim(uf) != '')
	{
		var url = $F('URL_EXEC') + 'biblioteca/getcidadessniic/' + uf;
		var pars = {'ms' : new Date().getTime()};
		var myAjax = new Ajax.Request(url, {parameters: pars, onComplete: ajaxGetCidadesBib_onComplete});
	} else {
		// Limpa os options, caso seja vazio a UF
		$('filtro_cidade').update('');
		return false;
	}
}

function ajaxGetCidadesBib_onComplete(objReq)
{
	// alert(objReq.responseText);
	// $('agencias_cidade').innerHTML = objReq.responseText;
	$('filtro_cidade').update(objReq.responseText);
}

/**
* ajaxGetCidadesSniic()
* Coleta as cidades de uma determinada uf para a pesquisa.
* @param string uf
* @return void
*/
function ajaxGetCidadesSniic(uf)
{
	if (trim(uf) != '')
	{
		var url = $F('URL_EXEC') + 'biblioteca/getcidadessniic/' + uf;
		var pars = {'ms' : new Date().getTime()};
		var myAjax = new Ajax.Request(url, {parameters: pars, onComplete: ajaxGetCidadesSniic_onComplete});
	} else {
		// Limpa os options, caso seja vazio a UF
		$('filtro_cidade').update('');
		return false;
	}
}

function ajaxGetCidadesSniic_onComplete(objReq)
{
	// alert(objReq.responseText);
	// $('agencias_cidade').innerHTML = objReq.responseText;
	$('filtro_cidade').update(objReq.responseText);
}

/**
* ajaxGetCidadesBibUnificacao()
* Coleta as cidades de uma determinada uf para a pesquisa levando em consideração
* somente onde existam pai.
* @param string uf
* @return void
*/
function ajaxGetCidadesBibUnificacao(uf)
{
	if (trim(uf) != '')
	{
		var url = $F('URL_EXEC') + 'biblioteca/getcidadesbibunificacao/' + uf;
		var pars = {'ms' : new Date().getTime()};
		var myAjax = new Ajax.Request(url, {parameters: pars, onComplete: ajaxGetCidadesBib_onComplete});
	} else {
		// Limpa os options, caso seja vazio a UF
		$('filtro_cidade').update('');
		return false;
	}
}

function ajaxGetCidadesBib_onComplete(objReq)
{
	// alert(objReq.responseText);
	// $('agencias_cidade').innerHTML = objReq.responseText;
	$('filtro_cidade').update(objReq.responseText);
}

function ajaxGetidspaiunificacao(idcidade)
{
	if (trim(idcidade) != '')
	{
		var url = $F('URL_EXEC') + 'biblioteca/getidspaiunificacao/' + idcidade;
		var pars = {'ms' : new Date().getTime()};
		var myAjax = new Ajax.Request(url, {parameters: pars, onComplete: ajaxGetidspaiunificacao_onComplete});
	} else {
		// Limpa os options, caso seja vazio a UF
		$('filtro_bibpai').update('');
		return false;
	}
}

function ajaxGetidspaiunificacao_onComplete(objReq)
{
	// alert(objReq.responseText);
	// $('agencias_cidade').innerHTML = objReq.responseText;
	$('filtro_bibpai').update(objReq.responseText);
}

/**
* ajaxGetCidadesAgenciaBB()
* Coleta as cidades de uma determinada uf para as agencias bb.
* @param string uf
* @return void
*/
function ajaxGetCidadesAgenciaBB(uf)
{
	if (trim(uf) != '')
	{
		var url = $F('URL_EXEC') + 'biblioteca/getoptionscidadesagenciasbb/' + uf;
		var pars = {'ms' : new Date().getTime()};
		var myAjax = new Ajax.Request(url, {parameters: pars, onComplete: ajaxGetCidadesAgenciaBB_onComplete});
	} else {
		// Limpa os options, caso seja vazio a UF
		$('agencias_cidade').update('');
		$('agencias_bairro').update('');
		return false;
	}
}

function ajaxGetCidadesAgenciaBB_onComplete(objReq)
{
	// alert(objReq.responseText);
	// $('agencias_cidade').innerHTML = objReq.responseText;
	$('agencias_cidade').update(objReq.responseText);
	$('agencias_bairro').update('');
}

/**
* ajaxGetBairrosAgenciaBB()
* Coleta as cidades de uma determinada uf para as agencias bb.
* @param string cidade
* @return void
*/
function ajaxGetBairrosAgenciaBB(cidade)
{
	if (trim(cidade) != '')
	{
		var url = $F('URL_EXEC') + 'biblioteca/getoptionsbairrosagenciasbb/' + cidade;
		var pars = {'ms' : new Date().getTime()};
		var myAjax = new Ajax.Request(url, {parameters: pars, onComplete: ajaxGetBairrosAgenciaBB_onComplete});
	} else {
		// Limpa os options, caso seja vazio a UF
		$('agencias_bairro').update('');
	}
}

function ajaxGetBairrosAgenciaBB_onComplete(objReq)
{
	$('agencias_bairro').update(objReq.responseText);
}

/**
* ajaxGetAgencias()
* Localiza as agências com base nos valores dos combos.
* @return void
*/
function ajaxGetAgencias()
{
	if ($('agencias_uf').value != '' && $('agencias_cidade').value != '' && $('agencias_bairro').value != '')
	{
		var url = $F('URL_EXEC') + 'biblioteca/ajaxgetagencias/' + $('agencias_uf').value + '/' + $('agencias_cidade').value + '/' + $('agencias_bairro').value;
		var pars = {'ms' : new Date().getTime()};
		var myAjax = new Ajax.Request(url, {parameters: pars, onComplete: ajaxGetAgencias_onComplete});
	} else {
		alert("ATENÇÃO:\n\nVocê deve selecionar uma uf, cidade e bairro para realizar a pesquisa");
		return false;
	}
}

function ajaxGetAgencias_onComplete(objreq)
{
	$('return_agencias_ajax').update(objreq.responseText);
}

function ajaxSetBibliotecaPai(pai, filho) {
	var url = $F('URL_EXEC') + 'adm/setpai/' + filho + '/' + pai;
	var pars = {'ms' : new Date().getTime() };
	var myAjax = new Ajax.Request(url, {parameters: pars, onComplete: ajaxSetBibliotecaPai_onComplete});
}

function ajaxSetBibliotecaPai_onComplete(objReq)
{
	// alert('Deu!');
	// $(this).reload();
	// window.location = window.location.href;
	// window.location.reload();
	// $('agencias_bairro').update(objReq.responseText);
	$('button_submit').click();
}

/**
* setAgencia()
* Seta a agencia selecionada.
* @param string nomeagencia
* @return void
*/
function setAgencia(nomeagencia, idagencia)
{
	if(window.confirm("ATENÇÃO!\n\nConfirma a seleção da agência " + nomeagencia + '?'))
	{
		// Seta o input de retorno para o valor selecionado
		$(inputReturn).value = idagencia;
		
		// Roda possiveis javascripts
		if(javascriptToRun != ''){ eval(javascriptToRun); }
		
		// Fecha a modal
		$('ligthbox_bg').setStyle({display : 'none'});
	} else {
		return false;
	}
}

/**
* ajaxGetNomeAgencia()
* Coleta o nome da agencia e coloca no input de id encaminhado.
* @param integer idagencia
* @param string idtoreturn
* @return void
*/
function ajaxGetNomeAgencia(idagencia, idinputname)
{
	// alert(idagencia);
	// alert(idinputname);
	if (idagencia != '' && idinputname)
	{
		var url = $F('URL_EXEC') + 'biblioteca/ajaxgetnomeagencia/' + idagencia;
		var pars = {'ms' : new Date().getTime()};
		var myAjax = new Ajax.Request(url, {parameters: pars, onComplete: function(objRequest){ ajaxGetNomeAgencia_onComplete(objRequest, idinputname); }});
	} else {
		alert("ATENÇÃO:\n\nVocê deve selecionar uma agência primeiramente");
		return false;
	}
}

function ajaxGetNomeAgencia_onComplete(objreq, idinputname)
{
	$(idinputname).value = objreq.responseText;
}

/**
* ajaxDesbloqueiaBiblioteca()
* Remove pendências da biblioteca nas tabelas log_gerenciamento e creditorestricao
* @param integer idusuario
* @param string status
* @return void
*/
function ajaxDesbloqueiaBiblioteca(idbiblioteca)
{
	if (idbiblioteca != '')
	{		
		if(window.confirm("ATENÇÃO!\n\nConfirma o desbloqueio da biblioteca?")) {
			var url = $F('URL_EXEC') + 'biblioteca/desbloqueia/';
			var pars = {'idbiblioteca' : idbiblioteca, 'ms' : new Date().getTime()};
			var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: function(objRequest){ ajaxDesbloqueiaBiblioteca_onComplete(objRequest); }});
			return true;
		} else {
			return false;
		}
		
	} else {
		return false;
	}
}

function ajaxDesbloqueiaBiblioteca_onComplete(objreq)
{
	//$(this).reload();
	// window.location = window.location.href;
	LoadedX();
	window.location.reload();
	//$(idinputname).value = objreq.responseText;
}
/**
* ajaxAlteraEmail()
* Altera e-mail do usuário
* @param integer idusuario
* @param string email
* @return void
*/
function ajaxAlteraEmail(tabela, idusuario, email)
{
	if (tabela != '' && idusuario != '')
	{
		if (email != '')
		{
			if(window.confirm("ATENÇÃO!\n\nConfirma a alteração do e-mail?")) {
				var url = $F('URL_EXEC') + 'biblioteca/alteraemail/';
				var pars = {'tabela' : tabela, 'idusuario' : idusuario, 'email': email, 'ms' : new Date().getTime()};
				var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: function(objRequest){ ajaxAlteraEmail_onComplete(objRequest); }});
			} else {
				return false;
			}
		} else {
			alert("ATENÇÃO:\n\nE-mail não pode ficar em branco");
			return false;
		}
	} else {
		return false;
	}
}

function ajaxAlteraEmail_onComplete(objreq)
{
	alert('E-mail alterado com sucesso.');
	LoadedX();
	window.location.reload();
	//$(idinputname).value = objreq.responseText;
}

/**
* ajaxSetHabilitado()
* Habilita/desabilita biblioteca
* @param integer idusuario
* @param string status
* @return void
*/
function ajaxSetHabilitado(idusuario, status)
{
	if (status != '' && idusuario != '')
	{
		acao = status == 'S' ? 'habilitação' : 'desabilitação' ;
		
		if(window.confirm("ATENÇÃO!\n\nConfirma a " + acao + " da biblioteca?")) {
			var url = $F('URL_EXEC') + 'biblioteca/sethabilitado/';
			var pars = {'idusuario' : idusuario, 'status': status, 'ms' : new Date().getTime()};
			var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: function(objRequest){ ajaxSetHabilitado_onComplete(objRequest); }});
			return true;
		} else {
			return false;
		}
		
	} else {
		return false;
	}
}

function ajaxSetHabilitado_onComplete(objreq)
{
	//$(this).reload();
	// window.location = window.location.href;
	LoadedX();
	window.location.reload();
	//$(idinputname).value = objreq.responseText;
}

/**
* ajaxCancelaPDV()
* Cancela PDV da biblioteca
* @param integer idusuario
* @param string status
* @return void
*/
function ajaxCancelaPDV(idbiblioteca)
{
	if (idbiblioteca != '')
	{		
		if(window.confirm("ATENÇÃO!\n\nConfirma a exclusão do ponto de venda da biblioteca?")) {
			var url = $F('URL_EXEC') + 'biblioteca/removepdvbiblioteca/';
			var pars = {'idbiblioteca' : idbiblioteca, 'ms' : new Date().getTime()};
			var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: function(objRequest){ ajaxCancelaPDV_onComplete(objRequest); }});
			return true;
		} else {
			return false;
		}
		
	} else {
		return false;
	}
}

function ajaxCancelaPDV_onComplete(objreq)
{
	//$(this).reload();
	// window.location = window.location.href;
	LoadedX();
	window.location.reload();
	//$(idinputname).value = objreq.responseText;
}

/**
* ajaxDesfazAssociacaoSniic()
* Habilita/desabilita biblioteca
* @param integer idusuario
* @param string status
* @return void
*/
function ajaxDesfazAssociacaoSniic(idusuario)
{
	if (idusuario != '')
	{		
		if(window.confirm("ATENÇÃO!\n\nConfirma a desassociação deste cadastro SNIIC a esta biblioteca?")) {
			var url = $F('URL_EXEC') + 'biblioteca/removesniic/';
			var pars = {'idusuario' : idusuario, 'ms' : new Date().getTime()};
			var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: function(objRequest){ ajaxDesfazAssociacaoSniic_onComplete(objRequest); }});
			return true;
		} else {
			return false;
		}
		
	} else {
		return false;
	}
}

function ajaxDesfazAssociacaoSniic_onComplete(objreq)
{
	alert('Alteração efetuada com sucesso.');
	LoadedX();
	window.location.reload();
}

function biblioteca_formalt_onload()
{
	if ($F('loadDadosEdit') == 1)
	{
		setTipoBiblioteca('', 0);
		$('naturezajur').value = $F('naturezajur_aux');
	}
	else
	{
		var tipoPessoa = Form.getInputs("form_cad", "radio", "tipopessoa").find(function(radio) { return radio.checked; }).value;
		setTipoBiblioteca(tipoPessoa, 0);
	}
}

function setTipoBiblioteca(tipo, limpar)
{
	if (tipo == '')
	{
		tipo = $F('tipopessoa_aux');
			
		var form  = $('form_cad')
		var nodes = form.getInputs('radio', 'tipopessoa');
		nodes.each(function(node){
				if (node.value == tipo)
					node.checked = true;
				else
					node.checked = false;
		});
	}
	
	if (limpar == 1)
	{
		$('cnpjcpf').value      = '';
		$('iestadual').value    = '';
		$('nomefantasia').value = '';
	}
	
	if (tipo == 'PJ')
	{
		$('spantipopessoa').update('CNPJ');
		$('cnpjcpf').maxLength = 18;
		$('cnpjcpf').setAttribute("onKeyUp","maskCnpj(this)");
		
		$('spannome').update('Razão Social');
		$('linhaie').setStyle({display : ''});
		$('linhanf').setStyle({display : ''});
	}
	else
	{
		$('spantipopessoa').update('CPF');
		$('cnpjcpf').maxLength = 14;
		$('cnpjcpf').setAttribute("onKeyUp","maskCpf(this)");
		
		$('spannome').update('Nome');
		$('linhaie').setStyle({display : 'none'});
		$('linhanf').setStyle({display : 'none'});
	}
	
	$('cnpjcpf').focus();
}

function validaFormAlt_biblioteca()
{
	$('btnSaveForm').disabled = true;
	
	var msg = '';
	
	var tipoPessoa = Form.getInputs("form_cad", "radio", "tipopessoa").find(function(radio) { return radio.checked; }).value;
	
	if (trim($F('cnpjcpf')) == '')
	{
		if (tipoPessoa == 'PJ')
			msg += 'O CNPJ deve ser informado.\n';
		else
			msg += 'O CPF deve ser informado.\n';
	}
	else
	{
		if (! validaCPFCNPJ(tipoPessoa, $F('cnpjcpf')))
		{
			if (tipoPessoa == 'PJ')
				msg += 'CNPJ inválido.\n';
			else
				msg += 'CPF inválido.\n';
		}
	}
	
	if (trim($('razaosocial').value) == '')
	{
		if (tipoPessoa == 'PJ')
			msg += 'A razão social deve ser informada.\n';
		else
			msg += 'O nome deve ser informado.\n';
	}
	
	if ($('naturezajur').value == 0)
	{
		msg += 'O tipo de biblioteca deve ser informado.\n';
	}
	
	if (trim($('telefone1').value) == '')
	{
		msg += 'O telefone geral deve ser informado.\n';
	}
	
	if (trim($('email').value) == '')
	{
		msg += 'O e-mail geral deve ser informado.\n';
	}
	else
	{
		if ( ! validaEmail(trim($('email').value)))
			msg += 'O e-mail geral informado é inválido.\n';
	}
	
	if (trim($('idlogradouro').value) == '')
	{
		msg += 'O endereço deve ser informado.\n';
	}
	
	if (trim($('end_numero').value) == '')
	{
		msg += 'O número do endereço deve ser informado.\n';
	}
	
	if (trim($('nomeresp').value) == '')
	{
		msg += 'O nome do responsável deve ser informado.\n';
	}
	
	if (trim($('telefoneresp').value) == '')
	{
		msg += 'O telefone do responsável deve ser informado.\n';
	}
	
	if (trim($('emailresp').value) == '')
	{
		msg += 'O e-mail do responsável deve ser informado.\n';
	}
	else
	{
		if ( ! validaEmail(trim($('emailresp').value)))
		{
			msg += 'O e-mail do responsável é inválido.\n';
		}
	}
	
	if (msg == '')
	{
		return true;
	}
	else
	{
		$('btnSaveForm').disabled = false;
		alert("ATENÇÃO:\n\n"+msg);
		return false;
	}
}

function savePdvParc()
{
	$('btnSavePP').disabled = true;
	
	var msg = '';
	
	if (trim($F('psel')) == '' || $F('psel') == 0)
	{
		msg += 'Selecione o ponto de venda para salvar.\n';
	}
	
	if (msg == '')
	{
		if (confirm("ATENÇÃO!\n\nApós salvar seu Ponto de Venda Parceiro não será possível alterá-lo.\n\nDeseja continuar?"))
		{
			LoadingX();
			window.location.href = $F('URL_EXEC') + 'biblioteca/savePdvParc/' + $F('psel');
		}
		else
		{
			$('btnSavePP').disabled = false;
			return false;
		}
	}
	else
	{
		$('btnSavePP').disabled = false;
		alert("ATENÇÃO:\n\n"+msg);
		return false;
	}
}

function validaForm_BibliotecaFinan()
{
    $('btnSaveForm').disabled = true;
	
	var msg = '';
	
	if (trim($F('cpf')) == '')
	{
        msg += 'O CPF deve ser informado.\n';
	}
    else
    {
        if(!validaCPFCNPJ('PF',trim($F('cpf'))))
             msg += 'O CPF informado é inválido.\n';
    }
	
	if (trim($('nome').value) == '')
	{
		msg += 'O nome deve ser informado.\n';
	}
	
	if (trim($('telefone').value) == '')
	{
		msg += 'O telefone deve ser informado.\n';
	}
    
    if (trim($('dataNasc').value) == '')
	{
		msg += 'A data de nascimento deve ser informada.\n';
	}
    else
        {
            if(!validaData(trim($('dataNasc').value)))
                msg += 'A data de nascimento informada é inválida.\n';
        }
    
    if (trim($('login').value) == '')
	{
		msg += 'O login deve ser informado.\n';
	}
    else
    {
        if(($('login').value).length < 6)
            msg += 'O login deve conter no minimo 6 caracteres.\n';
    }
    
    if (trim($('nomemae').value) == '')
	{
		msg += 'O nome da mãe deve ser informado.\n';
	}
	
	if (trim($('email').value) == '')
	{
		msg += 'O e-mail deve ser informado.\n';
	}
	else
	{
		if ( ! validaEmail(trim($('email').value)))
        {
			msg += 'O e-mail informado é inválido.\n';
        }
        else
        {
            if(trim($('email').value) != trim($('email2').value))
                msg += 'O e-mail de confirmação informado é inválido.\n';
        }
	}
    
	if($('agenciabb') != 'undefined')
	{
		if (trim($('agenciabb').value) == '')
		{
			msg += 'A agência do banco do brasil deve ser informada.\n';
		}
	}
	
	if (trim($('idlogradouro').value) == '')
	{
		msg += 'O endereço deve ser informado.\n';
	}
	
	if (trim($('end_numero').value) == '')
	{
		msg += 'O número do endereço deve ser informado.\n';
	}
	
	if (msg == '')
	{
		return true;
	}
	else
	{
		$('btnSaveForm').disabled = false;
		alert("ATENÇÃO:\n\n"+msg);
		return false;
	}
}

function validaForm_Comite()
{
    $('btnSaveForm').disabled = true;
	
	var msg = '';
	
	if (trim($F('cpf')) == '')
	{
        msg += 'O CPF deve ser informado.\n';
	}
    else
    {
        if(!validaCPFCNPJ('PF',trim($F('cpf'))))
             msg += 'O CPF informado é inválido.\n';
    }
	
	if (trim($('nome').value) == '')
	{
		msg += 'O nome deve ser informado.\n';
	}
	
	if (trim($('telefone').value) == '')
	{
		msg += 'O telefone deve ser informado.\n';
	}
    
    if (trim($('dataNasc').value) == '')
	{
		msg += 'A data de nascimento deve ser informada.\n';
	}
    else
        {
            if(!validaData(trim($('dataNasc').value)))
                msg += 'A data de nascimento informada é inválida.\n';
        }
    
    if (trim($('login').value) == '')
	{
		msg += 'O login deve ser informado.\n';
	}
    else
    {
        if(($('login').value).length < 6)
            msg += 'O login deve conter no minimo 6 caracteres.\n';
    }
    
    if (trim($('nomemae').value) == '')
	{
		msg += 'O nome da mãe deve ser informado.\n';
	}
	
	if (trim($('email').value) == '')
	{
		msg += 'O e-mail deve ser informado.\n';
	}
	else
	{
		if ( ! validaEmail(trim($('email').value)))
        {
			msg += 'O e-mail informado é inválido.\n';
        }
        else
        {
            if(trim($('email').value) != trim($('email2').value))
                msg += 'O e-mail de confirmação informado é inválido.\n';
        }
	}
    
	if (trim($('idlogradouro').value) == '')
	{
		msg += 'O endereço deve ser informado.\n';
	}
	
	if (trim($('end_numero').value) == '')
	{
		msg += 'O número do endereço deve ser informado.\n';
	}
	
	if (msg == '')
	{
		return true;
	}
	else
	{
		$('btnSaveForm').disabled = false;
		alert("ATENÇÃO:\n\n"+msg);
		return false;
	}
}

function validaForm_setRaio()
{
	var msg = '';
	
	if (trim($('raio').value) == '')
	{
		msg += 'O raio deve ser informado.\n';
	}
	else
	{
		if ( ! validaNumero($('raio').value, true))
		{
			msg += 'Raio inválido.\n';
		}
	}
	
	if (msg == '')
	{
		LoadingX();
		return true;
	}
	else
	{
		alert("ATENÇÃO:\n\n"+msg);
		return false;
	}
}

function confDelComite(idMembro)
{
	if (confirm("ATENÇÃO!\n\nDeseja realmente excluir o membro do Comitê de Acervo?\n\nEssa operação não pode ser desfeita."))
	{
		LoadingX();
		window.location.href = $F('URL_EXEC') + 'biblioteca/comitedel/' + idMembro;
	}
}
