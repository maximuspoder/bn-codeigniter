/*****************************************************
 * PESQUISA
******************************************************/
// View de Livro
function showLivro(idItem)
{
	if (trim(idItem) != '')
	{
		detalharLivro(idItem);
		$('ligthbox').setStyle({height  : '400px'});
		$('ligthbox').setStyle({width  : '660px'});
	} else {
		alert("ATENÇÃO:\n\nLista ou Livro não encontrados.");
	}
}

// Editar quantidade informada na lista de aquisição
var FROM_MODAL = 0;

function modalAddPedidoItem(idItem, from)
{
	FROM_MODAL = from;
	
	if (trim(idItem) != '')
	{
		var url = $F('URL_EXEC') + 'pedido/addPedidoItem/' + idItem;
		var pars = {'ms' : new Date().getTime()};
		var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: modalAddPedidoItem_onComplete});
	} else {
		alert("ATENÇÃO:\n\nLista ou Livro não encontrados.");
	}
}

function modalAddPedidoItem_onComplete(resp)
{
	var strRet = resp.responseText;
	LoadedX();
    $('ligthboxContext').update(strRet);
	$('ligthbox').setStyle({height  : '270px'});
	$('ligthbox').setStyle({width  : '560px'});
	$('ligthbox_bg').setStyle({display : 'block'});
}

// Submita formulário encaminhando a adição do novo item ao pedido
function submitAddPedidoItem(idItem, qtd)
{
	if (trim(idItem) != '' && trim(qtd) != '')
	{
		var url = $F('URL_EXEC') + 'pedido/addNewItem/' + idItem + '/' + qtd;
		var pars = {'ms' : new Date().getTime()};
		var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: submitAddPedidoItem_onComplete});
	} else {
		alert("ATENÇÃO:\n\Livro ou Quantidade não encontrados.");
	}
}

function submitAddPedidoItem_onComplete(resp)
{
	LoadedX();
	if(validaRetorno(resp.responseText))
	{
		alert("ATENÇÃO!\n\nLivro adicionado com sucesso.");
		$('filtros_lista').action = window.location.href;
		$('filtros_lista').submit();
	} 
	/*
	else {
		$('ligthbox_bg').setStyle({display : 'none'});
	}
	*/
}

// Valida Formulário de adição de novo item ao pedido
function validaForm_addPedidoItem()
{
	var msg = '';

	if (trim($('quantidade').value) == '' || trim($('quantidade').value) == 0)
	{
		msg += 'A quantidade deve ser informada.\n';
	}

    if (msg == '')
	{
		return true;
	} else {
		alert("ATENÇÃO:\n\n" + msg);
		return false;
	}
}

// Submita formulário encaminhando a alteração do item ao pedido
function submitUpdatePedidoItem(idItem, qtd)
{
	if (trim(idItem) != '' && trim(qtd) != '')
	{
		var url = $F('URL_EXEC') + 'pedido/updateItem/' + idItem + '/' + qtd;
		var pars = {'ms' : new Date().getTime()};
		var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: submitUpdatePedidoItem_onComplete});
	} else {
		alert("ATENÇÃO:\n\Livro ou Quantidade não encontrados.");
	}
}

function submitUpdatePedidoItem_onComplete(resp)
{
	LoadedX();
	if(validaRetorno(resp.responseText))
	{
		alert("ATENÇÃO!\n\nQuantidade alterada com sucesso.");
		
		if (FROM_MODAL == 2) //from gerenciamento do pedido
		{
			atualizaItensPed();
			atualizaTotaisPed();
			$('ligthboxContext').update('');
			$('ligthbox_bg').setStyle({display : 'none'});
		}
		else
		{
			$('filtros_lista').action = window.location.href;
			$('filtros_lista').submit();
		}
		
		FROM_MODAL = 0;
	}
}

// Valida Formulário de alteração de item ao pedido
function validaForm_updatePedidoItem()
{
	var msg = '';

	if (trim($('quantidade').value) == '' || trim($('quantidade').value) == 0)
	{
		msg += 'A quantidade deve ser informada.\n';
	}

    if (msg == '')
	{
		return true;
	} else {
		alert("ATENÇÃO:\n\n" + msg);
		return false;
	}
}

// Editar quantidade informada na lista de aquisição
function modalRemovePedidoItem(idItem, from)
{
	if (trim(idItem) != '')
	{
		FROM_MODAL = from;
		
		var url = $F('URL_EXEC') + 'pedido/removePedidoItem/' + idItem;
		var pars = {'ms' : new Date().getTime()};
		var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: modalRemovePedidoItem_onComplete});
	} else {
		alert("ATENÇÃO:\n\nLista ou Livro não encontrados.");
	}
}

function modalRemovePedidoItem_onComplete(resp)
{
	var strRet = resp.responseText;
	LoadedX();
    $('ligthboxContext').update(strRet);
	$('ligthbox').setStyle({height  : '340px'});
	$('ligthbox_bg').setStyle({display : 'block'});
}

// Submita formulário encaminhando a adição do novo item ao pedido
function submitRemovePedidoItem(idItem)
{
	if (trim(idItem) != '')
	{
		var url = $F('URL_EXEC') + 'pedido/removeItem/' + idItem;
		var pars = {'ms' : new Date().getTime()};
		var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: submitRemovePedidoItem_onComplete});
	} else {
		alert("ATENÇÃO:\n\Livro ou Quantidade não encontrados.");
	}
}

function submitRemovePedidoItem_onComplete(resp)
{
	LoadedX();
	if(validaRetorno(resp.responseText))
	{
		alert("ATENÇÃO!\n\nLivro removido com sucesso.");
		
		if (FROM_MODAL == 2) //from gerenciamento do pedido
		{
			atualizaItensPed();
			atualizaTotaisPed();
			$('ligthboxContext').update('');
			$('ligthbox_bg').setStyle({display : 'none'});
		}
		else
		{
			$('filtros_lista').action = window.location.href;
			$('filtros_lista').submit();
		}
		
		FROM_MODAL = 0;
	}
}

function indicarLivro()
{
	$('btnSaveForm').setAttribute('disabled', 'disabled');
	var idlivro = $F('idlivro');
	var ideditora = $RF('form_cad', 'ideditora');
	var nPag = $F('nPag');
	
	var boolchecked = false;
	var checks = document.getElementsByName('ideditora');
	var length = checks.length;
	
	for (i = 0; i < length; i++)
	{
		if(checks[i].checked == true)
		{
			boolchecked = true;
		}
	}
	
	if(boolchecked == true)
	{
		if (trim(idlivro) != '')
		{
			var url = $F('URL_EXEC') + 'pesquisa/indicaLivro/';
			var pars = {'ms' : new Date().getTime(), 'idlivro' : idlivro, 'ideditora' : ideditora, 'nPag' : nPag};
			var myAjax = new Ajax.Request(url, {parameters: pars, onComplete: indicarLivro_onComplete});
		} else {
			alert("ATENÇÃO:\n\ Livro não encontrado.");
			$('btnSaveForm').disabled = false;
		}
	} else {
		alert("ATENÇÃO:\n\ Selecione ao menos uma editora.");
		$('btnSaveForm').disabled = false;
	}
}

function indicarLivro_onComplete(resp)
{
    var strRet = resp.responseText;
    //$('ligthbox_bg').setStyle({display : 'none'});
	//LoadedX();
	if(validaRetorno(strRet))
	{
        if(strRet != 'ERRO')
        {
            alert("ATENÇÃO!\n\n Livro indicado com sucesso.");
            $('filtros_lista').setAttribute('action', $F('URL_EXEC') + 'pesquisa/pesquisarLivrosIndicacao/' + strRet); 
            $('filtros_lista').submit();
        }
        else
        {
            alert("ATENÇÃO!\n\n Saldo insuficiente para indicar livros.");
        }
	}
}

function listaEditoras()
{
	var idlivro = $F('idlivro');
	var ideditora = $RF('form_cad', 'ideditora');
	var nPag = $F('nPag');
	
    if (trim(idlivro) != '')
	{
		var url = $F('URL_EXEC') + 'pesquisa/indicaLivro/';
		var pars = {'ms' : new Date().getTime(), 'idlivro' : idlivro, 'ideditora' : ideditora, 'nPag' : nPag};
		var myAjax = new Ajax.Request(url, {parameters: pars, onComplete: indicarLivro_onComplete});
	} else {
		alert("ATENÇÃO:\n\ Livro não encontrado.");
	}
}

function listaEditoras_onComplete(resp)
{
    var strRet = resp.responseText;
    $('ligthbox_bg').setStyle({display : 'none'});
	LoadedX();
	if(validaRetorno(strRet))
	{
        if(strRet != 'ERRO')
        {
            alert("ATENÇÃO!\n\n Livro indicado com sucesso.");
            $('filtros_lista').setAttribute('action', $F('URL_EXEC') + 'pesquisa/pesquisarLivrosIndicacao/' + strRet); 
            $('filtros_lista').submit();
        }
        else
        {
            alert("ATENÇÃO!\n\n Saldo insuficiente para indicar livros.");
        }
	}
}

function ajaxRemoveLivroIndicacoes(id, pg)
{
    if (trim(id) != '')
	{
		var url = $F('URL_EXEC') + 'pesquisa/removeLivro/' + id + '/' + pg;
		var pars = {'ms' : new Date().getTime()};
		var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: ajaxRemoveLivroIndicacoes_onComplete});
	} else {
		alert("ATENÇÃO:\n\ Livro não encontrado.");
	}
}

function ajaxRemoveLivroIndicacoes_onComplete(resp)
{
    var strRet = resp.responseText;
    
	LoadedX();
	if(validaRetorno(strRet))
	{
        if(strRet != 'ERRO')
        {
            alert("ATENÇÃO!\n\n Livro removido com sucesso.");
            $('filtros_lista').setAttribute('action', $F('URL_EXEC') + 'pesquisa/pesquisarLivrosIndicacao/' + strRet); 
            $('filtros_lista').submit();
        }
        else
        {
            alert("ATENÇÃO!\n\n Ocorreu um erro na retirada do livro.");
        }
	}
}

// COMENTARIOS

function modalAddComentario(idLivro)
{
	if (trim(idLivro) != '')
	{
		var url = $F('URL_EXEC') + 'pesquisa/comentarios/' + idLivro;
		var pars = {'ms' : new Date().getTime()};
		var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: modalAddComentario_onComplete});
	}
	else
	{
		alert("ATENÇÃO:\n\nLivro não encontrado.");
	}
}

function modalAddComentario_onComplete(resp)
{
	var strRet = resp.responseText;
	LoadedX();
    $('ligthboxContext').update(strRet);
	$('ligthbox').setStyle({height  : '350px'});
	$('ligthbox').setStyle({width  : '560px'});
	$('ligthbox_bg').setStyle({display : 'block'});
}

// Valida Formulário de alteração de item ao pedido
function validaForm_Comentario()
{
	var msg = '';

	if (trim($('txtcomentario').value) == '')
	{
		msg += 'Deve ser escrito um comentário.\n';
	}

    if (msg == '')
	{
		return true;
	}
	else
	{
		alert("ATENÇÃO:\n\n" + msg);
		return false;
	}
}

// salva comentario
function salvaComentario(idLivro)
{
	if (trim(idLivro) != '')
	{
		var url = $F('URL_EXEC') + 'pesquisa/comentario_save';
		var pars = {'idLivro' : idLivro , 'comentario' : $F('txtcomentario') , 'ms' : new Date().getTime()};
		var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: salvaComentario_onComplete});
	} else {
		alert("ATENÇÃO:\n\Livro não encontrado.");
	}
}

function salvaComentario_onComplete(resp)
{
	LoadedX();
	
	var strRet = resp.responseText;
	
	if(validaRetorno(strRet))
	{
		var arrRet = strRet.split('||');
		
		if (arrRet[0] == 'OK')
		{
			$('ligthboxContext').update('');
			$('ligthbox_bg').setStyle({display : 'none'});
			
			modalAddComentario(arrRet[1]);
		}
		else
		{
			alert("ATENÇÃO!\n\nOcorreu um erro ao salvar. Tente novamente.");
		}
	}
}

function modalPesquisaLivroIndicacaoHelp()
{

	var url = $F('URL_EXEC') + 'pesquisa/modalpesquisalivroindicacaohelp/';
	var pars = {'ms' : new Date().getTime()};
	var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: modalPesquisaLivroIndicacaoHelp_onComplete});
}

function modalPesquisaLivroIndicacaoHelp_onComplete(objReq)
{
	var strRet = objReq.responseText;
	LoadedX();
    $('ligthboxContext').update(strRet);
    $('ligthbox').setStyle({top			: '30%'});
    $('ligthbox').setStyle({left		: '45%'});
	$('ligthbox').setStyle({height		: '570px'});
	$('ligthbox').setStyle({width		: '860px'});
	$('ligthbox_bg').setStyle({display	: 'block'});
}

function listaEditorasTituloUnico(idtitulounico, nPag)
{
	//var args = idbiborigem.split('|');
	
	//var idbiblioteca = args[0];
	//var origem = args[1];
	
    var url = $F('URL_EXEC') + 'pesquisa/lista_editoras_titulo_unico/'+idtitulounico;
    var pars = {'ms' : new Date().getTime(), 'nPag' : nPag };
    var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: listaEditorasTituloUnico_onComplete});
}

function listaEditorasTituloUnico_onComplete(resp)
{
	var strRet = resp.responseText;
	LoadedX();
    $('ligthboxContext').update(strRet);
    $('ligthbox').setStyle({width  : '600px'});
    $('ligthbox').setStyle({height  : '560px'});
	$('ligthbox_bg').setStyle({display : 'block'});
}

function livroIdicacaoBloqueio(npag, livro, tipo )
{ 
    LoadingX();
    $('filtros_lista').setAttribute('action', $F('URL_EXEC') + 'adm/bloqueioLivroIndicacao/'+ npag +'/' + livro + '/' + tipo); 
    $('filtros_lista').submit();
}
