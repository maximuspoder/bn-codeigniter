var LOAD_TOTAIS = 0;
var LOAD_ITENS  = 0;

/**
* download_xls_itens_pedido_editora()
* Retorna um xls dos itens de pedido de uma editora.
* @param integer idusuario
* @return void
*/
function download_xls_itens_pedido_editora(idusuario)
{
	$('download_excel_pedido').src = $F('URL_EXEC') + 'pedido/download_excel_itens_pedido_editora/' + idusuario;
}

/**
* download_xls_itens_pedido_editora_por_pdv()
* Retorna um xls dos itens de pedido de uma editora por pdv.
* @param integer idusuario
* @return void
*/
function download_xls_itens_pedido_editora_por_pdv(idusuario)
{
	$('download_excel_pedido').src = $F('URL_EXEC') + 'pedido/download_excel_itens_pedido_editora_por_pdv/' + idusuario;
}

/**
* downloadExcelPedido()
* Processa o excel do pedido. É necessário que a página tenha um iframe
* com display:none setado para que esta função funcione.
* @param integer idpedido
* @return void
*/
function downloadExcelPedido(idpedido)
{
	$('download_excel_pedido').src = $F('URL_EXEC') + 'pedido/download_excel_pedido/' + idpedido;
}

function onload_gerenciaped()
{
	atualizaItensPed();
	atualizaTotaisPed();
}

function atualizaItensPed()
{
	LOAD_ITENS = 1;
	
	var url = $F('URL_EXEC') + 'pedido/gerencia_itens';
	var pars = {'idprograma' : $F('idprograma') , 'idpedido' : $F('idpedido') , 'ms' : new Date().getTime()};
	var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: atualizaItensPed_onComplete });
}

function atualizaItensPed_onComplete(resp)
{
	var strRet = resp.responseText;
	
	LOAD_ITENS = 0;
	
	if (LOAD_TOTAIS == 0)
	{
		LoadedX();
	}
	
	if (validaRetorno(strRet))
	{
		$('divListaItens').update(strRet);
	}
}

function atualizaTotaisPed()
{
	LOAD_TOTAIS = 1;
	
	var url = $F('URL_EXEC') + 'pedido/gerencia_totais';
	var pars = {'idprograma' : $F('idprograma') , 'idpedido' : $F('idpedido') , 'ms' : new Date().getTime()};
	var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: atualizaTotaisPed_onComplete });
}

function atualizaTotaisPed_onComplete(resp)
{
	var strRet = resp.responseText;
	
	LOAD_TOTAIS = 0;
	
	if (LOAD_ITENS == 0)
	{
		LoadedX();
	}
	
	if (validaRetorno(strRet))
	{
		$('divTotalItens').update(strRet);
	}
}

function enviaPedidoComite(idBiblioteca)
{
	if ($('conf_envio').checked == true)
	{
		if (confirm("ATENÇÃO!\n\nDeseja realmente enviar o pedido? Após essa operação não será possível alterá-lo."))
		{
			//alert('chamada pra modal dos comitês');
			var url = $F('URL_EXEC') + 'pedido/gerencia_validacomite/' + idBiblioteca;
			var pars = {'ms' : new Date().getTime()};
			var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: modalEnviaPedidoComite_onComplete});

		}
	}
	else
	{
		alert("ATENÇÃO!\n\nÉ necessário confirmar que finalizou o pedido para poder enviá-lo.");
	}
}

function modalEnviaPedidoComite_onComplete(resp)
{
	var strRet = resp.responseText;

	if (validaRetorno(strRet))
	{
		LoadedX();
		$('ligthboxContext').update(strRet);
		$('ligthbox').setStyle({width  : '600px'});
		$('ligthbox_bg').setStyle({display : 'block'});
	}
	else
	{
		LoadedX();
	}
}

function modalEnviaPedidoComite(idBiblioteca)
{
	//FROM_MODAL = from;
	
	if (trim(idBiblioteca) != '')
	{
		var url = $F('URL_EXEC') + 'pedido/addPedidoItem/' + idItem;
		var pars = {'ms' : new Date().getTime()};
		var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: modalAddPedidoItem_onComplete});
	} else {
		alert("ATENÇÃO:\n\nBiblioteca não encontrada.");
	}
}

//Submita formulário de confirmação de senhas do comitê
function submitComite()
{
	var idBiblioteca = $F('idbiblioteca');
	var idusuario = new Array( $$('[name="idusuario[]"]').map(Form.Element.getValue) );
	var senha = new Array( $$('[name="senha[]"]').map(Form.Element.getValue) );

	if (trim(idBiblioteca) != '')
	{
		var url = $F('URL_EXEC') + 'pedido/gerencia_verificasenhascomite/';
		var pars = {'ms' : new Date().getTime(), 'idbiblioteca' : idBiblioteca, 'idusuario' : idusuario, 'senha' : senha };
		var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: submitComite_onComplete});
	} else {
		alert("ATENÇÃO:\n\Livro ou Quantidade não encontrados.");
	}
	LoadedX();
}

function submitComite_onComplete(resp)
{
	LoadedX();
	
	var strRet = resp.responseText;

	if(validaRetorno(strRet))
	{
		if (strRet == 'OK')
		{
			// envia pedido
			$('ligthbox_bg').setStyle({display : 'none'});
			
			var url = $F('URL_EXEC') + 'pedido/gerencia_envioped';
			var pars = {'idprograma' : $F('idprograma') , 'idpedido' : $F('idpedido') , 'ms' : new Date().getTime()};
			var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: enviaPedido_onComplete });

		}
		else
		{
			alert("ATENÇÃO!\n\n" + strRet);
		}
	} 
}

// Valida Formulário de confirmação de senhas do comitê
function validaForm_Comite()
{
	var msg = '';
	var i = 0;
	var x = 99;
	
	$$('[name="senha[]"]').map(Form.Element.getValue).each(function(r) {
		if (r == '') {
			msg = 'Ao menos uma das senhas está em branco!';
			if (x == 99) x = i;
		}
		i++;
	});

    if (msg == '')
	{
		return true;
	} else {
		alert("ATENÇÃO:\n\n" + msg);
		$$('[name="senha[]"]')[x].focus();
		return false;
	}
}

/*
 * Esta função não está mais sendo chamada, mas mantive-a inalterada para não impactar em outras partes do sistema
 * que porventura possam estar usando esta função.
 */
function enviaPedido()
{
	if ($('conf_envio').checked == true)
	{
		if (confirm("ATENÇÃO!\n\nDeseja realmente enviar o pedido? Após essa operação não será possível alterá-lo."))
		{
			var url = $F('URL_EXEC') + 'pedido/gerencia_envioped';
			var pars = {'idprograma' : $F('idprograma') , 'idpedido' : $F('idpedido') , 'ms' : new Date().getTime()};
			var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: enviaPedido_onComplete });
		}
	}
	else
	{
		alert("ATENÇÃO!\n\nÉ necessário confirmar que finalizou o pedido para poder enviá-lo.");
	}
}

function enviaPedido_onComplete(resp)
{
	var strRet = resp.responseText;
	
	if (validaRetorno(strRet))
	{
		if (strRet == 'OK')
		{
			alert("Enviado com sucesso!");
			window.location.href = $F('URL_EXEC') + 'pedido/gerencia';
		}
		else
		{
			alert("Não foi possível enviar o pedido. Tente novamente.");
		}
	}
	else
	{
		LoadedX();
	}
}

function confirmaRecebimento()
{
	if ($('conf_receb').checked == true)
	{
		if (confirm("ATENÇÃO!\n\nDeseja realmente confirmar o recebimento do pedido e as quantidades de cada produto?"))
		{
			var url = $F('URL_EXEC') + 'pedido/gerencia_confirmaped';
			var pars = {'idprograma' : $F('idprograma') , 'idpedido' : $F('idpedido') , 'ms' : new Date().getTime()};
			var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: confirmaRecebimento_onComplete });
		}
	}
	else
	{
		alert("ATENÇÃO!\n\nÉ necessário confirmar que recebeu o pedido.");
	}
}

function confirmaRecebimento_onComplete(resp)
{
	var strRet = resp.responseText;
	
	if (validaRetorno(strRet))
	{
		if (strRet == 'OK')
		{
			alert("Confirmado com sucesso!");
			window.location.href = $F('URL_EXEC') + 'pedido/gerencia';
		}
		else
		{
			alert("Não foi possível confirmar o pedido. Tente novamente.");
		}
	}
	else
	{
		LoadedX();
	}
}

function modalAjustePedidoItem(idItem)
{
	if (trim(idItem) != '')
	{
		var url = $F('URL_EXEC') + 'pedido/gerencia_formconf/' + idItem;
		var pars = {'ms' : new Date().getTime()};
		var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: modalAjustePedidoItem_onComplete});
	}
	else
	{
		alert("ATENÇÃO:\n\nLivro não encontrado.");
	}
}

function modalAjustePedidoItem_onComplete(resp)
{
	var strRet = resp.responseText;
	
	LoadedX();
	
    $('ligthboxContext').update(strRet);
	$('ligthbox').setStyle({height  : '270px'});
	$('ligthbox').setStyle({width  : '560px'});
	$('ligthbox_bg').setStyle({display : 'block'});
}

function validaForm_ajusteRecItem()
{
	var msg = '';

	if (trim($('quantidade').value) == '' || trim($('quantidade').value) < 0)
	{
		msg += 'A quantidade deve ser informada.\n';
	}

    if (msg == '')
	{
		return true;
	}
	else
	{
		alert("ATENÇÃO:\n\n" + msg);
		return false;
	}
}

function saveAjusteRecPedido(idItem)
{
	var qtd = $F('quantidade');
	
	if (trim(idItem) != '' && trim(qtd) != '')
	{
		var url = $F('URL_EXEC') + 'pedido/gerencia_saveajusteped/' + idItem + '/' + qtd;
		var pars = {'ms' : new Date().getTime()};
		var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: saveAjusteRecPedido_onComplete});
	}
	else
	{
		alert("ATENÇÃO:\n\Livro ou Quantidade não encontrados.");
	}
}

function saveAjusteRecPedido_onComplete(resp)
{
	LoadedX();
	
	if(validaRetorno(resp.responseText))
	{
		alert("ATENÇÃO!\n\nSalvo com sucesso!");
		
		atualizaItensPed();
		atualizaTotaisPed();
		$('ligthboxContext').update('');
		$('ligthbox_bg').setStyle({display : 'none'});
	}
}

function alteraStatus(idBiblioteca)
{
        
		if (confirm("ATENÇÃO!\n\nDeseja realmente alterar o status do pedido?"))
		{
			var url = $F('URL_EXEC') + 'pedido/alteraStatus/' + idBiblioteca;
			var pars = {'ms' : new Date().getTime()};
			var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: alteraStatus_onComplete});
		}
        
}

function alteraStatus_onComplete()
{
	LoadedX();
	alert("ATENÇÃO!\n\Status do pedido alterado com sucesso!");
	window.location.reload();
}