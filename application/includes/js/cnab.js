/**************************************************************
 *
 *	CNAB
 *
 **************************************************************/

function alteraStatus(IDARQUIVO)
{
        
		if (confirm("ATENÇÃO!\n\nDeseja realmente alterar o status do arquivo?"))
		{
			var url = $F('URL_EXEC') + 'cnab/listaArquivo/' + IDARQUIVO;
			var pars = {'ms' : new Date().getTime()};
			var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: alteraStatus_onComplete});
		}
        
}

function alteraStatus_onComplete()
{
	LoadedX();
	alert("ATENÇÃO!\n\Arquivo transmitido com sucesso!");
	window.location.reload();
}

function listaLogs(idarquivo)
{
	//var args = ID.split('|');
	
	//var id = args[0];
	//var origem = args[1];
	
        var url = $F('URL_EXEC') + 'cnab/lista_log_gerenciamento/'+ idarquivo;
        var pars = {'ms' : new Date().getTime()};
        var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: listaLogs_onComplete});
}

function listaLogs_onComplete(resp)
{
	var strRet = resp.responseText;
	LoadedX();
	$('ligthboxContext').update(strRet);
	/*
	$('ligthbox').setStyle({width  : '600px'});
	$('ligthbox').setStyle({height  : '560px'});
	*/
	$('ligthbox_bg').setStyle({display : 'block'});
}

function validaForm_geraArquivo()
{
    $('btnSaveForm').disabled = true;

	var msg = '';

    if (trim($F('idprograma')) == '')
    {
            msg += 'O edital deve ser informado.\n';
    }
    
    if (trim($F('tipoarquivo')) == '')
    {
            msg += 'O tipo de arquivo deve ser informado.\n';
    }
    
    if (msg == '')
    {
        return true;
    }
    else
    {
        $('btnSaveForm').disabled = false;
        alert("ATENÇÃO:\n\n"+msg);
        return false;
    } 
}

function validaForm_recebeArquivo()
{
    $('btnSaveForm').disabled = true;
	
	var msg = '';
	//alert($F('tipoarquivo'));
    if (trim($F('tipoarquivo')) == '')
    {
            msg += 'O tipo de arquivo deve ser informado.\n';
    }
    
    if (trim($F('arquivo')) == '')
    {
            msg += 'O arquivo deve ser informado.\n';
    }
    
    if (msg == '')
    {
        return true;
    }
    else
    {
        $('btnSaveForm').disabled = false;
        alert("ATENÇÃO:\n\n"+msg);
        return false;
    } 
}