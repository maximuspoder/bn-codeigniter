var map;
var marker     = new Array();
var myLatlng   = new Array();
var arrTxt     = new Array();
var arrId      = new Array();
var arrn       = new Array();
var arrTxt     = new Array();
var infowindow;

function initialize()
{
	var pdvs     = $F('txtpdv');
	var arrPdv   = (pdvs != '' ? pdvs.split('|#') : new Array());
	var qtdPdv   = arrPdv.length;
	var arrPonto = new Array();
	var i;
	
	var latlngCenter;
	var textWin;
	var image = $F('URL_IMG') + 'marker.png';
	
	if (qtdPdv > 0)
	{
		var latlngCenter  = new google.maps.LatLng(-15.775074,-47.938156);
		var myOptions = {
			zoom: 3,
			center: latlngCenter,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}
		
		map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
		
		infowindow = new google.maps.InfoWindow({
			content: '',
			size: new google.maps.Size(50,50),
			disableAutoPan: true
		});
		
		for (i = 0; i < qtdPdv; i++)
		{
			arrPonto = arrPdv[i].split('||');
			
			myLatlng[i] = new google.maps.LatLng(arrPonto[0],arrPonto[1]);
			
			//criando marcador
			marker[i] = new google.maps.Marker({
				position: myLatlng[i], 
				map: map,
				title: arrPonto[2],
				icon: image
			});
			
			//criando janela de informaçoes
			textWin = '<div style="width:100%; float: left; font-weight: bold;">' + arrPonto[2] + '<br />' + arrPonto[3] + '</div>';
			textWin += '<div style="width:100%; float: left;">' + arrPonto[4] + ', ' + arrPonto[5] + '</div>';
			textWin += '<div style="width:100%; float: left;">' + arrPonto[6] + '</div>';
			
			arrTxt[i] = textWin;
			arrId[i]  = arrPonto[7];
			arrn[i]   = arrPonto[2];
			
			//adicionando evento de escuta
			google.maps.event.addListener(marker[i], 'click', function() { clickMarker(this); });
			google.maps.event.addListener(marker[i], 'mouseover', function() { setInfoMarker(infowindow, this); });
			google.maps.event.addListener(marker[i], 'mouseout', function() { infowindow.close(); });
		}
	}
}

function clickMarker(mark)
{
	for (var i = 0; i < marker.length; i++)
	{
		if (marker[i] == mark)
		{
			map.setCenter(myLatlng[i]);
			map.setZoom(15);
			
			/*
			$('npdv').update(arrn[i]);
			$('psel').value = arrId[i];
			$('linkverSel').setAttribute('href', $F('URL_EXEC') + 'pesquisa/exibeDadosPesquisa/4/' + arrId[i]);
			$('divPdvSel').setStyle({display : ''});
			*/
			
			return;
		}
	}
}

function setInfoMarker(elemwin, mark)
{
	for (var i = 0; i < marker.length; i++)
	{
		if (marker[i] == mark)
		{
			elemwin.setContent(arrTxt[i]);
			elemwin.open(map,mark);
			return;
		}
	}
}

function zoomPdv(lat,lon)
{
	var latlngCenter  = new google.maps.LatLng(lat,lon);
	map.setCenter(latlngCenter);
	map.setZoom(15);
}

function selPdvNew(lat,lon,nomePdv,idPdv)
{
	if (lat == 0 && lon == 0)
	{
		var latlngCenter  = new google.maps.LatLng('-14.239424','53.186502');
		map.setCenter(latlngCenter);
		map.setZoom(4);
	}
	else
	{
		zoomPdv(lat,lon);
	}
	
	$('npdv').update(nomePdv);
	$('psel').value = idPdv;
	$('linkverSel').setAttribute('href', $F('URL_EXEC') + 'pesquisa/exibeDadosPesquisa/4/' + idPdv);
	$('divPdvSel').setStyle({display : ''});
}

/*
var geocoder;
function codeAddress()
{
	geocoder      = new google.maps.Geocoder();
	var address = 'rua santo angelo, 341, laranjal, pelotas RS';
	if (geocoder)
	{
		geocoder.geocode( { 'address': address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK)
			{
				map.setCenter(results[0].geometry.location);
				var marker = new google.maps.Marker({
					map: map, 
					position: results[0].geometry.location
				});
			}
			else
			{
				alert("Geocode was not successful for the following reason: " + status);
			}
		});
	}
}

//http://maps.google.com/maps/api/geocode/xml?address=santo+angelo,341+laranjal,pelotas+RS&sensor=false&region=BR
*/
