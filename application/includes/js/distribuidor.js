function dist_formalt_onload()
{
	if ($F('loadDadosEdit') == 1)
	{
		setTipoDist('', 0);
		$('naturezajur').value    = $F('naturezajur_aux');
	}
	else
	{
		var tipoPessoa = Form.getInputs("form_cad", "radio", "tipopessoa").find(function(radio) { return radio.checked; }).value;
		setTipoDist(tipoPessoa, 0);
	}
	
	listaSocios();
	populaAtuacao();
	populaEditoraTrab();
	populaRegiao();
	populaCidade();
}

function setTipoDist(tipo, limpar)
{
	if (tipo == '')
	{
		tipo = $F('tipopessoa_aux');
			
		var form  = $('form_cad')
		var nodes = form.getInputs('radio', 'tipopessoa');
		nodes.each(function(node){
				if (node.value == tipo)
					node.checked = true;
				else
					node.checked = false;
		});
	}
	
	if (limpar == 1)
	{
		$('cnpjcpf').value      = '';
		$('iestadual').value    = '';
		$('nomefantasia').value = '';
	}
	
	if (tipo == 'PJ')
	{
		$('spantipopessoa').update('CNPJ');
		$('cnpjcpf').maxLength = 18;
		$('cnpjcpf').setAttribute("onKeyUp","maskCnpj(this)");
		
		$('spannome').update('Razão Social');
		$('linhaie').setStyle({display : ''});
		$('linhanf').setStyle({display : ''});
	}
	else
	{
		$('spantipopessoa').update('CPF');
		$('cnpjcpf').maxLength = 14;
		$('cnpjcpf').setAttribute("onKeyUp","maskCpf(this)");
		
		$('spannome').update('Nome');
		$('linhaie').setStyle({display : 'none'});
		$('linhanf').setStyle({display : 'none'});
	}
	
	$('cnpjcpf').focus();
}

function validaFormAlt_Dist()
{
	$('btnSaveForm').disabled = true;
	
	var msg = '';
	
	var tipoPessoa = Form.getInputs("form_cad", "radio", "tipopessoa").find(function(radio) { return radio.checked; }).value;
	
	if (trim($F('cnpjcpf')) == '')
	{
		if (tipoPessoa == 'PJ')
			msg += 'O CNPJ deve ser informado.\n';
		else
			msg += 'O CPF deve ser informado.\n';
	}
	else
	{
		if (! validaCPFCNPJ(tipoPessoa, $F('cnpjcpf')))
		{
			if (tipoPessoa == 'PJ')
				msg += 'CNPJ inválido.\n';
			else
				msg += 'CPF inválido.\n';
		}
	}
	
	if (trim($('razaosocial').value) == '')
	{
		if (tipoPessoa == 'PJ')
			msg += 'A razão social deve ser informada.\n';
		else
			msg += 'O nome deve ser informado.\n';
	}
	
	if ($('naturezajur').value == 0)
	{
		msg += 'A natureza jurídica deve ser informada.\n';
	}
	
	if (trim($('telefone1').value) == '')
	{
		msg += 'O telefone geral deve ser informado.\n';
	}
	
	if (trim($('email').value) == '')
	{
		msg += 'O e-mail geral deve ser informado.\n';
	}
	else
	{
		if ( ! validaEmail(trim($('email').value)))
			msg += 'O e-mail geral informado é inválido.\n';
	}
	
	if (trim($('login').value) == '')
	{
		msg += 'O login deve ser informado.\n';
	}
	
	if (trim($('idlogradouro').value) == '')
	{
		msg += 'O endereço deve ser informado.\n';
	}
	
	if (trim($('end_numero').value) == '')
	{
		msg += 'O número do endereço deve ser informado.\n';
	}
	
	if (trim($('nomeresp').value) == '')
	{
		msg += 'O nome do responsável deve ser informado.\n';
	}
	
	if (trim($('telefoneresp').value) == '')
	{
		msg += 'O telefone do responsável deve ser informado.\n';
	}
	
	if (trim($('emailresp').value) == '')
	{
		msg += 'O e-mail do responsável deve ser informado.\n';
	}
	else
	{
		if ( ! validaEmail(trim($('emailresp').value)))
		{
			msg += 'O e-mail do responsável é inválido.\n';
		}
	}
	
	if (msg == '')
	{
		return true;
	}
	else
	{
		$('btnSaveForm').disabled = false;
		alert("ATENÇÃO:\n\n"+msg);
		return false;
	}
}
