//pesquisa ceps através do formulário de pesqui com uf, cidade e logradouro
function pesquisaCepLogradouro()
{
	var logra = trim($F('logradouro_pesq'));
	
	if (logra != '' && $F('uf_pesq') != '')
	{
		var url = $F('URL_EXEC') + 'cep/pesquisa2';
		var pars = {nomelogra : logra , uf : $F('uf_pesq') , cidade : $F('cidade_pesq') , 'ms' : new Date().getTime()};
		var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: pesquisaCepLogradouro_onComplete });
	}
	else
	{
		alert("ATENÇÃO:\n\nSelecione um estado e informe o nome total ou parcial do logradouro.");
		$('logradouro_pesq').focus();
	}
}

function pesquisaCepLogradouro_onComplete(resp)
{
	var strRet = resp.responseText;

	LoadedX();
	
	if (validaRetorno(strRet))
	{
		$('resultPesqCep').setStyle({border : '1px solid #FFFFFF'});
		$('resultPesqCep').update(strRet);
	}
}

//pesquisa logradouros a partir de um cep
function pesquisaCep(elemCep, elemCompletar)
{
	var cep = trim($F(elemCep));
	
	if (cep.length == 8 || cep.length == 0)
	{
		var url = $F('URL_EXEC') + 'cep/pesquisa';
		var pars = {'cep' : cep , 'elemCompletar' : elemCompletar , 'ms' : new Date().getTime()};
		var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: pesquisaCep_onComplete });
	}
	else
	{
		alert("ATENÇÃO:\n\nCEP inválido.");
		$(elemCep).focus();
	}
}

function pesquisaCep_onComplete(resp)
{
	var strRet = resp.responseText;

	LoadedX();
	
	if (validaRetorno(strRet))
	{
		$('popCep').setStyle({display : 'block'});
		$('popCepCombo').update(strRet);
	}
}

//abre formulario para cadastro de um logradouro
function formCadCep(cep)
{
	if (cep.length == 8 || cep.length == 0)
	{
		var url = $F('URL_EXEC') + 'cep/formcad';
		var pars = {'cep' : cep , 'ms' : new Date().getTime()};
		var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: formCadCep_onComplete });
	}
	else
	{
		alert("ATENÇÃO:\n\nCEP inválido.");
		$(elemCep).focus();
	}
}

function formCadCep_onComplete(resp)
{
	var strRet = resp.responseText;
	
	LoadedX();
	
	if (validaRetorno(strRet))
	{
		$('popCep').setStyle({display : 'block'});
		$('popCepCombo').update(strRet);
	}
}

function fechaPesquisaCep()
{
	$('popCepCombo').update('');
	$('popCep').setStyle({display : 'none'});
}

function selecionaCep(idLogradouro, elemCompletar)
{
	var arrElem = elemCompletar.split('#');
	//arrElem = (0 => CEP , 1 => IDLOGRADOURO , 2 => UF , 3 => CIDADE , 4 => BAIRRO , 5 => LOGRADOURO , 6 => COMPLEMENTO , 7 => PROXIMO ELEMENTO FOCUS)
	
	//CEP
	$(arrElem[0]).value = $('cep_'+idLogradouro).value;
	//IDLOGRADOURO
	$(arrElem[1]).value = idLogradouro;
	//UF
	$(arrElem[2]).value = $('uf_'+idLogradouro).value;
	//CIDADE
	$(arrElem[3]).value = $('cidade_'+idLogradouro).value;
	//BAIRRO
	$(arrElem[4]).value = $('bairro_'+idLogradouro).value;
	//LOGRADOURO
	$(arrElem[5]).value = $('logradouro_'+idLogradouro).value;
	//COMPLEMENTO
	$(arrElem[6]).value = $('complemento_'+idLogradouro).value;
	//PROXIMO ELEMENTO (FOCUS)
	if (arrElem[7] != '')
	{
		$(arrElem[7]).focus();
	}
	
	fechaPesquisaCep();
}

function apagaCep(elemCep, elemApagar)
{
	var arrElem = elemApagar.split('#');
	//arrElem = (0 => CEP , 1 => IDLOGRADOURO , 2 => UF , 3 => CIDADE , 4 => BAIRRO , 5 => LOGRADOURO , 6 => COMPLEMENTO)
	
	//CEP
	$(arrElem[0]).value = '';
	//IDLOGRADOURO
	$(arrElem[1]).value = '';
	//UF
	$(arrElem[2]).value = '';
	//CIDADE
	$(arrElem[3]).value = '';
	//BAIRRO
	$(arrElem[4]).value = '';
	//LOGRADOURO
	$(arrElem[5]).value = '';
	//COMPLEMENTO
	$(arrElem[6]).value = '';
}

/********************** 
 * Depois de feita uma pesquisa por CEP, se achou apenas parcial e solicita o cadastro
 * essa função completa o cadastro com as informações encontradas do CEP
 **********************/
function completaCadastroCEP(cidade, bairro)
{
	if (cidade != '' && bairro == '')
	{
		carregaBairros('cidade_cad', 'bairro_cad', 1);
	}
}

function saveCep()
{
	var cep        = $F('cep_cad');
	var uf         = $F('uf_cad');
	var cidade     = $F('cidade_cad');
	var bairro     = $F('bairro_cad');
	var novoBairro = $F('novoBairro');
	var tipologra  = $F('tipologra_cad');
	var logradouro = $F('logradouro_cad');
	var msg        = '';
	
	if (cep.length != 8)
	{
		msg += 'CEP inválido.\n'
	}
	
	if (uf == '')
	{
		msg += 'Selecione um estado.\n'
	}
	
	if (cidade == 0 || cidade == null)
	{
		msg += 'Selecione uma cidade.\n'
	}
	
	if (bairro == 0 || bairro == null)
	{
		msg += 'Selecione um bairro.\n'
	}
	else if (bairro == -1)
	{
		if (trim(novoBairro) == '')
		{
			msg += 'Informe o nome do novo bairro.\n'
		}
	}
	
	if (tipologra == 0)
	{
		msg += 'Selecione o tipo do logradouro.\n'
	}
	
	if (trim(logradouro) == '')
	{
		msg += 'Informe o nome do logradouro.\n'
	}
	
	if (msg != '')
	{
		alert("ATENÇÃO\n\n" + msg);
		return false;
	}
	else
	{
		//salvar novo endereço do CEP
		var url = $F('URL_EXEC') + 'cep/save';
		var pars = {
					cep        : cep , 
					uf         : uf , 
					cidade     : cidade , 
					bairro     : bairro , 
					novoBairro : novoBairro , 
					tipologra  : tipologra , 
					logradouro : logradouro , 
					ms         : new Date().getTime()
					};
		var myAjax = new Ajax.Request(url, {parameters: pars, onLoading: LoadingX(), onComplete: saveCep_onComplete });
	}
}

function saveCep_onComplete(resp)
{
	var strRet = resp.responseText;

	LoadedX();
	
	if (validaRetorno(strRet))
	{
		pesquisaCep('cep','cep#idlogradouro#uf#cidade#bairro#logradouro#logcomplemento#end_numero');
	}
}

function selecionaBairro(elemBairro)
{
	escondeCadNovoBairro();
	
	if ($F(elemBairro) == -1) //cadastra novo bairro
	{
		$('spanNovoBairro').setStyle({display : ''});
		$('novoBairro').focus();
	}
}

function escondeCadNovoBairro()
{
	try
	{
		$('novoBairro').value = '';
		$('spanNovoBairro').setStyle({display : 'none'});
	}
	catch(e){ }
}

/**************************************
 * CARREGANDO COMBOS
 **************************************/

var COMBO_CIDADE = '';
var COMBO_BAIRRO = '';

function carregaCidades(elemUf, elemCidade, elemBairro)
{
	COMBO_CIDADE = elemCidade;
	COMBO_BAIRRO = elemBairro;
	escondeCadNovoBairro();
	
	if ($F(elemUf) != '')
	{
		$('img_'+COMBO_CIDADE).setStyle({visibility : 'visible'});
		
		var url = $F('URL_EXEC') + 'cep/comboCidades';
		var pars = {'uf' : $F(elemUf) , 'ms' : new Date().getTime()};
		new Ajax.Request(url, {parameters: pars, onComplete: carregaCidades_onComplete});
	}
	else
	{
		myPowerCombo = new PowerCombo(COMBO_CIDADE, '', true, false, '');
		myPowerCombo.insert();
		
		if (COMBO_BAIRRO != '')
		{
			myPowerCombo = new PowerCombo(COMBO_BAIRRO, '', true, false, '');
			myPowerCombo.insert();
		}
		
		COMBO_CIDADE = '';
		COMBO_BAIRRO = '';
	}
}

function carregaCidades_onComplete(resp)
{
	var strRet = resp.responseText;
	
	if (validaRetorno(strRet))
	{
		myPowerCombo = new PowerCombo(COMBO_CIDADE, strRet, true, false, '');
		myPowerCombo.defSep('~#', '|#');
		myPowerCombo.insert();
		
		if (COMBO_BAIRRO != '')
		{
			myPowerCombo = new PowerCombo(COMBO_BAIRRO, '', true, false, '');
			myPowerCombo.insert();
		}
		
		$('img_'+COMBO_CIDADE).setStyle({visibility : 'hidden'});
		
		COMBO_CIDADE = '';
		COMBO_BAIRRO = '';
	}
}

function carregaBairros(elemCidade, elemBairro, opcaoCad)
{
	COMBO_BAIRRO = elemBairro;
	escondeCadNovoBairro();
	
	if ($F(elemCidade) != 0)
	{
		$('img_'+COMBO_BAIRRO).setStyle({visibility : 'visible'});
		
		var url = $F('URL_EXEC') + 'cep/comboBairros/' + opcaoCad;
		var pars = {'cidade' : $F(elemCidade) , 'ms' : new Date().getTime()};
		new Ajax.Request(url, {parameters: pars, onComplete: carregaBairros_onComplete});
	}
	else
	{
		myPowerCombo = new PowerCombo(COMBO_BAIRRO, '', true, false, '');
		myPowerCombo.insert();
		
		COMBO_BAIRRO = '';
	}
}

function carregaBairros_onComplete(resp)
{
	var strRet = resp.responseText;
	
	if (validaRetorno(strRet))
	{
		myPowerCombo = new PowerCombo(COMBO_BAIRRO, strRet, true, false, '');
		myPowerCombo.defSep('~#', '|#');
		myPowerCombo.insert();
		
		$('img_'+COMBO_BAIRRO).setStyle({visibility : 'hidden'});
		
		COMBO_BAIRRO = '';
	}
}
