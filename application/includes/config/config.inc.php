<?php
//CONFIGURAÇÕES DE BANCO DE DADOS
	define('BD_HOST',     'srvcit05');
	define('BD_DATABASE', 'binac');
	define('BD_USER',     'root');
	define('BD_PASSWORD', '89#cnt432');

//ENVIO DE E-MAILS
	define('EMAIL_FROM',         'bn@conectait.com.br');
	define('EMAIL_FROM_NAME',    'Biblioteca Nacional');
	define('EMAIL_USER',         'bn');
	define('EMAIL_PASSWORD',     '12qwaszx');


//CONFIGURAÇÕES DO SISTEMA
	define('TITLE_SISTEMA',       '.:: Biblioteca Nacional ::.');												//título das páginas
	define('PREF_COOKIE_CLIENTE', 'binac');																		//prefixo dos cookies do sistema
	define('BASE_URL_SISTEMA', 	  'http://10.10.1.41:8080/bn/BN_182_fernando_programa_pendencia_pedido_nota_num_entrega_nulo/');		 				//url para chamada se controllers
	//define('BASE_URL_SISTEMA', 	  'http://10.10.1.41:8080/bn/BN_174_ajuste_modal_busca_enderecos/');		 				//url para chamada se controllers
	define('BASE_URL_ARQUIVOS',   $_SERVER['DOCUMENT_ROOT'] . '/bn/BN_182_fernando_programa_pendencia_pedido_nota_num_entrega_nulo/');
	define('URL_EXEC',            BASE_URL_SISTEMA);				 											//url para chamada se controllers
	define('URL_INCLUDES',        BASE_URL_SISTEMA . 'application/includes/');									//url do diretorio de includes
	define('URL_CSS',             URL_INCLUDES . 'css/');														//dir arquivos css
	define('URL_JS',              URL_INCLUDES . 'js/');														//dir arquivos js
	define('URL_IMG',             URL_INCLUDES . 'img/');														//dir de imagens

//SERVICOS
	define('MAPS_HOST', 'http://maps.google.com/maps/api/geocode/xml?sensor=false&region=BR');
	define('IDPROGRAMA', 1);                                                        							//código do programa
	define('NOMEPROGRAMA', 'Livro Popular');                                        							//nome do programa
	define('NOME_EDITAL', 'NOME DO EDITAL AQUI');                                   							//nome do edital para habilitação

/* End of file config.inc.php */