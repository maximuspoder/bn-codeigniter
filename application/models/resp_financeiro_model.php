<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe Resp_Financeiro_Model
*
* Abstração da camada modelo para responsáveis financeiros do sistema.
* 
* @author		Ricardo Neves Becker
* @package		application
* @subpackage	models.resp_financeiro
* @since		06/07/2012
*
*/
class Resp_Financeiro_Model extends CI_Model {
	
	/**
	* __construct()
	* @return object
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	/**
	* get_lista_modulo()
	* Retorna os dados da listagem de entrada do modulo.
	* return array data
	*/
	function get_lista_modulo($filters = array())
	{
		$sql = "SELECT CF.IDUSUARIO 		 AS `#`, 
					   CF.RAZAOSOCIAL 		 AS `NOME`,
					   CB.RAZAOSOCIAL 		 AS `BIBLIOTECA`,
					   AB.CODAGENCIACOMPLETO AS `AGENCIA BB`,
					   CF.TELEFONERESP       AS `TELEFONE`,
					   CF.EMAILRESP          AS `EMAIL`,
					   U.LOGIN 				 AS `LOGIN`,
					   CASE WHEN U.ATIVO = 'N' THEN '<img src=\"" . URL_IMG . "icon_bullet_red.png\" title=\"USUARIO DESATIVADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_bullet_green_2.png\" title=\"USUARIO ATIVO\" />' END AS `ATIVO_IMG`,
					   CASE 
							WHEN cf.SIT_CARTAO = 'SEM_CARTAO' THEN '<img src=\"" . URL_IMG . "icon_cartao_sem.png\" title=\"SEM CARTÃO\" />'  
							WHEN cf.SIT_CARTAO = 'CARTAO_GERADO' THEN '<img src=\"" . URL_IMG . "icon_cartao_ok.png\" title=\"CARTÃO GERADO (OK)\" />' 
							WHEN cf.SIT_CARTAO = 'CARTAO_BLOQUEADO' THEN '<img src=\"" . URL_IMG . "icon_cartao_bloqueado.png\" title=\"CARTÃO BLOQUEADO\" />' 
							WHEN cf.SIT_CARTAO = 'REMESSA_CADASTRO' THEN '<img src=\"" . URL_IMG . "icon_cartao_processo_geracao.png\" title=\"CARTÃO EM PROCESSO DE GERAÇÃO\" />'
					   END AS CARTAO_IMG
				  FROM cadbibliotecafinan CF
			INNER JOIN usuario U ON (U.IDUSUARIO = CF.IDUSUARIO)
			 LEFT JOIN cadbiblioteca CB ON (CF.IDBIBLIOTECA = CB.IDUSUARIO)
			 LEFT JOIN agenciabb     AB ON (AB.IDAGENCIA = CF.IDAGENCIA)";
		
		// Carrega model auxiliar para processamento do where
		$this->load->model('global_model');
		$sql .= $this->global_model->get_where_filters($filters);
		
		// Concatena ORDER BY
		$sql .= ' ORDER BY CB.RAZAOSOCIAL';
		
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados)) ? $dados : array();
	}
	
	
	/*
	* get_dados_resp_financeiro()
	* Retorna os dados de um Responsavel financeiro
	* @param integer idusuario
	* return array biblioteca
	*/
	function get_dados_resp_financeiro($idusuario = 0, $idprograma = 0)
	{
		$sql = "SELECT CF.IDUSUARIO AS IDUSUARIO, 
					   CF.* ,
					   U.*,
					   L.*,
					   LT.*,
					   B.*,
					   C.*,
					   CASE WHEN up.IDPROGRAMA IS NULL THEN 'N' ELSE 'S' END AS HABILITADO,
					   CF.IDUSUARIO,
					   CASE WHEN u.ATIVO = 'N' THEN '<img src=\"" . URL_IMG . "icon_bullet_red.png\" title=\"USUARIO DESATIVADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_bullet_green_2.png\" title=\"USUARIO ATIVO\" />' END AS ATIVO_IMG,
					   CASE WHEN up.IDPROGRAMA IS NULL THEN '<img src=\"" . URL_IMG . "icon_flag_red.png\" title=\"USUARIO DESABILITADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_flag_green.png\" title=\"USUARIO HABILITADO\" />' END AS HABILITADO_IMG
				  FROM CADBIBLIOTECAFINAN   CF 
		    INNER JOIN USUARIO          U ON (U.IDUSUARIO = CF.IDUSUARIO) 
		     LEFT JOIN LOGRADOURO       L ON (L.IDLOGRADOURO = CF.IDLOGRADOURO) 
		     LEFT JOIN LOGRADOUROTIPO  LT ON (LT.IDLOGRADOUROTIPO = L.IDLOGRADOUROTIPO)
			 LEFT JOIN BAIRRO           B ON (B.IDBAIRRO = L.IDBAIRRO)
		     LEFT JOIN CIDADE           C ON (C.IDCIDADE = L.IDCIDADE) 
			 LEFT JOIN USUARIOPROGRAMA UP ON (UP.IDUSUARIO = CF.IDUSUARIO AND UP.IDPROGRAMA = $idprograma)
			WHERE CF.IDUSUARIO = $idusuario";
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados[0])) ? $dados[0] : array();
	}
}
