<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Configuracao_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	function lista($arrParam, $onlyCount = FALSE)
	{
		if ($onlyCount)
		{
			$sql = "SELECT count(*) as CONT FROM configuracao";
		}
		else
		{
			$sql = sprintf("SELECT c.IDCONFIGURACAO,
								   c.DESCCONFIG,
								   c.TIPOCONFIG,
								   pc.DATA_DE,
								   pc.DATA_ATE,
								   pc.VALORDEC,
								   pc.VALORINT
							  FROM configuracao c
                            LEFT JOIN programaconfig pc
								ON (c.IDCONFIGURACAO = pc.IDCONFIGURACAO AND pc.IDPROGRAMA = %s)
						  ORDER BY IDCONFIGURACAO
						     LIMIT %s, %s", $arrParam['programa'], $arrParam['limit_de'], $arrParam['exibir_pp']);
		}
		
		$dados = $this->db->query($sql);
		return $dados->result_array();
	}
	
	function getDadosToForm($arrParam)
	{
		$dados  = $this->lista($arrParam);
		$return = array();
		foreach($dados as $row)
		{
			switch(strtoupper($row['TIPOCONFIG']))
			{
				case 'PERIODO' :
					$return[strtolower($row['TIPOCONFIG'] . '_DE_' . $row['IDCONFIGURACAO'])]  = eua_to_br($row['DATA_DE']);
					$return[strtolower($row['TIPOCONFIG'] . '_ATE_' . $row['IDCONFIGURACAO'])] = eua_to_br($row['DATA_ATE']);
				break;
				
				case 'INTEIRO':
					$return[strtolower($row['TIPOCONFIG'] . '_' . $row['IDCONFIGURACAO'])] = $row['VALORINT'];
				break;
				
				case 'DECIMAL':
					$return[strtolower($row['TIPOCONFIG'] . '_' . $row['IDCONFIGURACAO'])] = ($row['VALORDEC'] != '') ? masc_real($row['VALORDEC']) : '';
				break;
			}
		}
		return $return;
	}
	
	function saveConfiguracaoValue($tipo, $programa, $value = null, $idConfig = null, $idUsuario = null)
	{
		if(!is_null($tipo) && !is_null($value) && !is_null($idConfig) && !is_null($idUsuario))
		{
			// Remove a configuração anterior
			$this->deleteConfiguracaoValue($programa, $idConfig);
			
			// Valida o tipo encaminhado, e conforme o tipo, o sql é alterado
			switch(strtoupper($tipo))
			{
				case 'DECIMAL':
					$sql = sprintf("INSERT INTO programaconfig (IDPROGRAMA, IDCONFIGURACAO, VALORDEC, IDUSUARIO) VALUES (%s, %s, %s, %s)", $programa, $idConfig, $value, $idUsuario);
				break;
				
				case 'INTEIRO':
					$sql = sprintf("INSERT INTO programaconfig (IDPROGRAMA, IDCONFIGURACAO, VALORINT, IDUSUARIO) VALUES (%s, %s, %s, %s)", $programa, $idConfig, $value, $idUsuario);
				break;
				
				case 'PERIODO':
					$sql = sprintf("INSERT INTO programaconfig (IDPROGRAMA, IDCONFIGURACAO, DATA_DE, DATA_ATE, IDUSUARIO) VALUES (%s, %s, '%s', '%s', %s)", $programa, $idConfig, $value[0], $value[1], $idUsuario);
				break;
				
				default: break;
			}
			
			// Executa Sql
			// echo($sql . "<br /><br />");
			$this->db->query($sql);
			return true;
		}
		return false;
	}
	
	function deleteConfiguracaoValue($programa, $idConfig = null)
	{
		if(!is_null($idConfig))
		{
			$sql = sprintf("DELETE FROM programaconfig WHERE IDCONFIGURACAO = %s AND IDPROGRAMA = %s", $idConfig, $programa);

			// Executa Sql
			// echo($sql . "<br /><br />");
			$this->db->query($sql);
			return true;
		}
		return false;
	}
	
	function getConfiguracaoName($programa, $idConfig = null)
	{
		if(!is_null($idConfig))
		{
			$sql = sprintf("SELECT DESCCONFIG FROM configuracao c
                            LEFT JOIN programaconfig pc ON (c.IDCONFIGURACAO = pc.IDCONFIGURACAO AND pc.IDPROGRAMA = %s) WHERE c.IDCONFIGURACAO = %s ",$programa, $idConfig);

			// Executa Sql
			$dados  = $this->db->query($sql);
			$return = $dados->result_array();
			return $return[0]['DESCCONFIG'];
		}
		return false;
	}
	
	function validaConfig($idConfig = null, $value = null, $programa = '')
	{
		// Carrega library de session
		$this->load->library('session');
		$return = false;
        
        if($programa == '')
        {	
			// if(!isset($_SESSION))
			if($this->session->userdata('programa') == 0)
			{
				$programa = 1;
			}
			else
			{
				$programa = $this->session->userdata('programa');
			}
        }

		if(!is_null($idConfig) && !is_null($value))
		{
			// Os ids de configurações podem ser um array de ids, neste caso
			// o value encaminhado é validado entre os valores do id de configuração
			// 1 e 2, respectivamente, caso contrário só verifica através do tipo se 
			// os valores estão validados.
			if(is_array($idConfig))
			{
				// print_r($idConfig);
				// $counter = count($idConfig);
				$i = 0;
				$values_compare = array();
				$ids = implode(',', $idConfig);
				$sql = sprintf("SELECT pc.DATA_DE, 
				                       pc.DATA_ATE, 
				  					   pc.VALORDEC, 
									   pc.VALORINT, 
									   c.TIPOCONFIG 
								  FROM programaconfig pc 
							INNER JOIN configuracao c 
									ON (c.IDCONFIGURACAO = pc.IDCONFIGURACAO)
							  	 WHERE pc.IDCONFIGURACAO IN (%s)
								   AND pc.IDPROGRAMA = %s", $ids, $programa);
				$dados  = $this->db->query($sql);
				$return = $dados->result_array();
				
				foreach($return as $row)
				{
					if(isset($row))
					{
						// Faz switch pelo tipo de dado, nao é testado o tipo PERIODO
						// pois periodo tem os valores em colunas e não em linhas
						switch(strtoupper($row['TIPOCONFIG']))
						{
							case 'DECIMAL':
								$values_compare[$i] = $row['VALORDEC'];
							break;
							
							case 'INTEIRO':
								$values_compare[$i] = $row['VALORINT'];
							break;
							
							default: break;
						}
					}
					$i++;
				}
				
				// Testa o vlaor encaminhado no estilo between
				if(count($values_compare) > 1)
				{
					$return = (($value >= $values_compare[0]) && ($value <= $values_compare[1])) ? true : false;
				}
				else
				{
					$return = false;
				}
			}
			else
			{
				$sql = sprintf("SELECT pc.DATA_DE, 
									   pc.DATA_ATE, 
									   pc.VALORDEC, 
									   pc.VALORINT, 
									   c.TIPOCONFIG, 
									   c.DESCCONFIG
								  FROM programaconfig pc 
							INNER JOIN configuracao c ON (c.IDCONFIGURACAO = pc.IDCONFIGURACAO)
								 WHERE pc.IDCONFIGURACAO = %s 
								   AND pc.IDPROGRAMA = %s", $idConfig, $programa);
				// echo($sql);
				$dados  = $this->db->query($sql);
				$return = $dados->result_array();
				
				if(isset($return[0]))
				{
					// Faz switch pelo tipo de dado, nao é testado o tipo PERIODO
					// pois periodo tem os valores em colunas e não em linhas
					switch(strtoupper($return[0]['TIPOCONFIG']))
					{
						case 'PERIODO':
							$return = ((datediff($value, $return[0]['DATA_DE']) >= 0) && (datediff($value, $return[0]['DATA_ATE']) <= 0)) ? true : false;
						break;
						
						case 'DECIMAL':
							if(stristr(removeAcentos($return[0]['DESCCONFIG'], false), 'maximo') || stristr(removeAcentos($return[0]['DESCCONFIG'], false), 'maxima'))
							{
								$return = ($value >= $return[0]['VALORDEC']) ? true : false;
							}
							else
							{
								$return = ($value <= $return[0]['VALORDEC']) ? true : false;
							}
						break;
							
						case 'INTEIRO':
							if(stristr(removeAcentos($return[0]['DESCCONFIG'], false), 'maximo') || stristr(removeAcentos($return[0]['DESCCONFIG'], false), 'maxima'))
							{
								$return = ($value >= $return[0]['VALORINT']) ? true : false;
							}
							else
							{
								$return = ($value <= $return[0]['VALORINT']) ? true : false;
							}
						break;
							
						default: break;
					}
				}
				else
				{
					$return = false;
				}
			}
		}
		
		return $return;
	}
	
	function getConfigData($idConfig = null)
	{
		$return = array();
		if(!is_null($idConfig))
		{
			$sql = sprintf("SELECT c.IDCONFIGURACAO,
								   c.DESCCONFIG,
								   c.TIPOCONFIG,
								   pc.DATA_DE,
								   pc.DATA_ATE,
								   pc.VALORDEC,
								   pc.VALORINT
							  FROM configuracao c
                              LEFT JOIN programaconfig pc
								ON (c.IDCONFIGURACAO = pc.IDCONFIGURACAO AND pc.IDPROGRAMA = %s)
							 WHERE c.IDCONFIGURACAO = %s", $this->session->userdata('programa'), $idConfig);
			$dados  = $this->db->query($sql);
			$return = $dados->result_array();
		}
		return $return;
	}
	
	function getConfigValue($idConfig = null)
	{
		$return = '';
		
		if( ! is_null($idConfig))
		{
			$sql = "SELECT c.IDCONFIGURACAO,
						   c.DESCCONFIG,
						   c.TIPOCONFIG,
						   pc.DATA_DE,
						   pc.DATA_ATE,
						   pc.VALORDEC,
						   pc.VALORINT
				      FROM configuracao c
				      LEFT JOIN programaconfig pc ON (c.IDCONFIGURACAO = pc.IDCONFIGURACAO AND pc.IDPROGRAMA = " . $this->session->userdata('programa') . ")
					 WHERE c.IDCONFIGURACAO = $idConfig";
			$dados = $this->db->query($sql);
			$dados = $dados->result_array();
			
			// Switch do tipo de configuração
			switch(strtoupper($dados[0]['TIPOCONFIG']))
			{
				case 'PERIODO':
					empty($return);
					$return[0] = $dados[0]['DATA_DE'];
					$return[1] = $dados[0]['DATA_ATE'];
				break;
				
				case 'DECIMAL':
					$return = $dados[0]['VALORDEC'];
				break;
				
				case 'INTEIRO':
					$return = $dados[0]['VALORINT'];
				break;
				
				default: break;
			}
		}
		
		return $return;
	}
    
    function validaEditais($idConfig)
    {
        $sql = sprintf("SELECT 
                            p.*,pc.DATA_DE, pc.DATA_ATE 
                        FROM programa p
                        INNER JOIN programaconfig pc ON (p.IDPROGRAMA = pc.IDPROGRAMA)
                        WHERE pc.IDCONFIGURACAO = %s
                            AND NOT EXISTS (SELECT 1 FROM usuarioprograma up WHERE up.IDPROGRAMA = p.IDPROGRAMA AND up.IDUSUARIO = %s)",
                            $idConfig, $this->session->userdata('idUsuario'));
        $dados = $this->db->query($sql);
		
		return $dados->result_array();
    }
	
	/*
	* get_sis_config_value()
	* Retorna o valor de uma configuração de sistema. Case-insensitive.
	* @param string id_configuracao
	* @return mixed value
	*/
	function get_sis_config_value($id_config = '')
	{
		$sql = "SELECT value FROM sis_configuracao WHERE LOWER(key_configuracao) = '" . strtolower($id_config) . "'";
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		return (get_value($dados[0], 'VALUE') != '') ? get_value($dados[0], 'VALUE') : '';
	}
}

/* End of file Configuracao_model.php */
/* Location: ./system/application/models/Configuracao_model.php */