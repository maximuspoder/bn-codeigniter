<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe Pesquisa_Model
*
* Esta classe contém metodos para a abstracao da entidade model pesquisa.
* 
* @author		Ricardo Neves Becker
* @package		application
* @subpackage	models.pesquisa
* @since		2012-07-03
*
*/
class Pesquisa_Model extends CI_Model {
	// Definição de variaveis
	public $sql = '';
	
	/**
	* __construct()
	* @return object
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	/**
	* add_sql_pesquisa_pdv()
	* Retorna o sql de pesquisa do pdv.
	* @param array filtros
	* @return string sql
	*/
	function add_sql_pesquisa_pdv($filtros = array())
	{
		$sql = "SELECT P.IDUSUARIO      AS `#`,
					   P.RAZAOSOCIAL    AS `RAZÃO SOCIAL`,
					   P.NOMEFANTASIA   AS `NOME FANTASIA`,
					   U.CPF_CNPJ       AS `CPF / CNPJ`,
					   P.TELEFONEGERAL  AS TELEFONE,
					   C.NOMECIDADE      AS CIDADE,
					   C.IDUF 			 AS UF,
					   U.IDUSUARIOTIPO,
					   '<img src=\"" . URL_IMG . "icon_pdv.png\" title=\"PONTO DE VENDA\" />' AS TIPO_ENTIDADE,
					   CONCAT('<a href=\"javascript:void(0);\" onclick=\"ajax_modal(''Visualizar Dados'', ''" . URL_EXEC . "pdv/modal_form_view_pdv/', P.IDUSUARIO, ''', ''" . URL_IMG . "icon_view.png'', 600, 800);\"><img src=\"" . URL_IMG . "icon_view.png\" title=\"Visualizar\" /></a>') AS ACAO_IMG
				  FROM CADPDV     P
			INNER JOIN USUARIO      U ON (U.IDUSUARIO = P.IDUSUARIO)	  
			 LEFT JOIN LOGRADOURO   L ON (L.IDLOGRADOURO = P.IDLOGRADOURO)
			 LEFT JOIN CIDADE       C ON (C.IDCIDADE     = L.IDCIDADE)
   		         WHERE 1 = 1 ";
		// WHERE 'OR' PARA (RAZAOSOCIAL OU NOMEFANTASIA OU CPF_CNPJ) E UF E CIDADE
		$sql .= (get_value($filtros, 'campo_pesquisa') != '') ? " AND (P.RAZAOSOCIAL LIKE '%" . get_value($filtros, 'campo_pesquisa') . "%' OR P.NOMEFANTASIA LIKE '%" . get_value($filtros, 'campo_pesquisa') . "%' OR U.CPF_CNPJ LIKE '%" . get_value($filtros, 'campo_pesquisa') . "%') " : '';
		$sql .= (get_value($filtros, 'iduf') != '') ? " AND C.IDUF = '" . get_value($filtros, 'iduf') . "'" : '';
		$sql .= (get_value($filtros, 'idcidade') != '') ? " AND C.IDCIDADE = " . get_value($filtros, 'idcidade') : '';
		   
		// SOMENTE CADASTROS ATIVOS
	    $sql .= " AND U.ATIVO = 'S'";
		$this->sql .= ($this->sql != '') ? ' UNION ALL ' . $sql : $sql;
	}
	
	/**
	* add_sql_pesquisa_biblioteca()
	* Retorna o sql de pesquisa do biblioteca.
	* @param array filtros
	* @return string sql
	*/
	function add_sql_pesquisa_biblioteca($filtros = array())
	{
		$sql = "SELECT P.IDUSUARIO      AS `#`,
					   P.RAZAOSOCIAL    AS `RAZÃO SOCIAL`,
					   P.NOMEFANTASIA   AS `NOME FANTASIA`,
					   U.CPF_CNPJ       AS `CPF / CNPJ`,
					   P.TELEFONEGERAL  AS TELEFONE,
					   C.NOMECIDADE      AS CIDADE,
					   C.IDUF 			 AS UF,
					   U.IDUSUARIOTIPO,
					   '<img src=\"" . URL_IMG . "icon_biblioteca.png\" title=\"BIBLIOTECA\" />' AS TIPO_ENTIDADE,
					   CONCAT('<a href=\"javascript:void(0);\" onclick=\"ajax_modal(''Visualizar Dados'', ''" . URL_EXEC . "pesquisa/modal_form_view_biblioteca/', P.IDUSUARIO, ''', ''" . URL_IMG . "icon_view.png'', 600, 800);\"><img src=\"" . URL_IMG . "icon_view.png\" title=\"Visualizar\" /></a>') AS ACAO_IMG
				  FROM CADBIBLIOTECA     P
			INNER JOIN USUARIO      U ON (U.IDUSUARIO = P.IDUSUARIO)	  
			 LEFT JOIN LOGRADOURO   L ON (L.IDLOGRADOURO = P.IDLOGRADOURO)
			 LEFT JOIN CIDADE       C ON (C.IDCIDADE     = L.IDCIDADE)
   		         WHERE 1 = 1 ";
		// WHERE 'OR' PARA (RAZAOSOCIAL OU NOMEFANTASIA OU CPF_CNPJ) E UF E CIDADE
		$sql .= (get_value($filtros, 'campo_pesquisa') != '') ? " AND (P.RAZAOSOCIAL LIKE '%" . get_value($filtros, 'campo_pesquisa') . "%' OR P.NOMEFANTASIA LIKE '%" . get_value($filtros, 'campo_pesquisa') . "%' OR U.CPF_CNPJ LIKE '%" . get_value($filtros, 'campo_pesquisa') . "%') " : '';
		$sql .= (get_value($filtros, 'iduf') != '') ? " AND C.IDUF = '" . get_value($filtros, 'iduf') . "'" : '';
		$sql .= (get_value($filtros, 'idcidade') != '') ? " AND C.IDCIDADE = " . get_value($filtros, 'idcidade') : '';
		   
		// SOMENTE CADASTROS ATIVOS
	    $sql .= " AND U.ATIVO = 'S'";
		$this->sql .= ($this->sql != '') ? ' UNION ALL ' . $sql : $sql;
	}
	
	/**
	* add_sql_pesquisa_distribuidor()
	* Retorna o sql de pesquisa do distribuidor.
	* @param array filtros
	* @return string sql
	*/
	function add_sql_pesquisa_distribuidor($filtros = array())
	{
		$sql = "SELECT P.IDUSUARIO      AS `#`,
					   P.RAZAOSOCIAL    AS `RAZÃO SOCIAL`,
					   P.NOMEFANTASIA   AS `NOME FANTASIA`,
					   U.CPF_CNPJ       AS `CPF / CNPJ`,
					   P.TELEFONEGERAL  AS TELEFONE,
					   C.NOMECIDADE      AS CIDADE,
					   C.IDUF 			 AS UF,
					   U.IDUSUARIOTIPO,
					   '<img src=\"" . URL_IMG . "icon_distribuidor.png\" title=\"DISTRIBUIDOR\" /> ' AS TIPO_ENTIDADE,
					   CONCAT('<a href=\"javascript:void(0);\" onclick=\"ajax_modal(''Visualizar Dados'', ''" . URL_EXEC . "distribuidor/modal_form_view_distribuidor/', P.IDUSUARIO, ''', ''" . URL_IMG . "icon_view.png'', 600, 800);\"><img src=\"" . URL_IMG . "icon_view.png\" title=\"Visualizar\" /></a>') AS ACAO_IMG
				  FROM CADDISTRIBUIDOR  P
			INNER JOIN USUARIO      U ON (U.IDUSUARIO = P.IDUSUARIO)	  
			 LEFT JOIN LOGRADOURO   L ON (L.IDLOGRADOURO = P.IDLOGRADOURO)
			 LEFT JOIN CIDADE       C ON (C.IDCIDADE     = L.IDCIDADE)
   		         WHERE 1 = 1 ";
		// WHERE 'OR' PARA (RAZAOSOCIAL OU NOMEFANTASIA OU CPF_CNPJ) E UF E CIDADE
		$sql .= (get_value($filtros, 'campo_pesquisa') != '') ? " AND (P.RAZAOSOCIAL LIKE '%" . get_value($filtros, 'campo_pesquisa') . "%' OR P.NOMEFANTASIA LIKE '%" . get_value($filtros, 'campo_pesquisa') . "%' OR U.CPF_CNPJ LIKE '%" . get_value($filtros, 'campo_pesquisa') . "%') " : '';
		$sql .= (get_value($filtros, 'iduf') != '') ? " AND C.IDUF = '" . get_value($filtros, 'iduf') . "'" : '';
		$sql .= (get_value($filtros, 'idcidade') != '') ? " AND C.IDCIDADE = " . get_value($filtros, 'idcidade') : '';
		   
		// SOMENTE CADASTROS ATIVOS
	    $sql .= " AND U.ATIVO = 'S'";
		$this->sql .= ($this->sql != '') ? ' UNION ALL ' . $sql : $sql;
	}
	
	/**
	* add_sql_pesquisa_editora()
	* Retorna o sql de pesquisa do editor(a).
	* @param array filtros
	* @return string sql
	*/
	function add_sql_pesquisa_editora($filtros = array())
	{
		$sql = "SELECT P.IDUSUARIO      AS `#`,
					   P.RAZAOSOCIAL    AS `RAZÃO SOCIAL`,
					   P.NOMEFANTASIA   AS `NOME FANTASIA`,
					   U.CPF_CNPJ       AS `CPF / CNPJ`,
					   P.TELEFONEGERAL  AS TELEFONE,
					   C.NOMECIDADE      AS CIDADE,
					   C.IDUF 			 AS UF,
					   U.IDUSUARIOTIPO,
					   '<img src=\"" . URL_IMG . "icon_editora.png\" title=\"EDITORA\" /> ' AS TIPO_ENTIDADE,
					   CONCAT('<a href=\"javascript:void(0);\" onclick=\"ajax_modal(''Visualizar Dados'', ''" . URL_EXEC . "editora/modal_form_view_editora/', P.IDUSUARIO, ''', ''" . URL_IMG . "icon_view.png'', 600, 800);\"><img src=\"" . URL_IMG . "icon_view.png\" title=\"Visualizar\" /></a>') AS ACAO_IMG
				  FROM CADEDITORA  P
			INNER JOIN USUARIO      U ON (U.IDUSUARIO = P.IDUSUARIO)	  
			 LEFT JOIN LOGRADOURO   L ON (L.IDLOGRADOURO = P.IDLOGRADOURO)
			 LEFT JOIN CIDADE       C ON (C.IDCIDADE     = L.IDCIDADE)
   		         WHERE 1 = 1 ";
		// WHERE 'OR' PARA (RAZAOSOCIAL OU NOMEFANTASIA OU CPF_CNPJ) E UF E CIDADE
		$sql .= (get_value($filtros, 'campo_pesquisa') != '') ? " AND (P.RAZAOSOCIAL LIKE '%" . get_value($filtros, 'campo_pesquisa') . "%' OR P.NOMEFANTASIA LIKE '%" . get_value($filtros, 'campo_pesquisa') . "%' OR U.CPF_CNPJ LIKE '%" . get_value($filtros, 'campo_pesquisa') . "%') " : '';
		$sql .= (get_value($filtros, 'iduf') != '') ? " AND C.IDUF = '" . get_value($filtros, 'iduf') . "'" : '';
		$sql .= (get_value($filtros, 'idcidade') != '') ? " AND C.IDCIDADE = " . get_value($filtros, 'idcidade') : '';
		   
		// SOMENTE CADASTROS ATIVOS
	    $sql .= " AND U.ATIVO = 'S'";
		$this->sql .= ($this->sql != '') ? ' UNION ALL ' . $sql : $sql;
	}
	
	/**
	* add_sql_pesquisa_livros()
	* Retorna o sql de pesquisa de livros.
	* @param string termo_pesquisa
	* @return string sql
	*/
	function add_sql_pesquisa_livros($termo_pesquisa = '')
	{
		$sql = "SELECT  L.ISBN AS 'ISBN',
						L.TITULO,
						L.AUTOR,
						CE.RAZAOSOCIAL AS EDITORA,
						CONCAT('<a href=\"javascript:void(0);\" onclick=\"ajax_modal(''Visualizar Detalhes'', ''" . URL_EXEC . "editora/modal_form_view_editora/', CE.IDUSUARIO, ''', ''" . URL_IMG . "icon_detalhes.png'', 500, 900);\" title=\"Visualizar Detalhes\">', CE.RAZAOSOCIAL, '</a>') AS EDITORA_LINK,
						L.EDICAO AS `EDIÇÂO`,					
						L.ANO,
						L.IDLIVRO,
						CONCAT ('<img src=\"" . URL_IMG . "icon_img_info.png\" title=\"ID Livro ', L.IDLIVRO, ' \" />') AS INFO_ID_LIVRO,			
						CONCAT('<a href=\"javascript:void(0);\" onclick=\"ajax_modal(''Visualizar Dados'', ''" . URL_EXEC . "acesso_externo/modal_form_view_pesquisa_externa/', L.IDLIVRO, ''', ''" . URL_IMG . "icon_view.png'', 500, 650);\"><img src=\"" . URL_IMG . "icon_view.png\" title=\"Visualizar\" /></a>') AS ACAO_IMG
			   	  FROM LIVRO L
			 LEFT JOIN CADEDITORA CE ON (CE.IDUSUARIO = L.IDUSUARIO)
			INNER JOIN USUARIO    U ON (CE.IDUSUARIO = U.IDUSUARIO)
				 WHERE CE.RAZAOSOCIAL LIKE '%" . $termo_pesquisa . "%'
				    OR L.TITULO LIKE '%" . $termo_pesquisa . "%'
					OR L.ISBN LIKE '%" . $termo_pesquisa . "%'
					OR L.AUTOR LIKE '%" . $termo_pesquisa . "%'
					OR L.EDICAO LIKE '%" . $termo_pesquisa . "%'
					OR L.ANO LIKE '%" . $termo_pesquisa . "%'";
		$this->sql .= ($this->sql != '') ? ' UNION ALL ' . $sql : $sql;
	}
	
	/**
	* get_dados_acesso_externo_livros()
	* Joga na tela os dados dos livros.
	* @param 
	* @return string sql
	*/
	function get_dados_pesquisa_livros($idlivro = 0, $idprograma = 0)
	{
		$sql = "SELECT  L.IDLIVRO,
						CE.RAZAOSOCIAL  AS  EDITORA,
						CE.NOMEFANTASIA AS 'NOME FANTASIA',
						U.CPF_CNPJ,
						L.TITULO,
						L.AUTOR,
						L.EDICAO,
						L.ANO
						FROM livro L
						inner join cadeditora CE on (CE.IDUSUARIO = L.IDUSUARIO)
						inner join usuario u on (CE.IDUSUARIO = u.IDUSUARIO)
						WHERE L.IDLIVRO = $idlivro";
	
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados[0])) ? $dados[0] : array();
	}
	
	/**
	* execute_pesquisa()
	* Executa o sql montado anteriormente da pesquisa de estabelecimento.
	* @return string sql
	*/
	function execute_pesquisa()
	{
		// DEBUG:
		// echo $this->sql;
		$dados = $this->db->query($this->sql);
		return $dados->result_array();
	}
}
