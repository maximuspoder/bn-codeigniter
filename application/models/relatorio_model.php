<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe Relatorio_Model
*
* Abstracao da camada modelo para relatorios no sistema.
* 
* @author		Leandro Mangini Antunes
* @package		application
* @subpackage	application.relatorio_model
* @since		2012-03-27
*
*/
class Relatorio_Model extends CI_Model {
	
	/**
	* __construct()
	* Carrega objetos para conexao e manipulacao com db internos.
	* @return void
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	/**
	* get_lista_modulo()
	* Retorna os dados da listagem de entrada do modulo.
	* return array data
	*/
	function get_lista_modulo($filters = array())
	{
		$sql = "SELECT	R.ID_RELATORIO AS '#',
						R.ID_RELATORIO_CATEGORIA AS 'CATEGORIA',
						R.TITULO,
						R.DESCRICAO,
						R.SQL,
						R.DTT_INS AS `DATA DE CRIAÇÂO`,
						RC.NOME AS CATEGORIA,
						RC.ID_RELATORIO_CATEGORIA
						FROM rpt_relatorio R
						LEFT JOIN RPT_RELATORIO_CATEGORIA RC ON (R.ID_RELATORIO_CATEGORIA = RC.ID_RELATORIO_CATEGORIA)";
		// Carrega model auxiliar para processamento do where
		$this->load->model('global_model');
		$sql .= $this->global_model->get_where_filters($filters);
		
		// Concatena ORDER BY
		$sql .= ' ORDER BY R.ID_RELATORIO';
		
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados)) ? $dados : array();
	}
	
	/**
	* execute_relatorio_sql()
	* Retorna os dados do sql consultado.
	* return void
	*/
	function execute_relatorio_sql($idrelatorio = 0)
	{
		// Coleta os dados do relatorio (para pegar coluna SQL)
		$dados = $this->get_dados_relatorio($idrelatorio);
		
		// Executa o relatorio
		$dados = $this->db->query(get_value($dados, 'SQL'));
		return $dados->result_array();
	}
	
	/**
	* get_dados_relatorio()
	* Retorna os dados do sql consultado.
	* return array data
	*/
	function get_dados_relatorio($idrelatorio = 0)
	{
		$sql = "SELECT	R.ID_RELATORIO AS '#',
						R.ID_RELATORIO_CATEGORIA AS 'CATEGORIA',
						R.TITULO,
						R.DESCRICAO,
						R.SQL,
						R.DTT_INS
						FROM rpt_relatorio r
						LEFT JOIN RPT_RELATORIO_CATEGORIA RC ON (R.ID_RELATORIO_CATEGORIA = RC.ID_RELATORIO_CATEGORIA)
						where r.id_relatorio = $idrelatorio ORDER BY R.ID_RELATORIO ";
		
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados[0])) ? $dados[0] : array();	
	}
	
	/**
	* get_categorias_relatorio()
	* Retorna os dados para uso dos filtros de pesquisa.
	* return array data
	*/	
	function get_categorias_relatorio()
	{
		$return = array();
		$sql = "SELECT * FROM rpt_relatorio_categoria";
		
		$result = $this->db->query($sql);
		$return = $result->result_array();
		$return = (isset($return)) ? $return : array();
		return $return;
	}
}
