<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Uf_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	function getEstados()
	{
		$sql = "SELECT IDUF, IDUF AS IDUF2 FROM UF";
		$dados = $this->db->query($sql);
		return $dados->result_array();
	}
	
	function getEstadosComBibliotecasPai()
	{
		$sql = "SELECT IDUF, 
					   IDUF AS IDUF2 
			  	  FROM UF
				 WHERE IDUF IN (SELECT DISTINCT U.IDUF 
								  FROM UF U
							INNER JOIN CIDADE         C ON (C.IDUF = U.IDUF)
							INNER JOIN LOGRADOURO     L ON (L.IDCIDADE = C.IDCIDADE)
							INNER JOIN CADBIBLIOTECA CB ON (CB.IDLOGRADOURO = L.IDLOGRADOURO)
							WHERE CB.IDUSUARIO IN (SELECT IDUSUARIOPAI FROM CADBIBLIOTECA CB2 WHERE CB2.IDUSUARIOPAI = CB.IDUSUARIO)
								)";
		$dados = $this->db->query($sql);
		return $dados->result_array();
	}
	
	/*
	* get_ufs()
	* Retorna todas ufs.
	* return array cidades
	*/
	function get_ufs()
	{
		$sql = "SELECT DISTINCT IDUF, IDUF AS LABEL FROM UF ORDER BY IDUF";
		$dados  = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}
}

/* End of file uf_model.php */
/* Location: ./system/application/models/uf_model.php */