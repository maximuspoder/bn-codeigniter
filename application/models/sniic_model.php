<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sniic_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	function getSniicByCpfCnpj($cpf_cnpj)
	{
		$sql = sprintf("SELECT 
                            sb.ID_BIBLIOTECA       AS ID_BIBLIOTECA,
                            sb.CNPJ_CPF            AS CPF_CNPJ,
                            UPPER(TRIM(sb.NOME_BIBLIOTECA)) AS NOME,
                            sb.ENDER_TELEFONE      AS TELEFONE,
                            sb.EMAIL_BIBLIOTECA    AS EMAIL,
                            sb.SITE_BIBLIOTECA     AS SITE,
                            REPLACE(REPLACE(REPLACE(sb.ENDER_CEP, '.', ''), '-', ''), '/', '') AS CEP,
                            sb.NOME_DIRIGENTE      AS NOME_DIRIGENTE,
                            sb.EMAIL_DIRIGENTE     AS EMAIL_DIRIGENTE
                        FROM sniiconline.sniic_biblioteca sb
						WHERE TRIM(REPLACE(REPLACE(REPLACE(sb.CNPJ_CPF, '.', ''), '-', ''), '/', '')) = '%s'", $cpf_cnpj);
		
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
    
    function getSniicByIdSniic($idSniic)
	{
		$sql = sprintf("SELECT
                            sb.ID_BIBLIOTECA          AS ID_BIBLIOTECA,
                            sb.CNPJ_CPF               AS CPF_CNPJ,
                            UPPER(TRIM(sb.NOME_BIBLIOTECA)) AS NOME,
                            sb.ENDER_TELEFONE         AS TELEFONE,
                            sb.EMAIL_BIBLIOTECA       AS EMAIL,
                            sb.SITE_BIBLIOTECA        AS SITE,
                            REPLACE(REPLACE(REPLACE(sb.ENDER_CEP, '.', ''), '-', ''), '/', '') AS CEP,
                            sb.NOME_DIRIGENTE         AS NOME_DIRIGENTE,
                            sb.EMAIL_DIRIGENTE        AS EMAIL_DIRIGENTE,
                            sb.ENDER_MUNICIPIO        AS MUNICIPIO
                        FROM sniiconline.sniic_biblioteca sb
						WHERE ID_BIBLIOTECA = '%s'", $idSniic);
		
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function getSniicByChave($chave)
	{
		$sql = sprintf("SELECT
                            sb.ID_BIBLIOTECA AS ID_BIBLIOTECA,
                            sb.CNPJ_CPF      AS CPF_CNPJ
                        FROM sniiconline.sniic_biblioteca sb
						WHERE sb.CHAVE_VALIDACAO = '%s'", $chave);
		
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
    
    function getSniicCadastradoByCpfCnpj($cpf_cnpj)
	{
		$sql = sprintf("SELECT 
                            sb.CNPJ_CPF AS CPF_CNPJ,
                            UPPER(TRIM(sb.NOME_BIBLIOTECA)) AS NOME_BIBLIOTECA, cb.IDSNIIC
                        FROM sniiconline.sniic_biblioteca sb INNER JOIN binac.cadbiblioteca cb
                        ON sb.id_biblioteca = cb.idsniic	
                        WHERE TRIM(REPLACE(REPLACE(REPLACE(sb.CNPJ_CPF, '.', ''), '-', ''), '/', '')) = '%s'", $cpf_cnpj);
		
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
    function getCreditoByIdBiblioteca($idBiblioteca, $idPrograma = null)
	{
		if(is_null($idPrograma))
		{
			$sql = sprintf("SELECT *
                        FROM sniiconline.credito sc
						WHERE ID_BIBLIOTECA = '%s'", $idBiblioteca);
		}
		else
		{
			$sql = sprintf("SELECT *
                        FROM sniiconline.credito sc
						WHERE ID_BIBLIOTECA = '%s' AND IDPROGRAMA = '%s'", $idBiblioteca, $idPrograma);
		}

		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
    function getCreditoInfoByIdBiblioteca($idBiblioteca, $idPrograma = null)
	{
		if(is_null($idPrograma))
		{
			$sql = sprintf("SELECT *
                        FROM sniiconline.creditoinfo sc
						WHERE ID_BIBLIOTECA = '%s'", $idBiblioteca);
		}
		else
		{
			$sql = sprintf("SELECT *
                        FROM sniiconline.creditoinfo sc
						WHERE ID_BIBLIOTECA = '%s' AND IDPROGRAMA = '%s'", $idBiblioteca, $idPrograma);
		}
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
}

/* End of file Isbn_model.php */
/* Location: ./system/application/models/Isbn_model.php */