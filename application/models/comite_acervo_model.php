<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe Comiteacervo_Model
*
* Esta classe contém métodos para a abstração da entidade model Comiteacervo.
* 
* @author		Fernando Alves
* @package		application
* @subpackage	models.comiteacervo
* @since		2012-07-05
*
*/
class Comite_acervo_model extends CI_Model {
	/* filtros */
	protected $ATIVO		= '';
	protected $HABILITADO   = '';
	protected $RAZAOSOCIAL  = '';
	protected $NOMEFANTASIA = '';
	protected $CIDADE		= '';
	protected $UF			= '';
	protected $CPFCNPJ		= '';
	protected $TIPOCAD		= '';	
	protected $IDUSUARIO    = '';

	/**
	* __construct()
	* @return object
	*/	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	
	/**
	* get_lista_modulo()
	* Retorna os dados da listagem de entrada do modulo.
	* return array data
	*/
	function get_lista_modulo($filters = array())
	{
		$sql = "SELECT
					 CC.IDUSUARIO AS `#`, 
					 CC.RAZAOSOCIAL AS `NOME`,
 					 CB.RAZAOSOCIAL AS `BIBLIOTECA`,
					 CC.TELEFONERESP AS `TELEFONE`,
					 CC.EMAILRESP AS `EMAIL`,
					 U.LOGIN AS `LOGIN`,
					 CASE WHEN U.ATIVO = 'N' THEN '<img src=\"" . URL_IMG . "icon_bullet_red.png\" title=\"USUARIO DESATIVADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_bullet_green_2.png\" title=\"USUARIO ATIVO\" />' END AS `ATIVO_IMG`
                FROM cadbibliotecacomite CC
				INNER JOIN usuario U ON (U.IDUSUARIO = CC.IDUSUARIO)
				LEFT JOIN cadbiblioteca CB ON (CC.IDBIBLIOTECA = CB.IDUSUARIO)";
		
		// Carrega model auxiliar para processamento do where
		$this->load->model('global_model');
		$sql .= $this->global_model->get_where_filters($filters);
		
		// Concatena ORDER BY
		$sql .= ' ORDER BY CC.IDUSUARIO';
		
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados)) ? $dados : array();
	}
	
	/*
	* get_dados_comite_acervo()
	* Retorna os dados do comite de acervo
	* @param integer idusuario
	* return array biblioteca
	*/
	function get_dados_comite_acervo($idusuario = 0)
	{
	 $sql = "SELECT
	          U.*,
		      CC.IDUSUARIO AS `#`, 
 		      CB.RAZAOSOCIAL AS `BIBLIOTECA`,
              CC.RAZAOSOCIAL AS `NOME`,
			  CC.NOMERESP AS `NOME RESPONSAVEL`,
			  CC.NOMEMAE AS `NOME DA MAE`,
              CC.DATANASC AS `DATA DE NASCIMENTO`,  
		      CC.TELEFONERESP AS `TELEFONE`,
		      CC.EMAILRESP AS `EMAIL`,
			  L.CEP AS `CEP_LOGRADOURO`,
					L.NOMELOGRADOURO,
					CC.IDLOGRADOURO,
					CC.END_NUMERO,
					CC.END_COMPLEMENTO,
					LT.*,
					B.*,
					C.*,
					UF.*,
		     CASE WHEN U.ATIVO = 'N' THEN '<img src=\"" . URL_IMG . "icon_bullet_red.png\" title=\"USUARIO DESATIVADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_bullet_green_2.png\" title=\"USUARIO ATIVO\" />' END AS `ATIVO_IMG` 
        FROM cadbibliotecacomite CC
        INNER JOIN usuario U ON (U.IDUSUARIO = CC.IDUSUARIO)
        LEFT JOIN cadbiblioteca CB ON (CC.IDBIBLIOTECA = CB.IDUSUARIO)
		LEFT JOIN LOGRADOURO       L ON (L.IDLOGRADOURO = CC.IDLOGRADOURO) 
				   LEFT JOIN LOGRADOUROTIPO  LT ON (LT.IDLOGRADOUROTIPO = L.IDLOGRADOUROTIPO)
				   LEFT JOIN BAIRRO           B ON (B.IDBAIRRO = L.IDBAIRRO)
				   LEFT JOIN CIDADE           C ON (C.IDCIDADE = L.IDCIDADE) 
				   LEFT JOIN UF               UF ON (UF.IDUF = C.IDUF)		
        WHERE U.IDUSUARIO = $idusuario";
   
   
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();		
		return (isset($dados[0])) ? $dados[0] : array();		
	}
	

}