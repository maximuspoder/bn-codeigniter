<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pedido_model extends CI_Model {
	/* filtros */
	protected $STATUS    = '';
	protected $PDV       = '';
	protected $PROGRAMA  = '';
	protected $IDUSUARIO = 0;
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	function setFiltro($coluna = null, $valor = null)
	{
		if(!is_null($coluna) && !is_null($valor))
		{
			$this->{strtoupper($coluna)} = addslashes($valor);
		}
	}
	
	function getArrayFiltros($arrDados = null)
	{
		$dados = array();
		if(!is_null($arrDados) && is_array($arrDados))
		{
			$dados['filtro_pdv'] = $arrDados['filtro_pdv'];
			$dados['filtro_status'] = $arrDados['filtro_status'];
			$dados['filtro_programa'] = $arrDados['filtro_programa'];
		} else {
			$dados['filtro_pdv'] = '';
			$dados['filtro_status'] = '';
			$dados['filtro_programa'] = '';
		}
		return $dados;
	}
	
	function listaPedidosBiblioteca($arrParam, $onlyCount = FALSE)
	{
		if($onlyCount)
		{
			$sql = "SELECT count(*) as CONT
                      FROM pedido p
                INNER JOIN pedidostatus  ps ON (p.IDPEDIDOSTATUS = ps.IDPEDIDOSTATUS)
                INNER JOIN cadbiblioteca cb ON (p.IDBIBLIOTECA = cb.IDUSUARIO)
				INNER JOIN programa      pr ON (pr.IDPROGRAMA = p.IDPROGRAMA)
				 LEFT JOIN cadpdv        cp ON (p.IDPDV = cp.IDUSUARIO)";
		}
		else
		{
			$sql = "SELECT p.*,
						   DATE_FORMAT(p.DATA_ULT_ATU, '%d/%m/%Y %H:%i') AS DATA_ATU,
						   ps.*,
						   cb.*,
						   cp.RAZAOSOCIAL AS PDV,
						   pr.*
					  FROM pedido p
    		    INNER JOIN pedidostatus  ps ON (p.IDPEDIDOSTATUS = ps.IDPEDIDOSTATUS)
			    INNER JOIN cadbiblioteca cb ON (p.IDBIBLIOTECA = cb.IDUSUARIO)
			    INNER JOIN programa      pr ON (pr.IDPROGRAMA = p.IDPROGRAMA)
			     LEFT JOIN cadpdv        cp ON (p.IDPDV = cp.IDUSUARIO)";
		}
		
		$limit = ($arrParam['limit_de'] == 0 && $arrParam['exibir_pp'] == 0) ? '' : 'LIMIT ' . $arrParam['limit_de'] .' , ' . $arrParam['exibir_pp'];
		
		// Filtragem - Serve tanto para o count quanto para dados de listagem
		// Adiciona um Where 1=1 para que os filtros abaixo possam ser utilizados (SOMENTE PARA COUNT)
		$sql .= ' WHERE p.IDBIBLIOTECA = ' . $this->IDUSUARIO;

		// Filtro PONTO DE VENDA
		if($this->PDV != '')
		{
			$sql .= " AND (cp.RAZAOSOCIAL LIKE '%" . $this->PDV . "%' OR cp.NOMEFANTASIA LIKE '%" . $this->PDV . "%') ";
		}
		
		// Filtro para STATUS
		if($this->STATUS != '')
		{
			$sql .= " AND p.IDPEDIDOSTATUS = " . $this->STATUS;
		}
		
		// Filtro para programa
		if($this->PROGRAMA != '')
		{
			$sql .= " AND p.IDPROGRAMA = " . $this->PROGRAMA;
		}
		
		// Finaliza SQL, somente para casos onde nao seja count
		if (!$onlyCount)
		{
			// Prossegue o sql
			$sql .= " ORDER BY p.IDPEDIDOSTATUS " . $limit;
		}
		
		$dados = $this->db->query($sql);
		return $dados->result_array();
	}
	
	function lista($arrParam, $onlyCount = FALSE)
	{
		if ($onlyCount)
		{
			$sql = sprintf("SELECT count(*) as CONT
                            FROM pedido p
                            INNER JOIN pedidostatus ps ON (p.IDPEDIDOSTATUS = ps.IDPEDIDOSTATUS)
                            INNER JOIN cadbiblioteca cb ON (p.IDBIBLIOTECA = cb.IDUSUARIO)
                            LEFT JOIN cadpdv cp ON (p.IDPDV = cp.IDUSUARIO)
                            WHERE %s ",  $arrParam['where']);
		}
		else
		{
            $limit = ($arrParam['limit_de'] == 0 && $arrParam['exibir_pp'] == 0) ? '' : 'LIMIT ' . $arrParam['limit_de'] .' , ' . $arrParam['exibir_pp'];

			$sql = sprintf("SELECT 	p.*,
                                    DATE_FORMAT(p.DATA_ULT_ATU, '%%d/%%m/%%Y %%H:%%i') AS DATA_ATU,
                                    ps.*,
                                    cb.*,
                                    cp.NOMEFANTASIA AS NOMEPDV,
                                    cp.RAZAOSOCIAL AS RAZAOPDV,
									c.*,
									L.*,
									LT.NOMELOGRADOUROTIPO,
                                    u.CPF_CNPJ,
									u.*
                             FROM 	pedido p
                             INNER JOIN pedidostatus  ps ON (p.IDPEDIDOSTATUS = ps.IDPEDIDOSTATUS)
                             INNER JOIN cadbiblioteca cb ON (p.IDBIBLIOTECA = cb.IDUSUARIO)
                             LEFT JOIN cadpdv cp ON (p.IDPDV = cp.IDUSUARIO)
                             LEFT JOIN usuario u ON (u.IDUSUARIO = cb.IDUSUARIO)
							 LEFT JOIN logradouro L ON (L.IDLOGRADOURO = CP.IDLOGRADOURO)
							 LEFT JOIN logradourotipo LT ON (LT.IDLOGRADOUROTIPO = L.IDLOGRADOUROTIPO)
							 LEFT JOIN CIDADE C ON (L.IDCIDADE = C.IDCIDADE)
                             WHERE %s
                             ORDER BY p.IDPEDIDOSTATUS, cb.RAZAOSOCIAL , cp.RAZAOSOCIAL
							 %s", $arrParam['where'], $limit);
		}
		
		$dados = $this->db->query($sql);
		return $dados->result_array();
	}
	
    function listaLivrosPedidoById($idPedido, $onlyCount = FALSE)
	{
		if ($onlyCount)
		{
			$sql = sprintf("SELECT sum(pl.QTD_ENTREGUE) as QTDTOTAL
                            FROM pedidolivro pl
                            WHERE pl.IDPEDIDO = %s", $idPedido);
		}
		else
		{
			$sql = sprintf("SELECT 	pl.*,
                                    ed.*,
                                    l.*                                    
                            FROM 	pedidolivro pl
                            INNER JOIN livro l ON (pl.IDLIVRO = l.IDLIVRO) 
                            INNER JOIN cadeditora ed ON (l.IDUSUARIO = ed.IDUSUARIO) 
                            WHERE pl.IDPEDIDO = %s", $idPedido);
		}

		$dados = $this->db->query($sql);
		return $dados->result_array();
	}
	
	/**
	* getItens()
	* Retorna os itens de um pedido encaminhado.
	* @param integer pedido
	* @return array itens
	*/
	function getItens($pedido = null)
	{
		$return = array();
		
		if(!is_null($pedido))
		{
			$sql = "SELECT livro.IDLIVRO,
						   pedido.IDPEDIDO,
			               livro.TITULO,
						   livro.AUTOR,
						   cadeditora.RAZAOSOCIAL AS EDITORA,
						   livro.EDICAO,
						   livro.ANO,
						   pedidolivro.QTD_ENTREGUE AS QTD
					  FROM pedidolivro
				INNER JOIN livro      ON (livro.IDLIVRO = pedidolivro.IDLIVRO)
				INNER JOIN cadeditora ON (cadeditora.IDUSUARIO = livro.IDUSUARIO)
				INNER JOIN pedido     ON (pedidolivro.IDPEDIDO = pedido.IDPEDIDO)
				     WHERE pedido.IDPEDIDO = $pedido
				  ORDER BY livro.TITULO";
			
			$result = $this->db->query($sql);
			
			$return = $result->result_array();
		}
		
		return $return;
	}
	
	/**
	* getCountItens()
	* Retorna a quantidade de itens de um pedido encaminhado.
	* @param integer pedido
	* @return array itens
	*/
	function getCountItens($pedido = NULL)
	{
		$return = 0;
		
		if( ! is_null($pedido))
		{
			$sql = "SELECT COUNT(*) AS CONT
					  FROM pedidolivro
				     WHERE IDPEDIDO = $pedido";
			
			$dados = $this->db->query($sql);
			
			if ($dados->num_rows() > 0)
			{
				$row = $dados->row();
				$return = $row->CONT;
			}
		}
		
		return $return;
	}
	
	/**
	* getCountExemplares()
	* Retorna a quantidade de exemplares do pedido
	*/
	function getCountExemplares($idPedido = 0)
	{
		$sql = sprintf("SELECT IFNULL(SUM(QTD_ENTREGUE), 0) AS CONT FROM pedidolivro WHERE IDPEDIDO = %s", $idPedido);
		
		$dados = $this->db->query($sql);
	
		if ($dados->num_rows() > 0)
		{
			$row = $dados->row();
			return $row->CONT;
		}
		else
		{
			return 0;
		}
	}
	
	/**
	* getIdPedido()
	* Retorna o id do pedido de um idusuario encaminhado.
	* @param integer idusuario
	* @return integer idpedido
	*/
	function getIdPedido($idusuario = 0)
	{
		$sql = sprintf("SELECT IDPEDIDO FROM pedido WHERE IDPROGRAMA = %s AND IDBIBLIOTECA = %s", $this->session->userdata('programa'), $idusuario);
		
		$dados = $this->db->query($sql);
		
		if ($dados->num_rows() > 0)
		{
			$row = $dados->row();
			return $row->IDPEDIDO;
		}
		else
		{
			return '';
		}
	}
	
	/**
	* getValorPedido()
	* Retorna o id do pedido de um idusuario encaminhado.
	* @param integer idusuario
	* @return integer idpedido
	*/
	function getValorPedido($idusuario = 0)
	{
		$sql = sprintf("SELECT VALORTOTAL FROM pedido WHERE IDPROGRAMA = %s AND IDBIBLIOTECA = %s", IDPROGRAMA, $idusuario);
		
		$dados = $this->db->query($sql);
		
		if ($dados->num_rows() > 0)
		{
			$row = $dados->row();
			return $row->VALORTOTAL;
		}
		else
		{
			return '';
		}
	}
	
	/**
     * getIdPedidoPrograma()
     * Retorna o id do pedido de um idusuario e um idprograma encaminhados.
     * @param integer idusuario, idprograma
     * @return integer idpedido
     */
    function getIdPedidoPrograma($idusuario = 0, $idprograma = 0) {
        $sql = sprintf("SELECT IDPEDIDO FROM pedido WHERE IDPROGRAMA = %d AND IDBIBLIOTECA = %d", $idprograma, $idusuario);

        $dados = $this->db->query($sql);

        if ($dados->num_rows() > 0) {
            $row = $dados->row();
            return $row->IDPEDIDO;
        } else {
            return '';
        }
    }
	
	/**
     * getValorPedidoPrograma()
     * Retorna o id do pedido de um idusuario encaminhado.
     * @param integer idusuario, idprograma
     * @return integer idpedido
     */
    function getValorPedidoPrograma($idusuario = 0, $idprograma = 0) {
        $sql = sprintf("SELECT VALORTOTAL FROM pedido WHERE IDPROGRAMA = %s AND IDBIBLIOTECA = %s", $idprograma, $idusuario);

        $dados = $this->db->query($sql);

        if ($dados->num_rows() > 0) {
            $row = $dados->row();
            return $row->VALORTOTAL;
        } else {
            return '';
        }
    }
	
	/**
     * pedidoProgramaExists()
     * Retorna um boolean dizendo se pedido existe ou não para o usuário e o programa encaminhados.
     * @param integer idusuario, idprograma
     * @return boolean
     */
    function pedidoProgramaExists($idusuario = NULL, $idprograma = null) 
	{
        $return = FALSE;

        if (!is_null($idusuario) && !is_null($idprograma)) {
            $sql = sprintf("SELECT COUNT(*) AS CONT FROM pedido WHERE IDBIBLIOTECA = %d AND IDPROGRAMA = %d", $idusuario, $idprograma);
            $result = $this->db->query($sql);
            $return = $result->result_array();
            $return = ($return[0]['CONT'] > 0) ? TRUE : FALSE;
        }

        return $return;
    }
	
	/**
	* pedidoExists()
	* Retorna um boolean dizendo se pedido existe ou não para o usuário encaminhado.
	* @param integer idusuario
	* @return boolean
	*/
	function pedidoExists($idusuario = NULL)
	{
		$return = FALSE;
		
		if( ! is_null($idusuario))
		{
			$sql = "SELECT COUNT(*) AS CONT FROM pedido WHERE IDBIBLIOTECA = $idusuario AND IDPROGRAMA = " . $this->session->userdata('programa');
			$result = $this->db->query($sql);
			$return = $result->result_array();
			$return = ($return[0]['CONT'] > 0) ? TRUE : FALSE;
		}
		
		return $return;
	}
	
	/**
	* itemExists()
	* Verifica se o item já existe em um pedido.
	* @param integer idItem
	* @return boolean
	*/
	function itemExists($idItem = NULL, $idpedido = NULL)
	{
		$return = FALSE;
		
		if( ! is_null($idItem) && ! is_null($idpedido) && $idItem != '' && $idpedido != '')
		{
			$sql = "SELECT COUNT(*) AS CONT FROM pedidolivro WHERE IDLIVRO = $idItem AND IDPEDIDO = $idpedido";
			$result = $this->db->query($sql);
			$return = $result->result_array();
			$return = ($return[0]['CONT'] > 0) ? TRUE : FALSE;
		}
		
		return $return;
	}
	
	/**
	* addItem()
	* Adiciona um item ao pedido encmainhado.
	* @param integer idItem
	* @param integer qtd
	* @param integer idPedido
	* @return void
	*/
	function addItem($idItem = null, $qtd = null, $idPedido = null)
	{
		if( ! is_null($idItem) && ! is_null($idPedido))
		{
			// Coleta os dados do livro encaminhado
			$sql = "SELECT programalivro.PRECO FROM programalivro WHERE IDPROGRAMA = " . $this->session->userdata('programa') . " AND IDLIVRO = $idItem";
			$result = $this->db->query($sql);
			$result = $result->result_array();
			
			// Passa valor de preco para variavel utilizavel
			$preco = $result[0]['PRECO'];
			
			$sql = "INSERT INTO pedidolivro (IDPEDIDO, IDLIVRO, PRECO_UNIT, QTD, QTD_ENTREGUE) VALUES ($idPedido, $idItem, $preco, $qtd, $qtd);";
			$this->db->query($sql);
		}
	}
	
	/**
	* updateItem()
	* Atualiza um item no pedido encmainhado.
	* @param integer idItem
	* @param integer qtd
	* @param integer idPedido
	* @return void
	*/
	function updateItem($idItem = null, $qtd = null, $idPedido = null)
	{
		if( ! is_null($idItem) && ! is_null($idPedido))
		{
			$sql = "UPDATE pedidolivro SET QTD = $qtd , QTD_ENTREGUE = $qtd WHERE IDLIVRO = $idItem AND IDPEDIDO = $idPedido;";
			$this->db->query($sql);
		}
	}
	
	/**
	* removeItem()
	* Remove um item no pedido encmainhado.
	* @param integer idItem
	* @param integer idPedido
	* @return void
	*/
	function removeItem($idItem = null, $idPedido = null)
	{
		if( ! is_null($idItem) && ! is_null($idPedido))
		{
			$sql = "DELETE FROM pedidolivro WHERE IDLIVRO = $idItem AND IDPEDIDO = $idPedido;";
			$this->db->query($sql);
		}
	}
	
	/**
	* getDadosItem()
	* Retorna os dados de um item de um pedido.
	* @param integer idItem
	* @param integer idPedido
	* @return array Item
	*/
	function getDadosItem($idItem = null, $idPedido = null)
	{
		$return = array();
		
		if( ! is_null($idItem) && ! is_null($idPedido))
		{
			// Coleta os dados do livro encaminhado
			$sql = "SELECT * FROM pedidolivro WHERE IDLIVRO = $idItem AND IDPEDIDO = $idPedido";
			$result = $this->db->query($sql);
			$return = $result->result_array();
			$return = (isset($return[0])) ? $return[0] : array();
		}
		
		return $return;
	}
	
	/**
	* getStatusPedido()
	* Retorna o status atual do pedido encaminhado.
	* @param integer idPedido
	* @return integer status
	*/
	function getStatusPedido($idPedido = 0)
	{
		$sql = "SELECT IDPEDIDOSTATUS FROM pedido WHERE IDPEDIDO = $idPedido AND IDPROGRAMA = " . $this->session->userdata('programa');
		
		$dados = $this->db->query($sql);
		
		if ($dados->num_rows() > 0)
		{
			$row = $dados->row();
			return $row->IDPEDIDOSTATUS;
		}
		else
		{
			return 0;
		}
	}

	/**
	* getBibliotecaPedido()
	* Retorna a biblioteca vinculada ao pedido.
	* @param integer idPedido
	* @return integer status
	*/
	function getBibliotecaPedido($idPedido = 0)
	{
		$sql = "SELECT IDBIBLIOTECA FROM pedido WHERE IDPEDIDO = $idPedido AND IDPROGRAMA = " . $this->session->userdata('programa');
		
		$dados = $this->db->query($sql);
		
		if ($dados->num_rows() > 0)
		{
			$row = $dados->row();
			return $row->IDBIBLIOTECA;
		}
		else
		{
			return 0;
		}
	}
	
	/**
	* validaItemPed()
	* Valida de livro existe no pedido
	*/
	function validaItemPed($idPrograma, $idUsuario, $idLivro)
	{
		$sql = sprintf("SELECT 1 AS CONT 
						FROM pedidolivro pl
						INNER JOIN pedido p ON (pl.IDPEDIDO = p.IDPEDIDO)
						WHERE p.IDPROGRAMA = %s
						  AND p.IDBIBLIOTECA = %s
						  AND pl.IDLIVRO = %s", $idPrograma, $idUsuario, $idLivro);
		
		$dados = $this->db->query($sql);
		
		if ($dados->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	/**
	* isStatusConfeccao()
	* Retorna um boolean se o status atual é confeccao ou não.
	* @param integer idPedido
	* @return boolean status
	*/
	function isStatusConfeccao($idPedido = NULL)
	{
		$return = FALSE;
		
		if( ! is_null($idPedido))
		{
			// Coleta O STATUS DO pedido encaminhado
			$status = $this->getStatusPedido($idPedido);
			$return = ($status != 1) ? FALSE : TRUE;
		}
		
		return $return;
	}
	
	/**
	* getDadosPedido()
	* retorna dados do cabeçalho do pedido
	*/
	function getDadosPedido($idPrograma, $idUsuario, $idPedido)
	{
		$wUser = '';
		$wPed  = '';
		
		if ($idUsuario != 0)
		{
			$wUser = 'AND p.IDBIBLIOTECA = ' . $idUsuario;
		}
		
		if ($idPedido != 0)
		{
			$wPed = 'AND p.IDPEDIDO = ' . $idPedido;
		}
		
		if ($wUser != '' || $wPed != '')
		{
			$sql = sprintf("SELECT
								p.IDPEDIDO, 
								p.IDPROGRAMA, 
								pr.DESCPROGRAMA,
								p.IDBIBLIOTECA, 
								cb.RAZAOSOCIAL AS RAZAOSOCIAL_B,
								p.IDPDV, 
								cp.RAZAOSOCIAL AS RAZAOSOCIAL_P,
								DATE_FORMAT(p.DATA_ULT_ATU, '%%d/%%m/%%Y %%H:%%i') AS DATA_ULT_ATU, 
								p.VALORTOTAL, 
								p.IDPEDIDOSTATUS,
								ps.DESCSTATUS
							FROM pedido p
							INNER JOIN programa      pr ON (p.IDPROGRAMA = pr.IDPROGRAMA)
							INNER JOIN pedidostatus  ps ON (p.IDPEDIDOSTATUS = ps.IDPEDIDOSTATUS)
							INNER JOIN cadbiblioteca cb ON (p.IDBIBLIOTECA = cb.IDUSUARIO)
							LEFT JOIN cadpdv        cp ON (p.IDPDV = cp.IDUSUARIO)
							WHERE p.IDPROGRAMA = %s
							  %s
							  %s", $idPrograma, $wUser, $wPed);
			
			$result = $this->db->query($sql);
			
			return $result->result_array();
		}
		else
		{
			return Array();
		}
	}
	
	/**
	* getItensPedido()
	* retorna itens do pedido
	*/
	function getItensPedido($idPrograma, $idUsuario, $idPedido)
	{
		$wUser = '';
		
		if ($idUsuario != 0)
		{
			$wUser = 'AND p.IDBIBLIOTECA = ' . $idUsuario;
		}
		
		$sql = sprintf("SELECT
							l.ISBN,
							e.RAZAOSOCIAL AS EDITORA,
							l.IDLIVRO,
							l.TITULO,
							l.AUTOR,
							l.EDICAO,
							l.ANO,
							pl.QTD,
							pl.QTD_ENTREGUE,
							pl.PRECO_UNIT,
							(pl.QTD_ENTREGUE * pl.PRECO_UNIT) AS SUBTOTAL
						FROM pedidolivro pl
						INNER JOIN pedido     p ON (pl.IDPEDIDO = p.IDPEDIDO)
						INNER JOIN livro      l ON (pl.IDLIVRO = l.IDLIVRO)
						INNER JOIN cadeditora e ON (l.IDUSUARIO = e.IDUSUARIO)
						WHERE p.IDPROGRAMA = %s
						  AND p.IDPEDIDO = %s
						  %s", $idPrograma, $idPedido, $wUser);
		
		$result = $this->db->query($sql);
		
		return $result->result_array();
	}
	
	/**
	* getHistoricoPedido()
	* retorna historico do pedido
	*/
	function getHistoricoPedido($idPedido)
	{
		if ($idPedido != 0)
		{
			$sql = sprintf("SELECT
								s.DESCSTATUS AS DESCSTATUSLOG,
								DATE_FORMAT(p.DATAHORA, '%%d/%%m/%%Y %%H:%%i') AS DATAHORALOG
							FROM pedidolog p
							INNER JOIN pedidostatus s ON (p.IDPEDIDOSTATUS = s.IDPEDIDOSTATUS)
							WHERE p.IDPEDIDO = %s
							ORDER BY p.DATAHORA", $idPedido);
			
			$result = $this->db->query($sql);
			
			return $result->result_array();
		}
		else
		{
			return Array();
		}
	}
	
	/**
	* saveDemandaReprimida()
	* Adiciona uma linha de demanda reprimida para linha e livro encaminhados
	* @param integer idPedido
	* @param integer idLivro
	* @param integer qdteDisponivel
	* @param integer qtdePrograma
	* @param integer qtdePedida
	* @return void
	*/
	function saveDemandaReprimida($idPedido = null, $idLivro = null, $qtdeDisponivel = 0, $qtdePrograma = 0, $qtdePedida = 0)
	{
		if(!is_null($idPedido) && !is_null($idLivro))
		{
			$sql = "INSERT INTO pedidoreprimido (IDPROGRAMA, IDPEDIDO, IDLIVRO, QTD_DISP_PROGRAMA, QTD_MAX_PERMITIDA, QTD_PEDIDA) VALUES (" . $this->session->userdata('programa') . ", $idPedido, $idLivro, $qtdeDisponivel, $qtdePrograma, $qtdePedida);";
			$this->db->query($sql);
		}
	}
	
	/**
	* getAllStatus()
	* Retorna todos os status de pedidos.
	* @return array status
	*/
	function getAllStatus()
	{
		$return = array();
		// Coleta os dados de status
		$sql = "SELECT IDPEDIDOSTATUS, DESCSTATUS FROM pedidostatus";
		$result = $this->db->query($sql);
		$return = $result->result_array();
		$return = (isset($return)) ? $return : array();
		return $return;
	}
	
	/**
	* get_pedidos()
	* Retorna todos os pedidos de um usuario para um determinado programa.
	* @return array pedidos
	*/
	function get_pedidos($idusuario = 0, $idprograma = 0)
	{
		$sql = "SELECT * FROM PEDIDO WHERE IDBIBLIOTECA = $idusuario AND IDPROGRAMA = $idprograma";
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return ((isset($return)) ? $return : array());
	}
	
	/**
	* get_dados_pedido()
	* Retorna dados do cabeçalho do pedido para um determinado programa.
	* @param integer idpedido
	* @return array pedido
	*/
	function get_dados_pedido($idpedido = 0)
	{
		$sql = "SELECT  p.IDPEDIDO, 
						p.IDPROGRAMA,
						IFNULL(P.MAX_QTDE_ENTREGAS, 0)     AS MAX_QTDE_ENTREGAS,
						IFNULL(P.PERCENTUAL_ATENDIMENTO,0) AS PERCENTUAL_ATENDIMENTO,
						P.TIPO_ATENDIMENTO,
						pr.DESCPROGRAMA,
						p.IDBIBLIOTECA, 
						cb.RAZAOSOCIAL AS BIBLIOTECA,
						p.IDPDV, 
						cp.RAZAOSOCIAL AS PDV,
						DATE_FORMAT(p.DATA_ULT_ATU, '%d/%m/%Y %H:%i') AS DATA_ULT_ATU,
						p.VALORTOTAL, 
						p.IDPEDIDOSTATUS,
						ps.DESCSTATUS,
						p.LIBERAR_INS_DOCUMENTO
				   FROM pedido p
			 INNER JOIN programa      pr ON (p.IDPROGRAMA = pr.IDPROGRAMA)
			 INNER JOIN pedidostatus  ps ON (p.IDPEDIDOSTATUS = ps.IDPEDIDOSTATUS)
			 INNER JOIN cadbiblioteca cb ON (p.IDBIBLIOTECA = cb.IDUSUARIO)
			  LEFT JOIN cadpdv        cp ON (p.IDPDV = cp.IDUSUARIO)
			      WHERE p.IDPEDIDO = $idpedido";
		// echo($sql);
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return(isset($return[0])) ? $return[0] : array();
	}
	
	/**
	* get_itens_pedido()
	* Retorna todos os itens do pedido.
	* @param integer idpedido
	* @param integer idprograma
	* @return array pedido
	*/
	function get_itens_pedido($idpedido = 0, $title = '', $idprograma = 0)
	{
		// $idprograma = ($idprograma != 0) ? $idprograma : $this->session->userdata('programa');
		$sql = "SELECT  l.IDLIVRO AS `#`,
						e.RAZAOSOCIAL AS EDITORA,
						CONCAT('<a href=\"javascript:void(0);\" onclick=\"ajax_modal(''Visualizar Detalhes'', ''" . URL_EXEC . "editora/modal_form_view_editora/', E.IDUSUARIO, ''', ''" . URL_IMG . "icon_detalhes.png'', 600, 750);\" title=\"Visualizar Detalhes\">', E.RAZAOSOCIAL, '</a>') AS EDITORA_LINK,
		                l.ISBN,
						l.IDLIVRO,
						l.TITULO,
						l.AUTOR,
						l.EDICAO,
						CONCAT('<div class=\"nowrap\">', L.EDICAO, ' - ', L.ANO, '</div>') AS `EDIÇÃO`,
						l.ANO,
						pl.QTD,
						pl.QTD_ENTREGUE,
						pl.PRECO_UNIT,
						pl.PRECO_UNIT AS `VLR. UNIT.`,
						(pl.QTD_ENTREGUE * pl.PRECO_UNIT) AS SUBTOTAL,
						CONCAT('<img src=\"" . URL_IMG . "icon_user_black.png\" title=\"AUTOR: ', l.AUTOR, '\" />') AS AUTOR_IMG
				   FROM pedidolivro pl
			 INNER JOIN pedido     p ON (pl.IDPEDIDO = p.IDPEDIDO)
			 INNER JOIN livro      l ON (pl.IDLIVRO = l.IDLIVRO)
			 INNER JOIN cadeditora e ON (l.IDUSUARIO = e.IDUSUARIO)
			 	  WHERE p.IDPEDIDO = $idpedido ";
		$sql .= ($title != 'true') ? " AND L.TITULO LIKE '%$title%' " : '';
		$sql .= "ORDER BY l.titulo";
		// echo($sql);
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return(isset($return)) ? $return : array();
	}
	
	/**
	* sum_vlr_itens()
	* Retorna a soma do valor dos itens de um pedido.
	* @param integer idpedido
	* @return double vlr_soma
	*/
	function sum_vlr_itens($idpedido = 0)
	{
		$sql = "SELECT  IFNULL(SUM(pl.QTD_ENTREGUE * pl.PRECO_UNIT), 0) AS SUBTOTAL
				   FROM pedidolivro pl
			 	  WHERE pl.IDPEDIDO = $idpedido ";
		// echo($sql);
		$result = $this->db->query($sql);
		
		// Método diferente para a coleta de valores do resultset (exemplo)
		if($result->num_rows() > 0)
		{
			$row = $result->row();
			return $row->SUBTOTAL;
		} else {
			return 0;
		}
	}
	
	/**
	* sum_vlr_itens_conferidos()
	* Retorna a soma do valor dos itens já conferidos de um pedido. O segundo parametro, se informado
	* limita a consulta ao valor das notas conferidas até o momento.
	* @param integer idpedido
	* @param integer id_documento
	* @return double vlr_soma
	*/
	function sum_vlr_itens_conferidos($idpedido = 0, $id_documento = 0)
	{
		$sql  = "SELECT  IFNULL(SUM(pl.QTD_ENTREGUE * pl.PRECO_UNIT), 0) AS SUBTOTAL FROM pedidolivro pl WHERE pl.IDPEDIDO = $idpedido"; 
		$sql .= ($id_documento != '' && $id_documento != 0) ? " AND pl.id_pedido_documento = $id_documento" : ' AND pl.id_pedido_documento IS NOT NULL';
		$result = $this->db->query($sql);
		
		// Método diferente para a coleta de valores do resultset (exemplo)
		if($result->num_rows() > 0)
		{
			$row = $result->row();
			return $row->SUBTOTAL;
		} else {
			return 0;
		}
	}
	
	/**
	* get_documentos_pedido()
	* Retorna todos os documentos do pedido. Caso informado segundo parametro mode,
	* limita a consulta em comprovantes ou notas fiscais.
	* @param integer idpedido
	* @param string mode
	* @param integer numero_entrega
	* @return array documentos
	*/
	function get_documentos_pedido($idpedido = 0, $mode = '', $numero_entrega = 0)
	{
		switch(strtolower($mode))
		{
			case 'comprovantes':
				$sql = "SELECT * FROM pedido_documento pd
			          LEFT JOIN pedido_documento_tipo pdt ON (pdt.id_pedido_documento_tipo = pd.id_pedido_documento_tipo)
					      WHERE idpedido = $idpedido AND PD.ID_PEDIDO_DOCUMENTO_TIPO = 2 ";
				$sql .= ($numero_entrega != 0 && $numero_entrega != '' && !is_null($numero_entrega)) ? " AND PD.NUMERO_ENTREGA = $numero_entrega " : '';
				$sql .= " ORDER BY pd.id_pedido_documento_tipo, numero_doc, dtt_ins";
			break;
			
			case 'notas_fiscais':
				$sql = "SELECT * FROM pedido_documento pd
			         LEFT JOIN pedido_documento_tipo pdt ON (pdt.id_pedido_documento_tipo = pd.id_pedido_documento_tipo)
					     WHERE pd.idpedido = $idpedido AND PD.ID_PEDIDO_DOCUMENTO_TIPO = 1 ";
 				$sql .= ($numero_entrega != 0 && $numero_entrega != '' && !is_null($numero_entrega)) ? " AND PD.NUMERO_ENTREGA = $numero_entrega " : '';
				$sql .= " ORDER BY pd.id_pedido_documento_tipo, numero_doc, dtt_ins";
			break;
			
			default:
				$sql = "SELECT PD.ID_PEDIDO_DOCUMENTO AS `#`,
				               PD.NUMERO_DOC          AS `NUM. DOC`,
							   PD.QTDE_ITENS          AS EXEMPLARES,
							   PD.VALOR_DOC           AS `VALOR DOC`,
							   PD.NUMERO_ENTREGA      AS `NÚMERO ENTREGA`,
							   UT.NOMEUSUARIOTIPO     AS `INSERIDO POR`,
							   PDT.NOME, 
							   PD.ARQUIVO,
							   PD.ID_PEDIDO_DOCUMENTO,
							   PD.IDUSUARIOTIPO
				               FROM pedido_documento pd
			         LEFT JOIN PEDIDO_DOCUMENTO_TIPO PDT ON (pdt.id_pedido_documento_tipo = pd.id_pedido_documento_tipo)
					INNER JOIN USUARIOTIPO            UT ON (UT.IDUSUARIOTIPO = PD.IDUSUARIOTIPO)
					     WHERE idpedido = $idpedido ";
				$sql .= ($numero_entrega != 0 && $numero_entrega != '' && !is_null($numero_entrega)) ? " AND PD.NUMERO_ENTREGA = $numero_entrega " : '';
				$sql .= " ORDER BY pd.id_pedido_documento_tipo, numero_doc, dtt_ins";
			break;
		}
		
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return(isset($return)) ? $return : array();
	}
	
	/**
	* get_documentos_pedido_options()
	* Retorna todos os documentos do pedido - para construção de COMBOBOX.
	* @param integer idpedido
	* @return array documentos
	*/
	function get_documentos_pedido_options($idpedido = 0, $tipo = 1)
	{
		$sql = "SELECT PD.ID_PEDIDO_DOCUMENTO, PD.NUMERO_DOC FROM pedido_documento pd
			        LEFT JOIN pedido_documento_tipo pdt ON (pdt.id_pedido_documento_tipo = pd.id_pedido_documento_tipo)
					    WHERE idpedido = $idpedido AND pdt.id_pedido_documento_tipo = $tipo ORDER BY dtt_ins";
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return(isset($return)) ? $return : array();
	}
	
	/**
	* update_item_conferencia()
	* Atualiza um item da conferencia, setando qtd_entregue e id do documento.
	* @param integer idpedido
	* @param integer idlivro
	* @param integer qtd_entregue
	* @param integer id_pedido_documento
	* @return array documentos
	*/
	function update_item_conferencia($idpedido = 0, $idlivro = 0, $qtd_entregue = 0, $id_pedido_documento = 0)
	{
		// Coluna id_pedido_documento
		$id_pedido_documento = ($id_pedido_documento == 0 || $id_pedido_documento == '') ? 'NULL' : $id_pedido_documento;
		
		// SQL de atualização do item
		$sql  = "UPDATE pedidolivro SET ";
		$sql .= "QTD_ENTREGUE = $qtd_entregue, ID_PEDIDO_DOCUMENTO = $id_pedido_documento ";
		$sql .= "WHERE IDLIVRO = $idlivro AND IDPEDIDO = $idpedido";
		$result = $this->db->query($sql);
	}
	
	/**
	* is_primeira_entrega_biblioteca()
	* Retorna booleano verificando se é primeira ou segunda entrega da biblioteca.
	* @param integer idpedido
	* @return boolean primeira_entrega
	*/
	function is_primeira_entrega_biblioteca($idpedido = 0)
	{
		// Faz somatório do total de itens
		$sql = "SELECT IFNULL(COUNT(*), 0) AS QTDE FROM pedido_documento WHERE IDPEDIDO = $idpedido AND id_pedido_documento_tipo = 1 AND NUMERO_ENTREGA = 1";
		$dados = $this->db->query($sql);
		
		// Método diferente para a coleta de valores do resultset (exemplo)
		if($dados->num_rows() > 0)
		{
			$row = $dados->row();
			return ($row->QTDE > 0) ? false : true;
		} else {
			return true;
		}
	}
	
	/**
	* get_count_titulos()
	* Retorna a quantidade de titulos do pedido.
	* @param integer idpedido
	* @return integer qtde
	*/
	function get_count_titulos($idpedido = 0)
	{
		// Faz somatório do total de itens
		$sql = "SELECT IFNULL(COUNT(*), 0) AS QTDE FROM pedidolivro WHERE IDPEDIDO = $idpedido";
		$dados = $this->db->query($sql);
		
		// Método diferente para a coleta de valores do resultset (exemplo)
		if($dados->num_rows() > 0)
		{
			$row = $dados->row();
			return $row->QTDE;
		} else {
			return 0;
		}
	}
	
	/**
	* get_count_itens()
	* Retorna a quantidade de itens do pedido.
	* @param integer idpedido
	* @return integer qtde
	*/
	function get_count_itens($idpedido = 0)
	{
		// Faz somatório do total de itens
		$sql = "SELECT SUM(QTD) AS QTDE FROM pedidolivro WHERE IDPEDIDO = $idpedido";
		$dados = $this->db->query($sql);
		
		// Método diferente para a coleta de valores do resultset (exemplo)
		if($dados->num_rows() > 0)
		{
			$row = $dados->row();
			return $row->QTDE;
		} else {
			return 0;
		}
	}
	
	/**
	* get_sum_itens_obrigatorios()
	* Retorna a somatória do valor total dos itens que são obrigatórios de um pedido,
	* isto é, os não inexigíveis.
	* @param integer idpedido
	* @return integer vlr
	*/
	function get_sum_itens_obrigatorios($idpedido = 0)
	{
		// Faz somatório do total de itens
		$sql = "SELECT IFNULL(SUM((PRECO_UNIT * QTD_ENTREGUE)), 0) AS VLR FROM pedidolivro pl WHERE IDPEDIDO = $idpedido AND pl.IDLIVRO IN (SELECT L.IDLIVRO FROM LIVRO L WHERE L.IDLIVRO = PL.IDLIVRO AND L.PEDIDO_OBRIGATORIO = 'S')";
		$dados = $this->db->query($sql);
		
		// Método diferente para a coleta de valores do resultset (exemplo)
		if($dados->num_rows() > 0)
		{
			$row = $dados->row();
			return $row->VLR;
		} else {
			return 0;
		}
	}
	
	/**
	* get_count_items_nao_conferidos()
	* Retorna a quantidade de itens do pedido com status nao conferido.
	* @param integer idpedido
	* @return integer qtde
	*/
	function get_count_itens_nao_conferidos($idpedido = 0)
	{
		// Faz somatório do total de itens
		$sql = "SELECT IFNULL(COUNT(*), 0) AS QTDE FROM pedidolivro WHERE IDPEDIDO = $idpedido AND ID_PEDIDO_DOCUMENTO IS NULL";
		$dados = $this->db->query($sql);
		
		// Método diferente para a coleta de valores do resultset (exemplo)
		if($dados->num_rows() > 0)
		{
			$row = $dados->row();
			return $row->QTDE;
		} else {
			return 0;
		}
	}
	
	/**
	* get_itens_nao_conferidos()
	* Retorna os itens nao conferidos ainda.
	* @param integer idpedido
	* @return array itens
	*/
	function get_itens_nao_conferidos($idpedido = 0)
	{
		// Faz somatório do total de itens
		$sql = "SELECT * FROM pedidolivro pl INNER JOIN livro l ON (L.IDLIVRO = PL.IDLIVRO) WHERE IDPEDIDO = $idpedido AND ID_PEDIDO_DOCUMENTO IS NULL";
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return(isset($return)) ? $return : array();
	}
	
	/**
    * get_itens_pedido_editora()
    * Retorna todos os itens de todos os pedidos de uma editora, para um programa.
	* @param integer idusuario
	* @param integer idprograma
    * @return array status
    */
    function get_itens_pedido_editora($idusuario = 0, $idprograma = 0) 
	{
        $return = array();
        // Coleta os dados de itens
        $sql = "SELECT CE.RAZAOSOCIAL                             AS EDITORA,
					   L.TITULO, 
					   PL.QTD_DISPONIVEL                          AS `QTDE. CADASTRADA`, 
					   PL.PRECO                                   AS `PREÇO BASE`, 
					   SUM(PEDL.QTD)                              AS `QTDE. SOLICITADA`, 
					   PEDL.PRECO_UNIT                            AS `PREÇO UNITÁRIO`,
					   (SUM(PEDL.QTD_ENTREGUE) * PEDL.PRECO_UNIT) AS SUBTOTAL,
                       L.IDLIVRO
				  FROM PEDIDOLIVRO PEDL
			INNER JOIN LIVRO          L ON (L.IDLIVRO = PEDL.IDLIVRO)
			INNER JOIN PEDIDO         P ON (P.IDPEDIDO = PEDL.IDPEDIDO)
			INNER JOIN CADEDITORA    CE ON (CE.IDUSUARIO = L.IDUSUARIO)
			INNER JOIN PROGRAMALIVRO PL ON (PL.IDLIVRO = L.IDLIVRO AND PL.IDPROGRAMA = $idprograma)
			     WHERE P.IDPEDIDOSTATUS != 1
                   AND CE.IDUSUARIO = $idusuario
			  GROUP BY IDLIVRO, EDITORA, TITULO, `QTDE. CADASTRADA`, `PREÇO BASE`, `PREÇO UNITÁRIO`
			  ORDER BY TITULO;";
        $result = $this->db->query($sql);
        $return = $result->result_array();
        $return = (isset($return)) ? $return : array();
        return $return;
    }
	
	/**
    * get_itens_pedido_editora_por_pdv()
    * Retorna todos os itens de todos os pedidos de uma editora, contendo nome do PDV.
	* @param integer idusuario
	* @param integer idprograma
    * @return array status
    */
    function get_itens_pedido_editora_por_pdv($idusuario = 0, $idprograma = 0) 
	{
		$return = array();
        // Coleta os dados de itens
        $sql = "SELECT CP.RAZAOSOCIAL       AS PDV,
					   CE.RAZAOSOCIAL       AS EDITORA,
					   L.TITULO,
					   SUM(PL.QTD_ENTREGUE) AS `QTDE. SOLICITADA`,
					   PL.PRECO_UNIT        AS `PREÇO UNITÁRIO`,
					   L.IDLIVRO
				  FROM LIVRO L
			INNER JOIN PEDIDOLIVRO    PL ON (PL.IDLIVRO = L.IDLIVRO)
			INNER JOIN PROGRAMALIVRO PL2 ON (PL2.IDLIVRO = L.IDLIVRO AND PL2.IDPROGRAMA = $idprograma)
		 	INNER JOIN PEDIDO          P ON (P.IDPEDIDO = PL.IDPEDIDO)
			INNER JOIN CADEDITORA     CE ON (CE.IDUSUARIO = L.IDUSUARIO)
			INNER JOIN CADPDV         CP ON (CP.IDUSUARIO = P.IDPDV)
			INNER JOIN CADBIBLIOTECA  CB ON (CB.IDUSUARIO = P.IDBIBLIOTECA)
			 	 WHERE CE.IDUSUARIO IN ($idusuario)
				   AND P.IDPEDIDOSTATUS != 1 
			  GROUP BY IDLIVRO, TITULO, EDITORA, PDV, `PREÇO UNITÁRIO`
			  ORDER BY PDV";
		$result = $this->db->query($sql);
        $return = $result->result_array();
        $return = (isset($return)) ? $return : array();
        return $return;
	}

	
	function copiaPedidosTabelaTemp($idpedido = null)
	{
		$return = false;
		if(!is_null($idpedido))
		{
			$sql = sprintf("SELECT COUNT(*) AS CONT FROM temppedido WHERE idpedido = %d", $idpedido);
			
			$result = $this->db->query($sql);
			$res = $result->result_array();
			
			if ($res[0]['CONT'] == 0) {
				$sql = sprintf("INSERT INTO temppedido SELECT * FROM pedido WHERE idpedido = %d", $idpedido);
	
				$dados = $this->db->query($sql);
			}
			
			$sql = sprintf("SELECT COUNT(*) as CONT FROM temppedidolivro WHERE idpedido = %d", $idpedido);
			
			$result = $this->db->query($sql);
			$res = $result->result_array();
			
			if ($res[0]['CONT'] == 0) {
				$sql = sprintf("INSERT INTO temppedidolivro SELECT * FROM pedidolivro WHERE idpedido = %d", $idpedido);
				
				$dados = $this->db->query($sql);
			}
			
			$return = true;
		}
		return $return;
	}
	
	/**
	* get_lista_modulo()
	* Retorna os dados da listagem de entrada do modulo.
	* return array data
	*/
	function get_lista_modulo($filters = array())
	{
		$sql = "SELECT 	p.IDPEDIDO AS `#`,
						ps.DESCSTATUS AS SITUACAO,
						P.VALORTOTAL,
						CONCAT(CONCAT('(', cb.IDUSUARIO, ')'), ' ', CB.RAZAOSOCIAL) AS BIBLIOTECA,
						U.CPF_CNPJ AS `CPF/CNPJ BIBLIOTECA`,
						CONCAT(CONCAT('(', CP.IDUSUARIO, ')'), ' ', CP.RAZAOSOCIAL) AS `PONTO DE VENDA`,
						C.IDUF AS `UF PDV`,
						C.NOMECIDADE AS `CIDADE PDV`,
						DATE_FORMAT(p.DATA_ULT_ATU, '%d/%m/%Y %H:%i') AS `DT. ATUALIZAÇÃO`,
						P.*
						/*SUM(PL.QTD) AS `QTDE. EXEMPLARES`*/
				 FROM 	pedido p
		   INNER JOIN pedidostatus   ps ON (p.IDPEDIDOSTATUS = ps.IDPEDIDOSTATUS)
		   INNER JOIN cadbiblioteca  cb ON (p.IDBIBLIOTECA = cb.IDUSUARIO)
		    /*LEFT JOIN pedidolivro    pl ON (pl.IDPEDIDO = P.IDPEDIDO)*/
			LEFT JOIN cadpdv         cp ON (p.IDPDV = cp.IDUSUARIO)
			LEFT JOIN usuario         u ON (u.IDUSUARIO = cb.IDUSUARIO)
			LEFT JOIN logradouro      L ON (L.IDLOGRADOURO = CP.IDLOGRADOURO)
			LEFT JOIN logradourotipo LT ON (LT.IDLOGRADOUROTIPO = L.IDLOGRADOUROTIPO)
			LEFT JOIN CIDADE          C ON (L.IDCIDADE = C.IDCIDADE)";
		
		// Carrega model auxiliar para processamento do where
		$this->load->model('global_model');
		$sql .= $this->global_model->get_where_filters($filters);
		
		// Concatena ORDER BY
		$sql .= ' ORDER BY p.IDPEDIDOSTATUS, cb.RAZAOSOCIAL , cp.RAZAOSOCIAL';
		
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados)) ? $dados : array();
	}
	
	/**
	* get_pedidos_conferencia_adm()
	* Retorna os pedidos que podem ser marcados como pagáveis na conferencia, isto é,
	* todos os pedidos que tenham pelo menos um documento tipo nota fiscal marcado com
	* numero de entrega. Caso informado numero de pedido, limita consulta.
	* @param integer idpedido
	* @return array data
	*/
	function get_pedidos_conferencia_adm($idpedido = 0, $order_by = '')
	{
		$sql = "SELECT P.*, 
		               CB.IDUSUARIO AS IDBIBLIOTECA,
					   CP.IDUSUARIO AS IDPDV,
					   CB.RAZAOSOCIAL AS BIBLIOTECA,
					   CP.RAZAOSOCIAL AS PDV,
					   (SELECT COUNT(*) FROM PEDIDO_VALOR_ENTREGA PV WHERE PV.IDPEDIDO = P.IDPEDIDO) AS TEM_RECURSO_LIBERADO
				  FROM pedido p
		    INNER JOIN pedidostatus   ps ON (p.IDPEDIDOSTATUS = ps.IDPEDIDOSTATUS)
		    INNER JOIN cadbiblioteca  cb ON (p.IDBIBLIOTECA = cb.IDUSUARIO)
			 LEFT JOIN cadpdv         cp ON (p.IDPDV = cp.IDUSUARIO)
			 LEFT JOIN usuario         u ON (u.IDUSUARIO = cb.IDUSUARIO)
			 LEFT JOIN logradouro      L ON (L.IDLOGRADOURO = CP.IDLOGRADOURO)
			 LEFT JOIN logradourotipo LT ON (LT.IDLOGRADOUROTIPO = L.IDLOGRADOUROTIPO)
			 LEFT JOIN CIDADE          C ON (L.IDCIDADE = C.IDCIDADE)
			     WHERE (SELECT COUNT(*) FROM PEDIDO_DOCUMENTO PD WHERE PD.IDPEDIDO = P.IDPEDIDO AND PD.ID_PEDIDO_DOCUMENTO_TIPO = 1 AND PD.NUMERO_ENTREGA != 0) > 0";
		$sql .= ($idpedido != 0 && $idpedido != '' && !is_null($idpedido)) ? " AND P.IDPEDIDO = $idpedido " : '';
		
		// Concatena ORDER BY
		$sql .= ' ORDER BY ';
		$sql .= ($order_by != '') ? $order_by . ', IDPEDIDO' : ' IDPEDIDO ';
		
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados)) ? $dados : array();
	}
	
	/**
	* get_vlr_total_doc_entrega()
	* Retorna o valor total de uma entrega, baseado nas notas fiscais cadastradas para ela e no numero da entrega.
	* @param integer idpedido
	* @param integer numero_entrega
	* @return array documentos
	*/
	function get_vlr_total_doc_entrega($idpedido = 0, $numero_entrega = 0)
	{
		$sql = "SELECT SUM(VALOR_DOC) AS TOTAL FROM pedido_documento pd
		     LEFT JOIN pedido_documento_tipo pdt ON (pdt.id_pedido_documento_tipo = pd.id_pedido_documento_tipo)
			     WHERE idpedido = $idpedido AND PD.ID_PEDIDO_DOCUMENTO_TIPO = 1";
		$sql .= ($numero_entrega != 0 && $numero_entrega != '' && !is_null($numero_entrega)) ? " AND PD.NUMERO_ENTREGA = $numero_entrega " : '';
		
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return (get_value($return[0], 'TOTAL') != '') ? get_value($return[0], 'TOTAL') : 0;
	}
	
	/**
	* tem_recurso_liberado()
	* Retorna booleano, dizendo se existe algum recurso já liberado e que está pronto para ser gerado credito, para o pedido em questão.
	* @param integer idpedido
	* @return boolean tem_recurso
	*/
	function tem_recurso_liberado($idpedido = 0)
	{
		$sql = "SELECT COUNT(*) AS TOTAL FROM pedido_valor_entrega WHERE idpedido = $idpedido";
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return (get_value($return[0], 'TOTAL') != '' && get_value($return[0], 'TOTAL') != 0) ? true : false;
	}
	
	/**
	* is_recurso_liberado()
	* Retorna booleano, dizendo se recurso já foi liberado e está pronto para ser gerado credito.
	* Por entrega.
	* @param integer idpedido
	* @param integer num_entrega
	*/
	function is_recurso_liberado($idpedido = 0, $num_entrega = 0)
	{
		$sql = "SELECT COUNT(*) AS TOTAL FROM pedido_valor_entrega WHERE idpedido = $idpedido AND NUMERO_ENTREGA = $num_entrega";
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return (get_value($return[0], 'TOTAL') != '' && get_value($return[0], 'TOTAL') != 0) ? true : false;
	}
	
	/**
	* is_credito_emitido()
	* Retorna booleano, dizendo se já foi emitido credito para o recurso em questao.
	* @param integer idpedido
	* @param integer num_entrega
	*/
	function is_credito_emitido($idpedido = 0, $num_entrega = 0)
	{
		$sql = "SELECT CREDITO_EMITIDO FROM pedido_valor_entrega WHERE idpedido = $idpedido AND NUMERO_ENTREGA = $num_entrega";
		$result = $this->db->query($sql);
		$return = $result->result_array();
		if(isset($return[0]))
		{
			return (get_value($return[0], 'CREDITO_EMITIDO') != '' && get_value($return[0], 'CREDITO_EMITIDO') != 'N') ? true : false;
		} else {
			return false;
		}
	}
	
	/**
	* get_observacoes()
	* Retorna registros de observações do pedido.
	* @param integer idpedido
	* @param array observacoes
	*/
	function get_observacoes($idpedido = 0)
	{
		$sql = "SELECT PO.ID_PEDIDO_OBSERVACAO AS `#`, 
		               PO.CATEGORIA,
					   U.LOGIN AS USUARIO,
					   PO.OBSERVACAO, 
					   DATE_FORMAT(PO.DTT_INS, '%d/%m/%Y %H:%i') AS `DATA INS.`,
					   PO.LIDO,
					   CASE WHEN PO.LIDO = 'S' THEN 'checked=\"checked\"' ELSE '' END AS LIDO_CHECKED
				  FROM PEDIDO_OBSERVACAO PO 
			INNER JOIN USUARIO U ON (U.IDUSUARIO = PO.IDUSUARIO)
				 WHERE PO.IDPEDIDO = $idpedido 
			  ORDER BY PO.DTT_INS DESC";
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return (isset($return) && count($return) > 0) ? $return : array();
	}
	
	/**
	* count_observacoes_nao_lidas()
	* Retorna a contagem de observacoes não lidas.
	* @param integer idpedido
	* @param integer count
	*/
	function count_observacoes_nao_lidas($idpedido = 0)
	{
		$sql = "SELECT IFNULL(COUNT(*), 0) AS TOTAL
				  FROM PEDIDO_OBSERVACAO PO
			INNER JOIN USUARIO U ON (U.IDUSUARIO = PO.IDUSUARIO)
				 WHERE PO.IDPEDIDO = $idpedido AND PO.LIDO = 'N'";
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return (get_value($return[0], 'TOTAL') == '') ? 0 : get_value($return[0], 'TOTAL');
	}
	
	/**
	* get_quantidade_entregas()
	* Retorna a contagem de entregas realizadas por uma entidade.
	* @param integer idpedido
	* @param string idusuariotipo
	* @param integer count
	*/
	function get_quantidade_entregas($idpedido = 0, $tipo_usuario = 0)
	{
		$sql = "SELECT IFNULL(MAX(NUMERO_ENTREGA), 0) AS ENTREGAS_FINALIZADAS
				  FROM PEDIDO_DOCUMENTO PD 
				 WHERE PD.IDPEDIDO = $idpedido 
				   AND PD.IDUSUARIOTIPO = $tipo_usuario";
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return (get_value($return[0], 'ENTREGAS_FINALIZADAS') == '') ? 0 : get_value($return[0], 'ENTREGAS_FINALIZADAS');
	}
	
	/**
	* get_lista_pedidos_pdv()
	* Retorna registros de pedidos de determinado pdv.
	* @param array filters
	* @param array pedidos
	*/
	function get_lista_pedidos_pdv($filters = array())
	{
		$sql = "SELECT P.IDPEDIDO AS `#`, 
					   PS.DESCSTATUS AS `SITUAÇÃO`,
		               CONCAT('<a class=\"font_11\" href=\"javascript:void(0);\" onclick=\"ajax_modal(''Visualizar Detalhes - Biblioteca'', ''" .  URL_EXEC . "biblioteca/modal_form_view_biblioteca/', IDBIBLIOTECA, ''', ''" . URL_IMG . "icon_detalhes.png'', 600, 750);\" title=\"Visualizar Detalhes\">', CB.RAZAOSOCIAL, '</a>') AS BIBLIOTECA,
					   (SELECT SUM(PL.QTD) FROM PEDIDOLIVRO PL WHERE PL.IDPEDIDO = P.IDPEDIDO) AS `QTDE.`,
					   P.VALORTOTAL,
					   CONCAT('<img src=\"" . URL_IMG . "', ED.ICONE_EDITAL, '\" title=\"', ED.NOME_EDITAL, '\" style=\"width:16px\" />') AS EDITAL_IMG
				  FROM PEDIDO P
			INNER JOIN CADBIBLIOTECA CB ON (CB.IDUSUARIO = P.IDBIBLIOTECA)
			INNER JOIN PEDIDOSTATUS  PS ON (PS.IDPEDIDOSTATUS = P.IDPEDIDOSTATUS)
			INNER JOIN PROGRAMA      ED ON (ED.IDPROGRAMA = P.IDPROGRAMA)";
		
		// Carrega model auxiliar para processamento do where
		$this->load->model('global_model');
		$sql .= $this->global_model->get_where_filters($filters);
		
		// Coloca o status no where
		$sql .= ' AND P.IDPEDIDOSTATUS > 1';
		
		// Concatena ORDER BY
		$sql .= ' ORDER BY p.IDPEDIDO';
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return (isset($return) && count($return) > 0) ? $return : array();
	}
	
	/**
	* get_lista_pedidos_biblioteca()
	* Retorna registros de pedidos de determinado BIBLIOTECA.
	* @param array filters
	* @param array pedidos
	*/
	function get_lista_pedidos_biblioteca($filters = array())
	{
		$sql = "SELECT P.IDPEDIDO AS `#`, 
					   PS.DESCSTATUS AS `SITUAÇÃO`,
		               CONCAT('<a class=\"font_11\" href=\"javascript:void(0);\" onclick=\"ajax_modal(''Visualizar Detalhes - Ponto de Venda'', ''" .  URL_EXEC . "pdv/modal_form_view_pdv/', IDPDV, ''', ''" . URL_IMG . "icon_detalhes.png'', 600, 750);\" title=\"Visualizar Detalhes\">', CP.RAZAOSOCIAL, '</a>') AS `PONTO DE VENDA`,
					   (SELECT SUM(PL.QTD) FROM PEDIDOLIVRO PL WHERE PL.IDPEDIDO = P.IDPEDIDO) AS `QTDE.`,
					   P.VALORTOTAL,
					   CONCAT('<img src=\"" . URL_IMG . "', ED.ICONE_EDITAL, '\" title=\"', ED.NOME_EDITAL, '\" style=\"width:16px\" />') AS EDITAL_IMG
				  FROM PEDIDO P
			INNER JOIN CADPDV        CP ON (CP.IDUSUARIO = P.IDPDV)
			INNER JOIN PEDIDOSTATUS  PS ON (PS.IDPEDIDOSTATUS = P.IDPEDIDOSTATUS)
			INNER JOIN PROGRAMA      ED ON (ED.IDPROGRAMA = P.IDPROGRAMA)";
		
		// Carrega model auxiliar para processamento do where
		$this->load->model('global_model');
		$sql .= $this->global_model->get_where_filters($filters);
		
		// Concatena ORDER BY
		$sql .= ' ORDER BY p.IDPEDIDO';
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return (isset($return) && count($return) > 0) ? $return : array();
	}
	
	/**
	* get_lista_itens_pedidos_total_pdv()
	* Retorna registros de pedidos de determinado pdv.
	* @param integer idusuario
	* @param array pedidos
	*/
	function get_lista_itens_pedidos_total_pdv($idusuario = 0)
	{
		$sql = "SELECT  l.IDLIVRO,
						CONCAT('=\"',l.ISBN,'\"') AS ISBN,
						e.RAZAOSOCIAL AS EDITORA,
						l.TITULO,
						l.AUTOR,
						l.EDICAO,
						l.ANO,
						pl.QTD,
						pl.PRECO_UNIT,
						(pl.QTD * pl.PRECO_UNIT) AS SUBTOTAL,
						CASE WHEN L.PEDIDO_OBRIGATORIO = 'N' THEN 'SIM' ELSE 'NÃO' END AS `LIVRO INEXIGÍVEL`,
						CASE WHEN ID_PEDIDO_DOCUMENTO IS NOT NULL THEN 'SIM' ELSE 'NÃO' END AS `CONFERIDO BIBLIOTECA`,
						CASE WHEN ID_PEDIDO_DOCUMENTO IS NOT NULL THEN PL.QTD_ENTREGUE ELSE 0 END AS `QTDE. INFORMADA CONFERENCIA`,
						p.IDPEDIDO,
						CB.RAZAOSOCIAL AS BIBLIOTECA
				   FROM pedidolivro pl
			 INNER JOIN pedido     p ON (pl.IDPEDIDO = p.IDPEDIDO)
			 INNER JOIN CADBIBLIOTECA CB ON (CB.IDUSUARIO = p.IDBIBLIOTECA)
			 INNER JOIN livro      l ON (pl.IDLIVRO = l.IDLIVRO)
			 INNER JOIN cadeditora e ON (l.IDUSUARIO = e.IDUSUARIO)
			      WHERE p.IDPDV = $idusuario
			   ORDER BY BIBLIOTECA";
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return (isset($return) && count($return) > 0) ? $return : array();
	}
	
	/**
	* get_max_entregas_finalizadas()
	* Retorna a quantidade total de entregas finalizadas até o momento. Basea-se nas notas fiscais da biblioteca,
	* que define quando uma entrega de fato está completa.
	* @param integer idpedido
	* @return integer count_entregas
	*/
	function get_max_entregas_finalizadas($idpedido = 0)
	{
		$sql = "SELECT DISTINCT IFNULL(MAX(NUMERO_ENTREGA), 0) AS MAX_ENTREGA FROM PEDIDO_DOCUMENTO WHERE IDPEDIDO = $idpedido AND ID_PEDIDO_DOCUMENTO_TIPO = 1";
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return (get_value($return[0], 'MAX_ENTREGA') == '') ? 0 : get_value($return[0], 'MAX_ENTREGA');
	}
	
	/**
	* get_max_numero_entrega_doc()
	* Retorna o numero maximo de numero_entrega de um determinado pedido com base nos documentos cadastrados.
	* @param integer idpedido
	* @return integer count_entregas
	*/
	function get_max_numero_entrega_doc($idpedido = 0)
	{
		$sql = "SELECT DISTINCT IFNULL(MAX(NUMERO_ENTREGA), 0) AS MAX_ENTREGA FROM PEDIDO_DOCUMENTO WHERE IDPEDIDO = $idpedido";
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return (get_value($return[0], 'MAX_ENTREGA') == '') ? 0 : get_value($return[0], 'MAX_ENTREGA');
	}
	
	/**
	* get_pedidos_com_docs_liberar_edicao()
	* Retorna um array de pedidos que possuem marcação em algum documento para liberar edição (Docs com base no
	* tipo de usuario atual, ou seja, somente comprovantes ou somente notas fiscais conforme usuario).
	* @param integer idusuario
	* @param integer tipo_usuario
	* @return array pedidos
	*/
	function get_pedidos_com_docs_liberar_edicao($idusuario = 0, $tipo_usuario = 0)
	{
		$sql = "SELECT P.*
                  FROM PEDIDO P
                 WHERE P.IDPEDIDO IN (SELECT IDPEDIDO FROM PEDIDO_DOCUMENTO PD WHERE PD.IDPEDIDO = P.IDPEDIDO AND PD.IDUSUARIOTIPO = $tipo_usuario AND PD.LIBERAR_EDICAO = 'S')
                   AND " . (($tipo_usuario == 2) ? 'P.IDBIBLIOTECA' : 'P.IDPDV') . " = $idusuario";
		$result = $this->db->query($sql);
		return $result->result_array(); 
	}
}