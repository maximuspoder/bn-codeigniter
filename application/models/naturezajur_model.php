<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Naturezajur_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	function getCombo($pessoa)
	{
		$where = '';
		
		if ($pessoa == 'PF')
		{
			$where = 'AND (n.IDNATUREZAJUR = 4 OR n.IDPAI = 4)';
		}
		elseif ($pessoa == 'PJ')
		{
			$where = 'AND n.IDNATUREZAJUR != 4 AND n.IDPAI != 4';
		}
		
		$sql = sprintf("SELECT n.*, n2.NOMENATUREZAJUR AS NOMEPAI
						FROM naturezajur n
						LEFT JOIN naturezajur n2 ON (n.IDPAI = n2.IDNATUREZAJUR)
						WHERE n.IDPAI != 0
						  AND n.ATIVO = 'S'
						  %s
						ORDER BY n2.NOMENATUREZAJUR, n.NOMENATUREZAJUR", $where);
		
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
}

/* End of file Naturezajur_model.php */
/* Location: ./system/application/models/Naturezajur_model.php */