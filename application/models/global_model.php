<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Global_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	/*
	 * INSERT
	 * $table    : nome da tabela
	 * $arrParam : array com campos a inserir
	 */
	function insert($table, $arrParam)
	{
		$result = $this->db->insert($table, $arrParam);
		return $result;
	}
	
	/*
	 * UPDATE
	 * $table    : nome da tabela
	 * $arrParam : array com campos a atualizar
	 * $where    : pode ser array('id' => $id)... ou "id = 1"
	 */
	function update($table, $arrParam, $where)
	{
		$result = $this->db->update($table, $arrParam, $where);
		return $result;
	}
	
	/*
	 * DELETE
	 * $table    : nome da tabela
	 * $where    : pode ser array('id' => $id)... ou "id = 1"
	 */
	function delete($table, $where)
	{
		$result = $this->db->delete($table, $where);
		return $result;
	}
	
	/*
	 * GET_INSERT_ID
	 * retorna o ultimo ID inserido
	 */
	function get_insert_id()
	{
		return $this->db->insert_id();
	}
	
	/*
	 * GET_AFFECTED_ROWS
	 * retorna o numero de linhas afetadas pela ultima query
	 */
	function get_affected_rows()
	{
		return $this->db->affected_rows();
	}
	
	/*
	 * GET_DADO
	 * retorna uma coluna especifica
	 */
	function get_dado($table,$coluna,$where)
	{
		$sql   = sprintf("select %s as dado from %s where %s", $coluna, $table, $where);
		$dados = $this->db->query($sql);
		
		if ($dados->num_rows() > 0)
		{
			$row = $dados->row();
			return $row->dado;
		}
		else
		{
			return '';
		}
	}
	
	/*
	 * GET_DADO_JOIN
	 * retorna uma coluna especifica
	 */
	function get_dado_join($table, $coluna, $join, $where)
	{
		$sql   = sprintf("select %s as dado 
						  from %s 
						  inner join %s
						  where %s", $coluna, $table, $join, $where);
		$dados = $this->db->query($sql);
		
		if ($dados->num_rows() > 0)
		{
			$row = $dados->row();
			return $row->dado;
		}
		else
		{
			return '';
		}
	}
	
	/*
	 * SELECTX
	 * retorna dados de uma tabela
	 */
	function selectx($table, $where, $order)
	{
		$sql = sprintf("SELECT * FROM %s WHERE %s ORDER BY %s", $table, $where, $order);
		
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}

	/*
	 * SELECT_ID
	 * retorna apenas os IDs de uma determinada consulta para uma tabela
	 */
	function select_id($table, $primarykey, $where)
	{
		$sql = sprintf("SELECT %s AS id FROM %s WHERE %s ORDER BY %s", $primarykey, $table, $where, $primarykey);
		
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
    /*
	 * GET_DADO_PESQUISA_ESTAB
	 * retorna dados de uma tabela
	 */
	function get_dado_pesquisa_estab($arrParam, $onlyCount = FALSE)
	{
        if ($onlyCount)
		{
			$sql = sprintf("SELECT count(*) as CONT 
                            FROM usuario u
							INNER JOIN %s               t  ON (u.IDUSUARIO = t.IDUSUARIO)
                            INNER JOIN logradouro       l  ON (t.IDLOGRADOURO = l.IDLOGRADOURO)
                            INNER JOIN cidade           c  ON (c.IDCIDADE = l.IDCIDADE)
                            WHERE %s",
                            $arrParam['table'],
                            $arrParam['where']);
		}
		else
		{
			$programa = ($this->session->userdata('programa') != 0) ? $this->session->userdata('programa') : 0;
            $sql = sprintf("SELECT
                                u.*,
                                t.*,
								cf.NOMERESP AS RESPFINAN,
								cf.EMAILRESP AS EMAILFINAN,
								cf.TELEFONERESP AS FONEFINAN,
                                ut.NOMEUSUARIOTIPO,
                                l.IDLOGRADOURO,
                                l.CEP,
                                c.IDUF,
                                c.IDCIDADE,
                                c.NOMECIDADE,
                                c.NOMECIDADESUB,
                                b.IDBAIRRO,
                                b.NOMEBAIRRO,
                                lt.NOMELOGRADOUROTIPO,
                                l.NOMELOGRADOURO,
                                l.COMPLEMENTO,
								CASE WHEN UP.IDUSUARIO IS NOT NULL THEN 'SIM' WHEN " . $programa . " = 0 THEN 'EDITAL NAO SELECIONAOD' ELSE 'NAO' END AS HABILITADO
                            FROM usuario u
                            INNER JOIN usuariotipo         ut ON (u.IDUSUARIOTIPO = ut.IDUSUARIOTIPO)
							 LEFT JOIN USUARIOPROGRAMA     UP ON (UP.IDUSUARIO = U.IDUSUARIO AND UP.IDPROGRAMA = " . $programa . ")
							INNER JOIN %s                   t ON (u.IDUSUARIO = t.IDUSUARIO)
							LEFT JOIN cadbibliotecafinan   cf ON (cf.IDBIBLIOTECA = t.IDUSUARIO)
                            INNER JOIN logradouro           l ON (t.IDLOGRADOURO = l.IDLOGRADOURO)
                            INNER JOIN logradourotipo      lt ON (lt.IDLOGRADOUROTIPO = l.IDLOGRADOUROTIPO)
                            INNER JOIN bairro               b ON (b.IDBAIRRO = l.IDBAIRRO)
                            INNER JOIN cidade               c ON (c.IDCIDADE = b.IDCIDADE)
                            WHERE %s
							ORDER BY c.IDUF,c.NOMECIDADE,t.RAZAOSOCIAL
							LIMIT %s, %s",
                            $arrParam['table'],
                            $arrParam['where'],
                            $arrParam['limit_de'],
                            $arrParam['exibir_pp']);	
        }
        
		$dados = $this->db->query($sql);

		return $dados->result_array();
	}
	
	/*
	* get_where_filters()
	* Retorna um where condition com base nos filtros encaminhados. Os indices dos filtros
	* devem conter o formato 'NOME/ALIAS_DA_TABELA__NOME/ALIAS_COLUNA' (separador '__')
	* @param array filters
	* @return string where
	*/
	function get_where_filters($filters = array())
	{
		$return = '';
		// print_r($filters);
		if(count($filters) > 0 && $filters != '')
		{
			$return = ' WHERE 1 = 1 ';
			foreach($filters as $index => $value)
			{
				$column = str_replace('__', '.', $index);
				// Testa o tipo de valor da coluna
				if(valida_integer($value) || valida_float($value))
				{
					$return .= " AND $column = $value";
				}
				
				elseif(valida_alfa($value))
				{
					$return .= " AND $column LIKE '%$value%'";
				}
				
				elseif(valida_currency($value))
				{
					$return .= " AND $column = " . to_float($value);
				}
			}
		}
		return $return;
	}
	
}


/* End of file global_model.php */
/* Location: ./system/application/models/global_model.php */