<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe Upload_Model
*
* Esta classe cont�m m�todos para a abstra��o da entidade model upload.
* 
* @author		Leandro Mangini Antunes
* @package		application
* @subpackage	application.upload
* @since		2012-06-15
*
*/
class Upload_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	/**
	* get_count_fila_upload()
	* Retorna todos a contagem geral da fila de itens que est�o sendo transferidos para o sistema neste momento.
	* @return integer count
	*/
	function get_count_fila_upload()
	{
		$sql = "SELECT COUNT(*) AS QTDE FROM upl_upload ORDER BY ORDEM";
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		return (get_value($dados[0], 'QTDE') != '') ? get_value($dados[0], 'QTDE') : 0;
	}
	
	/**
	* save_in_fila()
	* Insert one line inside table upl_upload, indicating that file is in state of 'uploading'.
	* @return string file_name
	* @return string file_size
	* @return string id_usuario
	* @return integer id_line_inserted
	*/
	function save_in_fila($file_name = '', $file_size = '', $id_usuario = '')
	{
		$sql = "SELECT MAX(ORDEM) + 10 AS MAX_ORDEM FROM upl_upload";
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		$max_v = (get_value($dados[0], 'MAX_ORDEM') != '') ? get_value($dados[0], 'MAX_ORDEM') : 10;
		
		$sql = "INSERT INTO upl_upload (file_name, file_size, idusuario, ordem) VALUES ('" . $file_name . "', '" . $file_size . "', " . $id_usuario . ", " . $max_v . ")";
		$this->db->query($sql);
		
		// Retrieve on last inserted id
		return $this->db->insert_id();
	}
	
	/**
	* remove_from_fila()
	* Remove one element from list of pending elements.
	* @return string id_file
	* @return void
	*/
	function remove_from_fila($id_file = 0)
	{
		$sql = "DELETE FROM upl_upload WHERE id_upload = $id_file";
		$this->db->query($sql);
	}
}
