<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Biblioteca_model extends CI_Model {
	/* filtros */
	protected $ATIVO		= '';
	protected $HABILITADO   = '';
	protected $RAZAOSOCIAL  = '';
	protected $NOMEFANTASIA = '';
	protected $CIDADE		= '';
	protected $UF			= '';
	protected $CPFCNPJ		= '';
	protected $TIPOCAD		= '';	
	protected $IDUSUARIO    = '';
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	function lista($arrParam, $onlyCount = FALSE, $filtros = null)
	{
        $this->load->model('logradouro_model');
        $this->load->model('uf_model');
        $this->load->model('global_model');
        
		if ($onlyCount)
		{
			$sql = "SELECT count(*) as CONT FROM cadbiblioteca c INNER JOIN usuario u ON (u.IDUSUARIO = c.IDUSUARIO)";
		}
		else
		{
			$sql = "SELECT u.*,
						   c.*,
					       DATE_FORMAT(u.DATA_ATIVACAO, '%d/%m/%Y %H:%i') AS ATIVACAO,
						   CASE 
								WHEN u.ATIVO = 'S' THEN 'v_peq.png'
								ELSE 'x.png'
						   END AS IMGSTATUS,
						   CASE 
								WHEN u.ATIVO = 'S' THEN 'VALIDADO'
								ELSE 'NAO VALIDADO'
						   END AS DESC_STATUS,
						   CASE 
								WHEN u.HABILITADO = 'S' THEN 'SIM'
								ELSE 'NAO'
						   END AS USER_HABILITADO
						   FROM usuario u
						   INNER JOIN cadbiblioteca c ON (u.IDUSUARIO = c.IDUSUARIO) ";
		}
		
		// Filtragem - Serve tanto para o count quanto para dados de listagem
		// Filtro para estado
		if($this->UF != '')
		{
			$sql .= "  INNER JOIN logradouro  l ON (l.IDLOGRADOURO = c.IDLOGRADOURO)
					   INNER JOIN cidade     ci ON (ci.IDCIDADE = l.IDCIDADE AND IDUF = '" . $this->UF . "')";
		}        
        
        // Filtro para cidade
		//echo "<script>alert('".$this->CIDADE."')</script>";
		if($this->CIDADE != '' && $this->CIDADE != '-- Selecione um Estado --')
		{
			$cidadeRelacao = '';	
			$sql3 = "select NOMECIDADE from binac.cidade where nomecidade = '$this->CIDADE' ";	
			$dados = $this->db->query($sql3);
			$rs3 = $dados->result_array();
			
			if (count($rs3) > 0) {
				$cidadeRelacao = $rs3[0]['NOMECIDADE'];
				//$ufRelacao_tratado = removeAcentos($ufRelacao->NOMEUF , true);
				$sql .= " AND ( NOMECIDADE = '".$this->CIDADE."' OR NOMECIDADE = '".$cidadeRelacao."')";
			}
		}
		
		// Filtro para estado
		if($this->UF != '')
		{
			$ufRelacao = '';	
			$sql2 = "select NOMEUF from binac.uf  where iduf = '$this->UF' ";
			$dados = $this->db->query($sql2);
			$rsl = $dados->result_array();
			$ufRelacao = $rsl[0]['NOMEUF'];
			//$ufRelacao_tratado = removeAcentos($ufRelacao->NOMEUF , true);
			$sql .= " AND ( IDUF = '".$this->UF."' OR IDUF = '".$ufRelacao."')";
		}
        
		// Adiciona um Where 1=1 para que os filtros abaixo possam ser utilizados
		$sql .= ' WHERE 1=1 ';
		
		// Filtro para habilitado
		if($this->HABILITADO != '')
		{
			$sql .= " AND u.HABILITADO = '" . $this->HABILITADO . "' ";
		}
		
		// Filtro para status
		if($this->ATIVO != '')
		{
			$sql .= " AND u.ATIVO = '" . $this->ATIVO . "' ";
		}
		
		// Filtro para Razao e Nome Fantasia
		if($this->NOMEFANTASIA != '' && $this->RAZAOSOCIAL != '')
		{
			$sql .= " AND (c.RAZAOSOCIAL LIKE '%" . $this->RAZAOSOCIAL . "%' OR c.NOMEFANTASIA LIKE '%" . $this->NOMEFANTASIA . "%')";
		} else {
			// Filtro para Razï¿½o social
			if($this->RAZAOSOCIAL != '')
			{
				$sql .= " AND c.RAZAOSOCIAL LIKE '%" . $this->RAZAOSOCIAL . "%' ";
			}
			
			// Filtro para Nome Fantasia
			if($this->NOMEFANTASIA != '')
			{
				$sql .= " AND c.NOMEFANTASIA LIKE '%" . $this->NOMEFANTASIA . "%' ";
			}
		}
		
		// Finaliza SQL, somente para casos onde nao seja count
		if (!$onlyCount)
		{
			// Prossegue o sql
			$sql .= " ORDER BY c.RAZAOSOCIAL LIMIT " . $arrParam['limit_de'] . "," . $arrParam['exibir_pp'];
		}
		
		$dados = $this->db->query($sql);
		return $dados->result_array();
	}
    
    
    function getCidadesCadbiblioteca2($uf=''){

    	// pega as cidades que possuem 2 ou mais bibliotecas cadastradas
        // através do município
        
        $this->load->model('logradouro_model');
        $this->load->model('uf_model');
        $this->load->model('global_model');
        
        $arrDados = array();
            
        $sql = "SELECT 
                        (CASE WHEN C2.IDCIDADESUB = 0 THEN C2.IDCIDADE ELSE C2.IDCIDADESUB END) AS IDCIDADE, C2.NOMECIDADE
                              FROM CIDADE C2 
                              INNER JOIN LOGRADOURO L ON (L.IDCIDADE = C2.IDCIDADE)
                              INNER JOIN CADBIBLIOTECA CB ON (CB.IDLOGRADOURO = L.IDLOGRADOURO)
                              GROUP BY (CASE WHEN C2.IDCIDADESUB = 0 THEN C2.IDCIDADE ELSE C2.IDCIDADESUB END), C2.IDUF
                              HAVING COUNT(CB.IDUSUARIO) > 1";

        if ($uf != "") $sql .= " AND C2.IDUF = '$uf'";
        
		$arrDados = $this->db->query($sql);
        $rs3 = $arrDados->result_array();
        
        return $rs3;
    }
    
    function getBibliotecasComFilhas(){
		$sql = "SELECT CB.IDUSUARIO, CB.RAZAOSOCIAL FROM CADBIBLIOTECA CB INNER JOIN LOGRADOURO L ON CB.IDLOGRADOURO = L.IDLOGRADOURO
			INNER JOIN CIDADE C ON L.IDCIDADE = C.IDCIDADE
			WHERE IDUSUARIOPAI IS NULL AND EXISTS (
			    SELECT 1 FROM CADBIBLIOTECA CBB WHERE IDUSUARIOPAI = CB.IDUSUARIO
			)
			ORDER BY C.IDUF, C.NOMECIDADE";
		
		$arrDados = $this->db->query($sql);
        $rs3 = $arrDados->result_array();

        return $rs3;
    }
    
	/**
	* getIdsComiteAcervo()
	* Retorna uma string separada por ponto e virgula dos ids do comite de acervo de uma 
	* determinada biblioteca.
	* @return array resultset
	*/
    function getIdsComiteAcervo($idusuario = '')
	{
		$result = '';
		$sql = "SELECT IDUSUARIO FROM CADBIBLIOTECACOMITE WHERE IDBIBLIOTECA = $idusuario";
		$dados = $this->db->query($sql);
        $dados = $dados->result_array();
		foreach($dados as $dado)
		{
			$result .=  $dado['IDUSUARIO'] . ',';
		}
		trim(',', $result);
		return $result;
	}
	
	/**
	* listaBibliotecasUnificacao()
	* Retorna um resultset da biblitoeca pai e as 'duplicadas'
	* para unificador de cadastro.
	* @return array resultset
	*/
    function listaBibliotecasUnificacao(){
		// Seta o result para vazio
		$result = array();
		
        // REtorna vazio caso o filtro de ID não esteja setado
		if($this->IDUSUARIO != '')
		{
			$sql = "SELECT CB.IDUSUARIO, CB.RAZAOSOCIAL, CB.NOMEFANTASIA,
					 U.CPF_CNPJ, CB.NOMERESP, C.IDUF, C.NOMECIDADE,
						(CASE 
								WHEN CF.IDUSUARIO IS NULL THEN 'NAO'
								ELSE 'SIM'
						   END) AS RESPONSAVEL_FINANCEIRO,
						   CF.IDUSUARIO AS IDRESPFINANCEIRO,
						   
						(CASE 
								WHEN U.HABILITADO = 'S' THEN 'SIM'
								ELSE 'NAO'
						   END) AS USER_HABILITADO,
						   
						   (CASE 
								WHEN U.ATIVO = 'S' THEN 'SIM'
								ELSE 'NAO'
						   END) AS DESC_STATUS,

						   (CASE 
								WHEN CRI.IDUSUARIO IS NULL THEN 'NAO'
								ELSE 'SIM'
						   END) AS BLOQUEIO_RESTRICAO,
						   
						   (SELECT COUNT(*) 
							FROM CADBIBLIOTECACOMITE com
							INNER JOIN USUARIO usr ON (com.IDUSUARIO = usr.IDUSUARIO)
							WHERE com.IDBIBLIOTECA = CB.IDUSUARIO) AS COMITE,
							
						   (SELECT COUNT(*) 
							FROM CADBIBLIOTECACOMITE com
							INNER JOIN USUARIO usr ON (com.IDUSUARIO = usr.IDUSUARIO)
							WHERE com.IDBIBLIOTECA = CB.IDUSUARIO AND usr.ATIVO = 'S') AS COMITE_ATIVO,
							
							(SELECT (CASE 
								WHEN COUNT(*) = 0 THEN 'NAO'
								ELSE 'SIM'
						   END) AS BLOQ 
							FROM LOG_GERENCIAMENTO LG
							INNER JOIN USUARIO USR ON (LG.IDBIBLIOTECA = USR.IDUSUARIO)
							WHERE LG.IDBIBLIOTECA = CB.IDUSUARIO) AS BLOQUEIO_LOGGERENCIAMENTO,
							
							(SELECT VLRCREDITO_INICIAL
							FROM CREDITO CR WHERE CR.IDUSUARIO = CB.IDUSUARIO AND CR.IDPROGRAMA = 1) AS VLRCREDITO_INICIAL_3ED,

							(SELECT VLRCREDITO_INICIAL
							FROM CREDITO CR WHERE CR.IDUSUARIO = CB.IDUSUARIO AND CR.IDPROGRAMA = 2) AS VLRCREDITO_INICIAL_4ED,
							
						   (CASE WHEN C.IDCIDADESUB = 0 THEN 
                            C.IDCIDADE 
                        ELSE C.IDCIDADESUB END) AS IDCIDADEMASTER,
						CONCAT(CONCAT(L.NOMELOGRADOURO, ', ', B.IDBAIRRO), ' - CEP: ', L.CEP) AS LOGRADOURO,
						L.IDLOGRADOURO 
						
                        
                        FROM USUARIO				  U
                        INNER JOIN CADBIBLIOTECA	  CB  ON (CB.IDUSUARIO	 = U.IDUSUARIO)
                        LEFT JOIN  CADBIBLIOTECAFINAN CF  ON (CF.IDUSUARIO   = U.IDUSUARIO)
                        LEFT JOIN  CREDITO			  CR  ON (CR.IDUSUARIO	 = U.IDUSUARIO)
                        LEFT JOIN  CREDITORESTRICAO	  CRI ON (CRI.IDUSUARIO	 = U.IDUSUARIO)
                        INNER JOIN LOGRADOURO		  L   ON (L.IDLOGRADOURO = CB.IDLOGRADOURO)
                        INNER JOIN CIDADE			  C   ON (C.IDCIDADE	 = L.IDCIDADE)
						left join  bairro             B   ON (B.IDBAIRRO     = L.IDBAIRRO)
                        WHERE CB.IDUSUARIO = " . $this->IDUSUARIO . "
                        
                        
				UNION

				SELECT CB.IDUSUARIO, CB.RAZAOSOCIAL, CB.NOMEFANTASIA,
					 U.CPF_CNPJ, CB.NOMERESP, C.IDUF, C.NOMECIDADE,
						(CASE 
								WHEN CF.IDUSUARIO IS NULL THEN 'NAO'
								ELSE 'SIM'
						   END) AS RESPONSAVEL_FINANCEIRO,
						   CF.IDUSUARIO AS IDRESPFINANCEIRO,
						   
						(CASE 
								WHEN U.HABILITADO = 'S' THEN 'SIM'
								ELSE 'NAO'
						   END) AS USER_HABILITADO,
						   
						   (CASE 
								WHEN U.ATIVO = 'S' THEN 'SIM'
								ELSE 'NAO'
						   END) AS DESC_STATUS,

						   (CASE 
								WHEN CRI.IDUSUARIO IS NULL THEN 'NAO'
								ELSE 'SIM'
						   END) AS BLOQUEIO_RESTRICAO,
						   
						   (SELECT COUNT(*) 
							FROM CADBIBLIOTECACOMITE com
							INNER JOIN USUARIO usr ON (com.IDUSUARIO = usr.IDUSUARIO)
							WHERE com.IDBIBLIOTECA = CB.IDUSUARIO) AS COMITE,
							
						   (SELECT COUNT(*) 
							FROM CADBIBLIOTECACOMITE com
							INNER JOIN USUARIO usr ON (com.IDUSUARIO = usr.IDUSUARIO)
							WHERE com.IDBIBLIOTECA = CB.IDUSUARIO AND usr.ATIVO = 'S') AS COMITE_ATIVO,
							
							(SELECT (CASE 
								WHEN COUNT(*) = 0 THEN 'NAO'
								ELSE 'SIM'
						   END) AS BLOQ 
							FROM LOG_GERENCIAMENTO LG
							INNER JOIN USUARIO USR ON (LG.IDBIBLIOTECA = USR.IDUSUARIO)
							WHERE LG.IDBIBLIOTECA = CB.IDUSUARIO) AS BLOQUEIO_LOGGERENCIAMENTO,
							
							(SELECT VLRCREDITO_INICIAL
							FROM CREDITO CR WHERE CR.IDUSUARIO = CB.IDUSUARIO AND CR.IDPROGRAMA = 1) AS VLRCREDITO_INICIAL_3ED,

							(SELECT VLRCREDITO_INICIAL
							FROM CREDITO CR WHERE CR.IDUSUARIO = CB.IDUSUARIO AND CR.IDPROGRAMA = 2) AS VLRCREDITO_INICIAL_4ED,
							
						   (CASE WHEN C.IDCIDADESUB = 0 THEN 
                            C.IDCIDADE 
                        ELSE C.IDCIDADESUB END) AS IDCIDADEMASTER,
						CONCAT(CONCAT(L.NOMELOGRADOURO, ', ', B.NOMEBAIRRO), ' - CEP: ', L.CEP) AS LOGRADOURO,
						L.IDLOGRADOURO 
                        
                        FROM USUARIO				  U
                        INNER JOIN CADBIBLIOTECA	  CB  ON (CB.IDUSUARIO	 = U.IDUSUARIO)
                        LEFT JOIN  CADBIBLIOTECAFINAN CF  ON (CF.IDUSUARIO   = U.IDUSUARIO)
                        LEFT JOIN  CREDITO			  CR  ON (CR.IDUSUARIO	 = U.IDUSUARIO)
                        LEFT JOIN  CREDITORESTRICAO	  CRI ON (CRI.IDUSUARIO	 = U.IDUSUARIO)
                        INNER JOIN LOGRADOURO		  L   ON (L.IDLOGRADOURO = CB.IDLOGRADOURO)
                        INNER JOIN CIDADE			  C   ON (C.IDCIDADE	 = L.IDCIDADE)
						left join  bairro             B   ON (B.IDBAIRRO     = L.IDBAIRRO)
                        WHERE CB.IDUSUARIOPAI = " . $this->IDUSUARIO;
		
			// Prossegue o sql, adicionando os orders by
			// $sql .= " ORDER BY X.IDCIDADEMASTER, X.CPF_CNPJ LIMIT " . $arrParam['limit_de'] . "," . $arrParam['exibir_pp'];

			$dados  = $this->db->query($sql);
			$result = $dados->result_array();
		}
        return $result;
	}
    
    function getCidadesCadbiblioteca($uf=''){

    	// pega as cidades que possuem 2 ou mais bibliotecas cadastradas
        // através do município
        
        $arrDados = array();
            
        $sql = "SELECT 
                        (CASE WHEN C2.IDCIDADESUB = 0 THEN C2.IDCIDADE ELSE C2.IDCIDADESUB END) AS IDCIDADE, C2.NOMECIDADE
                              FROM CIDADE C2 
                              INNER JOIN LOGRADOURO L ON (L.IDCIDADE = C2.IDCIDADE)
                              INNER JOIN CADBIBLIOTECA CB ON (CB.IDLOGRADOURO = L.IDLOGRADOURO)
                              GROUP BY (CASE WHEN C2.IDCIDADESUB = 0 THEN C2.IDCIDADE ELSE C2.IDCIDADESUB END), C2.IDUF
                              HAVING COUNT(CB.IDUSUARIO) > 1";

        if ($uf != "") $sql .= " AND C2.IDUF = '$uf'";
        
        $sql .= " ORDER BY C2.IDUF, C2.NOMECIDADE";
        
		$arrDados = $this->db->query($sql);
        $rs3 = $arrDados->result_array();
        
        return $rs3;
    }
 
    function getBibiotecasSemPaiByCidade($cidade = '', $idbiblioteca = '') {

    	$arrDados = array();
            
        $sql = "SELECT CB.IDUSUARIO FROM CADBIBLIOTECA CB WHERE IDUSUARIOPAI IS NULL AND CB.IDLOGRADOURO IN (
        			SELECT L.IDLOGRADOURO 
        			FROM LOGRADOURO L INNER JOIN CIDADE C ON (L.IDCIDADE = C.IDCIDADE)
        			WHERE (C.IDCIDADE = $cidade OR C.IDCIDADESUB = $cidade)
        		) AND NOT (CB.IDUSUARIO = $idbiblioteca)";
        
		$arrDados = $this->db->query($sql);
        $rs3 = $arrDados->result_array();
        
        return $rs3;
    }
    
    function listaCadbiblioteca($arrParam, $onlyCount = FALSE, $filtros = null){
        
        $this->load->model('logradouro_model');
        $this->load->model('uf_model');
        $this->load->model('global_model');

        // primeiro passo: pega as cidades que possuem 2 ou mais bibliotecas cadastradas
        // através do município
		$dbcidades = $this->getCidadesCadbiblioteca();
		
        $arrCid = array();
        
        foreach ($dbcidades as $c) {
            array_push($arrCid, $c['IDCIDADE']);
        }
        
        $cidades = implode(',', $arrCid);
        
		$cidadeRelacao = '';
		
		// segundo passo: busca as bibliotecas
		if ($onlyCount)
		{
			$sql = "SELECT COUNT(DISTINCT X.IDUSUARIO) AS CONT FROM (SELECT CB.IDUSUARIO, CB.RAZAOSOCIAL, CB.NOMEFANTASIA, U.CPF_CNPJ, CB.NOMERESP, C.IDUF, C.NOMECIDADE, 
                        (CASE WHEN C.IDCIDADESUB = 0 THEN 
                            C.IDCIDADE 
                        ELSE C.IDCIDADESUB END) AS IDCIDADEMASTER
                        FROM USUARIO			 U
                        INNER JOIN CADBIBLIOTECA CB ON (CB.IDUSUARIO = U.IDUSUARIO)
                        INNER JOIN LOGRADOURO    L  ON (L.IDLOGRADOURO = CB.IDLOGRADOURO)
                        INNER JOIN CIDADE        C  ON (C.IDCIDADE = L.IDCIDADE)
                        ) X
                        WHERE X.IDCIDADEMASTER IN  
                        ( 
                            $cidades
                        )";

		}
		else
		{
			$sql = "SELECT DISTINCT * FROM
					(SELECT CB.IDUSUARIO, CB.IDUSUARIOPAI, CB.RAZAOSOCIAL, CB.NOMEFANTASIA,
					 U.CPF_CNPJ, CB.NOMERESP, C.IDUF, C.NOMECIDADE,
						  (CASE 
								WHEN CF.IDUSUARIO IS NULL THEN 'NAO'
								ELSE 'SIM'
						   END) AS RESPONSAVEL_FINANCEIRO,
						
						(CASE 
								WHEN U.HABILITADO = 'S' THEN 'SIM'
								ELSE 'NAO'
						   END) AS USER_HABILITADO,
						   
						   (CASE 
								WHEN U.ATIVO = 'S' THEN 'SIM'
								ELSE 'NAO'
						   END) AS DESC_STATUS,

						   (CASE 
								WHEN CRI.IDUSUARIO IS NULL THEN 'NAO'
								ELSE 'SIM'
						   END) AS BLOQUEIO_RESTRICAO,
						   
						   (SELECT COUNT(*) 
							FROM CADBIBLIOTECACOMITE com
							INNER JOIN USUARIO usr ON (com.IDUSUARIO = usr.IDUSUARIO)
							WHERE com.IDBIBLIOTECA = CB.IDUSUARIO AND usr.ATIVO = 'S') AS COMITE,

							(SELECT (CASE 
								WHEN COUNT(*) = 0 THEN 'NAO'
								ELSE 'SIM'
						   END) AS BLOQ 
							FROM LOG_GERENCIAMENTO LG
							INNER JOIN USUARIO USR ON (LG.IDBIBLIOTECA = USR.IDUSUARIO)
							WHERE LG.IDBIBLIOTECA = CB.IDUSUARIO) AS BLOQUEIO_LOGGERENCIAMENTO,
							
							(SELECT VLRCREDITO_INICIAL
							FROM CREDITO CR WHERE CR.IDUSUARIO = CB.IDUSUARIO AND CR.IDPROGRAMA = 1) AS VLRCREDITO_INICIAL_3ED,

							(SELECT VLRCREDITO_INICIAL
							FROM CREDITO CR WHERE CR.IDUSUARIO = CB.IDUSUARIO AND CR.IDPROGRAMA = 2) AS VLRCREDITO_INICIAL_4ED,
							
						   (CASE WHEN C.IDCIDADESUB = 0 THEN 
                            C.IDCIDADE 
                        ELSE C.IDCIDADESUB END) AS IDCIDADEMASTER
                        
                        FROM USUARIO				  U
                        INNER JOIN CADBIBLIOTECA	  CB  ON (CB.IDUSUARIO	 = U.IDUSUARIO)
                        LEFT JOIN  CADBIBLIOTECAFINAN CF  ON (CF.IDUSUARIO   = U.IDUSUARIO)
                        LEFT JOIN  CREDITO			  CR  ON (CR.IDUSUARIO	 = U.IDUSUARIO)
                        LEFT JOIN  CREDITORESTRICAO	  CRI ON (CRI.IDUSUARIO	 = U.IDUSUARIO)
                        INNER JOIN LOGRADOURO		  L   ON (L.IDLOGRADOURO = CB.IDLOGRADOURO)
                        INNER JOIN CIDADE			  C   ON (C.IDCIDADE	 = L.IDCIDADE)
                        ) X
                        WHERE X.IDCIDADEMASTER IN  
                        ( 
                            $cidades
                        )";
		}
        
		// terceiro passo: adiciona os filtros
		// Filtro para cidade
		if($this->CIDADE != '' && $this->CIDADE != '-- Selecione um Estado --')
		{
			$cidadeRelacao = '';	
			$sql3 = "select IDCIDADE from binac.cidade where IDCIDADE = '$this->CIDADE' OR IDCIDADESUB = '$this->CIDADE'";	
			$dados = $this->db->query($sql3);
			$rs3 = $dados->result_array();

			if (count($rs3) > 0) {
				$cidadeRelacao = $rs3[0]['IDCIDADE'];
				//$ufRelacao_tratado = removeAcentos($ufRelacao->NOMEUF , true);
				$sql .= " AND ( IDCIDADEMASTER = '".$this->CIDADE."')";
			}
		}
		
		// Filtro para estado
		if($this->UF != '')
		{
			$ufRelacao = '';	
			$sql2 = "select NOMEUF from binac.uf  where iduf = '$this->UF' ";
			$dados = $this->db->query($sql2);
			$rsl = $dados->result_array();
			$ufRelacao = $rsl[0]['NOMEUF'];
			//$ufRelacao_tratado = removeAcentos($ufRelacao->NOMEUF , true);
			$sql .= " AND ( X.IDUF = '".$this->UF."' OR X.IDUF = '".$ufRelacao."')";
		}
		
       	// Finaliza SQL, somente para casos onde nao seja count
		if (!$onlyCount)
		{
			// Prossegue o sql
			$sql .= " ORDER BY X.IDCIDADEMASTER, X.CPF_CNPJ LIMIT " . $arrParam['limit_de'] . "," . $arrParam['exibir_pp'];
		}

		$dados = $this->db->query($sql);
        return $dados->result_array();
	}
    

    //COPIA DE LISTA
	function listaSniic($arrParam, $onlyCount = FALSE, $filtros = null)
	{
		if ($onlyCount)
		{
			$sql = " SELECT count(*) as CONT
					FROM 
					(SELECT  
						IDUF                AS UF,
						NOMECIDADE          AS CIDADE,
						u.CPF_CNPJ          AS CPF_CNPJ,
						cb.RAZAOSOCIAL      AS NOME_BIBLIOTECA,
						u.ATIVO             AS ATIVO,
						'S'                 AS HABILITADO,

						(SELECT CASE WHEN (COUNT(*) = 0) THEN 'N' ELSE 'S' END AS RESP 
							FROM binac.cadbibliotecafinan cbf
							INNER JOIN binac.usuario usrf ON (cbf.IDUSUARIO = usrf.IDUSUARIO)
							WHERE cbf.IDBIBLIOTECA = cb.IDUSUARIO AND usrf.ATIVO = 'S')
							
						AS RESPONSAVEL_FINANCEIRO,
									   
						(SELECT COUNT(*)
							FROM binac.cadbibliotecacomite com
							INNER JOIN binac.usuario usr ON (com.IDUSUARIO = usr.IDUSUARIO)
							WHERE com.IDBIBLIOTECA = cb.IDUSUARIO AND usr.ATIVO = 'S')
							AS COMITE,
                                                        
						'NOVO'              AS TIPO_CAD,                
						cb.IDUSUARIO        AS COD
                                                
					FROM binac.cadbiblioteca cb 
					INNER JOIN binac.usuario u ON (cb.IDUSUARIO = U.IDUSUARIO)
					INNER JOIN binac.logradouro l ON (cb.IDLOGRADOURO = l.IDLOGRADOURO)
					INNER JOIN binac.cidade c ON (l.IDCIDADE = c.IDCIDADE)

					UNION ALL

					SELECT 
						sb.ENDER_ESTADO        AS UF,
						sb.ENDER_MUNICIPIO     AS CIDADE,
						sb.CNPJ_CPF            AS CPF_CNPJ,
						sb.NOME_BIBLIOTECA     AS NOME_BIBLIOTECA,
						'N'                    AS ATIVO,
						'S'                    AS HABILITADO,
						'N'                    AS RESPONSAVEL_FINANCEIRO,
						0                      AS COMITE,
						'ANTIGO'               AS TIPO_CAD,                
						sb.ID_BIBLIOTECA       AS COD
                                                
					FROM sniiconline.sniic_biblioteca sb
					WHERE NOT EXISTS (SELECT 1 FROM binac.cadbiblioteca cb WHERE cb.IDSNIIC = sb.ID_BIBLIOTECA)
					) x ";
		}
		else
		{
			$sql = "SELECT 
						UF,
						NOMEUF,
						CIDADE,
						CPF_CNPJ,
						NOME_BIBLIOTECA,
						ATIVO,
						HABILITADO,
						RESPONSAVEL_FINANCEIRO,
						COMITE,
						TIPO_CAD,                
						COD,
						CHAVE_VALIDACAO,
						VLRCREDITO_INICIAL_3ED,
						VLRCREDITO_INICIAL_4ED
						FROM 
                                        
					(SELECT  
						uf.IDUF             AS UF,
						NOMEUF,
						NOMECIDADE          AS CIDADE,
						u.CPF_CNPJ          AS CPF_CNPJ,
						cb.RAZAOSOCIAL      AS NOME_BIBLIOTECA,
						u.ATIVO             AS ATIVO,
						u.HABILITADO		AS HABILITADO,

                                                    (SELECT CASE WHEN (COUNT(*) = 0) THEN 'N' ELSE 'S' END AS RESPONSAVEL_FINANCEIRO 
							FROM binac.cadbibliotecafinan cbf
							INNER JOIN binac.usuario usrf ON (cbf.IDUSUARIO = usrf.IDUSUARIO)
							WHERE cbf.IDBIBLIOTECA = cb.IDUSUARIO AND usrf.ATIVO = 'S')
							
						AS RESPONSAVEL_FINANCEIRO,
									   
						(SELECT COUNT(*) 
							FROM binac.cadbibliotecacomite com
							INNER JOIN binac.usuario usr ON (com.IDUSUARIO = usr.IDUSUARIO)
							WHERE com.IDBIBLIOTECA = cb.IDUSUARIO AND usr.ATIVO = 'S')
							
                                                                    AS COMITE,
						'NOVO'              AS TIPO_CAD,                
						cb.IDUSUARIO        AS COD,
						''                  AS CHAVE_VALIDACAO,
						
						(SELECT VLRCREDITO_INICIAL
							FROM binac.CREDITO CR WHERE CR.IDUSUARIO = CB.IDUSUARIO AND CR.IDPROGRAMA = 1) AS VLRCREDITO_INICIAL_3ED,

						(SELECT VLRCREDITO_INICIAL
							FROM binac.CREDITO CR WHERE CR.IDUSUARIO = CB.IDUSUARIO AND CR.IDPROGRAMA = 2) AS VLRCREDITO_INICIAL_4ED
							                                                
					FROM binac.cadbiblioteca cb 
					INNER JOIN binac.usuario u ON (cb.IDUSUARIO = U.IDUSUARIO)
					INNER JOIN binac.logradouro l ON (cb.IDLOGRADOURO = l.IDLOGRADOURO)
					INNER JOIN binac.cidade c ON (l.IDCIDADE = c.IDCIDADE)
					INNER JOIN binac.uf ON (c.IDUF = uf.IDUF)

					UNION ALL

					SELECT 
						sb.ENDER_ESTADO        AS UF,
						upper(sb.ENDER_ESTADO) AS NOMEUF,
						sb.ENDER_MUNICIPIO     AS CIDADE,
						sb.CNPJ_CPF            AS CPF_CNPJ,
						sb.NOME_BIBLIOTECA     AS NOME_BIBLIOTECA,
						'-'                    AS ATIVO,
						'-'                    AS HABILITADO,
						'-'                    AS RESPONSAVEL_FINANCEIRO,
						'-'                    AS COMITE,
						'ANTIGO'               AS TIPO_CAD,
						sb.ID_BIBLIOTECA       AS COD,
						sb.CHAVE_VALIDACAO     AS CHAVE_VALIDACAO,
						
						(SELECT VLRCREDITO_INICIAL
							FROM sniiconline.CREDITO CR WHERE CR.ID_BIBLIOTECA = SB.ID_BIBLIOTECA AND CR.IDPROGRAMA = 1) AS VLRCREDITO_INICIAL_3ED,

						(SELECT VLRCREDITO_INICIAL
							FROM sniiconline.CREDITO CR WHERE CR.ID_BIBLIOTECA = SB.ID_BIBLIOTECA AND CR.IDPROGRAMA = 2) AS VLRCREDITO_INICIAL_4ED
                                                
					FROM sniiconline.sniic_biblioteca sb
					WHERE NOT EXISTS (SELECT 1 FROM binac.cadbiblioteca cb WHERE cb.IDSNIIC = sb.ID_BIBLIOTECA)
					) x ";
		}
		
		// Filtragem - Serve tanto para o count quanto para dados de listagem
		
		
		// Adiciona um Where 1=1 para que os filtros abaixo possam ser utilizados
		$sql .= ' WHERE 1=1 ';	
		
		// Filtro para cidade
		//echo "<script>alert('".$this->CIDADE."')</script>";
		if($this->CIDADE != '' && $this->CIDADE != '-- Selecione um Estado --')
		{
			$cidadeRelacao = '';	
			$sql3 = "select NOMECIDADE from binac.cidade where nomecidade = '$this->CIDADE' ";	
			$dados = $this->db->query($sql3);
			$rs3 = $dados->result_array();
			
			if (count($rs3) > 0) {
				$cidadeRelacao = $rs3[0]['NOMECIDADE'];
				//$ufRelacao_tratado = removeAcentos($ufRelacao->NOMEUF , true);
				$sql .= " AND ( CIDADE = '".$this->CIDADE."' OR CIDADE = '".$cidadeRelacao."')";
			}
		}
		
		// Filtro para estado
		if($this->UF != '')
		{
			$ufRelacao = '';	
			$sql2 = "select NOMEUF from binac.uf  where iduf = '$this->UF' ";
			$dados = $this->db->query($sql2);
			$rsl = $dados->result_array();
			$ufRelacao = $rsl[0]['NOMEUF'];
			//$ufRelacao_tratado = removeAcentos($ufRelacao->NOMEUF , true);
			$sql .= " AND ( UF = '".$this->UF."' OR UF = '".$ufRelacao."')";
		}
		
		// Filtro para Razao e Nome Fantasia
		if($this->NOMEFANTASIA != '' && $this->RAZAOSOCIAL != '')
		{
			$sql .= " AND (x.NOME_BIBLIOTECA LIKE '%" . $this->RAZAOSOCIAL . "%' OR x.NOME_BIBLIOTECA LIKE '%" . $this->NOMEFANTASIA . "%')";
		} else {
			// Filtro para RazÃ£o social
			if($this->RAZAOSOCIAL != '')
			{
				$sql .= " AND x.NOME_BIBLIOTECA LIKE '%" . $this->RAZAOSOCIAL . "%' ";
			}
			
			// Filtro para Nome Fantasia
			if($this->NOMEFANTASIA != '')
			{
				$sql .= " AND x.NOME_BIBLIOTECA LIKE '%" . $this->NOMEFANTASIA . "%' ";
			}
		}
		
		if($this->CPFCNPJ != '')
		{
			$sql .= "  and x.CPF_CNPJ = '". $this->CPFCNPJ ."' ";
		}
		
		if($this->TIPOCAD != '')
		{
			$sql .= "  and x.TIPO_CAD = '". $this->TIPOCAD ."' ";
		}
		
		
		// Finaliza SQL, somente para casos onde nao seja count
		if (!$onlyCount)
		{
			// Prossegue o sql
			$sql .= " ORDER BY x.NOMEUF, x.CIDADE, x.CPF_CNPJ LIMIT " . $arrParam['limit_de'] . "," . $arrParam['exibir_pp'];
		}
		
		$dados = $this->db->query($sql);
		return $dados->result_array();
	}

    //COPIA DE LISTA
	function listaSniic2($arrParam, $onlyCount = FALSE, $filtros = null)
	{
		if ($onlyCount)
		{
			$sql = " SELECT count(*) as CONT
					FROM 
					(SELECT  
						IDUF                AS UF,
						NOMECIDADE          AS CIDADE,
						u.CPF_CNPJ          AS CPF_CNPJ,
						cb.RAZAOSOCIAL      AS NOME_BIBLIOTECA,
						u.ATIVO             AS ATIVO,
						'S'                 AS HABILITADO,

						(SELECT CASE WHEN (COUNT(*) = 0) THEN 'N' ELSE 'S' END AS RESP 
							FROM binac.cadbibliotecafinan cbf
							INNER JOIN binac.usuario usrf ON (cbf.IDUSUARIO = usrf.IDUSUARIO)
							WHERE cbf.IDBIBLIOTECA = cb.IDUSUARIO AND usrf.ATIVO = 'S')
							
						AS RESPONSAVEL_FINANCEIRO,
									   
						(SELECT COUNT(*)
							FROM binac.cadbibliotecacomite com
							INNER JOIN binac.usuario usr ON (com.IDUSUARIO = usr.IDUSUARIO)
							WHERE com.IDBIBLIOTECA = cb.IDUSUARIO AND usr.ATIVO = 'S')
							AS COMITE,
                                                        
						'NOVO'              AS TIPO_CAD,                
						cb.IDUSUARIO        AS COD
                                                
					FROM binac.cadbiblioteca cb 
					INNER JOIN binac.usuario u ON (cb.IDUSUARIO = U.IDUSUARIO)
					INNER JOIN binac.logradouro l ON (cb.IDLOGRADOURO = l.IDLOGRADOURO)
					INNER JOIN binac.cidade c ON (l.IDCIDADE = c.IDCIDADE)
					WHERE cb.idusuariopai IS NULL

					UNION ALL

					SELECT DISTINCT
						sb.ENDER_ESTADO        AS UF,
						sb.ENDER_MUNICIPIO     AS CIDADE,
						sb.CNPJ_CPF            AS CPF_CNPJ,
						sb.NOME_BIBLIOTECA     AS NOME_BIBLIOTECA,
						'N'                    AS ATIVO,
						'S'                    AS HABILITADO,
						'N'                    AS RESPONSAVEL_FINANCEIRO,
						0                      AS COMITE,
						'ANTIGO'               AS TIPO_CAD,                
						sb.ID_BIBLIOTECA       AS COD
                                                
					FROM sniiconline.sniic_biblioteca sb
					INNER JOIN sniiconline.credito sc ON sb.id_biblioteca = sc.id_biblioteca
					WHERE NOT EXISTS (SELECT 1 FROM binac.cadbiblioteca cb WHERE cb.IDSNIIC = sb.ID_BIBLIOTECA)
					) x 
					WHERE X.CIDADE IN (
						SELECT DISTINCT sb.ender_municipio
						FROM sniiconline.sniic_biblioteca sb 
						inner join sniiconline.credito sc on sb.id_biblioteca = sc.id_biblioteca
						where not exists(SELECT 1 FROM binac.cadbiblioteca cb WHERE cb.IDSNIIC = sb.ID_BIBLIOTECA)
					)";
		}
		else
		{
			$sql = "SELECT
						UF,
						NOMEUF,
						CIDADE,
						CPF_CNPJ,
						NOME_BIBLIOTECA,
						ATIVO,
						HABILITADO,
						RESPONSAVEL_FINANCEIRO,
						COMITE,
						TIPO_CAD,                
						COD,
						CHAVE_VALIDACAO,
						VLRCREDITO_INICIAL_3ED,
						VLRCREDITO_INICIAL_4ED
						
					FROM 
                                        
					(SELECT  
						uf.IDUF             AS UF,
						NOMEUF,
						NOMECIDADE          AS CIDADE,
						u.CPF_CNPJ          AS CPF_CNPJ,
						cb.RAZAOSOCIAL      AS NOME_BIBLIOTECA,
						u.ATIVO             AS ATIVO,
						u.HABILITADO		AS HABILITADO,

                        (SELECT CASE WHEN (COUNT(*) = 0) THEN 'N' ELSE 'S' END AS RESPONSAVEL_FINANCEIRO 
							FROM binac.cadbibliotecafinan cbf
							INNER JOIN binac.usuario usrf ON (cbf.IDUSUARIO = usrf.IDUSUARIO)
							WHERE cbf.IDBIBLIOTECA = cb.IDUSUARIO AND usrf.ATIVO = 'S')
							
						AS RESPONSAVEL_FINANCEIRO,
									   
						(SELECT COUNT(*) 
							FROM binac.cadbibliotecacomite com
							INNER JOIN binac.usuario usr ON (com.IDUSUARIO = usr.IDUSUARIO)
							WHERE com.IDBIBLIOTECA = cb.IDUSUARIO AND usr.ATIVO = 'S')
							
                                                                    AS COMITE,
						'NOVO'              AS TIPO_CAD,                
						cb.IDUSUARIO        AS COD,
						''                  AS CHAVE_VALIDACAO,
						
						(SELECT VLRCREDITO_INICIAL
							FROM binac.CREDITO CR WHERE CR.IDUSUARIO = CB.IDUSUARIO AND CR.IDPROGRAMA = 1) AS VLRCREDITO_INICIAL_3ED,

						(SELECT VLRCREDITO_INICIAL
							FROM binac.CREDITO CR WHERE CR.IDUSUARIO = CB.IDUSUARIO AND CR.IDPROGRAMA = 2) AS VLRCREDITO_INICIAL_4ED
                                                
					FROM binac.cadbiblioteca cb 
					INNER JOIN binac.usuario u ON (cb.IDUSUARIO = U.IDUSUARIO)
					INNER JOIN binac.logradouro l ON (cb.IDLOGRADOURO = l.IDLOGRADOURO)
					INNER JOIN binac.cidade c ON (l.IDCIDADE = c.IDCIDADE)
					INNER JOIN binac.uf ON (c.IDUF = uf.IDUF)
					WHERE cb.idusuariopai IS NULL

					UNION ALL

					SELECT DISTINCT
						sb.ENDER_ESTADO        AS UF,
						upper(sb.ENDER_ESTADO) AS NOMEUF,
						sb.ENDER_MUNICIPIO     AS CIDADE,
						sb.CNPJ_CPF            AS CPF_CNPJ,
						sb.NOME_BIBLIOTECA     AS NOME_BIBLIOTECA,
						'N'                    AS ATIVO,
						'N'                    AS HABILITADO,
						'N'                    AS RESPONSAVEL_FINANCEIRO,
						0                      AS COMITE,
						'ANTIGO'               AS TIPO_CAD,
						sb.ID_BIBLIOTECA       AS COD,
						sb.CHAVE_VALIDACAO     AS CHAVE_VALIDACAO,
						
						(SELECT VLRCREDITO_INICIAL
							FROM sniiconline.CREDITO CR WHERE CR.ID_BIBLIOTECA = SB.ID_BIBLIOTECA AND CR.IDPROGRAMA = 1) AS VLRCREDITO_INICIAL_3ED,

						(SELECT VLRCREDITO_INICIAL
							FROM sniiconline.CREDITO CR WHERE CR.ID_BIBLIOTECA = SB.ID_BIBLIOTECA AND CR.IDPROGRAMA = 2) AS VLRCREDITO_INICIAL_4ED
                                                
					FROM sniiconline.sniic_biblioteca sb
					INNER JOIN sniiconline.credito sc ON sb.id_biblioteca = sc.id_biblioteca
					WHERE NOT EXISTS (SELECT 1 FROM binac.cadbiblioteca cb WHERE cb.IDSNIIC = sb.ID_BIBLIOTECA)
					) x 
					WHERE X.CIDADE IN (
						SELECT DISTINCT sb.ender_municipio
						FROM sniiconline.sniic_biblioteca sb 
						inner join sniiconline.credito sc on sb.id_biblioteca = sc.id_biblioteca
						where not exists(SELECT 1 FROM binac.cadbiblioteca cb WHERE cb.IDSNIIC = sb.ID_BIBLIOTECA)
					)
					";
		}
		
		// Filtragem - Serve tanto para o count quanto para dados de listagem
		
		
		// Adiciona um Where 1=1 para que os filtros abaixo possam ser utilizados
		//$sql .= ' WHERE 1=1 ';	
		
		// Filtro para cidade
		//echo "<script>alert('".$this->CIDADE."')</script>";
		if($this->CIDADE != '' && $this->CIDADE != '-- Selecione um Estado --')
		{
			$cidadeRelacao = '';	
			$sql3 = "select NOMECIDADE from binac.cidade where idcidade = '$this->CIDADE' ";	
			$dados = $this->db->query($sql3);
			$rs3 = $dados->result_array();
			
			if (count($rs3) > 0) {
				$cidadeRelacao = $rs3[0]['NOMECIDADE'];
				//$ufRelacao_tratado = removeAcentos($ufRelacao->NOMEUF , true);
				$sql .= " AND ( CIDADE = '".$this->CIDADE."' OR CIDADE = '".$cidadeRelacao."')";
			}
		}
		
		// Filtro para estado
		if($this->UF != '')
		{
			$ufRelacao = '';	
			$sql2 = "select NOMEUF from binac.uf  where iduf = '$this->UF' ";
			$dados = $this->db->query($sql2);
			$rsl = $dados->result_array();
			$ufRelacao = $rsl[0]['NOMEUF'];
			//$ufRelacao_tratado = removeAcentos($ufRelacao->NOMEUF , true);
			$sql .= " AND ( UF = '".$this->UF."' OR UF = '".$ufRelacao."')";
		}
		
		// Filtro para Razao e Nome Fantasia
		if($this->NOMEFANTASIA != '' && $this->RAZAOSOCIAL != '')
		{
			$sql .= " AND (x.NOME_BIBLIOTECA LIKE '%" . $this->RAZAOSOCIAL . "%' OR x.NOME_BIBLIOTECA LIKE '%" . $this->NOMEFANTASIA . "%')";
		} else {
			// Filtro para RazÃ£o social
			if($this->RAZAOSOCIAL != '')
			{
				$sql .= " AND x.NOME_BIBLIOTECA LIKE '%" . $this->RAZAOSOCIAL . "%' ";
			}
			
			// Filtro para Nome Fantasia
			if($this->NOMEFANTASIA != '')
			{
				$sql .= " AND x.NOME_BIBLIOTECA LIKE '%" . $this->NOMEFANTASIA . "%' ";
			}
		}
		
		if($this->CPFCNPJ != '')
		{
			$sql .= "  and x.CPF_CNPJ = '". $this->CPFCNPJ ."' ";
		}
		
		if($this->TIPOCAD != '')
		{
			$sql .= "  and x.TIPO_CAD = '". $this->TIPOCAD ."' ";
		}
		
		
		// Finaliza SQL, somente para casos onde nao seja count
		if (!$onlyCount)
		{
			// Prossegue o sql
			$sql .= " ORDER BY x.NOMEUF, x.CIDADE, x.CPF_CNPJ LIMIT " . $arrParam['limit_de'] . "," . $arrParam['exibir_pp'];
			//echo $sql;
		}

		$dados = $this->db->query($sql);
		return $dados->result_array();
	}
	
	function setFiltro($coluna = null, $valor = null)
	{
		if(!is_null($coluna) && !is_null($valor))
		{
			$this->{strtoupper($coluna)} = addslashes($valor);
		}
	}
	
	function getArrayFiltros($arrDados = null)
	{
		$dados = array();
		if(!is_null($arrDados) && is_array($arrDados))
		{
			$dados['filtro_status']	= (isset($arrDados['filtro_status'])) ? $arrDados['filtro_status'] : '';
			$dados['filtro_habilitado']	= (isset($arrDados['filtro_habilitado'])) ? $arrDados['filtro_habilitado'] : '';
			$dados['filtro_razao_nome_fantasia'] = (isset($arrDados['filtro_razao_nome_fantasia'])) ? $arrDados['filtro_razao_nome_fantasia'] : '';
			$dados['filtro_cidade']	= (isset($arrDados['filtro_cidade'])) ? $arrDados['filtro_cidade'] : '';
			$dados['filtro_estado']	= (isset($arrDados['filtro_estado'])) ? $arrDados['filtro_estado'] : '';
			$dados['filtro_bibpai'] = (isset($arrDados['filtro_bibpai'])) ? $arrDados['filtro_bibpai'] : '';
		} else {
			$dados['filtro_status']	= '';
			$dados['filtro_habilitado']	= '';
			$dados['filtro_razao_nome_fantasia'] = '';
			$dados['filtro_cidade']	= '';
			$dados['filtro_estado']	= '';
			$dados['filtro_bibpai'] = '';
		}
		return $dados;
	}
	function getArrayFiltrosSniic($arrDados = null)
	{
		$dados = array();
		if(!is_null($arrDados) && is_array($arrDados))
		{
			$dados['filtro_razao_nome_fantasia']	= $arrDados['filtro_razao_nome_fantasia'];
			$dados['filtro_cidade']					= $arrDados['filtro_cidade'];
			$dados['filtro_estado']					= $arrDados['filtro_estado'];
			$dados['filtro_cpf_cnpj']				= $arrDados['filtro_cpf_cnpj'];
			$dados['filtro_tipo_cad']				= $arrDados['filtro_tipo_cad'];
		} else {
			$dados['filtro_razao_nome_fantasia']	= '';
			$dados['filtro_cidade']					= '';
			$dados['filtro_estado']					= '';
			$dados['filtro_cpf_cnpj']				= '';
			$dados['filtro_tipo_cad']				= '';
		}
		return $dados;
	}
	
	function getDadosCadastro($idUsuario)
	{
		$sql = sprintf("SELECT
							u.*,
							cb.*,
							l.CEP,
							c.IDUF,
							c.IDCIDADE,
							c.NOMECIDADESUB,
							b.NOMEBAIRRO,
							l.NOMELOGRADOURO,
							CONCAT(t.NOMELOGRADOUROTIPO,' ',l.NOMELOGRADOURO) AS NOMELOGRADOURO2,
							l.COMPLEMENTO
						FROM usuario u
						INNER JOIN cadbiblioteca cb ON (u.IDUSUARIO = cb.IDUSUARIO)
						LEFT JOIN logradouro     l  ON (l.IDLOGRADOURO = cb.IDLOGRADOURO)
						LEFT JOIN logradourotipo t  ON (t.IDLOGRADOUROTIPO = l.IDLOGRADOUROTIPO)
						LEFT JOIN bairro         b  ON (b.IDBAIRRO = l.IDBAIRRO)
						LEFT JOIN cidade         c  ON (c.IDCIDADE = b.IDCIDADE)
						WHERE u.IDUSUARIO = %s", $idUsuario);
						
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
    
    function getDadosCadastroFinan($idUsuario = 0, $idprograma = 0)
	{
		$sql = "SELECT u.*,					
					   cbf.*,
					   l.CEP,
					   c.IDUF,
					   c.IDCIDADE,
					   c.NOMECIDADESUB,
					   b.NOMEBAIRRO,
					   cbf.IDAGENCIA,
					   l.NOMELOGRADOURO,
					   CONCAT(t.NOMELOGRADOUROTIPO,' ',l.NOMELOGRADOURO) AS NOMELOGRADOURO2,
					   l.COMPLEMENTO
				  FROM usuario u
			INNER JOIN cadbibliotecafinan cbf ON (u.IDUSUARIO = cbf.IDUSUARIO)
			 LEFT JOIN logradouro          l   ON (l.IDLOGRADOURO = cbf.IDLOGRADOURO)
			 LEFT JOIN logradourotipo      t   ON (t.IDLOGRADOUROTIPO = l.IDLOGRADOUROTIPO)
			 LEFT JOIN bairro              b   ON (b.IDBAIRRO = l.IDBAIRRO)
			 LEFT JOIN cidade              c   ON (c.IDCIDADE = b.IDCIDADE)
				 WHERE cbf.IDBIBLIOTECA = $idUsuario AND cbf.IDPROGRAMA = $idprograma
				 AND u.ATIVO = 'S'";
		$dados = $this->db->query($sql);
		return $dados->result_array();
	}
    
	function getDadosCadastroFinanAtivo($idUsuario)
	{
		$sql = sprintf("SELECT
							u.*,
							cbf.*,
							l.CEP,
							c.IDUF,
							c.IDCIDADE,
							c.NOMECIDADESUB,
							b.NOMEBAIRRO,
							cbf.IDAGENCIA,
							l.NOMELOGRADOURO,
							CONCAT(t.NOMELOGRADOUROTIPO,' ',l.NOMELOGRADOURO) AS NOMELOGRADOURO2,
							l.COMPLEMENTO
						FROM usuario u
						INNER JOIN cadbibliotecafinan cbf ON (u.IDUSUARIO = cbf.IDUSUARIO)
						LEFT JOIN logradouro          l   ON (l.IDLOGRADOURO = cbf.IDLOGRADOURO)
						LEFT JOIN logradourotipo      t   ON (t.IDLOGRADOUROTIPO = l.IDLOGRADOUROTIPO)
						LEFT JOIN bairro              b   ON (b.IDBAIRRO = l.IDBAIRRO)
						LEFT JOIN cidade              c   ON (c.IDCIDADE = b.IDCIDADE)
						WHERE U.ativo = 'S' AND cbf.IDBIBLIOTECA = %s", $idUsuario);
						
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function getPdvParceiro($idPrograma, $idUsuario)
	{
		$sql = sprintf("SELECT
							pp.IDPDV,
							p.RAZAOSOCIAL,
							p.NOMEFANTASIA
						FROM programapdvparc pp
						INNER JOIN cadpdv p ON (pp.IDPDV = p.IDUSUARIO)
						WHERE pp.IDPROGRAMA = %s
						  AND pp.IDBIBLIOTECA = %s", $idPrograma, $idUsuario);
						
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function listaGeo()
	{
		$sql = "SELECT
					u.IDUSUARIO,
					p.END_NUMERO,
					p.END_COMPLEMENTO,
					l.CEP,
					c.IDUF,
					c.NOMECIDADESUB,
					b.NOMEBAIRRO,
					l.NOMELOGRADOURO,
					CONCAT(t.NOMELOGRADOUROTIPO,' ',l.NOMELOGRADOURO) AS NOMELOGRADOURO2,
					l.COMPLEMENTO
				FROM usuario u
				INNER JOIN cadbiblioteca p ON (u.IDUSUARIO = p.IDUSUARIO)
				LEFT JOIN logradouro     l ON (l.IDLOGRADOURO = p.IDLOGRADOURO)
				LEFT JOIN logradourotipo t ON (t.IDLOGRADOUROTIPO = l.IDLOGRADOUROTIPO)
				LEFT JOIN bairro         b ON (b.IDBAIRRO = l.IDBAIRRO)
				LEFT JOIN cidade         c ON (c.IDCIDADE = b.IDCIDADE)";
		
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function getCreditoAtual($idUsuario = null)
	{
		$return = 0;
		if(!is_null($idUsuario))
		{
			$sql = "SELECT VLRCREDITO FROM credito WHERE IDUSUARIO = $idUsuario AND IDPROGRAMA = " . IDPROGRAMA;
			$dados = $this->db->query($sql);
			$result = $dados->result_array();
			if(isset($result[0]['VLRCREDITO']))
			{
				$return = $result[0]['VLRCREDITO'];
			}
		}
		return $return;
	}
	
	function isHabilitado($idUsuario = null)
	{
		$return = false;
		if(!is_null($idUsuario))
		{
			// $sql = "SELECT HABILITADO FROM usuario WHERE IDUSUARIO = $idUsuario";
			$this->load->library('session');
			$sql = "SELECT IDUSUARIO FROM usuarioprograma WHERE IDUSUARIO = $idUsuario AND IDPROGRAMA = " . $this->session->userdata('programa');
			$dados = $this->db->query($sql);
			$result = $dados->result_array();
			if(isset($result[0]['IDUSUARIO']))
			{
				$return = ($result[0]['IDUSUARIO'] != '') ? true : false;
			}
		}
		return $return;
	}
	
	function isHabilitadoPrograma($idUsuario = null, $idPrograma = null)
	{
		$return = false;
		if(!is_null($idUsuario) && !is_null($idPrograma))
		{
			$sql = sprintf("SELECT COUNT(*) AS val FROM usuarioprograma WHERE IDUSUARIO = %d AND IDPROGRAMA = %d", $idUsuario, $idPrograma);
			$dados = $this->db->query($sql);
			$result = $dados->result_array();
			if(isset($result[0]['val']))
			{
				$return = ($result[0]['val'] == 0) ? false : true;
			}
		}
		return $return;
	}
	
	function isAtivo($idUsuario = null)
	{
		$return = false;
		if(!is_null($idUsuario))
		{
			$sql = "SELECT ATIVO FROM usuario WHERE IDUSUARIO = $idUsuario";
			$dados = $this->db->query($sql);
			$result = $dados->result_array();
			if(isset($result[0]['ATIVO']))
			{
				$return = ($result[0]['ATIVO'] != 'S') ? false : true;
			}
		}
		return $return;
	}
	
	function temFilhos($idUsuario = null)
	{
		$return = false;
		if(!is_null($idUsuario))
		{
			$sql = "SELECT COUNT(*) AS val FROM cadbiblioteca WHERE IDUSUARIOPAI = $idUsuario";
			$dados = $this->db->query($sql);
			$result = $dados->result_array();
			if(isset($result[0]['val']))
			{
				$return = ($result[0]['val'] == 0) ? false : true;
			}
		}
		return $return;
	}
	
	function listaFilhos($idUsuario = null)
	{
		$return = false;
		if(!is_null($idUsuario))
		{
			$sql = "SELECT IDUSUARIO FROM cadbiblioteca WHERE IDUSUARIOPAI = $idUsuario";
			$dados = $this->db->query($sql);
			$return = $dados->result_array();
		}
		return $return;
	}	
	
	function listaComite($idUsuario = 0, $idprograma = 0)
	{
		$sql = "SELECT u.*,
					   co.*,
					   CASE WHEN u.ATIVO = 'S' THEN 'v_peq.png'
					   ELSE 'x.png'	END AS IMGSTATUS,
					   CASE WHEN u.ATIVO = 'S' THEN 'VALIDADO'
					   ELSE 'NAO VALIDADO'
					   END AS DESC_STATUS
				  FROM usuario u
			INNER JOIN cadbibliotecacomite co ON (u.IDUSUARIO = co.IDUSUARIO)
			  	 WHERE co.IDBIBLIOTECA = $idUsuario AND co.IDPROGRAMA = $idprograma
				   AND (u.ATIVO = 'S' OR (u.ATIVO = 'N' AND u.DATA_ATIVACAO IS NULL))
			  ORDER BY co.RAZAOSOCIAL, co.NOMERESP";
		$dados = $this->db->query($sql);
		return $dados->result_array();
	}
	
	function listaComiteTodos($idUsuario)
	{
		$sql = sprintf("SELECT
							u.*,
							co.*,
							CASE 
								WHEN u.ATIVO = 'S' THEN 'v_peq.png'
								ELSE 'x.png'
						   END AS IMGSTATUS,
						   CASE 
								WHEN u.ATIVO = 'S' THEN 'VALIDADO'
								ELSE 'NAO VALIDADO'
						   END AS DESC_STATUS
						FROM usuario u
						INNER JOIN cadbibliotecacomite co ON (u.IDUSUARIO = co.IDUSUARIO)
						WHERE co.IDBIBLIOTECA = %s
						ORDER BY co.RAZAOSOCIAL, co.NOMERESP", $idUsuario);
		
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function getDadosMembroComite($idBiblioteca, $idMembro)
	{
		$sql = sprintf("SELECT
							u.*,
							co.*,
							l.CEP,
							c.IDUF,
							c.IDCIDADE,
							c.NOMECIDADESUB,
							b.NOMEBAIRRO,
							l.NOMELOGRADOURO,
							CONCAT(t.NOMELOGRADOUROTIPO,' ',l.NOMELOGRADOURO) AS NOMELOGRADOURO2,
							l.COMPLEMENTO
						FROM usuario u
						INNER JOIN cadbibliotecacomite co ON (u.IDUSUARIO = co.IDUSUARIO)
						LEFT JOIN logradouro           l  ON (l.IDLOGRADOURO = co.IDLOGRADOURO)
						LEFT JOIN logradourotipo       t  ON (t.IDLOGRADOUROTIPO = l.IDLOGRADOUROTIPO)
						LEFT JOIN bairro               b  ON (b.IDBAIRRO = l.IDBAIRRO)
						LEFT JOIN cidade               c  ON (c.IDCIDADE = b.IDCIDADE)
						WHERE u.IDUSUARIO = %s
						  AND co.IDBIBLIOTECA = %s
						  AND (u.ATIVO = 'S' OR (u.ATIVO = 'N' AND u.DATA_ATIVACAO IS NULL))", $idMembro, $idBiblioteca);
		
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function validaCpfComite($idBiblioteca, $idMembro, $cpf)
	{
		$sql = sprintf("SELECT
							1 AS CONT
						FROM usuario u
						INNER JOIN cadbibliotecacomite co ON (u.IDUSUARIO = co.IDUSUARIO)
						WHERE co.IDBIBLIOTECA = %s
						  AND (u.ATIVO = 'S' OR (u.ATIVO = 'N' AND u.DATA_ATIVACAO IS NULL))
						  AND u.IDUSUARIO != %s
						  AND u.CPF_CNPJ = '%s'", $idBiblioteca, $idMembro, $cpf);
		
		$dados = $this->db->query($sql);
		
		if ($dados->num_rows() > 0)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	function validaNewMembro($idBiblioteca)
	{
		$sql = "SELECT IFNULL(COUNT(*), 0) AS CONT
				  FROM usuario u
			INNER JOIN cadbibliotecacomite co ON (u.IDUSUARIO = co.IDUSUARIO)
			 	 WHERE co.IDBIBLIOTECA = $idBiblioteca AND CO.IDPROGRAMA = " . $this->session->userdata('programa') . "
				   AND (u.ATIVO = 'S' OR (u.ATIVO = 'N' AND u.DATA_ATIVACAO IS NULL))";
		$dados = $this->db->query($sql);
		if ($dados->num_rows() > 0)
		{
			$row = $dados->row();
			return $row->CONT;
		} else {
			return 0;
		}
	}
	
	function getAllUfsAgenciasBB()
	{
		$sql = "SELECT DISTINCT UF FROM agenciabb ORDER BY UF";
		$dados = $this->db->query($sql);
		return $dados->result_array();
	}
	
	function getAllCidadesUfAgenciasBB($uf = '')
	{
		$return = array();
		if($uf != '')
		{
			$sql = "SELECT DISTINCT MUNICIPIO FROM agenciabb WHERE UF = '" . $uf . "' AND PERFIL_AGENCIA = 'PF' ORDER BY MUNICIPIO";
			$dados = $this->db->query($sql);
			$return = $dados->result_array();
		}
		return $return;
	}
	
	function getAllBairrosCidadeAgenciasBB($cidade = '')
	{
		$return = array();
		if($cidade != '')
		{
			$sql = "SELECT DISTINCT BAIRRO FROM agenciabb WHERE MUNICIPIO = '" . $cidade . "' AND PERFIL_AGENCIA = 'PF' ORDER BY BAIRRO";
			// echo($sql);
			$dados = $this->db->query($sql);
			$return = $dados->result_array();
		}
		return $return;
	}
	
	function getAgencias($uf = '', $cidade = '', $bairro = '')
	{
		$return = array();
		if($cidade != '' && $uf != '' && $bairro != '')
		{
			$sql = "SELECT * FROM agenciabb WHERE uf = '" . $uf . "'  AND MUNICIPIO = '" . $cidade . "' AND BAIRRO = '" . $bairro . "'  AND PERFIL_AGENCIA = 'PF' ORDER BY NOMEAGENCIA";
			// echo($sql);
			$dados = $this->db->query($sql);
			$return = $dados->result_array();
		}
		return $return;
	}
	
	function associarAgenciaBB($idagencia = '', $idbiblioteca = '', $idusuariofinanceiro = '')
	{
		/*
		echo($idagencia . '<br />');
		echo($idbiblioteca . '<br />');
		echo($idusuariofinanceiro . '<br />');
		*/
		if($idagencia != '' && $idbiblioteca != '' && $idusuariofinanceiro != '')
		{
			$sql = "UPDATE cadbibliotecafinan SET IDAGENCIA = $idagencia WHERE IDUSUARIO = $idusuariofinanceiro AND idbiblioteca = $idbiblioteca";
			$this->db->query($sql);
			// echo($sql);
			// die();
			// $return = $dados->result_array();
		}
	}
	
	/*
	* getTipoBiblioteca()
	* Retorna o tipo numerico da biblioteca, onde:
	* 2 - BIBLIOTECA
	* 6 - RESP FINANCEIRO
	* 7 - COMITE
	*/
	function getTipoBiblioteca($idusuario = '')
	{
		if($idusuario != '')
		{
			$sql = "SELECT ut.IDUSUARIOTIPO FROM usuario u INNER JOIN usuariotipo ut ON (ut.IDUSUARIOTIPO = u.IDUSUARIO AND u.IDUSUARIO = $idusuario)";
			$dados = $this->db->query($sql);
			$dados = $dados->result_array();
			return $dados[0]['IDUSUARIOTIPO'];
		}
	}
	
	/*
	* getNomeAgencia()
	* Retorna o nome da agencia do banco do brasil.
	* @param integer idagencia
	* return string nomeagencia
	*/
	function getNomeAgencia($idagencia = '')
	{
		if($idagencia != '')
		{
			$sql = "SELECT CONCAT(a.CODAGENCIACOMPLETO, ' - ', a.NOMEAGENCIA) AS NOMEAGENCIA FROM agenciabb a WHERE idagencia = $idagencia";
			$dados = $this->db->query($sql);
			$dados = $dados->result_array();
			return $dados[0]['NOMEAGENCIA'];
		}
	}
    
    function getItensIndicados($idBiblioteca)
    {
        $sql = sprintf("SELECT l.* FROM bibliotecaindicacao bi
                        INNER JOIN isbnonline.isbn_titulo_unico l ON (l.COD_AUX_TITULO = bi.IDLIVRO)
                        WHERE bi.IDUSUARIO = %s", $idBiblioteca);

        $dados = $this->db->query($sql);
        return $dados->result_array();
    }
	
	/*
	* getIdsBibliotecasPai()
	* Retorna resultset de ids de bibliotecas que são pai no unificador de cadastro.
	* Até o momento os filtros são de: cidade
	* @param array params
	* return array result
	*/
	function getIdsBibliotecasPai($arr_params = array())
	{
		$result = array();
		if(isset($arr_params['CIDADE']) && $arr_params['CIDADE'] != '')
		{
			$sql = "SELECT DISTINCT   CB.IDUSUARIO, CB.RAZAOSOCIAL
									  FROM CIDADE         C 
								INNER JOIN LOGRADOURO     L ON (L.IDCIDADE = C.IDCIDADE)
								INNER JOIN CADBIBLIOTECA CB ON (CB.IDLOGRADOURO = L.IDLOGRADOURO)
								WHERE CB.IDUSUARIO IN (SELECT IDUSUARIOPAI FROM CADBIBLIOTECA CB2 WHERE CB2.IDUSUARIOPAI = CB.IDUSUARIO)";
			$sql .= (isset($arr_params['CIDADE'])) ? " AND C.IDCIDADE = " . $arr_params['CIDADE'] : '';
			$dados  = $this->db->query($sql);
			$result = $dados->result_array();
        }
		return $result;
	}
	
	/*
	* set_pai()
	* Seta o pai utilizado na unificação.
	* @param integer idpai
	* @param integer idfilho
	* return void
	*/
	function set_pai($idfilho = '', $idpai = '')
	{
		if($idfilho != '')
		{
			$idpai = ($idpai == '') ? 'NULL' : $idpai;
			$sql = "UPDATE CADBIBLIOTECA SET IDUSUARIOPAI = $idpai WHERE IDUSUARIO = $idfilho";
			//echo($sql);
			$dados = $this->db->query($sql);
		}
	}
	
	/*
	* getBibliotecasByCidade()
	* Pega todas as bibliotecas que estão em determinada cidade
	* @param string cidade
	* @param boolean extenso
	* return array result
	*/
	function getBibliotecasByCidade($cidade = '', $extenso = false)
	{
		if($extenso)
		{
			$sql = sprintf("
					SELECT U.IDUSUARIO, CB.RAZAOSOCIAL
						FROM USUARIO U
							INNER JOIN CADBIBLIOTECA CB ON U.IDUSUARIO	  = CB.IDUSUARIO
							INNER JOIN LOGRADOURO 	 L  ON L.IDLOGRADOURO = CB.IDLOGRADOURO
							INNER JOIN CIDADE		 C  ON C.IDCIDADE     = L.IDCIDADE
						WHERE C.NOMECIDADE = '%s'
						AND CB.IDSNIIC = 0
						AND CB.IDUSUARIOPAI IS NULL
						ORDER BY CB.RAZAOSOCIAL", $cidade);
		}
		else
		{
			$sql = sprintf("
					SELECT U.IDUSUARIO, CB.RAZAOSOCIAL
						FROM USUARIO U
							INNER JOIN CADBIBLIOTECA CB ON U.IDUSUARIO	  = CB.IDUSUARIO
							INNER JOIN LOGRADOURO 	 L  ON L.IDLOGRADOURO = CB.IDLOGRADOURO
							INNER JOIN CIDADE		 C  ON C.IDCIDADE     = L.IDCIDADE
						WHERE C.IDCIDADE = %d 
						AND CB.IDSNIIC = 0
						AND CB.IDUSUARIOPAI IS NULL
						ORDER BY CB.RAZAOSOCIAL", $cidade);
		}

		$dados = $this->db->query($sql);
		return $dados->result_array();
	}

    function getCreditoByIdBiblioteca($idBiblioteca, $idPrograma = null)
	{
		if(is_null($idPrograma))
		{
			$sql = sprintf("SELECT *
                        FROM CREDITO sc
						WHERE IDUSUARIO = '%s'", $idBiblioteca);
		}
		else
		{
			$sql = sprintf("SELECT *
                        FROM CREDITO sc
						WHERE IDUSUARIO = '%s' AND IDPROGRAMA = '%s'", $idBiblioteca, $idPrograma);
		}

		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
    function getCreditoInfoByIdBiblioteca($idBiblioteca, $idPrograma = null)
	{
		if(is_null($idPrograma))
		{
			$sql = sprintf("SELECT *
                        FROM CREDITOINFO sc
						WHERE IDUSUARIO = '%s'", $idBiblioteca);
		}
		else
		{
			$sql = sprintf("SELECT *
                        FROM CREDITOINFO sc
						WHERE IDUSUARIO = '%s' AND IDPROGRAMA = '%s'", $idBiblioteca, $idPrograma);
		}
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function getCreditoAtualPrograma($idUsuario = null, $idPrograma = null)
	{
		$return = 0;
		if(!is_null($idUsuario))
		{
			$sql = "SELECT VLRCREDITO FROM credito WHERE IDUSUARIO = $idUsuario AND IDPROGRAMA = " . $idPrograma;
			$dados = $this->db->query($sql);
			$result = $dados->result_array();
			if(isset($result[0]['VLRCREDITO']))
			{
				$return = $result[0]['VLRCREDITO'];
			}
		}
		return $return;
	}
	
	function setEmail($tabela = null, $idusuario = null, $email = null)
	{
		$return = false;
		if(!is_null($tabela) && !is_null($idusuario) && !is_null($email))
		{
			$sql = "UPDATE $tabela SET emailresp = '$email' WHERE idusuario = $idusuario";

			$dados = $this->db->query($sql);
			
			$return = true;
		}
		return $return;
	}
	
	function setHabilitado($idusuario = null, $status = null)
	{
		$return = false;
		if(!is_null($idusuario) && !is_null($status))
		{
			$sql = "UPDATE usuario SET habilitado = '$status' WHERE idusuario = $idusuario";	
			
			$dados = $this->db->query($sql);
			
			$return = true;
		}
		return $return;
	}

	function resetSniic($idusuario = null)
	{
		$return = false;
		if(!is_null($idusuario))
		{
			$sql = sprintf("UPDATE cadbiblioteca SET idsniic = 0 WHERE idusuario = %d", $idusuario);	
			
			$dados = $this->db->query($sql);
			
			$return = true;
		}
		return $return;
	}
	
	function temBloqueioLogGerenciamento($idbiblioteca = null, $origem = null)
	{
		$return = false;
		if(!is_null($idbiblioteca) && !is_null($origem))
		{
			$sql = "SELECT COUNT(*) AS val FROM log_gerenciamento WHERE tipolog = 'BLOQUEIO' AND idbiblioteca = '$idbiblioteca' AND origem = '$origem'";
			$dados = $this->db->query($sql);
			$result = $dados->result_array();
			if(isset($result[0]['val']))
			{
				$return = ($result[0]['val'] == 0) ? false : true;
			}
		}
		return $return;
	}

	function temBloqueioCreditoRestricao($idusuario = null)
	{
		$return = false;
		if(!is_null($idusuario))
		{
			$sql = "SELECT COUNT(*) AS val FROM creditorestricao WHERE idusuario = '$idusuario'";
			$dados = $this->db->query($sql);
			$result = $dados->result_array();
			if(isset($result[0]['val']))
			{
				$return = ($result[0]['val'] == 0) ? false : true;
			}
		}
		return $return;
	}
	
	function removeBloqueioLogGerenciamento($idbiblioteca = null, $origem = null)
	{
		$return = false;
		if(!is_null($idbiblioteca) && !is_null($origem))
		{
			$sql = "DELETE FROM log_gerenciamento WHERE tipolog = 'BLOQUEIO' AND idbiblioteca = '$idbiblioteca' AND origem = '$origem'";
			
			$dados = $this->db->query($sql);
			
			$return = true;
		}
		return $return;
	}
	
	function removeCreditoRestricao($idusuario = null)
	{
		$return = false;
		if(!is_null($idusuario))
		{
			$sql = "DELETE FROM creditorestricao WHERE idusuario = '$idusuario'";
			
			$dados = $this->db->query($sql);
			
			$return = true;
		}
		return $return;
	}
	
	function copiaBibliotecasTabelaTemp($idusuario = null)
	{
		$return = false;
		if(!is_null($idusuario))
		{
			$sql = sprintf("SELECT COUNT(*) AS CONT FROM tempusuario WHERE idusuario = %d", $idusuario);
			
			$result = $this->db->query($sql);
			$res = $result->result_array();
			
			if ($res[0]['CONT'] == 0) {
				
				$sql = sprintf("INSERT INTO tempusuario
							SELECT * FROM usuario WHERE idusuario IN (
								SELECT idusuario FROM cadbiblioteca WHERE idusuario = %d OR idusuariopai = %d
							)", $idusuario, $idusuario);
	
				$dados = $this->db->query($sql);
			}
			
			$sql = sprintf("SELECT COUNT(*) AS CONT FROM tempcadbiblioteca WHERE idusuario = %d", $idusuario);
			
			$result = $this->db->query($sql);
			$res = $result->result_array();
			
			if ($res[0]['CONT'] == 0) {
					
				$sql = sprintf("INSERT INTO tempcadbiblioteca
							SELECT * FROM cadbiblioteca	 WHERE idusuario IN (
								SELECT idusuario FROM cadbiblioteca WHERE idusuario = %d OR idusuariopai = %d
							)", $idusuario, $idusuario);
				
				$dados = $this->db->query($sql);
			}
			$return = true;
		}
		return $return;
	}
	
	/**
	* get_count_comite()
	* Retorna a quantidade de membros do comite cadastrados para o programa 
	* de id encaminhado.
	* @param integer idbiblioteca
	* @param integer idprograma
	* @return integer count
	*/
	function get_count_comite($idbiblioteca = 0, $idprograma = 0)
	{
		$sql = "SELECT IFNULL(COUNT(*), 0) AS CONT FROM USUARIO U
			    INNER JOIN CADBIBLIOTECACOMITE CO ON (U.IDUSUARIO = CO.IDUSUARIO)
				 WHERE CO.IDBIBLIOTECA = $idbiblioteca AND CO.IDPROGRAMA = $idprograma
				   AND (u.ATIVO = 'S' OR (u.ATIVO = 'N' AND u.DATA_ATIVACAO IS NULL))";
		// Fetch
		$dados = $this->db->query($sql);
		
		// Return
		if ($dados->num_rows() > 0)
		{
			$row = $dados->row();
			return $row->CONT;
		} else {
			return 0;
		}
	}
	
	/*
	* get_responsavel_financeiro()
	* Retorna o nome do resp. financeiro de uma biblioteca conforme o
	* idprograma encaminhado.
	* @param integer idusuario
	* @param integer idprograma
	* return string nome_resp_finan
	*/
	function get_responsavel_financeiro($idusuario = 0, $idprograma = 0)
	{
		$sql = "SELECT RAZAOSOCIAL FROM CADBIBLIOTECAFINAN WHERE IDBIBLIOTECA = $idusuario AND IDPROGRAMA = $idprograma";
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados[0]['RAZAOSOCIAL']) ? $dados[0]['RAZAOSOCIAL'] : '');
	}
	
	/*
	* get_responsaveis_financeiros()
	* Retorna todos os responsaveis financeiros de uma biblioteca, para um edital.
	* @param integer idusuario
	* @param integer idprograma
	* return array resp_financeiro
	*/
	function get_responsaveis_financeiros($idusuario = 0, $idprograma = 0)
	{
		$sql = "SELECT cf.idusuario as `#`,
					   cf.*, 
					   ag.*, 
					   ag.CODAGENCIACOMPLETO AS AGENCIA,
					   cf.RAZAOSOCIAL,
					   cf.NOMERESP AS NOME,
					   cf.EMAILRESP AS EMAIL,
					   u.LOGIN,
					    u.*,
					   CASE WHEN up.IDPROGRAMA IS NULL THEN 'N' ELSE 'S' END AS HABILITADO,
					   CASE WHEN u.ATIVO = 'N' THEN '<img src=\"" . URL_IMG . "icon_bullet_red.png\" title=\"USUARIO DESATIVADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_bullet_green_2.png\" title=\"USUARIO ATIVO\" />' END AS ATIVO_IMG,
					   CASE WHEN up.IDPROGRAMA IS NULL THEN '<img src=\"" . URL_IMG . "icon_flag_red.png\" title=\"USUARIO DESABILITADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_flag_green.png\" title=\"USUARIO HABILITADO\" />' END AS HABILITADO_IMG,
					   CASE 
							WHEN cf.SIT_CARTAO = 'SEM_CARTAO' THEN 'SEM CARTÃO'  
							WHEN cf.SIT_CARTAO = 'CARTAO_GERADO' THEN 'CARTÃO GERADO (OK)' 
							WHEN cf.SIT_CARTAO = 'CARTAO_BLOQUEADO' THEN 'CARTÃO BLOQUEADO' 
							WHEN cf.SIT_CARTAO = 'REMESSA_CADASTRO' THEN 'CARTÃO EM PROCESSO DE GERAÇÃO'
							WHEN cf.SIT_CARTAO = 'RETORNO_ERRO' THEN 'PROBLEMAS NA GERAÇÃO DO CARTÃO'
					   END AS CARTAO_SITUACAO,
					   CASE 
							WHEN cf.SIT_CARTAO = 'SEM_CARTAO' THEN '<img src=\"" . URL_IMG . "icon_cartao_sem.png\" title=\"SEM CARTÃO\" />'  
							WHEN cf.SIT_CARTAO = 'CARTAO_GERADO' THEN '<img src=\"" . URL_IMG . "icon_cartao_ok.png\" title=\"CARTÃO GERADO (OK)\" />' 
							WHEN cf.SIT_CARTAO = 'CARTAO_BLOQUEADO' THEN '<img src=\"" . URL_IMG . "icon_cartao_bloqueado.png\" title=\"CARTÃO BLOQUEADO\" />' 
							WHEN cf.SIT_CARTAO = 'REMESSA_CADASTRO' THEN '<img src=\"" . URL_IMG . "icon_cartao_processo_geracao.png\" title=\"CARTÃO EM PROCESSO DE GERAÇÃO\" />'
							WHEN cf.SIT_CARTAO = 'RETORNO_ERRO' THEN '<img src=\"" . URL_IMG . "icon_cartao_processo_geracao.png\" title=\"PROBLEMAS NA GERAÇÃO DO CARTÃO\" />'
					   END AS CARTAO_IMG
				  FROM CADBIBLIOTECAFINAN cf 
			 LEFT JOIN agenciabb       ag ON (ag.idagencia = cf.idagencia) 
			 LEFT JOIN usuarioprograma up ON (up.idusuario = cf.idusuario) 
			INNER JOIN usuario          u ON (u.idusuario  = cf.idusuario)
			     WHERE IDBIBLIOTECA = $idusuario AND UP.IDPROGRAMA = $idprograma";
		$dados = $this->db->query($sql);
		return $dados->result_array();
	}
	
	/*
	* get_comite()
	* Retorna resultset do comitê de acervo da biblioteca conforme o
	* idprograma encaminhado.
	* @param integer idusuario
	* @param integer idprograma
	* return array comite
	*/
	function get_comite($idusuario = 0, $idprograma = 0)
	{
		$sql = "SELECT CC.IDUSUARIO AS '#',
					   CC.NOMERESP AS NOME,
					   CC.TELEFONERESP AS TELEFONE,
					   CC.EMAILRESP AS EMAIL,
					   U.CPF_CNPJ AS `CPF CNPJ`,
					   U.LOGIN,
					   U.ATIVO,
					   CASE WHEN u.ATIVO = 'N' THEN '<img src=\"" . URL_IMG . "icon_bullet_red.png\" title=\"USUARIO DESATIVADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_bullet_green_2.png\" title=\"USUARIO ATIVO\" />' END AS ATIVO_IMG
				FROM CADBIBLIOTECACOMITE CC
				INNER JOIN USUARIO U ON (CC.IDUSUARIO = U.IDUSUARIO)
				WHERE IDBIBLIOTECA = $idusuario AND IDPROGRAMA = $idprograma
				ORDER BY U.ATIVO DESC";

		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados)) ? $dados : array();
	}
	
	/*
	* get_pdv_parceiro()
	* Retorna o nome do pdv parceiro da biblioteca conforme o
	* idprograma encaminhado.
	* @param integer idusuario
	* @param integer idprograma
	* return array pdv
	*/
	function get_pdv_parceiro($idusuario = 0, $idprograma = 0)
	{
		$sql = "SELECT NOMERESP FROM CADPDV C
			    INNER JOIN PROGRAMAPDVPARC P ON (C.IDUSUARIO = P.IDPDV) 
				WHERE P.IDBIBLIOTECA = $idusuario AND P.IDPROGRAMA = $idprograma";
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados[0]['NOMERESP']) ? $dados[0]['NOMERESP'] : '');
	}
	
	/**
	* get_lista_pdv_parceiro()
	* Retorna a lista de pdvs parceiros (ou um só) de uma biblioteca.
	* @param integer idusuario
	* @param integer idprograma
	* return array biblioteca
	*/
	function get_lista_pdv_parceiro($idusuario = 0, $idprograma = 0)
	{
		$sql = "SELECT cp.idusuario as `#`,
					   cp.*, 
					   u.*,
					   CASE WHEN up.IDPROGRAMA IS NULL THEN 'N' ELSE 'S' END AS HABILITADO,
					   CASE WHEN u.ATIVO = 'N' THEN '<img src=\"" . URL_IMG . "icon_bullet_red.png\" title=\"USUARIO DESATIVADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_bullet_green_2.png\" title=\"USUARIO ATIVO\" />' END AS ATIVO_IMG,
					   CASE WHEN up.IDPROGRAMA IS NULL THEN '<img src=\"" . URL_IMG . "icon_flag_red.png\" title=\"USUARIO DESABILITADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_flag_green.png\" title=\"USUARIO HABILITADO\" />' END AS HABILITADO_IMG
				  FROM cadpdv cp
			 LEFT JOIN usuarioprograma  up ON (up.idusuario = cp.idusuario) 
			INNER JOIN usuario           u ON (u.idusuario  = cp.idusuario)
			INNER JOIN programapdvparc ppp ON (ppp.idpdv    = cp.idusuario)
			     WHERE ppp.IDBIBLIOTECA = $idusuario AND ppp.IDPROGRAMA = $idprograma";
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados)) ? $dados : array();
	}
	
	/**
	* get_lista_pedidos()
	* Retorna a lista de pedidos (ou um só) de uma biblioteca, para um edital.
	* @param integer idusuario
	* @param integer idprograma
	* return array pedidos
	*/
	function get_lista_pedidos($idusuario = 0, $idprograma = 0)
	{
		$sql = "SELECT p.idpedido as `#`,
					   P.*, 
					   p.percentual_atendimento AS PERC_ATEND,
					   SUM(pl.QTD) AS QTD_ITENS,
					   SUM(pl.QTD_ENTREGUE) AS QTD_ENTREGUE,
					   ps.descstatus as SITUACAO
				  FROM pedido p
			 LEFT JOIN pedidostatus  ps ON (ps.idpedidostatus = p.idpedidostatus) 
			INNER JOIN cadbiblioteca cb ON (cb.idusuario      = p.idbiblioteca)
			INNER JOIN pedidolivro   pl ON (pl.idpedido       = p.idpedido)
			     WHERE p.IDBIBLIOTECA = $idusuario AND p.IDPROGRAMA = $idprograma
			  GROUP BY `#`, situacao";
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados)) ? $dados : array();
	}
	
	/*
	* pode_fazer_indicacao()
	* Retorna um booleano representando se usuario pode ou nao fazer
	* indicação de livros para o 4 edital.
	* @param integer idusuario
	* return boolean
	*/
	function pode_fazer_indicacao($idusuario = 0)
	{
		$sql = "SELECT IFNULL(VLRCREDITO_INICIAL, 0) AS TOTAL FROM CREDITO WHERE IDUSUARIO = $idusuario AND IDPROGRAMA = 2";
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		if(isset($dados[0]['TOTAL']))
		{
			return (($dados[0]['TOTAL'] > 0) ? true : false);
		} else {
			return false;
		}
	}
	
	/*
	* get_dados_biblioteca()
	* Retorna os dados de uma biblioteca
	* @param integer idusuario
	* return array biblioteca
	*/
	function get_dados_biblioteca($idusuario = 0, $idprograma = 0)
	{
		$sql = "SELECT CB.IDUSUARIO AS IDBIBLIOTECA, 
					   CB.* ,
					   U.*,
					   L.CEP AS `CEP_LOGRADOURO`,
					   L.*,
					   LT.*,
					   B.*,
					   C.*,
					   UF.*,
					   CASE WHEN up.IDPROGRAMA IS NULL THEN 'N' ELSE 'S' END AS HABILITADO,
					   CB.IDUSUARIO,
					   CASE WHEN u.ATIVO = 'N' THEN '<img src=\"" . URL_IMG . "icon_bullet_red.png\" title=\"USUARIO DESATIVADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_bullet_green_2.png\" title=\"USUARIO ATIVO\" />' END AS ATIVO_IMG,
					   CASE WHEN up.IDPROGRAMA IS NULL THEN '<img src=\"" . URL_IMG . "icon_flag_red.png\" title=\"USUARIO DESABILITADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_flag_green.png\" title=\"USUARIO HABILITADO\" />' END AS HABILITADO_IMG
				  FROM CADBIBLIOTECA   CB 
		    INNER JOIN USUARIO          U ON (U.IDUSUARIO = CB.IDUSUARIO) 
		     LEFT JOIN LOGRADOURO       L ON (L.IDLOGRADOURO = CB.IDLOGRADOURO) 
		     LEFT JOIN LOGRADOUROTIPO  LT ON (LT.IDLOGRADOUROTIPO = L.IDLOGRADOUROTIPO)
			 LEFT JOIN BAIRRO           B ON (B.IDBAIRRO = L.IDBAIRRO)
		     LEFT JOIN CIDADE           C ON (C.IDCIDADE = L.IDCIDADE) 
			 LEFT JOIN UF               UF ON (UF.IDUF = C.IDUF)
			 LEFT JOIN USUARIOPROGRAMA UP ON (UP.IDUSUARIO = CB.IDUSUARIO AND UP.IDPROGRAMA = $idprograma)
			WHERE CB.IDUSUARIO = $idusuario";
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados[0])) ? $dados[0] : array();
	}
	
	/**
	* get_comite_acervo()
	* Retorna os membros do comite de acervo de uma biblioteca.
	* @param integer idusuario
	* @param integer idprograma
	* return array biblioteca
	*/
	function get_comite_acervo($idusuario = 0, $idprograma = 0)
	{
		$sql = "SELECT cc.idusuario as `#`,
					   cc.*, 
					   u.*,
					   CASE WHEN up.IDPROGRAMA IS NULL THEN 'N' ELSE 'S' END AS HABILITADO,
					   CASE WHEN u.ATIVO = 'N' THEN '<img src=\"" . URL_IMG . "icon_bullet_red.png\" title=\"USUARIO DESATIVADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_bullet_green_2.png\" title=\"USUARIO ATIVO\" />' END AS ATIVO_IMG,
					   CASE WHEN up.IDPROGRAMA IS NULL THEN '<img src=\"" . URL_IMG . "icon_flag_red.png\" title=\"USUARIO DESABILITADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_flag_green.png\" title=\"USUARIO HABILITADO\" />' END AS HABILITADO_IMG
				  FROM CADBIBLIOTECACOMITE cc
			 LEFT JOIN usuarioprograma up ON (up.idusuario = cc.idusuario) 
			INNER JOIN usuario          u ON (u.idusuario  = cc.idusuario)
			     WHERE IDBIBLIOTECA = $idusuario AND cc.IDPROGRAMA = $idprograma";
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados)) ? $dados : array();
	}
	
	/**
	* lista_by_id()
	* VERIFICAR A UTILIDADE DESTA FUNCAO.
	* @param integer idarquivo
	* return array
	*/
	function lista_by_id($idarquivo)
	{
        $sql = " SELECT ".$idarquivo;                        
        $lista = $this->db->query($sql);
        return $lista->result_array();
    }
	
	/*
	* get_dados_responsavel_financeiro_by_id_biblioteca()
	* Retorna dados de um responsavel financeiro através de um idusuario de biblioteca.
	* Espera-se que seja somente um responsável financeiro, então tenta coletar o único ativo,
	* caso a consulta retorne dois, função retorna o primeiro.
	* @param integer idbiblioteca
	* return array resp_financeiro
	*/
	function get_dados_responsavel_financeiro_by_id_biblioteca($idbiblioteca = 0)
	{
		$sql = "SELECT cf.idusuario as `#`,
					   cf.*, 
					   ag.*, 
					   ag.CODAGENCIACOMPLETO AS AGENCIA,
					    u.*,
					   CB.RAZAOSOCIAL AS BIBLIOTECA,
					   CASE WHEN up.IDPROGRAMA IS NULL THEN 'N' ELSE 'S' END AS HABILITADO,
					   CASE WHEN u.ATIVO = 'N' THEN '<img src=\"" . URL_IMG . "icon_bullet_red.png\" title=\"USUARIO DESATIVADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_bullet_green_2.png\" title=\"USUARIO ATIVO\" />' END AS ATIVO_IMG,
					   CASE WHEN up.IDPROGRAMA IS NULL THEN '<img src=\"" . URL_IMG . "icon_flag_red.png\" title=\"USUARIO DESABILITADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_flag_green.png\" title=\"USUARIO HABILITADO\" />' END AS HABILITADO_IMG,
					   CASE 
							WHEN cf.SIT_CARTAO = 'SEM_CARTAO' THEN 'SEM CARTÃO'  
							WHEN cf.SIT_CARTAO = 'CARTAO_GERADO' THEN 'CARTÃO GERADO (OK)' 
							WHEN cf.SIT_CARTAO = 'CARTAO_BLOQUEADO' THEN 'CARTÃO BLOQUEADO' 
							WHEN cf.SIT_CARTAO = 'REMESSA_CADASTRO' THEN 'CARTÃO EM PROCESSO DE GERAÇÃO'
					   END AS CARTAO_SITUACAO,
					   CASE 
							WHEN cf.SIT_CARTAO = 'SEM_CARTAO' THEN '<img src=\"" . URL_IMG . "icon_cartao_sem.png\" title=\"SEM CARTÃO\" />'  
							WHEN cf.SIT_CARTAO = 'CARTAO_GERADO' THEN '<img src=\"" . URL_IMG . "icon_cartao_ok.png\" title=\"CARTÃO GERADO (OK)\" />' 
							WHEN cf.SIT_CARTAO = 'CARTAO_BLOQUEADO' THEN '<img src=\"" . URL_IMG . "icon_cartao_bloqueado.png\" title=\"CARTÃO BLOQUEADO\" />' 
							WHEN cf.SIT_CARTAO = 'REMESSA_CADASTRO' THEN '<img src=\"" . URL_IMG . "icon_cartao_processo_geracao.png\" title=\"CARTÃO EM PROCESSO DE GERAÇÃO\" />'
					   END AS CARTAO_IMG
				  FROM CADBIBLIOTECAFINAN cf 
			 LEFT JOIN agenciabb       ag ON (ag.idagencia = cf.idagencia) 
			 LEFT JOIN usuarioprograma up ON (up.idusuario = cf.idusuario) 
			 LEFT JOIN CADBIBLIOTECA   CB ON (CB.IDUSUARIO = CF.IDBIBLIOTECA)
			INNER JOIN usuario          u ON (u.idusuario  = cf.idusuario)
			     WHERE cf.IDBIBLIOTECA = $idbiblioteca
			  ORDER BY U.ATIVO DESC";
		$dados  = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados[0]) && count($dados[0]) > 0) ? $dados[0] : array();
	}
	
	/*
	* get_dados_responsavel_financeiro()
	* Retorna dados de um responsavel financeiro.
	* @param integer idusuario
	* return array resp_financeiro
	*/
	function get_dados_responsavel_financeiro($idusuario = 0)
	{
		$sql = "SELECT  cf.idusuario as `#`,
						cf.*, 
						ag.*,
						ag.IDAGENCIA,
						ag.LOGRADOURO AS LOGRADOUROBB,
						ag.UF AS UFBB,
						ag.BAIRRO AS BAIRROBB,
						ag.MUNICIPIO AS MUNICIPIOBB,
						ag.CODAGENCIACOMPLETO AS AGENCIA,
						u.*,
						CB.RAZAOSOCIAL AS BIBLIOTECA,
						CASE WHEN up.IDPROGRAMA IS NULL THEN 'N' ELSE 'S' END AS HABILITADO,
						CASE WHEN u.ATIVO = 'N' THEN '<img src=\"" . URL_IMG . "icon_bullet_red.png\" title=\"USUARIO DESATIVADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_bullet_green_2.png\" title=\"USUARIO ATIVO\" />' END AS ATIVO_IMG,
						CASE WHEN up.IDPROGRAMA IS NULL THEN '<img src=\"" . URL_IMG . "icon_flag_red.png\" title=\"USUARIO DESABILITADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_flag_green.png\" title=\"USUARIO HABILITADO\" />' END AS HABILITADO_IMG,
						L.CEP AS `CEP_LOGRADOURO`,
						L.IDLOGRADOURO,
						L.NOMELOGRADOURO,
						LT.*,
						B.*,
						C.*,
						UF.*
					FROM CADBIBLIOTECAFINAN cf
				   LEFT JOIN agenciabb       ag ON (ag.idagencia = cf.idagencia) 
				   LEFT JOIN usuarioprograma up ON (up.idusuario = cf.idusuario) 
				   LEFT JOIN CADBIBLIOTECA   CB ON (CB.IDUSUARIO = CF.IDBIBLIOTECA)
					INNER JOIN usuario        u ON (u.idusuario  = cf.idusuario)
				   LEFT JOIN LOGRADOURO       L ON (L.IDLOGRADOURO = CF.IDLOGRADOURO)
				   LEFT JOIN LOGRADOUROTIPO  LT ON (LT.IDLOGRADOUROTIPO = L.IDLOGRADOUROTIPO)
				   LEFT JOIN BAIRRO           B ON (B.IDBAIRRO = L.IDBAIRRO)
				   LEFT JOIN CIDADE           C ON (C.IDCIDADE = L.IDCIDADE)
				   LEFT JOIN UF              UF ON (UF.IDUF = C.IDUF)
					 WHERE cf.IDUSUARIO = $idusuario";
				 
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();		
		return (isset($dados[0])) ? $dados[0] : array();	
	}
	
	/**
	* get_lista_modulo()
	* Retorna os dados da listagem de entrada do modulo.
	* return array data
	*/
	function get_lista_modulo($filters = array())
	{
		$sql = "SELECT 	cb.IDUSUARIO AS `#`,
						CB.RAZAOSOCIAL AS `RAZÃO SOCIAL`,
						U.CPF_CNPJ AS `CPF/CNPJ`,
						CB.TELEFONEGERAL AS `TELEFONE`,
						CB.EMAILGERAL AS `EMAIL`,
						CB.NOMERESP AS `RESPONSÁVEL`,
						CB.TELEFONERESP AS `TEL. RESP.`,
						CB.EMAILRESP AS `EMAIL RESP.`,
						U.LOGIN,
						CASE WHEN u.ATIVO = 'N' THEN '<img src=\"" . URL_IMG . "icon_bullet_red.png\" title=\"USUARIO DESATIVADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_bullet_green_2.png\" title=\"USUARIO ATIVO\" />' END AS ATIVO_IMG
				  FROM CADBIBLIOTECA CB
			INNER JOIN USUARIO          U ON(U.IDUSUARIO = CB.IDUSUARIO)
			 LEFT JOIN LOGRADOURO       L ON(L.IDLOGRADOURO = CB.IDLOGRADOURO)
			 LEFT JOIN CIDADE           C ON(C.IDCIDADE = L.IDCIDADE)
			 LEFT JOIN USUARIOPROGRAMA UP ON(UP.IDUSUARIO = U.IDUSUARIO)";
		
		// Carrega model auxiliar para processamento do where
		$this->load->model('global_model');
		$sql .= $this->global_model->get_where_filters($filters);
		
		// Concatena ORDER BY
		$sql .= ' ORDER BY CB.IDUSUARIO';
		
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados)) ? $dados : array();
	}
	
	/*
	* get_ufs_bibliotecas()
	* Retorna dados de ufs que possuem bibliotecas.
	* return array ufs
	*/
	function get_ufs_bibliotecas()
	{
		$sql = "SELECT DISTINCT c.IDUF, C.IDUF AS LABEL FROM CADBIBLIOTECA CB
			INNER JOIN USUARIO          U ON(U.IDUSUARIO = CB.IDUSUARIO)
			 LEFT JOIN LOGRADOURO       L ON(L.IDLOGRADOURO = CB.IDLOGRADOURO)
			 LEFT JOIN CIDADE           C ON(C.IDCIDADE = L.IDCIDADE)
			 LEFT JOIN USUARIOPROGRAMA UP ON(UP.IDUSUARIO = U.IDUSUARIO)
			  ORDER BY IDUF";
		$dados  = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}
	
	/*
	* resp_financeiro_tem_cartao_gerado()
	* Retorna booleano dizendo se resp financeiro tem cartao gerado.
	* return integer idusuario
	* return boolean tem_Cartao
	*/
	function resp_financeiro_tem_cartao_gerado($idusuario = 0)
	{
		$sql = "SELECT COUNT(*) TOTAL FROM ARQUIVO_MOVIMENTACAO WHERE IDUSUARIO = $idusuario AND IDSTATUSMOVIMENTACAO = 0 AND IS_RETORNO = 'N'";
		$dados  = $this->db->query($sql);
		$dados = $dados->result_array();
		return (get_value($dados[0], 'TOTAL') > 0) ? true : false;
	}
	
	/*
	* resp_financeiro_em_processo_geracao_cartao()
	* Retorna booleano dizendo se resp financeiro esta com cartao em processo de geracao.
	* return integer idusuario
	* return boolean is_processo
	*/
	function resp_financeiro_em_processo_geracao_cartao($idusuario = 0)
	{
		$sql = "SELECT COUNT(*) AS TOTAL FROM CADBIBLIOTECAFINAN CF
		         WHERE (SELECT COUNT(*) TOTAL FROM ARQUIVO_MOVIMENTACAO WHERE IDUSUARIO = CF.IDUSUARIO AND IDSTATUSMOVIMENTACAO = 0 AND IS_RETORNO = 'N') = 0
		           AND (SELECT COUNT(*) TOTAL FROM ARQUIVO_MOVIMENTACAO WHERE IDUSUARIO = CF.IDUSUARIO AND IDSTATUSMOVIMENTACAO IS NULL AND IS_RETORNO = 'N') > 0
				   AND CF.IDUSUARIO = $idusuario";
		$dados  = $this->db->query($sql);
		$dados = $dados->result_array();
		return (get_value($dados[0], 'TOTAL') > 0) ? true : false;
	}
	
	/*
	* get_dados_comite_acervo()
	* Retorna os dados do comite de acervo
	* @param integer idusuario
	* return array biblioteca
	*/
	function get_dados_comite_acervo($idusuario = 0)
	{
	 $sql = "SELECT
					U.*,
					U.IDUSUARIO,
					CC.IDUSUARIO AS `#`, 
					CB.RAZAOSOCIAL AS `BIBLIOTECA`,
					CC.RAZAOSOCIAL AS `NOME`,
					CC.NOMERESP AS `NOME RESPONSAVEL`,
					CC.NOMEMAE AS `NOME DA MAE`,
					CC.DATANASC AS `DATA DE NASCIMENTO`,  
					CC.TELEFONERESP AS `TELEFONE`,
					CC.EMAILRESP AS `EMAIL`,
					CASE WHEN U.ATIVO = 'N' THEN '<img src=\"" . URL_IMG . "icon_bullet_red.png\" title=\"USUARIO DESATIVADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_bullet_green_2.png\" title=\"USUARIO ATIVO\" />' END AS `ATIVO_IMG`,
					L.CEP AS `CEP_LOGRADOURO`,
					L.NOMELOGRADOURO,
					CC.IDLOGRADOURO,
					CC.END_NUMERO,
					CC.END_COMPLEMENTO,
					LT.*,
					B.*,
					C.*,
					UF.*
					FROM cadbibliotecacomite CC
					INNER JOIN usuario U ON (U.IDUSUARIO = CC.IDUSUARIO)
				   LEFT JOIN cadbiblioteca CB ON (CC.IDBIBLIOTECA = CB.IDUSUARIO)
				   LEFT JOIN LOGRADOURO       L ON (L.IDLOGRADOURO = CC.IDLOGRADOURO) 
				   LEFT JOIN LOGRADOUROTIPO  LT ON (LT.IDLOGRADOUROTIPO = L.IDLOGRADOUROTIPO)
				   LEFT JOIN BAIRRO           B ON (B.IDBAIRRO = L.IDBAIRRO)
				   LEFT JOIN CIDADE           C ON (C.IDCIDADE = L.IDCIDADE) 
				   LEFT JOIN UF               UF ON (UF.IDUF = C.IDUF)
					WHERE U.IDUSUARIO = $idusuario";

		$dados = $this->db->query($sql);
		$dados = $dados->result_array();		
		return (isset($dados[0])) ? $dados[0] : array();		
	}	
	
	/*
	* get_qtd_rf_by_biblioteca()
	* Retorna a quantidade de responsaveis financeiros de uma biblioteca.
	* @param integer idusuario
	* return array QTD
	*/
	function get_qtd_rf_by_biblioteca($idusuario)
	{
		$sql = "select COUNT(*) as QTD
				from cadbibliotecafinan cf
				inner join usuario u on (cf.idusuario = u.idusuario)
				where u.ativo = 'S' and idbiblioteca = $idusuario";
				
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();		
		return (isset($dados[0]['QTD'])) ? $dados[0]['QTD'] : array();
	}

	/*
	* get_lista_pendencia_pedidos()
	* Retorna pedidos nao finalizados
	* return array dados
	*/
	function get_lista_pendencia_pedidos($idusuario, $idprograma)
	{
		$sql = "SELECT  DISTINCT P.IDPEDIDO
       				 FROM PEDIDO P
        		INNER JOIN PEDIDO_DOCUMENTO PD ON (PD.IDPEDIDO = P.IDPEDIDO)
        			WHERE PD.NUMERO_ENTREGA IS NULL
        			AND P.IDBIBLIOTECA = $idusuario
        			AND P.IDPROGRAMA = $idprograma";
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados)) ? $dados : array();
	}
}