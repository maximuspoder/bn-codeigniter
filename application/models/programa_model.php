<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe Programa_Model
*
* Esta classe cont�m m�todos para a abstra��o da entidade model programa.
* 
* @author		Leandro Mangini Antunes
* @package		application
* @subpackage	application.programa
* @since		2012-03-24
*
*/
class Programa_model extends CI_Model {
	
	/**
	* __construct()
	* @return object
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	/**
	* getAllProgramas()
	* Retorna todos os programas.
	* @return array programas
	*/
	function getAllProgramas()
	{
		$sql = "SELECT IDPROGRAMA, DESCPROGRAMA FROM programa";
		$dados = $this->db->query($sql);
		return $dados->result_array();
	}
	
	/**
	* getProgramasAtivos()
	* Retorna todos os programas ativos.
	* @return array programas
	*/
	function getProgramasAtivos()
	{
		$sql = "SELECT DESCPROGRAMA, IDPROGRAMA FROM programa WHERE ATIVO = 'S'";
		$dados = $this->db->query($sql);
		return $dados->result_array();
	}
	
	/**
	* get_programas_ativos()
	* Retorna todos os programas ativos.
	* @return array programas
	*/
	function get_programas_ativos()
	{
		$sql = "SELECT * FROM programa WHERE ATIVO = 'S'";
		$dados = $this->db->query($sql);
		return $dados->result_array();
	}
	
	/**
	* get_nome_programa()
	* Retorna o nome do programa, com base no idprograma encaminhado.
	* @param integer idprograma
	* @return string nome_programa
	*/
	function get_nome_programa($idprograma = 0)
	{
		$this->load->model('global_model');
		if($idprograma == 0)
		{
			$return = 'Nenhum edital selecionado';
		} else {
			$dados  = $this->global_model->selectx('programa', 'idprograma = ' . $idprograma, 'IDPROGRAMA');
			$return = $dados[0]['NOME_EDITAL'];
		}
		return $return;
	}
	
	/**
	* get_icone_programa()
	* Retorna o icone do programa, com base no idprograma encaminhado.
	* @param integer idprograma
	* @return string icone_programa
	*/
	function get_icone_programa($idprograma = 0)
	{
		$this->load->model('global_model');
		if($idprograma == 0)
		{
			$return = 'icon_nenhum_edital.png';
		} else {
			$dados  = $this->global_model->selectx('programa', 'idprograma = ' . $idprograma, 'ICONE_EDITAL');
			$return = $dados[0]['ICONE_EDITAL'];
		}
		return $return;
	}
	
	/**
	* get_creditos()
	* Retorna um resultset de possiveis creditos contemplados ao usuario 
	* de id encaminhado para o programa encaminhado.
	* @param integer idusuario
	* @param integer idprograma
	* @return array credittos
	*/
	function get_creditos($idusuario = 0, $idprograma = 0)
	{
		$return = array();
		if($idprograma != 0)
		{
			$sql = "SELECT * FROM CREDITO WHERE IDUSUARIO = $idusuario AND IDPROGRAMA = $idprograma";
			$dados  = $this->db->query($sql);
			$return = $dados->result_array();
		}
		return $return;
	}
	
	/**
	* get_programas_ativos_by_usuario()
	* Retorna todos os programas ativos, tentando relacionar o usu�rio de
	* id encaminhado.
	* @return array programas
	*/
	function get_programas_ativos_by_usuario($idusuario = 0)
	{
		$sql = "SELECT p.IDPROGRAMA,
					   p.NOME_EDITAL, 
				       p.TEXTO_EDITAL,
				       p.ICONE_EDITAL,
					   p.ARQUIVO_EDITAL,
                       up.IDUSUARIO
				  FROM programa p 
			 LEFT JOIN usuarioprograma up ON (up.idprograma = p.idprograma and up.idusuario = $idusuario)
				  WHERE ATIVO = 'S'";
		$dados = $this->db->query($sql);
		return $dados->result_array();
	}
	
	/**
	* lista_contemplados()
	* Retorna todos os contemplados do programa de id encaminhado.
	* @param integer idprograma
	* @return array programas
	*/
	function lista_contemplados($idprograma = 0)
	{
		$sql = "SELECT PC.UF, 
		               C.NOMECIDADE AS CIDADE,
					   PC.RAZAO_SOCIAL AS BIBLIOTECA,
					   REPLACE(PC.VLRCREDITO, '.', ',') AS `VALOR (R$)`
				  FROM temp_programa_contemplado PC
			 LEFT JOIN CIDADE C ON (C.IDCIDADE = PC.IDCIDADE)
				 WHERE IDPROGRAMA = $idprograma ORDER BY UF, CIDADE";
		$dados  = $this->db->query($sql);
		$return = $dados->result_array();
		return (count($return) > 0 ? $return : array());
	}
}

/* End of file programa_model.php */
/* Location: ./system/application/models/programa_model.php */