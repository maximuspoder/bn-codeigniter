<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe Cnab
*
* Esta classe contém métodos para a abstração da entidade cnab (emissao de arquivos).
* 
* @author		Leandro Mangini Antunes
* @package		application
* @subpackage	application.cnab
* @since		2012-06-21
*
*/
class Cnab_Model extends CI_Model {
	
	/**
	* __construct()
	* @return object
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	/**
	* listaRegistroCredito()
	* Retorna a listagem de creditos disponíveis e pendentes para serem gerados.
	* CRITÉRIOS PARA A GERAÇÃO DE CRÉDITOS:
	* 1) Deve possuir crédito liberado na tabela pedido_valor_entrega
	* 2) Deve possuir cartão já emitido
	* 3) Não pode possuir lote de comando de bloqueio de cadastro emitido ou emitido e ok
	* @param integer idprograma
	* @param integer limite
	* @param array dados
	*/
	function listaRegistroCredito($idprograma = 0, $limite = 0) {
		if ($idprograma > 0) {
			$sql = "SELECT CF.*, 
						   BB.CODAGENCIA, 
						   U.CPF_CNPJ, 
						   PVE.VALOR AS VALORTOTAL,
						   PVE.ID_PEDIDO_VALOR_ENTREGA
					  FROM cadbibliotecafinan    CF 	
				INNER JOIN usuario                U ON (CF.idusuario = U.idusuario)
				INNER JOIN usuarioprograma       UP ON (U.idusuario = UP.idusuario AND UP.idprograma = 1)
				INNER JOIN agenciaBB             BB ON (CF.idagencia = BB.idagencia)
				INNER JOIN pedido                 P ON (CF.idbiblioteca = P.idbiblioteca)
				INNER JOIN PEDIDO_VALOR_ENTREGA PVE ON (P.IDPEDIDO = PVE.IDPEDIDO)
					 WHERE U.ativo = 'S'
					   /* CREDITO LIBERADO E NAO EMITIDO */
					   AND PVE.CREDITO_EMITIDO = 'N'
					   
					   /* TEM CARTÃO */
					   AND (SELECT COUNT(*) FROM ARQUIVO_MOVIMENTACAO AM WHERE AM.IDTIPOLOTE = 21 AND AM.IDUSUARIO = CF.IDUSUARIO AND IDSTATUSMOVIMENTACAO = 0 AND IS_RETORNO = 'N') > 0
					   
					   /* NÃO TEM REMESSA DE BLOQUEIO DE CADASTRO */
					   AND (SELECT COUNT(*) FROM ARQUIVO_MOVIMENTACAO AM WHERE AM.IDTIPOLOTE = 40 AND AM.IDUSUARIO = CF.IDUSUARIO AND IDSTATUSMOVIMENTACAO = 0 AND IS_RETORNO = 'N') <= 0
					   
					   /* LIMITAÇÃO INICIAL TESTE */
					   /* AND P.IDPEDIDO IN (50, 1065, 163) */";
			$sql .= ($limite > 0) ? " LIMIT 0, $limite" : '';
			
			// DEBUG: 
			// echo $sql;
			$dados = $this->db->query($sql);
			return $dados->result_array();
		} else {
			return array();
		}
	}
	
	/**
	* listaRegistroCadastro()
	* Retorna a listagem de cadastros disponíveis e pendentes para que os cartões que
	* devem ser gerados.
	* @param integer idprograma
	* @param integer limite
	* @param array dados
	*/
	function listaRegistroCadastro($idprograma = 0, $limite = 0) {
		if ($idprograma > 0) {
			/*
			Union all abaixo foi utilizado apenas como teste, para geração de um arquivo
			de remessa de cadastro - e consequentemente um cartão - apenas como fim de teste,
			para análise do procedimento de retirada do cartão. (nenhum crédito foi inserido para este cartão)
			$sql = "SELECT * FROM (
				     SELECT CF.*, BB.CODAGENCIA, U.CPF_CNPJ, L.NOMELOGRADOURO, L.CEP, B.NOMEBAIRRO, C.NOMECIDADE,
					(SELECT idarquivostatus 
						FROM arquivo A 
						INNER JOIN arquivo_movimentacao AM ON (A.idarquivo = AM.idarquivo)
						WHERE AM.IDUSUARIO = CF.IDUSUARIO
						ORDER BY AM.DATAMOVIMENTACAO DESC LIMIT 0,1
					) AS ULTIMO_STATUS,
					(SELECT habilitado FROM usuario WHERE idusuario = CF.idbiblioteca) AS HABILITADO
					FROM cadbibliotecafinan CF 
					INNER JOIN usuario U			ON (CF.idusuario = U.idusuario)
					INNER JOIN usuarioprograma UP	ON (U.idusuario = UP.idusuario AND UP.idprograma = 1)
					INNER JOIN agenciaBB BB			ON (CF.idagencia = BB.idagencia)
					INNER JOIN logradouro L			ON CF.idlogradouro = L.idlogradouro
					INNER JOIN bairro B				ON L.idbairro = B.idbairro
					INNER JOIN cidade C				ON L.idcidade = C.idcidade
					WHERE U.ativo = 'S'and u.idusuario = 15973
					) X
					WHERE (X.ULTIMO_STATUS IS NULL OR X.ULTIMO_STATUS > 4)
					UNION ALL
			*/
			
			$sql = "
			SELECT * FROM 
			( 			SELECT CF.*, 
							   BB.CODAGENCIA, 
							   U.CPF_CNPJ, 
							   L.NOMELOGRADOURO, 
							   L.CEP, B.NOMEBAIRRO, 
							   C.NOMECIDADE,
							   (SELECT idarquivostatus FROM arquivo A INNER JOIN arquivo_movimentacao AM ON (A.idarquivo = AM.idarquivo) WHERE AM.IDUSUARIO = CF.IDUSUARIO ORDER BY AM.DATAMOVIMENTACAO DESC LIMIT 0,1) AS ULTIMO_STATUS,
							   (SELECT habilitado FROM usuario WHERE idusuario = CF.idbiblioteca) AS HABILITADO, 
							   P.IDPEDIDOSTATUS
						  FROM cadbibliotecafinan CF 
					INNER JOIN usuario          U			  	ON (CF.idusuario    = U.idusuario)
					INNER JOIN usuarioprograma  UP	    		ON (U.idusuario     = UP.idusuario AND UP.idprograma = $idprograma)
					INNER JOIN agenciaBB        BB				ON (CF.idagencia    = BB.idagencia)
					INNER JOIN logradouro       L			 	ON (CF.idlogradouro = L.idlogradouro)
					INNER JOIN bairro           B				ON (L.idbairro      = B.idbairro)
					INNER JOIN cidade           C				ON (L.idcidade      = C.idcidade)
					INNER JOIN pedido           P				ON (CF.IDBIBLIOTECA = P.IDBIBLIOTECA)
					INNER JOIN CADBIBLIOTECA    CB              ON (CB.IDUSUARIO    = CF.IDBIBLIOTECA)
						 WHERE U.ativo = 'S' 
						   AND P.IDPEDIDOSTATUS IN (2,3,4,5) 
						   AND p.idprograma = $idprograma
              /* SELECIONA SOMENTE OS USUARIOS QUE JÁ NÃO TENHAM UM RETORNO OK */
			  AND 
			  (
				CF.IDUSUARIO NOT IN (SELECT IDUSUARIO FROM ARQUIVO_MOVIMENTACAO AM2 WHERE AM2.IDUSUARIO = CF.IDUSUARIO AND AM2.IDSTATUSMOVIMENTACAO = 0 OR AM2.IDSTATUSMOVIMENTACAO IS NULL) 
				OR CF.IDUSUARIO IN (3121, 7822, 10470, 10545, 11045, 15632, 16367, 15830, 18419, 3883)
			  )
              
			  /* RETIRA CADASTROS DUPLICADOS DA LISTA */
              AND (SELECT COUNT(*) FROM CADBIBLIOTECAFINAN CF2 INNER JOIN USUARIO U2 ON (U2.IDUSUARIO = CF2.IDUSUARIO) WHERE CF2.IDBIBLIOTECA = CB.IDUSUARIO AND U2.ATIVO = 'S' GROUP BY CF2.IDBIBLIOTECA HAVING COUNT(*) > 1) is null
			  
			  /* RETIRA CADASTROS NAO VERIFICADOS POR SAC, DA LISTA */
			  /* AND CF.IDUSUARIO NOT IN (3305, 17994, 10638, 13434, 2099, 16837, 4907, 8336, 12074, 15689, 10365, 15345, 15818, 12383, 3230, 15358, 18111, 12941, 1462, 15171, 820, 15164, 18528) */ 
			   
			  /* OLDER: LIMITAÇÃO DE BIBLIOTECA */
              /* AND p.idbiblioteca NOT IN (257, 1075, 1535, 1672, 1924, 2496, 2728, 3300, 3453, 3501, 3572, 4848, 5744, 6371, 7073, 8233, 8802, 11648, 14162, 16186, 16435, 16570, 16603, 17505, 17953) */
			) X  ORDER BY RAZAOSOCIAL, X.IDUSUARIO";
			
			if ($limite > 0) $sql .= " LIMIT 0, $limite";
			$dados = $this->db->query($sql);
			return $dados->result_array();
		} else {
			return array();
		}
	}
	
	function getValorParametro($nomeparametro = '', $idtipolote = '') {
		
		$result = false;
		if($nomeparametro != '')
		{
			if($idtipolote == '')
			{
				$sql = sprintf("SELECT VLRPARAMETRO FROM arquivo_parametros WHERE NOMEPARAMETRO = '%s'", $nomeparametro);
			}
			else
			{
				$sql = sprintf("SELECT VLRPARAMETRO FROM arquivo_parametros WHERE IDTIPOLOTE = %d AND NOMEPARAMETRO = '%s'", $idtipolote, $nomeparametro);
			}

			$dados  = $this->db->query($sql);
			$dados  = $dados->result_array();
			$result = $dados[0]['VLRPARAMETRO'];
		}
		return $result;
	}
	
	function addValorParametro($nomeparametro = '', $idtipolote = '') {
		
		$result = false;
		if($nomeparametro != '' && $idtipolote != '')
		{
			$sql = sprintf("SELECT VLRPARAMETRO FROM arquivo_parametros WHERE IDTIPOLOTE = %s AND NOMEPARAMETRO = '%d'", $idtipolote, $nomeparametro);
			$dados  = $this->db->query($sql);
			$dados  = $dados->result_array();
			$valor = (int)$dados[0]['VLRPARAMETRO'];
			$valor++;
			$valor = str_pad($valor, 6, '0', STR_PAD_LEFT);
			$sql = "UPDATE arquivo_parametros SET VLRPARAMETRO = '$valor' WHERE IDTIPOLOTE = '$idtipolote' AND NOMEPARAMETRO = '$nomeparametro'";
			$this->db->query($sql);
		}
		return $result;
	}
	
	function gravaHistoricoMovimentacao($arrDados) {
		$arrIns = array();
		
		$res = $this->global_model->insert('arquivo_movimentacao', $arrIns);
		
		
	}
    
    function selecionaTipoArquivo()
	{
        
        $sql = "SELECT * FROM arquivotipo";
        $tipo = $this->db->query($sql);
        return $tipo->result_array();
    }
    

    function listaArquivo()
	{
        $sql = "SELECT 
                  arquivo.IDARQUIVO as ID, 
                  arquivo.IDTIPOLOTE as LOTE, 
                  arquivostatus.DESCSTATUS AS DESCRICAO,
                  arquivo.NOME AS NOME,
                  arquivo.DATA_GERACAO AS DATA
                  FROM arquivo
                  INNER JOIN arquivostatus ON arquivo.IDARQUIVOSTATUS = arquivostatus.IDARQUIVOSTATUS";
        
        $dados = $this->db->query($sql);
        return $dados->result_array();
    }
	
    /**
	* detalhe_arquivo()
	* Retorna os dados da movimentação do arquivo.
	* @param integer idarquivo 
	* @return array arquivo
	**/
    function detalhe_arquivo($idarquivo = 0, $idtipolote = 0){
        switch($idtipolote)
        {
			// Remessa de créditos
			case 20:
				$sql = "SELECT SUBSTR(DETALHE, 09, 02) AS TIPO_LOTE,
							   SUBSTR(DETALHE, 11, 9) AS NIB,
							   /* CONCAT('R$ ', REPLACE(REPLACE(REPLACE(FORMAT(SUBSTR(DETALHE, 53, 10), 2), '.', '|'), ',', '.'), '|', ',')) AS `VALOR CREDITO`, */
							   CONCAT('R$ ', TRIM(LEADING '0' FROM SUBSTR(DETALHE, 53, 10)), ',', TRIM(LEADING '0' FROM SUBSTR(DETALHE, 63, 2))) AS `VALOR CRÉDITO`,
							   DATE_FORMAT(SUBSTR(DETALHE, 66, 08), '%d/%m/%Y') AS DATA_FIM_VALIDADE
						  FROM ARQUIVO_MOVIMENTACAO AM 
						 WHERE AM.IDARQUIVO = $idarquivo
						   AND IDTIPOLOTE = $idtipolote";
			break;
			
			// Remessa de cadastros
            case 21:
				$sql = "SELECT CAST(SUBSTR(DETALHE, 2, 7) AS UNSIGNED) AS `SEQ.`,
							   CAST(SUBSTR(DETALHE, 11, 9) AS UNSIGNED) AS NIB,								   
							   SUBSTR(DETALHE, 73, 28) AS RESP_FINANCEIRO,
							   SUBSTR(DETALHE, 49, 11) AS CPF,
							   DATE_FORMAT(SUBSTR(DETALHE, 177, 08), '%d/%m/%Y') AS `DATA NASC.`,
							   CAST(SUBSTR(DETALHE, 21, 6) AS UNSIGNED) AS AGENCIA_BB,								   
							   CONCAT(ad.CODIGORETORNO, ' - ', ad.DESCRICAO) AS `SIT. PROCESSAMENTO`
						  FROM ARQUIVO_MOVIMENTACAO AM
					 LEFT JOIN ARQUIVO_RETORNO_DESCRICAO AD ON (AD.CODIGORETORNO = AM.IDSTATUSMOVIMENTACAO)
					     WHERE AM.IDARQUIVO = $idarquivo 
						   AND IDTIPOLOTE = $idtipolote 
						   AND AM.IS_RETORNO = 'N'";
            break;
			
			// PADRÃO
			default:
				$sql = "SELECT SUBSTR(DETALHE, 09, 02) AS TIPO_LOTE,
							   SUBSTR(DETALHE, 11, 9) AS NIB,
							   FORMAT(SUBSTR(DETALHE, 53, 12),0) AS VLR_CREDITO,
							   DATE_FORMAT(SUBSTR(DETALHE, 66, 08), '%d/%m/%Y') AS DATA_FIM_VALIDADE
						  FROM ARQUIVO_MOVIMENTACAO AM 
						 WHERE AM.IDARQUIVO = $idarquivo
						   AND IDTIPOLOTE = $idtipolote";
			break;
        }
		// Executa SQL
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return(isset($return)) ? $return : array();
    }
    
	/**
	* gerenciadorCpb()
	* Resultset de arquivos do modulo cpb. EXIBE ARQUIVOS DE REMESSA, ordenados
	* por tipo de lote, e data de geracao.
	* @return array data
	*/
    function gerenciadorCpb()
	{
		$sql = "SELECT a.IDARQUIVO as `#`,
					   p.IDPROGRAMA as PROGRAMA,
					   atl.IDTIPOLOTE as IDTIPOLOTE,
					   atl.DESCTIPOLOTE as TIPOLOTE,
					   CONCAT(a.IDARQUIVOSTATUS, ' - ', ast.DESCSTATUS) AS STATUS,
					   a.NOME AS ARQUIVO,
					   DATE_FORMAT(DATA_GERACAO, '%d/%m/%Y %H:%i:%s') AS `DATA GERAÇÃO`,
					   (SELECT COUNT(*) FROM arquivo_movimentacao AM2 WHERE IDARQUIVO = a.IDARQUIVO AND AM2.IS_RETORNO = 'N') AS `REG.`,
					   (SELECT COUNT(*) FROM arquivo_movimentacao AM2 WHERE IDARQUIVO = a.IDARQUIVO AND AM2.IS_RETORNO = 'N' AND AM2.IDSTATUSMOVIMENTACAO = 0) AS `REG. OK`,
					   (SELECT COUNT(*) FROM arquivo_movimentacao AM2 WHERE IDARQUIVO = a.IDARQUIVO AND AM2.IS_RETORNO = 'N' AND AM2.IDSTATUSMOVIMENTACAO != 0 AND AM2.IDSTATUSMOVIMENTACAO IS NOT NULL) AS `REG. ERRO`,
					   CONCAT('<a href=\"" . URL_EXEC . "application/files/', a.NOME, '\" title=\"Download do arquivo\" target=\"_blank\"><img src=\"" . URL_IMG . "icon_anexo.png\"></a>') AS ' '
					FROM arquivo a
			  INNER JOIN arquivostatus   ast on (a.IDARQUIVOSTATUS = ast.IDARQUIVOSTATUS)
			  INNER JOIN arquivotipolote atl on (a.IDTIPOLOTE = atl.IDTIPOLOTE)
			   LEFT JOIN programa          p on (a.IDPROGRAMA = p.IDPROGRAMA)
				   WHERE a.IDARQUIVOSTATUS != 5
				ORDER BY IDTIPOLOTE, DATA_GERACAO";
		$lista = $this->db->query($sql);
		return $lista->result_array();
    }
    
    function gerenciadorCpbById($idarquivo){
        $sql = "  
                  SELECT
                            a.IDARQUIVO as ID,
                            atl.DESCTIPOLOTE as TIPO,
                            atl.IDTIPOLOTE as TIPOLOTE,
                            ast.DESCSTATUS as STATUS,
                            a.NOME AS NOME,
                            a.DATA_GERACAO AS DATAG,
                            a.IDARQUIVOSTATUS as IDSTATUS,
                            p.DESCPROGRAMA as PROGRAMA,
                            a.IDUSUARIO_GERADOR as IDUSUARIO,
                            (SELECT COUNT(*) FROM arquivo_movimentacao AM2 WHERE IDARQUIVO = a.IDARQUIVO) AS QTDREG
                            from arquivo a
                            inner join arquivostatus ast on (a.IDARQUIVOSTATUS = ast.IDARQUIVOSTATUS )
                            inner join arquivotipolote atl on ( a.IDTIPOLOTE = atl.IDTIPOLOTE )
                            LEFT join programa p on ( a.IDPROGRAMA = p.IDPROGRAMA )
                            where a.IDARQUIVO = " .$idarquivo;                        
                            
        
                $lista = $this->db->query($sql);
                return $lista->result_array();
        
                
    }
    
    function geraConteudoCpbBox($idarquivo){
        $sql = "  
                  SELECT  
                  am.IDMOVIMENTACAO as IDMOVIMENTACAO,
                  a.IDARQUIVO as ID,
                  a.IDTIPOLOTE as LOTE, 
                  ast.DESCSTATUS AS DESCRICAO,
                  a.NOME AS NOME,
                  a.DATA_GERACAO AS DATA,
                  a.IDUSUARIO_GERADOR as IDUSUARIO                  
                  FROM arquivo a
                  INNER JOIN arquivostatus ast ON (a.IDARQUIVOSTATUS = ast.IDARQUIVOSTATUS)
                  left join arquivo_movimentacao am on (a.IDARQUIVO = am.IDARQUIVO )
                  where am.IDMOVIMENTACAO is not null and
                  a.IDARQUIVO = " .$idarquivo;                           
        
                $gera = $this->db->query($sql);
                return $gera->result_array();
        
    }
	
	/**
	* get_dados_arquivo()
	* Retorna dados de arquivo com base no idarquivo encaminhado.
	* @param integer idarquivo 
	* @return array arquivo
	**/
	function get_dados_arquivo($idarquivo = 0){
        $sql = "SELECT  a.IDARQUIVO as ID,
						atl.DESCTIPOLOTE as TIPO,
						atl.IDTIPOLOTE as TIPOLOTE,
						ast.DESCSTATUS as STATUS,
						a.NOME AS NOME,
						a.DATA_GERACAO AS DATAG,
						a.DATA_TRANSMISSAO AS DATATRANS,
						a.IDARQUIVOSTATUS as IDSTATUS,
						p.DESCPROGRAMA as PROGRAMA,
						a.IDUSUARIO_GERADOR as IDUSUARIO,
						(SELECT COUNT(*) FROM arquivo_movimentacao AM2 WHERE IDARQUIVO = a.IDARQUIVO) AS QTDREG
						from arquivo a
						inner join arquivostatus ast on (a.IDARQUIVOSTATUS = ast.IDARQUIVOSTATUS )
						inner join arquivotipolote atl on ( a.IDTIPOLOTE = atl.IDTIPOLOTE )
						LEFT join programa p on ( a.IDPROGRAMA = p.IDPROGRAMA )
						where a.IDARQUIVO = " .$idarquivo;                        
                            
        $result = $this->db->query($sql);
		$return = $result->result_array();
		return(isset($return[0])) ? $return[0] : array();
    }
	
	/**
	* envia_data_process()
	* Recebe a data de transmissão / retransmissão do arquivo.
	* @param integer idarquivo 
	* @return void
	**/
	function envia_data_process( $data = '', $idarquivo = 0)
	{
		$sql = "UPDATE arquivo SET DATA_TRANSMISSAO = '$data " . date('H:i:s') . "', IDARQUIVOSTATUS = 2 where IDARQUIVO = $idarquivo";
		$this->db->query($sql);
	}
	
	/**
	* cancela_arquivo()
	* Aqui fazemos um update para que o status do aquivo mude para cancelado.
	* @param integer idarquivo
	* @return void
	**/
	function cancela_arquivo($idarquivo)
	{
		$sql = "UPDATE arquivo set IDARQUIVOSTATUS = 5 where IDARQUIVO = " .$idarquivo ;
		$this->db->query($sql);
	}
	
	/**
	* get_count_arquivos_nao_transmitidos()
	* Verificando se o arquivo já foi transmitido.
	* @param integer idarquivo
	* @return void
	**/
	function get_count_arquivos_nao_transmitidos()
	{
		$sql = "SELECT IFNULL(count(*),0) AS QTD FROM ARQUIVO WHERE IDARQUIVOSTATUS = 1";
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return(isset($return[0]['QTD'])) ? $return[0]['QTD'] : 0;
			
	} 
	
	/**
	* get_valor_seq_lote()
	* Verifica o maior valor na coluna .
	* @param integer idarquivo
	* @return void
	**/
	function get_valor_seq_lote()
	{
		$sql = "SELECT MAX(`NU-SEQ-LOTE`) AS QTD FROM arquivo where DATE(DATA_GERACAO) = CURRENT_DATE";
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return(isset($return[0]['QTD'])) ? $return[0]['QTD'] : 0;			
	}
	
	
	/**
	* get_arquivo_remessa_by_nu_ctrl_trans()
	* Pega idarquivo pelo tipo de lote e nu-ctrl-trans.
	* @param integer nu_ctrl_trans
	* @param integer tipo_lote
	* @return void
	**/
	function get_arquivo_remessa_by_nu_ctrl_trans($nu_ctrl_trans = 0, $tipo_lote = 0)
	{
		$sql = "SELECT IDARQUIVO FROM arquivo where `NU-CTRL-TRANS` = " . $nu_ctrl_trans . " and IDTIPOLOTE = ". $tipo_lote;
		
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return(isset($return[0]['IDARQUIVO'])) ? $return[0]['IDARQUIVO'] : 0;
			
	}
	
	/**
	* get_arquivo_remessa_em_aberto()
	* Pega idarquivo pelo arquivo que esta em aberto.
	* @param integer nu_ctrl_trans
	* @param integer tipo_lote
	* @return void
	**/
	 function get_arquivo_remessa_em_aberto()
	{
		$sql = "SELECT IDARQUIVO FROM arquivo WHERE idarquivostatus = 2  AND IDTIPOLOTE = 21";
		
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return(isset($return[0]['IDARQUIVO'])) ? $return[0]['IDARQUIVO'] : 0;
	}
	
	/**
	* verifica_identificador_usuario()
	* verifica se o usuario já possui identificador para arquivos me geral.
	* @param integer idusuario
	* @param integer cpf_cnpj
	* @return array count, existe
	**/
	function verifica_identificador_usuario($idusuario = 0, $cpf_cnpj = 0)
	{
		$sql = "SELECT COUNT(*) AS QTDE, 
					CASE WHEN U.IDUSUARIO IS NOT NULL THEN 'S' ELSE 'N' END AS EXISTE
					FROM ARQUIVO_USUARIO_IDENTIFICADOR AM 
					LEFT JOIN USUARIO U ON (U.IDUSUARIO = AM.IDUSUARIO AND U.IDUSUARIO = " .$idusuario . ") 
					WHERE AM.IDUSUARIO IN (SELECT IDUSUARIO FROM USUARIO WHERE REPLACE(REPLACE(CPF_CNPJ, '.', ''), '-', '') = '" . $cpf_cnpj . "');";
			
			$result = $this->db->query($sql);
			$return = $result->result_array();
			return(isset($return[0])) ? $return[0] : 0;
			
	}	
	
	/**
	* atualiza_registro_remessa_correspondente()
	* Atualiza o registro da remessa conforme o numero de sequencia do lote e o idusuario.
	* @param integer nu_ctrl_trans
	* @param integer tipo_lote
	* @return void
	**/
	function atualiza_registro_remessa_correspondente($idarquivoremessa = 0, $idusuario = 0, $ocorrencia_conta = 0)
	{
		$sql = "UPDATE ARQUIVO_MOVIMENTACAO SET IDSTATUSMOVIMENTACAO =  " . $ocorrencia_conta . " WHERE IDUSUARIO = " . $idusuario . " AND IDARQUIVO = " . $idarquivoremessa . " AND IS_RETORNO = 'N' ";
		$this->db->query($sql);
		
	}
	
}
