<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Distribuidor_model extends CI_Model {
	/* filtros */
	protected $ATIVO = '';
	protected $HABILITADO = '';
	protected $RAZAOSOCIAL = '';
	protected $NOMEFANTASIA = '';
	protected $UF = '';
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	function lista($arrParam, $onlyCount = FALSE)
	{
		if ($onlyCount)
		{
			$sql = "SELECT count(*) as CONT FROM caddistribuidor c INNER JOIN usuario u ON (u.IDUSUARIO = c.IDUSUARIO) 
                    LEFT JOIN usuarioprograma up ON (u.IDUSUARIO = up.IDUSUARIO AND up.IDPROGRAMA = " . $this->session->userdata('programa') . ")";
		}
		else
		{
			$sql = "SELECT u.*,
						   c.*,
						   DATE_FORMAT(u.DATA_ATIVACAO, '%d/%m/%Y %H:%i') AS ATIVACAO,
						   CASE 
								WHEN u.ATIVO = 'S' THEN 'v_peq.png'
								ELSE 'x.png'
						   END AS IMGSTATUS,
						   CASE 
								WHEN u.ATIVO = 'S' THEN 'VALIDADO'
								ELSE 'NAO VALIDADO'
						   END AS DESC_STATUS,
						   CASE 
								WHEN up.IDPROGRAMA is not null THEN 'SIM'
								ELSE 'NAO'
						   END AS USER_HABILITADO
					  FROM usuario u
			    INNER JOIN caddistribuidor c ON (u.IDUSUARIO = c.IDUSUARIO)
                LEFT JOIN usuarioprograma up ON (u.IDUSUARIO = up.IDUSUARIO AND up.IDPROGRAMA = " . $this->session->userdata('programa') . ")";
		}
		
		// Filtragem - Serve tanto para o count quanto para dados de listagem
		// Filtro para estado
		if($this->UF != '')
		{
			$sql .= "  INNER JOIN logradouro  l ON (l.IDLOGRADOURO = c.IDLOGRADOURO)
					   INNER JOIN cidade     ci ON (ci.IDCIDADE = l.IDCIDADE AND IDUF = '" . $this->UF . "')";
		}
		
		// Adiciona um Where 1=1 para que os filtros abaixo possam ser utilizados
		$sql .= ' WHERE 1=1 ';
		
		// Filtro para habilitado
		if($this->HABILITADO != '')
		{
			if($this->HABILITADO == 'S')
            {
               $sql .= " and up.IDPROGRAMA is not null ";
            }
            else
            {
               $sql .= " and up.IDPROGRAMA is null ";
            }
		}
		
		// Filtro para status
		if($this->ATIVO != '')
		{
			$sql .= " AND u.ATIVO = '" . $this->ATIVO . "' ";
		}
		
		// Filtro para Razao e Nome Fantasia
		if($this->NOMEFANTASIA != '' && $this->RAZAOSOCIAL != '')
		{
			$sql .= " AND (c.RAZAOSOCIAL LIKE '%" . $this->RAZAOSOCIAL . "%' OR c.NOMEFANTASIA LIKE '%" . $this->NOMEFANTASIA . "%')";
		} else {
			// Filtro para Raz�o social
			if($this->RAZAOSOCIAL != '')
			{
				$sql .= " AND c.RAZAOSOCIAL LIKE '%" . $this->RAZAOSOCIAL . "%' ";
			}
			
			// Filtro para Nome Fantasia
			if($this->NOMEFANTASIA != '')
			{
				$sql .= " AND c.NOMEFANTASIA LIKE '%" . $this->NOMEFANTASIA . "%' ";
			}
		}
		
		// Finaliza SQL, somente para casos onde nao seja count
		if (!$onlyCount)
		{
			// Prossegue o sql
			$sql .= " ORDER BY c.RAZAOSOCIAL LIMIT " . $arrParam['limit_de'] . "," . $arrParam['exibir_pp'];
		}
		
		$dados = $this->db->query($sql);
		return $dados->result_array();
	}
	
	function getDadosCadastro($idUsuario)
	{
		$sql = sprintf("SELECT
							u.*,
							d.*,
							l.CEP,
							c.IDUF,
							c.NOMECIDADESUB,
							b.NOMEBAIRRO,
							l.NOMELOGRADOURO,
							CONCAT(t.NOMELOGRADOUROTIPO,' ',l.NOMELOGRADOURO) AS NOMELOGRADOURO2,
							l.COMPLEMENTO
						FROM usuario u
						INNER JOIN caddistribuidor d ON (u.IDUSUARIO = d.IDUSUARIO)
						LEFT JOIN logradouro     l ON (l.IDLOGRADOURO = d.IDLOGRADOURO)
						LEFT JOIN logradourotipo t ON (t.IDLOGRADOUROTIPO = l.IDLOGRADOUROTIPO)
						LEFT JOIN bairro         b ON (b.IDBAIRRO = l.IDBAIRRO)
						LEFT JOIN cidade         c ON (c.IDCIDADE = b.IDCIDADE)
						WHERE u.IDUSUARIO = %s", $idUsuario);
						
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function setFiltro($coluna = null, $valor = null)
	{
		if(!is_null($coluna) && !is_null($valor))
		{
			$this->{strtoupper($coluna)} = addslashes($valor);
		}
	}
	
	function getArrayFiltros($arrDados = null)
	{
		$dados = array();
		if(!is_null($arrDados) && is_array($arrDados))
		{
			$dados['filtro_status'] = $arrDados['filtro_status'];
			$dados['filtro_habilitado'] = $arrDados['filtro_habilitado'];
			$dados['filtro_razao_nome_fantasia'] = $arrDados['filtro_razao_nome_fantasia'];
			$dados['filtro_estado']	= $arrDados['filtro_estado'];
		} else {
			$dados['filtro_status'] = '';
			$dados['filtro_habilitado'] = '';
			$dados['filtro_razao_nome_fantasia'] = '';
			$dados['filtro_estado']	= '';
		}
		return $dados;
	}
	
	/**
	* get_lista_modulo()
	* Retorna os dados da listagem de entrada do modulo.
	* return array data
	*/
	function get_lista_modulo($filters = array())
	{
		$sql = "SELECT CD.IDUSUARIO AS `#`,
					   CD.RAZAOSOCIAL AS `RAZÃO SOCIAL`,
					   U.CPF_CNPJ AS `CPF/CNPJ`,
					   CD.TELEFONEGERAL AS `TELEFONE`,
					   CD.EMAILGERAL AS `EMAIL`,
					   CD.NOMERESP AS `RESPONSÁVEL`,
					   CD.TELEFONERESP AS `TEL. RESP.`,
					   CD.EMAILRESP AS `EMAIL RESP.`,
					   U.LOGIN,
					   CASE WHEN u.ATIVO = 'N' THEN '<img src=\"" . URL_IMG . "icon_bullet_red.png\" title=\"USUARIO DESATIVADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_bullet_green_2.png\" title=\"USUARIO ATIVO\" />' END AS ATIVO_IMG
				  FROM CADDISTRIBUIDOR CD
			 LEFT JOIN usuarioprograma up ON (up.idusuario = CD.idusuario)
			 LEFT JOIN LOGRADOURO       L ON(L.IDLOGRADOURO = CD.IDLOGRADOURO)
			 LEFT JOIN CIDADE           C ON(C.IDCIDADE = L.IDCIDADE)			 
			INNER JOIN usuario          u ON (u.idusuario  = CD.idusuario)";
		// Carrega model auxiliar para processamento do where
		$this->load->model('global_model');
		$sql .= $this->global_model->get_where_filters($filters);
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados)) ? $dados : array();
	}
	
	/*
	* get_ufs_distribuidores()
	* Retorna dados de ufs que possuem editoras.
	* return array ufs
	*/
	function get_ufs_distribuidores()
	{
		$sql = "SELECT DISTINCT c.IDUF, C.IDUF AS LABEL FROM CADDISTRIBUIDOR CE
			INNER JOIN USUARIO          U ON(U.IDUSUARIO = CE.IDUSUARIO)
			 LEFT JOIN LOGRADOURO       L ON(L.IDLOGRADOURO = CE.IDLOGRADOURO)
			 LEFT JOIN CIDADE           C ON(C.IDCIDADE = L.IDCIDADE)
			 LEFT JOIN USUARIOPROGRAMA UP ON(UP.IDUSUARIO = U.IDUSUARIO)
			  ORDER BY IDUF";
		$dados  = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}
	
    /*
	* get_dados_distribuidor()
	* Retorna os dados de um distribuidor
	* @param integer idusuario
	* return array biblioteca
	*/
	function get_dados_distribuidor($idusuario = 0, $idprograma = 0)
	{
		$sql = "SELECT CE.IDUSUARIO AS IDEDITORA, 
					   CE.* ,
					   U.*,
					   L.*,
					   L.CEP AS `CEP_LOGRADOURO`,
					   LT.*,
					   B.*,
					   C.*,
					   UF.*,
					   CASE WHEN up.IDPROGRAMA IS NULL THEN 'N' ELSE 'S' END AS HABILITADO,
					   CE.IDUSUARIO,
					   CASE WHEN u.ATIVO = 'N' THEN '<img src=\"" . URL_IMG . "icon_bullet_red.png\" title=\"USUARIO DESATIVADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_bullet_green_2.png\" title=\"USUARIO ATIVO\" />' END AS ATIVO_IMG,
					   CASE WHEN up.IDPROGRAMA IS NULL THEN '<img src=\"" . URL_IMG . "icon_flag_red.png\" title=\"USUARIO DESABILITADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_flag_green.png\" title=\"USUARIO HABILITADO\" />' END AS HABILITADO_IMG
				  FROM CADDISTRIBUIDOR   CE 
		    INNER JOIN USUARIO          U ON (U.IDUSUARIO = CE.IDUSUARIO) 
		     LEFT JOIN LOGRADOURO       L ON (L.IDLOGRADOURO = CE.IDLOGRADOURO) 
		     LEFT JOIN LOGRADOUROTIPO  LT ON (LT.IDLOGRADOUROTIPO = L.IDLOGRADOUROTIPO)
			 LEFT JOIN BAIRRO           B ON (B.IDBAIRRO = L.IDBAIRRO)
		     LEFT JOIN CIDADE           C ON (C.IDCIDADE = L.IDCIDADE)
			 LEFT JOIN UF              UF ON (UF.IDUF = C.IDUF)
			 LEFT JOIN USUARIOPROGRAMA UP ON (UP.IDUSUARIO = CE.IDUSUARIO AND UP.IDPROGRAMA = $idprograma)
			WHERE CE.IDUSUARIO = $idusuario";	
		$dados = $this->db->query($sql);		
		$dados = $dados->result_array();
		
		return (isset($dados[0])) ? $dados[0] : array();
	}
}

/* End of file Distribuidor_model.php */
/* Location: ./system/application/models/Distribuidor_model.php */