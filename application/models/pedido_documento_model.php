<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe Pedido_Documento_Model
*
* Esta classe contém métodos para a abstração da entidade model pedido_documento.
* 
* @author		Leandro Mangini Antunes
* @package		application
* @subpackage	application.pedido_documento
* @since		2012-04-27
*
*/
class Pedido_Documento_Model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	/**
	* get_anexos()
	* Retorna todos os anexos de um documento.
	* @param integer id_pedido_documento
	* @return array documento
	*/
	function get_anexos($id_pedido_documento = 0)
	{
		$sql = "SELECT DA.ID_PEDIDO_DOCUMENTO_ANEXO,
					   DA.ID_PEDIDO_DOCUMENTO,
					   DA.ARQUIVO,
					   DA.ARQUIVO_ORIGINAL,
					   DA.NUMERO_DOC,
					   DA.VALOR_DOC,
					   DA.DTT_INS,
					   PD.IDUSUARIOTIPO
				  FROM pedido_documento_anexo DA
			INNER JOIN pedido_documento PD ON (PD.ID_PEDIDO_DOCUMENTO = DA.ID_PEDIDO_DOCUMENTO)
			     WHERE da.id_pedido_documento = $id_pedido_documento";
		// echo($sql);
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return(isset($return)) ? $return : array();
	}
	
	/**
	* get_tipos_documentos()
	* Retorna todos os tipos de documento, para montar options de insercao de docs.
	* @param integer id_pedido_documento
	* @return array documento
	*/
	function get_tipos_documentos()
	{
		$sql = "SELECT ID_PEDIDO_DOCUMENTO_TIPO, NOME, DESCRICAO FROM pedido_documento_tipo ORDER BY NOME";
		// echo($sql);
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return(isset($return)) ? $return : array();
	}
	
	/**
	* get_dados_documento()
	* Retorna todos os dados de um documento, somente.
	* @param integer id_pedido_documento
	* @return array documento
	*/
	function get_dados_documento($id_pedido_documento = 0)
	{
		$sql = "SELECT PD.*, PDT.* FROM PEDIDO_DOCUMENTO PD LEFT JOIN PEDIDO_DOCUMENTO_TIPO PDT ON (PD.ID_PEDIDO_DOCUMENTO_TIPO = PDT.ID_PEDIDO_DOCUMENTO_TIPO) WHERE ID_PEDIDO_DOCUMENTO = $id_pedido_documento ORDER BY idusuariotipo";
		// echo($sql);
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return(isset($return[0])) ? $return[0] : array();
	}
	
	/**
	* get_dados_anexo()
	* Retorna todos os dados de um documento, somente.
	* @param integer id_pedido_documento
	* @return array documento
	*/
	function get_dados_anexo($id_pedido_documento_anexo = 0)
	{
		$sql = "SELECT * FROM pedido_documento_anexo WHERE id_pedido_documento_anexo = $id_pedido_documento_anexo";
		// echo($sql);
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return(isset($return[0])) ? $return[0] : array();
	}
	
	/**
	* get_itens_documento()
	* Retorna todos os itens (livros) relacionados a um documento.
	* @param integer id_pedido_documento
	* @return array itens
	*/
	function get_itens_documento($id_pedido_documento = 0)
	{
		$sql = "SELECT PL.QTD,
					   PL.QTD_ENTREGUE,
					   PL.PRECO_UNIT,
					   (PL.PRECO_UNIT * QTD_ENTREGUE) AS SUBTOTAL,
					   L.TITULO,
					   L.IDLIVRO
				  FROM PEDIDOLIVRO PL
			INNER JOIN LIVRO L ON (L.IDLIVRO = PL.IDLIVRO)
			     WHERE PL.ID_PEDIDO_DOCUMENTO = $id_pedido_documento";
		// echo($sql);
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return(isset($return)) ? $return : array();
	}
	
	/**
	* get_sum_vlr_itens_documento()
	* Retorna a soma dos subtotais de todos os itens (livros) relacionados a um documento.
	* @param integer id_pedido_documento
	* @return array itens
	*/
	function get_sum_vlr_itens_documento($id_pedido_documento = 0)
	{
		$sql = "SELECT IFNULL(SUM(PL.PRECO_UNIT * QTD_ENTREGUE),0) AS SOMA
				  FROM PEDIDOLIVRO PL
			INNER JOIN LIVRO L ON (L.IDLIVRO = PL.IDLIVRO)
			     WHERE PL.ID_PEDIDO_DOCUMENTO = $id_pedido_documento";
		// echo($sql);
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return(isset($return[0]['SOMA'])) ? $return[0]['SOMA'] : 0;
	}
	
	/**
	* get_documento_by_numero()
	* Retorna um documento pelo seu numero, idpedido e tipo (NF, COMPROVANTE, ETC).
	* @param integer numero_doc
	* @param integer idpedido
	* @param integer id_tipo
	* @return array item
	*/
	function get_documento_by_numero($numero_doc = 0, $idpedido = 0, $id_tipo = 0)
	{
		$sql = "SELECT ID_PEDIDO_DOCUMENTO FROM PEDIDO_DOCUMENTO WHERE numero_doc = '$numero_doc' AND id_pedido_documento_tipo = $id_tipo AND idpedido = $idpedido";
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return(isset($return[0]['ID_PEDIDO_DOCUMENTO'])) ? $return[0]['ID_PEDIDO_DOCUMENTO'] : 0;
	}
	
	/**
	* existe_doc_equivalente()
	* Retorna o numero do documento informando se existe um documento equivalente para o documento que está 
	* sendo encaminhado. Verifica pelo tipo de doc encaminhado e número da entrega.
	* @param integer numero_doc
	* @param integer id_pedido_documento_tipo
	* @param integer numero_entrega
	* @return integer id_pedido_documento
	*/
	function existe_doc_equivalente($numero_doc = 0, $tipo_documento = 0, $numero_entrega = 0, $idpedido = 0)
	{
		$sql = "SELECT ID_PEDIDO_DOCUMENTO FROM PEDIDO_DOCUMENTO WHERE numero_doc = '$numero_doc' AND id_pedido_documento_tipo = $tipo_documento AND numero_entrega = $numero_entrega AND idpedido = $idpedido";
		// echo($sql);
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return(isset($return[0]['ID_PEDIDO_DOCUMENTO'])) ? $return[0]['ID_PEDIDO_DOCUMENTO'] : 0;
	}
	
	/**
	* set_notas_conferidas()
	* Seta o array de itens encaminhado como conferidos, isto é, marca o número das 
	* notas atuais com o numero da entrega encaminhada.
	* @param array notas
	* @param integer numero_entrega
	* @return void
	*/
	function set_notas_conferidas($notas = array(), $numero_entrega = 0)
	{
		$ids = '';
		foreach($notas as $nota){ $ids .= get_value($nota, 'ID_PEDIDO_DOCUMENTO') . ','; }
		$ids = trim($ids, ',');
		$sql = "UPDATE PEDIDO_DOCUMENTO SET NUMERO_ENTREGA = $numero_entrega WHERE ID_PEDIDO_DOCUMENTO IN($ids)";
		// echo($sql);
		$result = $this->db->query($sql);
	}
	
	/**
	* get_id_pedido_documento_item()
	* Retorna o id do documento de um livro de um pedido.
	* @param integer idlivro
	* @param integer idpedido
	* @return integer id_pedido_documento
	*/
	function get_id_pedido_documento_item($idlivro = 0, $idpedido = 0)
	{
		$sql = "SELECT ID_PEDIDO_DOCUMENTO FROM pedidolivro WHERE idlivro = $idlivro AND idpedido = $idpedido";
		// echo($sql);
		$result = $this->db->query($sql);
		$return = $result->result_array();
		return(isset($return[0]['ID_PEDIDO_DOCUMENTO'])) ? $return[0]['ID_PEDIDO_DOCUMENTO'] : 0;
	}
}
