<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logsis_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	function lista($arrParam, $onlyCount = FALSE)
	{
		if ($onlyCount)
		{
			$sql = sprintf("SELECT count(*) as CONT FROM logsis l %s ", $arrParam['where']);
		}
		else
		{
			$sql = sprintf("SELECT 
								l.IDLOGSIS,
								l.IDUSUARIO,
								u.IDUSUARIOTIPO,
								l.RESULTADO,
								l.DESCRICAO,
								DATE_FORMAT(l.DATALOG, '%%d/%%m/%%Y') AS DATALOG,
								l.HORALOG,
								l.IPLOG,
                                ut.NOMEUSUARIOTIPO
							FROM logsis l
							INNER JOIN usuario     u  ON (l.IDUSUARIO = u.IDUSUARIO)
                            INNER JOIN usuariotipo ut ON (u.IDUSUARIOTIPO = ut.IDUSUARIOTIPO) 
							%s ORDER BY l.DATALOG, l.HORALOG  
							LIMIT %s, %s", $arrParam['where'], $arrParam['limit_de'], $arrParam['exibir_pp']);
		}
		
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
}

/* End of file logsis_model.php */
/* Location: ./system/application/models/logsis_model.php */