<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe _Exemplo_Model
*
* <DESCRI��O DA CLASSE AQUI> Exemplo de classe model.
* 
* @author		<AUTOR>
* @package		application
* @subpackage	models.<nome_controller>
* @since		<DATA>
*
*/
class _Exemplo_Model extends CI_Model {
	
	/**
	* __construct()
	* @return object
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	/**
	* metodo_exemplo()
	* <DESCRI��O M�TODO AQUI> Exemplo de m�todo.
	* @param type name
	* @return type name
	*/
	function metodo_exemplo()
	{
		// Do something here
	}
}
