<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe Livro_model
*
* Abstração da camada modelo para relatórios no sistema.
* 
* @author		Ricardo Neves Becker
* @package		application
* @subpackage	application.livros
* @since		2012-06-14
*
*/
class Livro_model extends CI_Model {
	/* filtros */
	protected $EDITORA = '';
	protected $AUTOR = '';
	protected $TITULO = '';
	protected $SINOPSE = '';
	protected $PALAVRASCHAVE = '';
	protected $CONFIRMACAO = '';
	protected $ISBN = '';
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	function listaSuporte()
	{
		$sql = "SELECT * FROM isbnsuporte ORDER BY CASE WHEN IDISBNSUPORTE = 1 THEN 1 ELSE 2 END, DESCSUPORTE";
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function listaTipoCapa()
	{
		$sql = "SELECT * FROM isbntipocapa ORDER BY DESCTIPOCAPA";
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function listaAcabamento()
	{
		$sql = "SELECT * FROM isbnacabamento ORDER BY DESCACABAMENTO";
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function listaIdioma()
	{
		$sql = "SELECT * FROM isbnidioma ORDER BY CASE WHEN IDISBNIDIOMA = 2210 THEN 1 ELSE 2 END, DESCIDIOMA";
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function listaAssunto($itempesq)
	{
		$sql = sprintf("SELECT * FROM isbnassunto WHERE DESCASSUNTO LIKE '%%%s%%' ORDER BY DESCASSUNTO", $itempesq);
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
        
        function listaLink ($linkdados){
            $sql = sprintf('SELECT LINKDADOS AS LINK FROM `binac`.`livro` where LINKDADOS != "" ');
            $dados = $this->db->query($sql);
		
		return $dados->result_array();
        }
	
	function getDadosCadastro($idLivro)
	{
		$sql = sprintf("SELECT
							l.*,
							c.DESCTIPOCAPA,
							a.DESCACABAMENTO,
							s.DESCSUPORTE,
							o.DESCASSUNTO,
							i.DESCIDIOMA
						FROM livro l
						INNER JOIN cadeditora    e ON (l.IDUSUARIO = e.IDUSUARIO)
						LEFT JOIN isbntipocapa   c ON (l.IDISBNTIPOCAPA = c.IDISBNTIPOCAPA)
						LEFT JOIN isbnacabamento a ON (l.IDISBNACABAMENTO = a.IDISBNACABAMENTO)
						LEFT JOIN isbnsuporte    s ON (l.IDISBNSUPORTE = s.IDISBNSUPORTE)
						LEFT JOIN isbnassunto    o ON (l.IDISBNASSUNTO = o.IDISBNASSUNTO)
						LEFT JOIN isbnidioma     i ON (l.IDISBNIDIOMA = i.IDISBNIDIOMA)
						WHERE l.IDLIVRO = %s", $idLivro);
		
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function lista($arrParam, $onlyCount = FALSE)
	{
		if ($onlyCount)
		{
			$sql = sprintf("SELECT count(*) as CONT 
                            FROM livro l 
                            INNER JOIN cadeditora e ON (l.IDUSUARIO = e.IDUSUARIO)
                            LEFT JOIN programalivro pl ON (l.IDLIVRO = pl.IDLIVRO %s)
                            LEFT JOIN programa      p  ON (p.IDPROGRAMA = pl.IDPROGRAMA)
                            WHERE %s ", $arrParam['programa'],  $arrParam['where']); 
		}
		else
		{
            $limit = ($arrParam['limit_de'] == 0 && $arrParam['exibir_pp'] == 0) ? '' : 'LIMIT ' . $arrParam['limit_de'] .' , ' . $arrParam['exibir_pp'];
			
			$sql = sprintf("SELECT 	l.*,
                                    e.RAZAOSOCIAL AS EDITORA,
                                    DATE_FORMAT(l.DATA_ULT_ATU, '%%d/%%m/%%Y %%H:%%i') AS DATA_ATU,
                                    CASE 
                                        WHEN l.CONFIRMACAO = 'S' THEN 'v_peq.png'
                                        ELSE 'x.png'
                                    END AS IMGSTATUS,
                                    CASE 
                                        WHEN l.CONFIRMACAO = 'S' THEN 'CONFIRMADO'
                                        ELSE 'NAO CONFIRMADO'
                                    END AS DESC_STATUS,
									CASE
										WHEN l.ANO <= 2006 THEN SUBSTRING(ISBN, 3,10)
										ELSE ISBN
									END AS ISBN_MOSTRAR,
                                    pl.PRECO AS PRECOP,
                                    pl.QTD_DISPONIVEL AS QTDP,
                                    pl.IDLIVRO AS LIVROPOPULAR,
                                    p.DESCPROGRAMA AS PROGRAMA
                             FROM 	livro l
                             INNER JOIN cadeditora e ON (l.IDUSUARIO = e.IDUSUARIO)
                             LEFT JOIN programalivro pl ON (l.IDLIVRO = pl.IDLIVRO %s)
                             LEFT JOIN programa      p  ON (p.IDPROGRAMA = pl.IDPROGRAMA)
                             WHERE %s  
                             ORDER BY l.CONFIRMACAO, l.TITULO $limit", $arrParam['programa'], $arrParam['where']);
		}

		$dados = $this->db->query($sql);
		return $dados->result_array();
	}
        
    function listaGeral($arrParam, $onlyCount = FALSE)
	{
		if ($onlyCount)
		{
			$sql = "SELECT count(*) as CONT FROM livro l INNER JOIN cadeditora e ON (l.IDUSUARIO = e.IDUSUARIO) ";
		}
		else
		{
			$sql = "
				SELECT 	l.*,
						DATE_FORMAT(l.DATA_ULT_ATU, '%d/%m/%Y %H:%i') AS DATA_ATU,
						e.RAZAOSOCIAL AS EDITORA,
						CASE 
							WHEN l.CONFIRMACAO = 'S' THEN 'v_peq.png'
							ELSE 'x.png'
						END AS IMGSTATUS,
						CASE 
							WHEN l.CONFIRMACAO = 'S' THEN 'CONFIRMADO'
							ELSE 'NAO CONFIRMADO'
						END AS DESC_STATUS
				   FROM livro l
			 INNER JOIN cadeditora e ON (l.IDUSUARIO = e.IDUSUARIO)";
		}
		
		// Filtragem - Serve tanto para o count quanto para dados de listagem
		// Adiciona um Where 1=1 para que os filtros abaixo possam ser utilizados (SOMENTE PARA COUNT)
		$sql .= ' WHERE 1=1 ';

		// Filtro para Editora (n�o � necess�rio pois este m�todo � utilizado por editoras fixas
		if($this->EDITORA != '')
		{
			$sql .= " AND (e.RAZAOSOCIAL LIKE '%" . $this->EDITORA . "%' OR e.NOMEFANTASIA LIKE '%" . $this->EDITORA . "%') ";
		}
		
		// Filtro para confirmação
		if($this->CONFIRMACAO != '')
		{
			$sql .= " AND l.CONFIRMACAO = '" . $this->CONFIRMACAO . "' ";
		}
		
		// Filtro para confirmação
		if($this->ISBN != '')
		{
			$sql .= " AND l.ISBN = '" . $this->ISBN . "' ";
		}
		
		// Filtro para Título
		if($this->TITULO != '')
		{
			$sql .= " AND l.TITULO LIKE '%" . $this->TITULO . "%' ";
		}
		
		// Filtro para Autor
		if($this->AUTOR != '')
		{
			$sql .= " AND l.AUTOR LIKE '%" . $this->AUTOR . "%' ";
		}
		
		// Filtro para Sinopse
		if($this->SINOPSE != '')
		{
			$sql .= " AND l.SINOPSE LIKE '%" . $this->SINOPSE . "%' ";
		}
		
		// Filtro para Palavras-Chave
		if($this->PALAVRASCHAVE != '')
		{
			$sql .= " AND l.PALAVRASCHAVE LIKE '%" . $this->PALAVRASCHAVE . "%' ";
		}
		
		// Finaliza SQL, somente para casos onde nao seja count
		if (!$onlyCount)
		{
			// Prossegue o sql
			$sql .= " ORDER BY l.CONFIRMACAO, l.TITULO LIMIT " . $arrParam['limit_de'] . "," . $arrParam['exibir_pp'];
		}
	
		$dados = $this->db->query($sql);
		return $dados->result_array();
	}
	
	function listaPesquisa($arrParam, $onlyCount = FALSE)
	{
		if ($onlyCount)
		{
			$sql = "SELECT count(*) as CONT FROM livro l 
                INNER JOIN cadeditora e ON (l.IDUSUARIO = e.IDUSUARIO) 
                INNER JOIN programalivro pl ON (pl.IDLIVRO  = l.IDLIVRO AND pl.IDPROGRAMA = " . $this->session->userdata('programa') . ") ";
		}
		else
		{
			$sql = "
				SELECT 	l.*,
						pl.*,
						DATE_FORMAT(l.DATA_ULT_ATU, '%d/%m/%Y %H:%i') AS DATA_ATU,
						e.RAZAOSOCIAL AS EDITORA,
						CASE 
							WHEN l.CONFIRMACAO = 'S' THEN 'v_peq.png'
							ELSE 'x.png'
						END AS IMGSTATUS,
						CASE 
							WHEN l.CONFIRMACAO = 'S' THEN 'CONFIRMADO'
							ELSE 'NAO CONFIRMADO'
						END AS DESC_STATUS";
			$sql .= (isset($arrParam['idPedido']) && $arrParam['idPedido'] != '') ? ", pe.IDPEDIDO" : ", 0 AS IDPEDIDO";
			$sql .= " FROM livro l
			 INNER JOIN cadeditora     e ON (l.IDUSUARIO = e.IDUSUARIO) 
			 INNER JOIN programalivro pl ON (pl.IDLIVRO  = l.IDLIVRO AND pl.IDPROGRAMA = " . $this->session->userdata('programa') . " ) 
			  LEFT JOIN pedidolivro   pe ON (pe.IDLIVRO  = pl.IDLIVRO ";
			$sql .= (isset($arrParam['idPedido']) && $arrParam['idPedido'] != '') ? " AND pe.IDPEDIDO = " . $arrParam['idPedido'] . ")" : ")";
		}
		
		// Filtragem - Serve tanto para o count quanto para dados de listagem
		// Adiciona um Where 1=1 para que os filtros abaixo possam ser utilizados (SOMENTE PARA COUNT)
		$sql .= ' WHERE 1=1 ';

		// Filtro para Editora (não é necessário pois este método é utilizado por editoras fixas
		if($this->EDITORA != '')
		{
			$sql .= " AND (e.RAZAOSOCIAL LIKE '%" . htmlspecialchars($this->EDITORA) . "%' OR e.NOMEFANTASIA LIKE '%" . htmlspecialchars($this->EDITORA) . "%') ";
		}
		
		// Filtro para confirmação
		if($this->CONFIRMACAO != '')
		{
			$sql .= " AND l.CONFIRMACAO = '" . $this->CONFIRMACAO . "' ";
		}
		
		// Filtro para confirmação
		if($this->ISBN != '')
		{
			$sql .= " AND l.ISBN = '" . $this->ISBN . "' ";
		}
		
		// Filtro para Título
		if($this->TITULO != '')
		{
			$sql .= " AND l.TITULO LIKE '%" . $this->TITULO . "%' ";
		}
		
		// Filtro para Autor
		if($this->AUTOR != '')
		{
			$sql .= " AND l.AUTOR LIKE '%" . $this->AUTOR . "%' ";
		}
		
		// Filtro para Sinopse
		if($this->SINOPSE != '')
		{
			$sql .= " AND l.SINOPSE LIKE '%" . $this->SINOPSE . "%' ";
		}
		
		// Filtro para Palavras-Chave
		if($this->PALAVRASCHAVE != '')
		{
			$sql .= " AND l.PALAVRASCHAVE LIKE '%" . $this->PALAVRASCHAVE . "%' ";
		}
		
		// Finaliza SQL, somente para casos onde nao seja count
		if (!$onlyCount)
		{
			// Prossegue o sql
			$sql .= " ORDER BY l.TITULO LIMIT " . $arrParam['limit_de'] . "," . $arrParam['exibir_pp'];
		}
		
		$dados = $this->db->query($sql);
		return $dados->result_array();
	}
	
	function setFiltro($coluna = null, $valor = null)
	{
		if(!is_null($coluna) && !is_null($valor))
		{
			$this->{strtoupper($coluna)} = addslashes($valor);
		}
	}
	
	function getArrayFiltros($arrDados = null)
	{
		$dados = array();
		if(!is_null($arrDados) && is_array($arrDados))
		{
			$dados['filtro_autor'] = $arrDados['filtro_autor'];
			$dados['filtro_status'] = $arrDados['filtro_status'];
			$dados['filtro_titulo'] = $arrDados['filtro_titulo'];
			$dados['filtro_sinopse'] = $arrDados['filtro_sinopse'];
			$dados['filtro_palavraschave'] = $arrDados['filtro_palavraschave'];
			$dados['filtro_editora'] = $arrDados['filtro_editora'];
			$dados['filtro_isbn']	= $arrDados['filtro_isbn'];
			$dados['visualizacao']	= $arrDados['visualizacao'];
		} else {
			$dados['filtro_autor'] = '';
			$dados['filtro_status'] = '';
			$dados['filtro_titulo'] = '';
			$dados['filtro_sinopse'] = '';
			$dados['filtro_palavraschave'] = '';
			$dados['filtro_editora'] = '';
			$dados['filtro_isbn']	= '';
			$dados['visualizacao']	= '';
		}
		return $dados;
	}
    
    function saveProgramaLivro($arrParam = null)
    {
        if(!is_null($arrParam) && is_array($arrParam))
        {
            $sql = sprintf("INSERT programalivro (IDPROGRAMA, IDLIVRO, PRECO, QTD_DISPONIVEL) 
                                   VALUES (%s,%s,%s,%s)  
                                ON DUPLICATE KEY UPDATE PRECO = %s, QTD_DISPONIVEL = %s ", 
                $this->session->userdata('programa'), $arrParam['idlivro'],
                $arrParam['preco'], $arrParam['quantidade'],
                $arrParam['preco'], $arrParam['quantidade']);
		
            $dados = $this->db->query($sql);
		
            return true;
        }
    }
	
	/**
	* getDadosLivro()
	* Retorna os dados do livro.
	* return array data
	*/
	function getDadosLivro($idLivro = null)
	{
		$return = array();
		
		if( ! is_null($idLivro))
        {
            $sql = "SELECT 
						l.IDLIVRO, 
						l.TITULO,
						l.AUTOR,
						l.EDICAO,
						l.ANO,
						l.PEDIDO_OBRIGATORIO,
						e.RAZAOSOCIAL, 
						pl.PRECO 
					  FROM livro l 
				INNER JOIN cadeditora e ON (e.IDUSUARIO = l.IDUSUARIO) 
				 LEFT JOIN programalivro pl ON (pl.IDLIVRO = l.IDLIVRO)
					 WHERE l.IDLIVRO = $idLivro";
		    $dados  = $this->db->query($sql);
			$return = $dados->result_array();
			$return = $return[0];
        }
		
		return $return;
	}
	
	function getQtdeDisponivel($idLivro = null)
	{
		$return = 0;
		
		if(!is_null($idLivro))
		{
			$sql = "SELECT p.IDPROGRAMA, p.IDLIVRO, (p.QTD_DISPONIVEL - IFNULL(SUM(pe.QTD_ENTREGUE),0)) AS DISPONIVEL 
					  FROM programalivro p
					  LEFT JOIN pedidolivro pe ON (p.IDLIVRO = pe.IDLIVRO)
					 WHERE p.IDLIVRO = $idLivro AND p.IDPROGRAMA = " . $this->session->userdata('programa') . "
					 GROUP BY p.IDPROGRAMA, p.IDLIVRO";
			$dados  = $this->db->query($sql);
			$arrRet = $dados->result_array();
			$return = (count($arrRet) > 0 ? $arrRet[0]['DISPONIVEL'] : 0);
		}
		
		return $return;
	}
	
	function verificaMovLivro($idPrograma, $idLivro)
	{
		$sql = sprintf("SELECT COUNT(*) AS CONT 
						FROM pedidolivro pl
						INNER JOIN pedido p ON (pl.IDPEDIDO = p.IDPEDIDO)
						WHERE p.IDPROGRAMA = %s
						  AND pl.IDLIVRO = %s", $idPrograma, $idLivro);
		
		$dados = $this->db->query($sql);
		
		if ($dados->num_rows() > 0)
		{
			$row = $dados->row();
			return $row->CONT;
		}
		else
		{
			return 0;
		}
	}
	
	/**
	* validaLivroPrograma()
	* Valida se livro está no programa
	*/
	function validaLivroPrograma($idPrograma, $idLivro)
	{
		$sql = sprintf("SELECT 1 AS CONT 
						FROM programalivro pl
						WHERE pl.IDPROGRAMA = %s
						  AND pl.IDLIVRO = %s", $idPrograma, $idLivro);
		
		$dados = $this->db->query($sql);
		
		if ($dados->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	function livrosParticipantes($arrParam, $onlyCount = FALSE)
	{
		if ($onlyCount)
		{
			$sql = sprintf("SELECT count(*) as CONT 
                            FROM livro l 
                            INNER JOIN cadeditora e ON (l.IDUSUARIO = e.IDUSUARIO)
                            INNER JOIN programalivro pl ON (l.IDLIVRO = pl.IDLIVRO AND pl.IDPROGRAMA = %s) 
                            WHERE %s ", IDPROGRAMA,  $arrParam['where']); 
		}
		else
		{
            $limit = ($arrParam['limit_de'] == 0 && $arrParam['exibir_pp'] == 0) ? '' : 'LIMIT ' . $arrParam['limit_de'] .' , ' . $arrParam['exibir_pp'];
			
			$sql = sprintf("SELECT 	l.*, PRECO AS PRECO ,  e.NOMEFANTASIA AS EDITORA_FANTASIA, LINKDADOS as LINK,
				CASE
				WHEN l.ANO <= 2006 THEN SUBSTRING(ISBN, 3,10)
				ELSE ISBN
				END AS ISBN_MOSTRAR
                             FROM livro l
                             INNER JOIN cadeditora e ON (l.IDUSUARIO = e.IDUSUARIO)
                             INNER JOIN programalivro pl ON (l.IDLIVRO = pl.IDLIVRO AND pl.IDPROGRAMA = %s)
                             WHERE %s  
                             ORDER BY  l.TITULO, e.RAZAOSOCIAL $limit", IDPROGRAMA, $arrParam['where']);
                        
                        
            
		}
		
		$dados = $this->db->query($sql);
		return $dados->result_array();
	}
	
	/**
	* is_inexigivel()
	* Retorna booleano se livro é inexigível ou não.
	* @param integer idlivro
	* @return boolean is_inexigivel
	*/
	function is_inexigivel($idlivro)
	{
		$sql = "SELECT L.PEDIDO_OBRIGATORIO FROM LIVRO L WHERE IDLIVRO = $idlivro";
		// echo($sql);
		$dados = $this->db->query($sql);
		$result  = $dados->result_array();
		$return = (!isset($result[0]['PEDIDO_OBRIGATORIO']) || $result[0]['PEDIDO_OBRIGATORIO'] == 'S') ? false : true;
		return $return;
	}
	
	/**
	* get_status_programa()
	* Retorna o tipo de programa.
	* @return array status
	*/
	function get_status_programa()
	{
		$return = array();
		// Coleta os dados de status
		$sql = "SELECT IDPROGRAMA, DESCPROGRAMA FROM PROGRAMA";
		$result = $this->db->query($sql);
		$return = $result->result_array();
		$return = (isset($return)) ? $return : array();
		return $return;
	}
	
	/**
	* get_lista_modulo()
	* Retorna os dados da listagem de entrada do modulo.
	* return array data
	*/
	function get_lista_modulo($filters = array(), $idlivro = 0)
	{
		$sql = "SELECT 	l.IDLIVRO as '#',
						l.ISBN as ISBN,
						e.RAZAOSOCIAL AS EDITORA,
						L.AUTOR,
						L.TITULO,
						L.EDICAO,
						L.ANO,
						P.DESCPROGRAMA AS EDITAL,
						DATE_FORMAT(l.DATA_ULT_ATU, '%d/%m/%Y %H:%i') AS `ATUALIZADO EM`,        
						CASE 
							WHEN l.CONFIRMACAO = 'S' THEN 'v_peq.png'
							ELSE 'x.png'
						END AS IMGSTATUS,
						CASE 
							WHEN l.CONFIRMACAO = 'S' THEN 'CONFIRMADO'
							ELSE 'NAO CONFIRMADO'
						END AS DESC_STATUS,
						p.IDPROGRAMA,
						p.*,
						pl.*,
						CASE WHEN l.PEDIDO_OBRIGATORIO = 'S' THEN '<img src=\"" . URL_IMG . "icon_bullet_green_2.png\" title=\"EXIGÍVEL EM PEDIDOS\" />' ELSE '<img src=\"" . URL_IMG . "icon_bullet_red.png\" title=\"INEXIGÍVEL EM PEDIDOS\" />' END AS IMG_INEXIGIVEL
						FROM livro l
						INNER JOIN cadeditora      e  ON (l.IDUSUARIO  = e.IDUSUARIO)
						 LEFT JOIN programalivro   pl ON (l.IDLIVRO    = pl.IDLIVRO)
						 LEFT JOIN programa        p  ON (p.IDPROGRAMA = pl.IDPROGRAMA)";
		
		// Carrega model auxiliar para processamento do where
		$this->load->model('global_model');
		$sql .= $this->global_model->get_where_filters($filters);
		
		// Concatena ORDER BY
		$sql .= ' ORDER BY l.IDLIVRO, e.RAZAOSOCIAL';
		
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados)) ? $dados : array();
	}
	
	/**
	* inexigibilidade_livro()
	* Recebe a data de transmissão / retransmissão do arquivo.
	* @param integer idlivro 
	* @param varchar inex 
	* @return void
	**/
	function inexigibilidade_livro($idlivro = 0, $inex = '')
	{
		$inexigivel = ($inex == '' || $inex == 'S') ? 'S' : 'N';
		$sql = "UPDATE livro SET PEDIDO_OBRIGATORIO = '$inexigivel' where IDLIVRO = " . $idlivro;
		$this->db->query($sql);
	}
	
	/**
	* inexigibilidade_livro()
	* Recebe a data de transmissão / retransmissão do arquivo.
	* @param integer idlivro 
	* @param varchar inex 
	* @return void
	**/
	function get_lista_inexigiveis()
	{
		$sql = "SELECT E.RAZAOSOCIAL AS EDITORA,
					   L.AUTOR,
					   L.TITULO,
					   L.EDICAO,
					   L.ANO, 
					   L.IDLIVRO
				  FROM LIVRO L
			INNER JOIN cadeditora      E  ON (L.IDUSUARIO  = E.IDUSUARIO)
			 LEFT JOIN programalivro   PL ON (L.IDLIVRO    = PL.IDLIVRO)
			 LEFT JOIN programa        P  ON (P.IDPROGRAMA = PL.IDPROGRAMA)
			     WHERE L.PEDIDO_OBRIGATORIO = 'N'";
		// Carrega model auxiliar para processamento do where
		$this->load->model('global_model');
		
		// Concatena ORDER BY
		$sql .= ' ORDER BY L.IDLIVRO, E.RAZAOSOCIAL';
		
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados)) ? $dados : array();
	}
}

/* End of file Livro_model.php */
/* Location: ./system/application/models/Livro_model.php */