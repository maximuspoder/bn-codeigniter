<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comentario_model extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	function listaComentarios($idLivro)
	{
		$sql = sprintf("SELECT c.*, DATE_FORMAT(c.DATACOMENTARIO, '%%d/%%m/%%Y %%H:%%i') AS DATACOMENTARIO2, b.RAZAOSOCIAL
						FROM livrocomentario c 
						INNER JOIN cadbiblioteca b ON (c.IDUSUARIO = b.IDUSUARIO)
						WHERE c.IDLIVRO = %s 
						ORDER BY c.DATACOMENTARIO", $idLivro);
		
		$dados = $this->db->query($sql);
		return $dados->result_array();
	}
	
}

/* End of file Comentario_model.php */
/* Location: ./system/application/models/Comentario_model.php */