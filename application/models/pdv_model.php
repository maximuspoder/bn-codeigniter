<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pdv_model extends CI_Model {
	/* filtros */
	protected $ATIVO = '';
	protected $HABILITADO = '';
	protected $RAZAOSOCIAL = '';
	protected $NOMEFANTASIA = '';
	protected $UF = '';
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	function lista($arrParam, $onlyCount = FALSE)
	{
		if ($onlyCount)
		{
			$sql = "SELECT count(*) as CONT FROM cadpdv c INNER JOIN usuario u ON (u.IDUSUARIO = c.IDUSUARIO) 
                    LEFT JOIN usuarioprograma up ON (u.IDUSUARIO = up.IDUSUARIO AND up.IDPROGRAMA = " . $this->session->userdata('programa') . ")";
		}
		else
		{
			$sql = "SELECT 	u.*,
					    	c.*,
							DATE_FORMAT(u.DATA_ATIVACAO, '%d/%m/%Y %H:%i') AS ATIVACAO,
							CASE 
								WHEN u.ATIVO = 'S' THEN 'v_peq.png'
								ELSE 'x.png'
							END AS IMGSTATUS,
							CASE 
								WHEN u.ATIVO = 'S' THEN 'VALIDADO'
								ELSE 'NAO VALIDADO'
							END AS DESC_STATUS,
							CASE 
								WHEN up.IDPROGRAMA is not null THEN 'SIM'
								ELSE 'NAO'
							END AS USER_HABILITADO
						FROM usuario u
				  INNER JOIN cadpdv c ON (u.IDUSUARIO = c.IDUSUARIO) 
                  LEFT JOIN usuarioprograma up ON (u.IDUSUARIO = up.IDUSUARIO AND up.IDPROGRAMA = " . $this->session->userdata('programa') . ")";
		}
		
		// Filtragem - Serve tanto para o count quanto para dados de listagem
		// Filtro para estado
		if($this->UF != '')
		{
			$sql .= "  INNER JOIN logradouro  l ON (l.IDLOGRADOURO = c.IDLOGRADOURO)
					   INNER JOIN cidade     ci ON (ci.IDCIDADE = l.IDCIDADE AND IDUF = '" . $this->UF . "')";
		}
		
		// Adiciona um Where 1=1 para que os filtros abaixo possam ser utilizados
		$sql .= ' WHERE 1=1 ';
		
		// Filtro para habilitado
		if($this->HABILITADO != '')
		{
			if($this->HABILITADO == 'S')
            {
               $sql .= " and up.IDPROGRAMA is not null ";
            }
            else
            {
               $sql .= " and up.IDPROGRAMA is null ";
            }
		}
		
		// Filtro para status
		if($this->ATIVO != '')
		{
			$sql .= " AND u.ATIVO = '" . $this->ATIVO . "' ";
		}
		
		// Filtro para Razao e Nome Fantasia
		if($this->NOMEFANTASIA != '' && $this->RAZAOSOCIAL != '')
		{
			$sql .= " AND (c.RAZAOSOCIAL LIKE '%" . $this->RAZAOSOCIAL . "%' OR c.NOMEFANTASIA LIKE '%" . $this->NOMEFANTASIA . "%')";
		} else {
			// Filtro para Raz�o social
			if($this->RAZAOSOCIAL != '')
			{
				$sql .= " AND c.RAZAOSOCIAL LIKE '%" . $this->RAZAOSOCIAL . "%' ";
			}
			
			// Filtro para Nome Fantasia
			if($this->NOMEFANTASIA != '')
			{
				$sql .= " AND c.NOMEFANTASIA LIKE '%" . $this->NOMEFANTASIA . "%' ";
			}
		}
		
		// Finaliza SQL, somente para casos onde nao seja count
		if (!$onlyCount)
		{
			// Prossegue o sql
			$sql .= " ORDER BY c.RAZAOSOCIAL LIMIT " . $arrParam['limit_de'] . "," . $arrParam['exibir_pp'];
		}
		
		$dados = $this->db->query($sql);
		return $dados->result_array();
	}
	
	function getDadosCadastro($idUsuario)
	{
		$sql = sprintf("SELECT
							u.*,
							p.*,
							l.CEP,
							c.IDUF,
							c.NOMECIDADESUB,
							b.NOMEBAIRRO,
							l.NOMELOGRADOURO,
							CONCAT(t.NOMELOGRADOUROTIPO,' ',l.NOMELOGRADOURO) AS NOMELOGRADOURO2,
							l.COMPLEMENTO
						FROM usuario u
						INNER JOIN cadpdv        p ON (u.IDUSUARIO = p.IDUSUARIO)
						LEFT JOIN logradouro     l ON (l.IDLOGRADOURO = p.IDLOGRADOURO)
						LEFT JOIN logradourotipo t ON (t.IDLOGRADOUROTIPO = l.IDLOGRADOUROTIPO)
						LEFT JOIN bairro         b ON (b.IDBAIRRO = l.IDBAIRRO)
						LEFT JOIN cidade         c ON (c.IDCIDADE = b.IDCIDADE)
						WHERE u.IDUSUARIO = %s", $idUsuario);
						
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function setFiltro($coluna = null, $valor = null)
	{
		if(!is_null($coluna) && !is_null($valor))
		{
			$this->{strtoupper($coluna)} = addslashes($valor);
		}
	}
	
	function getArrayFiltros($arrDados = null)
	{
		$dados = array();
		if(!is_null($arrDados) && is_array($arrDados))
		{
			$dados['filtro_status'] = $arrDados['filtro_status'];
			$dados['filtro_habilitado'] = $arrDados['filtro_habilitado'];
			$dados['filtro_razao_nome_fantasia'] = $arrDados['filtro_razao_nome_fantasia'];
			$dados['filtro_estado']	= $arrDados['filtro_estado'];
		} else {
			$dados['filtro_status'] = '';
			$dados['filtro_habilitado'] = '';
			$dados['filtro_razao_nome_fantasia'] = '';
			$dados['filtro_estado']	= '';
		}
		return $dados;
	}
	
	function listaGeo($idCidade = 0, $tudo = 0)
	{
		$where = '';
		
		if ($idCidade != 0)
		{
			$where = sprintf("WHERE u.ATIVO = 'S' AND up.IDPROGRAMA is not null AND c.IDCIDADE = %s", $idCidade);
		}
		elseif ($tudo == 1)
		{
			$where = "WHERE u.ATIVO = 'S' AND up.IDPROGRAMA is not null ";
		}
		
		$sql = sprintf("SELECT
							u.IDUSUARIO,
							u.TIPOPESSOA,
							u.CPF_CNPJ,
							p.RAZAOSOCIAL,
							p.NOMEFANTASIA,
							p.TELEFONEGERAL,
							p.EMAILGERAL,
							p.NOMERESP,
							p.TELEFONERESP,
							p.EMAILRESP,
							p.SITE,
							p.END_NUMERO,
							p.END_COMPLEMENTO,
							p.LATITUDE,
							p.LONGITUDE,
							l.CEP,
							c.IDUF,
							c.IDCIDADE,
							c.NOMECIDADESUB,
							b.NOMEBAIRRO,
							l.NOMELOGRADOURO,
							CONCAT(t.NOMELOGRADOUROTIPO,' ',l.NOMELOGRADOURO) AS NOMELOGRADOURO2,
							l.COMPLEMENTO,
							p.IDLOGRADOURO
						FROM usuario u
						INNER JOIN cadpdv         p  ON (u.IDUSUARIO = p.IDUSUARIO)
						LEFT JOIN logradouro      l  ON (l.IDLOGRADOURO = p.IDLOGRADOURO)
						LEFT JOIN logradourotipo  t  ON (t.IDLOGRADOUROTIPO = l.IDLOGRADOUROTIPO)
						LEFT JOIN bairro          b  ON (b.IDBAIRRO = l.IDBAIRRO)
						LEFT JOIN cidade          c  ON (c.IDCIDADE = b.IDCIDADE) 
                        LEFT JOIN usuarioprograma up ON (u.IDUSUARIO = up.IDUSUARIO AND up.IDPROGRAMA = " . $this->session->userdata('programa') . ") 
						%s", $where);
		
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function listaGeo2($arrIdPdvs, $arrIdLogra)
	{
		$sql = sprintf("SELECT
							u.IDUSUARIO,
							u.TIPOPESSOA,
							u.CPF_CNPJ,
							p.RAZAOSOCIAL,
							p.NOMEFANTASIA,
							p.TELEFONEGERAL,
							p.EMAILGERAL,
							p.NOMERESP,
							p.TELEFONERESP,
							p.EMAILRESP,
							p.SITE,
							p.END_NUMERO,
							p.END_COMPLEMENTO,
							p.LATITUDE,
							p.LONGITUDE,
							l.CEP,
							c.IDUF,
							c.IDCIDADE,
							c.NOMECIDADESUB,
							b.NOMEBAIRRO,
							l.NOMELOGRADOURO,
							CONCAT(t.NOMELOGRADOUROTIPO,' ',l.NOMELOGRADOURO) AS NOMELOGRADOURO2,
							l.COMPLEMENTO,
							p.IDLOGRADOURO
						FROM usuario u
						INNER JOIN cadpdv        p ON (u.IDUSUARIO = p.IDUSUARIO)
						LEFT JOIN logradouro     l ON (l.IDLOGRADOURO = p.IDLOGRADOURO)
						LEFT JOIN logradourotipo t ON (t.IDLOGRADOUROTIPO = l.IDLOGRADOUROTIPO)
						LEFT JOIN bairro         b ON (b.IDBAIRRO = l.IDBAIRRO)
						LEFT JOIN cidade         c ON (c.IDCIDADE = b.IDCIDADE)
						WHERE u.ATIVO = 'S' 
						  AND u.HABILITADO = 'S'
						  AND u.IDUSUARIO NOT IN (%s)
						  AND p.IDLOGRADOURO IN (%s)", implode(',', $arrIdPdvs), implode(',', $arrIdLogra));
		
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function hasPDV($idbiblioteca = null)
	{
		$return = false;
		if(!is_null($idbiblioteca))
		{
			$sql = "SELECT COUNT(*) AS val FROM programapdvparc WHERE idbiblioteca = '$idbiblioteca' AND idprograma = " . IDPROGRAMA;

			$dados = $this->db->query($sql);
			$result = $dados->result_array($dados);
			
			if ($result[0]['val'] == 0) {
				$return = false;	
			} else {
				$return = true;
			}
		}
		return $return;
	}
	
	function removePDVBiblioteca($idbiblioteca = null)
	{
		$return = false;
		if(!is_null($idbiblioteca))
		{
			$sql = "DELETE FROM programapdvparc WHERE idbiblioteca = '$idbiblioteca' AND idprograma = " . IDPROGRAMA;

			$dados = $this->db->query($sql);
			
			$return = true;
		}
		return $return;
	}
	
	/*
	* get_dados_pdv()
	* Retorna os dados de um pdv
	* @param integer idusuario
	* return array pdv
	*/
	function get_dados_pdv($idusuario = 0, $idprograma = 0)
	{
		$sql = "SELECT Cp.IDUSUARIO AS IDPDV, 
					   Cp.* ,
					   U.*,
					   L.CEP AS `CEP_LOGRADOURO`,
					   L.*,
					   LT.*,
					   B.*,
					   C.*,
					   UF.*,
					   CP.IDUSUARIO,
					   CASE WHEN u.ATIVO = 'N' THEN '<img src=\"" . URL_IMG . "icon_bullet_red.png\" title=\"USUARIO DESATIVADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_bullet_green_2.png\" title=\"USUARIO ATIVO\" />' END AS ATIVO_IMG,
					   CASE WHEN up.IDPROGRAMA IS NULL THEN '<img src=\"" . URL_IMG . "icon_flag_red.png\" title=\"USUARIO DESABILITADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_flag_green.png\" title=\"USUARIO HABILITADO\" />' END AS HABILITADO_IMG
				  FROM CADPDV         CP
		    INNER JOIN USUARIO         U ON (U.IDUSUARIO = CP.IDUSUARIO) 
		     LEFT JOIN LOGRADOURO      L ON (L.IDLOGRADOURO = CP.IDLOGRADOURO) 
		     LEFT JOIN LOGRADOUROTIPO LT ON (LT.IDLOGRADOUROTIPO = L.IDLOGRADOUROTIPO)
			 LEFT JOIN BAIRRO          B ON (B.IDBAIRRO = L.IDBAIRRO)
		     LEFT JOIN CIDADE          C ON (C.IDCIDADE = L.IDCIDADE)
			 LEFT JOIN UF               UF ON (UF.IDUF = C.IDUF)
			 LEFT JOIN USUARIOPROGRAMA UP ON (UP.IDUSUARIO = CP.IDUSUARIO AND UP.IDPROGRAMA = $idprograma)
			WHERE CP.IDUSUARIO = $idusuario";
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados[0])) ? $dados[0] : array();
	}
	
	/**
	* get_lista_modulo()
	* Retorna os dados da listagem de entrada do modulo.
	* return array data
	*/
	function get_lista_modulo($filters = array())
	{
		$sql = "SELECT 	CE.IDUSUARIO AS `#`,
						CE.RAZAOSOCIAL AS `RAZÃO SOCIAL`,
						U.CPF_CNPJ AS `CPF/CNPJ`,
						CE.TELEFONEGERAL AS `TELEFONE`,
						CE.EMAILGERAL AS `EMAIL`,
						CE.NOMERESP AS `RESPONSÁVEL`,
						CE.TELEFONERESP AS `TEL. RESP.`,
						CE.EMAILRESP AS `EMAIL RESP.`,
						U.LOGIN,
						CASE WHEN u.ATIVO = 'N' THEN '<img src=\"" . URL_IMG . "icon_bullet_red.png\" title=\"USUARIO DESATIVADO\" />' ELSE '<img src=\"" . URL_IMG . "icon_bullet_green_2.png\" title=\"USUARIO ATIVO\" />' END AS ATIVO_IMG
				  FROM CADPDV CE
			INNER JOIN USUARIO          U ON(U.IDUSUARIO = CE.IDUSUARIO)
			 LEFT JOIN LOGRADOURO       L ON(L.IDLOGRADOURO = CE.IDLOGRADOURO)
			 LEFT JOIN CIDADE           C ON(C.IDCIDADE = L.IDCIDADE)
			 LEFT JOIN USUARIOPROGRAMA UP ON(UP.IDUSUARIO = U.IDUSUARIO)";
		
		// Carrega model auxiliar para processamento do where
		$this->load->model('global_model');
		$sql .= $this->global_model->get_where_filters($filters);
		
		// Concatena ORDER BY
		$sql .= ' ORDER BY CE.IDUSUARIO';
		
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados)) ? $dados : array();
	}
	
	/*
	* get_ufs_pdvs()
	* Retorna dados de ufs que possuem editoras.
	* return array ufs
	*/
	function get_ufs_pdvs()
	{
		$sql = "SELECT DISTINCT c.IDUF, C.IDUF AS LABEL FROM CADPDV CE
			INNER JOIN USUARIO          U ON(U.IDUSUARIO = CE.IDUSUARIO)
			 LEFT JOIN LOGRADOURO       L ON(L.IDLOGRADOURO = CE.IDLOGRADOURO)
			 LEFT JOIN CIDADE           C ON(C.IDCIDADE = L.IDCIDADE)
			 LEFT JOIN USUARIOPROGRAMA UP ON(UP.IDUSUARIO = U.IDUSUARIO)
			  ORDER BY IDUF";
		$dados  = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	/*
	* get_lista_pendencia_pedidos()
	* Retorna pedidos nao finalizados
	* return array dados
	*/
	function get_lista_pendencia_pedidos($idusuario, $idprograma)
	{
		$sql = "SELECT  DISTINCT P.IDPEDIDO
       				 FROM PEDIDO P
        		INNER JOIN PEDIDO_DOCUMENTO PD ON (PD.IDPEDIDO = P.IDPEDIDO)
        			WHERE PD.NUMERO_ENTREGA IS NULL
        			AND P.IDPDV = $idusuario
        			AND P.IDPROGRAMA = $idprograma";
		$dados = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados)) ? $dados : array();
	}	
}

/* End of file Pdv_model.php */
/* Location: ./system/application/models/Pdv_model.php */