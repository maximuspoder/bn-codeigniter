<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Isbn_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	function getIsbn($idIsbn)
	{
		$sql = sprintf("SELECT
							i.ISBN_TX_NUMERO      AS IDISBN,
							o.OBR_NR_ID           AS IDOBRA,
							o.OBR_TX_TITULO       AS TITULO,
							o.TSUP_AI_ID          AS IDSUPORTE,
							o.OBR_NR_PAGINAS      AS NPAGINAS,
							o.OBR_TX_CAPA         AS SIGLACAPA,
							o.OBR_TX_ACABAMENTO   AS SIGLAACAB,
							o.IDI_ID_ORIGEM       AS IDIDIOMAORIG,
							o.IDI_ID_DESTINO      AS IDIDIOMATRAD,
							o.ASS_AI_ID           AS IDASSUNTO,
							o.OBR_NR_EDICAO       AS EDICAO,
							o.OBR_NR_ANO_EDICAO   AS EDICAOANO,
							e.EDT_AI_ID           AS IDEDITORA,
							e.EDT_NR_QUALIFICADOR AS TIPOEDITORA
						FROM isbnonline.isbn_tb_isbn i
						INNER JOIN isbnonline.isbn_tb_obra              o  ON (i.OBR_NR_ID = o.OBR_NR_ID)
						INNER JOIN isbnonline.isbn_tb_prefixo_editorial p  ON (i.PE_NR_PREFIXO = p.PE_NR_PREFIXO)
						INNER JOIN isbnonline.isbn_tb_editor_x_selo     s  ON (p.SEL_AI_ID = s.SEL_AI_ID)
						INNER JOIN isbnonline.isbn_tb_editor            e  ON (s.EDT_AI_ID = e.EDT_AI_ID)
						WHERE i.ISBN_TX_NUMERO_PESQ = '%s'
						  AND s.EDT_SEL_DT_DESASSOC IS NULL", $idIsbn);
		
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
    function getLivrosIsbn($arrParam, $onlyCount = FALSE)
	{
        if ($onlyCount)
		{
            $sql = sprintf("SELECT 
                               count(*) as CONT
                            FROM isbnonline.isbn_titulo_unico l 
                            WHERE NOT EXISTS (SELECT 1 FROM bibliotecaindicacao bi WHERE bi.IDLIVRO = l.COD_AUX_TITULO) %s ",   $arrParam['where']);
            
        }
		else
		{

            $sql = sprintf("SELECT 
                                l.COD_AUX_TITULO AS IDLIVRO,
                                l.TITULO AS TITULO,
                                l.AUTORES AS AUTOR
                            FROM isbnonline.isbn_titulo_unico l 
                            WHERE NOT EXISTS (SELECT 1 FROM bibliotecaindicacao bi WHERE bi.IDLIVRO = l.COD_AUX_TITULO) %s LIMIT %s, %s ", $arrParam['where'], $arrParam['limit_de'], $arrParam['exibir_pp']);
        
        }
        
        $dados = $this->db->query($sql);
		return $dados->result_array();
    }
    
    function getLivrosIsbnIndicacao($arrParam, $onlyCount = FALSE)
	{
        if ($onlyCount)
		{
            $sql = sprintf("SELECT COUNT(*) as CONT
                            FROM isbnonline.isbn_titulo_unico T
                            LEFT JOIN BIBLIOTECAINDICACAO BI ON (T.COD_AUX_TITULO = BI.IDLIVRO AND BI.IDUSUARIO = ".$this->session->userdata('idUsuario').") 
                            %s ", $arrParam['where']);
            
        }
		else
		{
            $sql = sprintf("SELECT T.COD_AUX_TITULO AS IDLIVRO,
                                T.TITULO AS TITULO,
                                T.AUTORES AS AUTOR,
                                BI.IDUSUARIO
                            FROM isbnonline.isbn_titulo_unico T
                            LEFT JOIN BIBLIOTECAINDICACAO BI ON (T.COD_AUX_TITULO = BI.IDLIVRO AND BI.IDUSUARIO = ".$this->session->userdata('idUsuario').")
                            %s LIMIT %s, %s ", $arrParam['where'], $arrParam['limit_de'], $arrParam['exibir_pp']);
        }
        $dados = $this->db->query($sql);
		return $dados->result_array();
    }
    
    function getLivrosIsbnIndicacaoAdm($arrParam, $onlyCount = FALSE)
	{
        if ($onlyCount)
		{
            $sql = sprintf("SELECT COUNT(*) as CONT
                                FROM isbnonline.isbn_titulo_unico T
                                %s ", $arrParam['where']);
            
        }
		else
		{
            $sql = sprintf("SELECT T.COD_AUX_TITULO AS IDLIVRO,
                                    T.TITULO AS TITULO,
                                    T.AUTORES AS AUTOR,
                                    T.BLOQUEADO AS BLOQUEADO, 
                                    (select count(*) from bibliotecaindicacao where idlivro = T.COD_AUX_TITULO ) AS N_INDICACAO
                                FROM isbnonline.isbn_titulo_unico T
                            %s LIMIT %s, %s ", $arrParam['where'], $arrParam['limit_de'], $arrParam['exibir_pp']);
          
        }
        $dados = $this->db->query($sql);
		return $dados->result_array();
    }
    
	function getAutoresObra($idObra)
	{
		$sql = sprintf("SELECT PAR_TX_NOME
						FROM isbnonline.isbn_tb_obra_participante
						WHERE OBR_AI_ID = %s
						  AND TPAR_AI_ID = 1", $idObra);
		
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function getEditorasTituloUnico($idTituloUnico = 0) {
	
		if ($idTituloUnico != 0) {
			$sql = sprintf("SELECT DISTINCT e.EDT_AI_ID           AS IDEDITORA, e.EDT_TX_CONTATO AS NOMEEDITORA
							FROM isbnonline.isbn_tb_isbn i
							INNER JOIN isbnonline.isbn_tb_obra              o  ON (i.OBR_NR_ID = o.OBR_NR_ID)
							INNER JOIN isbnonline.isbn_tb_prefixo_editorial p  ON (i.PE_NR_PREFIXO = p.PE_NR_PREFIXO)
							INNER JOIN isbnonline.isbn_tb_editor_x_selo     s  ON (p.SEL_AI_ID = s.SEL_AI_ID)
							INNER JOIN isbnonline.isbn_tb_editor            e  ON (s.EDT_AI_ID = e.EDT_AI_ID)
							WHERE s.EDT_SEL_DT_DESASSOC IS NULL AND o.cod_aux_titulo = %d ORDER BY e.EDT_TX_CONTATO ASC ", $idTituloUnico );
			
			$dados = $this->db->query($sql);
		
			return $dados->result_array();
		}
		else
		{
			return false;
		}
	}
	
	function getDadosEditora($idEditora, $tipoEditora)
	{
		if ($tipoEditora == 1) //pessoa física
		{
			$sql = sprintf("SELECT 
								p.EDT_TX_CPF        AS CPFCNPJ,
								p.EDT_TX_NOME       AS RAZAOSOCIAL,
								p.EDT_TX_PSEUDONIMO AS NOMEFANTASIA
							FROM isbnonline.isbn_tb_editor_pf p
							INNER JOIN isbnonline.isbn_tb_editor e ON (p.EDT_AI_ID = e.EDT_AI_ID)
							WHERE p.EDT_AI_ID = %s", $idEditora);
		}
		else //pessoa jurídica
		{
			$sql = sprintf("SELECT 
								p.EDT_TX_CNPJ      AS CPFCNPJ,
								p.EDT_TX_RSOCIAL   AS RAZAOSOCIAL,
								p.EDT_TX_NFANTASIA AS NOMEFANTASIA
							FROM isbnonline.isbn_tb_editor_pj p
							INNER JOIN isbnonline.isbn_tb_editor e ON (p.EDT_AI_ID = e.EDT_AI_ID)
							WHERE p.EDT_AI_ID = %s", $idEditora);
		}
		
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function validaEditora($cpjCnpj, $tipoEditora)
	{
		if ($tipoEditora == 1) //pessoa física
		{
			$sql = sprintf("SELECT 
								p.EDT_TX_CPF        AS CPFCNPJ,
								p.EDT_TX_NOME       AS RAZAOSOCIAL,
								''                  AS NOMEFANTASIA,
								e.EDT_TX_EMAIL_SEC  AS EMAIL,
								e.EDT_TX_SITE       AS SITE
							FROM isbnonline.isbn_tb_editor_pf p
							INNER JOIN isbnonline.isbn_tb_editor e ON (p.EDT_AI_ID = e.EDT_AI_ID)
							WHERE p.EDT_TX_CPF = '%s'", $cpjCnpj);
		}
		else //pessoa jurídica
		{
			$sql = sprintf("SELECT 
								p.EDT_TX_CNPJ       AS CPFCNPJ,
								p.EDT_TX_RSOCIAL    AS RAZAOSOCIAL,
								p.EDT_TX_NFANTASIA  AS NOMEFANTASIA,
								e.EDT_TX_EMAIL_SEC  AS EMAIL,
								e.EDT_TX_SITE       AS SITE
							FROM isbnonline.isbn_tb_editor_pj p
							INNER JOIN isbnonline.isbn_tb_editor e ON (p.EDT_AI_ID = e.EDT_AI_ID)
							WHERE p.EDT_TX_CNPJ = '%s'", $cpjCnpj);
		}
		
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
}

/* End of file Isbn_model.php */
/* Location: ./system/application/models/Isbn_model.php */