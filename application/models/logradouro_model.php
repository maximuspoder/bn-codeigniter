<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logradouro_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	function pesquisaCep($uf, $cidade, $nomelogra)
	{
		$sql = sprintf("SELECT
							l.IDLOGRADOURO,
							l.CEP,
							c.IDUF,
							c.IDCIDADE,
							c.NOMECIDADE,
							c.NOMECIDADESUB,
							b.IDBAIRRO,
							b.NOMEBAIRRO,
							t.NOMELOGRADOUROTIPO,
							l.NOMELOGRADOURO,
							l.COMPLEMENTO,
							l.LOGRA_ADD,
							'USUARIO' as USUARIO
						FROM logradouro l
						INNER JOIN logradourotipo t ON (t.IDLOGRADOUROTIPO = l.IDLOGRADOUROTIPO)
						INNER JOIN bairro         b ON (b.IDBAIRRO = l.IDBAIRRO)
						INNER JOIN cidade         c ON (c.IDCIDADE = b.IDCIDADE)
						LEFT JOIN usuario         u ON (u.IDUSUARIO = l.IDBAIRRO)
						WHERE c.IDUF = '%s'
						  %s
						  AND l.NOMELOGRADOURO LIKE '%%%s%%'
						ORDER BY c.NOMECIDADESUB,
								 b.NOMEBAIRRO, 
								 t.NOMELOGRADOUROTIPO, 
								 l.NOMELOGRADOURO", $uf, 
													($cidade != 0 ? 'AND l.IDCIDADE = ' . $cidade : ''), 
													$nomelogra);
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function pesquisaLogradouro($cep)
	{
		$sql = sprintf("SELECT
							l.IDLOGRADOURO,
							l.CEP,
							c.IDUF,
							c.IDCIDADE,
							c.NOMECIDADE,
							c.NOMECIDADESUB,
							b.IDBAIRRO,
							b.NOMEBAIRRO,
							t.NOMELOGRADOUROTIPO,
							l.NOMELOGRADOURO,
							l.COMPLEMENTO
						FROM logradouro l
						INNER JOIN logradourotipo t ON (t.IDLOGRADOUROTIPO = l.IDLOGRADOUROTIPO)
						INNER JOIN bairro         b ON (b.IDBAIRRO = l.IDBAIRRO)
						INNER JOIN cidade         c ON (c.IDCIDADE = b.IDCIDADE)
						WHERE l.CEP = '%s'
						ORDER BY b.NOMEBAIRRO, t.NOMELOGRADOUROTIPO, l.NOMELOGRADOURO", $cep);
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function pesquisaBairro($cep)
	{
		$sql = sprintf("SELECT
							c.IDUF,
							c.IDCIDADE,
							c.NOMECIDADE,
							c.NOMECIDADESUB,
							b.IDBAIRRO,
							b.NOMEBAIRRO
						FROM bairrofaixacep b2
						INNER JOIN bairro   b ON (b.IDBAIRRO = b2.IDBAIRRO)
						INNER JOIN cidade   c ON (c.IDCIDADE = b.IDCIDADE)
						WHERE CAST('%s' AS UNSIGNED) BETWEEN CAST(CONCAT(b2.RAD_INI,b2.SUF_INI) AS UNSIGNED) 
														 AND CAST(CONCAT(b2.RAD_FIM,b2.SUF_FIM) AS UNSIGNED)
						ORDER BY b.NOMEBAIRRO", $cep);
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function pesquisaCidade($cep)
	{
		$sql = sprintf("SELECT
							c.IDUF,
							c.IDCIDADE,
							c.NOMECIDADE,
							c.NOMECIDADESUB
						FROM cidadefaixacep c2
						INNER JOIN cidade   c ON (c.IDCIDADE = c2.IDCIDADE)
						WHERE c.CEP IS NULL
						  AND (CAST('%s' AS UNSIGNED) BETWEEN CAST(CONCAT(c2.RAD1_INI,c2.SUF1_INI) AS UNSIGNED) 
														  AND CAST(CONCAT(c2.RAD1_FIM,c2.SUF1_FIM) AS UNSIGNED)
							   OR
							   CAST('%s' AS UNSIGNED) BETWEEN CAST(CONCAT(c2.RAD2_INI,c2.SUF2_INI) AS UNSIGNED) 
														  AND CAST(CONCAT(c2.RAD2_FIM,c2.SUF2_FIM) AS UNSIGNED)
							   )
						
						UNION ALL 
						
						SELECT
							c.IDUF,
							c.IDCIDADE,
							c.NOMECIDADE,
							c.NOMECIDADESUB
						FROM cidade c
						WHERE c.CEP = '%s'", $cep, $cep, $cep);
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function getUF()
	{
		$sql = "SELECT
					u.IDUF,
					u.NOMEUF
				FROM uf u
				ORDER BY u.IDUF";
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function getCidades($uf)
	{
		$sql = sprintf("SELECT
							c.IDCIDADE,
							c.NOMECIDADE,
							c.NOMECIDADESUB
						FROM cidade c
						WHERE c.IDUF = '%s'
						  AND c.ATIVA = 'S'
						ORDER BY c.NOMECIDADE", $uf);
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}

	function getCidadesPesquisa($uf)
	{
		$sql = sprintf("SELECT
							c.NOMECIDADESUB,
							c.NOMECIDADESUB AS sub2
						FROM cidade c
						WHERE c.IDUF = '%s'
						  AND c.ATIVA = 'S'
						ORDER BY c.NOMECIDADESUB", $uf);
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function getBairros($cidade)
	{
		$sql = sprintf("SELECT
							b.IDBAIRRO,
							b.NOMEBAIRRO
						FROM bairro b
						WHERE b.IDCIDADE = %s
						ORDER BY b.NOMEBAIRRO", $cidade);
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
	
	function getTipoLogra()
	{
		$sql = "SELECT
					t.IDLOGRADOUROTIPO,
					t.NOMELOGRADOUROTIPO
				FROM logradourotipo t
				WHERE t.IDLOGRADOUROTIPO IN (61,73,83,93,99,102,105,107,128,140,141,146,174,181,196,198,212,216,233,235)
				ORDER BY t.NOMELOGRADOUROTIPO";
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
	}
    
    function getEnderecoByLogradouro($idLogradouro) {
        
        $sql = sprintf("SELECT
							l.IDLOGRADOURO,
							l.CEP,
							c.IDUF,
							c.IDCIDADE,
							c.NOMECIDADE,
							c.NOMECIDADESUB,
							b.IDBAIRRO,
							b.NOMEBAIRRO,
							t.NOMELOGRADOUROTIPO,
							l.NOMELOGRADOURO,
							l.COMPLEMENTO
						FROM logradouro l
						INNER JOIN logradourotipo t ON (t.IDLOGRADOUROTIPO = l.IDLOGRADOUROTIPO)
						INNER JOIN bairro         b ON (b.IDBAIRRO = l.IDBAIRRO)
						INNER JOIN cidade         c ON (c.IDCIDADE = b.IDCIDADE)
						WHERE l.IDLOGRADOURO = '%s'
						ORDER BY b.NOMEBAIRRO, t.NOMELOGRADOUROTIPO, l.NOMELOGRADOURO", $idLogradouro);
        
		$dados = $this->db->query($sql);
		
		return $dados->result_array();
        
    }
    
	function getUFByCidade($cidade)
	{
		$sql = sprintf("SELECT
							C.IDUF
						FROM CIDADE C
						WHERE C.IDCIDADE = %s", $cidade);
		
		$dados = $this->db->query($sql);
		$res = $dados->result_array();
		return $res[0]['IDUF'];
	}
}

/* End of file logradouro_model.php */
/* Location: ./system/application/models/logradouro_model.php */