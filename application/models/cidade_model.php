<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cidade_Model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	function getCidadesComBibliotecasPai($iduf = '')
	{
		$sql = "SELECT IDCIDADE, 
					   NOMECIDADE
			  	  FROM CIDADE
				 WHERE IDCIDADE IN (SELECT DISTINCT   C.IDCIDADE
								  FROM CIDADE         C
							INNER JOIN LOGRADOURO     L ON (L.IDCIDADE = C.IDCIDADE)
							INNER JOIN CADBIBLIOTECA CB ON (CB.IDLOGRADOURO = L.IDLOGRADOURO)
							WHERE CB.IDUSUARIO IN (SELECT IDUSUARIOPAI FROM CADBIBLIOTECA CB2 WHERE CB2.IDUSUARIOPAI = CB.IDUSUARIO)";
		$sql .= ($iduf != '') ? " AND C.IDUF = '$iduf'" : '';
		$sql .= ")";
		$dados = $this->db->query($sql);
		return $dados->result_array();
	}
	
	function isVerificada($idcidade = '')
	{
		$result = false;
		if($idcidade != '')
		{
			$sql = "SELECT VERIFICADA FROM CIDADE WHERE IDCIDADE = $idcidade";
			$dados  = $this->db->query($sql);
			$dados  = $dados->result_array();
			$result = ($dados[0]['VERIFICADA'] == 'S') ? true : false;
		}
		return $result;
	}
	
	function set_cidade_verificada($idcidade = '', $situacao = '')
	{
		if($idcidade != '' && $situacao != '')
		{
			$sql = "UPDATE CIDADE SET VERIFICADA = '$situacao' WHERE IDCIDADE = $idcidade";
			$dados  = $this->db->query($sql);
		}
	}
	
	function isVerificadaSniic($idcidade = '')
	{
		$result = false;
		if($idcidade != '')
		{
			$sql = "SELECT VERIFICADA_SNIIC FROM CIDADE WHERE IDCIDADE = $idcidade";
			$dados  = $this->db->query($sql);
			$dados  = $dados->result_array();
			$result = ($dados[0]['VERIFICADA_SNIIC'] == 'S') ? true : false;
		}
		return $result;
	}
	
	function set_cidade_verificada_sniic($idcidade = '', $situacao = '')
	{	
		$result = false;
		if($idcidade != '' && $situacao != '')
		{
			$sql = "UPDATE CIDADE SET VERIFICADA_SNIIC = '$situacao' WHERE IDCIDADE = $idcidade";
			$dados  = $this->db->query($sql);
		}
	}
	
	function getIdCidadeByNomeExtensoUF($nomecidade = '', $iduf = '')
	{
		$result = false;
		if($nomecidade != '' && $iduf != '')
		{
			$sql = "SELECT IDCIDADE FROM CIDADE C WHERE C.NOMECIDADE = '".removeAcentos($nomecidade, true)."' AND C.IDUF = '$iduf'";
			$dados  = $this->db->query($sql);
			$dados  = $dados->result_array();
			$result = $dados[0]['IDCIDADE'];
		}
		return $result;
	}
	
	
	function getCidadesSniicCredito($uf=''){
        
        $arrDados = array();
        $sql = "SELECT DISTINCT sb.IDCIDADE, sb.ENDER_MUNICIPIO
        		FROM sniiconline.sniic_biblioteca sb
        		INNER JOIN sniiconline.credito sc ON sb.id_biblioteca = sc.id_biblioteca
        		INNER JOIN uf ON sb.ender_estado = uf.nomeuf
        		INNER JOIN cidade bc ON sb.idcidade = bc.idcidade
        		INNER JOIN logradouro l ON bc.idcidade = l.idcidade
        		LEFT JOIN cadbiblioteca cb ON sb.id_biblioteca = cb.idsniic
        		WHERE (cb.idsniic = 0 or cb.idsniic is null) 	
        		AND bc.TIPOCIDADE = 'M'";

        if ($uf != "") $sql .= " AND uf.IDUF = '$uf'";
        
        $sql .= " ORDER BY sb.ender_estado, sb.ender_municipio";
        
		$arrDados = $this->db->query($sql);
        $rs3 = $arrDados->result_array();
        return $rs3;
    }

    function getCidadesEstadosSniicCredito($uf=''){
        
        $arrDados = array();
        $sql = "SELECT DISTINCT sb.ENDER_MUNICIPIO, UF.IDUF
        		FROM sniiconline.sniic_biblioteca sb
        		INNER JOIN sniiconline.credito sc ON sb.id_biblioteca = sc.id_biblioteca
        		INNER JOIN binac.uf ON sb.ender_estado = uf.nomeuf
        		INNER JOIN cidade bc ON sb.idcidade = bc.idcidade
        		INNER JOIN binac.logradouro l ON bc.idcidade = l.idcidade
        		LEFT JOIN binac.cadbiblioteca cb ON sb.id_biblioteca = cb.idsniic
        		WHERE (cb.idsniic = 0 or cb.idsniic is null) 	
        		AND bc.TIPOCIDADE = 'M'";

        if ($uf != "") $sql .= " AND uf.IDUF = '$uf'";
        
        $sql .= " ORDER BY sb.ender_estado, sb.ender_municipio";

		$arrDados = $this->db->query($sql);
        $rs3 = $arrDados->result_array();
        
        return $rs3;
    }   
	function getCidadesCadbibliotecaUnificacaoCadastro($uf=''){

    	// pega as cidades que possuem 2 ou mais bibliotecas cadastradas
        // atrav�s do munic�pio
        
        $arrDados = array();
            
        $sql = "SELECT 
                        (CASE WHEN C2.IDCIDADESUB = 0 THEN C2.IDCIDADE ELSE C2.IDCIDADESUB END) AS IDCIDADE, 
						(CASE WHEN VERIFICADA = 'S' THEN CONCAT(C2.NOMECIDADE, ' (VERIFICADA)') ELSE C2.NOMECIDADE END) AS NOMECIDADE
                              FROM CIDADE C2 
                              INNER JOIN LOGRADOURO L ON (L.IDCIDADE = C2.IDCIDADE)
                              INNER JOIN CADBIBLIOTECA CB ON (CB.IDLOGRADOURO = L.IDLOGRADOURO)
                              GROUP BY (CASE WHEN C2.IDCIDADESUB = 0 THEN C2.IDCIDADE ELSE C2.IDCIDADESUB END), C2.IDUF
                              HAVING COUNT(CB.IDUSUARIO) > 1";

        if ($uf != "") $sql .= " AND C2.IDUF = '$uf'";
        
        $sql .= " ORDER BY C2.IDUF, C2.NOMECIDADE";
        
		$arrDados = $this->db->query($sql);
        $rs3 = $arrDados->result_array();
        
        return $rs3;
    }
	
	/*
	* get_cidades_by_uf()
	* Retorna dados de cidades de uma uf.
	* return array cidades
	*/
	function get_cidades_by_uf($uf)
	{
		$sql = "SELECT DISTINCT  C.IDCIDADE, c.NOMECIDADE AS LABEL FROM CIDADE C WHERE IDUF = '$uf' ORDER BY NOMECIDADE";
		$dados  = $this->db->query($sql);
		$dados = $dados->result_array();
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}
}
