<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Logsis Class
 *
 * @package		Application
 * @subpackage	Libraries
 * @category	Logs do sistema
 * @author		Moisés Viana
 * 
 */
class Logsis {
	
	var $CI;
	
	/**
	 * Constructor
	 *
	 * @access	public
	 * @param	Array
	 * Array[0] OPERACAO   -> "INSERT", "UPDATE", "DELETE", "LIST", "ACCESS"
	 * Array[1] RESULTADO  -> "OK" OU "ERRO"
	 * Array[2] TABELA REFERENCIA
	 * Array[3] ID AUXILIAR
	 * Array[4] DESCRICAO
	 * Array[5] TEXTO
	 */
	
	function __construct($dadosLog = null)
	{
		$this->CI =& get_instance();
		$this->CI->load->database();
		$this->CI->load->library('session');
		
		if (!is_null($dadosLog)) {
			$this->insereLog($dadosLog);
		}
	}
	
	function insereLog($dadosLog)
	{
		$sql = sprintf("INSERT INTO logsis (IDUSUARIO,OPERACAO,RESULTADO,TABELA,IDAUX,DESCRICAO,TEXTO,DATALOG,HORALOG,IPLOG)
						VALUES (%s,'%s','%s','%s',%s,'%s','%s','%s','%s','%s')", $this->CI->session->userdata('idUsuario'),
																				$dadosLog[0],
																				$dadosLog[1],
																				$dadosLog[2],
																				($dadosLog[3] == '' ? 0 : $dadosLog[3]),
																				$dadosLog[4],
																				addslashes($dadosLog[5]),
																				date('Y/m/d'),
																				date('H:i:s'),
																				$_SERVER['REMOTE_ADDR']);
		$this->CI->db->query($sql);
	}
	
}

/* End of file Logsis.php */