<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Usuario Class
 *
 * @package		Application
 * @subpackage	Libraries
 * @category	Usuario
 * @author		Moises
 * @link		
 */

class Usuario_class {

	var $CI;
	
	/**
	 * Constructor
	 *
	 * @access	public
	 * @param	
	 */
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->model('global_model');
		$this->CI->load->model('usuario_model');
	}
	
	/**
	 * Insere socios
	 *
	 * @access	public
	 * @param	integer idusuario
	 * @param	string socios
	 * @return	void
	 */
	function save_socio($idUsuario, $strSocios)
	{
		$arrSocios = ($strSocios != '' ? explode('##', $strSocios) : Array());
		$tamArr    = count($arrSocios);
		
		if ($tamArr > 0)
		{
			for ($i = 0; $i < $tamArr; $i++)
			{
				$arrAux = explode('||', $arrSocios[$i]);
				
				//$arrAux[0] CPF
				//$arrAux[1] NOME
				//$arrAux[2] % PARTICIPACAO
				//$arrAux[3] SOCIO ADMIN
				
				$arrDadosSocios = Array();
				$arrDadosSocios['IDUSUARIO']     = $idUsuario;
				$arrDadosSocios['NOMESOCIO']     = removeAcentos($arrAux[1], TRUE);
				$arrDadosSocios['CPF']           = $arrAux[0];
				$arrDadosSocios['PARTICIPACAO']  = $arrAux[2];
				$arrDadosSocios['ADMINISTRADOR'] = ($arrAux[3] == 'S' ? 'S' : 'N');
				
				$this->CI->global_model->insert('cadsocio', $arrDadosSocios);
			}
		}
	}
	
	/**
	 * Deleta todos os socios do usuario
	 *
	 * @access	public
	 * @param	integer idusuario
	 * @return	result
	 */
	function delete_socio($idUsuario)
	{
		return $this->CI->global_model->delete('cadsocio', 'IDUSUARIO = ' . $idUsuario);
	}
	
	/**
	 * Retorna socios
	 *
	 * @access	public
	 * @param	integer idusuario
	 * @return	result
	 */
	function get_socio($idUsuario, $tipoRet = '')
	{
		$aDados = $this->CI->usuario_model->getCadSocios($idUsuario);
		
		if ($tipoRet == 'string')
		{
			$arrAux = Array();
			
			for ($i = 0; $i < count($aDados); $i++)
			{
				array_push($arrAux, $aDados[$i]['CPF'] . '||' . $aDados[$i]['NOMESOCIO'] . '||' . $aDados[$i]['PARTICIPACAO'] . '||' . $aDados[$i]['ADMINISTRADOR']);
			}
			
			return implode('##', $arrAux);
		}
		else
		{
			return $aDados;
		}
	}
	
	/**
	 * Insere Atuação
	 *
	 * @access	public
	 * @param	integer idusuario
	 * @param	string socios
	 * @return	void
	 */
	function save_atuacao($idUsuario, $strAtuacao, $principal = 0)
	{
		$arrSocios = ($strAtuacao != '' ? explode('##', $strAtuacao) : Array());
		
		if ( ! in_array($principal, $arrSocios) && $principal != 0)
		{
			array_push($arrSocios, $principal);
		}
		
		$tamArr = count($arrSocios);
		
		if ($tamArr > 0)
		{
			for ($i = 0; $i < $tamArr; $i++)
			{
				$arrDadosAtvd = Array();
				$arrDadosAtvd['IDUSUARIO']   = $idUsuario;
				$arrDadosAtvd['IDATIVIDADE'] = $arrSocios[$i];
				$arrDadosAtvd['PRINCIPAL']   = ($arrSocios[$i] == $principal ? 'S' : 'N');
				
				$this->CI->global_model->insert('cadatividade', $arrDadosAtvd);
			}
		}
	}
	
	/**
	 * Deleta todos atividades de atuação do usuario
	 *
	 * @access	public
	 * @param	integer idusuario
	 * @return	result
	 */
	function delete_atuacao($idUsuario)
	{
		return $this->CI->global_model->delete('cadatividade', 'IDUSUARIO = ' . $idUsuario);
	}
	
	/**
	 * Retorna atividades que atua
	 *
	 * @access	public
	 * @param	integer idusuario
	 * @return	result
	 */
	function get_atuacao($idUsuario, $tipoRet = '')
	{
		$aDados = $this->CI->usuario_model->getCadAtuacao($idUsuario);
		
		if ($tipoRet == 'string')
		{
			$arrAux = Array();
			
			for ($i = 0; $i < count($aDados); $i++)
			{
				array_push($arrAux, $aDados[$i]['IDATIVIDADE']);
			}
			
			return implode('##', $arrAux);
		}
		else
		{
			return $aDados;
		}
	}
	
	/**
	 * Insere Região de atendimento
	 *
	 * @access	public
	 * @param	integer idusuario
	 * @param	string regioes
	 * @return	void
	 */
	function save_regiao_atende($idUsuario, $strRegiao)
	{
		$arrRegiao = ($strRegiao != '' ? explode('#', $strRegiao) : Array());
		$tamArr    = count($arrRegiao);
		
		if ($tamArr > 0)
		{
			for ($i = 0; $i < $tamArr; $i++)
			{
				$arrAux = explode('|', $arrRegiao[$i]);
				
				//$arrAux[0] IDUF
				//$arrAux[1] S para TOTAL N para PARCIAL
				
				$arrDadosReg = Array();
				$arrDadosReg['IDUSUARIO'] = $idUsuario;
				$arrDadosReg['IDUF']      = $arrAux[0];
				$arrDadosReg['TOTAL']     = ($arrAux[1] == 'S' ? 'S' : 'N');
				
				$this->CI->global_model->insert('cadatendimentouf', $arrDadosReg);
			}
		}
	}
	
	/**
	 * Insere Cidades de atendimento
	 *
	 * @access	public
	 * @param	integer idusuario
	 * @param	string cidades
	 * @return	void
	 */
	function save_cidade_atende($idUsuario, $strCidade)
	{
		$arrCid = ($strCidade != '' ? explode('#', $strCidade) : Array());
		$tamArr = count($arrCid);
		
		if ($tamArr > 0)
		{
			for ($i = 0; $i < $tamArr; $i++)
			{
				$arrDadosCid = Array();
				$arrDadosCid['IDUSUARIO'] = $idUsuario;
				$arrDadosCid['IDCIDADE']  = $arrCid[$i];
				
				$this->CI->global_model->insert('cadatendimentocid', $arrDadosCid);
			}
		}
	}
	
	/**
	 * Deleta regiao de atendimento do usuario
	 *
	 * @access	public
	 * @param	integer idusuario
	 * @return	result
	 */
	function delete_regiao_atende($idUsuario)
	{
		$this->CI->global_model->delete('cadatendimentocid', 'IDUSUARIO = ' . $idUsuario);
		return $this->CI->global_model->delete('cadatendimentouf', 'IDUSUARIO = ' . $idUsuario);
	}
	
	/**
	 * Retorna regiao que usuario atende
	 *
	 * @access	public
	 * @param	integer idusuario
	 * @param	string  tipo de retorno uf ou cidade
	 * @return	result
	 */
	function get_regiao_atende($idUsuario, $tipoRet)
	{
		if ($tipoRet == 'uf')
		{
			$aDados = $this->CI->usuario_model->getCadRegiaoAtende($idUsuario);
			
			$arrAux = Array();
			
			for ($i = 0; $i < count($aDados); $i++)
			{
				array_push($arrAux, $aDados[$i]['IDUF'] . '|' . $aDados[$i]['TOTAL']);
			}
			
			return implode('#', $arrAux);
		}
		elseif ($tipoRet == 'cidade')
		{
			$aDados = $this->CI->usuario_model->getCadCidadeAtende($idUsuario);
			
			$arrAux = Array();
			
			for ($i = 0; $i < count($aDados); $i++)
			{
				array_push($arrAux, $aDados[$i]['IDCIDADE']);
			}
			
			return implode('#', $arrAux);
		}
	}
	
	/**
	 * Insere editoras com que trabalha
	 *
	 * @access	public
	 * @param	integer idusuario
	 * @param	string editoras
	 * @return	void
	 */
	function save_editoratrab($idUsuario, $strEditora)
	{
		$arrE   = ($strEditora != '' ? explode('#', $strEditora) : Array());
		$tamArr = count($arrE);
		
		if ($tamArr > 0)
		{
			for ($i = 0; $i < $tamArr; $i++)
			{
				$arrDadosE = Array();
				$arrDadosE['IDDISTRIBUIDOR'] = $idUsuario;
				$arrDadosE['IDEDITORA']      = $arrE[$i];
				
				$this->CI->global_model->insert('caddistribuidoreditora', $arrDadosE);
			}
		}
	}
	
	/**
	 * Deleta editoras com que trabalha
	 *
	 * @access	public
	 * @param	integer idusuario
	 * @return	result
	 */
	function delete_editoratrab($idUsuario)
	{
		return $this->CI->global_model->delete('caddistribuidoreditora', 'IDDISTRIBUIDOR = ' . $idUsuario);
	}
	
	/**
	 * Retorna editoras com que trabalha
	 *
	 * @access	public
	 * @param	integer idusuario
	 * @return	result
	 */
	function get_editoratrab($idUsuario, $tipoRet = '')
	{
		$aDados = $this->CI->usuario_model->getCadEditorasTrab($idUsuario);
		
		if ($tipoRet == 'string')
		{
			$arrAux = Array();
			
			for ($i = 0; $i < count($aDados); $i++)
			{
				array_push($arrAux, $aDados[$i]['IDEDITORA']);
			}
			
			return implode('#', $arrAux);
		}
		else
		{
			return $aDados;
		}
	}
	
}

/* End of file Usuario_class.php */
/* Location: ./application/libraries/Usuario_class.php */