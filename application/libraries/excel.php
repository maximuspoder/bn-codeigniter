<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe Excel
*
* Esta biblioteca gerencia funcionalidades para excel de maneira geral, como importação de arquivos
* xls para o banco de dados, exportação xls utilizando sql ou array de resultset de dados.
* 
* @author		Leandro Mangini Antunes
* @package		application
* @subpackage	application.excel
* @since		2012-04-26
*
*/
class Excel {
	// Definição de Atributos
	protected $columns = array();
	protected $file_name = 'default';
	protected $extension = 'xls';
	protected $encode = 'utf-8';
	protected $data = array();
	
	/**
	* __construct()
	* Construtor de classe.
	* @return object
	*/
	function __construct()
	{
	}
	
	/**
	* set_columns()
	* Array definindo quais colunas serão visiveis no momento da exportação
	* ou importação.
	* @param array data
	* @return void
	*/
	public function set_columns($data = array())
	{
		$this->columns = (!is_null($data) && count($data) > 0 ) ? $data : $this->columns;
	}
	
	/**
	* set_data()
	* Define o array data (conteudo) do arquivo xls.
	* @param array data
	* @return void
	*/
	public function set_data($data = array())
	{
		$this->data = (!is_null($data) && count($data) > 0 ) ? $data : $this->data;
	}
	
	/**
	* set_extension()
	* Seta a extensão do arquivo a ser exportado/importado.
	* @param string extension
	* @return void
	*/
	public function set_extension($extension = '')
	{
		$this->extension = $extension;
	}
	
	/**
	* set_file_name()
	* Seta o nome do arquivo a ser exportado/importado.
	* @param string file_name
	* @return void
	*/
	public function set_file_name($file_name = '')
	{
		$this->file_name = $file_name;
	}
	
	/**
	* download_file()
	* Através de um resultset encaminhado, monta o xls e força download.
	* Construtor de classe.
	* @param array data
	* @return void
	*/
	function download_file()
	{
		if(count($this->data) > 0)
		{
			// Seta nome do arquivo e faz controles com idpedido
			$data = $this->data;
			$filename = $this->file_name . $this->extension;
			$contents = '';
			
			// Carrega models necessários e CI (codeigniter), para manipulação
			$CI = get_instance();
			
			// O array de resultset vem no formato para foreach, ou seja
			// $data[0]['IDLIVRO'], temos que transformar para numerico para montar cabeçalho
			$aux_data = array_keys($data[0]);
			
			// Monta o cabeçalho conforme as colunas que foram setadas
			$count_header = count($data[0]);
			$count_colums = count($this->columns);
			for($i = 0; $i < $count_header; $i++)
			{
				if($count_colums > 0)
				{
					$contents .= (in_array($i, $this->columns)) ? $aux_data[$i] . "\t" : '';
				} else {
					$contents .= $aux_data[$i] . "\t";
				}
			}
			
			// Cabeçalho pronto, quebra linha para conteudo do arquivo
			$contents .= "\n";
			$i = 0;
			foreach($data as $arr_data)
			{
				foreach($arr_data as $index => $value)
				{
					if($count_colums > 0)
					{
						$contents .= (in_array($i, $this->columns)) ? $value . "\t" : '';
					} else {
						$contents .= $value . "\t";
					}
				}
				$contents .= "\n";
			}
			
			// Exporta efetivamente, o xls para tela
			// header('Content-Length: '. strlen($contents));// 
			header('Content-Description: File Transfer');
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename='.$filename);
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			echo chr(255) . chr(254) . mb_convert_encoding($contents, 'UTF-16LE', 'UTF-8');
			exit;
		}
	}
}
