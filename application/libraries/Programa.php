<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe Programa
*
* Esta classe contém métodos para a abstração da entidade programa.
* 
* @author		Leandro Mangini Antunes
* @package		application
* @subpackage	application.programa
* @since		2012-03-24
*
*/
class Programa {
	
	/**
	* __construct()
	* @return object
	*/
	function __construct()
	{
	}
	
	/**
	* get_first_programa()
	* Retorna o PRIMEIRO programa cadastrado no banco de dados.
	* @return integer idprograma
	*/
	function get_first_programa()
	{
		$CI =& get_instance();
		$CI->load->model('global_model');
		$dados  = $CI->global_model->selectx('programa', '1=1', 'IDPROGRAMA');
		$return = $dados[0]['IDPROGRAMA'];
		return $return;
	}
	
	/**
	* get_nome_programa()
	* Retorna o nome do programa, com base no idprograma encaminhado.
	* @param integer idprograma
	* @return string nome_programa
	*/
	function get_nome_programa($idprograma = 0)
	{
		$CI =& get_instance();
		$CI->load->model('global_model');
		if($idprograma == 0 || $idprograma == -1)
		{
			$return = 'Nenhum edital selecionado';
		} else {
			$dados  = $CI->global_model->selectx('programa', 'idprograma = ' . $idprograma, 'IDPROGRAMA');
			$return = $dados[0]['NOME_EDITAL'];
		}
		return $return;
	}
	
	/**
	* get_icone_programa()
	* Retorna o icone do programa, com base no idprograma encaminhado.
	* @param integer idprograma
	* @return string icone_programa
	*/
	function get_icone_programa($idprograma = 0)
	{
		$CI =& get_instance();
		$CI->load->model('global_model');
		if($idprograma == 0  || $idprograma == -1)
		{
			$return = 'icon_nenhum_edital.png';
		} else {
			$dados  = $CI->global_model->selectx('programa', 'idprograma = ' . $idprograma, 'ICONE_EDITAL');
			$return = $dados[0]['ICONE_EDITAL'];
		}
		return $return;
	}
	
	/**
	* get_dados_programa()
	* Retorna todas as informações do programa.
	* @param integer idprograma
	* @return array programa
	*/
	function get_dados_programa($idprograma = 0)
	{
		$CI =& get_instance();
		$CI->load->model('global_model');
		if($idprograma == 0)
		{
			$return = array();
		} else {
			$dados  = $CI->global_model->selectx('programa', 'idprograma = ' . $idprograma, 'IDPROGRAMA');
			$return = $dados[0];
		}
		return $return;
	}
	
	/**
	* get_pendencias_biblioteca()
	* Retorna um html com as pendencias de uma biblioteca, com base no 
	* idprograma encaminhado.
	* @param integer idprograma
	* @return html pendencias
	*/
	function get_pendencias_biblioteca($idprograma = 0, $idusuario = 0)
	{
		$return = '';
		$CI =& get_instance();
		$CI->load->model('global_model');
		$CI->load->model('biblioteca_model');
		$CI->load->model('configuracao_model');
		if($idprograma == 0)
		{
			$return = get_mensagem('success', '', 'Nenhuma pendência para este edital');
		} else {
			// Ajusta idusuario para uso interno
			$idusuario = ($idusuario != '' && $idusuario != 0) ? $idusuario : $CI->session->userdata('idUsuario');
			
			// Coleta informação se usuário está habilitado ou não
			$habilitado = $CI->global_model->get_dado('usuarioprograma', "'S'", 'IDUSUARIO = ' . $idusuario . ' AND IDPROGRAMA =' . $idprograma);
			
			// Validação de PDV
			$pdvParc = $CI->biblioteca_model->getPdvParceiro($idprograma, $idusuario);
			$jaEscolheuPdv = (count($pdvParc) > 0 ? TRUE : FALSE);
			$pdvParceiro = $pdvParc;
			$boolPeriodoSetPdv = ($CI->configuracao_model->validaConfig('7', date('Y/m/d'))) ? TRUE : FALSE;
			$config7 = $CI->configuracao_model->getConfigData('7');
			$dateDePdv  = (count($config7) > 0) ? $config7[0]['DATA_DE'] : '';
			$dateAtePdv = (count($config7) > 0) ? $config7[0]['DATA_ATE'] : '';
			
			// Verifica os pedidos com entrgas não finalizadas.
			$args['dados'] = $CI->biblioteca_model->get_lista_pendencia_pedidos($idusuario, $idprograma);
			if(count($args['dados']) > 0)
			{
				$conteudo= '';
				foreach($args['dados'] as $id_pedido)
				{
					$conteudo .= '&bull; O pedido de id <strong>'  . $id_pedido['IDPEDIDO'] . '</strong> ainda não foi finalizado.<a href="' .  URL_EXEC . 'pedido/conferencia/' . $id_pedido['IDPEDIDO'] . '"> Clique aqui</a> para ir para a conferência <br />';
				}
				$return .= get_mensagem('error', '', $conteudo);
			}

			// PDV PARCEIRO
			if($habilitado == 'S' && $boolPeriodoSetPdv && ! $jaEscolheuPdv)
			{
				$return .= get_mensagem('error', '', 'O período para escolha do seu Ponto de Venda Parceiro já está em aberto e você tem até o dia ' . eua_to_br($dateAtePdv) . ' para fazer sua escolha. O Ponto de Venda Parceiro é o estabelecimento que irá receber o seu pedido com a lista dos livros escolhidos para aquisição. <a href="' . URL_EXEC . 'biblioteca/escolhapdv">Escolher PDV agora</a>', false, 'margin-bottom:10px;');
			}
			elseif($habilitado == 'S' && $jaEscolheuPdv)
			{
				// $return .= get_mensagem('note', '', 'Seu Ponto de Venda Parceiro é: <a href="' . URL_EXEC . 'pesquisa/exibeDadosPesquisa/4/' . $pdvParceiro[0]['IDPDV'] . '">' . $pdvParceiro[0]['RAZAOSOCIAL'] . '</a>', false, 'margin-bottom:10px;');
			}
			
			// Responsável financeiro para o programa atual
			$financeiro = $CI->global_model->get_dado('cadbibliotecafinan', '1', 'IDBIBLIOTECA = ' . $idusuario . ' AND IDPROGRAMA = ' . $idprograma);
			$return .= ($financeiro >= 1) ? '' : get_mensagem('error', '', 'Você deve cadastrar o responsável financeiro da sua biblioteca clicando <a href="' . URL_EXEC . 'biblioteca/respFinanceiroFormcad">aqui</a>', false, 'margin-bottom:10px;');
			
			// Comitê de acervo para o programa atual
			$comite = ($CI->session->userdata('idMembro') == 0) ? true : false ;
			$qtde_comite = $CI->biblioteca_model->get_count_comite($idusuario, $idprograma);
			$return .= ($comite && $qtde_comite < 3 && $habilitado == 'S') ? get_mensagem('error', '', 'Você deve cadastrar no mínimo 3 membros no Comitê de Acervo. <a href="' . URL_EXEC .'biblioteca/comite">Cadastrar</a>', false, 'margin-bottom:10px;') : '';
			
			// Varre todos os membros de comite de acervo, verifica se algum deles tem cadastro nao ativo
			/*
			$comite_acervo = $CI->biblioteca_model->get_comite_acervo($CI->session->userdata('idUsuario'), $idprograma);
			$membros_nao_validados = '';
			if(count($comite_acervo) > 0)
			{
				foreach($comite_acervo as $membro)
				{
					$membros_nao_validados .= (get_value($membro, 'ATIVO') == 'N') ? '<br />&bull;&nbsp;' . get_value($membro, 'RAZAOSOCIAL') : '';
				}
			}
			$return .= ($membros_nao_validados != '') ? get_mensagem('error', '', 'Os seguintes membros do comitê de acervo ainda não validaram seu cadastro:' . $membros_nao_validados, false, 'margin-bottom:10px;') : '';
			*/
			
			// VERIFICA SE EXISTE AGENCIA BB RELACIONADA
			// SE IDFINANCEIRO == 0, ENTAO USUARIO LOGADO É UMA BIBLITOECA
			// NESTE CASO DEVE SOMENTE ALERTAR QUE RESP FINANCEIRO DEVE INFORMAR
			// AGENCIA DE RELACIONAMENTO BB
			$agenciabb = '0';
			$isFinanceiro = ($CI->session->userdata('idFinanceiro') == 0) ? false : true;
			$isBiblioteca = ($CI->session->userdata('idMembro') == 0 ) ? true : false;
			
			// Verifica a agencia de relacionamento
			if($isFinanceiro)
			{
				$agenciabb = $CI->global_model->get_dado('cadbibliotecafinan', 'IDAGENCIA', 'IDUSUARIO = ' . $CI->session->userdata('idFinanceiro'));
			} else if($isBiblioteca) {
				$agenciabb = $CI->global_model->get_dado('cadbibliotecafinan', 'IDAGENCIA', 'IDBIBLIOTECA = ' . $idusuario);
			}
			$arrDados['agenciabb'] = ($agenciabb == '0' || $agenciabb == '') ? false : true;
			
			// É utilizado no caso do usuário ser um responsavel financeiro (para associacao de agencia bb)
			$arrDados['isBiblioteca'] = $isBiblioteca;
			$arrDados['isFinanceiro'] = $isFinanceiro;
			$arrDados['idusuario'] = $idusuario;
			$arrDados['idfinanceiro'] = $CI->session->userdata('idFinanceiro');
			
			// Cadastramento da agência do banco do brasil
			if(!$agenciabb && $isFinanceiro)
			{
				$return .= get_mensagem('error', '', 'Você ainda não informou a agência de relacionamento com o Banco do brasil de sua biblioteca. <br />Associe agora clicando <a href="javascript:void(0);" onclick="modalAssociarAgenciaBB(\'idagenciabb\', \'$(\'form_hidden_agencia_bb\').submit();\');">aqui</a>', false, 'margin-bottom:10px;');
				$return .= '<form name="form_hidden_agencia_bb" id="form_hidden_agencia_bb" action="' . URL_EXEC . 'biblioteca/associarAgenciaBB/" method="post">
								<input type="hidden" name="idagenciabb" id="idagenciabb" value=""/>
								<input type="hidden" name="idbiblioteca" id="idbiblioteca" value="' . $idusuario . '"/>
								<input type="hidden" name="idrespfinanceiro" id="idrespfinanceiro" value=" ' . $idfinanceiro . '"/>
							</form>';	
			} 
			// NO CASO DE SER UMA BIBLIOTECA LOGADA, APENAS EXIBE MENSAGEM DIZENDO
			// QUE NAO HA AGENCIA DE RELACIONAMENTO BB ASSOCIADA
			elseif(!$agenciabb && $isBiblioteca && $financeiro) 
			{
				$return .= get_mensagem('warning', 'Atenção', 'O Responsável Financeiro ainda não informou sua agência de relacionamento com o Banco do Brasil. Solicite que ele acesse sua área de trabalho no Portal FBN mediante login e senha.&nbsp;', false, 'margin-bottom:10px;');
			}
		}

		// Caso nenhuma pendência
		$return = ($return == '') ? get_mensagem('success', '', 'Nenhuma pendência para este edital') : $return;
		
		return $return;
	}
	
	/**
	* get_pendencias_distribuidor()
	* Retorna um html com as pendencias de um distribuidor, com base no 
	* idprograma encaminhado.
	* @param integer idprograma
	* @return html pendencias
	*/
	function get_pendencias_distribuidor($idprograma = 0)
	{
		// Distribuidor não tem pendências para nenhum edital, por enquanto
		$return = '';
		$return = ($return == '') ? get_mensagem('success', '', 'Nenhuma pendência para este edital') : $return;
		return $return;
	}
	
	/**
	* get_pendencias_pdv()
	* Retorna um html com as pendencias de um pdv, com base no 
	* idprograma encaminhado.
	* @param integer idprograma
	* @return html pendencias
	*/
	function get_pendencias_pdv($idprograma = 0, $idusuario = 0)
	{
		$CI =& get_instance();
		$CI->load->model('pdv_model');
		$return = '';

		// Coleta na sessão o id do usuário.
		$idusuario = ($idusuario != '' && $idusuario != 0) ? $idusuario : $CI->session->userdata('idUsuario');
		
		// Verifica os pedidos com entrgas não finalizadas.
		$args['dados'] = $CI->pdv_model->get_lista_pendencia_pedidos($idusuario, $idprograma);
		if(count($args['dados']) > 0)
		{	
			$conteudo= '';
			foreach($args['dados'] as $id_pedido)
			{
				
				$conteudo .= '&bull; O pedido de id <strong>'  . $id_pedido['IDPEDIDO'] . '</strong> ainda não foi finalizado.<a href="' .  URL_EXEC . 'pedido/conferencia/' . $id_pedido['IDPEDIDO'] . '"> Clique aqui</a> para ir para a conferência <br />';
			}
			$return .= get_mensagem('error', '', $conteudo);
		}	

		// PDV não tem pendências para nenhum edital, por enquanto
		$return = ($return == '') ? get_mensagem('success', '', 'Nenhuma pendência para este edital') : $return;
		return $return;
	}
	
	/**
	* get_pendencias_editora()
	* Retorna um html com as pendencias de uma editora, com base no 
	* idprograma encaminhado.
	* @param integer idprograma
	* @return html pendencias
	*/
	function get_pendencias_editora($idprograma = 0)
	{
		$return = '';
		$CI =& get_instance();
		$CI->load->model('global_model');
		$CI->load->model('biblioteca_model');
		$CI->load->model('configuracao_model');
		if($idprograma == 0)
		{
			$return = get_mensagem('success', '', 'Nenhuma pendência para este edital');
		} else {
			// IDCONFIGURACAO 6: Período para inclusão de livros no Programa Livro Popular (informar quantidades disponíveis e valor).
			$boolPeriodoInclusaoLivro = ($CI->configuracao_model->validaConfig('6', date('Y/m/d'))) ? TRUE : FALSE;
			$config = $CI->configuracao_model->getConfigData('6');
			$dateAteInclusaoLivro = (count($config) > 0) ? $config[0]['DATA_ATE'] : '';
			if($boolPeriodoInclusaoLivro)
			{
				$return .= get_mensagem('error', '', 'O período para inclusão de livros no Programa Livro Popular já está em aberto e você tem até o dia' . eua_to_br($dateAteInclusaoLivro) . ' para incluí-los.', false, 'margin-bottom:10px;');
			}
		}
		// Caso nenhuma pendência
		$return = ($return == '') ? get_mensagem('success', '', 'Nenhuma pendência para este edital') : $return;
		
		return $return;
	}
	
	/**
	* get_quotas_e_pedidos()
	* Retorna html de quotas e pedidos para o usuario atual, caso possua.
	* @param integer idprograma
	* @return string html_quotas_e_pedidos
	*/
	function get_quotas_e_pedidos($idprograma = 0)
	{
		$return = '';
		$CI =& get_instance();
		$CI->load->model('pedido_model');
		$CI->load->model('programa_model');
		if($idprograma != 0)
		{
			// Coleta pedidos para o programa encaminhado
			$pedidos = $CI->pedido_model->get_pedidos($CI->session->userdata('idUsuario'), $idprograma);
			
			// Coleta créditos (contemplações) para o programa encaminhado
			$creditos = $CI->programa_model->get_creditos($CI->session->userdata('idUsuario'), $idprograma);
			
			if(count($pedidos) > 0 || count($creditos) > 0)
			{
				$return .= '<div style="margin-top:15px;">';
				$return .= '<strong><img id="pedidos_img" src="' . URL_IMG . 'icon_bullet_plus.png" style="float:left;margin:1px 3px 0 0;cursor:pointer" onclick="show_area(\'pedidos\');" />Pedidos e Quotas</strong><br />';
				$return .= '<div id="pedidos" style="display:none;">';
				
				// Varre todos os pedidos
				foreach($pedidos as $pedido)
				{
					$return .= '<div><img src="' . URL_IMG . 'icon_pedidos.png" style="float:left;margin-right:3px;" />Pedido ID ' . get_value($pedido, 'IDPEDIDO') . ' (Valor: R$ ' . masc_real(get_value($pedido, 'VALORTOTAL')) . ')</div>';
				}
				
				// Varre todos os créditos
				foreach($creditos as $credito)
				{
					$return .= '<div><img src="' . URL_IMG . 'icon_coins.png" style="float:left;margin-right:3px;" />Quota do edital no valor de R$ ' . masc_real(get_value($credito, 'VLRCREDITO_INICIAL')) . '</div>';
				}
				$return .= '</div>';
				$return .= '</div>';
			}
		}
		return $return;
	}
	
	/**
	* get_responsavel_financeiro()
	* Retorna um com um icone de usuario e o nome do responsvel financeiro
	* com base no idprograma encaminhado.
	* @param integer idprograma
	* @return html resp_finan
	*/
	function get_responsavel_financeiro($idprograma = 0)
	{
		$return = '';
		$CI =& get_instance();
		$CI->load->model('biblioteca_model');
		if($idprograma != 0)
		{
			$financeiro = $CI->biblioteca_model->get_responsavel_financeiro($CI->session->userdata('idUsuario'), $idprograma);
			if($financeiro != '')
			{
				$return .= '<div style="margin-top:15px;">';
				$return .= '<strong><img id="resp_finan_img" src="' . URL_IMG . 'icon_bullet_plus.png" style="float:left;margin:1px 3px 0 0;cursor:pointer" onclick="show_area(\'resp_finan\');" />Responsável Financeiro</strong><br />';
				$return .= '<div id="resp_finan" style="display:none;">';
				$return .= '<img src="' . URL_IMG . 'icon_user.png" style="float:left;margin-right:3px;" />' . $financeiro;
				$return .= '</div>';
				$return .= '</div>';
			}
		}
		return $return;
	}
	
	/**
	* get_comite()
	* Retorna um html com um icone de usuario e o nome de cada membro do comite
	* de acervo relacionado ao biblioteca com base no idprograma encaminhado.
	* @param integer idprograma
	* @return html resp_finan
	*/
	function get_comite($idprograma = 0)
	{
		$return = '';
		$CI =& get_instance();
		$CI->load->model('biblioteca_model');
		if($idprograma != 0)
		{
			$comite = $CI->biblioteca_model->get_comite($CI->session->userdata('idUsuario'), $idprograma);
			if(count($comite) > 0)
			{
				$return .= '<div style="margin-top:15px;">';
				$return .= '<strong><img id="comite_acervo_img" src="' . URL_IMG . 'icon_bullet_plus.png" style="float:left;margin:1px 3px 0 0;cursor:pointer" onclick="show_area(\'comite_acervo\');" />Comitê de Acervo</strong><br />';
				$return .= '<div id="comite_acervo" style="display:none;">';
				foreach($comite as $membro)
				{
					if(get_value($membro, 'NOMERESP') != '')
					{
						$return .= '<div><img src="' . URL_IMG . 'icon_user_black.png" style="float:left;margin-right:3px;" />' . get_value($membro, 'NOMERESP') . '</div>';
					}
				}
				$return .= '</div>';
				$return .= '</div>';
			}
		}
		return $return;
	}
	
	/**
	* get_pdv_parceiro()
	* Retorna um html com um icone de um usuario e o nome do pdv parceiro
	* relacionado ao biblioteca com base no idprograma encaminhado.
	* @param integer idprograma
	* @return html pdv_parc
	*/
	function get_pdv_parceiro($idprograma = 0)
	{
		$return = '';
		$CI =& get_instance();
		$CI->load->model('biblioteca_model');
		if($idprograma != 0)
		{
			$pdv = $CI->biblioteca_model->get_pdv_parceiro($CI->session->userdata('idUsuario'), $idprograma);
			if($pdv != '')
			{
				$return .= '<div style="margin-top:15px;">';
				$return .= '<strong><img id="pdv_parceiro_img" src="' . URL_IMG . 'icon_bullet_plus.png" style="float:left;margin:1px 3px 0 0;cursor:pointer" onclick="show_area(\'pdv_parceiro\');" />PDV Parceiro</strong><br />';
				$return .= '<div id="pdv_parceiro" style="display:none">';
				$return .= '<img src="' . URL_IMG . 'icon_pdv.png" style="float:left;margin-right:3px;" />' . $pdv;
				$return .= '</div>';
				$return .= '</div>';
			}
		}
		return $return;
	}
}
