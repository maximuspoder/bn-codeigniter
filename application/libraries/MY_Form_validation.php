<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Extensão da Form Validation Class
 *
 * @package		Application
 * @subpackage	Libraries
 * @category	Validation
 * @author		MCV
 * @link		http://codeigniter.com/user_guide/libraries/form_validation.html
 */
class MY_Form_validation extends CI_Form_validation {
	
	var $_error_prefix = '<div class="error">'; //alterado prefix e suffix dos erros de validação
	var $_error_suffix = '</div>';
	
	/**
     * Valida se o campo possui uma data válida
     *
     * @access	public
     * @param	string data no formato brasileiro
     * @return	bool
     */
	function valida_data($str)
    {
		if ($str != '')
		{
			if (is_numeric(substr($str,0,2)) && is_numeric(substr($str,3,2)) && is_numeric(substr($str,6,4)))
			{
				if (checkdate(substr($str,3,2), substr($str,0,2), substr($str,6,4)) == TRUE)
				{
					return TRUE;
				}
				else
				{
					return FALSE;
				}
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return TRUE;
		}
    }
	
	/**
     * Valida se o campo possui um timestamp válido (a partir de 1970)
     *
     * @access	public
     * @param	string data no formato brasileiro
     * @return	bool
     */
	function valida_timestamp($str)
    {
		if ($str != '')
		{
			if (is_numeric(substr($str,0,2)) && is_numeric(substr($str,3,2)) && is_numeric(substr($str,6,4)))
			{
				if(substr($str,6,4) >= 1970)
				{
					if (checkdate(substr($str,3,2), substr($str,0,2), substr($str,6,4)) == TRUE)
					{
						return TRUE;
					}
				}
				else
				{
					return FALSE;
				}
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return TRUE;
		}
    }
	
    /**
     * Valida se o campo possui um valor em reais válido
     *
     * @access	public
     * @param	string
     * @return	bool
     */
	function valida_moeda($str)
    {
		$moedaAux = '';
		if ($str != '')
		{
			if (strpos($str, ','))
			{
				$moedaAux = str_replace('.', '',  $str);
				$moedaAux = str_replace(',', '.', $moedaAux);
			}
			else
			{
				$moedaAux = $str;
			}
			
			if (is_numeric($moedaAux))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return TRUE;
		}
    }
	
    /**
     * Valida se o campo foi selecionado
     *
     * @access	public
     * @param	string
     * @return	bool
     */
	function valida_combo($str)
    {
		if ($str != '' && $str != '0')
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
    }
	
	/**
     * Valida se sócios foram informados corretamente
     *
     * @access	public
     * @param	string
     * @return	bool
     */
	function valida_socios($str)
    {
		if ($str != '')
		{
			$arr1    = explode('##', $str);
			$tamArr1 = count($arr1);
			
			if ($tamArr1 > 0)
			{
				for ($i = 0; $i < $tamArr1; $i++)
				{
					$arr2 = explode('||', $arr1[$i]);
					
					//$arr2[0] CPF
					//$arr2[1] NOME
					//$arr2[2] % PARTICIPACAO
					//$arr2[3] SOCIO ADMIN
					
					if ($arr2[0] == '' || ! $this->valida_cnpj_cpf($arr2[0]) || trim($arr2[1]) == '' || trim($arr2[2]) == '' || trim($arr2[3]) == '')
					{
						return FALSE;
					}
				}
				
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return TRUE;
		}
    }
	
	/**
     * Valida se atuacao fo informada corretamente
     *
     * @access	public
     * @param	string
     * @return	bool
     */
	function valida_atuacao($str)
    {
		if ($str != '')
		{
			$arr1    = explode('##', $str);
			$tamArr1 = count($arr1);
			
			if ($tamArr1 > 0)
			{
				for ($i = 0; $i < $tamArr1; $i++)
				{
					if ( ! is_numeric($arr1[$i]))
					{
						return FALSE;
					}
				}
				
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return TRUE;
		}
    }
	
	/**
     * Valida se foi passada uma string de numeros separado por um #
     *
     * @access	public
     * @param	string
     * @return	bool
     */
	function valida_str_codigos($str)
    {
		if ($str != '')
		{
			$arr1    = explode('#', $str);
			$tamArr1 = count($arr1);
			
			if ($tamArr1 > 0)
			{
				for ($i = 0; $i < $tamArr1; $i++)
				{
					if ( ! is_numeric($arr1[$i]))
					{
						return FALSE;
					}
				}
				
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return TRUE;
		}
    }
	
	/**
     * Valida regioes de atendimento foram informadas corretamente
     *
     * @access	public
     * @param	string
     * @return	bool
     */
	function valida_regiaoatend($str)
    {
		if ($str != '')
		{
			$arr1    = explode('#', $str);
			$tamArr1 = count($arr1);
			
			if ($tamArr1 > 0)
			{
				for ($i = 0; $i < $tamArr1; $i++)
				{
					$arr2 = explode('|', $arr1[$i]);
					
					//$arr2[0] IDUF
					//$arr2[1] S para TOTAL N para PARCIAL
					
					if (strlen($arr2[0]) != 2 || ($arr2[1] != 'S' && $arr2[1] != 'N'))
					{
						return FALSE;
					}
				}
				
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return TRUE;
		}
    }
	
	/**
     * Valida se o campo tem um CNPJ ou CPF válido
     *
     * @access	public
     * @param	string
     * @return	bool
     */
	function valida_cnpj_cpf($str)
    {
		if ($str != '')
		{
			$str = str_replace('.', '',  $str);
			$str = str_replace('-', '',  $str);
			$str = str_replace('/', '',  $str);
			
			if (strlen($str) == 11)
			{
				$cpf = $str;
				
				if(preg_match("/^[0-9]{11,11}$/", $cpf))
				{
					// Verifica se veio preenchido com números iguais
					if( ($cpf == '11111111111') || ($cpf == '22222222222') ||
						($cpf == '33333333333') || ($cpf == '44444444444') ||
						($cpf == '55555555555') || ($cpf == '66666666666') ||
						($cpf == '77777777777') || ($cpf == '88888888888') ||
						($cpf == '99999999999') || ($cpf == '00000000000') )
					{
						return FALSE;
					} else {
						// Retira o ultimo digito verificador
						$dv_informado = substr($cpf, 9,2);
						for($i=0; $i<=8; $i++) {
							$digito[$i] = substr($cpf, $i,1);
						}
						// Calcula o valor do 10º digito de verificação
						$posicao = 10;
						$soma = 0;
						for($i=0; $i<=8; $i++) {
							$soma = $soma + $digito[$i] * $posicao;
							$posicao = $posicao - 1;
						}
						$digito[9] = $soma % 11;
						if($digito[9] < 2) {
							$digito[9] = 0;
						} else {
							$digito[9] = 11 - $digito[9];
						}
						// Calcula o valor do 11º digito de verificação
						$posicao = 11;
						$soma = 0;
						for ($i=0; $i<=9; $i++) {
							$soma = $soma + $digito[$i] * $posicao;
							$posicao = $posicao - 1;
						}
						$digito[10] = $soma % 11;
						if ($digito[10] < 2) {
							$digito[10] = 0;
						} else {
							$digito[10] = 11 - $digito[10];
						}
						// Verifica se o DV calculado é igual ao do CPF
						$dv = $digito[9] * 10 + $digito[10];
						if ($dv != $dv_informado) {
							return FALSE;
						} else { 
							return TRUE;
						}
					}
				}
			}
			elseif (strlen($str) == 14)
			{
				$cnpj = $str;
				
				if(preg_match("/^[0-9]{14,14}$/", $cnpj))
				{
					$soma1 =($cnpj[0]  * 5) +
							($cnpj[1]  * 4) +
							($cnpj[2]  * 3) +
							($cnpj[3]  * 2) +
							($cnpj[4]  * 9) +
							($cnpj[5]  * 8) +
							($cnpj[6]  * 7) +
							($cnpj[7]  * 6) +
							($cnpj[8]  * 5) +
							($cnpj[9]  * 4) +
							($cnpj[10] * 3) +
							($cnpj[11] * 2);
					$resto = $soma1 % 11;
					$digito1 = $resto < 2 ? 0 : 11 - $resto;
					$soma2 =($cnpj[0]  * 6) +
							($cnpj[1]  * 5) +
							($cnpj[2]  * 4) +
							($cnpj[3]  * 3) +
							($cnpj[4]  * 2) +
							($cnpj[5]  * 9) +
							($cnpj[6]  * 8) +
							($cnpj[7]  * 7) +
							($cnpj[8]  * 6) +
							($cnpj[9]  * 5) +
							($cnpj[10] * 4) +
							($cnpj[11] * 3) +
							($cnpj[12] * 2);
					$resto = $soma2 % 11;
					$digito2 = $resto < 2 ? 0 : 11 - $resto;
					
					return (($cnpj[12] == $digito1) && ($cnpj[13] == $digito2));
				}
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return TRUE;
		}
    }

}

/* End of file MY_Form_validation.php */
/* Location: ./application/libraries/MY_Form_validation.php */